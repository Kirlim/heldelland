#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec3 color;

out vec3 fragPos;
out vec3 fragNml;
out vec2 fragUV0;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    vec4 worldPosition = model * vec4(position, 1.0f);
    fragPos = worldPosition.xyz;

    gl_Position = projection * view * worldPosition;
    fragUV0 = vec2(texCoords.x, 1-texCoords.y);

    mat3 normalMatrix = transpose(inverse(mat3(model)));
    fragNml = normalMatrix * normal;
}
#version 330 core
in vec2 TexCoords;
out vec4 FragColor;

uniform sampler2D MainTexture;

void main()
{    
    vec4 sampled = vec4(1.0, 1.0f, 1.0, texture(MainTexture, TexCoords).r);
    FragColor = sampled;
    //color = vec4(textColor, 1.0) * sampled;
}
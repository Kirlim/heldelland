#version 330 core

layout (location = 0) in vec4 vertex;
layout (location = 2) in vec2 texCoords;

out vec2 TexCoords;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main()
{
    gl_Position = projection * view * model * vertex;
    TexCoords = vec2(texCoords.x, 1-texCoords.y);
} 
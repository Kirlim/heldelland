#version 330 core
out vec4 FragColor;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;

uniform vec3 lightColor;
uniform vec3 lightPosition;
uniform float lightLinear;
uniform float lightQuadratic;

uniform vec2 screenSize;



vec2 GetTexCoords()
{
	return(gl_FragCoord.xy / screenSize);
}



vec3 CalculatePointLight(vec3 worldPos, vec3 normal)
{
	vec3 lightDir = lightPosition - worldPos;
	float dist = length(lightDir);
	lightDir = normalize(lightDir);

	float attenuation = 1.0 +
						lightLinear * dist +
						lightQuadratic * dist * dist;
	attenuation = max(1.0, attenuation);

	vec2 texCoords = GetTexCoords(); // TODO : remove duplicated texcoords code
	vec3 albedo = texture(gAlbedoSpec, texCoords).rgb;
	vec3 color = albedo / 3.1415;
	vec3 diffuse = (max(0.0, dot(normal, lightDir)) * lightColor) / attenuation;
	color *= diffuse;

	return(color);
}



void main() 
{
	vec2 texCoords = GetTexCoords();

	// Retrieve data from the gBuffer
	vec3 fragPos = texture(gPosition, texCoords).rgb;
	vec3 normal = texture(gNormal, texCoords).rgb;
	float specular = texture(gAlbedoSpec, texCoords).a;

	// Will store the results of lightings
	FragColor = vec4(CalculatePointLight(fragPos, normal), 1.0);
}




#version 330 core
layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;

in vec3 fragPos;
in vec3 fragNml;
in vec2 fragUV0;

uniform sampler2D albedoTexture;



void main()
{
	gPosition = fragPos;
	gNormal = normalize(fragNml);
	gAlbedoSpec.rgb = texture(albedoTexture, fragUV0).rgb;
	gAlbedoSpec.a = 0.0f;
}
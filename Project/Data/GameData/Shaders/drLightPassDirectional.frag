#version 330 core
out vec4 FragColor;
in vec2 TexCoords;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;



struct DirectionalLight
{
	vec3 direction;
	vec3 color;
};

const int NR_DIRECTIONAL_LIGHTS = 1;
uniform DirectionalLight directionalLights[NR_DIRECTIONAL_LIGHTS];

uniform vec3 viewPos;



void main() 
{
	// Retrieve data from the gBuffer
	vec3 fragPos = texture(gPosition, TexCoords).rgb;
	vec3 normal = texture(gNormal, TexCoords).rgb;
	vec3 albedo = texture(gAlbedoSpec, TexCoords).rgb;
	float specular = texture(gAlbedoSpec, TexCoords).a;

	// Will store the results of lightings
	vec3 lighting = albedo / 3.1415;

	// Calculate directional lighting
	for(int i = 0; i < NR_DIRECTIONAL_LIGHTS; ++i)
	{
		lighting *= max(dot(normal, 0 - directionalLights[i].direction), 0.0) * directionalLights[i].color;
	}

	FragColor = vec4(lighting, 1.0);
}




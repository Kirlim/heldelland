# Tries to find Soil2 lib.
# Will also search with the following path variables:
#	SOIL_LOCATION_HINT
#
# Once colled, will define
# 	SOIL2_FOUND
#	SOIL2_INCLUDE_DIR
#	SOIL2_LIBRARIES

find_path(
	SOIL2_INCLUDE_DIR
		SOIL2/SOIL2.h
	PATHS
		/usr/include
		/usr/local/include
		/usr/share/include
		${SOIL2_LOCATION_HINT}/include
	DOC
		"Location which SOIL2/SOIL2.h can be found."
)

find_library(
	SOIL2_LIBRARIES
		SOIL2
	NAMES
		soil2
		soil2-debug
	PATHS
		${SOIL2_INCLUDE_DIR}/lib
		/usr/lib
		/usr/local/lib
		/usr/share/lib
		${SOIL2_LOCATION_HINT}/lib
	DOC
		"Location of the SOIL2 library."
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
	SOIL2
	"Could not find SOIL2"
	SOIL2_LIBRARIES SOIL2_INCLUDE_DIR
)

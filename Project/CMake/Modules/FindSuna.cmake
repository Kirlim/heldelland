# Tries to find Soil2 lib.
# Will also search with the following path variables:
#	SUNA_LOCATION_HINT
#
# Once colled, will define
# 	SUNA_FOUND
#	SUNA_INCLUDE_DIR
#	SUNA_LIBRARIES

find_path(
	SUNA_INCLUDE_DIR
		suna/SunaSettings.h
	PATHS
		/usr/include
		/usr/local/include
		/usr/share/include
		${SUNA_LOCATION_HINT}/include
	DOC
		"Location which Suna can be found."
)

find_library(
	SUNA_LIBRARIES
		SUNA
	NAMES
		Suna
	PATHS
		${SUNA_INCLUDE_DIR}/lib
		/usr/lib
		/usr/local/lib
		/usr/share/lib
		${SUNA_LOCATION_HINT}/lib
	DOC
		"Location of the SUNA library."
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
	SUNA
	"Could not find SUNA"
	SUNA_LIBRARIES SUNA_INCLUDE_DIR
)

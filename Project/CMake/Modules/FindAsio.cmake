# - Try to find Asio - Standalone (c11) version
# 
# Also searches on environment variable ASIO_LOCATION
#
# Will define:
# ASIO_FOUND - System has Asio Standalone
# ASIO_INCLUDE_DIRS - The Asio Standalone include directories
# ASIO_DEFINITIONS - Compiler switches for making Asio to be used as Standalone

set(ASIO_DEFINITIONS ASIO_STANDALONE)

find_path(
	ASIO_INCLUDE_DIR
	NAMES
		asio.hpp
	HINTS
		/usr/include
		/usr/local/include
		$ENV{ASIO_LOCATION}/include
	DOC
		"Asio Standalone - Capable path"
	)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ASIO DEFAULT_MSG ASIO_INCLUDE_DIR)

if(ASIO_FOUND)
	set(ASIO_INCLUDE_DIRS ${ASIO_INCLUDE_DIR})
endif()


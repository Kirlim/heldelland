#
#Copyright (c) 2012-2015 Computer Graphics Systems Group at the Hasso-Plattner-Institute, Germany. #
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT #NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND #NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, #DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, #OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#This source code was copied from cginternals (https://github.com/cginternals/cmake-init)

# ASSIMP_FOUND
# ASSIMP_INCLUDE_DIR
# ASSIMP_LIBRARY_RELEASE
# ASSIMP_LIBRARY_DEBUG
# ASSIMP_LIBRARIES
# ASSIMP_BINARY (win32 only)

include(FindPackageHandleStandardArgs)

find_path(ASSIMP_INCLUDE_DIR assimp/Importer.hpp

    PATHS
    $ENV{ASSIMP_DIR}
    $ENV{PROGRAMFILES}/Assimp
    /usr
    /usr/local
    /sw
    /opt/local

    PATH_SUFFIXES
    /include

    DOC "The directory where assimp/Importer.hpp etc. resides")

if(MSVC AND X64)
    set(ASSIMP_PF "64")
else()
    set(ASSIMP_PF "86")
endif()

find_library(ASSIMP_LIBRARY_RELEASE NAMES assimp
    
    HINTS
    ${ASSIMP_INCLUDE_DIR}/..
    
    PATHS
    $ENV{ASSIMP_DIR}
    /usr
    /usr/local
    /sw
    /opt/local

    PATH_SUFFIXES
    /lib
    /lib${ASSIMP_PF}
    /build/code
    /build-debug/code

    DOC "The Assimp library (release)")

find_library(ASSIMP_LIBRARY_DEBUG NAMES assimpd
	#added by Eric
	assimp-vc140-mt
    
    HINTS
    ${ASSIMP_INCLUDE_DIR}/..

    PATHS
    $ENV{ASSIMP_DIR}
    /usr
    /usr/local
    /sw
    /opt/local

    PATH_SUFFIXES
    /lib
    /lib${ASSIMP_PF}
    /build/code
    /build-debug/code

    DOC "The Assimp library (debug)")

set(ASSIMP_LIBRARIES "")
if(ASSIMP_LIBRARY_RELEASE AND ASSIMP_LIBRARY_DEBUG)
    set(ASSIMP_LIBRARIES 
        optimized   ${ASSIMP_LIBRARY_RELEASE}
        debug       ${ASSIMP_LIBRARY_DEBUG})
elseif(ASSIMP_LIBRARY_RELEASE)
    set(ASSIMP_LIBRARIES ${ASSIMP_LIBRARY_RELEASE})
elseif(ASSIMP_LIBRARY_DEBUG)
    set(ASSIMP_LIBRARIES ${ASSIMP_LIBRARY_DEBUG})
endif()

if(WIN32)

    find_file(ASSIMP_BINARY NAMES assimp.dll "assimp${ASSIMP_PF}.dll"

        HINTS
        ${ASSIMP_INCLUDE_DIR}/..
        
        PATHS
        $ENV{ASSIMP_DIR}

        PATH_SUFFIXES
        /bin
        /bin${ASSIMP_PF}

        DOC "The Assimp binary")

endif()

find_package_handle_standard_args(ASSIMP DEFAULT_MSG ASSIMP_LIBRARIES ASSIMP_INCLUDE_DIR)
mark_as_advanced(ASSIMP_FOUND ASSIMP_INCLUDE_DIR ASSIMP_LIBRARIES)

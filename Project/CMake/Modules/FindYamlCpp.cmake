# Attempts to find the Yaml-cpp lib
#
# If found, will define
#	YAMLCPP_FOUND
#	YAMLCPP_LIBRARY
#	YAMLCPP_INCLUDE_DIR

find_path(
	YAMLCPP_INCLUDE_DIR
		yaml-cpp/yaml.h
	PATHS
	/usr/include
	/usr/local/include
	${YAML_LOCATION_HINT}/include
)

find_library(
	YAMLCPP_LIBRARY
	NAMES
		libyaml-cpp.a
		yaml-cpp
		libyaml-cppmdd
	PATHS
		/usr/lib
		kusr/local/lib
		${YAML_LOCATION_HINT}/lib
)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(
	YAMLCPP
	DEFAULT_MSG
	YAMLCPP_INCLUDE_DIR
	YAMLCPP_LIBRARY
)
mark_as_advanced(YAMLCPP_INCLUDE_DIR YAMLCPP_LIBRARY)


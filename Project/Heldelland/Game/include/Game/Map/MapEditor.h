
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__MAP_EDITOR_H
#define GAME__MAP__MAP_EDITOR_H



namespace Game
{
	class MapData;



	class MapEditor final
	{
	public:
		explicit MapEditor(MapData& mapData);
		~MapEditor() = default;



	private:
		MapData& m_MapData;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<memory>



namespace Game
{
    // Forward declarations
    class Tile;
    class Tileset;
    enum class TileOrientation;



    // Implement this interface for classes that stores relevant map
    // data, in a format that can be used by the Map class to build and
    // manage a map.
    class IMapData
    {
    public:
        virtual ~IMapData() {};

        // Sets the dimensions of the map data. Expected to resize any data
        // storage to reflect the changes. If width or depth are zero, expected
        // behavior is that they'll be reset to one.
        virtual void SetDimensions(const unsigned int width, const unsigned int depth) = 0;
        virtual unsigned int GetWidth() const = 0;
        virtual unsigned int GetDepth() const = 0;


        // Sets the vertex at given coordinates (note: vertex are at the border of the
        // tiles) to the given height. Returns false if the coordinate or height are
        // out of range.
        virtual bool SetVertexHeight(const unsigned int vertexX, const unsigned int vertexZ, const int height) = 0;

        // Returns the vertex height in the @outHeight parameter.
        // Returns false if the coordinate is out of range.
        virtual bool GetVertexHeight(const unsigned int vertexX, const unsigned int vertexZ, int& outHeight) const = 0;


        // Sets the tile id for the tile at the given coordinate.
        //
        // Returns false if the coordinate is out of range.
        virtual bool SetTileId(const unsigned int tileX, const unsigned int tileZ, const unsigned int tileId) = 0;
        
        // Returns the tile id fo the tile at the given coordinate, in the @outTileId parameter.
        //
        // Returns false if the coordinate is out of range.
        virtual bool GetTileId(const unsigned int tileX, const unsigned int tileZ, unsigned int& outTileId) const = 0;


        // Sets the tile orientation at the givent coordinate.
        //
        // Returns false if the coordinate is out of range.
        virtual bool SetTileOrientation(const unsigned int tileX, const unsigned int tileZ, const TileOrientation orientation) = 0;

        // Returns in the @outOrientation the tile orientation of the tile at the given
        // coordinate.
        //
        // Returns false if the coordinate is out of range.
        virtual bool GetTileOrientation(const unsigned int tileX, const unsigned int tileZ, TileOrientation& outOrientation) const = 0;


        // Sets the tile information at the given coordinate.
        //
        // Returns false if the coordinate is out of range.
        virtual bool SetTile(const unsigned int tileX, const unsigned int tileZ, const Tile tile) = 0;

        // Returns the tile information set at the given coordinate, in the @outTile parameter.
        //
        // Returns false if the coordinate is out of range.
        virtual bool GetTile(const unsigned int tileX, const unsigned int tileZ, Tile& outTile) const = 0;


        // TODO : Need a generic type of resource manager for the tilesets, maybe?

        // Sets the tileset to be used by the map
        virtual void SetTileset(std::shared_ptr<Tileset> tileset) = 0;

        // Retuns the tileset to be used by the map
        virtual std::weak_ptr<Tileset> GetTileset() const = 0;
    };
}

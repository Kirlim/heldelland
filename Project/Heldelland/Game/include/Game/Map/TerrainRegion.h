
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__TERRAIN_REGION_H
#define GAME__MAP__TERRAIN_REGION_H

#include<memory>
#include<cmath> // Needed to build correctly Vector3 with sqrt



namespace Nest
{
	template <typename NumberType> class Vector2;
	template <typename NumberType> class Vector3;
}



namespace Game
{
	class Terrain;
	class IMapData;



	// A terrain mesh is divided into various submeshes, in order to
	// control the amount of vertices data per region.
	class TerrainRegion 
	{
		friend class TerrainRegionView;

		// Type aliases
		using VertexType = Nest::Vector3<float>;
		using NormalType = Nest::Vector3<float>;
		using UvType = Nest::Vector2<float>;
		using ColorType = Nest::Vector3<float>;
	
	
	public:
		TerrainRegion();
		TerrainRegion(const TerrainRegion&) = delete;
		TerrainRegion(TerrainRegion&&) = default;

		TerrainRegion& operator=(const TerrainRegion&) = delete;
		TerrainRegion& operator=(TerrainRegion&&) = delete;

		~TerrainRegion();

		void SetUp(
			const Terrain& terrain,
			const int startX, const int startY, const int endX, const int endY, 
			const IMapData& mapData
		);

		// Casts a raycast against the terrain. Will return false if the cast hits nothing, or
		// true if it hits. If there is a hit, the tile x and z (y in the tilemap coordinates) are
		// returned.
		bool RaycastToTerrain(
			const Nest::Vector3<float>& rayOrigin, const Nest::Vector3<float>& rayDirection,
			int& outTileX, int& outTileZ,
			Nest::Vector3<float>& outIntersectionPosition
		) const;

		// Gets the height of a position over the terrain. The position is assumed to be valid and thus
		// there are no error check here. The integer tile coordinate is used here to avoid duplicated
		// calculations
		float GetPositionHeight(const float x, const float z, const int tileX, const int tileZ) const;

		int GetTilesCount() const;


		// Internal enums
		enum class TileShape { None, Plain, SimpleSlope, DiamondSlope };



	protected:
		void BuildRegionMesh(const IMapData& mapData);
		void RefreshTileMesh(const int x, const int y, const IMapData& mapData);

		TileShape GetTileShape(const int x, const int y, const IMapData& mapData) const;

		void SetPlaneTile(const int x, const int y, const UvType& uvA, const UvType& uvB, const UvType& uvC, const UvType& uvD, const IMapData& mapData);
		void SetFoldedTile(const int x, const int y, const UvType& uvA, const UvType& uvB, const UvType& uvC, const UvType& uvD, const IMapData& mapData);
		bool IsAHorizontalCut(const int x, const int y, const IMapData& mapData) const;

		int GetTileFirstVertexIndex(const int x, const int y) const;

		void RefreshPlaneTileUvs(const int baseVertex, const UvType& uvA, const UvType& uvB, const UvType& uvC, const UvType& uvD);
		void RefreshFoldedTileUvs(const int baseVertex, const UvType& uvA, const UvType& uvB, const UvType& uvC, const UvType& uvD, bool horizontalCut);

		void CalculateNormals(const int baseVertex);
		void CalculateTriangles(const int baseVertex);


	private:
		const Terrain* m_Owner;

		std::unique_ptr<VertexType[]> m_Vertices;
		std::unique_ptr<NormalType[]> m_Normals;
		std::unique_ptr<UvType[]> m_Uv;
		std::unique_ptr<UvType[]> m_Uv1;
		std::unique_ptr<ColorType[]> m_Colors;
		std::unique_ptr<int[]> m_Triangles;

		int m_StartX;
		int m_StartY;
		int m_EndX;
		int m_EndY;
	};
}



#endif

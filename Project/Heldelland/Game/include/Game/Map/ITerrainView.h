//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__I_TERRAIN_VIEW_H
#define GAME__MAP__I_TERRAIN_VIEW_H

#include<vector>

#include<Nest/Vector2.h>



namespace Game
{
	class ITerrainView
	{
	public:
		virtual ~ITerrainView() = default;
		virtual void GetPositionsHeights(const std::vector<Nest::Vector2f>& positions, std::vector<float>& out_Heights) const = 0;
	};
}



#endif

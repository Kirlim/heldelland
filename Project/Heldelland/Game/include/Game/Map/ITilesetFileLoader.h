
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__I_TILESET_FILE_LOADER_H
#define GAME__MAP__I_TILESET_FILE_LOADER_H

#include<string>
#include<memory>



namespace Game
{
    // Forward declarations
    class Tileset;



    class ITilesetFileLoader
    {
    public:
        virtual ~ITilesetFileLoader() {};
        virtual std::unique_ptr<Tileset> Load(const std::string file) = 0;
    };
}


#endif

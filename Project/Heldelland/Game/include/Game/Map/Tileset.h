
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<string>
#include<vector>
#include<unordered_map>

#include"Nest/Vector2.h"



// Forward declarations
namespace Nest { class Material; }



namespace Game
{
    // Represents a tileset to be used by maps.
    // The material is allowed to be null - since the tileset material
    // can be changed anytime.
    class Tileset
    {
    public:
        // TODO : change parameter to const Nest::Material* material, when material becomes const correct.
        // Sets up a new tileset with the given data.
        // Expected Exceptions:
        //  LogicErrorException: @tilesCount differs from @tilesIds, @uvsStarts or @uvsEnds size,
        //                       or if there are repeated tiles id's in @tilesIds
        Tileset(
            const int tilesCount, const std::vector<unsigned int>& tilesIds,
            const std::vector<Nest::Vector2f>&  uvsStarts, const std::vector<Nest::Vector2f>& uvsEnds,
            Nest::Material* material
        );
        ~Tileset();

        // TODO : use something better than raw pointer, when available (like a resource handler)
        void SetMaterial(Nest::Material* material);
        Nest::Material* GetMaterial() const;

        // Updates a tile information.
        // Expected Exceptions:
        //  LogicErrorException: @tileId is not a registered tile id.
        void SetTileInformation(const unsigned int tileId, Nest::Vector2f uvStart, Nest::Vector2f uvEnd);

        // Retuns the information of the tile of the given @tileId identifier.
        // Expected Exceptions:
        //  LogicErrorException: @tileId is not a registered tile id.
        void GetTileInformation(const unsigned int tileId, Nest::Vector2f& outUvStart, Nest::Vector2f& outUvEnd) const;



    protected:
        struct TileInfo
        {
            Nest::Vector2f uvStart;
            Nest::Vector2f uvEnd;
        };

        std::unordered_map<unsigned int, TileInfo> m_Tiles;
        Nest::Material* m_Material = nullptr;
    };
}

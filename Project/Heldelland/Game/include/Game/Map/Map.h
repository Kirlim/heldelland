
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__MAP_H
#define GAME__MAP__MAP_H

#include<memory>



namespace Nest
{
	// Forward declarations
	class MeshesFactory;
	template <typename NumberType> class Vector3;
}



namespace Game
{
	// Forward declarations
	class Terrain;
	class IMapData;



	class Map
	{
		static struct Passkey {} const passkey;
		friend class MapsFactory;

	public:
		// TODO : use map data changed events
		// If the terrain data changes, the terrain will need to be manually rebuilt.
		Map(std::unique_ptr<IMapData>& mapData, const Nest::MeshesFactory& meshesFactory, Passkey const&);
		Map(const Map&) = delete;
		Map(Map&&) = default;

		Map& operator=(const Map&) = delete;
		Map& operator=(Map&&) = delete;

		~Map();

		IMapData* GetMapData() const;
		Terrain* GetTerrain() const;

		// Casts a raycast against the terrain. Will return false if the cast hits nothing, or
		// true if it hits. If there is a hit, the tile x and z (y in the tilemap coordinates) are
		// returned. Ray direction is assumed to be normalized.
		bool RaycastToTerrain(
			const Nest::Vector3<float>& rayOrigin, const Nest::Vector3<float>& rayDirection,
			int& outTileX, int& outTileZ
		) const;



	private:
		std::unique_ptr<IMapData> m_MapData;
		std::unique_ptr<Terrain> m_Terrain;
	};
}



#endif

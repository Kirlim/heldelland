
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__MAP_VIEW_FACTORY_H
#define GAME__MAP__MAP_VIEW_FACTORY_H

#include<memory>

#include"Game/Map/IMapViewFactory.h"



namespace Nest
{
	class EntitiesManager;
	class MeshesFactory;
}

namespace Game
{
	class Map;
	class MapView;



	class MapViewFactory final : public IMapViewFactory
	{
	public:
		MapViewFactory(Nest::EntitiesManager& entitiesManager, const Nest::MeshesFactory& meshesFactory);

		std::unique_ptr<MapView> Instantiate(Map& map) const override;



	private:
		Nest::EntitiesManager& m_EntitiesManager;
		const Nest::MeshesFactory& m_MeshesFactory;
	};
}



#endif

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__MAP_VIEW_H
#define GAME__MAP__MAP_VIEW_H

#include<memory>



namespace Nest
{
	class EntitiesManager;
	class MeshesFactory;
}

namespace Game
{
	class Map;
	class TerrainView;
	class ITerrainView;



	class MapView final
	{
	public:
		MapView(Map& map, Nest::EntitiesManager& entitiesManager, const Nest::MeshesFactory& meshesFactory);
		MapView(const MapView&) = delete;
		MapView(MapView&&) = default;

		MapView& operator=(const MapView&) = delete;
		MapView& operator=(MapView&&) = delete;

		~MapView();

		void InstantiateHierarchyOfEntities() const;

		const ITerrainView* GetTerrainView() const;



	private:
		Map& m_Map;
		Nest::EntitiesManager& m_EntitiesManager;

		std::unique_ptr<TerrainView> m_TerrainView;
	};
}



#endif

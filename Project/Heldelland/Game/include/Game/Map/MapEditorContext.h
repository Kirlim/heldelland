
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__MAP_EDITOR_CONTEXT_H
#define GAME__MAP__MAP_EDITOR_CONTEXT_H

#include<memory>
#include<functional>



namespace Game
{
	class MapEditor;



	class MapEditorContext
	{
	public:
		explicit MapEditorContext(const std::function<std::unique_ptr<MapEditor>()> mapEditorFactoryFunction);
		~MapEditorContext() = default;



	private:
		const std::function<std::unique_ptr<MapEditor>()> m_MapEditorFactoryFunction;
		std::unique_ptr<MapEditor> m_MapEditor;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__MAPS_FACTORY_H
#define GAME__MAP__MAPS_FACTORY_H

#include<memory>



namespace Nest
{
	class MeshesFactory;
}

namespace Game
{
	class IMapData;
	class Map;



	class MapsFactory
	{
	public:
		explicit MapsFactory(const Nest::MeshesFactory& meshesFactory);
		~MapsFactory() = default;

		std::unique_ptr<Map> Instantiate(std::unique_ptr<IMapData>& mapData) const;



	private:
		const Nest::MeshesFactory& m_MeshesFactory;
	};
}

#endif

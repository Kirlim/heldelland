
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__TERRAIN_REGION_VIEW_H
#define GAME__MAP__TERRAIN_REGION_VIEW_H

#include<memory>



namespace Nest
{
	template <typename ComponentType> class ComponentReference;
	class EntitiesManager;
	class Mesh;
	class MeshesFactory;
	class Transform;
}

namespace Game
{
	class TerrainRegion;
	class Tileset;


	class TerrainRegionView final
	{
	public:
		TerrainRegionView(
			const TerrainRegion& terrainRegion, 
			Nest::EntitiesManager& entitiesManager,
			const Nest::MeshesFactory& meshesFactory
		);

		void InstantiateHierarchyOfEntities(
			const Nest::ComponentReference<Nest::Transform>& terrainTransform,
			const Tileset& tileset
		) const;



	private:
		void BuildTerrainRegionMesh();



	private:
		const TerrainRegion& m_ReferencedTerrainRegion;
		Nest::EntitiesManager& m_EntitiesManager;
		const Nest::MeshesFactory& m_MeshesFactory;

		std::unique_ptr<Nest::Mesh> m_TerrainRegionMesh;
	};
}

#endif

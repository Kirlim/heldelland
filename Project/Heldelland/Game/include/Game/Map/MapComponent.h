
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<memory>

#include<Nest/Component.h>



namespace Game
{
    // Forward declarations
    class Map;



    // The map component is a way to easily detect an entity that
    // owns a Map instance. This component has no system.
    class MapComponent : public Nest::Component
    {
    public:
        MapComponent();
	MapComponent(const MapComponent&) = delete;
	MapComponent(MapComponent&&) = default;

        ~MapComponent() override;

	MapComponent& operator =(const MapComponent&) = delete;
	MapComponent& operator =(MapComponent&&) = default;


        void SetMap(Map* map);
        Map* GetMap() const;

        static std::string GetTypeName() { return("MapComponent"); }



    protected:
        virtual void OnDestroy() override;



    protected:
        Map* m_Map;
    };
}

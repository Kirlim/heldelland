
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__YAML_TILESET_FILE_LOADER_H
#define GAME__MAP__YAML_TILESET_FILE_LOADER_H

#include<memory>

#include"Game/Map/ITilesetFileLoader.h"



namespace YAML
{
    // Forward declarations
    class Node;
}



namespace Nest
{
    // Forward declarations
    class IResourcesManager;
}



namespace Game
{
    // Forward declarations
    class Tileset;

    // YAML file structure:
    // Material: "material name"
    // TileSize: [size of a tile in pixels]
    // TilePixelRepeatSize: [size, in pixels, of extra pixels arond the tile (a border) to avoid artifacts]
    // TextureSize:
    //    X: [texture horizontal size]
    //    Y: [texture vertical size]
    // Tileset:
    //    - Id : [identifier of the tile]
    //      xPos: [x coordinate of the tile in the tileset, in tiles coordinate system]
    //      yPos: [y coordinate of the tile in the tileset, in tiles coordinate system]



    class YamlTilesetFileLoader : public ITilesetFileLoader
    {
    public:
        explicit YamlTilesetFileLoader(Nest::IResourcesManager& resourcesManager);
        virtual ~YamlTilesetFileLoader();

        // Expected Exeptions:
        //  BadFileException:
        //      - File could not be read or found
        //  InvalidFileStructuceException:
        //      - YAML is ill formatted
        //      - One or more needed keys may be missing
        //  LogicErrorException:
        //      - The referenced material does not exists in the resources manager
        //  InvalidDataException:
        //      - Tile size is not higher than 0
        //      - Texture Width is not higher than 0
        //      - Texture Height is not higher than 0
        //      - Tile Id, xPos or yPos is negative
        //
        //  Cascaded: exceptions thrown by Tileset constructor
        virtual std::unique_ptr<Tileset> Load(const std::string file) override final;



    protected:
        YAML::Node LoadYamlFile(const std::string& file) const;



    protected:
        Nest::IResourcesManager& m_ResourcesManager;
    };
}



#endif

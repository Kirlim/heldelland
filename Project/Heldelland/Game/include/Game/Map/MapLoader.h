
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__MAP_LOADER_H
#define GAME__MAP__MAP_LOADER_H

#include<memory>



namespace Game
{
    // Forward declarations
    class IMapData;



    class MapLoader
    {
    public:
        // Used for testing only. Will be removed when better map data saving and loading
        // becomes available.
		std::unique_ptr<IMapData> MakeTestMap() const;
    };
}



#endif

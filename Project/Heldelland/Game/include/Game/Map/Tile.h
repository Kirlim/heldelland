
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once



namespace Game
{
    enum class TileOrientation { North, West, South, East };
    
    
    
    class Tile
    {
    public:
        void SetTile(const unsigned int tile);
        unsigned int GetTile() const;
        
        void SetOrientation(const TileOrientation orientation);
        TileOrientation GetOrientation() const;
        
        
        
    protected:
        unsigned int m_Tile;
        TileOrientation m_Orientation;
    };
}
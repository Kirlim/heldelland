
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__I_MAP_VIEW_FACTORY_H
#define GAME__MAP__I_MAP_VIEW_FACTORY_H

#include<memory>



namespace Game
{
	class Map;
	class MapView;



	class IMapViewFactory
	{
	public:
		virtual ~IMapViewFactory() = default;
		virtual std::unique_ptr<MapView> Instantiate(Map& map) const = 0;
	};
}



#endif

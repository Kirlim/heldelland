//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__TERRAIN_VIEW_H
#define GAME__MAP__TERRAIN_VIEW_H

#include<memory>

#include"Game/Map/ITerrainView.h"



namespace Nest
{
	template <typename ComponentType>
	class ComponentReference;

	class EntitiesManager;
	class MeshesFactory;
	class Transform;
}

namespace Game
{
	class Terrain;
	class TerrainRegionView;
	class Tileset;



	class TerrainView final : public ITerrainView
	{
	public:
		TerrainView(const Terrain& terrain, Nest::EntitiesManager& entitiesManager, const Nest::MeshesFactory& meshesFactory);
		TerrainView(const TerrainView&) = delete;
		TerrainView(TerrainView&&) = delete;

		TerrainView& operator=(const TerrainView&) = delete;
		TerrainView& operator=(TerrainView&&) = delete;

		~TerrainView();

		void InstantiateHierarchyOfEntities(
			const Nest::ComponentReference<Nest::Transform>& mapTransform,
			const Tileset& tileset
		) const;

		float GetPositionHeight(float x, float z) const;
		void GetPositionsHeights(const std::vector<Nest::Vector2f>& positions, std::vector<float>& out_Heights) const override;



	private:
		const Terrain& m_Terrain;
		Nest::EntitiesManager& m_EntitiesManager;

		std::vector<std::unique_ptr<TerrainRegionView>> m_TerrainRegionViews;
	};
}

#endif

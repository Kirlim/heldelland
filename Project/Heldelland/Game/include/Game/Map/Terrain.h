//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__TERRAIN_H
#define GAME__MAP__TERRAIN_H

#include<memory>
#include<functional>

#include<Nest/Vector2.h>



namespace Nest
{
	template <typename NumberType>
	class Vector2;
	template <typename NumberType>
	class Vector3;
}



namespace Game
{
	class IMapData;
	class TerrainRegion;
	class Tile;

	enum class TileOrientation;




	// Stores ground data and meshes for a map. Will also trigger
	// the creation of meshes for terrain rendering.
	class Terrain final
	{
		friend class TerrainRegion;

	public:
		// The terrain heightmap will be copied.
		explicit Terrain(const IMapData& mapData);
		Terrain(const Terrain&) = delete;
		Terrain(Terrain&&) = default;

		Terrain& operator=(const Terrain&) = delete;
		Terrain& operator=(Terrain&&) = delete;

		~Terrain();

		// Returns the terrain width, in tiles.
		int GetWidth() const;

		// Returns the terrain height, in tiles.
		int GetHeight() const;

		float GetPositionHeight(float x, float z) const;
		void GetPositionsHeights(const std::vector<Nest::Vector2f>& positions, std::vector<float>& out_Heights) const;

		TerrainRegion* GetRegions() const;
		int GetRegionsCount() const;

		// Casts a raycast against the terrain. Will return false if the cast hits nothing, or
		// true if it hits. If there is a hit, the tile x and z (y in the tilemap coordinates) are
		// returned.
		bool RaycastToTerrain(
			const Nest::Vector3<float>& rayOrigin, const Nest::Vector3<float>& rayDirection,
			int& outTileX, int& outTileZ
		) const;

		void ForEachTerrainRegion(const std::function<void(const TerrainRegion&)>& action) const;


	private:
		void BuildRegions(const IMapData& mapData);

		// TODO : RotateUv method certainly does not belong in this class...
		static void RotateUv(
			const Nest::Vector2<float>& uvStart, const Nest::Vector2<float>& uvEnd,
			TileOrientation tileOrientation,
			Nest::Vector2<float>& outUvA, Nest::Vector2<float>& outUvB,
			Nest::Vector2<float>& outUvC, Nest::Vector2<float>& outUvD
		);



	public:
		const float TileSize = 1.0f;
		const int RegionSizeWidth = 20;
		const int RegionSizeHeigth = 20;


	private:
		int m_TilesWidth;
		int m_TilesHeight;

		float m_TerrainStartPosX, m_TerrainStartPosZ;
		float m_TerrainEndPosX, m_TerrainEndPosZ;

		int m_RegionsCount;
		std::unique_ptr<TerrainRegion[]> m_Regions;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__MAP__MAP_DATA_H
#define GAME__MAP__MAP_DATA_H

#include<memory>
#include<vector>

#include"Game/Map/IMapData.h"
#include"Game/Map/Tile.h"



namespace Game
{
	// Forward declarations
	class Tileset;



	class MapData : public IMapData
	{
	public:
		virtual ~MapData() = default;

		void SetDimensions(const unsigned int width, const unsigned int depth) override;
		unsigned int GetWidth() const override;
		unsigned int GetDepth() const override;
		bool SetVertexHeight(const unsigned int vertexX, const unsigned int vertexZ, const int height) override;
		bool GetVertexHeight(const unsigned int vertexX, const unsigned int vertexZ, int & outHeight) const override;
		bool SetTileId(const unsigned int tileX, const unsigned int tileZ, const unsigned int tileId) override;
		bool GetTileId(const unsigned int tileX, const unsigned int tileZ, unsigned int & outTileId) const override;
		bool SetTileOrientation(const unsigned int tileX, const unsigned int tileZ, const TileOrientation orientation) override;
		bool GetTileOrientation(const unsigned int tileX, const unsigned int tileZ, TileOrientation & outOrientation) const override;
		bool SetTile(const unsigned int tileX, const unsigned int tileZ, const Tile tile) override;
		bool GetTile(const unsigned int tileX, const unsigned int tileZ, Tile & outTile) const override;
		void SetTileset(std::shared_ptr<Tileset> tileset) override;
		std::weak_ptr<Tileset> GetTileset() const override;



	protected:
		void ResizeToDepth(const unsigned int depth);
		void ResizeToWidth(const unsigned int width);

		void TrimMapMatrices();
		void TrimTileMatrix();
		void TrimHeightsMatrix();



	protected:
		unsigned int m_Width = 0;
		unsigned int m_Depth = 0;

		std::shared_ptr<Tileset> m_Tileset;

		std::vector<std::vector<Tile>> m_Tiles;
		std::vector<std::vector<int>> m_Heights;
	};
}

#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__WORLDS_FACTORY_H
#define GAME__WORLDS_FACTORY_H

#include<memory>



namespace Nest
{
	class World;
}



namespace Game
{
	class WorldFactory
	{
	public:
		std::unique_ptr<Nest::World> InstantiateWorld() const;
	};
}

#endif

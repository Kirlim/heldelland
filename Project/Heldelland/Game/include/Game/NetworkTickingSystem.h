
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK_TICKING_SYSTEM_H
#define GAME__NETWORK_TICKING_SYSTEM_H

#include<Nest/System.h>



namespace Game
{
	// Forward declarations
	class IHost;
	class IHostChannel;



	class NetworkTickingSystem : public Nest::System
	{
	public:
		NetworkTickingSystem();
		virtual ~NetworkTickingSystem();



	private:
		void DoFixedUpdate(float fixedDeltaTime) override;
	};
}



#endif

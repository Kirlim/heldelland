//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__CAMERA_CONTROL_INPUT_OBSERVER_H
#define GAME__CAMERA_CONTROL_INPUT_OBSERVER_H

#include<memory>
#include<functional>



namespace Nest
{
	class EventsBusToken;
	struct InputEvent;

	template <typename T>
	class EventsBus;
}

namespace Game
{
	class CameraControlInputObserver
	{
	public:
		explicit CameraControlInputObserver(Nest::EventsBus<Nest::InputEvent&>& inputEventsBus);

		void SetYawRotationCallback(std::function<void(int /*YawRotation*/)> yawRotationCallback);
		void SetZoomCallback(std::function<void(int /*scrollDeltaY*/)> zoomCallback);



	private:
		void HandleInputEvent(const Nest::InputEvent& inputEvent);



	private:
		std::unique_ptr<Nest::EventsBusToken> m_InputEventListenerToken;

		std::function<void(int /*YawRotation*/)> m_YawRotationCallback;
		std::function<void(int /*scrollDeltaY*/)> m_ZoomCallback;

		bool m_RightMouseButtonPressed = false;
	};
}



#endif

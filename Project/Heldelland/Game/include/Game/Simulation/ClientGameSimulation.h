//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__CLIENT_GAME_SIMULATION_H
#define GAME__SIMULATION__CLIENT_GAME_SIMULATION_H

#include<memory>

#include<Nest/Vector2.h>
#include<Nest/Events/EventsBus.h>

#include"Game/SimulationCommons/SimulationTypes.h"
#include"Game/Simulation/IGameClientSimulation.h"
#include"Game/SimulationCommons/SimulationTickValue.h"
#include"Game/SimulationCommons/ActorsTypes.h"



namespace Nest
{
	class World;
}

namespace Game
{
	// Forward declarations
	enum class GameSimulationUpdateType;
	class ActorsManager;
	class ISimActorsSpawner;
	class Map;
	class AStar;
	class SimulationActorMovementSystem;

	class PlayerSimulationCommandsQueuesManager;
	class ServerSimulationCommandsQueue;



	// Responsibility:
	//    Handle "client side" game simulation logic (even if the server is being handled
	//    in the same application instance).
	class ClientGameSimulation : public IGameClientSimulation
	{
	public:
		ClientGameSimulation(
			const Nest::World& simulationWorld,
			std::unique_ptr<ActorsManager> actorsManager,
			std::unique_ptr<ISimActorsSpawner> simActorsSpawner,
			std::unique_ptr<Map> map,
			std::unique_ptr<AStar> pathfinding, // TODO : pathfinding here seem to be unused. Verify and remove if true
			std::unique_ptr<SimulationActorMovementSystem> simulationActorMovementSystem,
			simfix64 tickDeltaTime,
			float maxDelayBeforeSpeedUp,
			float timeSpeedUpFactor
		);
		~ClientGameSimulation();

		ActorsManager* GetActorsManager() const override;
		Map* GetMap() const override;
		AStar* GetPathfinder() const;
		SimulationActorMovementSystem* GetSimulationActorMovementSystem() const override;
		PlayerSimulationCommandsQueuesManager* GetQueuesManager() const override;
		ServerSimulationCommandsQueue* GetToServerSimulationCommandsQueue() const override;
		simfix64 GetTickDeltaTime() const;

		const Nest::World& GetWorld() const;

		void ForceNextTickValue(unsigned int nextTickValue) override;
		unsigned int GetCurrentTickValue() const;
		float GetCurrentTickElapsedTimePercentage() const override;

		void Update(float deltaTime) override;
		float GetSimulationTicksTimeOnHold() const;


		void SetPlayerActorNetworkId(ActorNetworkId actorNetworkId) noexcept override;
		ActorNetworkId GetPlayerActorNetworkId() const noexcept override;


		// Actions on simulations
		ActorId SpawnActor(
			ActorTypeId typeId, 
			Nest::Vector2<simfix64> position, 
			bool hasNetworkId, 
			ActorNetworkId networkId
		) const override;


		std::unique_ptr<Nest::EventsBusToken> RegisterToActorSpawnedEvent(std::function<void(const ActorSpawnedEvt&)> listener) override;



	private:
		void ExecuteSimulationTick();



	private:
		const Nest::World& m_SimulationWorld;

		std::unique_ptr<ActorsManager> m_ActorsManager;
		std::unique_ptr<ISimActorsSpawner> m_SimActorsSpawner;
		std::unique_ptr<Map> m_Map;
		std::unique_ptr<AStar> m_Pathfinding;
		std::unique_ptr<SimulationActorMovementSystem> m_SimulationActorMovementSystem;

		const simfix64 m_TickDeltaTime;
		const float m_MaxDelayBeforeSpeedUp;
		const float m_TimeSpeedUpFactor;
		simfix64 m_AccumulatedTime{ 0.0f };
		float m_SimulationTickElapsedPct{ 0.0f };

		std::unique_ptr<PlayerSimulationCommandsQueuesManager> m_PlayerSimulationCommandsQueues;
		std::unique_ptr<ServerSimulationCommandsQueue> m_ServerSimulationCommandsQueue; // Commands that we want to send to the server

		SimulationTickValue m_SimulationTickValue;

		ActorNetworkId m_PlayerActorNetworkId{ 0 };

		Nest::EventsBus<ActorSpawnedEvt> m_OnActorSpawnedEventsBus;
	};
}

#endif

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__GAME_CLIENT_SIMULATION_NETWORK_SYSTEM_H
#define GAME__SIMULATION__GAME_CLIENT_SIMULATION_NETWORK_SYSTEM_H

#include<functional>
#include<memory>

#include<Nest/System.h>
#include<Nest/Events/EventsBus.h>

#include"Game/Network/HostId.h"
#include"Game/Network/NetworkMessage.h"



namespace Game
{
	class GameClientSimulationSystem;
	class IClientNetworkController;
	class IGameClientSimulation;
	class IHost;
	class IHostChannel;
	class INetworkMessagesFactory;
	class INetworkMessagesHandlersManager;
	class INetworkSystem;

	class ClientSimulationCommand;

	class GameSimulationSettingsMessage;
	class JoinGameReplyMessage;
	class ForceNextTickValueCommand;
	class SimulationTickEndCommand;



	struct NewClientSimulationStartedEvt
	{
		IGameClientSimulation* gameClientSimulation;
	};



	class GameClientSimulationNetworkSystem : public Nest::System
	{
	public:
		GameClientSimulationNetworkSystem(
			INetworkSystem& networkSystem,
			GameClientSimulationSystem& gameClientSimulationSystem,
			std::function<std::unique_ptr<IGameClientSimulation>(std::string /*mapName*/)> gameClientSimulationFactoryMethod,
			const INetworkMessagesFactory& networkMessagesFactory,
			const INetworkMessagesHandlersManager& networkMessagesHandlersManager
		);
		~GameClientSimulationNetworkSystem();


		std::unique_ptr<Nest::EventsBusToken> RegisterToNewClientSimulationStartedEvent(
			std::function<void(const NewClientSimulationStartedEvt&)> action
		);



	private:
		void RegisterIncomingMessagesHandlers();
		void SetGenericClientSimulationCommandToken(
			std::unique_ptr<Nest::EventsBusToken>& token, 
			NetworkMessageTypeId networkMessageTypeId
		);

		void DoFixedUpdate(float) override;
		void TickClientNetworkController();
		void AddCommandsFromClientSimulationToServerChannel() const;

		void HandleIncomingConnectionRequestReply(JoinGameReplyMessage& joinGameReplyMessage);

		void HandleIncomingGameSimulationSettingsMessage(const GameSimulationSettingsMessage& gameSimulationSettingsMessage) const;
		void StartNewGameClientSimulation(std::string mapName) const;
		void SignalNewClientSimulationStartedEvent() const;

		void DispatchGameSimulationLoadedMessage(const IHost* host) const;
		std::shared_ptr<NetworkMessage> CreateGameSimulationLoadedNetworkMessage() const;

		void HandleIncomingForceNextTickValueCommand(ForceNextTickValueCommand& command) const;

		void HandleIncomingSimulationTickEndCommand(std::shared_ptr<SimulationTickEndCommand> command) const;

		void HandleIncomingClientSimulationCommand(std::shared_ptr<ClientSimulationCommand> command) const;

		IHostChannel* GetSendToServerChannel() const;
		void SendNetworkMessageToServerChannel(std::shared_ptr<NetworkMessage>& message) const;



	private:
		INetworkSystem& m_NetworkSystem;
		GameClientSimulationSystem& m_GameClientSimulationSystem;

		const std::function<std::unique_ptr<IGameClientSimulation>(std::string /*mapName*/)> m_GameClientSimulationFactoryMethod;
		const INetworkMessagesFactory& m_NetworkMessagesFactory;
		const INetworkMessagesHandlersManager& m_NetworkMessagesHandlersManager;

		HostId m_ServerHostId{ 0 };
		bool m_HasServerId = false;

		Nest::EventsBus<const NewClientSimulationStartedEvt&> m_NewClientSimulationStartedEventBus;

		std::unique_ptr<Nest::EventsBusToken> m_JoinGameReplyMessagesToken;
		std::unique_ptr<Nest::EventsBusToken> m_GameSimulationSettingsMessageToken;
		std::unique_ptr<Nest::EventsBusToken> m_ForceNextTickValueCommandsToken;
		std::unique_ptr<Nest::EventsBusToken> m_SimulationTickStartCommandsToken;
		std::unique_ptr<Nest::EventsBusToken> m_SimulationTickEndCommandsToken;

		std::unique_ptr<Nest::EventsBusToken> m_SpawnActorCommandsToken;
		std::unique_ptr<Nest::EventsBusToken> m_SpawnPlayerCharacterCommandsToken;
		std::unique_ptr<Nest::EventsBusToken> m_SetActorFullPathCommandsToken;
		std::unique_ptr<Nest::EventsBusToken> m_SetActorMovementSpeedCommandsToken;
		std::unique_ptr<Nest::EventsBusToken> m_SetActorPositionCommandsToken;

		std::unique_ptr<IClientNetworkController> m_ClientNetworkController;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__TYPES__PLAYER_SIM_INFO_ID_H
#define GAME__SIMULATION__TYPES__PLAYER_SIM_INFO_ID_H

#include<Nest/IdType.h>



namespace Game
{
	struct PlayerSimInfoIdTraits
	{
		using underlying_type = unsigned int;
	};

	using PlayerSimInfoId = Nest::IdType<PlayerSimInfoIdTraits>;
}



#endif

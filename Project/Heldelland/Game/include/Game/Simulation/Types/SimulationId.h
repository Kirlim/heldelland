
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__TYPES__SIMULATION_ID
#define GAME__SIMULATION__TYPES__SIMULATION_ID

#include"Nest/IdType.h"

namespace Game
{
	// TODO : does not seem to be used. Delete this file, if it wont be needed.
	struct SimulationIdTag {};
	using SimulationId = Nest::IdType<SimulationIdTag, unsigned int>;
}

#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__GAME_CLIENT_SIMULATION_SYSTEM_H
#define GAME__SIMULATION__GAME_CLIENT_SIMULATION_SYSTEM_H

#include<memory>

#include<Nest/System.h>



namespace Game
{
	class IGameClientSimulation;



	class GameClientSimulationSystem : public Nest::System
	{
	public:
		~GameClientSimulationSystem() override;
		void StartClientSimulation(std::unique_ptr<IGameClientSimulation>& gameClientSimulation);

		IGameClientSimulation* GetClientSimulation() const;



	private:
		void DoUpdate(float deltaTime) override;



	private:
		std::unique_ptr<IGameClientSimulation> m_GameClientSimulation;
	};
}



#endif

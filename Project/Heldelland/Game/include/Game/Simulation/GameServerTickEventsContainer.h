//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__GAME_SERVER_TICK_EVENTS_CONTAINER_H
#define GAME__SIMULATION__GAME_SERVER_TICK_EVENTS_CONTAINER_H

#include<functional>
#include<vector>
#include<memory>

#include"Game/SimulationCommons/SimulationTypes.h"



namespace Game
{
	class Path;



	class GameServerTickEventsContainer
	{
	public:
		using NewActorMovementSpeedEventsContainer = std::vector<std::tuple<ActorNetworkId, simfix64>>;
		using NewActorMovementSpeedEventsIterator = NewActorMovementSpeedEventsContainer::iterator;

		using NewActorPathEventsContainer = std::vector<std::tuple<ActorNetworkId, std::shared_ptr<Path>>>;
		using NewActorPathEventsIterator = NewActorPathEventsContainer::iterator;



	public:
		~GameServerTickEventsContainer();

		void RegisterNewActorMovementSpeedEvent(ActorNetworkId actorNetworkId, simfix64 movementSpeed);
		void RegisterNewActorPathEvent(ActorNetworkId actorNetworkId, std::shared_ptr<Path> path);

		int GetNewActorMovementSpeedEventsCount() const;
		int GetNewActorPathEventsCount() const;
		void ClearAllRegisteredEvents();

		void RunForNewActorMovementSpeedEvents(
			std::function<void(NewActorMovementSpeedEventsIterator, NewActorMovementSpeedEventsIterator)> action
		);
		void RunForNewActorPathEvents(
			std::function<void(NewActorPathEventsIterator, NewActorPathEventsIterator)> action
		);



	private:
		NewActorMovementSpeedEventsContainer m_NewActorMovementSpeedEvents;
		NewActorPathEventsContainer m_NewActorPathEvents;
	};
}



#endif

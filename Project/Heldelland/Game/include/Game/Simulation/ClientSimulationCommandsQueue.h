
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__CLIENT_SIMULATION_COMMAND_QUEUE_H
#define GAME__SIMULATION__CLIENT_SIMULATION_COMMAND_QUEUE_H

#include<memory>
#include<queue>



namespace Game
{
    // Forward declarations
    class ClientSimulationCommand;



    class ClientSimulationCommandsQueue
    {
    public:
        void Enqueue(std::shared_ptr<ClientSimulationCommand> command);
        std::shared_ptr<ClientSimulationCommand> Dequeue();

        // Set the status of this queue to ready or not ready for execution.
        void SetReadyStatus(bool readyStatus);

        // Returns if this queue of commands is ready for execution, meaning
        // that all commands have been enqueued and now the simulation must
        // execute them in order.
        bool IsReady() const;

        void Clear();

        unsigned int Size() const;

        bool HasCommands() const;



    protected:
        std::queue<std::shared_ptr<ClientSimulationCommand>> m_Queue;
        bool m_Ready = false;
    };
}



#endif

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__SIM_ACTOR_MOVEMENT_BEHAVIOR_H
#define GAME__SIMULATION__SIM_ACTOR_MOVEMENT_BEHAVIOR_H

#include<memory>

#include<Nest/Vector3.h>
#include<Nest/Component.h>

#include"Game/SimulationCommons/SimulationTypes.h"



namespace Game
{
	// Forward declarations
	class Path;



	class SimActorMovementBehavior : public Nest::Component
	{
	public:
		SimActorMovementBehavior();
		SimActorMovementBehavior(const SimActorMovementBehavior&) = delete;
		SimActorMovementBehavior(SimActorMovementBehavior&&) = default;

		virtual ~SimActorMovementBehavior();

		SimActorMovementBehavior& operator=(const SimActorMovementBehavior&) = delete;
		SimActorMovementBehavior& operator=(SimActorMovementBehavior&&) = default;


		static std::string GetTypeName() { return ("SimActorMovementBehavior"); }

		// Defines a new position for this actor. Using this method will also remove any path
		// that the actor was following.
		void ResetPosition(Nest::Vector3<simfix64> position);

		// Defines a new movement order to this behavior, by setting its path. It will immediately
		// jump to the "distance" factor passed by parameter.
		void MoveInPath(std::shared_ptr<Path> path, simfix64 startingDistance);

		// Sets the natural movement speed of the actor, in units per second
		void SetMovementSpeed(simfix64 movementSpeed);

		// Retrieves the natural movement speed of the actor, in units per second
		simfix64 GetMovementSpeed() const;

		// Retrieves the fixed point form of the actor position
		Nest::Vector3<simfix64> GetPosition() const;

		// Moves the actor within the current set path, if any
		void Update(simfix64 deltaTime);

		void SetActorId(ActorId actorId);
		ActorId GetActorId() const;



	protected:
		void MoveToDistance(simfix64 distance);



	protected:
		Nest::Vector3<simfix64> m_Position = Nest::Vector3<simfix64>(simfix64(0), simfix64(0), simfix64(0));
		simfix64 m_MovementSpeed = simfix64(0);

		ActorId m_ActorId;

		std::shared_ptr<Path> m_Path; // Using shared ptr since theres no "move" on components yet.
		int m_NextPathNodeIndex = 0;

		// These variables are used when navigating a path
		bool m_IsMoving = false;
		simfix64 m_CurrentDistance;
	};
}



#endif

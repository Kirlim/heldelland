
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__COMMAND__SERVER_SIMULATION_COMMAND_H
#define GAME__SIMULATION__COMMAND__SERVER_SIMULATION_COMMAND_H



namespace Game
{
    // Forward declarations
    class ServerGameSimulation;



    // Base class for game simulation commands.
    class ServerSimulationCommand
    {
    public:
        ServerSimulationCommand();
        virtual ~ServerSimulationCommand();

        virtual void Execute(ServerGameSimulation& simulation) = 0;
    };
}



#endif

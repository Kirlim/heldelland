//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__SERVER_GAME_SIMULATION_H
#define GAME__SIMULATION__SERVER_GAME_SIMULATION_H

#include<memory>
#include<vector>
#include<functional>
#include<unordered_map>
#include<mutex>
#include<stack>

#include<Nest/Vector2.h>

#include"Game/SimulationCommons/ActorsTypes.h"
#include"Game/SimulationCommons/SimulationTickValue.h"
#include"Game/Simulation/Types/PlayerSimInfoId.h"
#include"Game/SimulationCommons/SimulationTypes.h"
#include"Game/Simulation/IGameServerSimulation.h"



namespace Nest
{
	template <typename T>
	class ITemplatedSingleComponentsContainer;
}

namespace Game
{
	class ActorsManager;
	class ISimActorsSpawner;
	class Map;
	class AStar;

	class PlayersSimulationsManager;
	class PlayerSimulationCommandsQueuesManager;
	class ClientSimulationCommand;
	class PlayerSimulationInformation;

	class ServerSimulationCommandsQueue;
	class ServerSimulationCommand;

	class SimActorMovementBehavior;
	class SimulationActorMovementSystem;

	class GameServerTickEventsContainer;



	// Responsibility:
	//    Handle the game simulation login of the server side, even if
	//    the client simulation is in this same application instance.
	class ServerGameSimulation : public IGameServerSimulation
	{
	public:
		ServerGameSimulation(
			std::unique_ptr<ActorsManager> actorsManager,
			std::unique_ptr<ISimActorsSpawner> simulationActorsSpawner,
			std::unique_ptr<Map> map,
			std::unique_ptr<AStar> pathfinding,
			Nest::ITemplatedSingleComponentsContainer<SimActorMovementBehavior>& simActorMovementBehaviorsContainer,
			std::unique_ptr<SimulationActorMovementSystem> simulationActorMovementSystem
		);
		~ServerGameSimulation();

		// Main getters
		ActorsManager* GetActorsManager() const;
		Map* GetMap() const;
		AStar* GetPathfinder() const;
		PlayersSimulationsManager* GetPlayersSimulationsManager() const override;
		SimulationActorMovementSystem* GetSimulationActorMovementSystem() const override;

		// Simulation management
		void RegisterCommand(std::shared_ptr<ServerSimulationCommand>& command) override;


		// Players management
		bool RegisterPlayer(std::shared_ptr<PlayerSimulationInformation> playerInformation) override;
		bool IsPlayerRegistered(PlayerSimInfoId playerId) const;
		void UnregisterPlayer(PlayerSimInfoId playerId);

		// Returns a weak pointer to avoid the need to handle concurrency of when the server unloads player information.
		std::weak_ptr<PlayerSimulationInformation> GetPlayerInformation(PlayerSimInfoId playerId) const;
		std::vector<PlayerSimInfoId> GetListRegisteredPlayersIds() const override;

		void PlayerSimulationLoaded(PlayerSimInfoId playerId) override;


		// Join management
		bool HasJoinGameRepliesToDispatch() const override;
		void DispatchJoinGameReplies(std::function<void(std::weak_ptr<PlayerSimulationInformation>)> dispatchFunction) override;


		// Actions on simulations
		ActorId SpawnActor(ActorTypeId typeId, Nest::Vector2<simfix64> position);
		void RegisterPlayerMovementRequest(PlayerSimInfoId playerId, Nest::Vector2<simfix64> destination);



	private:
		void DoTick(simfix64 tickDeltaTime) override;
		void ExecuteReceivedServerCommands();

		void ExecutePlayerMovementRequests();
		void SwapPlayerMovementRequestsList();
		void HandleActorsMovementEventsForSimulation() const;

		void AppendCommandToAllPlayers(const std::shared_ptr<ClientSimulationCommand>& simulationCommand);
		void AppendCommandToPlayer(PlayerSimulationCommandsQueuesManager* playerCommandsQueueManager, const std::shared_ptr<ClientSimulationCommand>& simulationCommand);
		void CommitAllPlayersQueues();

		void ProcessPlayersSimulationsLoaded();

		// Simulation helping methods (server)
		void ExecuteSpawnPlayersActors();
		void ForEachPlayerActorThatNeedsToBeSpawned(std::function<void(PlayerSimInfoId)> action);
		void SpawnPlayerActor(PlayerSimInfoId id);

		void ExecutePlayerSimulationRangeManagersUpdate();
		void SendSpawnActorMessageOnPlayerRangeEnter(PlayerSimInfoId playerId, ActorNetworkId actorNetworkId);

		void ExecuteSendMovementUpdatesToPlayers();




	protected:
		std::unique_ptr<ActorsManager> m_ActorsManager;
		std::unique_ptr<ISimActorsSpawner> m_SimulationActorsSpawner;
		std::unique_ptr<Map> m_Map;
		std::unique_ptr<AStar> m_Pathfinding;

		Nest::ITemplatedSingleComponentsContainer<SimActorMovementBehavior>& m_SimActorMovementBehaviorsContainer;

		std::unique_ptr<SimulationActorMovementSystem> m_SimulationActorMovementSystem;

		std::unique_ptr<ServerSimulationCommandsQueue> m_ServerSimulationCommandsQueue;

		std::unique_ptr<PlayersSimulationsManager> m_PlayersSimulationsManager;
		std::unordered_map<PlayerSimInfoId, std::shared_ptr<PlayerSimulationInformation>> m_Players;

		std::mutex m_PlayersSimulationsLoadedToProcessMutex;
		std::vector<PlayerSimInfoId> m_PlayersSimulationsLoadedToProcess;
		std::vector<std::weak_ptr<PlayerSimulationInformation>> m_PlayersToReplyJoinGameRequests;


		struct PlayerMovementRequest
		{
			PlayerSimInfoId playerId;
			Nest::Vector2<simfix64> destination;
		};

		std::stack<PlayerMovementRequest> m_FirstPlayerMovementRequestsList;
		std::stack<PlayerMovementRequest> m_SecondPlayerMovementRequestsList;
		std::stack<PlayerMovementRequest>* m_BuildingPlayerMovementRequestsList;
		std::mutex m_SwapPlayerMovementRequestsListsMutex;

		SimulationTickValue m_SimulationTickValue;
		std::unique_ptr<GameServerTickEventsContainer> m_GameServerTickEventsContainer;
	};
}



#endif

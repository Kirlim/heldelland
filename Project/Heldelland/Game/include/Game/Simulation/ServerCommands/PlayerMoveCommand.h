//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__SERVER_COMMANDS__PLAYER_MOVE_COMMAND_H
#define GAME__SIMULATION__SERVER_COMMANDS__PLAYER_MOVE_COMMAND_H

#include<cinttypes>

#include<Nest/Vector2.h>

#include"Game/SimulationCommons/SimulationTypes.h"
#include"Game/Simulation/ServerSimulationCommand.h"
#include"Game/Network/NetworkMessage.h"



namespace Game
{
	// Responsibility:
	//    Request the player's avatar movement to a specific position.
	class PlayerMoveCommand : public ServerSimulationCommand, public NetworkMessage
	{
	public:
		PlayerMoveCommand();
		~PlayerMoveCommand() override;

		void Execute(ServerGameSimulation& simulation) override;
		NetworkMessageTypeId GetTypeId() const override;

		void SetDestination(Nest::Vector2<simfix64> destination);
		Nest::Vector2<simfix64> GetDestination() const;



	protected:
		bool DoSerialize(INetworkSerializer& serializer) override;



	public:
		static const NetworkMessageTypeId TypeId;



	protected:
		Nest::Vector2<simfix64> m_Destination;
	};
}



#endif

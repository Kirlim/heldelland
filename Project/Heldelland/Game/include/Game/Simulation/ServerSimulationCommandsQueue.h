
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__SERVER_SIMULATION_COMMANDS_QUEUE_H
#define GAME__SIMULATION__SERVER_SIMULATION_COMMANDS_QUEUE_H

#include<queue>
#include<memory>
#include<mutex>



namespace Game
{
    // Forward declarations
    class ServerSimulationCommand;



    class ServerSimulationCommandsQueue
    {
        using QueueType = std::queue<std::shared_ptr<ServerSimulationCommand>>;
    public:
        ServerSimulationCommandsQueue();
        virtual ~ServerSimulationCommandsQueue();

        void Enqueue(std::shared_ptr<ServerSimulationCommand> serverSimulationCommand);
        std::shared_ptr<ServerSimulationCommand> PeekNext() const;
        void DequeueFront();

        bool HasCommands() const;

        size_t Size() const;



    protected:
        QueueType m_CommandsQueue;
        mutable std::mutex m_QueueAccessMutex;
    };
}



#endif

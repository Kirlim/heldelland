
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__ISIM_ACTORS_SPAWNER_H
#define GAME__SIMULATION__ISIM_ACTORS_SPAWNER_H

#include"Game/SimulationCommons/ActorsTypes.h"



namespace Nest
{
    class IEntity;
}



namespace Game
{
    class ISimActorsSpawner
    {
    public:
        virtual ~ISimActorsSpawner();

        virtual Nest::IEntity* Spawn(ActorTypeId actorTypeId) const = 0;
    };
}



#endif

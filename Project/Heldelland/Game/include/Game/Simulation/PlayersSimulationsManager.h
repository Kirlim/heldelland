
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__PLAYERS_SIMULATIONS_MANAGER_H
#define GAME__SIMULATION__PLAYERS_SIMULATIONS_MANAGER_H

#include<unordered_map>
#include<memory>
#include<functional>

#include"Game/Simulation/Types/PlayerSimInfoId.h"



namespace Game
{
    // Forward declarations
    class PlayerSimulationCommandsQueuesManager;



    // Responsibility:
    //    Contain players simulations and provide some basic methods for their
    //    handling.
    class PlayersSimulationsManager
    {
        using SimulationsContainer = std::unordered_map<PlayerSimInfoId, std::unique_ptr<PlayerSimulationCommandsQueuesManager>>;

    public:
        PlayersSimulationsManager();
        ~PlayersSimulationsManager();

        void Register(PlayerSimInfoId playerId);
        void Unregister(PlayerSimInfoId playerId);
        bool IsRegistered(PlayerSimInfoId playerId) const;

        PlayerSimulationCommandsQueuesManager* GetPlayerCommandsQueueManager(PlayerSimInfoId playerId) const;

        void ForEach(std::function<void(PlayerSimulationCommandsQueuesManager*)> action);

        SimulationsContainer::iterator begin();
        SimulationsContainer::iterator end();



    protected:
        SimulationsContainer m_SimulationsQueuesManagers;
    };
}



#endif
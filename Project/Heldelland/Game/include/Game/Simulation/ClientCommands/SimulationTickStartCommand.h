//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__CLIENT_COMMANDS__SIMULATION_TICK_START_COMMAND_H
#define GAME__SIMULATION__CLIENT_COMMANDS__SIMULATION_TICK_START_COMMAND_H

#include"Game/Simulation/ClientSimulationCommand.h"
#include"Game/Network/NetworkMessage.h"



namespace Game
{
	// Responsibility:
	//    Flags that a new server tick will follow in a commands tick.
	//    This message should be the very first (unless there is a force
	//    tick value change message) in any commands queue from the server.
	class SimulationTickStartCommand : public ClientSimulationCommand, public NetworkMessage
	{
	public:
		SimulationTickStartCommand();
		~SimulationTickStartCommand() override;

		void Execute(IGameClientSimulation& simulation) override;

		NetworkMessageTypeId GetTypeId() const override;

		void SetTickValue(unsigned int tickValue);
		unsigned int GetTickValue() const;



	protected:
		bool DoSerialize(INetworkSerializer& serializer) override;



	public:
		static const NetworkMessageTypeId TypeId;

	protected:
		unsigned int m_TickValue{ 0 };
	};
}



#endif

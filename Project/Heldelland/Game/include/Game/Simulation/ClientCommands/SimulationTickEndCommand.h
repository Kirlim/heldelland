//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__CLIENT_COMMANDS__SIMULATION_TICK_END_COMMAND_H
#define GAME__SIMULATION__CLIENT_COMMANDS__SIMULATION_TICK_END_COMMAND_H

#include"Game/Simulation/ClientSimulationCommand.h"
#include"Game/Network/NetworkMessage.h"



namespace Game
{
	// Responsibility:
	//    Flags that a server tick has ended. Should be the very last
	//    command in every commands queue.
	class SimulationTickEndCommand : public ClientSimulationCommand, public NetworkMessage
	{
	public:
		SimulationTickEndCommand();
		~SimulationTickEndCommand() override;

		void Execute(IGameClientSimulation& simulation) override;

		NetworkMessageTypeId GetTypeId() const override;

		void SetTickValue(unsigned int tickValue);
		unsigned int GetTickValue() const;



	protected:
		bool DoSerialize(INetworkSerializer& serializer) override;



	public:
		static const NetworkMessageTypeId TypeId;

	protected:
		unsigned int m_TickValue{ 0 };
	};
}



#endif

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__CLIENT_COMMANDS__SET_ACTOR_FULL_PATH_COMMAND_H
#define GAME__SIMULATION__CLIENT_COMMANDS__SET_ACTOR_FULL_PATH_COMMAND_H

#include<vector>

#include<Nest/Vector2.h>

#include"Game/SimulationCommons/SimulationTypes.h"
#include"Game/Simulation/ClientSimulationCommand.h"
#include"Game/Network/NetworkMessage.h"



namespace Game
{
	class Path;



	class SetActorFullPathCommand : public ClientSimulationCommand, public NetworkMessage
	{
	public:
		SetActorFullPathCommand();
		~SetActorFullPathCommand() override;

		void Execute(IGameClientSimulation& simulation) override;

		NetworkMessageTypeId GetTypeId() const override;

		void SetActorNetworkId(ActorNetworkId actorNetworkId);
		void SetPath(const Path& path);

		ActorNetworkId GetActorNetworkId() const;
		int GetPathSize() const;
		Nest::Vector2<simfix64> GetWaypointAtIndex(int index) const;



	private:
		bool DoSerialize(INetworkSerializer& serializer) override;



	public:
		static const NetworkMessageTypeId TypeId;



	private:
		ActorNetworkId m_ActorNetworkId;
		std::vector<Nest::Vector2<simfix64>> m_Waypoints;
	};
}



#endif

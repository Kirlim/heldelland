//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__CLIENT_COMMANDS__FORCE_NEXT_TICK_VALUE_COMMAND_H
#define GAME__SIMULATION__CLIENT_COMMANDS__FORCE_NEXT_TICK_VALUE_COMMAND_H

#include"Game/Simulation/ClientSimulationCommand.h"
#include"Game/Network/NetworkMessage.h"



namespace Game
{
	// Responsibility:
	//    Force a client simulation tick to become an specific value.
	//    Useful for situations like setting the first simulation tick
	//    right after a client joins the game.
	class ForceNextTickValueCommand : public ClientSimulationCommand, public NetworkMessage
	{
	public:
		ForceNextTickValueCommand();
		~ForceNextTickValueCommand() override;

		void Execute(IGameClientSimulation& simulation) override;

		NetworkMessageTypeId GetTypeId() const override;

		void SetNextTickValue(unsigned int nextTick);
		unsigned int GetNextTickValue() const;



	protected:
		bool DoSerialize(INetworkSerializer& serializer) override;



	public:
		static const NetworkMessageTypeId TypeId;

	protected:
		unsigned int m_NextTickValue{ 0 };
	};
}



#endif

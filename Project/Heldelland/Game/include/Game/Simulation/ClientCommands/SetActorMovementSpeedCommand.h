//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__CLIENT_COMANDS__SET_ACTOR_MOVEMENT_SPEED_COMMAND_H
#define GAME__SIMULATION__CLIENT_COMANDS__SET_ACTOR_MOVEMENT_SPEED_COMMAND_H

#include"Game/Network/NetworkMessage.h"
#include"Game/SimulationCommons/SimulationTypes.h"
#include"Game/Simulation/ClientSimulationCommand.h"



namespace Game
{
	class SetActorMovementSpeedCommand : public ClientSimulationCommand, public NetworkMessage
	{
	public:
		SetActorMovementSpeedCommand();
		~SetActorMovementSpeedCommand() override;

		void Execute(IGameClientSimulation& simulation) override;

		NetworkMessageTypeId GetTypeId() const override;

		void SetActorNetworkId(ActorNetworkId actorNetworkId);
		void SetMovementSpeed(simfix64 movementSpeed);

		ActorNetworkId GetActorNetworkId() const;
		simfix64 GetMovementSpeed() const;



	private:
		bool DoSerialize(INetworkSerializer& serializer) override;



	public:
		static const NetworkMessageTypeId TypeId;



	private:
		ActorNetworkId m_ActorNetworkId;
		simfix64 m_MovementSpeed;
	};
}



#endif

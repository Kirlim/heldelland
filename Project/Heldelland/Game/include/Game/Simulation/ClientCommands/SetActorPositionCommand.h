//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__CLIENT_COMMANDS__SET_ACTOR_POSITION_COMMAND_H
#define GAME__SIMULATION__CLIENT_COMMANDS__SET_ACTOR_POSITION_COMMAND_H

#include<Nest/Vector2.h>

#include"Game/SimulationCommons/SimulationTypes.h"
#include"Game/Simulation/ClientSimulationCommand.h"
#include"Game/Network/NetworkMessage.h"



namespace Game
{
	class SetActorPositionCommand : public ClientSimulationCommand, public NetworkMessage
	{
	public:
		SetActorPositionCommand();
		~SetActorPositionCommand() override;

		void Execute(IGameClientSimulation& simulation) override;
		NetworkMessageTypeId GetTypeId() const override;

		void SetActorNetworkId(ActorNetworkId actorNetworkId);
		ActorNetworkId GetActorNetworkId() const;

		void SetPosition(Nest::Vector2<simfix64> position);
		Nest::Vector2<simfix64> GetPosition() const;



	protected:
		bool DoSerialize(INetworkSerializer& serializer) override;



	public:
		static const NetworkMessageTypeId TypeId;



	protected:
		ActorNetworkId m_ActorNetworkId;
		Nest::Vector2<simfix64> m_Position;
	};
}



#endif

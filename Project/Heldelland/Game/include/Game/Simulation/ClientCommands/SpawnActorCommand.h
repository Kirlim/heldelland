//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__CLIENT_COMMANDS__SPAWN_ACTOR_COMMAND_H
#define GAME__SIMULATION__CLIENT_COMMANDS__SPAWN_ACTOR_COMMAND_H

#include<Nest/Vector2.h>

#include"Game/SimulationCommons/SimulationTypes.h"

#include"Game/Simulation/ClientSimulationCommand.h"
#include"Game/SimulationCommons/ActorsTypes.h"
#include"Game/Network/NetworkMessage.h"



namespace Game
{
	// Responsibility:
	//    Tell a player simulation that a specific actor should be spawned,
	//    at a given position
	class SpawnActorCommand : public ClientSimulationCommand, public NetworkMessage
	{
	public:
		SpawnActorCommand();
		~SpawnActorCommand() override;

		void Execute(IGameClientSimulation& simulation) override;
		NetworkMessageTypeId GetTypeId() const override;

		void SetSpawnPosition(Nest::Vector2<simfix64> spawnPosition);
		Nest::Vector2<simfix64> GetSpawnPosition() const;

		void SetActorTypeId(ActorTypeId actorTypeId);
		ActorTypeId GetActorTypeId() const;

		void SetNetworkId(ActorNetworkId networkId);
		ActorNetworkId GetNetworkId() const;



	protected:
		bool DoSerialize(INetworkSerializer& serializer) override;



	public:
		static const NetworkMessageTypeId TypeId;

	protected:
		Nest::Vector2<simfix64> m_SpawnPosition;
		ActorTypeId m_ActorTypeId;
		ActorNetworkId m_ActorNetworkId{ 0 };
	};
}



#endif

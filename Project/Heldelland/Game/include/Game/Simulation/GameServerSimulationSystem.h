//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__GAME_SERVER_SIMULATION_SYSTEM_H
#define GAME__SIMULATION__GAME_SERVER_SIMULATION_SYSTEM_H

#include<memory>

#include<Nest/System.h>

#include"Game/SimulationCommons/SimulationTypes.h"



namespace Game
{
	class IGameServerSimulation;



	class GameServerSimulationSystem : public Nest::System
	{
	public:
		~GameServerSimulationSystem() override;
		void StartServerSimulation(std::unique_ptr<IGameServerSimulation>& gameServerSimulation);

		IGameServerSimulation* GetServerSimulation() const;



	private:
		void DoFixedUpdate(float fixedDeltaTime) override;



	public:
		const int TicksPerSecond = 10;
		const simfix64 TickDeltaTime{ 1.0f / float(TicksPerSecond) };

		float m_AccumulatedFixedDeltaTime = 0.0f;


		std::unique_ptr<IGameServerSimulation> m_GameServerSimulation;
	};
}



#endif

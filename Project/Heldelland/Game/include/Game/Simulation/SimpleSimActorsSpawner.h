
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__SIMPLE_SIM_ACTORS_SPAWNER_H
#define GAME__SIMULATION__SIMPLE_SIM_ACTORS_SPAWNER_H

#include<memory>
#include<functional>
#include<unordered_map>

#include"Game/Simulation/ISimActorsSpawner.h"



namespace Nest
{
    // Forward declarations
    class IEntity;
}



namespace Game
{
    // TODO : delete this class when prefabs are available
    // Placeholder class to allow instantiating actors
    // while prefabs are not implemented
    class SimpleSimActorsSpawner : public ISimActorsSpawner
    {
    public:
        SimpleSimActorsSpawner();
        ~SimpleSimActorsSpawner() override;

        Nest::IEntity* Spawn(ActorTypeId actorTypeId) const override;

        void SetSpawnFunction(ActorTypeId actorTypeId, std::function<Nest::IEntity*()> function);



    protected:
        std::unordered_map<ActorTypeId, std::function<Nest::IEntity*()>> m_SpawnFunctions;
    };
}

#endif
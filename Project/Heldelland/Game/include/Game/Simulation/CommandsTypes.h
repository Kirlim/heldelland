//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__COMMANDS_TYPES_H
#define GAME__SIMULATION__COMMANDS_TYPES_H

// TODO : Put NetworkMessageTypeId and NetworkMessageId on a specific header, to reduce big headers includes
#include"Game/Network/NetworkMessage.h"



#define SPAWN_PLAYER_CHARACTER_COMMAND_TYPE             10
#define SPAWN_ACTOR_COMMAND_TYPE                        11
#define SET_ACTOR_POSITION_COMMAND_TYPE                 12
#define SET_ACTOR_FULL_PATH_COMMAND_TYPE                20
#define SET_ACTOR_MOVEMENT_SPEED_COMMAND_TYPE           21
#define FORCE_NEXT_TICK_VALUE_COMMAND_TYPE              100
#define SIMULATION_TICK_START_COMMAND_TYPE              101
#define SIMULATION_TICK_END_COMMAND_TYPE                102
#define PLAYER_MOVE_COMMAND_TYPE                        200



namespace Game
{
	bool IsActorSpawningCommand(NetworkMessageTypeId networkMessageTypeId);
}



#endif

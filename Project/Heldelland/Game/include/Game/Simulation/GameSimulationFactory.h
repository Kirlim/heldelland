//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__GAME_SIMULATION_FACTORY_H
#define GAME__SIMULATION__GAME_SIMULATION_FACTORY_H

#include<memory>
#include<string>

#include"Game/SimulationCommons/SimulationTypes.h"



namespace Nest
{
	class EntitiesManager;
	class IComponentsContainers;
	class IResourcesManager;
	class World;
}

namespace Game
{
	class IGameClientSimulation;
	class IGameServerSimulation;
	class MapsFactory;
	class SimulationActorsMovementData;
	class SimulationActorsMovementDataManager;
	class SimulationActorsMovementLogic;



	class GameSimulationFactory
	{
	public:
		GameSimulationFactory(
			Nest::EntitiesManager& entitiesManager,
			Nest::IResourcesManager& resourcesManager,
			const Nest::IComponentsContainers& componentsContainers,
			const MapsFactory& mapsFactory,
			SimulationActorsMovementData& simulationActorsMovementData,
			SimulationActorsMovementDataManager& simulationActorsMovementDataManager,
			SimulationActorsMovementLogic& simulationActorsMovementLogic
		);
		~GameSimulationFactory();

		std::unique_ptr<IGameServerSimulation> InstantiateServerSimulation(std::string mapName) const;
		std::unique_ptr<IGameClientSimulation> InstantiateClientSimulation(
			const Nest::World& simulationWorld,
			std::string mapName,
			simfix64 tickDeltaTime,
			float clientMaxTimeBehindBeforeTimeSpeedUp,
			float clientTimeSpeedUpFactor
		) const;



	private:
		Nest::EntitiesManager& m_EntitiesManager;
		Nest::IResourcesManager& m_ResourcesManager;
		const Nest::IComponentsContainers& m_ComponentsContainers;
		const MapsFactory& m_MapsFactory;
		SimulationActorsMovementData& m_SimulationActorsMovementData;
		SimulationActorsMovementDataManager& m_SimulationActorsMovementDataManager;
		SimulationActorsMovementLogic& m_SimulationActorsMovementLogic;
	};
}



#endif

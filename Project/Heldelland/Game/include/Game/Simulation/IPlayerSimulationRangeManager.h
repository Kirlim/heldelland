//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__I_PLAYER_SIMULATION_RANGE_MANAGER_H
#define GAME__SIMULATION__I_PLAYER_SIMULATION_RANGE_MANAGER_H

#include<vector>

#include"Game/SimulationCommons/SimulationTypes.h"



namespace Nest
{
	template <typename T>
	class Vector3;
}

namespace Game
{
	// Responsibility:
	//    Store and control information and helping classes intances that allows
	//    the management of what is supposed to be sent to the player for client
	//    simulation, and in what format.
	class IPlayerSimulationRangeManager
	{
	public:
		virtual ~IPlayerSimulationRangeManager()
		{
		};

		virtual void SetViewRadius(simfix64 viewRadius) = 0;
		virtual void SetPlayerPosition(Nest::Vector3<simfix64> playerPosition) = 0;

		virtual bool IsEntityRegistered(ActorNetworkId actorNetworkId) const = 0;

		virtual std::vector<ActorNetworkId> RegisterAndReturnMovingEntitiesThatCameWithinRange() = 0;
		virtual std::vector<ActorNetworkId> UnregisterAndReturnMovingEntitiesThatWentOutOfRange() = 0;

		virtual void GetRegisteredActorsNetworkIdsByAppending(std::vector<ActorNetworkId>& out_RegisteredActorsNetworkIds) const = 0;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__PLAYER_SIMULATION_COMMANDS_QUEUES_MANAGER_H
#define GAME__SIMULATION__PLAYER_SIMULATION_COMMANDS_QUEUES_MANAGER_H

#include<memory>
#include<queue>



namespace Game
{
    // Forward declarations
    class ClientSimulationCommand;
    class ClientSimulationCommandsQueue;



    // Resposibility:
    //    Handle the gradual building of the next commands queue to be
    //    executed by the player's simulation, as well as keeping a storage
    //    of execution queues already built (until they are removed from
    //    the instance)
    class PlayerSimulationCommandsQueuesManager
    {
    public:
        PlayerSimulationCommandsQueuesManager();
        ~PlayerSimulationCommandsQueuesManager();

        // Commits the current queue being built, and store it as the next queue in line.
        // Will immediately start the building of the next queue.
        void CommitQueue();

        // Will append a new simulation command to the queue being built
        void AppendCommand(std::shared_ptr<ClientSimulationCommand> command);

        // Returns the number of commands in the queue being built
        unsigned int GetCurrentQueueSize() const;

        // Clears the queue being built.
        void ClearQueue();



        bool HasCommittedQueues() const;
        unsigned int CommittedQueuesCount() const;
        std::unique_ptr<ClientSimulationCommandsQueue>  FetchNextCommittedQueue();



    protected:
        std::queue<std::unique_ptr<ClientSimulationCommandsQueue>> m_CommandsQueues;
        std::unique_ptr<ClientSimulationCommandsQueue> m_CurrentQueue;
    };
}

#endif
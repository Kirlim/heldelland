//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__I_GAME_CLIENT_SIMULATION_H
#define GAME__SIMULATION__I_GAME_CLIENT_SIMULATION_H

#include<memory>
#include<functional>

#include"Game/SimulationCommons/ActorsTypes.h"
#include"Game/SimulationCommons/ActorBehavior.h"



namespace Nest
{
	class EventsBusToken;
	template <typename T>
	class Vector2;
}

namespace Game
{
	class ActorsManager;
	class Map;
	class PlayerSimulationCommandsQueuesManager;
	class ServerSimulationCommandsQueue;
	class SimulationActorMovementSystem;



	// Available Client Game Simulation Events
	struct ActorSpawnedEvt
	{
		ActorId id;
		ActorNetworkId networkId;
		ActorTypeId typeId;
	};



	// TODO : check out if we really need an interface for game client simulation
	class IGameClientSimulation
	{
	public:
		virtual ~IGameClientSimulation();

		// TODO : verify which of these methods will stay here, and convert to Do...() form
		// TODO : also, some of them might need to be refactored for better designe (such as putting on new interfaces, wrapper methods or specific patterns)

		virtual ActorsManager* GetActorsManager() const = 0;

		virtual void Update(float deltaTime) = 0;
		virtual Map* GetMap() const = 0;
		virtual SimulationActorMovementSystem* GetSimulationActorMovementSystem() const = 0;
		virtual PlayerSimulationCommandsQueuesManager* GetQueuesManager() const = 0;
		virtual ServerSimulationCommandsQueue* GetToServerSimulationCommandsQueue() const = 0;

		virtual void ForceNextTickValue(unsigned int nextTickValue) = 0;
		virtual float GetCurrentTickElapsedTimePercentage() const = 0;

		virtual void SetPlayerActorNetworkId(ActorNetworkId actorNetworkId) noexcept = 0;
		virtual ActorNetworkId GetPlayerActorNetworkId() const noexcept = 0;

		virtual ActorId SpawnActor(
			ActorTypeId typeId,
			Nest::Vector2<simfix64> position,
			bool hasNetworkId,
			ActorNetworkId networkId = ActorNetworkId{ 0 }
		) const = 0;

		virtual std::unique_ptr<Nest::EventsBusToken> RegisterToActorSpawnedEvent(std::function<void(const ActorSpawnedEvt&)> listener) = 0;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__I_GAME_SERVER_SIMULATION_H
#define GAME__SIMULATION__I_GAME_SERVER_SIMULATION_H

#include<memory>
#include<vector>

#include"Game/SimulationCommons/SimulationTypes.h"
#include"Game/Simulation/Types/PlayerSimInfoId.h"



namespace Game
{
	class PlayerSimulationInformation;
	class PlayersSimulationsManager;
	class ServerSimulationCommand;
	class SimulationActorMovementSystem;



	class IGameServerSimulation
	{
	public:
		virtual ~IGameServerSimulation() = default;

		void Tick(simfix64 tickDeltaTime);

		// TODO : methods moved here to reduce the amount of refactor in order to get stable when dividing simulation into server and client. Verify if it is possible to move them into another class
		// TODO : if not, create new virtual abstracts Do...()
		virtual PlayersSimulationsManager* GetPlayersSimulationsManager() const = 0;
		virtual SimulationActorMovementSystem* GetSimulationActorMovementSystem() const = 0;
		virtual void RegisterCommand(std::shared_ptr<ServerSimulationCommand>& command) = 0;
		virtual bool RegisterPlayer(std::shared_ptr<PlayerSimulationInformation> playerInformation) = 0;
		virtual std::vector<PlayerSimInfoId> GetListRegisteredPlayersIds() const = 0;
		virtual void PlayerSimulationLoaded(PlayerSimInfoId playerId) = 0;
		virtual bool HasJoinGameRepliesToDispatch() const = 0;
		virtual void DispatchJoinGameReplies(std::function<void(std::weak_ptr<PlayerSimulationInformation>)> dispatchFunction) = 0;



	private:
		virtual void DoTick(simfix64 tickDeltaTime) = 0;
	};
}



#endif

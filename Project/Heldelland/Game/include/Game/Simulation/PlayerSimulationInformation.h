//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__PLAYER_SIMULATION_INFORMATION_H
#define GAME__SIMULATION__PLAYER_SIMULATION_INFORMATION_H

#include<memory>

#include"Game/Simulation/Types/PlayerSimInfoId.h"
#include"Game/SimulationCommons/SimulationTypes.h"



namespace Game
{
	// Forward declarations
	class IPlayerSimulationRangeManager;



	// Responsibility:
	//    Store player information in a game simulation, such as owning simulation
	//    id, player id, and gameplay information as statistics and others.
	class PlayerSimulationInformation
	{
	public:
		PlayerSimulationInformation(PlayerSimInfoId id, std::unique_ptr<IPlayerSimulationRangeManager> playerSimulationRangeManager);
		~PlayerSimulationInformation();

		PlayerSimInfoId GetId() const;

		void SetSimulationLoadedFlag(bool loaded);
		bool HasSimulationBeenLoaded() const;

		bool IsSpawned() const;
		bool CanBeSpawned() const;
		void SetAsSpawned();

		void SetActorNetworkId(ActorNetworkId actorNetworkId);
		ActorNetworkId GetActorNetworkId() const;

		IPlayerSimulationRangeManager* GetPlayerSimulationRangeManager() const;



	protected:
		const PlayerSimInfoId m_Id;

		bool m_HasSimulationBeenLoaded = false;
		bool m_IsSpawned = false;

		ActorNetworkId m_ActorNetworkId;

		std::unique_ptr<IPlayerSimulationRangeManager> m_PlayerSimulationRangeManager;
	};
}

#endif

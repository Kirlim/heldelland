//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__PLAYER_SIMULATION_RANGE_MANAGER_H
#define GAME__SIMULATION__PLAYER_SIMULATION_RANGE_MANAGER_H

#include<vector>

#include<Nest/Vector3.h>

#include"Game/SimulationCommons/SimulationTypes.h"
#include"Game/Simulation/IPlayerSimulationRangeManager.h"



namespace Game
{
	// Forward declarations
	class SimulationActorsMovementData;



	class PlayerSimulationRangeManager : public IPlayerSimulationRangeManager
	{
	public:
		explicit PlayerSimulationRangeManager(const SimulationActorsMovementData& simulationActorsMovementData);
		PlayerSimulationRangeManager(const PlayerSimulationRangeManager&) = delete;
		PlayerSimulationRangeManager(PlayerSimulationRangeManager&&) = delete;

		~PlayerSimulationRangeManager() override;

		PlayerSimulationRangeManager& operator=(const PlayerSimulationRangeManager&) = delete;
		PlayerSimulationRangeManager& operator=(PlayerSimulationRangeManager&&) = delete;

		void SetViewRadius(simfix64 viewRadius) override;
		void SetPlayerPosition(Nest::Vector3<simfix64> playerPosition) override;

		bool IsEntityRegistered(ActorNetworkId actorNetworkId) const override;

		// TODO : There should be a singe method UpdateActorsInRange, instead of two (register and unregister)
		// TODO : callbacks should be converted te messages bus. Keep batching in mind
		std::vector<ActorNetworkId> RegisterAndReturnMovingEntitiesThatCameWithinRange() override;
		std::vector<ActorNetworkId> UnregisterAndReturnMovingEntitiesThatWentOutOfRange() override;

		void GetRegisteredActorsNetworkIdsByAppending(std::vector<ActorNetworkId>& out_RegisteredActorsNetworkIds) const override;



	private:
		void RegisterMovingEntity(ActorNetworkId actorNetworkId);



	private:
		const SimulationActorsMovementData& m_SimulationActorsMovementData;

		Nest::Vector3<simfix64> m_PlayerPosition;
		simfix64 m_ViewRadius;

		std::vector<ActorNetworkId> m_RegisteredActorsNetworkIds;
	};
}



#endif

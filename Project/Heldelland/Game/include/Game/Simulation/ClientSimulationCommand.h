//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__CLIENT_SIMULATION_COMMAND_H
#define GAME__SIMULATION__CLIENT_SIMULATION_COMMAND_H



namespace Game
{
	// Forward declarations
	class IGameClientSimulation;



	// Base class for game simulation commands.
	class ClientSimulationCommand
	{
	public:
		ClientSimulationCommand();
		virtual ~ClientSimulationCommand();

		virtual void Execute(IGameClientSimulation& simulation) = 0;
	};
}



#endif

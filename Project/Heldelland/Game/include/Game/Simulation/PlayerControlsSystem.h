
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__PLAYER_CONTROLS_SYSTEM_H
#define GAME__SIMULATION__PLAYER_CONTROLS_SYSTEM_H

#include<Nest/System.h>



namespace Nest
{
	class RenderingManager;
}

namespace Game
{
	// Forward declarations
	class GameSimulationSystem;
	class ServerSimulationCommandsQueue;
	class ClientGameSimulation;

	class GameClientSimulationSystem;



	// Responsibility:
	//    Detect player controls and translate them so that they
	//    can be used by either client or server simulations, or
	//    the game UI.
	class PlayerControlsSystem final : public Nest::System
	{
	public:
		PlayerControlsSystem(Nest::RenderingManager& renderingManager, GameClientSimulationSystem& gameClientSimulationSystem);
		virtual ~PlayerControlsSystem() = default;




	private:
		void DoUpdate(float deltaTime) override;
		void OnMapClickHandler(float mapX, float mapZ) const;



	private:
		Nest::RenderingManager& m_RenderingManager;
		GameClientSimulationSystem& m_GameClientSimulationSystem;
	};
}



#endif

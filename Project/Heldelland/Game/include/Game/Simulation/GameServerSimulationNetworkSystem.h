//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__GAME_SERVER_SIMULATION_NETWORK_SYSTEM_H
#define GAME__SIMULATION__GAME_SERVER_SIMULATION_NETWORK_SYSTEM_H

#include<memory>
#include<unordered_map>

#include<Nest/System.h>

#include"Game/Network/HostId.h"
#include"Game/Simulation/Types/PlayerSimInfoId.h"



namespace Nest
{
	class EventsBusToken;
}

namespace Game
{
	class ClientSimulationCommandsQueue;
	class GameServerSimulationSystem;
	class IHost;
	class IHostChannel;
	class IHostsChannels;
	class IHostsManager;
	class INetworkMessagesHandlersManager;
	class INetworkSystem;
	class IServerNetworkController;
	class NetworkMessage;
	class INetworkMessagesFactory;
	class PlayerSimulationInformation;

	class SimActorMovementBehavior;

	class ServerSimulationCommand;
	class JoinGameRequestMessage;



	class GameServerSimulationNetworkSystem : public Nest::System
	{
	public:
		GameServerSimulationNetworkSystem(
			INetworkSystem& networkSystem,
			GameServerSimulationSystem& gameServerSimulationSystem,
			INetworkMessagesFactory& networkMessagesFactory,
			INetworkMessagesHandlersManager& networkMessagesHandlersManager,
			IHostsManager& hostsManager
		);
		GameServerSimulationNetworkSystem(const GameServerSimulationNetworkSystem&) = delete;
		GameServerSimulationNetworkSystem(GameServerSimulationNetworkSystem&&) = default;

		GameServerSimulationNetworkSystem& operator=(const GameServerSimulationNetworkSystem&) = delete;
		GameServerSimulationNetworkSystem& operator=(GameServerSimulationNetworkSystem&&) = default;

		~GameServerSimulationNetworkSystem() override;



	private:
		void RegisterIncomingMessagesHandlers();

		void DoFixedUpdate(float) override;
		void TickServerNetworkController();
		void TickServerSimulation();

		void SendPendingPlayersSimulationsMessages() const;
		void SendCommandsQueueToPlayer(PlayerSimInfoId playerSimInfoId, ClientSimulationCommandsQueue& commandsQueue) const;

		void DispatchJoinGameRequestsReplies();
		void AddJoinGameRequestReplyToClientChannel(HostId hostId) const;
		std::shared_ptr<NetworkMessage> CreateJoinGameReplyNetworkMessage() const;

		void AddGameSimulationSettingsToClientChannel(HostId hostId) const;
		std::shared_ptr<NetworkMessage> CreateGameSimulationSettingsNetworkMessage() const;

		HostId GetHostIdFromPlayerSimulationInfo(const std::weak_ptr<PlayerSimulationInformation>& playerSimulationInformation) const;
		IHost* GetHost(HostId hostId) const;
		IHostChannel* GetNetworkChannelToClient(HostId hostId) const;
		void SendNetworkMessageToClientChannel(std::shared_ptr<NetworkMessage>& message, HostId hostId) const;


		void HandleIncomingJoinGameRequest(const NetworkMessage& joinGameRequestMessage);
		std::shared_ptr<PlayerSimulationInformation> RegisterNewPlayerSimulationInformationToServer() const;
		void RegisterNewPlayerHostInformation(
			const JoinGameRequestMessage& joinGameRequestMessage,
			const std::shared_ptr<PlayerSimulationInformation>& playerSimulationInformation
		);

		void HandleGameSimulationLoadedMessage(const NetworkMessage& gameSimulationLoadedMessage) const;

		void HandleServerSimulationCommand(std::shared_ptr<ServerSimulationCommand> command) const;



	private:
		INetworkSystem& m_NetworkSystem;
		GameServerSimulationSystem& m_GameServerSimulationSystem;
		INetworkMessagesFactory& m_NetworkMessagesFactory;
		INetworkMessagesHandlersManager& m_NetworkMessagesHandlersManager;

		IHostsManager& m_HostsManager;

		std::unordered_map<PlayerSimInfoId, HostId> m_PlayerIdToHostIdMap;


		std::unique_ptr<Nest::EventsBusToken> m_JoinGameRequestMessagesToken;
		std::unique_ptr<Nest::EventsBusToken> m_GameSimulationLoadedMessageToken;
		std::unique_ptr<Nest::EventsBusToken> m_PlayerMoveCommandsToken;

		std::unique_ptr<IServerNetworkController> m_ServerNetworkController;
	};
}



#endif

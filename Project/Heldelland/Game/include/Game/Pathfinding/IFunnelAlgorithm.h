//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__PATHFINDING__I_FUNNEL_ALGORITHM_H
#define GAME__PATHFINDING__I_FUNNEL_ALGORITHM_H

#include<vector>

#include<Nest/Vector2.h>

#include"Game/SimulationCommons/SimulationTypes.h"



namespace Game
{
	class IFunnelAlgorithm
	{
	public:
		virtual ~IFunnelAlgorithm();

		// Processes a path made up by portals. Portals are assumed to be pairs
		// of Left-Right positions, with left positions being on even indexes,
		// and right positions on odd indexes.
		//
		// Returns an optimized path to be followed.
		std::vector<Nest::Vector2<simfix64>> ProcessPath(const std::vector<Nest::Vector2<simfix64>>& portals);



	private:
		virtual std::vector<Nest::Vector2<simfix64>> DoProcessPath(const std::vector<Nest::Vector2<simfix64>>& portals) = 0;
	};
}



#endif

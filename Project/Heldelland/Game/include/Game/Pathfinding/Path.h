//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__PATHFINDING__PATH_H
#define GAME__PATHFINDING__PATH_H

#include<vector>

#include<Nest/Vector2.h>

#include"Game/SimulationCommons/SimulationTypes.h"



namespace Game
{
	class Path
	{
	public:
		Path();
		Path(const Path&) = default;
		Path(Path&&) = default;

		~Path();

		Path& operator=(const Path&) = default;
		Path& operator=(Path&&) = default;

		void PushBack(Nest::Vector2<simfix64> waypoint);
		void RecalculateDistances();
		void Invert();

		int GetSize() const;
		simfix64 GetTotalDistance() const;
		Nest::Vector2<simfix64> GetPositionByIndex(int waypointIndex) const;
		Nest::Vector2<simfix64> GetPositionByDistance(simfix64 distance) const;

		void Reset();



	protected:
		struct Waypoint
		{
			Nest::Vector2<simfix64> position;
			simfix64 accumulatedDistance;

			Waypoint(Nest::Vector2<simfix64> position, simfix64 accumulatedDistance)
			{
				this->position = position;
				this->accumulatedDistance = accumulatedDistance;
			}
		};

		std::vector<Waypoint> m_Waypoints;
	};
}



#endif

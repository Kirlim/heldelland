//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__PATHFINDING__SIMPLE_STUPID_FUNNEL_ALGORITHM_H
#define GAME__PATHFINDING__SIMPLE_STUPID_FUNNEL_ALGORITHM_H

#include"Game/PathFinding/IFunnelAlgorithm.h"



namespace Game
{
	// Simple Stupid Funnel Algorithm is an easy to understand and to
	// implement "path string pulling" algorithm for improving paths.
	//
	// It has been posted by Mikko Mononen on digestingduck blog.
	class SimpleStupidFunnelAlgorithm : public IFunnelAlgorithm
	{
	private:
		std::vector<Nest::Vector2<simfix64>> DoProcessPath(const std::vector<Nest::Vector2<simfix64>>& portals) override;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once



namespace Game
{
    struct AStarNode
    {
        int parent;
        int heapIndex; // if != -1, is in the open list
        unsigned int walkCost;
        unsigned int totalCost; //Sacrificing memory for slight performance improvement
        unsigned int status; //Used to know if it is in the closed list or not

        AStarNode() {
            this->parent = 0;
            this->heapIndex = 0;
            this->walkCost = 0;
            this->totalCost = 0;
            this->status = 0;
        }
        
        AStarNode(int parent, int heapIndex, unsigned int walkCost, unsigned int heuristic, unsigned int status) {
            this->parent = parent;
            this->heapIndex = heapIndex;
            this->walkCost = walkCost;
            this->totalCost = walkCost + heuristic;
            this->status = status;
        }
    };
}

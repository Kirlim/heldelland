
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<memory>



namespace Game
{
    // Forward declarations
    struct AStarNode;



    class BinaryHeap
    {
    public:
        BinaryHeap();
        ~BinaryHeap();

        void SetUp(AStarNode* const nodes, const unsigned int maxSize);
        void Add(const int node, const int parentNode, const unsigned int walkCost, const unsigned int heuristicCost);
        void UpdateIfNeeded(const int index, const int newParent, const unsigned int walkCost, const unsigned int heuristicCost);
        int FetchTop();

        int Size() const;
        void Clear();



    protected:
        int m_MaxSize;
        int m_CurrentSize;
        std::unique_ptr<int[]> m_HeapData; // List of indexes of nodes. Note: "starts" on index 1, for performance.
        AStarNode* m_Nodes;
    };
}

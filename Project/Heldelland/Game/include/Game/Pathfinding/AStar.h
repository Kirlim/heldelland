//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__PATHFINDING_ASTAR_H
#define GAME__PATHFINDING_ASTAR_H

#include<memory>
#include<vector>

#include<Nest/Vector2.h>

#include"Game/SimulationCommons/SimulationTypes.h"



namespace Game
{
	// Forward declarations
	struct AStarNode;

	class IFunnelAlgorithm;
	class BinaryHeap;
	class Path;



	class AStar
	{
	public:
		explicit AStar(std::unique_ptr<IFunnelAlgorithm>& funnelAlgorithm);
		AStar(const AStar&) = delete;
		AStar(AStar&&) = default;

		~AStar();

		AStar& operator=(const AStar&) = delete;
		AStar& operator=(AStar&&) = default;


		void SetUp(const bool* solidTilesMatrix, int mapWidth, int mapHeight, simfix64 tileSize);

		bool FindPath(simfix64 originX, simfix64 originY, simfix64 destinationX, simfix64 destinationY, Path& outPath);


		static const unsigned char NavNorth = 1;
		static const unsigned char NavSouth = 2;
		static const unsigned char NavEast = 4;
		static const unsigned char NavWest = 8;
		static const unsigned char NavNortheast = 16;
		static const unsigned char NavSoutheast = 32;
		static const unsigned char NavNorthwest = 64;
		static const unsigned char NavSouthwest = 128;



	protected:
		void BuildPathPortals(
			std::vector<Nest::Vector2<simfix64>>& out_Portals, simfix64 originPosX, simfix64 originPosY, int destination
		) const;

		void BuildCollisionMap(const bool* solidTilesMatrix, int mapWidth, int mapHeight);
		void InitMapNodes(int mapWidth, int mapHeight);

		void AddNeighbour(int node, int parent, int cost) const;

		int GetHeuristic(int node) const
		{
			int dx = m_DestinationX - (node % m_MapWidth);
			if (dx < 0) dx = 0 - dx;

			int dy = m_DestinationY - (node / m_MapWidth);
			if (dy < 0) dy = 0 - dy;

			return ((dx + dy) * 10);
		}



	private:
		int m_MapWidth;
		int m_MapHeight;
		simfix64 m_TileSize;

		std::unique_ptr<unsigned char[]> m_CollisionMap;
		std::unique_ptr<AStarNode[]> m_MapNodes;
		std::unique_ptr<BinaryHeap> m_OpenList;

		std::unique_ptr<IFunnelAlgorithm> m_FunnelAlgorithm;

		// Used to know the value of the current closed status. Any
		// node with a value smaller or equal to this is closed. This is
		// useful to avoid visiting every node in order to clear the
		// 'visited' and 'closed' flags for each path.
		unsigned int m_CurrentClosedStatus;

		// Storing destination coordinates of a path so that we don't need to constantly
		// pass it by parameter
		int m_DestinationX, m_DestinationY;
	};
}



#endif

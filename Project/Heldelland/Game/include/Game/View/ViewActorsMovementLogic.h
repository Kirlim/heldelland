//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__VIEW__VIEW_ACTORS_MOVEMENT_LOGIC_H
#define GAME__VIEW__VIEW_ACTORS_MOVEMENT_LOGIC_H

#include<vector>

#include<Nest/Vector2.h>

#include"Game/SimulationCommons/SimulationTypes.h"
#include"Game/SimulationCommons/SimulationActorsMovementData.h"



namespace Game
{
	class ITerrainView;



	class ViewActorsMovementLogic
	{
	public:
		~ViewActorsMovementLogic();

		void CalculateActorsPositions(
			float simulationTickTimeElapsedPct,
			const std::vector<ActorNetworkId>& actorsNetworkIds,
			const std::vector<ActorInFrameMovementPositions>& actorsPositions
		);

		// Pre-requisite: CalculateActorsPositions has been called previously.
		void CalculateActorsPositionsHeights(
			const ITerrainView& terrainView
		);



	public:
		std::vector<ActorNetworkId> actorsNetworkIds;
		std::vector<Nest::Vector2f> actorsPositions;
		std::vector<float> actorsPositionHeights;
	};
}



#endif

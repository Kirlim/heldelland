
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__VIEW__IVIEW_ACTORS_SPAWNER_H
#define GAME__VIEW__IVIEW_ACTORS_SPAWNER_H

#include"Game/SimulationCommons/ActorsTypes.h"



namespace Nest
{
    // Forward declarations
    class IEntity;
}



namespace Game
{
    // Responsibility:
    //    Provide an easy way to spawn specific actors for the game
    //    simulation (client sided).
    class IViewActorsSpawner
    {
    public:
        IViewActorsSpawner() {}
        virtual ~IViewActorsSpawner() {}

        virtual Nest::IEntity* Spawn(ActorTypeId actorTypeId) const = 0;
    };
}


#endif
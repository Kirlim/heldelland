
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__VIEW__GAME_VIEW_SIMULATION_SYSTEM_H
#define GAME__VIEW__GAME_VIEW_SIMULATION_SYSTEM_H

#include<memory>

#include<Nest/System.h>



namespace Game
{
	class IGameViewSimulation;



	class GameViewSimulationSystem : public Nest::System
	{
	public:
		~GameViewSimulationSystem() override;
		void StartViewSimulation(std::unique_ptr<IGameViewSimulation>& gameClientSimulation);

		IGameViewSimulation* GetViewSimulation() const;



	private:
		void DoUpdate(float deltaTime) override;



	private:
		std::unique_ptr<IGameViewSimulation> m_GameViewSimulation;
	};
}



#endif

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__VIEW__GAME_VIEW_SIMULATION_H
#define GAME__VIEW__GAME_VIEW_SIMULATION_H

#include<memory>

#include"Game/View/IGameViewSimulation.h"
#include"Game/SimulationCommons/SimulationTypes.h"



namespace Nest
{
	class EntitiesManager;
	class EventsBusToken;
	class RenderingManager;
	class RenderingModule;
	class World;
}

namespace Game
{
	class IMapViewFactory;
	class IViewActorsSpawner;
	class Map;
	class MapView;
	class ViewActorsManager;
	class ViewActorsMovementLogic;

	struct ActorSpawnedEvt;



	class GameSimulationView : public IGameViewSimulation
	{
	public:
		GameSimulationView(
			Nest::EntitiesManager& entitiesManager,
			Map& map,
			const IMapViewFactory& mapViewFactory,
			Nest::RenderingModule& renderingModule,
			Nest::World& world,
			Nest::RenderingManager& renderingManager,
			std::unique_ptr<IViewActorsSpawner>& viewActorsSpawner,
			std::unique_ptr<ViewActorsMovementLogic>& viewActorsMovementLogic
		);



	private:
		void DoUpdateViewSimulation(float deltaTime) const override;
		void CalculateActorsPositions() const;

		void DoBindToGameClientSimulation(IGameClientSimulation& gameClientSimulation) override;
		void DoRegisterToClientSimulationEvents(IGameClientSimulation& gameClientSimulation) override;

		void InitializeMapView(Map& map);
		void InitializeMapLights() const;

		void InitializeViewCamera(Nest::RenderingModule& renderingModule, Nest::World& world) const;

		void OnActorSpawnedEvent(const ActorSpawnedEvt& actorSpawnedEvent) const;
		void SpawnActor(ActorNetworkId actorNetworkId) const;



	private:
		IGameClientSimulation* m_GameClientSimulation = nullptr;
		Nest::EntitiesManager& m_EntitiesManager;
		Nest::RenderingManager& m_RenderingManager;
		std::unique_ptr<IViewActorsSpawner> m_ViewActorsSpawner;
		std::unique_ptr<ViewActorsManager> m_ViewActorsManager;
		std::unique_ptr<ViewActorsMovementLogic> m_ViewActorsMovementLogic;

		const IMapViewFactory& m_MapViewFactory;
		std::unique_ptr<MapView> m_MapView;

		std::unique_ptr<Nest::EventsBusToken> m_OnActorSpawnedListenerToken;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__VIEW__I_GAME_SIMULATION_VIEW_FACTORY_H
#define GAME__VIEW__I_GAME_SIMULATION_VIEW_FACTORY_H

#include<memory>



namespace Game
{
	class IGameViewSimulation;
	class Map;



	class IGameViewSimulationFactory
	{
	public:
		virtual std::unique_ptr<IGameViewSimulation> Instantiate(Map& map) const = 0;
		virtual ~IGameViewSimulationFactory() = default;
	};
}



#endif

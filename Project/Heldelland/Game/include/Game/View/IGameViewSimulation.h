
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__VIEW__I_GAME_VIEW_SIMULATION_H
#define GAME__VIEW__I_GAME_VIEW_SIMULATION_H



namespace Game
{
	class IGameClientSimulation;



	class IGameViewSimulation
	{
	public:
		virtual ~IGameViewSimulation();

		void UpdateViewSimulation(float deltaTime) const;
		void BindToGameClientSimulation(IGameClientSimulation& gameClientSimulation);



	private:
		virtual void DoUpdateViewSimulation(float deltaTime) const = 0;
		virtual void DoBindToGameClientSimulation(IGameClientSimulation& gameClientSimulation) = 0;
		virtual void DoRegisterToClientSimulationEvents(IGameClientSimulation& gameClientSimulation) = 0;
	};
}



#endif

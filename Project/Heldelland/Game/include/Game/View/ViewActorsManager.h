//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__VIEW__VIEW_ACTORS_MANAGER_H
#define	GAME__VIEW__VIEW_ACTORS_MANAGER_H

#include<vector>
#include<functional>

#include<Nest/EntityId.h>

#include"Game/SimulationCommons/SimulationTypes.h"



namespace Game
{
	class ViewActorsManager
	{
	public:
		using MovingActorsContainer = std::vector<std::tuple<ActorNetworkId, Nest::EntityId>>;
		using MovingActorsConstIterator = MovingActorsContainer::const_iterator;


		~ViewActorsManager();

		void RegisterMovingActor(ActorNetworkId actorNetworkId, Nest::EntityId entityId);
		bool IsMovingActorRegistered(ActorNetworkId actorNetworkId) const;
		bool UnregisterMovingActorIfRegistered(ActorNetworkId actorNetworkId);

		void ExecuteForAllRegisteredMovingActors(
			std::function<void(MovingActorsConstIterator /*begin*/, MovingActorsConstIterator /*end*/)> action
		) const;



	private:
		MovingActorsContainer m_MovingActorsContainer;
	};
}



#endif


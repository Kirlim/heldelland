
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__VIEW__GAME_SIMULATION_VIEW_FACTORY_H
#define GAME__VIEW__GAME_SIMULATION_VIEW_FACTORY_H

#include<memory>

#include"Game/View/IGameViewSimulationFactory.h"



namespace Nest
{
	class EntitiesManager;
	class RenderingModule;
	class World;
}

namespace Game
{
	class IGameViewSimulation;
	class Map;
	class IMapViewFactory;



	class GameViewSimulationFactory : public IGameViewSimulationFactory
	{
	public:
		GameViewSimulationFactory(
			Nest::EntitiesManager& entitiesManager,
			Nest::RenderingModule& renderingModule,
			Nest::World& world,
			const IMapViewFactory& mapViewFactory
		);

		std::unique_ptr<IGameViewSimulation> Instantiate(Map& map) const override;



	private:
		void InstantiateShowCurrentTickValueEntity(const IGameViewSimulation& gameSimulationView) const;



	private:
		Nest::EntitiesManager& m_EntitiesManager;
		Nest::RenderingModule& m_RenderingModule;
		Nest::World& m_World;
		const IMapViewFactory& m_MapViewFactory;
	};
}



#endif

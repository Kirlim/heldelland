
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<string>
#include<vector>



// Nest forward declarations
namespace Nest
{
    enum class GameRenderer;
}



namespace Game
{
    class GameSettings
    {
    public:
        GameSettings();

        // Loads the game settings from a yaml file.
        void Load(std::string fileName);

        Nest::GameRenderer GetGameRenderer() const;
        void SetScreenResolution(int screendWidth, int screenHeight);
        int GetScreenWidth() const;
        int GetScreenHeight() const;
        const std::vector<std::string>* GetAssetsPaths() const;


    protected:
        void SetGameRenderer(Nest::GameRenderer gameRenderer);



    protected:
        Nest::GameRenderer m_GameRenderer;
        int m_ScreenWidth;
        int m_ScreenHeight;

        std::vector<std::string> m_AssetsPaths;
    };
}
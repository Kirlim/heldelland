//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__DEVELOPERS_CONSOLE_BEHAVIOR_H
#define GAME__DEVELOPERS_CONSOLE_BEHAVIOR_H

#include<vector>
#include<functional>

#include<Nest/InputEnumerators.h>
#include<Nest/Component.h>
#include<Nest/Vector3.h>

#include<Nest/TextMesh.h>
#include<Nest/ComponentReference.h>



namespace Game
{
	// Forward declarations
	class DevelopersConsoleSystem;



	class DevelopersConsoleBehavior : public Nest::Component
	{
		friend class DevelopersConsoleSystem;

	public:
		DevelopersConsoleBehavior();
		DevelopersConsoleBehavior(const DevelopersConsoleBehavior&) = delete;
		DevelopersConsoleBehavior(DevelopersConsoleBehavior&&) = default;

		~DevelopersConsoleBehavior() override;

		DevelopersConsoleBehavior& operator=(const DevelopersConsoleBehavior&) = delete;
		DevelopersConsoleBehavior& operator=(DevelopersConsoleBehavior&&) = default;



		static std::string GetTypeName() { return ("DevelopersConsoleBehavior"); }


		// Movement setup
		void SetShowPosition(Nest::Vector3f showPosition);
		void SetHidePosition(Nest::Vector3f hidePosition);

		Nest::Vector3f GetShowPosition() const;
		Nest::Vector3f GetHidePosition() const;


		// Related objects and components set up
		void SetCommandTextMesh(Nest::ComponentReference<Nest::TextMesh> textMesh);
		Nest::ComponentReference<Nest::TextMesh> GetCommandTextMesh() const;

		void SetBottomTextLinePosition(Nest::Vector3f position);
		void SetLinesDistance(float distance);

		Nest::Vector3f GetBottomTextLinePosition() const;
		float GetLinesDistance() const;

		void AddTextLines(int count);
		void RemoveTextLines(int count);


		// Text handling

		void ScrollLinesUp(int count);

		void WriteLine(std::string line);
		void ClearText();


		// Viewing handling
		bool IsShowing() const;
		void Show();
		void Hide();
		void ToggleVisibility();

		void SetToggleKeyboardKey(Nest::KeyCode keyCode);
		Nest::KeyCode GetToggleKeyboardKey() const;


		// Functinal handling
		void SetCommitFunction(std::function<void(std::string)> commitFunction);
		std::function<void(std::string)> GetCommitFunction() const;
		bool HasCommitFunction() const;

		void Commit() const;



	private:
		void OnCreate() override;
		void OnDestroy() override;


		// TODO : remove this method when we get access to prefabs
		Nest::ComponentReference<Nest::TextMesh> CreateTextLine() const;



	private:
		bool m_IsShowing;
		Nest::Vector3f m_ShowPosition;
		Nest::Vector3f m_HidePosition;
		Nest::KeyCode m_ToggleConsoleKeyboardKey = Nest::KeyCode::F1;

		std::shared_ptr<std::string> m_CommandString;
		Nest::ComponentReference<Nest::TextMesh> m_CommandTextMesh;

		std::vector<Nest::ComponentReference<Nest::TextMesh>> m_Lines;
		Nest::Vector3f m_BottomLinePosition;
		float m_LinesDistance;

		std::function<void(std::string)> m_CommitFunction;
	};
}



#endif

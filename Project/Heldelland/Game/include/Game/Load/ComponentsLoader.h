
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__LOAD__COMPONENTS_LOADER_H
#define GAME__LOAD__COMPONENTS_LOADER_H



#include<Nest/IComponentsFactory.h>
#include<Nest/IComponentsContainers.h>
#include<Nest/ArrayStoringFactory.h>
#include<Nest/SimpleStoringFactoryWrap.h>
#include<Nest/StoringFactoryComponentsContainer.h>
#include<Nest/PrefabComponentsFactory.h>

#include<Nest/NestAssert.h>



namespace Game
{
	namespace Load
	{
		template <typename ComponentType>
		auto InstantiateStoringFactory()
		{
			const auto typeId = Nest::ComponentTypeMap::GetInstance()->GetTypeId<ComponentType>();
			return std::make_unique<Nest::ArrayStoringFactory<ComponentType>>();
		}



		template <typename ComponentType>
		auto InstantiateStoringFactoryWrap(std::unique_ptr<Nest::StoringFactory<ComponentType>> storingFactory)
		{
			NestAssert(storingFactory != nullptr);

			const auto typeId = Nest::ComponentTypeMap::GetInstance()->GetTypeId<ComponentType>();

			auto factory = std::make_unique<Nest::SimpleStoringFactoryWrap<ComponentType>>(
				typeId, ComponentType::GetTypeName(), std::move(storingFactory)
			);

			return std::move(factory);
		}



		template <typename ComponentType>
		void LoadStoringFactoryWrapToComponentsFactory(
			Nest::IComponentsFactory& componentsFactory, 
			std::unique_ptr<Nest::SimpleStoringFactoryWrap<ComponentType>> factoryWrap
		)
		{
			NestAssert(factoryWrap != nullptr);

			const auto typeId = Nest::ComponentTypeMap::GetInstance()->GetTypeId<ComponentType>();
			componentsFactory.RegisterFactory(typeId, ComponentType::GetTypeName(), std::move(factoryWrap));
		}



		template <typename ComponentType>
		void RegisterStoringFactoryIntoComponentsContainers(
			Nest::IComponentsContainers& componentsContainers, 
			Nest::StoringFactory<ComponentType>& storingFactory
		)
		{
			const auto typeId = Nest::ComponentTypeMap::GetInstance()->GetTypeId<ComponentType>();
			auto container = std::make_unique<Nest::StoringFactoryComponentsContainer<ComponentType>>(&storingFactory);

			componentsContainers.RegisterSingleContainer(typeId, std::move(container));
		}



		// Helping method to create a factory for components
		template <typename ComponentType>
		void SetUpComponentFactory(
			Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers
		)
		{
			auto storingFactory = InstantiateStoringFactory<ComponentType>();
			auto storingFactoryAddress = storingFactory.get();

			auto storingFactoryWrap = InstantiateStoringFactoryWrap<ComponentType>(std::move(storingFactory));

			LoadStoringFactoryWrapToComponentsFactory(componentsFactory, std::move(storingFactoryWrap));
			RegisterStoringFactoryIntoComponentsContainers(componentsContainers, *storingFactoryAddress);
		}
	}
}



#endif

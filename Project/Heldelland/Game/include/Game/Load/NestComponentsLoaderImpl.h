
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__LOAD__NEST_COMPONENTS_LOADER_IMPL_H
#define GAME__LOAD__NEST_COMPONENTS_LOADER_IMPL_H



namespace Nest
{
    // Forward declarations
    class IComponentsFactory;
    class IComponentsContainers;
}



namespace Game
{
    class NestComponentsLoaderImpl
    {
    public:
        void Load(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers) const;
    };
}



#endif

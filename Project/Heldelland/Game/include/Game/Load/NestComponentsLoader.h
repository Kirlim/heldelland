
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__LOAD__NEST_COMPONENTS_LOADER_H
#define GAME__LOAD__NEST_COMPONENTS_LOADER_H

#include<memory>



namespace Nest
{
    // Forward declarations
    class IComponentsFactory;
    class IComponentsContainers;
}



namespace Game
{
    // Forward declarations
    class NestComponentsLoaderImpl;



    // Responsibility
    //     Load the Nest engine components
    class NestComponentsLoader
    {
    public:
        NestComponentsLoader();
        ~NestComponentsLoader();
        void Load(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers) const;



    protected:
        std::unique_ptr<NestComponentsLoaderImpl> m_Impl;
    };
}



#endif

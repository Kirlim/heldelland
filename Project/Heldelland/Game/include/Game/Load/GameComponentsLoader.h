
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__LOAD__GAME_COMPONENTS_LOADER_H
#define GAME__LOAD__GAME_COMPONENTS_LOADER_H

#include<memory>



namespace Nest
{
    // Forward declarations
    class IComponentsFactory;
    class IComponentsContainers;
}



namespace Game
{
    // Forward declarations
    class GameComponentsLoaderImpl;



    // Responsibility:
    //     Load Game Components for use, registering their factories.
    class GameComponentsLoader
    {
    public:
        GameComponentsLoader();
        ~GameComponentsLoader();

        void LoadServerSimulationComponents(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers) const;
        void LoadClientSimulationComponents(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers) const;
        void LoadSimulationViewsComponents(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers) const;



    protected:
        std::unique_ptr<GameComponentsLoaderImpl> m_Impl;
    };
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__HOST_H
#define GAME__NETWORK__HOST_H

#include<string>
#include<memory>
#include<functional>

#include"Game/Network/IHost.h"
#include"Game/Network/Endpoint.h"



namespace Game
{
	class IHostChannel;
	struct HostGameInformation;



	class Host : public IHost
	{
	public:
		Host(HostId localId, std::function<std::unique_ptr<IHostChannel>(IHost&)> channelFactoryFunction);
		~Host() override;

		HostId GetLocadId() const override;

		Endpoint GetEndpoint() const override;
		void SetEndpoint(Endpoint endpoint) override;

		void SetPlayerAlias(std::string alias) override;
		std::string GetPlayerAlias() const override;

		void SetGameInformation(std::unique_ptr<HostGameInformation>& hostGameInformation) override;
		HostGameInformation* GetGameInformation() const override;

		IHostChannel* GetFromHostChannel() const override;
		IHostChannel* GetToHostChannel() const override;



	protected:
		Endpoint m_Endpoint;
		const HostId m_LocalId;

		std::unique_ptr<IHostChannel> m_FromHostChannel;
		std::unique_ptr<IHostChannel> m_ToHostChannel;

		std::unique_ptr<HostGameInformation> m_GameInformation;

		std::string m_Alias;
	};
}



#endif

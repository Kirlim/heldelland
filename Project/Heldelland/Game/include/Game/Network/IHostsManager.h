
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__I_HOSTS_MANAGER_H
#define GAME__NETWORK__I_HOSTS_MANAGER_H

#include<memory>

#include"Game/Network/HostId.h"



namespace Game
{
	// Forward declarations
	class Endpoint;
	class IHost;



	class IHostsManagerIteratorImpl
	{
	public:
		virtual ~IHostsManagerIteratorImpl() {}
		virtual bool Equals(const IHostsManagerIteratorImpl& other) const = 0;
		virtual void Increment() = 0;
		virtual IHost& Dereference() const = 0;
	};



	class IHostsManagerIterator
	{
	public:
		// TODO : Safety checks on debug builds!
		IHostsManagerIterator(std::unique_ptr<IHostsManagerIteratorImpl> iteratorImpl) : m_Impl(std::move(iteratorImpl)) {}
		IHostsManagerIterator& operator++() { m_Impl->Increment(); return(*this); }
		bool operator !=(IHostsManagerIterator& other) const { return(!m_Impl->Equals(*other.m_Impl)); }
		IHost& operator*() const { return(m_Impl->Dereference()); }


	private:
		std::unique_ptr<IHostsManagerIteratorImpl> m_Impl;
	};



	class IHostsManager
	{
	public:
		virtual ~IHostsManager() {}

		virtual bool HostExists(HostId localId) const = 0;
		virtual bool HostExists(Endpoint endpoint) const = 0;

		virtual std::weak_ptr<IHost> GetHost(HostId localId) const = 0;
		virtual std::weak_ptr<IHost> GetHost(Endpoint endpoint) const = 0;

		virtual std::weak_ptr<IHost> InstantiateNewHost(Endpoint endpoint) = 0;


		virtual IHostsManagerIterator begin() const = 0;
		virtual IHostsManagerIterator end() const = 0;



	public:
		static const HostId LocalHostId{ 0 };

	};
}



#endif

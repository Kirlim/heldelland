//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__NETWORK_MESSAGES_HANDLERS_MANAGER_H
#define GAME__NETWORK__NETWORK_MESSAGES_HANDLERS_MANAGER_H

#include<unordered_map>

#include"Game/Network/INetworkMessagesHandlersManager.h"
#include"Game/Network/INetworkMessagesHandler.h"



namespace Game
{
	// Forward declarations
	class INetworkMessagesHandler;
	class NetworkMessage;



	class NetworkMessagesHandlersManager : public INetworkMessagesHandlersManager
	{
	public:
		~NetworkMessagesHandlersManager() override;

		void AddHandler(std::unique_ptr<INetworkMessagesHandler> handler) override;
		void RemoveHandler(NetworkMessageTypeId messageTypeId) override;
		INetworkMessagesHandler* GetHandler(NetworkMessageTypeId messageTypeId) const override;
		INetworkMessagesHandler* GetHandler(NetworkMessage& networkMessage) const override;

		std::vector<NetworkMessageTypeId> GetAllRegisteredNetworkMessageTypeIds() const override;



	protected:
		std::unordered_map<NetworkMessageTypeId, std::unique_ptr<INetworkMessagesHandler>> m_Handlers;
	};
}



#endif

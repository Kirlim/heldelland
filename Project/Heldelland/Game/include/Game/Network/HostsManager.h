
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__HOSTS_MANAGER_H
#define GAME__NETWORK__HOSTS_MANAGER_H

#include<memory>
#include<vector>

#include"Game/Network/IHostsManager.h"



namespace Game
{
	// Forward declarations
	class IHost;
	class Endpoint;



	class HostsManagerIteratorImpl final : public IHostsManagerIteratorImpl
	{
	public:
		explicit HostsManagerIteratorImpl(std::vector<std::shared_ptr<IHost>>::const_iterator iterator);
		~HostsManagerIteratorImpl() override;

		bool Equals(const IHostsManagerIteratorImpl& other) const override;
		void Increment() override;
		IHost& Dereference() const override;



	protected:
		std::vector<std::shared_ptr<IHost>>::const_iterator m_Iterator;
	};



	class HostsManager final : public IHostsManager
	{
	public:
		HostsManager();
		~HostsManager() override;

		bool HostExists(HostId localId) const override;
		bool HostExists(Endpoint endpoint) const override;

		std::weak_ptr<IHost> GetHost(HostId localId) const override;
		std::weak_ptr<IHost> GetHost(Endpoint endpoint) const override;

		std::weak_ptr<IHost> InstantiateNewHost(Endpoint endpoint) override;

		IHostsManagerIterator begin() const override;
		IHostsManagerIterator end() const override;



	private:
		static HostId GetNextLocalId();

		std::shared_ptr<IHost> CreateNewHostInstance(HostId hostId, Endpoint endpoint) const;



	private:
		std::vector<std::shared_ptr<IHost>> m_Hosts;
	};
}



#endif

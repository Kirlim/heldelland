
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__INETWORK_SYSTEM_H
#define GAME__NETWORK__INETWORK_SYSTEM_H

#include<string>
#include<functional>



namespace Game
{
	// Forward declarations
	class INetworkMessagesFactory;
	class INetworkMessagesHandlersManager;
	class IHostChannel;
	class IHostsChannels;
	class IHost;
	class IHostsManager;



	// Used to know wether the network system is set to be a Server or
	// a Client.
	enum class NetworkSide { Server, Client };



	// High Coesion:
	//    Have ease of access for networking facilities by storing
	//    the main network classes, and relaying overall method calls
	//    to the correct network managing components.
	class INetworkSystem
	{
	public:
		virtual ~INetworkSystem() {}

		virtual void Tick() = 0;
		virtual NetworkSide GetNetworkSide() const = 0;

		virtual bool Start(std::string listenIp, unsigned int listenPort, NetworkSide networkSide) = 0;
		virtual void Stop() = 0;

		virtual void ConnectAsync(std::string destinationIp, unsigned int destinationPort) = 0;
		virtual void SetLocalInstanceServerFlag(bool hasLocalInstanceServer) = 0;
		virtual bool IsConnectedToServer() const = 0;

		// Will call the passed @function with the "from server host channel" (first parameter)
		// and the "to server host channel" (second parameter).
		virtual void ExecuteWithServerHostsChannels(std::function<void(IHostChannel&, IHostChannel&)> function) = 0;

		// Will call the passed @function for each known connected client (first parameter). The
		// second parameter is the "from client host channel", and the third parameter is
		// the "to client host channel".
		virtual void ExecuteWithClientHostsChannel(std::function<void(IHost&, IHostChannel&, IHostChannel&)> function) = 0;

		virtual IHostsManager* GetHostsManager() const = 0;
		virtual INetworkMessagesFactory* GetMessagesFactory() const = 0;
		virtual INetworkMessagesHandlersManager* GetMessagesHandlersManager() const = 0;
	};
}



#endif

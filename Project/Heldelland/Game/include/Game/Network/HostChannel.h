//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__HOST_CHANNEL_H
#define GAME__NETWORK__HOST_CHANNEL_H

#include<queue>
#include<memory>
#include<mutex>

#include"Game/Network/IHostChannel.h"



namespace Game
{
	// Forward declarations
	class NetworkMessage;
	class IHost;



	class HostChannel final : public IHostChannel
	{
	public:
		explicit HostChannel(IHost& host);
		~HostChannel() override;

		IHost* GetHost() const override final;

		bool AddMessage(std::shared_ptr<NetworkMessage> message) override final;

		NetworkMessage* PeekNextMessage() const override final;
		std::shared_ptr<NetworkMessage> FetchNextMessage() override final;

		unsigned int Size() const override final;

		bool IsValid() const override final;



	protected:
		IHost& m_Host;
		std::queue<std::shared_ptr<NetworkMessage>> m_MessagesQueue;
		mutable std::mutex m_QueueMutex;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<functional>
#include<memory>

#include"Game/Network/INetworkMessagesReceiver.h"



namespace Game
{
    // Forward declarations
    class INetworkMessagesFactory;
    class INetworkReadSerializer;



    class NetworkMessagesReceiver : public INetworkMessagesReceiver
    {
    public:
        NetworkMessagesReceiver(
            INetworkMessagesFactory& networkMessagesFactory,
            std::function<std::unique_ptr<INetworkReadSerializer>(unsigned char*, const unsigned int)> readerSerializerFactory
        );
        virtual ~NetworkMessagesReceiver();

        virtual void Receive(IHostChannel& channel, const unsigned char* dataBuffer, const unsigned int dataStartBit, const unsigned int bufferSizeInBytes) const override;



    protected:
        INetworkMessagesFactory& m_NetworkMessagesFactory;

        std::function<std::unique_ptr<INetworkReadSerializer>(unsigned char*, const unsigned int)> m_ReaderSerializerFactoryFunction;
    };
}

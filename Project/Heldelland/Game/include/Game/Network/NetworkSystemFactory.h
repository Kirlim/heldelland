
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<memory>

#include"Game/Network/NetworkSystemFactoryImpl.h"



namespace Game
{
    // Forward declarations
    class INetworkSystem;
    class NetworkSystemFactoryImpl;



    // Responsibility:
    //    Instantiate a network system with it's basic dependencies.
    class NetworkSystemFactory
    {
    public:
        NetworkSystemFactory();
        std::unique_ptr<INetworkSystem> Instantiate(const unsigned int sendReceiveSerializersBuffersSizeInBytes) const;



    protected:
        std::unique_ptr<NetworkSystemFactoryImpl> m_FactoryImplementation;
    };
}

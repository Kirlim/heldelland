//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__CLIENT_NETWORK_CONTROLLER_H
#define GAME__NETWORK__CLIENT_NETWORK_CONTROLLER_H

#include"Game/Network/IClientNetworkController.h"



namespace Game
{
	// Forward declarations
	class IHostChannel;
	class INetworkMessagesFactory;
	class INetworkMessagesHandlersManager;



	class ClientNetworkController : public IClientNetworkController
	{
	public:
		enum class GameConnectionStatus { NotConnected, Connecting, Connected };



	public:
		ClientNetworkController(
			const INetworkMessagesFactory& networkMessageFactory,
			const INetworkMessagesHandlersManager& networkMessagesHandlersManager
		);
		virtual ~ClientNetworkController() = default;

		void Tick(IHostChannel& receiveFromServerChannel, IHostChannel& sendToServerChannel) override;



	protected:
		const INetworkMessagesFactory& m_NetworkMessagesFactory;
		const INetworkMessagesHandlersManager& m_NetworkMessagesHandlersManager;
		GameConnectionStatus m_ConnectionStatus;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once



namespace Game
{
    // Forward declarations
    class IHostChannel;



    class IClientNetworkController
    {
    public:
        virtual ~IClientNetworkController() {}

        virtual void Tick(IHostChannel& receiveFromServerChannel, IHostChannel& sendToServerChannel) = 0;
    };
}

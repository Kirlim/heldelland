
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once



namespace Game
{
    // Forward declarations
    class IHostChannel;



    // Responsibility:
    //     Unpack netwokr messages from a data buffer and store them in the
    //     corresponding host channel.
    class INetworkMessagesReceiver
    {
    public:
        virtual ~INetworkMessagesReceiver() {};

        // Will read messages from the @dataBuffer and add them to the specified @channel,
        // even if the host channel is expired. Will do nothing if @dataBuffer is nullptr,
        // or @dataStartBits is bigger than the converted to bits size @bufferSizeInBytes.
        virtual void Receive(IHostChannel& channel, const unsigned char* dataBuffer, const unsigned int dataStartBit, const unsigned int bufferSizeInBytes) const = 0;
    };
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__I_NETWORK_MESSAGES_FACTORY_H
#define GAME__NETWORK__I_NETWORK_MESSAGES_FACTORY_H

#include<functional>
#include<memory>

#include"Game/Network/NetworkMessageId.h"



namespace Game
{
	// Forward declarations
	class NetworkMessage;



	// Responsibility:
	//     Return instances of network messages by their id.
	class INetworkMessagesFactory
	{
	public:
		virtual ~INetworkMessagesFactory() {};

		// Registers a new factory function, relating it to a type id.
		// Will return true on success, or false if the id is already registered, or
		// the factory function is null.
		virtual bool Register(NetworkMessageTypeId id, std::function<std::shared_ptr<NetworkMessage>()> factoryFunction) = 0;

		// Returns a new NetworkMessage instance, by using the type id. Will return an empty
		// smart pointer if the id is not registered.
		virtual std::shared_ptr<NetworkMessage> InstantiateMessage(NetworkMessageTypeId	id) const = 0;

		// Returns a vector with all currently NetworkMessageTypeId registered with factories
		virtual std::vector<NetworkMessageTypeId> GetAllRegisteredNetworkMessageTypeIds() const = 0;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__INETWORK_MESSAGES_HANDLER_H
#define GAME__NETWORK__INETWORK_MESSAGES_HANDLER_H

// TODO : consider passing network message type id to another file, to reduce include hell
#include"Game/Network/NetworkMessage.h"



namespace Game
{
    // Forward declarations
    class NetworkMessage;
    class IHost;



    // Responsibility:
    //    Base class of network messages handlers, that are to be used to
    //    execute logic based on a network message data. A handler is supposed
    //    to handle a single message type.
    class INetworkMessagesHandler
    {
    public:
        virtual ~INetworkMessagesHandler() {};

        // Executes the handler logic to the given message
        virtual void Execute(IHost& remoteHost, std::shared_ptr<NetworkMessage>& message) = 0;

        // Returns the id of the message type that this handler
        // works with. Expected to be exactly the same value.
        virtual NetworkMessageTypeId GetMessageTypeId() const = 0;
    };
}

#endif

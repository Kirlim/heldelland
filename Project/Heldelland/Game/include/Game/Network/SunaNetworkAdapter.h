
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<functional>
#include<memory>

#include"Game/Network/INetworkAdapter.h"



namespace Suna
{
    // Forward declarations
    class LocalHost;
    class RemoteHost;
}



namespace Game
{
    class SunaNetworkAdapter : public INetworkAdapter
    {
    public:
        SunaNetworkAdapter();
        virtual ~SunaNetworkAdapter();

        virtual void Tick() override final;

        virtual bool Start(const std::string listenIp, const unsigned int listenPort) override final;
        virtual void Stop() override final;

        virtual bool IsRunning() const override final;

        virtual void ConnectTo(const std::string ip, const unsigned int port) override final;

        virtual void SetIncomingConnectionListener(const std::function<bool(std::string, unsigned int)> listener) override final;
        virtual void SetOnConnectedListener(std::function<void(const std::string, const unsigned int)> event) override final;
        virtual void SetOnConnectionRequestRefusedListener(std::function<void(const std::string, const unsigned int)> event) override final;
        virtual void SetOnConnectionRequestTimedOutListener(std::function<void(const std::string, const unsigned int)> event) override final;
        virtual void SetOnNetworkMessageReceived(std::function<void(const std::string, const unsigned int, const unsigned char*, const unsigned int, const unsigned int)> event) override final;

        virtual void SendDataAsUnreliable(const std::string ip, const unsigned int port,const unsigned char* data, const unsigned int sizeInBits) override final;
        virtual void SendDataAsReliable(const std::string ip, const unsigned int port,const unsigned char* data, const unsigned int sizeInBits) override final;



    public:
        const unsigned int UnreliableChannelId = 1;
        const unsigned int ReliableChannelId = 2;
        const unsigned int ReliableSeqBits = 8; // TODO : make this bigger
        const unsigned int ReliableRedundantAcksBits = 32;




    protected:
        void RegisterChannels(Suna::RemoteHost& remoteHost);

        bool OnIncomingConnectionRequestEvent(const std::string ip, const unsigned int port);
        void OnConnectedEvent(const int hostId);
        void OnRemoteHostConnectionRefusedEvent(const int hostId);
        void OnRemoteHostConnectionTimeoutEvent(const int hostId);
        void OnNetworkMessageReceivedEvent(const Suna::RemoteHost* remoteHost, const unsigned int channelId);



    protected:
        std::unique_ptr<Suna::LocalHost> m_LocalHost;

        std::function<void(const std::string, const unsigned int)> m_OnConnectedEvent;
        std::function<void(const std::string, const unsigned int)> m_OnConnectionRequestRefusedEvent;
        std::function<void(const std::string, const unsigned int)> m_OnConnectionRequestTimedOutEvent;
        std::function<void(const std::string, const unsigned int, const unsigned char*, const unsigned int, const unsigned int)> m_OnNetworkMessageReceivedEvent;


        // TODO : delete the next variable when Suna receives the IsRunningThread method in the LocalHost
        bool m_IsRunning = false;
    };
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__MESSAGES__GENERIC_MESSAGES_HANDLER_H
#define GAME__NETWORK__MESSAGES__GENERIC_MESSAGES_HANDLER_H

#include<memory>

#include<Nest/Events/EventsBus.h>

#include"Game/Network/INetworkMessagesHandler.h"



namespace Game
{
	// Forward declarations
	class IHost;
	class NetworkMessage;



	// Responsibility:
	//    Provide a template for generic handling of network messages and
	//    simulation commands.
	template <typename MessageType>
	class GenericMessagesHandler : public INetworkMessagesHandler
	{
	public:
		explicit GenericMessagesHandler(NetworkMessageTypeId typeId);
		~GenericMessagesHandler() override;

		void Execute(IHost& remoteHost, std::shared_ptr<NetworkMessage>& message) override;

		NetworkMessageTypeId GetMessageTypeId() const override;

		Nest::EventsBus<std::shared_ptr<MessageType>&>& GetEventsBus() const;



	protected:
		std::unique_ptr<Nest::EventsBus<std::shared_ptr<MessageType>&>> m_EventsBus;
		const NetworkMessageTypeId m_TypeId;
	};



	template <typename MessageType>
	GenericMessagesHandler<MessageType>::GenericMessagesHandler(NetworkMessageTypeId typeId) :
		INetworkMessagesHandler(),
		m_EventsBus(std::make_unique<Nest::EventsBus<std::shared_ptr<MessageType>&>>()),
		m_TypeId{typeId.value}
	{
		// Nothing here
	}



	template <typename MessageType>
	GenericMessagesHandler<MessageType>::~GenericMessagesHandler()
	{
		// Nothing here
	}



	template <typename MessageType>
	void GenericMessagesHandler<MessageType>::Execute(IHost& remoteHost, std::shared_ptr<NetworkMessage>& message)
	{
		auto castedMessage = std::dynamic_pointer_cast<MessageType>(message);
		m_EventsBus->Signal(castedMessage);
	}



	template <typename MessageType>
	NetworkMessageTypeId GenericMessagesHandler<MessageType>::GetMessageTypeId() const
	{
		return m_TypeId;
	}



	template <typename MessageType>
	Nest::EventsBus<std::shared_ptr<MessageType>&>& GenericMessagesHandler<MessageType>::GetEventsBus() const
	{
		return *m_EventsBus;
	}
}

#endif

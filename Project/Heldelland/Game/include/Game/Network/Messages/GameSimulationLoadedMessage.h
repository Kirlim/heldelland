
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__MESSAGES__GAME_SIMULATION_LOADED_MESSAGE_H
#define GAME__NETWORK__MESSAGES__GAME_SIMULATION_LOADED_MESSAGE_H

#include"Game/Network/NetworkMessage.h"



namespace Game
{
    // Forward declarations
    class INetworkSerializer;



    // Responsibility:
    //    This message is sent from the client to the server to flag that
    //    the client has successfully loaded the basics of a simulation
    //    (i.e map), and that the server can start syncing up the game.
    class GameSimulationLoadedMessage : public NetworkMessage
    {
    public:
        GameSimulationLoadedMessage();
        ~GameSimulationLoadedMessage() override;

        NetworkMessageTypeId GetTypeId() const override;



    protected:
        bool DoSerialize(INetworkSerializer& serializer) override;



    public:
        static const NetworkMessageTypeId TypeId;
    };
}

#endif

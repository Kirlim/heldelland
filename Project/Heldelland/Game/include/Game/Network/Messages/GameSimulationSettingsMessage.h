
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__MESSAGES__GAME_SIMULATION_SETTINGS_MESSAGE_H
#define GAME__NETWORK__MESSAGES__GAME_SIMULATION_SETTINGS_MESSAGE_H

#include<string>

#include"Game/Network/NetworkMessage.h"



namespace Game
{
    class GameSimulationSettingsMessage : public NetworkMessage
    {
    public:
        GameSimulationSettingsMessage();
        ~GameSimulationSettingsMessage() override;

        NetworkMessageTypeId GetTypeId() const override;

        void SetMapName(std::string mapName);
        std::string GetMapName() const;



    protected:
        bool DoSerialize(INetworkSerializer& serializer) override;



    public:
        static const NetworkMessageTypeId TypeId;



    protected:
        std::string m_MapName;
    };
}


#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__MESSAGES__JOIN_GAME_REQUEST_MESSAGE_H
#define GAME__NETWORK__MESSAGES__JOIN_GAME_REQUEST_MESSAGE_H

#include"Game/Network/NetworkMessage.h"



namespace Game
{
    // Forward declarations
    class INetworkSerializer;



    // Responsibility:
    //    Send enough data to request to join the current game
    //    running in the server.
    class JoinGameRequestMessage : public NetworkMessage
    {
    public:
        JoinGameRequestMessage();
        ~JoinGameRequestMessage() override;

        NetworkMessageTypeId GetTypeId() const override;



    protected:
        bool DoSerialize(INetworkSerializer& serializer) override;



    public:
        static const NetworkMessageTypeId TypeId;
    };
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__MESAGES__JOIN_GAME_REPLY_MESSAGE_H
#define GAME__NETWORK__MESAGES__JOIN_GAME_REPLY_MESSAGE_H

#include"Game/Network/NetworkMessage.h"



namespace Game
{
    class JoinGameReplyMessage : public NetworkMessage
    {
    public:
        enum class Reply : unsigned char { Undefined = 0, Accepted = 1, Refused = 2};

    public:
        JoinGameReplyMessage();
        ~JoinGameReplyMessage() override;

        NetworkMessageTypeId GetTypeId() const override;

        void SetReply(Reply reply);
        Reply GetReply() const;



    protected:
        bool DoSerialize(INetworkSerializer& serializer) override;



    protected:
        Reply m_Reply;



    public:
        static const NetworkMessageTypeId TypeId;
    };
}

#endif
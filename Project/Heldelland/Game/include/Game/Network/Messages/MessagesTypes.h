
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__NETWORK_MESSAGES__MESSAGES_TYPES_H
#define GAME__NETWORK__NETWORK_MESSAGES__MESSAGES_TYPES_H



#define JOIN_GAME_REQUEST_NETWORK_MESSAGE_ID			1
#define JOIN_GAME_REPLY_NETWORK_MESSAGE_ID				2
#define GAME_SIMULATION_SETTINGS_NETWORK_MESSAGE_ID		3
#define GAME_SIMULATION_LOADED_NETWORK_MESSAGE_ID		4



#endif
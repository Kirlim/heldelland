//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__NETWORK_MESSAGE_ID_H
#define GAME__NETWORK__NETWORK_MESSAGE_ID_H

#include"Nest/IdType.h"



namespace Game
{
	struct NetworkTypeIdTraits
	{
		using underlying_type = unsigned char;
	};

	using NetworkMessageTypeId = Nest::IdType<NetworkTypeIdTraits>;



	struct NetworkMessageIdTraits
	{
		using underlying_type = unsigned char;
	};

	using NetworkMessageId = Nest::IdType<NetworkMessageIdTraits>;
}



#endif

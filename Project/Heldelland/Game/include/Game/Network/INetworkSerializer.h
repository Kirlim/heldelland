
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<cinttypes>



namespace Game
{
    class INetworkSerializer
    {
    public:
        virtual ~INetworkSerializer() {}

        // Will set the next operation starting bit to @position.
        // Must return true if the position is valid and has been set,
        // or false otherwise.
        virtual bool SetBitPointer(unsigned int position) = 0;
        virtual unsigned int GetBitPointer() const = 0;


        // The serializations methods will read or write a value,
        // according to the method and the implementation. These
        // methods will serialize at the current bit position, and will
        // update its position with the amount of bits used.
        //
        // True must be returned if the operation succeeds, or false
        // otherwise (e.g won't fit into the buffer). The result of an
        // unsuccessful serialization is undefined, but must not affect
        // bits before the bit position pointer at the start of the failed
        // operation.

        virtual bool SerializeBool(bool& value) = 0;
        virtual bool SerializeChar(char& value, int bits = 8) = 0;
        virtual bool SerializeUChar(unsigned char& value, int bits = 8) = 0;
        virtual bool SerializeInt16(int16_t& value, int bits = 16) = 0;
        virtual bool SerializeUInt16(uint16_t& value, int bits = 16) = 0;
        virtual bool SerializeInt32(int32_t& value, int bits = 32) = 0;
        virtual bool SerializeUInt32(uint32_t& value, int bits = 32) = 0;
        virtual bool SerializeInt64(int64_t& value, int bits = 64) = 0;
        virtual bool SerializeUInt64(uint64_t& value, int bits = 64) = 0;
    };
}

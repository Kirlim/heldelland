
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include"Game/Network/INetworkReadSerializer.h"
#include"Game/Network/SunaNetworkSerializer.h"



namespace Game
{
    class SunaNetworkReadSerializer : public INetworkReadSerializer, public SunaNetworkSerializer
    {
    public:
        SunaNetworkReadSerializer(unsigned char* buffer, const unsigned int bitStartPosition, const unsigned int sizeInBytes);
        virtual ~SunaNetworkReadSerializer();

        virtual bool SetBitPointer(unsigned int position) override;
        virtual unsigned int GetBitPointer() const override;
        virtual bool SerializeBool(bool & value) override;
        virtual bool SerializeChar(char & value, int bits = 8) override;
        virtual bool SerializeUChar(unsigned char & value, int bits = 8) override;
        virtual bool SerializeInt16(int16_t & value, int bits = 16) override;
        virtual bool SerializeUInt16(uint16_t & value, int bits = 16) override;
        virtual bool SerializeInt32(int32_t & value, int bits = 32) override;
        virtual bool SerializeUInt32(uint32_t & value, int bits = 32) override;
        virtual bool SerializeInt64(int64_t & value, int bits = 64) override;
        virtual bool SerializeUInt64(uint64_t & value, int bits = 64) override;
    };
}

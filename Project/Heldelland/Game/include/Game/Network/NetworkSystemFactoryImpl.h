//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__NETWORK_SYSTEM_FACTORY_IMPL_H
#define GAME__NETWORK__NETWORK_SYSTEM_FACTORY_IMPL_H

#include<memory>



namespace Game
{
	// Forward declarations
	class INetworkSystem;
	class INetworkMessagesFactory;
	class INetworkMessagesHandlersManager;



	class NetworkSystemFactoryImpl
	{
	public:
		std::unique_ptr<INetworkSystem> Instantiate(const unsigned int sendReceiveSerializersBuffersSizeInBytes) const;



	protected:
		void RegisterNetworkMessagesToFactory(INetworkMessagesFactory& factory) const;
		void RegisterNetworkMessagesHandlersToFactory(INetworkMessagesHandlersManager& manager) const;
	};
}



#endif

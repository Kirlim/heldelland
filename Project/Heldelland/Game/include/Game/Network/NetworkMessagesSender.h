
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<memory>
#include<functional>

#include"Game/Network/INetworkMessagesSender.h"



namespace Game
{
    // Forward declarations
    class IHost;
    class IHostChannel;
    class INetworkAdapter;
    class NetworkMessage;
    class INetworkWriteSerializer;



    class NetworkMessagesSender : public INetworkMessagesSender
    {
    public:
        NetworkMessagesSender(
            const unsigned int dataBufferSize,
            INetworkAdapter* const networkAdapter,
            std::function<std::unique_ptr<INetworkWriteSerializer>(unsigned char*, const unsigned int)> writerSerializerFactory
        );
        virtual ~NetworkMessagesSender();

        // Thread unsafe: ensure that, at most, only this instance
        // is dequeueing messages from the channel!
        virtual void Send(IHostChannel& channel) override final;



    protected:
        bool DoesFit(NetworkMessage* const message) const;
        void WriteToBuffer(NetworkMessage* const message) const;
        void SendDataToNetworkAdapter(const IHost* host) const;



    protected:
        INetworkAdapter* const m_NetworkAdapter;

        const unsigned int m_DataBufferSize;
        std::unique_ptr<unsigned char[]> m_ReliableBuffer;
        std::unique_ptr<unsigned char[]> m_UnreliableBuffer;

        std::unique_ptr<INetworkWriteSerializer> m_ReliableWriteSerializer;
        std::unique_ptr<INetworkWriteSerializer> m_UnreliableWriteSerializer;
    };
}

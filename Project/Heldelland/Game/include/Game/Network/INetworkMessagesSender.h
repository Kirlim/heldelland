
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once



namespace Game
{
    // Forward declarations
    class IHostChannel;



    // Responsibility:
    //    Pick up INetworkMessages from IHostChannel and put it in a
    //    INetworkAdapter for sending.
    class INetworkMessagesSender
    {
    public:
        virtual ~INetworkMessagesSender() {}
        virtual void Send(IHostChannel& channel) = 0;
    };
}

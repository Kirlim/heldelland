//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__NETWORK_SYSTEM_H
#define GAME__NETWORK__NETWORK_SYSTEM_H

#include<memory>
#include<functional>

#include"Game/Network/INetworkSystem.h"



namespace Game
{
	// Forward declarations
	class INetworkAdapter;
	class INetworkMessagesFactory;
	class INetworkMessagesHandlersManager;
	class INetworkMessagesSender;
	class INetworkMessagesReceiver;
	class IHostsManager;



	class NetworkSystem final : public INetworkSystem
	{
	public:
		NetworkSystem(
			std::unique_ptr<INetworkAdapter> networkAdapter,
			std::unique_ptr<INetworkMessagesFactory> networkMessagesFactory, std::unique_ptr<INetworkMessagesHandlersManager> networkMessagesHandlersManager,
			std::unique_ptr<IHostsManager> hostsManager,
			std::unique_ptr<INetworkMessagesSender> networkMessagesSender, std::unique_ptr<INetworkMessagesReceiver> networkMessagesReceiver
		);
		~NetworkSystem() override;

		void Tick() override;
		NetworkSide GetNetworkSide() const override;

		bool Start(std::string listenIp, unsigned int listenPort, NetworkSide networkSide) override;
		void Stop() override;

		void ConnectAsync(std::string destinationIp, unsigned int destinationPort) override;
		void SetLocalInstanceServerFlag(bool hasLocalInstanceServer) override;
		bool IsConnectedToServer() const override;

		void ExecuteWithServerHostsChannels(std::function<void(IHostChannel&, IHostChannel&)> function) override;
		void ExecuteWithClientHostsChannel(std::function<void(IHost&, IHostChannel&, IHostChannel&)> function) override;

		IHostsManager* GetHostsManager() const override;
		INetworkMessagesFactory* GetMessagesFactory() const override;
		INetworkMessagesHandlersManager* GetMessagesHandlersManager() const override;



	protected:
		bool ValidateThatMessagesFactoryAndHandlerKnowsTheSameTypes() const;

		bool OnIncomingConnection(std::string ip, unsigned int port);
		void OnConnected(std::string ip, unsigned int port);
		void OnNetworkMessageReceived(
			std::string ip, unsigned int port,
			const unsigned char* dataBuffer, unsigned int dataStartBit, unsigned int bufferSizeInBytes
		);

		void TickMessagesReceive();
		void TickMessagesSend();



	protected:
		std::unique_ptr<INetworkAdapter> m_NetworkAdapter;
		std::unique_ptr<INetworkMessagesFactory> m_NetworkMessagesFactory;
		std::unique_ptr<INetworkMessagesHandlersManager> m_NetworkMessagesHandlersManager;
		std::unique_ptr<IHostsManager> m_HostsManager;
		std::unique_ptr<INetworkMessagesSender> m_NetworkMessagesSender;
		std::unique_ptr<INetworkMessagesReceiver> m_NetworkMessagesReceiver;

		bool m_IsConnectedToRemoteServer = false;
		bool m_HasLocalInstanceServer = false;
		unsigned int m_ServerId = 0;

		NetworkSide m_NetworkSide;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include"Game/Network/INetworkSerializer.h"



namespace Game
{
    class NetworkCountSerializer : public INetworkSerializer
    {
    public:
        NetworkCountSerializer();
        virtual ~NetworkCountSerializer();

        virtual bool SetBitPointer(unsigned int position) override final;
        virtual unsigned int GetBitPointer() const override final;

        virtual bool SerializeBool(bool& value) override final;
        virtual bool SerializeChar(char& value, int bits = 8) override final;
        virtual bool SerializeUChar(unsigned char& value, int bits = 8) override final;
        virtual bool SerializeInt16(int16_t& value, int bits = 16) override final;
        virtual bool SerializeUInt16(uint16_t& value, int bits = 16) override final;
        virtual bool SerializeInt32(int32_t& value, int bits = 32) override final;
        virtual bool SerializeUInt32(uint32_t& value, int bits = 32) override final;
        virtual bool SerializeInt64(int64_t& value, int bits = 64) override final;
        virtual bool SerializeUInt64(uint64_t& value, int bits = 64) override final;



    protected:
        unsigned int m_BitPointerPosition;
    };
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__I_NETWORK_MESSAGES_HANDLERS_MANAGER_H
#define GAME__NETWORK__I_NETWORK_MESSAGES_HANDLERS_MANAGER_H

#include<memory>
#include<vector>

#include"Game/Network/NetworkMessageId.h"



namespace Game
{
	// Forward declarations
	class INetworkMessagesHandler;
	class NetworkMessage;



	// Responsibility:
	//     Store and own instances of messages handlers that'll be used
	//     to interpret read network messages and apply some logic based
	//     on them.
	class INetworkMessagesHandlersManager
	{
	public:
		virtual ~INetworkMessagesHandlersManager()
		{
		};

		// TODO : receive handler ptr by reference, so it wont be lost in case of error
		// Adds a new handler to this instance. Must throw std::logic_error if @handler
		// is null or of an already registered handler type id
		virtual void AddHandler(std::unique_ptr<INetworkMessagesHandler> handler) = 0;

		// Unregister and unload the handler responsible to work with the messages
		// of @messageTypeId. If the handler is not registered, this method does nothing.
		virtual void RemoveHandler(NetworkMessageTypeId messageTypeId) = 0;

		// Returns an unsafe pointer to handler responsible to work with the messages
		// of @messageTypeId. Ensuring that the handler will outlive the caller is
		// let to the programmer. Will return nullptr if the handler was not found.
		virtual INetworkMessagesHandler* GetHandler(NetworkMessageTypeId messageTypeId) const = 0;

		// Shortcut to GetHandler(unsigned int), by using the @networkMessage id.
		virtual INetworkMessagesHandler* GetHandler(NetworkMessage& networkMessage) const = 0;

		// Returns a vector with all currently NetworkMessageTypeId registered by
		// means of the registered INetworkMessagesHandler.
		virtual std::vector<NetworkMessageTypeId> GetAllRegisteredNetworkMessageTypeIds() const = 0;
	};
}



#endif

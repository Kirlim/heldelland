//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__NETWORK_MESSAGE_H
#define GAME__NETWORK__NETWORK_MESSAGE_H

#include"Game/Network/NetworkMessageId.h"



namespace Game
{
	// Forward declarations
	class INetworkSerializer;
	class IHost;



	enum class NetworkMessageReliabilityType
	{
		Unreliable,
		Reliable
	};



	class NetworkMessage
	{
	public:
		explicit NetworkMessage(NetworkMessageReliabilityType type);
		virtual ~NetworkMessage();

		NetworkMessageReliabilityType GetReliabilityType() const;
		NetworkMessageId GetId() const;

		// Will use the @serializer to write or read data, by calling the abstract
		// DoSerialize method with it. Will use the GetBitPositionPointer() of the
		// serializer before and after DoSerialize, in order to return the amount
		// of bits written in the @outBitsCount parameter. Will return success or failure,
		// accordingly to the DoSerialize call.
		bool Serialize(INetworkSerializer& serializer, unsigned int& outBitsCount);

		// Will use the @serializer to write or read data, by calling the abstract
		// DoSerialize method with it. Will return success of failure,
		// accordingly to the DoSerialize call.
		bool Serialize(INetworkSerializer& serializer);

		// Should return the size of the class when serialized. Default
		// behavior is to call Serialize with a counting serializer. Must
		// return true on success or false in failure. The bits count is in
		// the @outBitsCount parameter.
		// This method must take into account the size of the type identifier
		// of the messae, which must also be serialized (as the first data)
		virtual bool GetBitsSize(unsigned int& outBitsCount);

		// Should store a weak reference to the sender remote host, if
		// this is a received message.
		IHost* GetRemoteHost() const;
		void SetRemoteHost(IHost* remoteHost);


		virtual NetworkMessageTypeId GetTypeId() const = 0;



	public:
		static const int TypeIdBitsSize = 8;



	private:
		// It is expected that the serialization will write/read the
		// type identifier of the message, with the correct amount of bits.
		virtual bool DoSerialize(INetworkSerializer& serializer) = 0;
		static NetworkMessageId MakeNewId();



	private:
		NetworkMessageId m_Id;
		NetworkMessageReliabilityType m_ReliabilityType;
		IHost* m_RemoteHost = nullptr;
	};
}



#endif

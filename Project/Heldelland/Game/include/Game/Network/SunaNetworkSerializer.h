
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<Suna/Bitstream.h>



namespace Game
{
    class SunaNetworkSerializer
    {
    public:
        // Be sure that the buffer outlives the serializer!
        SunaNetworkSerializer(unsigned char* buffer, const unsigned int bitStartPosition, const unsigned int sizeInBytes);
        virtual ~SunaNetworkSerializer();

        bool SetBitPointerPosition(const unsigned int position);



    protected:
        unsigned char* m_Buffer;
        const unsigned int m_BitStartPosition;
        const unsigned int m_SizeInBits;

        Suna::Bitstream m_Stream;
    };
}

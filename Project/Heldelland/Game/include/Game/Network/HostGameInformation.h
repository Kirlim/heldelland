
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__HOST_GAME_INFORMATION
#define GAME__NETWORK__HOST_GAME_INFORMATION

#include"Game/Simulation/Types/PlayerSimInfoId.h"



namespace Game
{
    struct HostGameInformation
    {
    public:
        PlayerSimInfoId playerId;

    public:
        // TODO : remove this variable, if it is not used
        static const unsigned int ServerPlayerId = 0;
    };
}

#endif

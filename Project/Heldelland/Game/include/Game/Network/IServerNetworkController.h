
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once



namespace Game
{
    // Forward declarations
    class IHost;
    class IHostChannel;



    class IServerNetworkController
    {
    public:
        virtual ~IServerNetworkController() {}

        virtual void Tick(IHost& host, IHostChannel& receiveFromClientChannel, IHostChannel& sendToClientChannel) = 0;
    };
}

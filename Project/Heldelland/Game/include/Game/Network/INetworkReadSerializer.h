
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include"Game/Network/INetworkSerializer.h"



namespace Game
{
    class INetworkReadSerializer : public INetworkSerializer
    {
    public:
        virtual ~INetworkReadSerializer() {}
    };
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<string>



namespace Game
{
    class Endpoint
    {
        friend bool operator==(const Endpoint&, const Endpoint&);
        friend bool operator!=(const Endpoint&, const Endpoint&);

    public:
        void SetIp(const std::string ip);
        std::string GetIp() const;

        void SetPort(const unsigned int port);
        unsigned int GetPort() const;

        void SetAddress(std::string ip, unsigned int port);



    protected:
        std::string m_Ip;
        unsigned int m_Port;
    };



    inline bool operator==(const Endpoint& lhs, const Endpoint& rhs)
    {
        return(lhs.m_Port == rhs.m_Port && lhs.m_Ip == rhs.m_Ip);
    }

    inline bool operator!=(const Endpoint& lhs, const Endpoint& rhs)
    {
        return(!(lhs == rhs));
    }
}

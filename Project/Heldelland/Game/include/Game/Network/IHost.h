//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__I_HOST_H
#define GAME__NETWORK__I_HOST_H

#include<string>
#include<memory>

#include"Game/Network/HostId.h"



namespace Game
{
	class Endpoint;
	class IHostChannel;
	struct HostGameInformation;



	enum class HostConnectionStatus
	{
		Connected,

		// Server side
		RequestingToJoinGame,
		InGame
	};

	class IHost
	{
	public:
		virtual ~IHost()
		{
		};

		virtual HostId GetLocadId() const = 0;

		virtual Endpoint GetEndpoint() const = 0;
		virtual void SetEndpoint(Endpoint endpoint) = 0;

		virtual void SetPlayerAlias(std::string alias) = 0;
		virtual std::string GetPlayerAlias() const = 0;

		virtual void SetGameInformation(std::unique_ptr<HostGameInformation>& hostGameInformation) = 0;
		virtual HostGameInformation* GetGameInformation() const = 0;

		virtual IHostChannel* GetFromHostChannel() const = 0;
		virtual IHostChannel* GetToHostChannel() const = 0;
	};
}



#endif

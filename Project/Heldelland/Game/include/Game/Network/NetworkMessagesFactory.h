
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__NETWORK_MESSAGES_FACTORY_H
#define GAME__NETWORK__NETWORK_MESSAGES_FACTORY_H

#include<unordered_map>

#include"Game/Network/INetworkMessagesFactory.h"

// TODO : pass Network Message Type Id to its own header, to reduce include hell.



namespace Game
{
	class NetworkMessagesFactory : public INetworkMessagesFactory
	{
	public:
		~NetworkMessagesFactory() override;

		bool Register(NetworkMessageTypeId id, std::function<std::shared_ptr<NetworkMessage>()> factoryFunction) override;
		std::shared_ptr<NetworkMessage> InstantiateMessage(NetworkMessageTypeId id) const override;
		std::vector<NetworkMessageTypeId> GetAllRegisteredNetworkMessageTypeIds() const override;



	protected:
		std::unordered_map<NetworkMessageTypeId, std::function<std::shared_ptr<NetworkMessage>()>> m_Factories;
	};
}



#endif

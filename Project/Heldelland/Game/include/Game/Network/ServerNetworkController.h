
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__SERVER_NETWORK_CONTROLLER_H
#define GAME__NETWORK__SERVER_NETWORK_CONTROLLER_H

#include"Game/Network/IServerNetworkController.h"



namespace Game
{
	// Forward declarations
	class IHost;
	class IHostChannel;
	class INetworkMessagesHandlersManager;



	class ServerNetworkController : public IServerNetworkController
	{
	public:
		explicit ServerNetworkController(const INetworkMessagesHandlersManager& networkMessagesHandlersManager);
		virtual ~ServerNetworkController() = default;

		void Tick(IHost& host, IHostChannel& receiveFromClientChannel, IHostChannel& sendToClientChannel) override;



	private :
		const INetworkMessagesHandlersManager& m_NetworkMessagesHandlersManager;
	};
}

#endif

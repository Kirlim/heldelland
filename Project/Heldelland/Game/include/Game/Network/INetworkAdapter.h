//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__I_NETWORK_ADAPTER_H
#define GAME__NETWORK__I_NETWORK_ADAPTER_H

#include<string>
#include<functional>



namespace Game
{
	class INetworkAdapter
	{
	public:
		virtual ~INetworkAdapter()
		{
		}

		// Executes logic that needs to be execute every frame or so
		virtual void Tick() = 0;

		// Set ups connections listening with the @listenIp for incoming connections
		// (0.0.0.0 for any), and the @listenPort. It is expected that this method will
		// create whatever is needed in other threads, to keep communications working
		// asynchronously to the calling thread.
		// 
		// Returns true on success, or false on failure or if already started.
		virtual bool Start(const std::string listenIp, const unsigned int listenPort) = 0;

		// Attempts to close all established connections to this adapter, and then finish
		// any thread created by the Start method. Smooth disconnects are not guaranteed, as
		// disconnection messages can not be guaranteed to be sent or received.
		virtual void Stop() = 0;

		// Returns true if Start has been called and the adapter has a network
		// thread running.
		virtual bool IsRunning() const = 0;

		// Attempts to connect to the given @ip and @port.
		virtual void ConnectTo(const std::string ip, const unsigned int port) = 0;


		// Sets up the function to be called when a new connection request arrives.
		// If the function returns true, the connection is to be accepted. If the function
		// returns false, the request'll be ignored.
		virtual void SetIncomingConnectionListener(const std::function<bool(std::string, unsigned int)> handler) = 0;

		// Function to be called when a connection is stablished with a remote host
		virtual void SetOnConnectedListener(std::function<void(const std::string, const unsigned int)> event) = 0;

		// Function to be called when a connection is refused by a remote host (when trying to connect)
		virtual void SetOnConnectionRequestRefusedListener(std::function<void(const std::string, const unsigned int)> event) = 0;

		// Function to be called when a connection request has timed out
		virtual void SetOnConnectionRequestTimedOutListener(std::function<void(const std::string, const unsigned int)> event) = 0;

		// Function to be called when a new message is received from a remote host.
		virtual void SetOnNetworkMessageReceived(std::function<void(const std::string, const unsigned int, const unsigned char*, const unsigned int, const unsigned int)> event) = 0;


		// Sends a network message over the network through an unreliable channel. Will silently
		// do nothing if @data is null, or @sizeInBits is less than or equal to zero.
		virtual void SendDataAsUnreliable(const std::string ip, const unsigned int port, const unsigned char* data, const unsigned int sizeInBits) = 0;

		// Sends a network message over the network throught a reliable channel. Will silently
		// do nothing if @data is null, or @sizeInBits is less than or equal to zero.
		virtual void SendDataAsReliable(const std::string ip, const unsigned int port, const unsigned char* data, const unsigned int sizeInBits) = 0;
	};
}



#endif

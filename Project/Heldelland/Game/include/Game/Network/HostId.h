
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__HOST_ID_H
#define GAME__NETWORK__HOST_ID_H

#include<Nest/IdType.h>



namespace Game
{
	struct HostIdTraits
	{
		using underlying_type = unsigned int;
	};

	// TODO : uncomment next line and do fixes needed
	//using HostId = Nest::IdType<HostIdTraits>;

	using HostId = unsigned int;
}



#endif 
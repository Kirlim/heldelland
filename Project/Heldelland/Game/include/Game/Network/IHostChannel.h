//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__NETWORK__I_HOST_CHANNEL_H
#define GAME__NETWORK__I_HOST_CHANNEL_H

#include<memory>



namespace Game
{
	// Forward declarations
	class IHost;
	class NetworkMessage;



	// Represents a channel of communication with a host. An instance of this class
	// can represent an incoming buffer, or outgoing buffer, but not both at the same time.
	class IHostChannel
	{
	public:
		virtual ~IHostChannel();

		virtual IHost* GetHost() const = 0;

		virtual bool AddMessage(std::shared_ptr<NetworkMessage> message) = 0;

		virtual NetworkMessage* PeekNextMessage() const = 0;
		virtual std::shared_ptr<NetworkMessage> FetchNextMessage() = 0;

		virtual unsigned int Size() const = 0;

		virtual bool IsValid() const = 0;
	};
}



#endif

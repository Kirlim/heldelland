//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION_COMMONS__SIMULATION_ACTORS_MOVEMENT_DATA_H
#define GAME__SIMULATION_COMMONS__SIMULATION_ACTORS_MOVEMENT_DATA_H

#include<memory>
#include<vector>

#include"Game/SimulationCommons/SimulationTypes.h"



namespace Game
{
	class Path;



	struct ActorDistanceMovementInformation
	{
		simfix64 distance;
		// TODO : rename to baseSpeed, as this speed should be stable and without modifiers (unless the base speed has a better place to be)
		simfix64 speed;
	};

	// TODO : remove Path from movement behavior, and store it only here. Use SimActorMovementSystem to store the path
	struct ActorPathInformation
	{
		std::shared_ptr<Path> path;
	};

	struct ActorInFrameMovementPositions
	{
		simfix64 frameStartX;
		simfix64 frameStartY;
		simfix64 frameEndX;
		simfix64 frameEndY;
	};



	class SimulationActorsMovementData
	{
	public:
		SimulationActorsMovementData() = default;
		SimulationActorsMovementData(const SimulationActorsMovementData&) = delete;
		SimulationActorsMovementData(SimulationActorsMovementData&&) = delete;

		~SimulationActorsMovementData();

		SimulationActorsMovementData& operator=(const SimulationActorsMovementData&) = delete;
		SimulationActorsMovementData& operator=(SimulationActorsMovementData&&) = delete;

		void RegisterNetworkActor(ActorNetworkId id);
		void UnregisterNetworkActor(ActorNetworkId id);
		bool HasNetworkActor(ActorNetworkId id) const;

		// Returns negative if actor was not found
		int GetNetworkActorIndex(ActorNetworkId id) const;

		// Obtains the indexes of actorsIds. Indexes are in the same order as the ids.
		void GetNetworkActorsIndexes(const std::vector<ActorNetworkId>& searchActorsNetworkIds, std::vector<int>& out_Indexes) const;



	public:
		// All the data must be stored in the same actors order as the actorsIds vector.
		std::vector<ActorNetworkId> actorsNetworkIds;

		std::vector<ActorDistanceMovementInformation> actorsDistanceMovementInformation;
		std::vector<ActorPathInformation> actorsPathInformation;
		std::vector<ActorInFrameMovementPositions> actorsInFrameMovementPositions;
	};
}



#endif

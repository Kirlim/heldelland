//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION_COMMONS__SIMULATION_ACTORS_MOVEMENT_DATA_MANAGER_H
#define GAME__SIMULATION_COMMONS__SIMULATION_ACTORS_MOVEMENT_DATA_MANAGER_H

#include<vector>

#include"Game/SimulationCommons/SimulationTypes.h"



namespace Game
{
	class SimulationActorsMovementData;



	class SimulationActorsMovementDataManager
	{
	public:
		explicit SimulationActorsMovementDataManager(SimulationActorsMovementData& simulationActorsMovementData);
		~SimulationActorsMovementDataManager();

		void RegisterNewActorPendingToAdd(ActorNetworkId actorNetworkId);
		void RegisterNewActorPendingToRemove(ActorNetworkId actorNetworkId);

		void ApplyPendingOperations();



	private:
		void ApplyPendingInserts();
		void ApplyPendingRemoves();



	private:
		SimulationActorsMovementData& m_SimulationActorsMovementData;

		std::vector<ActorNetworkId> m_ActorsPendingToAdd;
		std::vector<ActorNetworkId> m_ActorsPendingToRemove;
	};
}



#endif

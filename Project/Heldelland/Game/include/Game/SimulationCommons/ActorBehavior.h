//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION__COMMONS__ACTOR_BEHAVIOR_H
#define GAME__SIMULATION__COMMONS__ACTOR_BEHAVIOR_H

#include<Nest/Component.h>
#include"Game/SimulationCommons/SimulationTypes.h"



namespace Game
{
	// TODO : gradually work in order to remove the need of ActorBehavior as a component in the game. Actually, try to remove the Component class itself, in favor to a better data-oriented approach.
	class ActorBehavior : public Nest::Component
	{
	public:
		ActorBehavior();
		ActorBehavior(const ActorBehavior&) = delete;
		ActorBehavior(ActorBehavior&&) = default;

		virtual ~ActorBehavior();

		ActorBehavior& operator=(const ActorBehavior&) = delete;
		ActorBehavior& operator=(ActorBehavior&&) = default;


		void SetActorId(ActorId newId);
		ActorId GetActorId() const;

		void SetNetworkId(ActorNetworkId newNetworkId);
		ActorNetworkId GetNetworkId() const;

		void SetNetworkEntityFlag(bool isNetworkEntity);
		bool IsNetworkEntity() const;


		// Inherited via Component
		static std::string GetTypeName() { return ("ActorBehavior"); }



	protected:
		void OnDestroy() override;



	protected:
		ActorId m_Id;
		ActorNetworkId m_NetworkId;
		bool m_IsNetworkEntity;
	};
}

#endif

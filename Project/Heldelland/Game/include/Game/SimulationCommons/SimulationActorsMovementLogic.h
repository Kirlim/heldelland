//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION_COMMONS__SIMULATION_ACTORS_MOVEMENT_LOGIC_H
#define GAME__SIMULATION_COMMONS__SIMULATION_ACTORS_MOVEMENT_LOGIC_H

#include"Game/SimulationCommons/SimulationTypes.h"

#include<memory>



namespace Game
{
	class Path;
	class SimulationActorsMovementData;



	class SimulationActorsMovementLogic
	{
	public:
		explicit SimulationActorsMovementLogic(SimulationActorsMovementData& simulationActorsMovementData);
		SimulationActorsMovementLogic(const SimulationActorsMovementLogic&) = delete;
		SimulationActorsMovementLogic(SimulationActorsMovementLogic&&) = delete;

		~SimulationActorsMovementLogic();

		SimulationActorsMovementLogic& operator=(const SimulationActorsMovementLogic&) = delete;
		SimulationActorsMovementLogic& operator=(SimulationActorsMovementLogic&&) = delete;

		void SetPath(ActorNetworkId actorNetworkId, std::shared_ptr<Path> path) const;

		void UpdateActorsMovementDistance(simfix64 deltaTime) const;
		void UpdateActorsMovementInFrame() const;



	private:
		SimulationActorsMovementData& m_SimulationActorsMovementData;
	};
}



#endif

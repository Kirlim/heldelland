//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION_COMMONS__ACTORS_MANAGER_H
#define GAME__SIMULATION_COMMONS__ACTORS_MANAGER_H

#include<unordered_map>
#include<Nest/ComponentReference.h>

#include"Game/SimulationCommons/SimulationTypes.h"



namespace Nest
{
	class EntitiesManager;
}

namespace Game
{
	// Forward declarations
	class ActorBehavior;



	// TODO : use typedef for actor id and network id
	class ActorsManager
	{
	public:
		explicit ActorsManager(Nest::EntitiesManager& entitiesManager);

		// Instantiates a new empty actor and returns the id. Will use the network
		// flag to know if we need to generate an auto-increment network id, or not.
		ActorId InstantiateNew(bool isNetworkEntity);

		// Instantiates a new empty actor, using the given network id. Will be flagged as
		// a network entity
		ActorId InstantiateNew(ActorNetworkId networkId);


		// Returns a "component reference" to the actor with specific id, or
		// a "reference" to nullptr if no actor was found.
		Nest::ComponentReference<ActorBehavior> GetActor(ActorId id) const;

		// Returns a "component reference" to the actor with specific network id, or
		// a "reference" to nullptr if no actor was found.
		Nest::ComponentReference<ActorBehavior> GetActorWithNetworkId(ActorNetworkId networkId) const;

		// TODO : way to unregister actor, or by destruction

		void GetActorsNetworkIdsByAppending(
			const std::vector<ActorId>& actorsIds,
			std::vector<ActorNetworkId>& out_ActorsNetworkIds
		) const;



	private:
		void InstantiateNew(ActorId desiredActorId, bool isNetworkEntity, ActorNetworkId networkId = ActorNetworkId{ 0 });

		ActorId GetNextId();
		ActorNetworkId GetNextNetworkId();



	private:
		Nest::EntitiesManager& m_EntitiesManager;

		ActorId m_NextActorId{ 1 };
		ActorNetworkId m_NextNetworkId{ 1 };

		// TODO : try to not depend on ComponentReference<ActorBehavior>. Note that this will invalidate GetActor and GetActorWithNetworkId methods.
		std::unordered_map<ActorId, Nest::ComponentReference<ActorBehavior>> m_Actors;
	};
}

#endif

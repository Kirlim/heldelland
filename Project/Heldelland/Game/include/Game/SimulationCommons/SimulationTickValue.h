
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION_COMMONS__SIMULATION_TICK_VALUE_H
#define GAME__SIMULATION_COMMONS__SIMULATION_TICK_VALUE_H



namespace Game
{
    class SimulationTickValue
    {
    public:
        SimulationTickValue();
        ~SimulationTickValue();

        void SetCurrent(unsigned int newCurrentTickValue);
        unsigned int GetCurrent() const;
        unsigned int GetNext() const;
        unsigned int GetPrevious() const;

        void Increment();



    public:
        static const unsigned int MaxTickValue;



    protected:
        unsigned int m_CurrentTick = 0;
    };
}



#endif

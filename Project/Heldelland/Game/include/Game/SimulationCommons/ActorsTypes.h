
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION_COMMONS__ACTORS_TYPES_H
#define GAME__SIMULATION_COMMONS__ACTORS_TYPES_H

#include"Nest/IdType.h"

namespace Game
{
	struct ActorTypeIdTraits
	{
		using underlying_type = uint16_t;
	};
	using ActorTypeId = Nest::IdType<ActorTypeIdTraits>;
}



#define ACTOR_TYPE_TEST                         99

#define ACTOR_TYPE_PLAYER                       100

#endif

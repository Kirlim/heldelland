//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION_COMMONS__SIMULATION_ACTOR_MOVEMENT_SYSTEM_H
#define GAME__SIMULATION_COMMONS__SIMULATION_ACTOR_MOVEMENT_SYSTEM_H

#include<memory>

#include<vector>

#include<Nest/Vector2.h>

#include"Game/SimulationCommons/SimulationTypes.h"



namespace Game
{
	struct ActorInFrameMovementPositions;

	class Path;
	class SimulationActorsMovementData;
	class SimulationActorsMovementDataManager;
	class SimulationActorsMovementLogic;



	class SimulationActorMovementSystem final
	{
	public:
		SimulationActorMovementSystem(
			SimulationActorsMovementData& simulationActorsMovementData,
			SimulationActorsMovementDataManager& simulationActorsMovementDataManager,
			SimulationActorsMovementLogic& simulationActorsMovementLogic
		);
		~SimulationActorMovementSystem();

		SimulationActorsMovementData* GetSimulationActorsMovementData() const;

		void RegisterActorAsMovementCapable(ActorNetworkId actorNetworkId) const;

		void SetActorPath(ActorNetworkId actorNetworkId, std::shared_ptr<Path> actorPath) const;
		void SetActorMovementSpeed(ActorNetworkId actorNetworkId, simfix64 movementSpeed) const;

		Nest::Vector2<simfix64> GetActorNextPosition(ActorNetworkId actorNetworkId) const;

		void UpdateActorsMovementData(simfix64 deltaTime) const;

		// Creates a vector of movement in frame positions information, for each input actor.
		// The order of the actors in the input is the same as in the output
		void GetMovementInFramePositionsForActors(
			const std::vector<ActorNetworkId>& in_Ids,
			std::vector<ActorInFrameMovementPositions>& out_ActorInFrameMovementPositions
		) const;



	private:
		SimulationActorsMovementData& m_SimulationActorsMovementData;
		SimulationActorsMovementDataManager& m_SimulationActorsMovementDataManager;
		SimulationActorsMovementLogic& m_SimulationActorsMovementLogic;
	};
}



#endif

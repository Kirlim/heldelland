//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMULATION_COMMONS__SIMULATION_TYPES_H
#define GAME__SIMULATION_COMMONS__SIMULATION_TYPES_H



#include<cinttypes>
#include<Nest/IdType.h>
#include<Nest/FixedPoint.h>



namespace Game
{
	// sfloat is the fixed point type to be used by the simulation classes
	// and components, instead of a float.
	// HINT: SQRT(2^47) = 11,863,283.2
	using simfix64 = FixedPoint<true, 48, 16>;

	// Enumerator used to know which simulation type the simulation is.
	// Server: complete simulation
	// Client: fragment of a simulation
	enum class GameSimulationType { Server, Client };


	struct ActorIdTraits
	{
		using underlying_type = uint16_t;
	};

	using ActorId = Nest::IdType<ActorIdTraits>;


	struct ActorNetworkIdTraits
	{
		using underlying_type = uint16_t;
	};

	using ActorNetworkId = Nest::IdType<ActorNetworkIdTraits>;


	const ActorId InvalidActorId{ 0 };
	const ActorNetworkId InvalidActorNetworkId{ 0 };
}



#endif

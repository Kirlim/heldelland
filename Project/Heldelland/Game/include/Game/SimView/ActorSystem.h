
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__SIMVIEW__ACTOR_SYSTEM_H
#define GAME__SIMVIEW__ACTOR_SYSTEM_H



#include<Nest/System.h>



namespace Game
{
	class ActorSystem final : public Nest::System
	{
	public:
		ActorSystem();
		virtual ~ActorSystem() = default;



	private:
		void DoUpdate(const float deltaTime) override;
	};
}



#endif

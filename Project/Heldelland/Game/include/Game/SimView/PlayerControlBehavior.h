
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<Nest/Component.h>
#include<Nest/ComponentReference.h>



namespace Game
{
    // Forward declarations
    class SimActorBehavior;



    class PlayerControlBehavior : public Nest::Component
    {
        friend class PlayerControlSystem;

    public:
        PlayerControlBehavior();
	PlayerControlBehavior(const PlayerControlBehavior&) = delete;
	PlayerControlBehavior(PlayerControlBehavior&&) = default;

        virtual ~PlayerControlBehavior();

	PlayerControlBehavior& operator =(const PlayerControlBehavior&) = delete;
	PlayerControlBehavior& operator =(PlayerControlBehavior&&) = default;


        static std::string GetTypeName() { return("PlayerControlBehavior"); }

        void SetPlayerSimulationActor(Nest::ComponentReference<SimActorBehavior> simulationActor);
        Nest::ComponentReference<SimActorBehavior> GetPlayerSimulationActor() const;

        // Inherited via Component



    protected:
        virtual void OnDestroy() override;



    protected:
        Nest::ComponentReference<SimActorBehavior> m_SimActor;
    };
}

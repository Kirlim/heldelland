//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__GAME_H
#define GAME__GAME_H

#include<memory>
#include<string>
#include<vector>



namespace Nest
{
	// Forward declarations
	class IComponentsFactory;
	class IComponentsContainers;
	class SystemsManager;
	class IResourcesManager;
	class EntitiesManager;
	class MeshesFactory;
	class NestEngine;
	class RenderingManager;
	class RenderingModule;
	class World;
	enum class GameRenderer;

	class EventsBusToken;
}



namespace Game
{
	// Forward declarations
	class CameraControlInputObserver;
	class DevelopersConsoleSystem;
	class GameSettings;
	class IGameViewSimulationFactory;
	class INetworkSystem;
	class MapsFactory;
	class IMapViewFactory;

	class SimulationActorsMovementData;
	class SimulationActorsMovementDataManager;
	class SimulationActorsMovementLogic;

	class GameNetworkConsoleCommandsHandler;



	// Global variables. Might become a better way ta pass thing over, if needed (as singleton, or by injection)
	extern std::unique_ptr<INetworkSystem> Network;



	enum class GameWorldId : unsigned int { ServerSimulation = 1, ClientSimulation, View };



	class Game
	{
	public:
		Game(
			Nest::NestEngine& engine,
			const Nest::MeshesFactory& meshesFactory,
			const MapsFactory& mapsFactory
		);
		Game(const Game&) = delete;
		Game(Game&&) = delete;

		~Game();

		Game& operator=(const Game&) = delete;
		Game& operator=(Game&&) = delete;

		void Init(const GameSettings& gameSettings);
		void LoadGameScene();



	private:
		void SetEngineWorlds() const;

		void SetUpServerSimulation() const;
		void SetUpMetadataManager(const GameSettings& gameSettings) const;
		void SetUpGameSimulationViewFactory(
			Nest::EntitiesManager& entitiesManager, Nest::RenderingModule& renderingModule, Nest::World& world
		);

		void LoadEngineComponents(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers);
		void LoadEngineSystems(Nest::SystemsManager& systemsManager);
		void LoadEngineRenderingSystem(
			Nest::SystemsManager& systemsManager, Nest::GameRenderer gameRenderer,
			const Nest::IComponentsContainers& componentsContainers, const Nest::RenderingManager& renderingManager
		) const;

		void LoadSimulationComponents() const;
		void LoadSimulationsSystems();
		void LoadNetwork();

		void LoadServerSimulationSystems();
		void InstantiateServerSimulationActorsMovementDataManagers();

		void LoadClientSimulationSystems();
		void InstantiateClientSimulationActorsMovementDataManagers();

		void LoadViewSystems() const;

		void LoadSimulationViewComponents() const;
		void LoadSimulationViewSystems(Nest::SystemsManager& systemsManager) const;


		void LoadCommonMeshes(Nest::IResourcesManager& resourcesManager, const Nest::MeshesFactory& meshesFactory);
		void SetUpRenderingSystem(Nest::IResourcesManager& resourcesManager, Nest::SystemsManager& systemsManager, Nest::GameRenderer gameRenderer);

		void SetUpDevelopersConsole(Nest::EntitiesManager& entitiesManager, Nest::IResourcesManager& resourcesManager, DevelopersConsoleSystem& developersConsoleSystem);
		void InitializeMainConsoleCommandsHandlers(DevelopersConsoleSystem& developersConsoleSystem);

		void StartGameplay();
		void StartNetwork(unsigned int port, bool client);

		void SetUpGameSimulationNetworkEventsListeners();


		// Main console commands
		// TODO : is here really the best place to have them?
		void MapEditorCommandAction(const std::vector<std::string>& command);

		Nest::World* GetWorld(GameWorldId worldId) const;



	private:
		Nest::NestEngine& m_Engine;

		const Nest::MeshesFactory& m_MeshesFactory;
		const MapsFactory& m_MapsFactory;

		std::unique_ptr<IMapViewFactory> m_MapViewFactory;
		std::unique_ptr<IGameViewSimulationFactory> m_GameViewSimulationFactory;

		std::unique_ptr<SimulationActorsMovementData> m_ServerSimulationActorsMovementData;
		std::unique_ptr<SimulationActorsMovementDataManager> m_ServerSimulationActorsMovementDataManager;
		std::unique_ptr<SimulationActorsMovementLogic> m_ServerSimulationActorsMovementLogic;

		std::unique_ptr<SimulationActorsMovementData> m_ClientSimulationActorsMovementData;
		std::unique_ptr<SimulationActorsMovementDataManager> m_ClientSimulationActorsMovementDataManager;
		std::unique_ptr<SimulationActorsMovementLogic> m_ClientSimulationActorsMovementLogic;

		bool m_IsGameplayRunning = false;
		bool m_IsNetworkStarted = false;

		std::unique_ptr<CameraControlInputObserver> m_CameraControlInputObserver;

		std::unique_ptr<GameNetworkConsoleCommandsHandler> m_GameNetworkConsoleCommandsHandler;

		std::unique_ptr<Nest::EventsBusToken> m_OnNewClientSimulationStartedEventToken;
	};
}



#endif

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__BIRD_EYE_CAMERA_BEHAVIOR_H
#define GAME__BIRD_EYE_CAMERA_BEHAVIOR_H

#include<string>

#include<Nest/Component.h>
#include<Nest/ComponentReference.h>



namespace Nest
{
	class Transform;
	class IEntity;
}



namespace Game
{
	class BirdEyeCameraBehavior : public Nest::Component
	{
	public:
		BirdEyeCameraBehavior() = default;
		BirdEyeCameraBehavior(const BirdEyeCameraBehavior&) = delete;
		BirdEyeCameraBehavior(BirdEyeCameraBehavior&&) = default;

		~BirdEyeCameraBehavior() override;

		BirdEyeCameraBehavior& operator =(const BirdEyeCameraBehavior&) = delete;
		BirdEyeCameraBehavior& operator =(BirdEyeCameraBehavior&&) = default;


		static std::string GetTypeName() { return "BirdEyeCameraBehavior"; }

		void SetFollowTarget(Nest::ComponentReference<Nest::Transform> targetEntity);
		Nest::ComponentReference<Nest::Transform> GetFollowTarget() const;

		void SetPitchRadians(float pitchRadians);
		float GetPitchRadians() const;

		void SetYawRadians(float yawRadians);
		float GetYawRadians() const;

		void SetDistanceToTarget(float distanceToTarget);
		float GetDistanceToTarget() const;



	private:
		Nest::ComponentReference<Nest::Transform> m_FollowTarget;
		float m_PitchRadians = -0.6f;
		float m_YawRadians = 0.6f;

		float m_MinDistanceToTarget = 3.0f;
		float m_MaxDistanceToTarget = 20.0f;
		float m_DistanceToTarget = 10.0f;
	};
}



#endif

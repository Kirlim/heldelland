//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__DEVELOPERS_CONSOLE_SYSTEM_H
#define GAME__DEVELOPERS_CONSOLE_SYSTEM_H



#include<string>
#include<vector>
#include<functional>
#include<unordered_map>
#include<mutex>

#include<Nest/System.h>



namespace Nest
{
	class IComponentsContainers;
}

namespace Game
{
	class DevelopersConsoleBehavior;



	class DevelopersConsoleSystem final : public Nest::System
	{
		// Represents the command and then all arguments of the command. The first position
		// of the vector is always the command itself.
		using ActionFunction = std::function<void(std::vector<std::string>)>;
	public:
		explicit DevelopersConsoleSystem(const Nest::IComponentsContainers& componentsContainers);
		DevelopersConsoleSystem(const DevelopersConsoleSystem&) = delete;
		DevelopersConsoleSystem(DevelopersConsoleSystem&&) = delete;

		~DevelopersConsoleSystem() override;

		DevelopersConsoleSystem& operator=(const DevelopersConsoleSystem&) = delete;
		DevelopersConsoleSystem& operator=(DevelopersConsoleSystem&&) = delete;

		void RegisterCommand(const std::string& command, ActionFunction action);
		bool IsCommandRegistered(std::string command) const;
		void UnregisterCommand(std::string command);
		void ExecuteLine(const std::string& line) const;

		// TODO : Refactor the system to not use a container, and use and DevelopersConsole base or interface, allowing easier testing of methods in this system
		void WriteOutputLine(std::string line);



	private:
		void DoFixedUpdate(float deltaTime) override;
		void WritePendingLinesToConsole(DevelopersConsoleBehavior& developersConsole);
		static void ConsoleVisibilityTogglingLogic(DevelopersConsoleBehavior& developersConsole);



	private:
		const Nest::IComponentsContainers& m_ComponentsContainers;
		std::unordered_map<std::string, ActionFunction> m_Actions;

		std::vector<std::string> m_PendingOutputLines;
		std::mutex m_PendingOutputLinesMutex;
	};
}



#endif

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__BIRD_EYE_CAMERA_SYSTEM_H
#define GAME__BIRD_EYE_CAMERA_SYSTEM_H

#include<Nest/System.h>



namespace Nest
{
	class IComponentsContainers;
}

namespace Game
{
	class BirdEyeCameraBehavior;



	class BirdEyeCameraSystem : public Nest::System
	{
	public:
		explicit BirdEyeCameraSystem(const Nest::IComponentsContainers& componentsContainers);
		BirdEyeCameraSystem(const BirdEyeCameraSystem&) = delete;
		BirdEyeCameraSystem(BirdEyeCameraSystem&&) = default;

		BirdEyeCameraSystem& operator=(const BirdEyeCameraSystem&) = delete;
		BirdEyeCameraSystem& operator=(BirdEyeCameraSystem&&) = default;

		~BirdEyeCameraSystem() override;

		void SetMouseYawRotationInput(int mouseDeltaX);
		void SetMouseZoomInput(int mouseScrollDeltaY);

		void SetYawRotationSpeedMultiplier(float yawRotationSpeedMultiplier);
		void SetZoomSpeedMultiplier(float zoomSpeedMultiplier);



	private:
		void DoUpdate(float) override;

		void UpdateCameraYawRotation(BirdEyeCameraBehavior& birdEyeCameraBehavior);
		void UpdateCameraZoom(BirdEyeCameraBehavior& birdEyeCameraBehavior);
		static void UpdateCameraTransform(BirdEyeCameraBehavior& birdEyeCameraBehavior);



	private:
		const Nest::IComponentsContainers& m_ComponentsContainers;
		int m_AccumulatedYawRotation = 0;
		int m_AccumulatedZoom = 0;

		float m_YawRotationSpeedMultiplier = 0.01f;
		float m_ZoomSpeedMultiplier = 0.2f;
	};
}



#endif

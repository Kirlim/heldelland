//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef GAME__CONSOLE_COMMANDS__GAME_NETWORK_CONSOLE_COMMANDS_HANDLER_H
#define GAME__CONSOLE_COMMANDS__GAME_NETWORK_CONSOLE_COMMANDS_HANDLER_H

#include<vector>
#include<string>
#include<functional>



namespace Game
{
	class GameNetworkConsoleCommandsHandler
	{
	public:
		GameNetworkConsoleCommandsHandler(
			std::function<void(unsigned int, bool)> startNetworkAction,
			std::function<bool()> isNetworkStartedFunction,
			std::function<void()> startGameAction,
			std::function<bool()> isGameStartedFunction,
			std::function<void(std::string, unsigned int)> connectAsyncAction
		);

		GameNetworkConsoleCommandsHandler(const GameNetworkConsoleCommandsHandler&) = delete;
		GameNetworkConsoleCommandsHandler(GameNetworkConsoleCommandsHandler&&) = default;

		GameNetworkConsoleCommandsHandler& operator=(const GameNetworkConsoleCommandsHandler&) = delete;
		GameNetworkConsoleCommandsHandler& operator=(GameNetworkConsoleCommandsHandler&&) = default;

		~GameNetworkConsoleCommandsHandler();

		// net_start [port] [client/server]
		void NetStartAction(const std::vector<std::string>& command) const;

		// game_start
		void GameStartAction(const std::vector<std::string>& command) const;

		// s_start_client
		void StartClientAction(const std::vector<std::string>& command) const;

		// s_start_server
		void StartServerAction(const std::vector<std::string>& command) const;

		// connect
		void ConnectAction(const std::vector<std::string>& command) const;

		// s_connect #Short for connect 127.0.0.1:60000
		void DefaultLoopbackConnectAction(const std::vector<std::string>& command) const;



	private:
		std::function<void(unsigned int, bool)> m_StartNetworkAction;
		std::function<bool()> m_IsNetworkStartedFunction;
		std::function<void()> m_StartGameAction;
		std::function<bool()> m_IsGameStartedFunction;
		std::function<void(std::string, unsigned int)> m_ConnectAsyncAction;
	};
}



#endif

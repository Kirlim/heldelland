//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/CameraControlInputObserver.h"

#include<Nest/Events/EventsBus.h>
#include<Nest/InputEvent.h>



namespace Game
{
	CameraControlInputObserver::CameraControlInputObserver(Nest::EventsBus<Nest::InputEvent&>& inputEventsBus)
	{
		m_InputEventListenerToken = inputEventsBus.Register([&](const Nest::InputEvent& evt) { HandleInputEvent(evt); });
	}



	void CameraControlInputObserver::SetYawRotationCallback(std::function<void(int /*YawRotation*/)> yawRotationCallback)
	{
		m_YawRotationCallback = yawRotationCallback;
	}



	void CameraControlInputObserver::SetZoomCallback(std::function<void(int /*scrollDeltaY*/)> zoomCallback)
	{
		m_ZoomCallback = zoomCallback;
	}



	void CameraControlInputObserver::HandleInputEvent(const Nest::InputEvent& inputEvent)
	{
		if (inputEvent.type == Nest::InputEventType::MouseMovement)
		{
			if (m_RightMouseButtonPressed && m_YawRotationCallback != nullptr)
				m_YawRotationCallback(inputEvent.deltaX);
		}
		else if (inputEvent.type == Nest::InputEventType::MouseButtonDown && inputEvent.mouseButton == Nest::MouseButton::Right)
		{
			m_RightMouseButtonPressed = true;
		}
		else if (inputEvent.type == Nest::InputEventType::MouseButtonUp && inputEvent.mouseButton == Nest::MouseButton::Right)
		{
			m_RightMouseButtonPressed = false;
		}
		else if (inputEvent.type == Nest::InputEventType::MouseScroll)
		{
			if (m_ZoomCallback != nullptr)
				m_ZoomCallback(inputEvent.deltaY);
		}
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/BirdEyeCameraSystem.h"

#include<Nest/IComponentsContainers.h>
#include<Nest/ITemplatedSingleComponentsContainer.h>
#include<Nest/Transform.h>
#include<Nest/IEntity.h>

#include<Nest/Vector3.h>
#include<Nest/Quaternion.h>

#include"Game/BirdEyeCameraBehavior.h"



namespace Game
{
	BirdEyeCameraSystem::BirdEyeCameraSystem(const Nest::IComponentsContainers& componentsContainers) : System(),
		m_ComponentsContainers(componentsContainers)
	{
	}



	BirdEyeCameraSystem::~BirdEyeCameraSystem() = default;



	void BirdEyeCameraSystem::SetMouseYawRotationInput(int mouseDeltaX)
	{
		m_AccumulatedYawRotation += mouseDeltaX;
	}



	void BirdEyeCameraSystem::SetMouseZoomInput(int mouseScrollDeltaY)
	{
		m_AccumulatedZoom -= mouseScrollDeltaY;
	}



	void BirdEyeCameraSystem::SetYawRotationSpeedMultiplier(float yawRotationSpeedMultiplier)
	{
		m_YawRotationSpeedMultiplier = yawRotationSpeedMultiplier;
	}



	void BirdEyeCameraSystem::SetZoomSpeedMultiplier(float zoomSpeedMultiplier)
	{
		m_ZoomSpeedMultiplier = zoomSpeedMultiplier;
	}



	void BirdEyeCameraSystem::DoUpdate(float)
	{
		const auto cameras = m_ComponentsContainers.GetSingleContainer<BirdEyeCameraBehavior>();
		for (auto& camera : *cameras)
		{
			UpdateCameraYawRotation(camera);
			UpdateCameraZoom(camera);
			UpdateCameraTransform(camera);
		}
	}



	void BirdEyeCameraSystem::UpdateCameraYawRotation(BirdEyeCameraBehavior& birdEyeCameraBehavior)
	{
		const float rotationAmount = float(m_AccumulatedYawRotation) * m_YawRotationSpeedMultiplier;

		const auto previousYawnRotation = birdEyeCameraBehavior.GetYawRadians();
		birdEyeCameraBehavior.SetYawRadians(previousYawnRotation - rotationAmount);

		m_AccumulatedYawRotation = 0;
	}



	void BirdEyeCameraSystem::UpdateCameraZoom(BirdEyeCameraBehavior& birdEyeCameraBehavior)
	{
		const float zoomAmount = float(m_AccumulatedZoom) * m_ZoomSpeedMultiplier;

		const auto previousZoomAmount = birdEyeCameraBehavior.GetDistanceToTarget();
		birdEyeCameraBehavior.SetDistanceToTarget(previousZoomAmount + zoomAmount);

		m_AccumulatedZoom = 0;
	}



	void BirdEyeCameraSystem::UpdateCameraTransform(BirdEyeCameraBehavior& birdEyeCameraBehavior)
	{
		auto transform = birdEyeCameraBehavior.GetOwner()->GetComponent<Nest::Transform>();
		if (transform == nullptr)
			return;

		auto targetTransform = birdEyeCameraBehavior.GetFollowTarget();
		if (targetTransform == nullptr)
			return;


		const auto targetPosition = targetTransform->GetPosition();
		transform->SetLocalPosition(targetPosition);

		const auto yawRotation = Nest::Quaternion<float>{ birdEyeCameraBehavior.GetYawRadians(), Nest::Vector3f{ 0.0f, 1.0f, 0.0f } };
		const auto pitchRotation = Nest::Quaternion<float>{ birdEyeCameraBehavior.GetPitchRadians(), Nest::Vector3f{ 1.0f, 0.0f, 0.0f } };
		auto finalRotation = yawRotation;
		finalRotation *= pitchRotation;
		transform->SetLocalRotation(finalRotation);

		Nest::Vector3f adjustmentTranslation{ 0.0f, 0.0f, birdEyeCameraBehavior.GetDistanceToTarget() };
		pitchRotation.RotateVectorInplace(adjustmentTranslation);
		yawRotation.RotateVectorInplace(adjustmentTranslation);

		transform->LocalTranslate(adjustmentTranslation);
	}
}

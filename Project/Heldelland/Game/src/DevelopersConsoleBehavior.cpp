
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/DevelopersConsoleBehavior.h"

#include<Nest/IResourcesManager.h>
#include<Nest/EntitiesManager.h>
#include<Nest/IEntity.h>
#include<Nest/Transform.h>
#include<Nest/Input.h>
//#include<Nest/World.h>
#include<Nest/Global.h>
#include<Nest/Log.h>

// TODO : remove when we get prefabs
#include<Nest/MeshRenderer.h>



namespace Game
{
	DevelopersConsoleBehavior::DevelopersConsoleBehavior() :
		m_IsShowing(false), m_LinesDistance(0.0f)
	{
	}



	DevelopersConsoleBehavior::~DevelopersConsoleBehavior() = default;



	void DevelopersConsoleBehavior::SetShowPosition(Nest::Vector3f showPosition)
	{
		m_ShowPosition = showPosition;
	}



	void DevelopersConsoleBehavior::SetHidePosition(Nest::Vector3f hidePosition)
	{
		m_HidePosition = hidePosition;
	}



	Nest::Vector3f DevelopersConsoleBehavior::GetShowPosition() const
	{
		return(m_ShowPosition);
	}



	Nest::Vector3f DevelopersConsoleBehavior::GetHidePosition() const
	{
		return(m_HidePosition);
	}



	void DevelopersConsoleBehavior::SetBottomTextLinePosition(Nest::Vector3f position)
	{
		m_BottomLinePosition = position;
	}



	void DevelopersConsoleBehavior::SetLinesDistance(float distance)
	{
		m_LinesDistance = distance;
	}



	Nest::Vector3f DevelopersConsoleBehavior::GetBottomTextLinePosition() const
	{
		return(m_BottomLinePosition);
	}



	float DevelopersConsoleBehavior::GetLinesDistance() const
	{
		return(m_LinesDistance);
	}



	void DevelopersConsoleBehavior::AddTextLines(int count)
	{
		const auto myTransform = this->GetOwner()->GetComponent<Nest::Transform>();

		for (auto i = 0; i < count; ++i)
		{
			const auto newLine = CreateTextLine();
			auto newLineEntity = newLine->GetOwner();
			auto newLineTransform = newLineEntity->GetComponent<Nest::Transform>();

			const auto currentSize = m_Lines.size();
			auto newLinePosition = m_BottomLinePosition;
			newLinePosition.y += currentSize * m_LinesDistance;
			newLineTransform->SetParent(myTransform);
			newLineTransform->SetLocalPosition(newLinePosition);

			m_Lines.push_back(newLine);
		}
	}



	void DevelopersConsoleBehavior::RemoveTextLines(int count)
	{
		// TODO : method (need to have entites destruction capabilites)
		throw - 1;
	}



	void DevelopersConsoleBehavior::ScrollLinesUp(int count)
	{
		for (auto i = int(m_Lines.size()) - 1; i >= count; --i)
		{
			m_Lines[i]->SetText(m_Lines[i - 1]->GetText());
		}

		for (auto i = 0; i < count; ++i)
		{
			m_Lines[i]->SetText("");
		}
	}



	void DevelopersConsoleBehavior::WriteLine(std::string line)
	{
		if (!m_Lines.empty())
		{
			ScrollLinesUp(1);
			m_Lines[0]->SetText(line);
		}
	}



	void DevelopersConsoleBehavior::ClearText()
	{
		for (const auto& line : m_Lines)
		{
			line->SetText("");
		}
	}



	bool DevelopersConsoleBehavior::IsShowing() const
	{
		return(m_IsShowing);
	}



	void DevelopersConsoleBehavior::Show()
	{
		if (!Nest::Global->GetInput()->IsOnTextInput())
		{
			m_IsShowing = true;

			Nest::Global->GetInput()->StartTextInput(m_CommandString);

			auto transform = this->GetOwner()->GetComponent<Nest::Transform>();
			if (transform != nullptr)
			{
				transform->SetLocalPosition(m_ShowPosition);
			}
		}
	}



	void DevelopersConsoleBehavior::Hide()
	{
		auto transform = this->GetOwner()->GetComponent<Nest::Transform>();
		if (transform != nullptr)
		{
			transform->SetLocalPosition(m_HidePosition);
		}

		if (m_IsShowing)
		{
			m_IsShowing = false;

			Nest::Global->GetInput()->StopTextInput();
		}
	}



	void DevelopersConsoleBehavior::ToggleVisibility()
	{
		if (IsShowing())
		{
			Hide();
		}
		else
		{
			Show();
		}
	}



	void DevelopersConsoleBehavior::SetToggleKeyboardKey(Nest::KeyCode keyCode)
	{
		m_ToggleConsoleKeyboardKey = keyCode;
	}



	Nest::KeyCode DevelopersConsoleBehavior::GetToggleKeyboardKey() const
	{
		return m_ToggleConsoleKeyboardKey;
	}



	void DevelopersConsoleBehavior::SetCommandTextMesh(Nest::ComponentReference<Nest::TextMesh> textMesh)
	{
		m_CommandTextMesh = textMesh;
	}



	Nest::ComponentReference<Nest::TextMesh> DevelopersConsoleBehavior::GetCommandTextMesh() const
	{
		return(m_CommandTextMesh);
	}



	void DevelopersConsoleBehavior::SetCommitFunction(std::function<void(std::string)> commitFunction)
	{
		m_CommitFunction = std::move(commitFunction);
	}



	std::function<void(std::string)> DevelopersConsoleBehavior::GetCommitFunction() const
	{
		return(m_CommitFunction);
	}



	bool DevelopersConsoleBehavior::HasCommitFunction() const
	{
		return(m_CommitFunction != nullptr);
	}



	void DevelopersConsoleBehavior::Commit() const
	{
		if (m_CommandString->empty()) return;

		if (m_CommitFunction != nullptr)
		{
			m_CommitFunction(*m_CommandString);
		}
		else
		{
			Nest::Log::Warning("Trying to commit command, but Developers Console has no commit function set.");
		}

		m_CommandString->clear();
	}



	void DevelopersConsoleBehavior::OnCreate()
	{
		m_CommandString = std::make_shared<std::string>();
	}



	void DevelopersConsoleBehavior::OnDestroy()
	{
		// Nothing here
	}



	Nest::ComponentReference<Nest::TextMesh> DevelopersConsoleBehavior::CreateTextLine() const
	{
		auto entitiesManager = GetOwner()->GetEntitiesManager();

		// TODO : New Line should crome from a prefab
		auto lineEntity = entitiesManager->Instantiate().lock();

		auto newLineTextMesh = lineEntity->AddComponent<Nest::TextMesh>();
		lineEntity->AddComponent<Nest::MeshRenderer>();
		lineEntity->AddComponent<Nest::Transform>();

		const auto resourcesManager = GetOwner()->GetEntitiesManager()->GetResourcesManager();
		newLineTextMesh->SetText("");
		newLineTextMesh->SetFontSize(0.35f);
		newLineTextMesh->SetFont(resourcesManager->FetchFont("Raleway"));

		auto newLineMeshRenderer = lineEntity->GetComponent<Nest::MeshRenderer>();
		newLineMeshRenderer->SetMaterial(resourcesManager->FetchMaterial("FontMaterial"));
		newLineMeshRenderer->SetGuiFlag(true);


		return(lineEntity->GetComponent<Nest::TextMesh>());
	}
}
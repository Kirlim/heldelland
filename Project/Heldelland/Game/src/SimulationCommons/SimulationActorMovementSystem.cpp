//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/SimulationCommons/SimulationActorMovementSystem.h"

#include<Nest/NestAssert.h>

#include"Game/SimulationCommons/SimulationActorsMovementData.h"
#include"Game/SimulationCommons/SimulationActorsMovementDataManager.h"
#include"Game/SimulationCommons/SimulationActorsMovementLogic.h"



namespace Game
{
	SimulationActorMovementSystem::SimulationActorMovementSystem(
		SimulationActorsMovementData& simulationActorsMovementData,
		SimulationActorsMovementDataManager& simulationActorsMovementDataManager,
		SimulationActorsMovementLogic& simulationActorsMovementLogic
	) :
		m_SimulationActorsMovementData(simulationActorsMovementData),
		m_SimulationActorsMovementDataManager(simulationActorsMovementDataManager),
		m_SimulationActorsMovementLogic(simulationActorsMovementLogic)
	{
	}



	SimulationActorMovementSystem::~SimulationActorMovementSystem() = default;



	SimulationActorsMovementData* SimulationActorMovementSystem::GetSimulationActorsMovementData() const
	{
		return &m_SimulationActorsMovementData;
	}



	void SimulationActorMovementSystem::RegisterActorAsMovementCapable(ActorNetworkId actorNetworkId) const
	{
		m_SimulationActorsMovementDataManager.RegisterNewActorPendingToAdd(actorNetworkId);
		m_SimulationActorsMovementDataManager.ApplyPendingOperations();
	}



	void SimulationActorMovementSystem::SetActorPath(ActorNetworkId actorNetworkId, std::shared_ptr<Path> actorPath) const
	{
		m_SimulationActorsMovementLogic.SetPath(actorNetworkId, std::move(actorPath));
	}



	void SimulationActorMovementSystem::SetActorMovementSpeed(ActorNetworkId actorNetworkId, simfix64 movementSpeed) const
	{
		const auto actorIndex = m_SimulationActorsMovementData.GetNetworkActorIndex(actorNetworkId);
		NestAssert(actorIndex >= 0);

		m_SimulationActorsMovementData.actorsDistanceMovementInformation[actorIndex].speed = movementSpeed;
	}



	Nest::Vector2<simfix64> SimulationActorMovementSystem::GetActorNextPosition(ActorNetworkId actorNetworkId) const
	{
		const auto actorIndex = m_SimulationActorsMovementData.GetNetworkActorIndex(actorNetworkId);
		NestAssert(actorIndex >= 0);

		return Nest::Vector2<simfix64>{
			m_SimulationActorsMovementData.actorsInFrameMovementPositions[actorIndex].frameEndX,
			m_SimulationActorsMovementData.actorsInFrameMovementPositions[actorIndex].frameEndY
		};
	}



	void SimulationActorMovementSystem::UpdateActorsMovementData(simfix64 deltaTime) const
	{
		m_SimulationActorsMovementDataManager.ApplyPendingOperations();
		m_SimulationActorsMovementLogic.UpdateActorsMovementDistance(deltaTime);
		m_SimulationActorsMovementLogic.UpdateActorsMovementInFrame();
	}



	void SimulationActorMovementSystem::GetMovementInFramePositionsForActors(
		const std::vector<ActorNetworkId>& in_Ids,
		std::vector<ActorInFrameMovementPositions>& out_ActorInFrameMovementPositions
	) const
	{
		out_ActorInFrameMovementPositions.clear();

		std::vector<int> actorsIndexes{};
		actorsIndexes.reserve(in_Ids.size());

		// TODO : curiosity - if get all actors indexes by list improves performance if implemented on actors movement data instead
		for (const auto& actorId : in_Ids)
			actorsIndexes.emplace_back(m_SimulationActorsMovementData.GetNetworkActorIndex(actorId));

		for (const auto& actorIndex : actorsIndexes)
		{
			NestAssert(actorIndex >= 0);
			out_ActorInFrameMovementPositions.emplace_back(m_SimulationActorsMovementData.actorsInFrameMovementPositions[actorIndex]);
		}
	}
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/SimulationCommons/ActorBehavior.h"



namespace Game
{
	ActorBehavior::ActorBehavior() :
		m_Id{ 0 }
	{
		// Nothing here
	}



	ActorBehavior::~ActorBehavior()
	{
		// Nothing here
	}



	void ActorBehavior::SetActorId(ActorId newId)
	{
		m_Id = newId;
	}



	ActorId ActorBehavior::GetActorId() const
	{
		return m_Id;
	}



	void ActorBehavior::SetNetworkId(ActorNetworkId newNetworkId)
	{
		m_NetworkId = newNetworkId;
	}



	ActorNetworkId ActorBehavior::GetNetworkId() const
	{
		return m_NetworkId;
	}



	void ActorBehavior::SetNetworkEntityFlag(bool isNetworkEntity)
	{
		m_IsNetworkEntity = isNetworkEntity;
	}



	bool ActorBehavior::IsNetworkEntity() const
	{
		return m_IsNetworkEntity;
	}



	void ActorBehavior::OnDestroy()
	{
		// Nothing here
	}
}
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/SimulationCommons/SimulationActorsMovementDataManager.h"

#include<Nest/NestAssert.h>

#include"Game/SimulationCommons/SimulationActorsMovementData.h"



namespace Game
{
	SimulationActorsMovementDataManager::SimulationActorsMovementDataManager(
		SimulationActorsMovementData& simulationActorsMovementData
	) :
		m_SimulationActorsMovementData(simulationActorsMovementData)
	{
	}



	SimulationActorsMovementDataManager::~SimulationActorsMovementDataManager() = default;



	void SimulationActorsMovementDataManager::RegisterNewActorPendingToAdd(ActorNetworkId actorNetworkId)
	{
		NestAssert(actorNetworkId != InvalidActorNetworkId);
		m_ActorsPendingToAdd.emplace_back(actorNetworkId);
	}



	void SimulationActorsMovementDataManager::RegisterNewActorPendingToRemove(ActorNetworkId actorNetworkId)
	{
		NestAssert(actorNetworkId != InvalidActorNetworkId);
		m_ActorsPendingToRemove.emplace_back(actorNetworkId);
	}



	void SimulationActorsMovementDataManager::ApplyPendingOperations()
	{
		ApplyPendingInserts();
		ApplyPendingRemoves();
	}



	void SimulationActorsMovementDataManager::ApplyPendingInserts()
	{
		for (const auto& actorNetworkId : m_ActorsPendingToAdd)
		{
			m_SimulationActorsMovementData.RegisterNetworkActor(actorNetworkId);
		}

		m_ActorsPendingToAdd.clear();
	}



	void SimulationActorsMovementDataManager::ApplyPendingRemoves()
	{
		for (const auto& actorNetworkId : m_ActorsPendingToRemove)
		{
			if (m_SimulationActorsMovementData.HasNetworkActor(actorNetworkId))
				m_SimulationActorsMovementData.UnregisterNetworkActor(actorNetworkId);
		}

		m_ActorsPendingToRemove.clear();
	}
}

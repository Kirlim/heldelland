
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/SimulationCommons/SimulationTickValue.h"



namespace Game
{
    const unsigned int SimulationTickValue::MaxTickValue = 65000;



    SimulationTickValue::SimulationTickValue()
    {
        // Nothing here
    }



    SimulationTickValue::~SimulationTickValue()
    {
        // Nothing here
    }



    void SimulationTickValue::SetCurrent(unsigned int newCurrentTickValue)
    {
        m_CurrentTick = newCurrentTickValue;
    }



    unsigned int SimulationTickValue::GetCurrent() const
    {
        return(m_CurrentTick);
    }



    unsigned int SimulationTickValue::GetNext() const
    {
        unsigned int nextTick = m_CurrentTick;
        ++nextTick;

        if (nextTick > MaxTickValue) nextTick = 0;

        return(nextTick);
    }



    unsigned int SimulationTickValue::GetPrevious() const
    {
        unsigned int previousTick;

        if (m_CurrentTick > 0)
        {
            previousTick = m_CurrentTick - 1;
        }
        else
        {
            previousTick = MaxTickValue;
        }

        return(previousTick);
    }



    void SimulationTickValue::Increment()
    {
        m_CurrentTick = GetNext();
    }
}

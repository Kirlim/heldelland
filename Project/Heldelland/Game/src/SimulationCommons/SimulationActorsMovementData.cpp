//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/SimulationCommons/SimulationActorsMovementData.h"

#include<algorithm>

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/NestAssert.h>



namespace
{
	bool ValidateAllVectorsSize(const Game::SimulationActorsMovementData& data)
	{
		const auto correctSize = data.actorsNetworkIds.size();

		return
		(
			data.actorsDistanceMovementInformation.size() == correctSize &&
			data.actorsPathInformation.size() == correctSize &&
			data.actorsInFrameMovementPositions.size() == correctSize
		);
	}
}

namespace Game
{
	SimulationActorsMovementData::~SimulationActorsMovementData() = default;



	void SimulationActorsMovementData::RegisterNetworkActor(ActorNetworkId id)
	{
		actorsNetworkIds.emplace_back(id);

		actorsDistanceMovementInformation.emplace_back();
		actorsPathInformation.emplace_back();
		actorsInFrameMovementPositions.emplace_back();

		NestAssert(ValidateAllVectorsSize(*this));
	}



	void SimulationActorsMovementData::UnregisterNetworkActor(ActorNetworkId id)
	{
		const auto actorIndex = GetNetworkActorIndex(id);
		if (actorIndex < 0)
			ThrowLogicErrorExceptionWithLine("Attempted to remove a not registered Actor Network Id");

		const auto lastIndex = actorsNetworkIds.size();

		std::swap(actorsNetworkIds[actorIndex], actorsNetworkIds[lastIndex - 1]);
		std::swap(actorsDistanceMovementInformation[actorIndex], actorsDistanceMovementInformation[lastIndex - 1]);
		std::swap(actorsPathInformation[actorIndex], actorsPathInformation[lastIndex - 1]);
		std::swap(actorsInFrameMovementPositions[actorIndex], actorsInFrameMovementPositions[lastIndex - 1]);

		actorsNetworkIds.pop_back();
		actorsDistanceMovementInformation.pop_back();
		actorsPathInformation.pop_back();
		actorsInFrameMovementPositions.pop_back();

		NestAssert(ValidateAllVectorsSize(*this));
	}



	bool SimulationActorsMovementData::HasNetworkActor(ActorNetworkId id) const
	{
		return (GetNetworkActorIndex(id) >= 0);
	}



	int SimulationActorsMovementData::GetNetworkActorIndex(ActorNetworkId id) const
	{
		const auto findResult = std::find_if(
			std::begin(actorsNetworkIds), std::end(actorsNetworkIds),
			[&id](const ActorNetworkId& actorNetworkId)
			{
				return actorNetworkId == id;
			}
		);

		if (findResult != std::end(actorsNetworkIds))
		{
			return int(findResult - std::begin(actorsNetworkIds));
		}

		return -1;
	}



	void SimulationActorsMovementData::GetNetworkActorsIndexes(const std::vector<ActorNetworkId>& searchActorsNetworkIds, std::vector<int>& out_Indexes) const
	{
		out_Indexes.clear();

		for (const auto& actorNetworkId : searchActorsNetworkIds)
		{
			for (decltype(actorsNetworkIds.size()) i = 0; i < actorsNetworkIds.size(); ++i)
			{
				if (actorsNetworkIds[i] == actorNetworkId)
					out_Indexes.emplace_back(i);
			}
		}
	}
}

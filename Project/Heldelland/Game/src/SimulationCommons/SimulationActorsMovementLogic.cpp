//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/SimulationCommons/SimulationActorsMovementLogic.h"

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NullArgumentException.h>

#include"Game/Pathfinding/Path.h"
#include"Game/SimulationCommons/SimulationActorsMovementData.h"



namespace Game
{
	SimulationActorsMovementLogic::SimulationActorsMovementLogic(SimulationActorsMovementData& simulationActorsMovementData) :
		m_SimulationActorsMovementData(simulationActorsMovementData)
	{
	}



	SimulationActorsMovementLogic::~SimulationActorsMovementLogic() = default;



	void SimulationActorsMovementLogic::SetPath(ActorNetworkId actorNetworkId, std::shared_ptr<Path> path) const
	{
		if (path == nullptr)
			ThrowNullArgumentExceptionWithLine("Path cannot be null");

		const auto actorIndex = m_SimulationActorsMovementData.GetNetworkActorIndex(actorNetworkId);
		if (actorIndex < 0)
			ThrowLogicErrorExceptionWithLine("Actor Network Id is not registered");

		m_SimulationActorsMovementData.actorsDistanceMovementInformation[actorIndex].distance = simfix64{ 0 };
		m_SimulationActorsMovementData.actorsPathInformation[actorIndex].path = std::move(path);
	}



	void SimulationActorsMovementLogic::UpdateActorsMovementDistance(simfix64 deltaTime) const
	{
		// TODO : how to handle over-path distances?
		for (auto& movementDistance : m_SimulationActorsMovementData.actorsDistanceMovementInformation)
		{
			movementDistance.distance += movementDistance.speed * deltaTime;
		}
	}



	void SimulationActorsMovementLogic::UpdateActorsMovementInFrame() const
	{
		for (decltype(m_SimulationActorsMovementData.actorsNetworkIds.size()) i = 0; i < m_SimulationActorsMovementData.actorsNetworkIds.size(); ++i)
		{
			if (m_SimulationActorsMovementData.actorsPathInformation[i].path != nullptr)
			{
				const auto& distance = m_SimulationActorsMovementData.actorsDistanceMovementInformation[i].distance;
				const auto newPosition = m_SimulationActorsMovementData.actorsPathInformation[i].path->GetPositionByDistance(distance);

				auto& inFramePositions = m_SimulationActorsMovementData.actorsInFrameMovementPositions[i];
				inFramePositions.frameStartX = inFramePositions.frameEndX;
				inFramePositions.frameStartY = inFramePositions.frameEndY;
				inFramePositions.frameEndX = newPosition.x;
				inFramePositions.frameEndY = newPosition.y;
			}
			else
			{
				auto& inFramePositions = m_SimulationActorsMovementData.actorsInFrameMovementPositions[i];
				inFramePositions.frameStartX = inFramePositions.frameEndX;
				inFramePositions.frameStartY = inFramePositions.frameEndY;
			}
		}
	}
}

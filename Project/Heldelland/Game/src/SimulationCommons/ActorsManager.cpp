//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/SimulationCommons/ActorsManager.h"

#include<stdexcept>
#include<algorithm>

#include<Nest/IEntity.h>
#include<Nest/EntitiesManager.h>

#include"Game/SimulationCommons/ActorBehavior.h"



namespace Game
{
	ActorsManager::ActorsManager(Nest::EntitiesManager& entitiesManager) :
		m_EntitiesManager(entitiesManager)
	{
	}



	ActorId ActorsManager::InstantiateNew(bool isNetworkEntity)
	{
		const auto nextId = GetNextId();
		ActorNetworkId networkId{ 0 };

		if (isNetworkEntity)
		{
			networkId = GetNextNetworkId();
		}

		InstantiateNew(nextId, isNetworkEntity, networkId);
		return nextId;
	}



	ActorId ActorsManager::InstantiateNew(ActorNetworkId networkId)
	{
		const auto nextId = GetNextId();
		InstantiateNew(nextId, true, networkId);
		return nextId;
	}



	void ActorsManager::InstantiateNew(ActorId desiredActorId, bool isNetworkEntity, ActorNetworkId networkId)
	{
		// TODO : unit testing instantiating
		if (m_Actors.count(desiredActorId) > 0)
		{
			std::string message = "Failed to instantiate actor: there is already an actor with given id [" + std::to_string(desiredActorId.value) + "].";
			throw std::runtime_error{ message.c_str() };
		}

		// Instantiate new entity
		auto entityWPtr = m_EntitiesManager.Instantiate();
		auto entity = entityWPtr.lock();

		// Add the Actor Behavior
		auto actorBehavior = entity->AddComponent<ActorBehavior>();

		// Setup initial data
		actorBehavior->SetActorId(desiredActorId);
		if (isNetworkEntity)
		{
			actorBehavior->SetNetworkEntityFlag(true);
			actorBehavior->SetNetworkId(networkId);
		}
		else
		{
			actorBehavior->SetNetworkEntityFlag(false);
			actorBehavior->SetNetworkId(ActorNetworkId{ 0 });
		}

		// Store the actor
		m_Actors[desiredActorId] = actorBehavior;
	}



	Nest::ComponentReference<ActorBehavior> ActorsManager::GetActor(ActorId id) const
	{
		if (m_Actors.count(id) > 0)
		{
			return Nest::ComponentReference<ActorBehavior>(m_Actors.at(id));
		}
		else
		{
			return Nest::ComponentReference<ActorBehavior>();
		}
	}



	Nest::ComponentReference<ActorBehavior> ActorsManager::GetActorWithNetworkId(ActorNetworkId networkId) const
	{
		for (auto& actor : m_Actors)
		{
			const auto& actorBehavior = actor.second;
			if (actorBehavior->GetNetworkId() == networkId)
			{
				return (Nest::ComponentReference<ActorBehavior>(actorBehavior));
			}
		}

		return Nest::ComponentReference<ActorBehavior>();
	}



	void ActorsManager::GetActorsNetworkIdsByAppending(
		const std::vector<ActorId>& actorsIds,
		std::vector<ActorNetworkId>& out_ActorsNetworkIds
	) const
	{
		std::for_each(
			std::begin(actorsIds), std::end(actorsIds),
			[this, &out_ActorsNetworkIds](const ActorId& actorId)
			{
				const auto actor = GetActor(actorId);
				NestAssert(actor != nullptr);
				NestAssert(actor->IsNetworkEntity());

				out_ActorsNetworkIds.emplace_back(actor->GetNetworkId());
			}
		);
	}



	ActorId ActorsManager::GetNextId()
	{
		return ActorId{ m_NextActorId.value++ };
	}



	ActorNetworkId ActorsManager::GetNextNetworkId()
	{
		return ActorNetworkId{ m_NextNetworkId.value++ };
	}
}

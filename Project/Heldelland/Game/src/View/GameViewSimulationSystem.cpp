//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/View/GameViewSimulationSystem.h"

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NullArgumentException.h>

#include"Game/View/IGameViewSimulation.h"



namespace Game
{
	GameViewSimulationSystem::~GameViewSimulationSystem() = default;



	void GameViewSimulationSystem::StartViewSimulation(std::unique_ptr<IGameViewSimulation>& gameViewSimulation)
	{
		if (gameViewSimulation == nullptr)
			ThrowNullArgumentExceptionWithLine("Game View Simulation cannot be null");

		if (m_GameViewSimulation != nullptr)
			ThrowLogicErrorExceptionWithLine("There is already a Game View Simulation set up");

		m_GameViewSimulation = std::move(gameViewSimulation);
	}



	IGameViewSimulation* GameViewSimulationSystem::GetViewSimulation() const
	{
		return m_GameViewSimulation.get();
	}



	void GameViewSimulationSystem::DoUpdate(float deltaTime)
	{
		if (m_GameViewSimulation != nullptr)
			m_GameViewSimulation->UpdateViewSimulation(deltaTime);
	}
}

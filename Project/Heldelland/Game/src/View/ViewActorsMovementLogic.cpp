//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/View/ViewActorsMovementLogic.h"

#include<algorithm>

#include"Game/Map/ITerrainView.h"



namespace Game
{
	ViewActorsMovementLogic::~ViewActorsMovementLogic() = default;



	void ViewActorsMovementLogic::CalculateActorsPositions(
		float simulationTickTimeElapsedPct,
		const std::vector<ActorNetworkId>& actorsNetworkIds,
		const std::vector<ActorInFrameMovementPositions>& actorsPositions
	)
	{
		simulationTickTimeElapsedPct = std::clamp(simulationTickTimeElapsedPct, 0.0f, 1.0f);

		this->actorsNetworkIds.clear();
		this->actorsPositions.clear();

		for (const auto& actorNetworkId : actorsNetworkIds)
			this->actorsNetworkIds.emplace_back(actorNetworkId);

		for (const auto& actorInFramePosition : actorsPositions)
		{
			this->actorsPositions.emplace_back(
				actorInFramePosition.frameStartX.ToFloat()
				+ (actorInFramePosition.frameEndX.ToFloat() - actorInFramePosition.frameStartX.ToFloat()) * simulationTickTimeElapsedPct,

				actorInFramePosition.frameStartY.ToFloat()
				+ (actorInFramePosition.frameEndY.ToFloat() - actorInFramePosition.frameStartY.ToFloat()) * simulationTickTimeElapsedPct
			);
		}
	}



	void ViewActorsMovementLogic::CalculateActorsPositionsHeights(
		const ITerrainView& terrainView
	)
	{
		terrainView.GetPositionsHeights(actorsPositions, /*out*/ actorsPositionHeights);
	}
}

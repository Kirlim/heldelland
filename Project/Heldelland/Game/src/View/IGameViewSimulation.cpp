//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/View/IGameViewSimulation.h"



namespace Game
{
	IGameViewSimulation::~IGameViewSimulation() = default;



	void IGameViewSimulation::UpdateViewSimulation(float deltaTime) const
	{
		DoUpdateViewSimulation(deltaTime);
	}



	void IGameViewSimulation::BindToGameClientSimulation(IGameClientSimulation& gameClientSimulation)
	{
		DoBindToGameClientSimulation(gameClientSimulation);
		DoRegisterToClientSimulationEvents(gameClientSimulation);
	}
}

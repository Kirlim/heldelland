
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/View/SimpleViewActorsSpawner.h"

#include<stdexcept>



namespace Game
{
    SimpleViewActorsSpawner::SimpleViewActorsSpawner()
    {
        // Nothing here
    }



    SimpleViewActorsSpawner::~SimpleViewActorsSpawner() = default;



    Nest::IEntity* SimpleViewActorsSpawner::Spawn(ActorTypeId actorTypeId) const
    {
        if (m_SpawnFunctions.count(actorTypeId) == 0)
        {
            throw std::runtime_error("Trying to spawn from unknown actor type id.");
        }

        auto& function = m_SpawnFunctions.at(actorTypeId);
        return function();
    }



    void SimpleViewActorsSpawner::SetSpawnFunction(ActorTypeId actorTypeId, std::function<Nest::IEntity*()> function)
    {
        if (m_SpawnFunctions.count(actorTypeId) > 0)
        {
            throw std::runtime_error("Trying to register spawn function with already registered actor type id.");
        }

        if (function == nullptr)
        {
            throw std::runtime_error("Trying to register null spawn function.");
        }

        m_SpawnFunctions[actorTypeId] = function;
    }
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/View/ViewActorsManager.h"

#include<algorithm>

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NullArgumentException.h>



namespace Game
{
	ViewActorsManager::~ViewActorsManager() = default;



	void ViewActorsManager::RegisterMovingActor(ActorNetworkId actorNetworkId, Nest::EntityId entityId)
	{
		if (IsMovingActorRegistered(actorNetworkId))
			ThrowLogicErrorExceptionWithLine("Trying to register repeated network id [" + std::to_string(actorNetworkId.value) + "]");

		m_MovingActorsContainer.emplace_back(std::make_tuple(actorNetworkId, entityId));
	}



	bool ViewActorsManager::IsMovingActorRegistered(ActorNetworkId actorNetworkId) const
	{
		return std::any_of(
			std::begin(m_MovingActorsContainer), std::end(m_MovingActorsContainer),
			[actorNetworkId](std::tuple<ActorNetworkId, Nest::EntityId> tuple)
			{
				return (std::get<ActorNetworkId>(tuple) == actorNetworkId);
			}
		);
	}



	bool ViewActorsManager::UnregisterMovingActorIfRegistered(ActorNetworkId actorNetworkId)
	{
		const auto searchResultIt = std::find_if(
			std::begin(m_MovingActorsContainer), std::end(m_MovingActorsContainer),
			[actorNetworkId](std::tuple<ActorNetworkId, Nest::EntityId> tuple)
			{
				return (std::get<ActorNetworkId>(tuple) == actorNetworkId);
			}
		);

		if (searchResultIt != std::end(m_MovingActorsContainer))
		{
			m_MovingActorsContainer.erase(searchResultIt);
			return true;
		}

		return false;
	}



	void ViewActorsManager::ExecuteForAllRegisteredMovingActors(
		std::function<void(MovingActorsConstIterator /*begin*/, MovingActorsConstIterator /*end*/)> action
	) const
	{
		if (action == nullptr)
			ThrowNullArgumentExceptionWithLine("Action cannot be null");

		action(std::cbegin(m_MovingActorsContainer), std::cend(m_MovingActorsContainer));
	}
}

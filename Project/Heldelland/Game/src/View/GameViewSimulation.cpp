//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/View/GameViewSimulation.h"

#include<cmath>

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NullArgumentException.h>
#include<Nest/Quaternion.h>
#include<Nest/Events/EventsBusToken.h>

#include<Nest/RenderingModule.h>
#include<Nest/IEntity.h>
#include<Nest/EntitiesManager.h>
#include<Nest/World.h>

#include<Nest/Camera.h>
#include<Nest/Transform.h>
#include<Nest/DirectionalLightComponent.h>

#include"Game/Map/Map.h"
#include"Game/Map/MapView.h"
#include"Game/Map/ITerrainView.h"
#include"Game/Map/IMapViewFactory.h"
#include"Game/BirdEyeCameraBehavior.h"

#include"Game/View/IViewActorsSpawner.h"
#include"Game/View/ViewActorsManager.h"
#include"Game/View/ViewActorsMovementLogic.h"

#include"Game/SimulationCommons/SimulationActorMovementSystem.h"
#include"Game/Simulation/IGameClientSimulation.h"



namespace Game
{
	GameSimulationView::GameSimulationView(
		Nest::EntitiesManager& entitiesManager,
		Map& map,
		const IMapViewFactory& mapViewFactory,
		Nest::RenderingModule& renderingModule,
		Nest::World& world,
		Nest::RenderingManager& renderingManager,
		std::unique_ptr<IViewActorsSpawner>& viewActorsSpawner,
		std::unique_ptr<ViewActorsMovementLogic>& viewActorsMovementLogic
	) :
		m_EntitiesManager(entitiesManager),
		m_RenderingManager(renderingManager),
		m_ViewActorsManager(std::make_unique<ViewActorsManager>()),
		m_MapViewFactory(mapViewFactory)
	{
		if (viewActorsSpawner == nullptr)
			ThrowNullArgumentExceptionWithLine("View Actors Spawner cannot be null");

		if (viewActorsMovementLogic == nullptr)
			ThrowNullArgumentExceptionWithLine("View Actors Movement Logic cannot be null");

		InitializeMapView(map);
		InitializeMapLights();
		InitializeViewCamera(renderingModule, world);

		m_ViewActorsSpawner = std::move(viewActorsSpawner);
		m_ViewActorsMovementLogic = std::move(viewActorsMovementLogic);
	}



	void UpdateActorsPositions(
		const ViewActorsMovementLogic& viewActorsMovementLogic,
		const ViewActorsManager& viewActorsManager,
		const Nest::EntitiesManager& entitiesManager
	)
	{
		const auto& actorsNetworkIds = viewActorsMovementLogic.actorsNetworkIds;
		const auto& actorsPositions = viewActorsMovementLogic.actorsPositions;
		const auto& actorsPositionHeights = viewActorsMovementLogic.actorsPositionHeights;

		viewActorsManager.ExecuteForAllRegisteredMovingActors(
			[&](ViewActorsManager::MovingActorsConstIterator begin, ViewActorsManager::MovingActorsConstIterator end)
			{
				while (begin != end)
				{
					for (decltype(actorsNetworkIds.size()) i = 0; i < actorsNetworkIds.size(); ++i)
					{
						if (actorsNetworkIds[i] != std::get<ActorNetworkId>(*begin))
							continue;

						auto actorEntity = entitiesManager.GetById(std::get<Nest::EntityId>(*begin)).lock();
						auto actorTransform = actorEntity->GetComponent<Nest::Transform>();
						NestAssert(actorTransform != nullptr);

						const auto& position = actorsPositions[i];
						const auto& height = actorsPositionHeights[i];
						actorTransform->SetLocalPosition(Nest::Vector3f{ position.x, height, position.y });
					}

					++begin;
				}
			}
		);
	}



	void GameSimulationView::DoUpdateViewSimulation(float deltaTime) const
	{
		if (m_GameClientSimulation == nullptr)
			return;

		CalculateActorsPositions();

		UpdateActorsPositions(*m_ViewActorsMovementLogic, *m_ViewActorsManager, m_EntitiesManager);
	}



	void GameSimulationView::CalculateActorsPositions() const
	{
		const auto simulationActorsMovementData =
			m_GameClientSimulation->GetSimulationActorMovementSystem()->GetSimulationActorsMovementData();

		m_ViewActorsMovementLogic->CalculateActorsPositions(
			m_GameClientSimulation->GetCurrentTickElapsedTimePercentage(),
			simulationActorsMovementData->actorsNetworkIds,
			simulationActorsMovementData->actorsInFrameMovementPositions
		);

		NestAssert(m_MapView->GetTerrainView() != nullptr);
		m_ViewActorsMovementLogic->CalculateActorsPositionsHeights(*m_MapView->GetTerrainView());
	}



	void GameSimulationView::DoBindToGameClientSimulation(IGameClientSimulation& gameClientSimulation)
	{
		if (m_GameClientSimulation != nullptr)
			ThrowLogicErrorExceptionWithLine("The Simulation View is already bound to a client simulation");


		m_GameClientSimulation = &gameClientSimulation;
	}



	void GameSimulationView::DoRegisterToClientSimulationEvents(IGameClientSimulation& gameClientSimulation)
	{
		m_OnActorSpawnedListenerToken = gameClientSimulation.RegisterToActorSpawnedEvent(
			[this](const ActorSpawnedEvt& evt) { OnActorSpawnedEvent(evt); }
		);
	}



	void GameSimulationView::InitializeMapView(Map& map)
	{
		m_MapView = m_MapViewFactory.Instantiate(map);
		m_MapView->InstantiateHierarchyOfEntities();
	}



	void GameSimulationView::InitializeMapLights() const
	{
		// TODO : read directional lights from map

		auto directionalLightEntity = m_EntitiesManager.Instantiate().lock();
		auto directionalLightTransform = directionalLightEntity->AddComponent<Nest::Transform>();
		auto directionalLightComponent = directionalLightEntity->AddComponent<Nest::DirectionalLightComponent>();

		auto lightDirection = Nest::Quaternion<float>::Identity();
		lightDirection.SetRotation(
			float(cos(-0.4f)), float(sin(-0.4)),
			Nest::Vector3<float>(float(1.0f), float(0.0f), float(0.0f))
		);

		directionalLightTransform->SetLocalRotation(lightDirection);
	}



	void GameSimulationView::InitializeViewCamera(Nest::RenderingModule& renderingModule, Nest::World& world) const
	{
		auto cameraEntity = m_EntitiesManager.Instantiate().lock();
		auto cameraTransform = cameraEntity->AddComponent<Nest::Transform>();
		auto cameraComponent = cameraEntity->AddComponent<Nest::Camera>();
		auto cameraFollowRulesComponent = cameraEntity->AddComponent<BirdEyeCameraBehavior>();

		const auto screenWidth = float(renderingModule.GetScreenWidth());
		const auto screenHeight = float(renderingModule.GetScreenHeight());
		const auto aspect = screenWidth / screenHeight;
		cameraComponent->SetAsPerspective(aspect, 1.0f, 0.01f, 100.0f, 3.1415f * 0.25f);

		world.GetRenderingManager()->SetActiveCamera(cameraComponent);
	}



	void GameSimulationView::OnActorSpawnedEvent(const ActorSpawnedEvt& actorSpawnedEvent) const
	{
		NestAssert(m_GameClientSimulation != nullptr);

		if (actorSpawnedEvent.typeId != ActorTypeId{ ACTOR_TYPE_PLAYER })
			ThrowLogicErrorExceptionWithLine("Currently, only player actor types are accepted");

		SpawnActor(actorSpawnedEvent.networkId);
	}



	void GameSimulationView::SpawnActor(ActorNetworkId actorNetworkId) const
	{
		auto actorEntity = m_ViewActorsSpawner->Spawn(ActorTypeId{ ACTOR_TYPE_PLAYER });
		m_ViewActorsManager->RegisterMovingActor(actorNetworkId, actorEntity->Id());

		if (m_GameClientSimulation->GetPlayerActorNetworkId() == actorNetworkId)
		{
			const auto camera = m_RenderingManager.GetActiveCamera();
			if (camera == nullptr)
				ThrowNullPointerExceptionWithLine("Camera is null");

			auto birdEyeCamera = camera->GetOwner()->GetComponent<BirdEyeCameraBehavior>();
			if (birdEyeCamera == nullptr)
				ThrowNullPointerExceptionWithLine("BirdEyeCameraBehavior not found on spawned actor instance");


			const auto actorTransform = actorEntity->GetComponent<Nest::Transform>();
			if (actorTransform == nullptr)
				ThrowNullPointerExceptionWithLine("Transform not found on spawned actor instance");

			birdEyeCamera->SetFollowTarget(actorTransform);
		}
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/View/GameViewSimulationFactory.h"

#include<Nest/Log.h>

#include<Nest/Events/EventsBusToken.h>
#include<Nest/Transform.h>
#include<Nest/TextMesh.h>
#include<Nest/MeshRenderer.h>
#include<Nest/IEntity.h>
#include<Nest/EntitiesManager.h>
#include<Nest/EntityFromModelBuilder.h>
#include<Nest/World.h>

#include"Game/Map/Map.h"
#include"Game/Map/MapView.h"
#include"Game/Map/IMapViewFactory.h"

#include"Game/View/ViewActorsManager.h"
#include"Game/View/IGameViewSimulation.h"
#include"Game/View/GameViewSimulation.h"

#include"Game/View/SimpleViewActorsSpawner.h"
#include"Game/View/ViewActorsMovementLogic.h"

#include"Nest/MeshFilter.h"



namespace
{
	std::unique_ptr<Game::IViewActorsSpawner> InstantiateViewActorsSpawner(
		Nest::EntitiesManager& entitiesManager,
		const Nest::IResourcesManager& resourcesManager
	)
	{
		auto spawner = std::make_unique<Game::SimpleViewActorsSpawner>();

		spawner->SetSpawnFunction(Game::ActorTypeId{ ACTOR_TYPE_PLAYER }, [&]()
		{
			auto entity = entitiesManager.Instantiate().lock();

			entity->AddComponent<Nest::MeshFilter>();
			entity->AddComponent<Nest::MeshRenderer>();
			auto transform = entity->AddComponent<Nest::Transform>();

			auto model = resourcesManager.FetchModel("PlayerTestModel");
			NestAssert(model != nullptr);

			Nest::EntityFromModelBuilder entityFromModelBuilder{ entitiesManager, resourcesManager };
			auto modelEntity = entityFromModelBuilder.BuildVisualEntity(*model);
			auto modelTransform = modelEntity->GetComponent<Nest::Transform>();

			modelTransform->SetParent(transform);

			Nest::Log::Message("Instantiated new Player Actor View");

			return entity.get();
		});

		return spawner;
	}
}



namespace Game
{
	GameViewSimulationFactory::GameViewSimulationFactory(
		Nest::EntitiesManager& entitiesManager,
		Nest::RenderingModule& renderingModule,
		Nest::World& world,
		const IMapViewFactory& mapViewFactory
	) :
		m_EntitiesManager(entitiesManager),
		m_RenderingModule(renderingModule),
		m_World(world),
		m_MapViewFactory(mapViewFactory)
	{
	}



	std::unique_ptr<IGameViewSimulation> GameViewSimulationFactory::Instantiate(Map& map) const
	{
		auto viewActorsSpawner = InstantiateViewActorsSpawner(m_EntitiesManager, *m_World.GetResourcesManager());

		auto viewActorsMovementLogic = std::make_unique<ViewActorsMovementLogic>();

		std::unique_ptr<IGameViewSimulation> simulationView = std::make_unique<GameSimulationView>(
			m_EntitiesManager,
			map,
			m_MapViewFactory,
			m_RenderingModule,
			m_World,
			*m_World.GetRenderingManager(),
			viewActorsSpawner,
			viewActorsMovementLogic
		);


		return simulationView;
	}



	void GameViewSimulationFactory::InstantiateShowCurrentTickValueEntity(const IGameViewSimulation& gameSimulationView) const
	{
		auto entity = m_EntitiesManager.Instantiate().lock();
		auto entityTransform = entity->AddComponent<Nest::Transform>();
		auto entityMeshRenderer = entity->AddComponent<Nest::MeshRenderer>();
		auto entityTextMesh = entity->AddComponent<Nest::TextMesh>();

		entityTransform->SetLocalPosition(Nest::Vector3f(340.0f, -275.0f, 0.0f));

		// TODO : remove next set up lines, as prefab will come configured
		const auto resourcesManager = m_World.GetResourcesManager();

		entityTextMesh->SetText("");
		entityTextMesh->SetFontSize(0.35f);
		entityTextMesh->SetFont(resourcesManager->FetchFont("Raleway"));

		entityMeshRenderer->SetMaterial(resourcesManager->FetchMaterial("FontMaterial"));
		entityMeshRenderer->SetGuiFlag(true);
	}
}

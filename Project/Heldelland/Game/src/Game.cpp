//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Game.h"
#include"Game/GameSettings.h"

#include<Nest/Engine.h>
#include<Nest/Cast.h>
#include<Nest/Input.h>

#include<Nest/IResourcesManager.h>
#include<Nest/IEntity.h>
#include<Nest/EntitiesManager.h>
#include<Nest/MeshesFactory.h>

#include<Nest/ITemplatedSingleComponentsContainer.h>
#include<Nest/IComponentsFactory.h>
#include<Nest/IComponentsContainers.h>
#include<Nest/SystemsManager.h>
#include<Nest/World.h>
#include<Nest/Global.h>

#include<Nest/RenderingModule.h> // Right now, only for the enum

#include<Nest/Vector3.h>

#include<Nest/Log.h>


#include"Game/WorldFactory.h"

// Components loading
#include"Game/Load/NestComponentsLoader.h"
#include"Game/Load/GameComponentsLoader.h"



// Nest Engine Components and Systems
#include<Nest/Transform.h>

#include<Nest/TextMesh.h>
#include<Nest/MeshRenderer.h>
#include<Nest/Camera.h>
#include<Nest/NinePatchGraphic.h>

#include<Nest/RenderingSystemOpenGL.h>



// Non-component
#include"Game/Map/MapsFactory.h"
#include"Game/Map/MapViewFactory.h"

// Game Simulation Components and Systems
#include"Game/Simulation/GameClientSimulationSystem.h"
#include"Game/Simulation/GameServerSimulationSystem.h"
#include"Game/Simulation/GameClientSimulationNetworkSystem.h"
#include"Game/Simulation/GameServerSimulationNetworkSystem.h"

#include"Game/SimulationCommons/SimulationActorsMovementData.h"
#include"Game/SimulationCommons/SimulationActorsMovementDataManager.h"
#include"Game/SimulationCommons/SimulationActorsMovementLogic.h"
#include"Game/Simulation/IGameServerSimulation.h"
#include"Game/Simulation/ClientGameSimulation.h"
#include"Game/Simulation/GameSimulationFactory.h"

#include"Game/Simulation/PlayerControlsSystem.h"


// Game Simulation View Components and Systems
#include"Game/SimView/ActorSystem.h"
#include"Game/View/IGameViewSimulation.h"
#include"Game/View/GameViewSimulationFactory.h"
#include"Game/View/GameViewSimulationSystem.h"



// Game Components and Systems
#include"Game/BirdEyeCameraSystem.h"
#include"Game/CameraControlInputObserver.h"

#include"Game/DevelopersConsoleBehavior.h"
#include"Game/DevelopersConsoleSystem.h"

#include"Game/ConsoleCommands/GameNetworkConsoleCommandsHandler.h"


// Global sytems
#include"Game/Network/NetworkSystemFactory.h"
#include"Game/Network/INetworkSystem.h"
#include"Game/NetworkTickingSystem.h"


#include<Nest/NestAssert.h>
#include<Nest/Exceptions/InvalidOperationException.h>
#include<Nest/Exceptions/LogicErrorException.h>



namespace
{
	template <typename SystemType>
	SystemType* FetchSystem(const Nest::SystemsManager& systemsManager)
	{
		auto foundSystem = systemsManager.FindSystem([](Nest::System* system)
		{
			return (dynamic_cast<SystemType*>(system) != nullptr);
		});

		if (foundSystem == nullptr)
			ThrowLogicErrorExceptionWithLine("Desired system not found in the systems manager");

		return DoCast<SystemType>(foundSystem);
	}



	void InstantiateViewSimulationOnNewClientSimulation(
		const Game::NewClientSimulationStartedEvt& evt,
		Game::IGameViewSimulationFactory& viewFactory,
		Game::GameViewSimulationSystem& viewSystem
	)
	{
		NestAssert(evt.gameClientSimulation != nullptr);

		auto viewSimulation = viewFactory.Instantiate(*evt.gameClientSimulation->GetMap());
		// TODO : consider call binding on constructor?
		viewSimulation->BindToGameClientSimulation(*evt.gameClientSimulation);
		viewSystem.StartViewSimulation(viewSimulation);
	}
}



namespace Game
{
	namespace Helpers
	{
		void StartNewGameServerSimulation(
			const Nest::World& serverWorld,
			const MapsFactory& mapsFactory,
			GameServerSimulationSystem* gameServerSimulationSystem,
			SimulationActorsMovementData& serverSimulationActorsMovementData,
			SimulationActorsMovementDataManager& serverSimulationActorsMovementDataManager,
			SimulationActorsMovementLogic& serverSimulationActorsMovementLogic
		);
	}



	std::unique_ptr<INetworkSystem> Network = nullptr;



	// TODO : customize SplitString method, and up it into a better file
	std::vector<std::string> SplitString(const std::string& text, char sep)
	{
		std::vector<std::string> tokens;
		std::size_t start = 0, end = 0;
		while ((end = text.find(sep, start)) != std::string::npos)
		{
			if (end != start)
			{
				tokens.push_back(text.substr(start, end - start));
			}
			start = end + 1;
		}
		if (end != start)
		{
			tokens.push_back(text.substr(start));
		}
		return tokens;
	}



	Game::Game(
		Nest::NestEngine& engine,
		const Nest::MeshesFactory& meshesFactory,
		const MapsFactory& mapsFactory
	) :
		m_Engine(engine),
		m_MeshesFactory(meshesFactory),
		m_MapsFactory(mapsFactory)
	{
		SetEngineWorlds();

		m_CameraControlInputObserver = std::make_unique<CameraControlInputObserver>(Nest::Global->GetInput()->GetInputEventsBus());
	}



	Game::~Game() = default;



	void Game::Init(const GameSettings& gameSettings)
	{
		auto viewWorld = GetWorld(GameWorldId::View);
		NestAssert(viewWorld != nullptr);

		auto viewResourcesManager = viewWorld->GetResourcesManager();
		NestAssert(viewResourcesManager != nullptr);

		auto viewSystemsManager = viewWorld->GetSystemsManager();
		NestAssert(viewSystemsManager != nullptr);

		auto viewComponentsFactory = viewWorld->GetComponentsFactory();
		NestAssert(viewComponentsFactory != nullptr);

		auto viewComponentsContainers = viewWorld->GetComponentsContainers();
		NestAssert(viewComponentsContainers != nullptr);

		auto viewEntitiesManager = viewWorld->GetEntitiesManager();
		NestAssert(viewEntitiesManager != nullptr);

		auto renderingModule = m_Engine.GetRenderingModule();
		NestAssert(renderingModule != nullptr);

		auto renderingManager = viewWorld->GetRenderingManager();
		NestAssert(renderingManager != nullptr);


		SetUpMetadataManager(gameSettings);
		SetUpGameSimulationViewFactory(*viewEntitiesManager, *renderingModule, *viewWorld);


		// Load components and systems
		LoadEngineComponents(*viewComponentsFactory, *viewComponentsContainers);
		LoadEngineSystems(*viewSystemsManager);
		LoadEngineRenderingSystem(
			*viewSystemsManager, gameSettings.GetGameRenderer(), *viewComponentsContainers, *renderingManager
		);

		SetUpRenderingSystem(*viewResourcesManager, *viewSystemsManager, gameSettings.GetGameRenderer());

		LoadCommonMeshes(*viewResourcesManager, m_MeshesFactory);

		LoadSimulationComponents();
		LoadSimulationsSystems();

		LoadSimulationViewComponents();
		LoadSimulationViewSystems(*viewSystemsManager);
	}



	void Game::LoadGameScene()
	{
		const auto viewWorld = GetWorld(GameWorldId::View);
		NestAssert(viewWorld != nullptr);

		auto viewEntitiesManager = viewWorld->GetEntitiesManager();
		NestAssert(viewEntitiesManager != nullptr);

		// Create the GUI Camera
		auto guiCameraEntityPtr = viewEntitiesManager->Instantiate();
		auto guiCameraEntity = guiCameraEntityPtr.lock();
		guiCameraEntity->AddComponent<Nest::Transform>();
		guiCameraEntity->AddComponent<Nest::Camera>();
		guiCameraEntity->GetComponent<Nest::Camera>()->SetAsOrthographic(800.0f, 600.0f, -100.0f, 100.0f); // TODO : use game settings

		viewWorld->GetRenderingManager()->SetActiveGuiCamera(guiCameraEntity->GetComponent<Nest::Camera>());


		// Set up the developers console as the logging for the engine
		const auto viewResourcesManager = viewWorld->GetResourcesManager();
		NestAssert(viewResourcesManager != nullptr);

		auto systemsManager = viewWorld->GetSystemsManager();

		const auto system = systemsManager->FindSystem([&](Nest::System* sys) { return (dynamic_cast<DevelopersConsoleSystem*>(sys) != nullptr); });
		NestAssert(system != nullptr);

		const auto developersConsoleSystem = DoCast<DevelopersConsoleSystem>(system);
		NestAssert(developersConsoleSystem != nullptr);

		SetUpDevelopersConsole(*viewEntitiesManager, *viewResourcesManager, *developersConsoleSystem);

		Nest::Log::Message("Game scene loaded!");
	}



	void Game::SetEngineWorlds() const
	{
		WorldFactory worldFactory{};

		const Nest::WorldId clientSimulationWorldId{ static_cast<unsigned int>(GameWorldId::ClientSimulation) };
		auto clientSimulationWorld = worldFactory.InstantiateWorld();
		m_Engine.AddWorld(clientSimulationWorldId, clientSimulationWorld);

		const Nest::WorldId serverSimulationWorldId{ static_cast<unsigned int>(GameWorldId::ServerSimulation) };
		auto serverSimulationWorld = worldFactory.InstantiateWorld();
		m_Engine.AddWorld(serverSimulationWorldId, serverSimulationWorld);

		const Nest::WorldId viewSimulationWorldId{ static_cast<unsigned int>(GameWorldId::View) };
		auto viewSimulationWorld = worldFactory.InstantiateWorld();
		m_Engine.AddWorld(viewSimulationWorldId, viewSimulationWorld);
	}



	void Game::SetUpServerSimulation() const
	{
		const auto serverWorld = GetWorld(GameWorldId::ServerSimulation);

		const auto gameServerSimulationSystem = FetchSystem<GameServerSimulationSystem>(*serverWorld->GetSystemsManager());
		NestAssert(gameServerSimulationSystem != nullptr);

		try
		{
			Helpers::StartNewGameServerSimulation(
				*serverWorld, m_MapsFactory, gameServerSimulationSystem,
				*m_ServerSimulationActorsMovementData, *m_ServerSimulationActorsMovementDataManager, *m_ServerSimulationActorsMovementLogic
			);
			Network->SetLocalInstanceServerFlag(true);
		}
		catch (Nest::LogicErrorException& logicErrorException)
		{
			Nest::Log::Error("[Game] Logic error while trying to set up server simulation:");
			Nest::Log::Error(" => " + std::string(logicErrorException.what()));
		}
	}



	void Helpers::StartNewGameServerSimulation(
		const Nest::World& serverWorld,
		const MapsFactory& mapsFactory,
		GameServerSimulationSystem* gameServerSimulationSystem,
		SimulationActorsMovementData& serverSimulationActorsMovementData,
		SimulationActorsMovementDataManager& serverSimulationActorsMovementDataManager,
		SimulationActorsMovementLogic& serverSimulationActorsMovementLogic
	)
	{
		GameSimulationFactory simulationFactory{
			*serverWorld.GetEntitiesManager(),
			*serverWorld.GetResourcesManager(),
			*serverWorld.GetComponentsContainers(),
			mapsFactory,
			serverSimulationActorsMovementData,
			serverSimulationActorsMovementDataManager,
			serverSimulationActorsMovementLogic
		};

		auto serverSimulation = simulationFactory.InstantiateServerSimulation("testMap");

		NestAssert(gameServerSimulationSystem != nullptr);
		gameServerSimulationSystem->StartServerSimulation(serverSimulation);
	}



	void Game::SetUpMetadataManager(const GameSettings& gameSettings) const
	{
		auto metadataManager = Nest::Global->GetMetadataManager();
		if (metadataManager == nullptr)
			ThrowInvalidOperationExceptionWithLine("There is not a Metadata Manager instance set.");


		metadataManager->SetMetadataExtension(".dfs");

		for (const auto& assetsPath : *(gameSettings.GetAssetsPaths()))
		{
			metadataManager->LoadMetadataFromPath(assetsPath);
		}

		// Load all metadata resources for view
		const auto viewResourcesManager = GetWorld(GameWorldId::View)->GetResourcesManager();
		metadataManager->LoadAllResources(*viewResourcesManager);


		// TODO : the following resources loaded are here due to resources cross dependency between worlds. Remove when these dependencies are eliminated
		const auto clientWorldResourcesManager = GetWorld(GameWorldId::ClientSimulation)->GetResourcesManager();
		metadataManager->LoadAllResources(*clientWorldResourcesManager);

		const auto serverWorldResourcesManager = GetWorld(GameWorldId::ServerSimulation)->GetResourcesManager();
		metadataManager->LoadAllResources(*serverWorldResourcesManager);
	}



	void Game::SetUpGameSimulationViewFactory(
		Nest::EntitiesManager& entitiesManager, Nest::RenderingModule& renderingModule, Nest::World& world
	)
	{
		m_MapViewFactory = std::make_unique<MapViewFactory>(entitiesManager, m_MeshesFactory);
		m_GameViewSimulationFactory = std::make_unique<GameViewSimulationFactory>(entitiesManager, renderingModule, world, *m_MapViewFactory);
	}



	void Game::LoadEngineComponents(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers)
	{
		NestComponentsLoader nestComponentsLoader;
		nestComponentsLoader.Load(componentsFactory, componentsContainers);
	}



	void Game::LoadEngineSystems(Nest::SystemsManager& systemsManager)
	{
		//
	}



	void Game::LoadEngineRenderingSystem(
		Nest::SystemsManager& systemsManager, Nest::GameRenderer gameRenderer,
		const Nest::IComponentsContainers& componentsContainers, const Nest::RenderingManager& renderingManager
	) const
	{
		if (gameRenderer == Nest::GameRenderer::OpenGL)
		{
			auto renderingSystemInstance = std::make_unique<Nest::RenderingSystemOpenGL>(
				*m_Engine.GetRenderingModule()->GetSDLWindow(), componentsContainers, renderingManager
			);
			auto renderingSystem = renderingSystemInstance.get();
			renderingSystem->SetExecutionOrder(2000);
			renderingSystem->Init();

			systemsManager.AddSystem(std::move(renderingSystemInstance));
		}
		else if (gameRenderer == Nest::GameRenderer::Vulkan)
		{
			//auto renderingSystemInstance = std::make_unique<Nest::RenderingSystemVulkan>();
			//auto renderingSystem = renderingSystemInstance.get();
			//renderingSystem->SetExecutionOrder(100);

			//systemsManager->AddSystem(std::move(renderingSystemInstance));
		}
	}



	void Game::LoadSimulationComponents() const
	{
		GameComponentsLoader gameComponentsLoader;

		const auto serverComponentsFactory = GetWorld(GameWorldId::ServerSimulation)->GetComponentsFactory();
		const auto serverComponentsContainers = GetWorld(GameWorldId::ServerSimulation)->GetComponentsContainers();
		gameComponentsLoader.LoadServerSimulationComponents(*serverComponentsFactory, *serverComponentsContainers);

		const auto clientComponentsFactory = GetWorld(GameWorldId::ClientSimulation)->GetComponentsFactory();
		const auto clientComponentsContainers = GetWorld(GameWorldId::ClientSimulation)->GetComponentsContainers();
		gameComponentsLoader.LoadClientSimulationComponents(*clientComponentsFactory, *clientComponentsContainers);
	}



	void Game::LoadSimulationsSystems()
	{
		LoadNetwork();
		LoadServerSimulationSystems();
		LoadClientSimulationSystems();
		LoadViewSystems();

		SetUpGameSimulationNetworkEventsListeners();
	}



	void Game::LoadNetwork()
	{
		NetworkSystemFactory networkSystemFactory;
		Network = networkSystemFactory.Instantiate(5000); // TODO : dont use magic values
	}



	void Game::LoadServerSimulationSystems()
	{
		InstantiateServerSimulationActorsMovementDataManagers();

		const auto serverWorld = GetWorld(GameWorldId::ServerSimulation);
		auto serverSystemsManager = serverWorld->GetSystemsManager();

		serverSystemsManager->AddSystem(std::make_unique<GameServerSimulationSystem>());

		auto gameServerSimulationNetworkSystem = std::make_unique<GameServerSimulationNetworkSystem>(
			*Network,
			*FetchSystem<GameServerSimulationSystem>(*serverSystemsManager),
			*Network->GetMessagesFactory(),
			*Network->GetMessagesHandlersManager(),
			*Network->GetHostsManager()
		);
		serverSystemsManager->AddSystem(std::move(gameServerSimulationNetworkSystem));
	}



	void Game::InstantiateServerSimulationActorsMovementDataManagers()
	{
		NestAssert(m_ServerSimulationActorsMovementData == nullptr);
		NestAssert(m_ServerSimulationActorsMovementDataManager == nullptr);
		NestAssert(m_ServerSimulationActorsMovementLogic == nullptr);

		m_ServerSimulationActorsMovementData = std::make_unique<SimulationActorsMovementData>();
		m_ServerSimulationActorsMovementDataManager = std::make_unique<SimulationActorsMovementDataManager>(
			*m_ServerSimulationActorsMovementData
		);
		m_ServerSimulationActorsMovementLogic = std::make_unique<SimulationActorsMovementLogic>(*m_ServerSimulationActorsMovementData);
	}



	void Game::LoadClientSimulationSystems()
	{
		InstantiateClientSimulationActorsMovementDataManagers();

		const auto clientWorld = GetWorld(GameWorldId::ClientSimulation);
		auto clientSystemsManager = clientWorld->GetSystemsManager();

		clientSystemsManager->AddSystem(std::make_unique<GameClientSimulationSystem>());

		auto gameClientSimulationNetworkSystem = std::make_unique<GameClientSimulationNetworkSystem>(
			*Network,
			*FetchSystem<GameClientSimulationSystem>(*clientSystemsManager),
			[clientWorld, this](std::string mapName)
			{
				GameSimulationFactory gameSimulationFactory{
					*clientWorld->GetEntitiesManager(),
					*clientWorld->GetResourcesManager(),
					*clientWorld->GetComponentsContainers(),
					m_MapsFactory,
					*m_ClientSimulationActorsMovementData,
					*m_ClientSimulationActorsMovementDataManager,
					*m_ClientSimulationActorsMovementLogic
				};

				return gameSimulationFactory.InstantiateClientSimulation(*clientWorld, mapName, simfix64(0.1f), 2.0f, 1.05f);
			},
			*Network->GetMessagesFactory(),
			*Network->GetMessagesHandlersManager()
		);
		clientSystemsManager->AddSystem(std::move(gameClientSimulationNetworkSystem));
	}



	void Game::InstantiateClientSimulationActorsMovementDataManagers()
	{
		NestAssert(m_ClientSimulationActorsMovementData == nullptr);
		NestAssert(m_ClientSimulationActorsMovementDataManager == nullptr);
		NestAssert(m_ClientSimulationActorsMovementLogic == nullptr);

		m_ClientSimulationActorsMovementData = std::make_unique<SimulationActorsMovementData>();
		m_ClientSimulationActorsMovementDataManager = std::make_unique<SimulationActorsMovementDataManager>(
			*m_ClientSimulationActorsMovementData
		);
		m_ClientSimulationActorsMovementLogic = std::make_unique<SimulationActorsMovementLogic>(*m_ClientSimulationActorsMovementData);
	}



	void Game::LoadViewSystems() const
	{
		const auto viewWorld = GetWorld(GameWorldId::View);
		auto viewSystemsManager = viewWorld->GetSystemsManager();

		viewSystemsManager->AddSystem(std::make_unique<GameViewSimulationSystem>());


		const auto clientSimulationWorld = GetWorld(GameWorldId::ClientSimulation);
		const auto clientSystemsManager = clientSimulationWorld->GetSystemsManager();

		viewSystemsManager->AddSystem(std::make_unique<PlayerControlsSystem>(
			*viewWorld->GetRenderingManager(),
			*FetchSystem<GameClientSimulationSystem>(*clientSystemsManager)
		));
	}



	void Game::LoadSimulationViewComponents() const
	{
		GameComponentsLoader gameComponentsLoader;

		const auto viewComponentsFactory = GetWorld(GameWorldId::View)->GetComponentsFactory();
		const auto viewComponentsContainers = GetWorld(GameWorldId::View)->GetComponentsContainers();
		gameComponentsLoader.LoadSimulationViewsComponents(*viewComponentsFactory, *viewComponentsContainers);
	}



	void Game::LoadSimulationViewSystems(Nest::SystemsManager& systemsManager) const
	{
		const auto viewComponentsContainers = GetWorld(GameWorldId::View)->GetComponentsContainers();

		systemsManager.AddSystem(std::make_unique<ActorSystem>());

		auto birdEyeCameraSystem = std::make_unique<BirdEyeCameraSystem>(*viewComponentsContainers);
		birdEyeCameraSystem->SetExecutionOrder(1000);

		auto birdEyeCameraSystemPtr = birdEyeCameraSystem.get();
		m_CameraControlInputObserver->SetYawRotationCallback(
			[birdEyeCameraSystemPtr](int mouseDeltaX) { birdEyeCameraSystemPtr->SetMouseYawRotationInput(mouseDeltaX); }
		);
		m_CameraControlInputObserver->SetZoomCallback(
			[birdEyeCameraSystemPtr](int mouseScrollDeltaY) { birdEyeCameraSystemPtr->SetMouseZoomInput(mouseScrollDeltaY); }
		);

		systemsManager.AddSystem(std::move(birdEyeCameraSystem));

		systemsManager.AddSystem(std::make_unique<DevelopersConsoleSystem>(*viewComponentsContainers));

		systemsManager.AddSystem(std::make_unique<NetworkTickingSystem>());
	}



	void Game::LoadCommonMeshes(Nest::IResourcesManager& resourcesManager, const Nest::MeshesFactory& meshesFactory)
	{
		auto cubeMesh = meshesFactory.InstantiateBox(Nest::Vector3<float>(0.5f, 0.5f, 0.5f), Nest::Vector3<float>(0.0f, 0.0f, 0.0f));
		resourcesManager.AddMesh("Cube_0.5", cubeMesh);

		auto sphereMesh = meshesFactory.InstantiateSphere(1.0f, 20, 10);
		resourcesManager.AddMesh("Sphere_1_20_10", sphereMesh);

		auto rectangleMesh = meshesFactory.InstantiateRectangle(Nest::Vector3<float>(1.0f, 1.0f, 1.0f), Nest::Vector3<float>(0.0f, 0.0f, 0.0f));
		resourcesManager.AddMesh("Square1x1Centered", rectangleMesh);
	}



	void Game::SetUpRenderingSystem(Nest::IResourcesManager& resourcesManager, Nest::SystemsManager& systemsManager, Nest::GameRenderer gameRenderer)
	{
		if (gameRenderer == Nest::GameRenderer::OpenGL)
		{
			//
			Nest::RenderingSystemOpenGL* rsogl = dynamic_cast<Nest::RenderingSystemOpenGL*>(
				systemsManager.FindSystem([=](Nest::System* system) { return (dynamic_cast<Nest::RenderingSystemOpenGL*>(system) != nullptr); })
			);

			if (rsogl == nullptr)
				ThrowInvalidOperationExceptionWithLine("Attempting to set up OpenGL rendering system, but the system class was not found.");


			rsogl->SetDirectionalLightPassShader(resourcesManager.FetchShaderProgram("drLightPassDirectional"));
			rsogl->SetPointLightPassShader(resourcesManager.FetchShaderProgram("drLightPassPoint"));
			rsogl->SetSphereLightVolume(resourcesManager.FetchMesh("Sphere_1_20_10"));
		}
		else if (gameRenderer == Nest::GameRenderer::Vulkan)
		{
			// TODO : set up rendering system for vulkan
		}
	}



	void Game::SetUpDevelopersConsole(Nest::EntitiesManager& entitiesManager, Nest::IResourcesManager& resourcesManager, DevelopersConsoleSystem& developersConsoleSystem)
	{
		// Create the console and graphic components
		auto consoleEntityPtr = entitiesManager.Instantiate();
		auto consoleEntity = consoleEntityPtr.lock();

		auto consoleGraphic = consoleEntity->AddComponent<Nest::NinePatchGraphic>();
		consoleGraphic->SetFrameSizes(
			Nest::Vector2f(10.0f, 10.0f), 10.0f, Nest::Vector2f(10.0f, 10.0f),
			10.0f, 10.0f,
			Nest::Vector2f(10.0f, 10.0f), 10.0f, Nest::Vector2f(10.0f, 10.0f)
		);
		consoleGraphic->SetMaterials(
			resourcesManager.FetchMaterial("9PTopLeft"),
			resourcesManager.FetchMaterial("9PTop"),
			resourcesManager.FetchMaterial("9PTopRight"),
			resourcesManager.FetchMaterial("9PLeft"),
			resourcesManager.FetchMaterial("9PBody"),
			resourcesManager.FetchMaterial("9PRight"),
			resourcesManager.FetchMaterial("9PBottomLeft"),
			resourcesManager.FetchMaterial("9PBottom"),
			resourcesManager.FetchMaterial("9PBottomRight")
		);
		consoleGraphic->SetBodySize(780.0f, 200.0f);

		auto consoleTransform = consoleEntity->GetComponent<Nest::Transform>();
		auto developersConsole = consoleEntity->AddComponent<DevelopersConsoleBehavior>();


		// Create the input command component
		auto textCommandEntityPtr = entitiesManager.Instantiate();
		auto textCommandEntity = textCommandEntityPtr.lock();

		auto textCommandTransform = textCommandEntity->AddComponent<Nest::Transform>();
		auto textCommandTextMesh = textCommandEntity->AddComponent<Nest::TextMesh>();
		auto textCommandRenderer = textCommandEntity->AddComponent<Nest::MeshRenderer>();

		textCommandRenderer->SetGuiFlag(true);
		textCommandRenderer->SetMaterial(resourcesManager.FetchMaterial("FontMaterial")); // TODO : dont use magic values
		textCommandTextMesh->SetText("");
		textCommandTextMesh->SetFontSize(0.5f); // TODO : dont use magic values?
		textCommandTextMesh->SetFont(resourcesManager.FetchFont("Raleway")); // TODO : dont use magic values


		// Finish setting up the console
		developersConsole->SetShowPosition(Nest::Vector3f(0.0f, 200.0f, 0.0f));
		developersConsole->SetHidePosition(Nest::Vector3f(0.0f, 500.0f, 0.0f));
		developersConsole->SetCommandTextMesh(textCommandTextMesh);
		developersConsole->SetBottomTextLinePosition(Nest::Vector3f(-383.0f, -68.0f, 0.0f));
		developersConsole->SetLinesDistance(20.0f);
		developersConsole->AddTextLines(8);
		developersConsole->ClearText();
		developersConsole->Hide();

		textCommandTransform->SetParent(consoleTransform);
		textCommandTransform->SetLocalPosition(Nest::Vector3f(-383.0f, -93.0f, 5.0f));

		Nest::Log::SetLogMessageFunction([&developersConsoleSystem](const std::string message) { developersConsoleSystem.WriteOutputLine("Log: " + message); });
		Nest::Log::SetLogWarningFunction([&developersConsoleSystem](const std::string message) { developersConsoleSystem.WriteOutputLine("Warning: " + message); });
		Nest::Log::SetLogErrorFunction([&developersConsoleSystem](const std::string message) { developersConsoleSystem.WriteOutputLine("Error: " + message); });


		InitializeMainConsoleCommandsHandlers(developersConsoleSystem);

		developersConsole->SetCommitFunction([&](const std::string commitCommand)
		{
			try
			{
				developersConsoleSystem.ExecuteLine(commitCommand);
			}
			catch (std::exception& ex)
			{
				Nest::Log::Error(ex.what());
			}
		});
	}



	void Game::InitializeMainConsoleCommandsHandlers(DevelopersConsoleSystem& developersConsoleSystem)
	{
		const auto viewWorld = GetWorld(GameWorldId::View);
		NestAssert(viewWorld != nullptr);

		m_GameNetworkConsoleCommandsHandler = std::make_unique<GameNetworkConsoleCommandsHandler>(
			/*startNetworkAction*/
			[&](const unsigned int port, const bool isClient) { StartNetwork(port, isClient); },

			/*isNetworkStartedFunction*/
			[&]() { return m_IsNetworkStarted; },

			/*startGameAction*/
			[&]() { StartGameplay(); },

			/*isGameStartedAction*/
			[&]() { return m_IsGameplayRunning; },

			/*connectAsyncAction*/
			[&](const std::string ip, const unsigned int port) { Network->ConnectAsync(ip, port); }
		);

		using CommandType = std::vector<std::string>;

		// TODO : there should be a better way to register the commands in another class, in a clean way
		developersConsoleSystem.RegisterCommand("net_start", [&](const CommandType command)
		{
			m_GameNetworkConsoleCommandsHandler->NetStartAction(command);
		});

		developersConsoleSystem.RegisterCommand("game_start", [&](const CommandType command)
		{
			m_GameNetworkConsoleCommandsHandler->GameStartAction(command);
		});

		developersConsoleSystem.RegisterCommand("s_start_client", [&](const CommandType command)
		{
			m_GameNetworkConsoleCommandsHandler->StartClientAction(command);
		});

		developersConsoleSystem.RegisterCommand("s_start_server", [&](const CommandType command)
		{
			m_GameNetworkConsoleCommandsHandler->StartServerAction(command);
		});

		developersConsoleSystem.RegisterCommand("connect", [&](const CommandType command)
		{
			m_GameNetworkConsoleCommandsHandler->ConnectAction(command);
		});

		developersConsoleSystem.RegisterCommand("s_connect", [&](const CommandType command)
		{
			m_GameNetworkConsoleCommandsHandler->DefaultLoopbackConnectAction(command);
		});

		developersConsoleSystem.RegisterCommand("map_editor", [&](const std::vector<std::string> command) { MapEditorCommandAction(command); });
	}



	void Game::StartGameplay()
	{
		SetUpServerSimulation();

		m_IsGameplayRunning = true;
		Nest::Log::Message("New game has started.");
	}



	void Game::StartNetwork(unsigned int port, bool client)
	{
		if (!m_IsNetworkStarted)
		{
			Network->Start("0.0.0.0", port, client ? NetworkSide::Client : NetworkSide::Server);
			Nest::Log::Message("Starting the network.");
			m_IsNetworkStarted = true;
		}
	}



	void Game::SetUpGameSimulationNetworkEventsListeners()
	{
		const auto clientWorld = GetWorld(GameWorldId::ClientSimulation);

		const auto systemsManager = clientWorld->GetSystemsManager();
		auto gameClientSimulationNetworkSystem = FetchSystem<GameClientSimulationNetworkSystem>(*systemsManager);


		m_OnNewClientSimulationStartedEventToken = gameClientSimulationNetworkSystem->RegisterToNewClientSimulationStartedEvent(
			[this](const NewClientSimulationStartedEvt& evt)
			{
				auto viewSystemsManager = GetWorld(GameWorldId::View)->GetSystemsManager();

				auto gameViewSimulationSystem = FetchSystem<GameViewSimulationSystem>(*viewSystemsManager);
				InstantiateViewSimulationOnNewClientSimulation(evt, *m_GameViewSimulationFactory, *gameViewSimulationSystem);
			}
		);
	}



	void Game::MapEditorCommandAction(const std::vector<std::string>& command)
	{
		if (command.size() > 1)
		{
			if (command[1] == "new_map")
			{
				Nest::Log::Message(">>> New map command");
			}
			else if (command[1] == "load_map")
			{
				Nest::Log::Message(">>> Load map command");
			}
			else
			{
				Nest::Log::Error("Syntax error at [map_editor] command: invalid command arguments");
			}
		}
		else
		{
			Nest::Log::Error("Syntax error at [map_editor] command: command arguments are missing");
		}
	}



	Nest::World* Game::GetWorld(GameWorldId worldId) const
	{
		const auto world = m_Engine.GetWorld(Nest::WorldId{ static_cast<unsigned int>(worldId) });
		NestAssert(world != nullptr);

		return world;
	}
}

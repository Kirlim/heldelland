//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include<iostream>

#include"Game/GameSettings.h"

#include<Nest/ResourcesManager.h>
#include<Nest/RenderingModule.h>

#include<Nest/Engine.h>

#include<Nest/MeshesFactory.h>
#include<Nest/Mesh_OpenGL.h>
#include<Nest/AssimpModelLoader.h>

#include"Game/Game.h"
#include"Game/Map/MapsFactory.h"

#include<Nest/World.h>



#define GAME_FPS		60

std::unique_ptr<Game::GameSettings> LoadGameSettings();


void PrintException(const std::exception& e, int level = 0);



int main(int argc, char* argv[])
{
	const std::unique_ptr<Game::GameSettings> gameSettings = LoadGameSettings();

	// Main modules and managers needed to initialize the engine
	auto resourcesManager = std::make_unique<Nest::ResourcesManager>();
	auto renderingManager = std::make_unique<Nest::RenderingManager>();
	auto renderingModule = std::make_unique<Nest::RenderingModule>();

	renderingModule->Initialize(gameSettings->GetGameRenderer(), gameSettings->GetScreenWidth(), gameSettings->GetScreenHeight());

	Nest::MeshesFactory meshesFactory{ []() { return std::make_unique<Nest::Mesh_OpenGL>(); } };
	auto modelLoader = std::make_unique<Nest::AssimpModelLoader>(meshesFactory);
	Nest::NestEngine nest(
		GAME_FPS,
		std::move(resourcesManager),
		std::move(renderingManager),
		std::move(renderingModule),
		std::move(modelLoader)
	);


	Game::MapsFactory mapsFactory{ meshesFactory };

	auto game = std::make_unique<Game::Game>(nest, meshesFactory, mapsFactory);
	game->Init(*gameSettings);
	game->LoadGameScene();

	try
	{
		nest.Execute();
	}
	catch (const Nest::AssertException&)
	{
		throw;
	}
	catch (const std::exception& e)
	{
		PrintException(e);
		return 1;
	}

	return 0;
}



std::unique_ptr<Game::GameSettings> LoadGameSettings()
{
	auto gameSettings = std::make_unique<Game::GameSettings>();
	gameSettings->Load("Config.txt");

	return gameSettings;
}



void PrintException(const std::exception& e, int level)
{
	std::cerr << std::string(level * 2, ' ') << "exception: " << e.what() << '\n';

	try
	{
		std::rethrow_if_nested(e);
	}
	catch (const std::exception& ex)
	{
		PrintException(ex, level + 1);
	}
}

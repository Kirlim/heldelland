
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Load/GameComponentsLoader.h"

#include"Game/Load/GameComponentsLoaderImpl.h"



namespace Game
{
	GameComponentsLoader::GameComponentsLoader() :
		m_Impl(std::make_unique<GameComponentsLoaderImpl>())
	{
		// Nothing here
	}



	GameComponentsLoader::~GameComponentsLoader()
	{
		// Nothing here
	}



	void GameComponentsLoader::LoadServerSimulationComponents(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers) const
	{
		m_Impl->LoadServerSimulationComponents(componentsFactory, componentsContainers);
	}



	void GameComponentsLoader::LoadClientSimulationComponents(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers) const
	{
		m_Impl->LoadClientSimulationComponents(componentsFactory, componentsContainers);
	}



	void GameComponentsLoader::LoadSimulationViewsComponents(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers) const
	{
		m_Impl->LoadSimulationViewsComponents(componentsFactory, componentsContainers);
	}
}

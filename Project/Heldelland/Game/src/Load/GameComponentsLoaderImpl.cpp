
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Load/GameComponentsLoaderImpl.h"



#include"Game/Load/ComponentsLoader.h"

#include"Game/SimulationCommons/ActorBehavior.h"
#include"Game/Simulation/SimActorMovementBehavior.h"

#include"Game/SimView/PlayerControlBehavior.h"

#include"Game/BirdEyeCameraBehavior.h"
#include"Game/Map/MapComponent.h"
#include"Game/DevelopersConsoleBehavior.h"



namespace Game
{
	void GameComponentsLoaderImpl::LoadServerSimulationComponents(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers) const
	{
		Load::SetUpComponentFactory<ActorBehavior>(componentsFactory, componentsContainers);
		Load::SetUpComponentFactory<SimActorMovementBehavior>(componentsFactory, componentsContainers);
	}



	void GameComponentsLoaderImpl::LoadClientSimulationComponents(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers) const
	{
		Load::SetUpComponentFactory<ActorBehavior>(componentsFactory, componentsContainers);
		Load::SetUpComponentFactory<SimActorMovementBehavior>(componentsFactory, componentsContainers);
		Load::SetUpComponentFactory<PlayerControlBehavior>(componentsFactory, componentsContainers);
	}



	void GameComponentsLoaderImpl::LoadSimulationViewsComponents(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers) const
	{
		Load::SetUpComponentFactory<BirdEyeCameraBehavior>(componentsFactory, componentsContainers);
		Load::SetUpComponentFactory<MapComponent>(componentsFactory, componentsContainers);
		Load::SetUpComponentFactory<DevelopersConsoleBehavior>(componentsFactory, componentsContainers);
	}
}
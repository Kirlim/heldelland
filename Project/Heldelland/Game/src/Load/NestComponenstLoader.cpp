
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Load/NestComponentsLoader.h"

#include"Game/Load/NestComponentsLoaderImpl.h"



namespace Game
{
	NestComponentsLoader::NestComponentsLoader() :
		m_Impl(std::make_unique<NestComponentsLoaderImpl>())
	{
		// Nothing here
	}



	NestComponentsLoader::~NestComponentsLoader()
	{
		// Nothing here
	}



	void NestComponentsLoader::Load(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers) const
	{
		m_Impl->Load(componentsFactory, componentsContainers);
	}
}
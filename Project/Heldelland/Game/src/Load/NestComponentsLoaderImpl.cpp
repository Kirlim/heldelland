
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Load/NestComponentsLoaderImpl.h"


#include"Game/Load/ComponentsLoader.h"

#include<Nest/Transform.h>
#include<Nest/MeshFilter.h>
#include<Nest/TextMesh.h>
#include<Nest/MeshRenderer.h>
#include<Nest/PointLightComponent.h>
#include<Nest/DirectionalLightComponent.h>
#include<Nest/Camera.h>
#include<Nest/NinePatchGraphic.h>



namespace Game
{
	void NestComponentsLoaderImpl::Load(Nest::IComponentsFactory& componentsFactory, Nest::IComponentsContainers& componentsContainers) const
	{
		Load::SetUpComponentFactory<Nest::Transform>(componentsFactory, componentsContainers);
		Load::SetUpComponentFactory<Nest::MeshFilter>(componentsFactory, componentsContainers);
		Load::SetUpComponentFactory<Nest::TextMesh>(componentsFactory, componentsContainers);
		Load::SetUpComponentFactory<Nest::MeshRenderer>(componentsFactory, componentsContainers);
		Load::SetUpComponentFactory<Nest::PointLightComponent>(componentsFactory, componentsContainers);
		Load::SetUpComponentFactory<Nest::DirectionalLightComponent>(componentsFactory, componentsContainers);
		Load::SetUpComponentFactory<Nest::Camera>(componentsFactory, componentsContainers);
		Load::SetUpComponentFactory<Nest::NinePatchGraphic>(componentsFactory, componentsContainers);
	}
}
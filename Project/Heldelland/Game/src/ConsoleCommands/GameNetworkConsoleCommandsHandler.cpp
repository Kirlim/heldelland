//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/ConsoleCommands/GameNetworkConsoleCommandsHandler.h"

#include<Nest/Helpers/StringHelper.h>

#include<Nest/NestAssert.h>
#include<Nest/Exceptions/InvalidArgumentException.h>
#include<Nest/Exceptions/LogicErrorException.h>


namespace Game
{
	GameNetworkConsoleCommandsHandler::GameNetworkConsoleCommandsHandler(
		std::function<void(unsigned int, bool)> startNetworkAction,
		std::function<bool()> isNetworkStartedFunction,
		std::function<void()> startGameAction,
		std::function<bool()> isGameStartedFunction,
		std::function<void(std::string, unsigned int)> connectAsyncAction
	) :
		m_StartNetworkAction(startNetworkAction),
		m_IsNetworkStartedFunction(isNetworkStartedFunction),
		m_StartGameAction(startGameAction),
		m_IsGameStartedFunction(isGameStartedFunction),
		m_ConnectAsyncAction(connectAsyncAction)
	{
		NestAssert(startNetworkAction != nullptr);
		NestAssert(isNetworkStartedFunction != nullptr);
		NestAssert(startGameAction != nullptr);
		NestAssert(isGameStartedFunction != nullptr);
		NestAssert(connectAsyncAction != nullptr);
	}



	GameNetworkConsoleCommandsHandler::~GameNetworkConsoleCommandsHandler() = default;



	void GameNetworkConsoleCommandsHandler::NetStartAction(const std::vector<std::string>& command) const
	{
		if (command.size() != 3)
			ThrowInvalidArgumentExceptionWithLine("[net_start] command requires 2 arguments. {net_start [port_number] [client/server]}");

		if (m_IsNetworkStartedFunction())
			ThrowLogicErrorExceptionWithLine("[net_start] network is already started");


		bool isClient;
		const auto port = strtol(command[1].c_str(), nullptr, 10);
		const auto& type = command[2];

		if (port == 0)
			ThrowInvalidArgumentExceptionWithLine("[net_start] invalid port number. {net_start [port_number] [clien/server]}");


		if (type == "client")
			isClient = true;
		else if (type == "server")
			isClient = false;
		else
			ThrowInvalidArgumentExceptionWithLine("[net_start] invalid simulation type. {net_start [port_number] [client/server]}");


		m_StartNetworkAction(port, isClient);
	}



	void GameNetworkConsoleCommandsHandler::GameStartAction(const std::vector<std::string>& command) const
	{
		if (command.size() != 1)
			ThrowInvalidArgumentExceptionWithLine("[game_start] command does not receive any argument");

		if (!m_IsGameStartedFunction())
		{
			m_StartGameAction();
		}
		else
		{
			ThrowLogicErrorExceptionWithLine("[game_start]: a game has already been started.");
		}
	}



	void GameNetworkConsoleCommandsHandler::StartClientAction(const std::vector<std::string>& command) const
	{
		if (command.size() != 1)
			ThrowInvalidArgumentExceptionWithLine("[s_start_client] command does not receive any argument");

		std::vector<std::string> startNetworkCommand{};
		startNetworkCommand.emplace_back("net_start");
		startNetworkCommand.emplace_back("60001");
		startNetworkCommand.emplace_back("client");

		NetStartAction(startNetworkCommand);
	}



	void GameNetworkConsoleCommandsHandler::StartServerAction(const std::vector<std::string>& command) const
	{
		if (command.size() != 1)
			ThrowInvalidArgumentExceptionWithLine("[s_start_server] command does not receive any argument}");

		const std::vector<std::string> startNetworkCommand{ "net_start", "60000", "server" };
		NetStartAction(startNetworkCommand);

		const std::vector<std::string> gameStartCommand{ "game_start" };
		GameStartAction(gameStartCommand);
	}



	void GameNetworkConsoleCommandsHandler::ConnectAction(const std::vector<std::string>& command) const
	{
		if (command.size() != 2)
			ThrowInvalidArgumentExceptionWithLine("[connect] command requires two argumenst {net_start [ipv4:port]}");

		// TODO : validate ip mask [ipv4:port]
		auto ipSegment = Nest::Helpers::SplitString(command[1], ':');
		const auto ip = ipSegment[0];
		const auto port = atoi(ipSegment[1].c_str()); // TODO : helper that ensure that a string can be converted to a number

		m_ConnectAsyncAction(ip, port);
	}



	void GameNetworkConsoleCommandsHandler::DefaultLoopbackConnectAction(const std::vector<std::string>& command) const
	{
		if (command.size() != 1)
			ThrowInvalidArgumentExceptionWithLine("[s_connect] command does not receive any argument");

		const std::vector<std::string> connectCommand{ "connect", "127.0.0.1:60000" };

		ConnectAction(connectCommand);
	}
}

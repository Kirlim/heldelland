//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/BirdEyeCameraBehavior.h"

#include<algorithm>

#include<Nest/Math.h>



namespace Game
{
	BirdEyeCameraBehavior::~BirdEyeCameraBehavior() = default;



	void BirdEyeCameraBehavior::SetFollowTarget(Nest::ComponentReference<Nest::Transform> targetEntity)
	{
		m_FollowTarget = targetEntity;
	}



	Nest::ComponentReference<Nest::Transform> BirdEyeCameraBehavior::GetFollowTarget() const
	{
		return (m_FollowTarget);
	}



	void BirdEyeCameraBehavior::SetPitchRadians(float pitchRadians)
	{
		m_PitchRadians = std::clamp(pitchRadians, NEST_PI * -0.5f, NEST_PI * 0.5f);
	}



	float BirdEyeCameraBehavior::GetPitchRadians() const
	{
		return m_PitchRadians;
	}



	void BirdEyeCameraBehavior::SetYawRadians(float yawRadians)
	{
		while (yawRadians < -NEST_PI * 2.0f)
			yawRadians += NEST_PI * 2.0f;

		while (yawRadians > NEST_PI * 2.0f)
			yawRadians -= NEST_PI * 2.0f;

		m_YawRadians = yawRadians;
	}



	float BirdEyeCameraBehavior::GetYawRadians() const
	{
		return m_YawRadians;
	}



	void BirdEyeCameraBehavior::SetDistanceToTarget(float distanceToTarget)
	{
		m_DistanceToTarget = std::clamp(distanceToTarget, m_MinDistanceToTarget, m_MaxDistanceToTarget);
	}



	float BirdEyeCameraBehavior::GetDistanceToTarget() const
	{
		return m_DistanceToTarget;
	}
}

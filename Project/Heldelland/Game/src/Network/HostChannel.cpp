//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/HostChannel.h"



namespace Game
{
	HostChannel::HostChannel(IHost& host) :
		IHostChannel(),
		m_Host(host)
	{
		// Nothing here
	}



	HostChannel::~HostChannel()
	{
		// Nothing here
	}



	IHost* HostChannel::GetHost() const
	{
		return &m_Host;
	}



	bool HostChannel::AddMessage(std::shared_ptr<NetworkMessage> message)
	{
		if (message == nullptr) return (false);

		std::unique_lock<std::mutex> queueLock{ m_QueueMutex, std::defer_lock };
		while (!queueLock.try_lock()) continue; // Busy waiting

		m_MessagesQueue.push(message);
		return true;
	}



	NetworkMessage* HostChannel::PeekNextMessage() const
	{
		std::unique_lock<std::mutex> queueLock{ m_QueueMutex, std::defer_lock };
		while (!queueLock.try_lock()) continue; // Busy waiting

		NetworkMessage* result = nullptr;

		if (m_MessagesQueue.size() > 0)
		{
			result = m_MessagesQueue.front().get();
		}

		return result;
	}



	std::shared_ptr<NetworkMessage> HostChannel::FetchNextMessage()
	{
		std::unique_lock<std::mutex> queueLock{ m_QueueMutex, std::defer_lock };
		while (!queueLock.try_lock()) continue; // Busy waiting


		if (m_MessagesQueue.size() > 0)
		{
			auto result = m_MessagesQueue.front();
			m_MessagesQueue.pop();

			return result;
		}
		else
		{
			return std::shared_ptr<NetworkMessage>{ nullptr };
		}
	}



	unsigned int HostChannel::Size() const
	{
		std::unique_lock<std::mutex> queueLock{ m_QueueMutex, std::defer_lock };
		while (!queueLock.try_lock()) continue; // Busy waiting

		return static_cast<unsigned int>(m_MessagesQueue.size());
	}




	bool HostChannel::IsValid() const
	{
		// TODO : verify if this method can be improved, or if need to be removed
		return true;
	}
}

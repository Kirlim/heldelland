//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/NetworkMessagesFactory.h"

#include<string>
#include<stdexcept>



namespace Game
{
	NetworkMessagesFactory::~NetworkMessagesFactory()
	{
		// Nothing here
	}



	bool NetworkMessagesFactory::Register(NetworkMessageTypeId id, std::function<std::shared_ptr<NetworkMessage>()> factoryFunction)
	{
		bool success = false;

		if (factoryFunction != nullptr && m_Factories.count(id) == 0)
		{
			m_Factories[id] = factoryFunction;
			success = true;
		}

		return success;
	}



	std::shared_ptr<NetworkMessage> NetworkMessagesFactory::InstantiateMessage(NetworkMessageTypeId id) const
	{
		if (m_Factories.count(id) > 0)
		{
			return (m_Factories.at(id)());
		}
		else
		{
			// TODO : better type of exception?
			auto message = "Trying to instantiate unknown type of message [" + std::to_string(id.value) + "].";
			throw std::runtime_error(message.c_str());
		}
	}



	std::vector<NetworkMessageTypeId> NetworkMessagesFactory::GetAllRegisteredNetworkMessageTypeIds() const
	{
		std::vector<NetworkMessageTypeId> networkMessageTypeIds{};

		for(const auto& keyValue : m_Factories)
		{
			networkMessageTypeIds.emplace_back(keyValue.first);
		}

		return networkMessageTypeIds;
	}
}

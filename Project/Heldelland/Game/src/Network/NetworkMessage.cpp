
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/NetworkMessage.h"

#include"Game/Network/IHost.h"
#include"Game/Network/INetworkSerializer.h"
#include"Game/Network/NetworkCountSerializer.h"



namespace Game
{
	NetworkMessage::NetworkMessage(NetworkMessageReliabilityType type) :
		m_Id(MakeNewId()), m_ReliabilityType(type)
	{
		// Nothing here
	}



	NetworkMessage::~NetworkMessage()
	{
		// Nothing here
	}



	NetworkMessageReliabilityType NetworkMessage::GetReliabilityType() const
	{
		return m_ReliabilityType;
	}



	NetworkMessageId NetworkMessage::GetId() const
	{
		return m_Id;
	}



	bool NetworkMessage::Serialize(INetworkSerializer& serializer, unsigned int& outBitsCount)
	{
		const auto start = serializer.GetBitPointer();
		const bool success = DoSerialize(serializer);
		const auto end = serializer.GetBitPointer();

		outBitsCount = end - start;
		return success;
	}



	bool NetworkMessage::Serialize(INetworkSerializer& serializer)
	{
		return(DoSerialize(serializer));
	}



	bool NetworkMessage::GetBitsSize(unsigned int& outBitsSize)
	{
		NetworkCountSerializer serializer;
		const bool success = Serialize(serializer);

		outBitsSize = serializer.GetBitPointer();
		return success;
	}



	IHost* NetworkMessage::GetRemoteHost() const
	{
		// TODO : consider asserting that remote host is not null?
		return m_RemoteHost;
	}



	void NetworkMessage::SetRemoteHost(IHost* remoteHost)
	{
		m_RemoteHost = remoteHost;
	}



	NetworkMessageId NetworkMessage::MakeNewId()
	{
		static NetworkMessageId nextId{ 1 };
		return NetworkMessageId{ nextId.value++ };
	}
}
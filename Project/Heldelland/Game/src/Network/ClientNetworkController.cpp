//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/ClientNetworkController.h"

#include<stdexcept>

#include<Nest/Log.h>
#include<Nest/NestAssert.h>

#include"Game/Network/IHostChannel.h"
#include"Game/Network/IHost.h"
#include"Game/Network/INetworkMessagesFactory.h"
#include"Game/Network/INetworkMessagesHandler.h"
#include"Game/Network/INetworkMessagesHandlersManager.h"
#include"Game/Network/Messages/JoinGameRequestMessage.h"
#include"Game/Network/IHostsManager.h"



namespace Game
{
	ClientNetworkController::ClientNetworkController(
		const INetworkMessagesFactory& networkMessagesFactory,
		const INetworkMessagesHandlersManager& networkMessagesHandlersManager
	) :
		IClientNetworkController(),
		m_NetworkMessagesFactory(networkMessagesFactory),
		m_NetworkMessagesHandlersManager(networkMessagesHandlersManager),
		m_ConnectionStatus(GameConnectionStatus::NotConnected)
	{
		// Nothing here
	}



	void ClientNetworkController::Tick(IHostChannel& receiveFromServerChannel, IHostChannel& sendToServerChannel)
	{
		switch (m_ConnectionStatus)
		{
			case GameConnectionStatus::NotConnected:
			{
				Nest::Log::Message("Sending join game request.");
				const auto message = m_NetworkMessagesFactory.InstantiateMessage(JoinGameRequestMessage::TypeId);
				sendToServerChannel.AddMessage(message);

				m_ConnectionStatus = GameConnectionStatus::Connecting;
				break;
			}

			case GameConnectionStatus::Connecting:
			{
				// Just wait
				break;
			}

			case GameConnectionStatus::Connected:
			{
				// Not implemented yet
				break;
			}
		}

		const auto serverHost = receiveFromServerChannel.GetHost();
		if (serverHost == nullptr)
		{
			// TODO : decide on the better way to handle this error
			throw std::runtime_error("Somehow, server is null!!!!");
		}

		auto receivedMessage = receiveFromServerChannel.FetchNextMessage();
		while (receivedMessage != nullptr)
		{
			if (serverHost->GetLocadId() == IHostsManager::LocalHostId)
			{
				// TODO : move this logic to a better place. Refactor, make the code more predictable and maintanable
				receivedMessage->SetRemoteHost(serverHost);
			}

			// TODO : break after amount of time (risk of starvation)

			auto messageHandler = m_NetworkMessagesHandlersManager.GetHandler(*receivedMessage);
			NestAssert(messageHandler != nullptr);

			messageHandler->Execute(*serverHost, receivedMessage);

			receivedMessage = receiveFromServerChannel.FetchNextMessage();
		}
	}
}

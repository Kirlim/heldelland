
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/NetworkMessagesHandlersManager.h"

#include<string>

#include"Game/Network/INetworkMessagesHandler.h"
#include"Game/Network/NetworkMessage.h"



namespace Game
{
	NetworkMessagesHandlersManager::~NetworkMessagesHandlersManager()
	{
		// Nothing here
	}



	void NetworkMessagesHandlersManager::AddHandler(std::unique_ptr<INetworkMessagesHandler> handler)
	{
		if (handler == nullptr)
			throw std::logic_error("Cannot register messages handler: handler is null.");
		if (m_Handlers.count(handler->GetMessageTypeId()) > 0)
			throw std::logic_error("Cannot register messages handler: a handler of id [" + std::to_string(handler->GetMessageTypeId().value) + "] is already registered.");

		m_Handlers[handler->GetMessageTypeId()] = std::move(handler);
	}



	void NetworkMessagesHandlersManager::RemoveHandler(NetworkMessageTypeId messageTypeId)
	{
		if (m_Handlers.count(messageTypeId) > 0)
		{
			m_Handlers.erase(messageTypeId);
		}
	}



	INetworkMessagesHandler* NetworkMessagesHandlersManager::GetHandler(NetworkMessageTypeId messageTypeId) const
	{
		INetworkMessagesHandler* handler = nullptr;

		if (m_Handlers.count(messageTypeId) > 0)
		{
			handler = m_Handlers.at(messageTypeId).get();
		}

		return handler;
	}



	INetworkMessagesHandler* NetworkMessagesHandlersManager::GetHandler(NetworkMessage& networkMessage) const
	{
		return GetHandler(networkMessage.GetTypeId());
	}



	std::vector<NetworkMessageTypeId> NetworkMessagesHandlersManager::GetAllRegisteredNetworkMessageTypeIds() const
	{
		std::vector<NetworkMessageTypeId> networkMessageTypeIds{};

		for(const auto& keyValue : m_Handlers)
		{
			networkMessageTypeIds.emplace_back(keyValue.first);
		}

		return networkMessageTypeIds;
	}
}
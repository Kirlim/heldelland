//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/NetworkSystemFactoryImpl.h"

#include<Nest/Log.h>
#include<Nest/Cast.h>

#include"Game/Network/INetworkSystem.h"
#include"Game/Network/NetworkSystem.h"
#include"Game/Network/SunaNetworkAdapter.h"
#include"Game/Network/NetworkMessagesFactory.h"
#include"Game/Network/NetworkMessagesHandlersManager.h"
#include"Game/Network/HostsManager.h"
#include"Game/Network/NetworkMessagesSender.h"
#include"Game/Network/SunaNetworkWriteSerializer.h"
#include"Game/Network/NetworkMessagesReceiver.h"
#include"Game/Network/SunaNetworkReadSerializer.h"

#include"Game/Network/Messages/JoinGameRequestMessage.h"
#include"Game/Network/Messages/JoinGameReplyMessage.h"
#include"Game/Network/Messages/GameSimulationSettingsMessage.h"
#include"Game/Network/Messages/GameSimulationLoadedMessage.h"

#include"Game/Simulation/ClientCommands/ForceNextTickValueCommand.h"
#include"Game/Simulation/ClientCommands/SimulationTickStartCommand.h"
#include"Game/Simulation/ClientCommands/SimulationTickEndCommand.h"

#include"Game/Simulation/ClientCommands/SetActorFullPathCommand.h"
#include"Game/Simulation/ClientCommands/SetActorMovementSpeedCommand.h"
#include"Game/Simulation/ClientCommands/SetActorPositionCommand.h"
#include"Game/Simulation/ClientCommands/SpawnActorCommand.h"
#include"Game/Simulation/ClientCommands/SpawnPlayerCharacterCommand.h"

#include"Game/Simulation/ServerCommands/PlayerMoveCommand.h"

#include"Game/Network/Messages/GenericMessagesHandler.h"



namespace Game
{
	std::unique_ptr<INetworkSystem> NetworkSystemFactoryImpl::Instantiate(const unsigned int sendReceiveSerializersBuffersSizeInBytes) const
	{
		auto networkAdapter = std::make_unique<SunaNetworkAdapter>();
		auto networkMessagesFactory = std::make_unique<NetworkMessagesFactory>();
		auto networkMessagesHandlersManager = std::make_unique<NetworkMessagesHandlersManager>();
		auto hostsManager = std::make_unique<HostsManager>();
		auto networkMessagesSender = std::make_unique<NetworkMessagesSender>(
			sendReceiveSerializersBuffersSizeInBytes,
			networkAdapter.get(),
			[](unsigned char* buffer, const unsigned int bufferSizeInBytes) { return (std::make_unique<SunaNetworkWriteSerializer>(buffer, 0, bufferSizeInBytes)); }
		);
		auto networkMessagesReceiver = std::make_unique<NetworkMessagesReceiver>(
			*networkMessagesFactory,
			[](unsigned char* buffer, const unsigned int bufferSizeInBytes) { return (std::make_unique<SunaNetworkReadSerializer>(buffer, 0, bufferSizeInBytes)); }
		);

		RegisterNetworkMessagesToFactory(*networkMessagesFactory);
		RegisterNetworkMessagesHandlersToFactory(*networkMessagesHandlersManager);

		auto networkSystem = std::make_unique<NetworkSystem>(
			std::move(networkAdapter),
			std::move(networkMessagesFactory),
			std::move(networkMessagesHandlersManager),
			std::move(hostsManager),
			std::move(networkMessagesSender),
			std::move(networkMessagesReceiver)
		);
		return (std::move(networkSystem));
	}



	void NetworkSystemFactoryImpl::RegisterNetworkMessagesToFactory(INetworkMessagesFactory& factory) const
	{
		// Client commands
		if (!factory.Register(JoinGameRequestMessage::TypeId, []() { return (std::make_shared<JoinGameRequestMessage>()); }))
			Nest::Log::Error("Failed to register network message [JoinGameRequestMessage] in the messages factory.");

		if (!factory.Register(JoinGameReplyMessage::TypeId, []() { return (std::make_shared<JoinGameReplyMessage>()); }))
			Nest::Log::Error("Failed to register network message [JoinGameReplyMessage] in the messages factory.");

		if (!factory.Register(GameSimulationSettingsMessage::TypeId, []() { return (std::make_shared<GameSimulationSettingsMessage>()); }))
			Nest::Log::Error("Failed to register network message [GameSimulationSettingsMessage] in the messages factory.");

		if (!factory.Register(GameSimulationLoadedMessage::TypeId, []() { return (std::make_shared<GameSimulationLoadedMessage>()); }))
			Nest::Log::Error("Failed to register network message [GameSimulationLoadedMessage] in the messages factory.");

		if (!factory.Register(ForceNextTickValueCommand::TypeId, []() { return (std::make_shared<ForceNextTickValueCommand>()); }))
			Nest::Log::Error("Failed to register command message [ForceNextTickValueCommand] in the messages factory.");

		if (!factory.Register(SimulationTickStartCommand::TypeId, []() { return (std::make_shared<SimulationTickStartCommand>()); }))
			Nest::Log::Error("Failed to register command message [SimulationTickStartCommand] in the messages factory.");

		if (!factory.Register(SimulationTickEndCommand::TypeId, []() { return (std::make_shared<SimulationTickEndCommand>()); }))
			Nest::Log::Error("Failed to register command message [SimulationTickEndCommand] in the messages factory.");



		if (!factory.Register(SetActorPositionCommand::TypeId, []() { return (std::make_shared<SetActorPositionCommand>()); }))
			Nest::Log::Error("Failed to register command message [SetActorPositionCommand] in the messages factory.");

		if (!factory.Register(SetActorFullPathCommand::TypeId, []() { return (std::make_shared<SetActorFullPathCommand>()); }))
			Nest::Log::Error("Failed to register command message [SetActorFullPathCommand] in the messages factory.");

		if (!factory.Register(SetActorMovementSpeedCommand::TypeId, []() { return (std::make_shared<SetActorMovementSpeedCommand>()); }))
			Nest::Log::Error("Failed to register command message [SetActorMovementSpeedCommand] in the messages factory.");

		if (!factory.Register(SpawnActorCommand::TypeId, []() { return (std::make_shared<SpawnActorCommand>()); }))
			Nest::Log::Error("Failed to register command message [SpawnActorCommand] in the messages factory.");

		if (!factory.Register(SpawnPlayerCharacterCommand::TypeId, []() { return (std::make_shared<SpawnPlayerCharacterCommand>()); }))
			Nest::Log::Error("Failed to register command message [SpawnPlayerCharacterCommand] in the messages factory.");


		// Server commands
		if (!factory.Register(PlayerMoveCommand::TypeId, []() { return (std::make_shared<PlayerMoveCommand>()); }))
			Nest::Log::Error("Failed to register command message [PlayerMoveCommand] in the messages factory.");
	}



	void NetworkSystemFactoryImpl::RegisterNetworkMessagesHandlersToFactory(INetworkMessagesHandlersManager& manager) const
	{
		// Client commands
		manager.AddHandler(std::make_unique<GenericMessagesHandler<NetworkMessage>>(JoinGameRequestMessage::TypeId));
		manager.AddHandler(std::make_unique<GenericMessagesHandler<NetworkMessage>>(JoinGameReplyMessage::TypeId));
		manager.AddHandler(std::make_unique<GenericMessagesHandler<NetworkMessage>>(GameSimulationSettingsMessage::TypeId));
		manager.AddHandler(std::make_unique<GenericMessagesHandler<NetworkMessage>>(GameSimulationLoadedMessage::TypeId));
		manager.AddHandler(std::make_unique<GenericMessagesHandler<NetworkMessage>>(ForceNextTickValueCommand::TypeId));
		manager.AddHandler(std::make_unique<GenericMessagesHandler<NetworkMessage>>(SimulationTickStartCommand::TypeId));
		manager.AddHandler(std::make_unique<GenericMessagesHandler<NetworkMessage>>(SimulationTickEndCommand::TypeId));

		manager.AddHandler(std::make_unique<GenericMessagesHandler<NetworkMessage>>(SetActorFullPathCommand::TypeId));
		manager.AddHandler(std::make_unique<GenericMessagesHandler<NetworkMessage>>(SetActorMovementSpeedCommand::TypeId));
		manager.AddHandler(std::make_unique<GenericMessagesHandler<NetworkMessage>>(SetActorPositionCommand::TypeId));
		manager.AddHandler(std::make_unique<GenericMessagesHandler<NetworkMessage>>(SpawnActorCommand::TypeId));
		manager.AddHandler(std::make_unique<GenericMessagesHandler<NetworkMessage>>(SpawnPlayerCharacterCommand::TypeId));

		// Server commands
		manager.AddHandler(std::make_unique<GenericMessagesHandler<NetworkMessage>>(PlayerMoveCommand::TypeId));
	}
}

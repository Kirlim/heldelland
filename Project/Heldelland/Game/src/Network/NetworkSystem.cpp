//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/NetworkSystem.h"

#include<algorithm>

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/NestAssert.h>
#include<Nest/Log.h>

#include"Game/Network/INetworkAdapter.h"
#include"Game/Network/INetworkMessagesSender.h"
#include"Game/Network/IHostsManager.h"
#include"Game/Network/INetworkMessagesReceiver.h"
#include"Game/Network/INetworkMessagesFactory.h"
#include"Game/Network/INetworkMessagesHandlersManager.h"
#include"Game/Network/IHostChannel.h"
#include"Game/Network/IHost.h"
#include"Game/Network/Endpoint.h"



namespace Game
{
	NetworkSystem::NetworkSystem(
		std::unique_ptr<INetworkAdapter> networkAdapter,
		std::unique_ptr<INetworkMessagesFactory> networkMessagesFactory, std::unique_ptr<INetworkMessagesHandlersManager> networkMessagesHandlersManager,
		std::unique_ptr<IHostsManager> hostsManager,
		std::unique_ptr<INetworkMessagesSender> networkMessagesSender, std::unique_ptr<INetworkMessagesReceiver> networkMessagesReceiver
	) :
		m_NetworkAdapter(std::move(networkAdapter)),
		m_NetworkMessagesFactory(std::move(networkMessagesFactory)), m_NetworkMessagesHandlersManager(std::move(networkMessagesHandlersManager)),
		m_HostsManager(std::move(hostsManager)),
		m_NetworkMessagesSender(std::move(networkMessagesSender)), m_NetworkMessagesReceiver(std::move(networkMessagesReceiver))
	{
		m_NetworkAdapter->SetIncomingConnectionListener(
			[&](const std::string ip, const unsigned int port) { return (OnIncomingConnection(ip, port)); }
		);
		m_NetworkAdapter->SetOnConnectedListener(
			[&](const std::string ip, const unsigned int port) { OnConnected(ip, port); }
		);
		m_NetworkAdapter->SetOnNetworkMessageReceived(
			[&](const std::string ip, const unsigned int port, const unsigned char* dataBuffer, const unsigned int dataStartBit, const unsigned int bufferSizeInBytes)
			{
				OnNetworkMessageReceived(ip, port, dataBuffer, dataStartBit, bufferSizeInBytes);
			}
		);

		if (!ValidateThatMessagesFactoryAndHandlerKnowsTheSameTypes())
			ThrowLogicErrorExceptionWithLine("Registered Message Type Ids on Factory and Handlers are different");
	}



	NetworkSystem::~NetworkSystem()
	{
		if (m_NetworkAdapter != nullptr && m_NetworkAdapter->IsRunning())
		{
			// TODO : need flag to know if it is iniatialized
			m_NetworkAdapter->Stop();
		}
	}



	void NetworkSystem::Tick()
	{
		if (m_NetworkAdapter != nullptr && m_NetworkAdapter->IsRunning())
		{
			for (auto& host : *m_HostsManager)
			{
				if (host.GetLocadId() == IHostsManager::LocalHostId)
					continue;

				const auto sendChannel = host.GetToHostChannel();
				m_NetworkMessagesSender->Send(*sendChannel);
			}


			m_NetworkAdapter->Tick();
		}
	}



	NetworkSide NetworkSystem::GetNetworkSide() const
	{
		return m_NetworkSide;
	}



	bool NetworkSystem::Start(std::string listenIp, unsigned int listenPort, NetworkSide networkSide)
	{
		bool success = false;

		if (m_NetworkAdapter != nullptr && !m_NetworkAdapter->IsRunning())
		{
			success = m_NetworkAdapter->Start(listenIp, listenPort);
			if (success)
			{
				m_NetworkSide = networkSide;
			}
		}

		return success;
	}



	void NetworkSystem::Stop()
	{
		if (m_NetworkAdapter != nullptr && m_NetworkAdapter->IsRunning())
		{
			m_NetworkAdapter->Stop();
		}
	}



	void NetworkSystem::ConnectAsync(std::string destinationIp, unsigned int destinationPort)
	{
		if (m_NetworkAdapter != nullptr && m_NetworkAdapter->IsRunning())
		{
			m_NetworkAdapter->ConnectTo(destinationIp, destinationPort);
		}
	}



	void NetworkSystem::SetLocalInstanceServerFlag(bool hasLocalInstanceServer)
	{
		m_HasLocalInstanceServer = true;
	}



	bool NetworkSystem::IsConnectedToServer() const
	{
		return m_IsConnectedToRemoteServer || m_HasLocalInstanceServer;
	}



	void NetworkSystem::ExecuteWithServerHostsChannels(std::function<void(IHostChannel&, IHostChannel&)> function)
	{
		if (function == nullptr)
			return;

		const auto host = m_HostsManager->GetHost(m_ServerId).lock();
		NestAssert(host != nullptr);

		IHostChannel* sendChannel;
		IHostChannel* receiveChannel;

		// TODO : who sets the m_ServerId to LocalHostId?
		if (m_ServerId == IHostsManager::LocalHostId)
		{
			// Server running in same executable instance. Client has "inverted" channels.
			sendChannel = host->GetFromHostChannel();
			receiveChannel = host->GetToHostChannel();
		}
		else
		{
			sendChannel = host->GetToHostChannel();
			receiveChannel = host->GetFromHostChannel();
		}

		function(*receiveChannel, *sendChannel);
	}



	void NetworkSystem::ExecuteWithClientHostsChannel(std::function<void(IHost&, IHostChannel&, IHostChannel&)> function)
	{
		if (function == nullptr) return;

		for (auto& host : *m_HostsManager)
		{
			const auto sendChannel = host.GetToHostChannel();
			const auto receiveChannel = host.GetFromHostChannel();

			function(host, *receiveChannel, *sendChannel);
		}
	}



	IHostsManager* NetworkSystem::GetHostsManager() const
	{
		return m_HostsManager.get();
	}



	INetworkMessagesFactory* NetworkSystem::GetMessagesFactory() const
	{
		return m_NetworkMessagesFactory.get();
	}



	INetworkMessagesHandlersManager* NetworkSystem::GetMessagesHandlersManager() const
	{
		return m_NetworkMessagesHandlersManager.get();
	}



	bool NetworkSystem::ValidateThatMessagesFactoryAndHandlerKnowsTheSameTypes() const
	{
		auto registeredOnHandlers = m_NetworkMessagesHandlersManager->GetAllRegisteredNetworkMessageTypeIds();
		auto registeredOnFactories = m_NetworkMessagesFactory->GetAllRegisteredNetworkMessageTypeIds();

		std::sort(std::begin(registeredOnHandlers), std::end(registeredOnHandlers));
		std::sort(std::begin(registeredOnFactories), std::end(registeredOnFactories));

		std::vector<NetworkMessageTypeId> diff{ registeredOnHandlers.size() + registeredOnFactories.size() };
		const auto it = std::set_difference(
			std::begin(registeredOnHandlers), std::end(registeredOnHandlers),
			std::begin(registeredOnFactories), std::end(registeredOnFactories),
			diff.begin()
		);

		diff.resize(it - diff.begin());

		return diff.empty();
	}



	bool NetworkSystem::OnIncomingConnection(std::string ip, unsigned int port)
	{
		Nest::Log::Message("[Network System] Incoming connection from " + ip + ":" + std::to_string(port) + ".");

		Endpoint endpoint;
		endpoint.SetAddress(ip, port);

		return !m_HostsManager->HostExists(endpoint);
	}



	void NetworkSystem::OnConnected(std::string ip, unsigned int port)
	{
		Endpoint endpoint;
		endpoint.SetAddress(ip, port);

		if (!m_HostsManager->HostExists(endpoint))
		{
			auto hostWptr = m_HostsManager->InstantiateNewHost(endpoint);
			const auto host = hostWptr.lock();

			if (m_NetworkSide == NetworkSide::Client)
			{
				Nest::Log::Message("[NetworkSystem] Connected as client to " + ip + ":" + std::to_string(port));
				m_IsConnectedToRemoteServer = true;
				m_ServerId = host->GetLocadId();
			}
			else
			{
				Nest::Log::Message("[NetworkSystem] Connected as server to " + ip + ":" + std::to_string(port));
			}
		}
	}



	void NetworkSystem::OnNetworkMessageReceived(
		std::string ip, unsigned int port,
		const unsigned char* dataBuffer, unsigned int dataStartBit, unsigned int bufferSizeInBytes
	)
	{
		Endpoint endpoint;
		endpoint.SetAddress(ip, port);

		if (m_HostsManager->HostExists(endpoint))
		{
			auto hostWPtr = m_HostsManager->GetHost(endpoint);
			const auto host = hostWPtr.lock();

			const auto hostReceiveChannel = host->GetFromHostChannel();

			m_NetworkMessagesReceiver->Receive(*hostReceiveChannel, dataBuffer, dataStartBit, bufferSizeInBytes);
		}
		else
		{
			// TODO : decide how to handle this situation
			throw - 1;
		}
	}



	void NetworkSystem::TickMessagesReceive()
	{
		//
	}



	void NetworkSystem::TickMessagesSend()
	{
		//
	}
}

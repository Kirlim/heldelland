
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/NetworkSystemFactory.h"

#include"Game/Network/INetworkSystem.h"



namespace Game
{
    NetworkSystemFactory::NetworkSystemFactory() :
        m_FactoryImplementation(std::make_unique<NetworkSystemFactoryImpl>())
    {
        // Nothing here
    }



    std::unique_ptr<INetworkSystem> NetworkSystemFactory::Instantiate(const unsigned int sendReceiveSerializersBuffersSizeInBytes) const
    {
        return(m_FactoryImplementation->Instantiate(sendReceiveSerializersBuffersSizeInBytes));
    }
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/NetworkMessagesReceiver.h"

#include<Nest/Log.h>
#include"Game/Network/INetworkReadSerializer.h"
#include"Game/Network/INetworkMessagesFactory.h"
#include"Game/Network/IHostChannel.h"
#include"Game/Network/NetworkMessage.h"



namespace Game
{
	NetworkMessagesReceiver::NetworkMessagesReceiver(
		INetworkMessagesFactory& networkMessagesFactory,
		std::function<std::unique_ptr<INetworkReadSerializer>(unsigned char*, const unsigned int)> readerSerializerFactory
	) :
		INetworkMessagesReceiver(),
		m_NetworkMessagesFactory(networkMessagesFactory),
		m_ReaderSerializerFactoryFunction(readerSerializerFactory)
	{
		// Nothing here
	}



	NetworkMessagesReceiver::~NetworkMessagesReceiver()
	{
		// Nothing
	}



	void NetworkMessagesReceiver::Receive(IHostChannel& channel, const unsigned char* dataBuffer, const unsigned int dataStartBit, const unsigned int bufferSizeInBytes) const
	{
		if (dataBuffer == nullptr) return;

		const unsigned int bufferSizeInBits = bufferSizeInBytes * 8;
		if (dataStartBit > bufferSizeInBits) return;

		const auto dataBufferMod = const_cast<unsigned char*>(dataBuffer); // Only casting in order to pass as parameter. Not supposed to change
		auto readSerializer = m_ReaderSerializerFactoryFunction(dataBufferMod, bufferSizeInBytes);
		readSerializer->SetBitPointer(dataStartBit);
		int remainingBufferSize = bufferSizeInBits - readSerializer->GetBitPointer();

		while (remainingBufferSize >= NetworkMessage::TypeIdBitsSize)
		{
			uint32_t messageTypeId = 0;
			const auto readBitPosition = readSerializer->GetBitPointer();
			readSerializer->SerializeUInt32(messageTypeId, NetworkMessage::TypeIdBitsSize);
			readSerializer->SetBitPointer(readBitPosition);

			auto message = m_NetworkMessagesFactory.InstantiateMessage(NetworkMessageTypeId{ static_cast<NetworkMessageTypeId::underlying_type>(messageTypeId) });
			message->SetRemoteHost(channel.GetHost());
			message->Serialize(*readSerializer);

			channel.AddMessage(message);

			remainingBufferSize = bufferSizeInBits - readSerializer->GetBitPointer();
		}
	}
}

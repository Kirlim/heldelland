//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/Host.h"

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NullArgumentException.h>
#include<Nest/Exceptions/NullPointerException.h>

#include"Game/Network/IHostChannel.h"
#include"Game/Network/HostGameInformation.h"



namespace Game
{
	Host::Host(HostId localId, std::function<std::unique_ptr<IHostChannel>(IHost&)> channelFactoryFunction) :
		m_LocalId(localId)
	{
		if (channelFactoryFunction == nullptr)
			ThrowNullArgumentExceptionWithLine("Channels Factory Function cannot be null");

		m_FromHostChannel = channelFactoryFunction(*this);
		m_ToHostChannel = channelFactoryFunction(*this);

		if (m_FromHostChannel == nullptr || m_ToHostChannel == nullptr)
			ThrowNullPointerExceptionWithLine("Channel Factory Function has returned at least one null channel");
	}



	Host::~Host() = default;



	HostId Host::GetLocadId() const
	{
		return m_LocalId;
	}



	Endpoint Host::GetEndpoint() const
	{
		return m_Endpoint;
	}



	void Host::SetEndpoint(Endpoint endpoint)
	{
		m_Endpoint = endpoint;
	}



	void Host::SetPlayerAlias(std::string alias)
	{
		m_Alias = alias;
	}



	std::string Host::GetPlayerAlias() const
	{
		return m_Alias;
	}



	void Host::SetGameInformation(std::unique_ptr<HostGameInformation>& hostGameInformation)
	{
		if (hostGameInformation == nullptr)
			ThrowNullArgumentExceptionWithLine("Host Game Information cannot be null.");

		if (m_GameInformation != nullptr)
			ThrowLogicErrorExceptionWithLine("There is already a Host Game Information set");

		m_GameInformation = std::move(hostGameInformation);
	}



	HostGameInformation* Host::GetGameInformation() const
	{
		return m_GameInformation.get();
	}



	IHostChannel* Host::GetFromHostChannel() const
	{
		return m_FromHostChannel.get();
	}



	IHostChannel* Host::GetToHostChannel() const
	{
		return m_ToHostChannel.get();
	}
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/ServerNetworkController.h"

#include<memory>

#include"Game/Network/IHost.h"
#include"Game/Network/IHostChannel.h"
#include"Game/Network/IHostsManager.h"
#include"Game/Network/INetworkMessagesHandler.h"
#include"Game/Network/INetworkMessagesHandlersManager.h"

#include"Game/Network/NetworkMessage.h"



namespace Game
{
	ServerNetworkController::ServerNetworkController(const INetworkMessagesHandlersManager& networkMessagesHandlersManager)
		: IServerNetworkController(),
		m_NetworkMessagesHandlersManager(networkMessagesHandlersManager)
	{
		// Nothing here
	}



	void ServerNetworkController::Tick(IHost& host, IHostChannel& receiveFromClientChannel, IHostChannel& sendToClientChannel)
	{
		auto receivedMessage = receiveFromClientChannel.FetchNextMessage();
		while (receivedMessage != nullptr)
		{
			if (host.GetLocadId() == IHostsManager::LocalHostId)
			{
				// TODO : move this logic to a better place. Refactor, make the code more predictable and maintanable
				// TODO : this set is here because the message didn't come from the network, so it was not set. Is there a better way to make this set?
				receivedMessage->SetRemoteHost(&host);
			}

			// TODO : break after amount of time (risk of starvation)

			auto messageHandler = m_NetworkMessagesHandlersManager.GetHandler(*receivedMessage);
			messageHandler->Execute(host, receivedMessage);

			receivedMessage = receiveFromClientChannel.FetchNextMessage();
		}
	}
}
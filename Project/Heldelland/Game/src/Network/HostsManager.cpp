//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/HostsManager.h"

#include<algorithm>

#include<Nest/Cast.h>

#include"Game/Network/Endpoint.h"
#include"Game/Network/IHost.h"

// TODO : remove after we get an injector or factory to instantiate the host and return as an IHost
#include"Game/Network/HostChannel.h"
#include"Game/Network/Host.h"



namespace Game
{
	HostsManagerIteratorImpl::HostsManagerIteratorImpl(std::vector<std::shared_ptr<IHost>>::const_iterator iterator) :
		m_Iterator(iterator)
	{
		// TODO : assert that iterator is valid
	}



	HostsManagerIteratorImpl::~HostsManagerIteratorImpl() = default;



	bool HostsManagerIteratorImpl::Equals(const IHostsManagerIteratorImpl& other) const
	{
		const auto otherImpl = DoCast<const HostsManagerIteratorImpl>(&other);
		return (m_Iterator == otherImpl->m_Iterator);
	}



	void HostsManagerIteratorImpl::Increment()
	{
		++m_Iterator;
	}



	IHost& HostsManagerIteratorImpl::Dereference() const
	{
		// TODO : validate on debug that iterator is valid
		return *m_Iterator->get();
	}



	HostsManager::HostsManager()
	{
		Endpoint localEndpoint{};
		localEndpoint.SetAddress(".", 0);

		const auto localHost = CreateNewHostInstance(LocalHostId, localEndpoint);
		m_Hosts.push_back(localHost);
	}



	HostsManager::~HostsManager() = default;



	bool HostsManager::HostExists(HostId localId) const
	{
		auto host = GetHost(localId);
		return !host.expired();
	}



	bool HostsManager::HostExists(Endpoint endpoint) const
	{
		auto host = GetHost(endpoint);
		return !host.expired();
	}



	std::weak_ptr<IHost> HostsManager::GetHost(HostId localId) const
	{
		const auto host = std::find_if(
			m_Hosts.begin(),
			m_Hosts.end(),
			[&](const std::shared_ptr<IHost>& hostElement) { return (hostElement->GetLocadId() == localId); }
		);

		std::weak_ptr<IHost> result;
		if (host != m_Hosts.end())
		{
			result = *host;
		}

		return result;
	}



	std::weak_ptr<IHost> HostsManager::GetHost(Endpoint endpoint) const
	{
		const auto host = std::find_if(
			m_Hosts.begin(),
			m_Hosts.end(),
			[&](const std::shared_ptr<IHost>& hostElement) { return (hostElement->GetEndpoint() == endpoint); }
		);

		std::weak_ptr<IHost> result;
		if (host != m_Hosts.end())
		{
			result = *host;
		}

		return result;
	}



	std::weak_ptr<IHost> HostsManager::InstantiateNewHost(Endpoint endpoint)
	{
		// TODO : Get new IHost implementation from a factory or injector container
		const auto host = CreateNewHostInstance(GetNextLocalId(), endpoint);

		m_Hosts.push_back(host);
		return std::weak_ptr<IHost>(host);
	}



	IHostsManagerIterator HostsManager::begin() const
	{
		return IHostsManagerIterator{ std::make_unique<HostsManagerIteratorImpl>(m_Hosts.begin()) };
	}



	IHostsManagerIterator HostsManager::end() const
	{
		return IHostsManagerIterator{ std::make_unique<HostsManagerIteratorImpl>(m_Hosts.end()) };
	}



	HostId HostsManager::GetNextLocalId()
	{
		static unsigned int nextId = 1;
		return nextId++;
	}



	std::shared_ptr<IHost> HostsManager::CreateNewHostInstance(HostId hostId, Endpoint endpoint) const
	{
		auto host = std::make_shared<Host>(hostId, [](IHost& host) { return std::make_unique<HostChannel>(host); });
		host->SetEndpoint(endpoint);

		return host;
	}
}

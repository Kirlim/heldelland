
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/SunaNetworkSerializer.h"

#include<Suna/DataBuffer.h>



namespace Game
{
    SunaNetworkSerializer::SunaNetworkSerializer(unsigned char* buffer, const unsigned int bitStartPosition, const unsigned int sizeInBytes) :
        m_Buffer(buffer),
        m_BitStartPosition(bitStartPosition), m_SizeInBits(sizeInBytes * 8),
        m_Stream(buffer, sizeInBytes)
    {
        // Nothing here
    }



    SunaNetworkSerializer::~SunaNetworkSerializer()
    {
        // Nothing here
    }



    bool SunaNetworkSerializer::SetBitPointerPosition(const unsigned int position)
    {
        // TODO : handle possible errors
        m_Stream.SetBitPositionPointer(m_BitStartPosition + position);
        return(true);
    }
}
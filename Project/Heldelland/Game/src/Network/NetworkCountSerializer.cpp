
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/NetworkCountSerializer.h"



namespace Game
{
    NetworkCountSerializer::NetworkCountSerializer() : INetworkSerializer(),
        m_BitPointerPosition(0)
    {
        // Nothing here
    }



    NetworkCountSerializer::~NetworkCountSerializer()
    {
        // Nothing here
    }



    bool NetworkCountSerializer::SetBitPointer(unsigned int position)
    {
        m_BitPointerPosition = position;
        return(true);
    }



    unsigned int NetworkCountSerializer::GetBitPointer() const
    {
        return(m_BitPointerPosition);
    }



    bool NetworkCountSerializer::SerializeBool(bool& value)
    {
        ++m_BitPointerPosition;
        return(true);
    }



    bool NetworkCountSerializer::SerializeChar(char& value, int bits)
    {
        if (bits <= 8)
        {
            m_BitPointerPosition += bits;
            return(true);
        }
        else
        {
            return(false);
        }
    }



    bool NetworkCountSerializer::SerializeUChar(unsigned char& value, int bits)
    {
        if (bits <= 8)
        {
            m_BitPointerPosition += bits;
            return(true);
        }
        else
        {
            return(false);
        }
    }



    bool NetworkCountSerializer::SerializeInt16(int16_t& value, int bits)
    {
        if (bits <= 16)
        {
            m_BitPointerPosition += bits;
            return(true);
        }
        else
        {
            return(false);
        }
    }



    bool NetworkCountSerializer::SerializeUInt16(uint16_t& value, int bits)
    {
        if (bits <= 16)
        {
            m_BitPointerPosition += bits;
            return(true);
        }
        else
        {
            return(false);
        }
    }



    bool NetworkCountSerializer::SerializeInt32(int32_t& value, int bits)
    {
        if (bits <= 32)
        {
            m_BitPointerPosition += bits;
            return(true);
        }
        else
        {
            return(false);
        }
    }



    bool NetworkCountSerializer::SerializeUInt32(uint32_t& value, int bits)
    {
        if (bits <= 32)
        {
            m_BitPointerPosition += bits;
            return(true);
        }
        else
        {
            return(false);
        }
    }



    bool NetworkCountSerializer::SerializeInt64(int64_t& value, int bits)
    {
        if (bits <= 64)
        {
            m_BitPointerPosition += bits;
            return(true);
        }
        else
        {
            return(false);
        }
    }



    bool NetworkCountSerializer::SerializeUInt64(uint64_t& value, int bits)
    {
        if (bits <= 64)
        {
            m_BitPointerPosition += bits;
            return(true);
        }
        else
        {
            return(false);
        }
    }
}
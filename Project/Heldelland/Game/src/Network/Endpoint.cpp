
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/Endpoint.h"



namespace Game
{
    void Endpoint::SetIp(const std::string ip)
    {
        m_Ip = ip;
    }



    std::string Endpoint::GetIp() const
    {
        return(m_Ip);
    }



    void Endpoint::SetPort(const unsigned int port)
    {
        m_Port = port;
    }



    unsigned int Endpoint::GetPort() const
    {
        return(m_Port);
    }



    void Endpoint::SetAddress(std::string ip, unsigned int port)
    {
        m_Ip = ip;
        m_Port = port;
    }
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/SunaNetworkWriteSerializer.h"



namespace Game
{
    SunaNetworkWriteSerializer::SunaNetworkWriteSerializer(unsigned char* buffer, const unsigned int bitStartPosition, const unsigned int sizeInBytes) :
        SunaNetworkSerializer(buffer, bitStartPosition, sizeInBytes)
    {
        // Nothing here
    }



    SunaNetworkWriteSerializer::~SunaNetworkWriteSerializer()
    {
        // Nothing here
    }



    bool SunaNetworkWriteSerializer::SetBitPointer(unsigned int position)
    {
        return(SetBitPointerPosition(position));
    }



    unsigned int SunaNetworkWriteSerializer::GetBitPointer() const
    {
        return(m_Stream.GetBitPositionPointer());
    }



    bool SunaNetworkWriteSerializer::SerializeBool(bool & value)
    {
        return(m_Stream.WriteBool(value));
    }
    
    
    
    bool SunaNetworkWriteSerializer::SerializeChar(char & value, int bits)
    {
        return(m_Stream.WriteChar(value, bits));
    }
    
    
    
    bool SunaNetworkWriteSerializer::SerializeUChar(unsigned char & value, int bits)
    {
        return(m_Stream.WriteUChar(value, bits));
    }
    
    
    
    bool SunaNetworkWriteSerializer::SerializeInt16(int16_t & value, int bits)
    {
        return(m_Stream.WriteInt16(value, bits));
    }
    
    
    
    bool SunaNetworkWriteSerializer::SerializeUInt16(uint16_t & value, int bits)
    {
        return(m_Stream.WriteUInt16(value, bits));
    }
    
    
    
    bool SunaNetworkWriteSerializer::SerializeInt32(int32_t & value, int bits)
    {
        return(m_Stream.WriteInt32(value, bits));
    }
    
    
    
    bool SunaNetworkWriteSerializer::SerializeUInt32(uint32_t & value, int bits)
    {
        return(m_Stream.WriteUInt32(value, bits));
    }



    bool SunaNetworkWriteSerializer::SerializeInt64(int64_t & value, int bits)
    {
        return(m_Stream.WriteInt64(value, bits));
    }



    bool SunaNetworkWriteSerializer::SerializeUInt64(uint64_t & value, int bits)
    {
        return(m_Stream.WriteUInt64(value, bits));
    }
}
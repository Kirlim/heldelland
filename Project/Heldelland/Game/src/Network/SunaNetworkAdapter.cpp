
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/SunaNetworkAdapter.h"

#include<stdexcept>

#include<Nest/Log.h>

#include<Suna/LocalHost.h>
#include<Suna/RemoteHost.h>
#include<Suna/CommunicationChannels.h>
#include<Suna/UnreliableChannel.h>
#include<Suna/ReliableSequencedChannel.h>
#include<Suna/AsioSocket.h>
#include<Suna/AsioEndpoint.h>
#include<Suna/INetworkPacket.h>
#include<Suna/MainHeader.h>
#include<Suna/ChannelHeader.h>
#include<Suna/DataBuffer.h>
#include<Suna/SystemTimeWrapper.h>

#include<Suna/SunaSettings.h>
#include<Suna/Logger.h>



namespace Game
{
	
	SunaNetworkAdapter::SunaNetworkAdapter() :
		INetworkAdapter()
	{
		auto asioEndpoint = std::make_unique<Suna::AsioEndpoint>();
		std::unique_ptr<Suna::ISocket> asioSocket = std::make_unique<Suna::AsioSocket>(std::move(asioEndpoint), 5000); // TODO : not use magic values for buffer size
		m_LocalHost = std::make_unique<Suna::LocalHost>(asioSocket, 5000); // TODO : not use magic values for buffer size.


		Suna::SunaSettings::ApplicationCode = 0xBEBACAFE;

		m_LocalHost->SetConnectedToHostListener([&](int hostId) {OnConnectedEvent(hostId);});
		m_LocalHost->SetRemoteHostConnectionRequestRefusedListener([&](int hostId) {OnRemoteHostConnectionRefusedEvent(hostId);});
		m_LocalHost->SetRemoteHostConnectionRequestTimeoutListener([&](int hostId) {OnRemoteHostConnectionTimeoutEvent(hostId);});
		m_LocalHost->SetNetworkMessageReceivedListener([&](const Suna::RemoteHost* remoteHost, const unsigned int port) { OnNetworkMessageReceivedEvent(remoteHost, port);});

		Suna::Logger::SetErrorLogger(Nest::Log::Error);
		Suna::Logger::SetWarningLogger(Nest::Log::Warning);
		Suna::Logger::SetMessageLogger(Nest::Log::Message);
		Suna::Logger::SetTraceLogger(Nest::Log::Message);
	}



	SunaNetworkAdapter::~SunaNetworkAdapter()
	{
		Stop();
	}



	void SunaNetworkAdapter::Tick()
	{
		if (m_IsRunning)
		{
			m_LocalHost->TickChannels();
		}
	}



	bool SunaNetworkAdapter::Start(const std::string listenIp, const unsigned int listenPort)
	{
		bool success = false;

		if (!m_LocalHost->IsInitialized())
		{
			// TODO : how to handle Init failure?
			m_LocalHost->Init(listenIp, listenPort);
			m_LocalHost->StartSocketThread();

			m_IsRunning = true;

			success = true;
		}

		return(success);
	}



	void SunaNetworkAdapter::Stop()
	{
		if (m_IsRunning)
		{
			m_LocalHost->StopSocketThread();
			m_IsRunning = false;
		}
	}



	bool SunaNetworkAdapter::IsRunning() const
	{
		return(m_IsRunning);
	}



	void SunaNetworkAdapter::ConnectTo(const std::string ip, const unsigned int port)
	{
		m_LocalHost->ConnectAsync(ip, port);
	}



	void SunaNetworkAdapter::SetIncomingConnectionListener(const std::function<bool(std::string, unsigned int)> listener)
	{
		m_LocalHost->SetIncomingConnectionListener(listener);
	}



	void SunaNetworkAdapter::SetOnConnectedListener(std::function<void(const std::string, const unsigned int)> event)
	{
		m_OnConnectedEvent = event;
	}



	void SunaNetworkAdapter::SetOnConnectionRequestRefusedListener(std::function<void(const std::string, const unsigned int)> event)
	{
		m_OnConnectionRequestRefusedEvent = event;
	}



	void SunaNetworkAdapter::SetOnConnectionRequestTimedOutListener(std::function<void(const std::string, const unsigned int)> event)
	{
		m_OnConnectionRequestTimedOutEvent = event;
	}



	void SunaNetworkAdapter::SetOnNetworkMessageReceived(std::function<void(const std::string, const unsigned int, const unsigned char*, const unsigned int, const unsigned int)> event)
	{
		m_OnNetworkMessageReceivedEvent = event;
	}



	void SunaNetworkAdapter::SendDataAsUnreliable(const std::string ip, const unsigned int port,const unsigned char* data, const unsigned int sizeInBits)
	{
		Suna::AsioEndpoint endpoint;
		endpoint.SetAddress(ip, port);

		auto remoteHostWptr = m_LocalHost->GetRemoteHost(&endpoint);
		auto remoteHost = remoteHostWptr.lock();

		if (remoteHost != nullptr)
		{
			auto channels = remoteHost->GetCommunicationChannels();
			channels->SendTo(UnreliableChannelId, data, sizeInBits);
		}
		else
		{
			throw std::runtime_error("Remote host was null while trying to send data over channel.");
		}
	}



	void SunaNetworkAdapter::SendDataAsReliable(const std::string ip, const unsigned int port,const unsigned char* data, const unsigned int sizeInBits)
	{
		Suna::AsioEndpoint endpoint;
		endpoint.SetAddress(ip, port);

		auto remoteHostWprt = m_LocalHost->GetRemoteHost(&endpoint);
		auto remoteHost = remoteHostWprt.lock();

		if (remoteHost != nullptr)
		{
			auto channels = remoteHost->GetCommunicationChannels();
			channels->SendTo(ReliableChannelId, data, sizeInBits);
		}
		else
		{
			throw std::runtime_error("Remote host was null while trying to send data over reliable channel.");
		}
	}



	void SunaNetworkAdapter::RegisterChannels(Suna::RemoteHost& remoteHost)
	{
		auto channels = remoteHost.GetCommunicationChannels();
		channels->AddChannel(std::make_unique<Suna::UnreliableChannel>(UnreliableChannelId));
		channels->AddChannel(std::make_unique<Suna::ReliableSequencedChannel>(
			std::make_unique<Suna::SystemTimeWrapper>(),
			ReliableChannelId, 
			ReliableSeqBits, 
			ReliableRedundantAcksBits)
		);
	}
	


	bool SunaNetworkAdapter::OnIncomingConnectionRequestEvent(const std::string ip, const unsigned int port)
	{
		// TODO : method
		throw - 1;
		return(false);
	}



	void SunaNetworkAdapter::OnConnectedEvent(const int hostId)
	{
		Nest::Log::Message("Connected to host id [" + std::to_string(hostId) + "].");
		auto remoteHost = m_LocalHost->GetRemoteHost(hostId).lock();

		if (remoteHost == nullptr)
		{
			// TODO : better exception. Also, need to handle it somewhere
			throw std::runtime_error("On Connected event, the remote host was null.");
		}

		if (m_OnConnectedEvent != nullptr)
		{
			RegisterChannels(*remoteHost);
			m_OnConnectedEvent(remoteHost->GetEndpointAddress(), remoteHost->GetEndpointPort());
		}
	}



	void SunaNetworkAdapter::OnRemoteHostConnectionRefusedEvent(const int hostId)
	{
		// TODO : method
		throw - 1;
	}



	void SunaNetworkAdapter::OnRemoteHostConnectionTimeoutEvent(const int hostId)
	{
		// TODO : method
		throw - 1;
	}



	void SunaNetworkAdapter::OnNetworkMessageReceivedEvent(const Suna::RemoteHost* remoteHost, const unsigned int channelId)
	{
		if (channelId == 0)
		{
			Nest::Log::Message("---Ignoring message, since it is on channel 0.");
			return;
		}

		auto packet = remoteHost->GetCommunicationChannels()->FetchNext(channelId);
		if (packet == nullptr)
		{
			// TODO : decide how to handle this situation
			throw - 1;
		}

		auto dataBuffer = packet->GetDataBuffer();
		if (dataBuffer == nullptr)
		{
			// TODO : decide how to handle this situation
			throw - 1;
		}

		auto buffer = dataBuffer->GetBuffer();

		auto ip = remoteHost->GetEndpointAddress();
		auto port = remoteHost->GetEndpointPort();

		unsigned int payloadStartBit = packet->GetDataStartBitPosition();
		auto sizeInBytes = packet->GetSizeInBytes();
		m_OnNetworkMessageReceivedEvent(ip, port, buffer, payloadStartBit, sizeInBytes);
	}
}

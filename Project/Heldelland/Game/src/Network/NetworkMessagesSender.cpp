//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/NetworkMessagesSender.h"

#include<Nest/NestAssert.h>
#include<Nest/Log.h>

#include"Game/Network/NetworkMessage.h"
#include"Game/Network/Endpoint.h"
#include"Game/Network/IHost.h"
#include"Game/Network/IHostChannel.h"
#include"Game/Network/INetworkAdapter.h"
#include"Game/Network/INetworkWriteSerializer.h"



namespace Game
{
	NetworkMessagesSender::NetworkMessagesSender(
		unsigned const int dataBufferSize,
		INetworkAdapter* const networkAdapter,
		std::function<std::unique_ptr<INetworkWriteSerializer>(unsigned char*, const unsigned int)> writeSerializerFactory
	) :
		INetworkMessagesSender(),
		m_NetworkAdapter(networkAdapter),
		m_DataBufferSize(dataBufferSize),
		m_ReliableBuffer(std::make_unique<unsigned char[]>(dataBufferSize)),
		m_UnreliableBuffer(std::make_unique<unsigned char[]>(dataBufferSize)),
		m_ReliableWriteSerializer(writeSerializerFactory(m_ReliableBuffer.get(), dataBufferSize)),
		m_UnreliableWriteSerializer(writeSerializerFactory(m_UnreliableBuffer.get(), dataBufferSize))
	{
		// Nothing here
	}



	NetworkMessagesSender::~NetworkMessagesSender()
	{
		// Nothing here
	}



	void NetworkMessagesSender::Send(IHostChannel& channel)
	{
		const auto host = channel.GetHost();
		NestAssert(host != nullptr);

		m_ReliableWriteSerializer->SetBitPointer(0);
		m_UnreliableWriteSerializer->SetBitPointer(0);

		while (channel.Size() > 0)
		{
			const auto message = channel.PeekNextMessage();

			if (DoesFit(message))
			{
				WriteToBuffer(message);
				channel.FetchNextMessage(); // Remove the message from the queue.
			}
			else
			{
				break;
			}
		}

		SendDataToNetworkAdapter(host);
	}



	bool NetworkMessagesSender::DoesFit(NetworkMessage* const message) const
	{
		unsigned int messageBitsSize;
		message->GetBitsSize(messageBitsSize);

		switch (message->GetReliabilityType())
		{
		case NetworkMessageReliabilityType::Reliable:
			return (m_ReliableWriteSerializer->GetBitPointer() + messageBitsSize <= m_DataBufferSize);

		case NetworkMessageReliabilityType::Unreliable:
			return (m_UnreliableWriteSerializer->GetBitPointer() + messageBitsSize <= m_DataBufferSize);

		default:
			Nest::Log::Error("Unknown network message type at messages sender.");
			return (false);
		}
	}



	void NetworkMessagesSender::WriteToBuffer(NetworkMessage* const message) const
	{
		switch (message->GetReliabilityType())
		{
		case NetworkMessageReliabilityType::Reliable:
			if (!message->Serialize(*m_ReliableWriteSerializer))
			{
				Nest::Log::Error("Failed to write network message to the reliable serializer.");
			}
			break;

		case NetworkMessageReliabilityType::Unreliable:
			if (!message->Serialize(*m_UnreliableWriteSerializer))
			{
				Nest::Log::Error("Failed to write network message to the unreliable serializer.");
			}
			break;

		default:
			Nest::Log::Error("Unknown network message type at messages sender.");
			break;
		}
	}



	void NetworkMessagesSender::SendDataToNetworkAdapter(const IHost* host) const
	{
		const auto ip = host->GetEndpoint().GetIp();
		const auto port = host->GetEndpoint().GetPort();

		if (m_ReliableWriteSerializer->GetBitPointer() > 0)
		{
			m_NetworkAdapter->SendDataAsReliable(ip, port, m_ReliableBuffer.get(), m_ReliableWriteSerializer->GetBitPointer());
		}

		if (m_UnreliableWriteSerializer->GetBitPointer() > 0)
		{
			m_NetworkAdapter->SendDataAsUnreliable(ip, port, m_UnreliableBuffer.get(), m_UnreliableWriteSerializer->GetBitPointer());
		}
	}
}

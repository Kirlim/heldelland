
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/Messages/GameSimulationLoadedMessage.h"

#include"Game/Network/INetworkSerializer.h"

#include"Game/Network/Messages/MessagesTypes.h"



namespace Game
{
	const NetworkMessageTypeId GameSimulationLoadedMessage::TypeId{ GAME_SIMULATION_LOADED_NETWORK_MESSAGE_ID };



	GameSimulationLoadedMessage::GameSimulationLoadedMessage()
		: NetworkMessage(NetworkMessageReliabilityType::Reliable)
	{
		// Nothing here
	}



	GameSimulationLoadedMessage::~GameSimulationLoadedMessage() = default;



	NetworkMessageTypeId GameSimulationLoadedMessage::GetTypeId() const
	{
		return TypeId;
	}



	bool GameSimulationLoadedMessage::DoSerialize(INetworkSerializer& serializer)
	{
		auto typeId = uint32_t(TypeId.value);

		const bool success = serializer.SerializeUInt32(typeId, TypeIdBitsSize);
		// TODO : verify that type id matches?

		return success;
	}
}
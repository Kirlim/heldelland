
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/Messages/JoinGameRequestMessage.h"

#include"Game/Network/INetworkSerializer.h"

#include"Game/Network/Messages/MessagesTypes.h"



namespace Game
{
	const NetworkMessageTypeId JoinGameRequestMessage::TypeId{ JOIN_GAME_REQUEST_NETWORK_MESSAGE_ID };



	JoinGameRequestMessage::JoinGameRequestMessage() :
		NetworkMessage(NetworkMessageReliabilityType::Reliable)
	{
		// Nothing here
	}



	JoinGameRequestMessage::~JoinGameRequestMessage()
	{
		// Nothing here
	}



	NetworkMessageTypeId JoinGameRequestMessage::GetTypeId() const
	{
		return TypeId;
	}



	bool JoinGameRequestMessage::DoSerialize(INetworkSerializer& serializer)
	{
		auto typeId = uint32_t(TypeId.value);
		return(serializer.SerializeUInt32(typeId, TypeIdBitsSize));
	}
}
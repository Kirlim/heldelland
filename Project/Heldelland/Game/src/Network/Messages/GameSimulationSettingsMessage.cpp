
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/Messages/GameSimulationSettingsMessage.h"

#include"Game/Network/INetworkSerializer.h"

#include"Game/Network/Messages/MessagesTypes.h"



namespace Game
{
	const NetworkMessageTypeId GameSimulationSettingsMessage::TypeId{ GAME_SIMULATION_SETTINGS_NETWORK_MESSAGE_ID };



	GameSimulationSettingsMessage::GameSimulationSettingsMessage() :
		NetworkMessage(NetworkMessageReliabilityType::Reliable)
	{
		// Nothing here
	}



	GameSimulationSettingsMessage::~GameSimulationSettingsMessage() = default;



	NetworkMessageTypeId GameSimulationSettingsMessage::GetTypeId() const
	{
		return(GameSimulationSettingsMessage::TypeId);
	}



	void GameSimulationSettingsMessage::SetMapName(std::string mapName)
	{
		m_MapName = mapName;
	}



	std::string GameSimulationSettingsMessage::GetMapName() const
	{
		return(m_MapName);
	}



	bool GameSimulationSettingsMessage::DoSerialize(INetworkSerializer& serializer)
	{
		auto typeId = uint32_t(TypeId.value);

		bool success = serializer.SerializeUInt32(typeId, TypeIdBitsSize);

		// TODO : make and use a string serialization from Suna?
		unsigned char mapNameSize = m_MapName.size();
		if (success) success = serializer.SerializeUChar(mapNameSize, 8);

		if (m_MapName.size() != mapNameSize)
		{
			m_MapName.clear();
			m_MapName.append(mapNameSize, ' ');
		}

		// TODO : never trust array size data from network. How to do this?
		for (unsigned char i = 0; i < mapNameSize; ++i)
		{
			if (success)
			{
				success = serializer.SerializeChar(m_MapName[i], 8);
			}
		}

		return(success);
	}
}
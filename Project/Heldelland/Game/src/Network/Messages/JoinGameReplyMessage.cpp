
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/Messages/JoinGameReplyMessage.h"

#include"Game/Network/INetworkSerializer.h"

#include"Game/Network/Messages/MessagesTypes.h"



namespace Game
{
	const NetworkMessageTypeId JoinGameReplyMessage::TypeId{ JOIN_GAME_REPLY_NETWORK_MESSAGE_ID };



	JoinGameReplyMessage::JoinGameReplyMessage() :
		NetworkMessage(NetworkMessageReliabilityType::Reliable)
	{
		// Nothing here
	}



	JoinGameReplyMessage::~JoinGameReplyMessage()
	{
		// Nothing here
	}



	NetworkMessageTypeId JoinGameReplyMessage::GetTypeId() const
	{
		return(TypeId);
	}



	void JoinGameReplyMessage::SetReply(Reply reply)
	{
		m_Reply = reply;
	}



	JoinGameReplyMessage::Reply JoinGameReplyMessage::GetReply() const
	{
		return m_Reply;
	}



	bool JoinGameReplyMessage::DoSerialize(INetworkSerializer& serializer)
	{
		auto typeId = uint32_t(TypeId.value);
		unsigned char reply = static_cast<unsigned char>(m_Reply);

		bool success = serializer.SerializeUInt32(typeId, TypeIdBitsSize);
		if (success) success = serializer.SerializeUChar(reply);
		if (success)
		{
			m_Reply = static_cast<Reply>(reply);
		}

		return(success);
	}
}
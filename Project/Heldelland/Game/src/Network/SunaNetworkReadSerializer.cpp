
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Network/SunaNetworkReadSerializer.h"



namespace Game
{
    SunaNetworkReadSerializer::SunaNetworkReadSerializer(unsigned char* buffer, const unsigned int bitStartPosition, const unsigned int sizeInBytes) :
        SunaNetworkSerializer(buffer, bitStartPosition, sizeInBytes)
    {
        // Nothing here
    }

    SunaNetworkReadSerializer::~SunaNetworkReadSerializer()
    {
        // Nothing here
    }



    bool SunaNetworkReadSerializer::SetBitPointer(unsigned int position)
    {
        return(SetBitPointerPosition(position));
    }



    unsigned int SunaNetworkReadSerializer::GetBitPointer() const
    {
        return(m_Stream.GetBitPositionPointer());
    }



    bool SunaNetworkReadSerializer::SerializeBool(bool & value)
    {
        return(m_Stream.ReadBool(value));
    }
    
    
    
    bool SunaNetworkReadSerializer::SerializeChar(char & value, int bits)
    {
        return(m_Stream.ReadChar(value, bits));
    }
    
    
    
    bool SunaNetworkReadSerializer::SerializeUChar(unsigned char & value, int bits)
    {
        return(m_Stream.ReadUChar(value, bits));
    }
    
    
    
    bool SunaNetworkReadSerializer::SerializeInt16(int16_t & value, int bits)
    {
        return(m_Stream.ReadInt16(value, bits));
    }
    
    
    
    bool SunaNetworkReadSerializer::SerializeUInt16(uint16_t & value, int bits)
    {
        return(m_Stream.ReadUInt16(value, bits));
    }
    
    
    
    bool SunaNetworkReadSerializer::SerializeInt32(int32_t & value, int bits)
    {
        return(m_Stream.ReadInt32(value, bits));
    }
    
    
    
    bool SunaNetworkReadSerializer::SerializeUInt32(uint32_t & value, int bits)
    {
        return(m_Stream.ReadUInt32(value, bits));
    }



    bool SunaNetworkReadSerializer::SerializeInt64(int64_t & value, int bits)
    {
        return(m_Stream.ReadInt64(value, bits));
    }



    bool SunaNetworkReadSerializer::SerializeUInt64(uint64_t & value, int bits)
    {
        return(m_Stream.ReadUInt64(value, bits));
    }
}

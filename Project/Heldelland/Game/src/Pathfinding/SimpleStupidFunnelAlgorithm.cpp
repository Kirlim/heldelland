//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Pathfinding/SimpleStupidFunnelAlgorithm.h"

#include<Nest/Math.h>



namespace Game
{
	std::vector<Nest::Vector2<simfix64>> SimpleStupidFunnelAlgorithm::DoProcessPath(const std::vector<Nest::Vector2<simfix64>>& portals)
	{
		std::vector<Nest::Vector2<simfix64>> processedPath{};
		processedPath.emplace_back(portals[0]);


		int apexIndex, leftIndex = 0, rightIndex = 0;

		Nest::Vector2<simfix64> portalApex = portals[0];
		Nest::Vector2<simfix64> portalLeft = portals[0];
		Nest::Vector2<simfix64> portalRight = portals[0];

		const int portalsCount = int(portals.size()) / 2;
		for (int i = 1; i < portalsCount; ++i)
		{
			const auto& checkingPortalLeft = portals[i * 2];
			const auto& checkingPortalRight = portals[i * 2 + 1];

			// Update right vertex
			if (Nest::Math::GetTriangleArea(portalApex, portalRight, checkingPortalRight) >= 0.0f)
			{
				if (portalApex == portalRight || Nest::Math::GetTriangleArea(portalApex, portalLeft, checkingPortalRight) < 0.0f)
				{
					// Tighten the funnel
					portalRight = checkingPortalRight;
					rightIndex = i;
				}
				else
				{
					// Right went over left
					processedPath.emplace_back(portalLeft);
					portalApex = portalLeft;
					apexIndex = leftIndex;

					// Reset portal
					portalLeft = portalApex;
					portalRight = portalApex;

					leftIndex = apexIndex;
					rightIndex = apexIndex;

					// Restart the scan
					i = apexIndex;
					continue;
				}
			}

			// Update left vertex
			if (Nest::Math::GetTriangleArea(portalApex, portalLeft, checkingPortalLeft) <= 0.0f)
			{
				if (portalApex == portalLeft || Nest::Math::GetTriangleArea(portalApex, portalRight, checkingPortalLeft) > 0.0f)
				{
					// Tighten the funnel
					portalLeft = checkingPortalLeft;
					leftIndex = i;
				}
				else
				{
					// Left went over right
					processedPath.emplace_back(portalRight);
					portalApex = portalRight;
					apexIndex = rightIndex;

					// Reset portal
					portalLeft = portalApex;
					portalRight = portalApex;
					leftIndex = apexIndex;
					rightIndex = apexIndex;

					// Restart the scan
					i = apexIndex;
				}
			}
		}


		if (processedPath[processedPath.size() - 1] != portals[portals.size() - 1])
		{
			processedPath.emplace_back(portals[portals.size() - 1]);
		}

		return processedPath;
	}
}

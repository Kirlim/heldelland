//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Pathfinding/BinaryHeap.h"

#include<Nest/Exceptions/NullArgumentException.h>

#include"Game/Pathfinding/AStarNode.h"



namespace Game
{
	BinaryHeap::BinaryHeap() :
		m_MaxSize(0), m_CurrentSize(0), m_HeapData(nullptr),
		m_Nodes(nullptr)
	{
	}



	BinaryHeap::~BinaryHeap() = default;



	void BinaryHeap::SetUp(AStarNode* const nodes, const unsigned int maxSize)
	{
		if (nodes == nullptr)
			ThrowNullArgumentExceptionWithLine("nodes");

		m_HeapData = std::make_unique<int[]>(maxSize);
		m_Nodes = nodes;
		m_MaxSize = maxSize;
	}



	void BinaryHeap::Add(const int node, const int parentNode, const unsigned int walkCost, const unsigned int heuristicCost)
	{
		++m_CurrentSize;

		const int TotalCost = walkCost + heuristicCost;
		m_Nodes[node].parent = parentNode;
		m_Nodes[node].walkCost = walkCost;
		m_Nodes[node].totalCost = TotalCost;
		m_Nodes[node].heapIndex = m_CurrentSize;
		m_HeapData[m_CurrentSize] = node;


		int iterator = m_CurrentSize;
		while (iterator != 1)
		{
			const int parent = iterator / 2;
			if (m_Nodes[m_HeapData[iterator]].totalCost <= m_Nodes[m_HeapData[parent]].totalCost)
			{
				m_Nodes[m_HeapData[iterator]].heapIndex = parent;
				m_Nodes[m_HeapData[parent]].heapIndex = iterator;

				const int swapTmp = m_HeapData[iterator];
				m_HeapData[iterator] = m_HeapData[parent];
				m_HeapData[parent] = swapTmp;

				iterator = parent;
			}
			else
			{
				return;
			}
		}
	}



	void BinaryHeap::UpdateIfNeeded(const int index, const int newParent, const unsigned int walkCost, const unsigned int heuristicCost)
	{
		if (walkCost + heuristicCost >= m_Nodes[m_HeapData[index]].totalCost) return;

		m_Nodes[m_HeapData[index]].walkCost = walkCost;
		m_Nodes[m_HeapData[index]].totalCost = walkCost + heuristicCost;
		m_Nodes[m_HeapData[index]].parent = newParent;

		int iterator = index;
		while (iterator != 1)
		{
			const int parent = iterator / 2;
			if (m_Nodes[m_HeapData[iterator]].totalCost <= m_Nodes[m_HeapData[parent]].totalCost)
			{
				m_Nodes[m_HeapData[iterator]].heapIndex = parent;
				m_Nodes[m_HeapData[parent]].heapIndex = iterator;

				const int swapTmp = m_HeapData[iterator];
				m_HeapData[iterator] = m_HeapData[parent];
				m_HeapData[parent] = swapTmp;

				iterator = parent;
			}
			else
			{
				return;
			}
		}
	}



	int BinaryHeap::FetchTop()
	{
		const int return_value = m_HeapData[1];
		m_Nodes[return_value].heapIndex = -1;
		m_Nodes[m_HeapData[m_CurrentSize]].heapIndex = 1;
		m_HeapData[1] = m_HeapData[m_CurrentSize];
		--m_CurrentSize;

		int current = 1;
		while (true)
		{
			//Swap with lowest child
			int candidate = current;
			if ((current * 2) + 1 <= m_CurrentSize)
			{
				if (m_Nodes[m_HeapData[current]].totalCost > m_Nodes[m_HeapData[current * 2]].totalCost) candidate = current * 2;
				if (m_Nodes[m_HeapData[candidate]].totalCost > m_Nodes[m_HeapData[(current * 2) + 1]].totalCost) candidate = (current * 2) + 1;
			}
			else if (current * 2 <= m_CurrentSize)
			{
				if (m_Nodes[m_HeapData[current]].totalCost > m_Nodes[m_HeapData[current * 2]].totalCost) candidate = current * 2;
			}

			if (current != candidate)
			{
				m_Nodes[m_HeapData[current]].heapIndex = candidate;
				m_Nodes[m_HeapData[candidate]].heapIndex = current;

				const int swap_tmp = m_HeapData[current];
				m_HeapData[current] = m_HeapData[candidate];
				m_HeapData[candidate] = swap_tmp;

				current = candidate;
			}
			else
			{
				return (return_value);
			}
		}
	}



	int BinaryHeap::Size() const
	{
		return (m_CurrentSize);
	}



	void BinaryHeap::Clear()
	{
		m_CurrentSize = 0;
	}
}

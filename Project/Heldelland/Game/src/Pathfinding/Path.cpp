//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Pathfinding/Path.h"

#include<algorithm>



namespace Game
{
	Path::Path() = default;



	Path::~Path() = default;



	void Path::PushBack(Nest::Vector2<simfix64> waypoint)
	{
		m_Waypoints.emplace_back(waypoint, simfix64(0));
	}



	void Path::RecalculateDistances()
	{
		if (m_Waypoints.empty()) return;

		simfix64 accumulatedDistance(0);
		Nest::Vector2<simfix64> previousPosition = m_Waypoints.begin()->position;

		for (auto& waypoint : m_Waypoints)
		{
			Nest::Vector2<simfix64> distance = waypoint.position - previousPosition;
			const simfix64 d = distance.GetMagnitude();

			accumulatedDistance += d;
			waypoint.accumulatedDistance = accumulatedDistance;

			previousPosition = waypoint.position;
		}
	}



	void Path::Invert()
	{
		std::reverse(m_Waypoints.begin(), m_Waypoints.end());
	}



	int Path::GetSize() const
	{
		return int(m_Waypoints.size());
	}



	simfix64 Path::GetTotalDistance() const
	{
		simfix64 distance = simfix64(0);

		if (!m_Waypoints.empty())
		{
			distance = m_Waypoints[m_Waypoints.size() - 1].accumulatedDistance;
		}

		return distance;
	}



	Nest::Vector2<simfix64> Path::GetPositionByIndex(int waypointIndex) const
	{
		Nest::Vector2<simfix64> result;

		if (waypointIndex >= 0 && waypointIndex < int(m_Waypoints.size()))
		{
			result = m_Waypoints[waypointIndex].position;
		}

		return result;
	}



	Nest::Vector2<simfix64> Path::GetPositionByDistance(simfix64 distance) const
	{
		Nest::Vector2<simfix64> pos(simfix64(0), simfix64(0));

		if (distance >= m_Waypoints[m_Waypoints.size() - 1].accumulatedDistance)
		{
			pos = m_Waypoints[m_Waypoints.size() - 1].position;
		}
		else
		{
			for (int i = 0; i < int(m_Waypoints.size()) - 1; ++i)
			{
				if (m_Waypoints[i].accumulatedDistance <= distance && m_Waypoints[i + 1].accumulatedDistance > distance)
				{
					const simfix64 pct = (distance - m_Waypoints[i].accumulatedDistance) / (m_Waypoints[i + 1].accumulatedDistance - m_Waypoints[i].accumulatedDistance);
					pos = Nest::Lerp(m_Waypoints[i].position, m_Waypoints[i + 1].position, pct);
					break;
				}
			}
		}

		return pos;
	}



	void Path::Reset()
	{
		m_Waypoints.clear();
	}
}

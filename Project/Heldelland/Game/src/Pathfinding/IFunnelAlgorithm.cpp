//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Pathfinding/IFunnelAlgorithm.h"

#include<Nest/Exceptions/InvalidDataException.h>
#include<Nest/Exceptions/LogicErrorException.h>



namespace Game
{
	IFunnelAlgorithm::~IFunnelAlgorithm() = default;



	std::vector<Nest::Vector2<simfix64>> IFunnelAlgorithm::ProcessPath(const std::vector<Nest::Vector2<simfix64>>& portals)
	{
		if (portals.size() < 4)
			ThrowLogicErrorExceptionWithLine("Failed to process path. Portals must have at least four elements - path start and end, each appearing twice");

		if (portals.size() % 2)
			ThrowInvalidDataExceptionWithLine("Portals with odd number of elements. Maybe one portal start or end is missing?");

		if (portals[0] != portals[1])
			ThrowLogicErrorExceptionWithLine("First two Portals elements must be equal, as they indicate the starting position");

		if (*(portals.end() - 1) != *(portals.end() - 2))
			ThrowLogicErrorExceptionWithLine("Final two Portals elements must be equal, as they indicate the destination position");

		return DoProcessPath(portals);
	}
}

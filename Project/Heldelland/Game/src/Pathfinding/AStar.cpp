//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Pathfinding/AStar.h"

#include<Nest/Exceptions/NullArgumentException.h>

#include"Game/Pathfinding/AStarNode.h"
#include"Game/Pathfinding/BinaryHeap.h"
#include"Game/Pathfinding/Path.h"
#include"Game/Pathfinding/IFunnelAlgorithm.h"



namespace
{
	bool IsOriginAlsoDestination(int originX, int originY, int destinationX, int destinationY)
	{
		return (originX == destinationX && originY == destinationY);
	}


	bool IsPointInsideMap(int x, int y, int mapWidth, int mapHeight)
	{
		return (x >= 0 && y >= 0 && x < mapWidth && y < mapHeight);
	}
}



namespace Game
{
	AStar::AStar(std::unique_ptr<IFunnelAlgorithm>& funnelAlgorithm) :
		m_MapWidth(0), m_MapHeight(0),
		m_CollisionMap(), m_MapNodes(), m_OpenList(),
		m_CurrentClosedStatus(0),
		m_DestinationX(0), m_DestinationY(0)
	{
		if (funnelAlgorithm == nullptr)
			ThrowNullArgumentExceptionWithLine("Funnel algorithm cannot be null");

		m_FunnelAlgorithm = std::move(funnelAlgorithm);
	}



	AStar::~AStar() = default;



	void AStar::SetUp(const bool* solidTilesMatrix, int mapWidth, int mapHeight, simfix64 tileSize)
	{
		m_MapWidth = mapWidth;
		m_MapHeight = mapHeight;
		m_TileSize = tileSize;

		InitMapNodes(m_MapWidth, m_MapHeight);
		BuildCollisionMap(solidTilesMatrix, m_MapWidth, m_MapHeight);

		m_OpenList = std::make_unique<BinaryHeap>();
		m_OpenList->SetUp(m_MapNodes.get(), m_MapWidth * m_MapHeight);
	}



	bool AStar::FindPath(simfix64 originPosX, simfix64 originPosY, simfix64 destinationPosX, simfix64 destinationPosY, Path& outPath)
	{
		outPath.Reset();

		// Convert positions to tiles
		const int originX = (originPosX / m_TileSize).ToInt();
		const int originY = (originPosY / m_TileSize).ToInt();
		const int destinationX = (destinationPosX / m_TileSize).ToInt();
		const int destinationY = (destinationPosY / m_TileSize).ToInt();

		// Early return, or abortions
		if (IsOriginAlsoDestination(originX, originY, destinationX, destinationY))
			return false;

		if (
			!IsPointInsideMap(destinationX, destinationY, m_MapWidth, m_MapHeight) ||
			!IsPointInsideMap(originX, originY, m_MapWidth, m_MapHeight)
		)
			return false;


		// Prepare for path finding
		m_OpenList->Clear();
		m_CurrentClosedStatus += 2;

		m_OpenList->Add((originY * m_MapWidth) + originX, -1, 0, GetHeuristic((originY * m_MapWidth) + originX));
		const int destination = (destinationY * m_MapWidth) + destinationX;


		bool found = false;

		while (m_OpenList->Size() > 0)
		{
			const int currentNode = m_OpenList->FetchTop();
			const int currentCost = m_MapNodes[currentNode].walkCost;
			m_MapNodes[currentNode].status = m_CurrentClosedStatus;

			if (currentNode == destination)
			{
				found = true;
				break;
			}



			// Add neighbours

			// North
			if ((m_CollisionMap[currentNode] & NavNorth) != 0) AddNeighbour(currentNode - m_MapWidth, currentNode, currentCost + 10);
			// Northwest
			if ((m_CollisionMap[currentNode] & NavNorthwest) != 0) AddNeighbour(currentNode - m_MapWidth - 1, currentNode, currentCost + 14);
			// West
			if ((m_CollisionMap[currentNode] & NavWest) != 0) AddNeighbour(currentNode - 1, currentNode, currentCost + 10);
			// Southwest
			if ((m_CollisionMap[currentNode] & NavSouthwest) != 0) AddNeighbour(currentNode + m_MapWidth - 1, currentNode, currentCost + 14);
			// South
			if ((m_CollisionMap[currentNode] & NavSouth) != 0) AddNeighbour(currentNode + m_MapWidth, currentNode, currentCost + 10);
			// Southeast
			if ((m_CollisionMap[currentNode] & NavSoutheast) != 0) AddNeighbour(currentNode + m_MapWidth + 1, currentNode, currentCost + 14);
			// East
			if ((m_CollisionMap[currentNode] & NavEast) != 0) AddNeighbour(currentNode + 1, currentNode, currentCost + 10);
			// Northeast
			if ((m_CollisionMap[currentNode] & NavNortheast) != 0) AddNeighbour(currentNode - m_MapWidth + 1, currentNode, currentCost + 14);
		}


		if (found)
		{
			static std::vector<Nest::Vector2<simfix64>> portals{};
			BuildPathPortals(portals, originPosX, originPosY, destination);

			auto pathPoints = m_FunnelAlgorithm->ProcessPath(portals);
			for (const auto& point : pathPoints)
			{
				outPath.PushBack(point);
			}

			outPath.RecalculateDistances();

			return true;
		}

		return false;
	}



	void AStar::BuildPathPortals(
		std::vector<Nest::Vector2<simfix64>>& out_Portals, simfix64 originPosX, simfix64 originPosY, int destination
	) const
	{
		out_Portals.clear();

		const auto halfTileSize = m_TileSize * simfix64{ 0.5f };

		int x = destination % m_MapWidth;
		int y = destination / m_MapWidth;

		out_Portals.emplace_back(simfix64{ x * m_TileSize + halfTileSize }, simfix64{ y * m_TileSize + halfTileSize });
		out_Portals.emplace_back(simfix64{ x * m_TileSize + halfTileSize }, simfix64{ y * m_TileSize + halfTileSize });


		int previousY = y;
		int node = m_MapNodes[destination].parent;
		do
		{
			x = node % m_MapWidth;
			y = node / m_MapWidth;

			if (y != previousY)
			{
				int portalLeftX = x, portalRightX = x;
				for (int xp = x - 1; xp >= 0; --xp)
				{
					if (xp == 0 || (m_CollisionMap[node - (x - xp)] & NavWest) == 0)
					{
						portalLeftX = xp;
						break;
					}
				}

				for (int xp = x + 1; xp < m_MapWidth; ++xp)
				{
					if (xp == m_MapWidth - 1 || (m_CollisionMap[node + (xp - x)] & NavEast) == 0)
					{
						portalRightX = xp;
						break;
					}
				}


				// Direction of traveling matters. Note that portals coordinates will be inverted at the end
				// of the method
				if (y < previousY)
				{
					out_Portals.emplace_back(simfix64{ portalRightX } * m_TileSize + halfTileSize, simfix64{ previousY } * m_TileSize + halfTileSize);
					out_Portals.emplace_back(simfix64{ portalLeftX } * m_TileSize + halfTileSize, simfix64{ previousY } * m_TileSize + halfTileSize);

					out_Portals.emplace_back(simfix64{ portalRightX } * m_TileSize + halfTileSize, simfix64{ y } * m_TileSize + halfTileSize);
					out_Portals.emplace_back(simfix64{ portalLeftX } * m_TileSize + halfTileSize, simfix64{ y } * m_TileSize + halfTileSize);
				}
				else
				{
					out_Portals.emplace_back(simfix64{ portalLeftX } * m_TileSize + halfTileSize, simfix64{ previousY } * m_TileSize + halfTileSize);
					out_Portals.emplace_back(simfix64{ portalRightX } * m_TileSize + halfTileSize, simfix64{ previousY } * m_TileSize + halfTileSize);

					out_Portals.emplace_back(simfix64{ portalLeftX } * m_TileSize + halfTileSize, simfix64{ y } * m_TileSize + halfTileSize);
					out_Portals.emplace_back(simfix64{ portalRightX } * m_TileSize + halfTileSize, simfix64{ y } * m_TileSize + halfTileSize);
				}
			}

			previousY = y;
			node = m_MapNodes[node].parent;
		}
		while (node >= 0);


		if (out_Portals.size() > 2)
		{
			// HACK: drop last created portal, as the originY might be misaligned, resulting in the actor moving all the
			// way to the portal side due to inverted portal direction. T
			const auto& lastSide1 = *(out_Portals.end() - 1);
			const auto& lastSide2 = *(out_Portals.end() - 2);

			if (
				(originPosX >= lastSide1.x && originPosX <= lastSide2.x) ||
				(originPosX >= lastSide2.x && originPosX <= lastSide1.x)
			)
			{
				out_Portals.pop_back();
				out_Portals.pop_back();
			}
		}

		out_Portals.emplace_back(originPosX, originPosY);
		out_Portals.emplace_back(originPosX, originPosY);

		std::reverse(std::begin(out_Portals), std::end(out_Portals));
	}



	void AStar::BuildCollisionMap(const bool* solidTilesMatrix, int mapWidth, int mapHeight)
	{
		const int size = m_MapWidth * m_MapHeight;
		m_CollisionMap = std::make_unique<unsigned char[]>(size);

		// Note : negative y is north.

		for (int y = 0; y < mapHeight; ++y)
		{
			for (int x = 0; x < mapWidth; ++x)
			{
				const int index = y * mapWidth + x;
				const int top = index - mapWidth;
				const int bottom = index + mapWidth;

				unsigned char collValue = 0x00;

				if (y > 0)
				{
					// North
					if (!solidTilesMatrix[top]) collValue |= AStar::NavNorth;

					// Northwest
					if (x > 0 && !solidTilesMatrix[top - 1]) collValue |= AStar::NavNorthwest;
				}

				if (x > 0)
				{
					// West
					if (!solidTilesMatrix[index - 1]) collValue |= AStar::NavWest;

					// Southwest
					if (y < mapHeight - 1 && !solidTilesMatrix[bottom - 1]) collValue |= AStar::NavSouthwest;
				}

				if (y < mapHeight - 1)
				{
					// South
					if (!solidTilesMatrix[bottom]) collValue |= AStar::NavSouth;

					// Southeast
					if (x < mapWidth - 1 && !solidTilesMatrix[bottom + 1]) collValue |= AStar::NavSoutheast;
				}

				if (x < mapWidth - 1)
				{
					// East
					if (!solidTilesMatrix[index + 1]) collValue |= AStar::NavEast;

					// Northeast
					if (y > 0 && !solidTilesMatrix[top + 1]) collValue |= AStar::NavNortheast;
				}


				m_CollisionMap[index] = collValue;
			}
		}
	}



	void AStar::InitMapNodes(int mapWidth, int mapHeight)
	{
		const int size = mapWidth * mapHeight;
		m_MapNodes = std::make_unique<AStarNode[]>(size);

		for (int i = 0; i < size; ++i)
		{
			m_MapNodes[i] = AStarNode(0, 0, 0, 0, 0);
		}
	}



	void AStar::AddNeighbour(int node, int parent, int walkCost) const
	{
		// TODO : add logic for not adding if node is currently blocked (i.e static enemy there)

		if (m_MapNodes[node].status < m_CurrentClosedStatus)
		{
			if (m_MapNodes[node].status < m_CurrentClosedStatus - 1)
			{
				// Not on open list
				m_MapNodes[node].status = m_CurrentClosedStatus - 1;
				m_OpenList->Add(node, parent, walkCost, GetHeuristic(node));
			}
			else
			{
				// On open list
				m_OpenList->UpdateIfNeeded(m_MapNodes[node].heapIndex, parent, walkCost, GetHeuristic(node));
			}
		}
	}
}

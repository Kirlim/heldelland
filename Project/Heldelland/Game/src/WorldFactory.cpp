
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/WorldFactory.h"

#include<Nest/World.h>
#include<Nest/ComponentsContainers.h>
#include<Nest/ComponentsFactory.h>
#include<Nest/ResourcesManager.h>
#include<Nest/RenderingManager.h>
#include<Nest/SystemsManager.h>



namespace Game
{
	std::unique_ptr<Nest::World> WorldFactory::InstantiateWorld() const
	{
		std::unique_ptr<Nest::IComponentsContainers> componentsContainers = std::make_unique<Nest::ComponentsContainers>();
		std::unique_ptr<Nest::IComponentsFactory> componentsFactory = std::make_unique<Nest::ComponentsFactory>();
		std::unique_ptr<Nest::IResourcesManager> resourcesManager = std::make_unique<Nest::ResourcesManager>();
		auto renderingManager = std::make_unique<Nest::RenderingManager>();
		auto systemsManager = std::make_unique<Nest::SystemsManager>();

		return std::make_unique<Nest::World>(
			componentsContainers,
			componentsFactory,
			resourcesManager,
			renderingManager,
			systemsManager
		);
	}
}

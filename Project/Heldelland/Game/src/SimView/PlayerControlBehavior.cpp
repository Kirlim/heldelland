
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/SimView/PlayerControlBehavior.h"



namespace Game
{
    PlayerControlBehavior::PlayerControlBehavior() : Component()
    {
        // Nothing here
    }



    PlayerControlBehavior::~PlayerControlBehavior()
    {
        // Nothing here
    }



    void PlayerControlBehavior::SetPlayerSimulationActor(Nest::ComponentReference<SimActorBehavior> simulationActor)
    {
        m_SimActor = simulationActor;
    }



    Nest::ComponentReference<SimActorBehavior> PlayerControlBehavior::GetPlayerSimulationActor() const
    {
        return(m_SimActor);
    }



    void PlayerControlBehavior::OnDestroy()
    {
        // Nothing here
    }
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/SimView/ActorSystem.h"

#include<Nest/Global.h>
#include<Nest/IComponentsContainers.h>
#include<Nest/ITemplatedSingleComponentsContainer.h>
#include<Nest/IEntity.h>
#include<Nest/Transform.h>
#include<Nest/Input.h>

#include"Game/Map/Terrain.h"
#include"Game/Map/Map.h"
#include"Game/Map/MapComponent.h"
#include"Game/SimulationCommons/ActorBehavior.h"



namespace Game
{
	ActorSystem::ActorSystem()
	{
		// Nothing here
	}



	void ActorSystem::DoUpdate(const float deltaTime)
	{
		// Empty, but reserved
	}
}
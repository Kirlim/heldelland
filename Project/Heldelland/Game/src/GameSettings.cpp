//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/GameSettings.h"

#include<Nest/Exceptions/FailedToLoadException.h>

#include<fstream>
#include<yaml-cpp/yaml.h>
#include<Nest/RenderingModule.h>



namespace Game
{
	GameSettings::GameSettings() :
		m_GameRenderer(Nest::GameRenderer::NotSet),
		m_ScreenWidth(0), m_ScreenHeight(0)
	{
		// Nothing here
	}



	void GameSettings::Load(std::string fileName)
	{
		YAML::Node mainNode;

		try
		{
			mainNode = YAML::LoadFile(fileName);
		}
		catch (std::ifstream::failure&)
		{
			ThrowFailedToLoadExceptionWithLine("Failed to load game configuration: file not successfully loaded.");
		}

		if (mainNode["Video"])
		{
			YAML::Node videoNode = mainNode["Video"];
			for (const auto& node : videoNode)
			{
				if (!node.first.IsScalar() || !node.second.IsScalar()) continue;

				if (node.first.as<std::string>() == "ScreenWidth")
				{
					m_ScreenWidth = node.second.as<int>();
				}

				if (node.first.as<std::string>() == "ScreenHeight")
				{
					m_ScreenHeight = node.second.as<int>();
				}

				if (node.first.as<std::string>() == "Renderer")
				{
					const std::string renderer = node.second.as<std::string>();

					if (renderer == "OpenGL")
					{
						m_GameRenderer = Nest::GameRenderer::OpenGL;
					}
					else if (renderer == "Vulkan")
					{
						m_GameRenderer = Nest::GameRenderer::Vulkan;
					}
				}
			}
		}

		if (mainNode["AssetsPaths"])
		{
			YAML::Node assetsPathsNode = mainNode["AssetsPaths"];
			for (const auto& node : assetsPathsNode)
			{
				m_AssetsPaths.push_back(node.as<std::string>());
			}

			m_AssetsPaths.shrink_to_fit();
		}
	}



	Nest::GameRenderer GameSettings::GetGameRenderer() const
	{
		return (m_GameRenderer);
	}



	void GameSettings::SetScreenResolution(int screenWidth, int screenHeight)
	{
		this->m_ScreenWidth = screenWidth;
		this->m_ScreenHeight = screenHeight;
	}



	int GameSettings::GetScreenWidth() const { return (this->m_ScreenWidth); }
	int GameSettings::GetScreenHeight() const { return (this->m_ScreenHeight); }



	const std::vector<std::string>* GameSettings::GetAssetsPaths() const
	{
		return (&m_AssetsPaths);
	}



	void GameSettings::SetGameRenderer(Nest::GameRenderer gameRenderer)
	{
		m_GameRenderer = gameRenderer;
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/MapView.h"

#include<Nest/NestAssert.h>
#include<Nest/MeshesFactory.h>
#include<Nest/IEntity.h>
#include<Nest/EntitiesManager.h>
#include<Nest/Transform.h>
#include<Nest/ComponentReference.h>

#include"Game/Map/IMapData.h"
#include"Game/Map/Map.h"
#include"Game/Map/MapComponent.h"
#include"Game/Map/TerrainView.h"



namespace Game
{
	Nest::ComponentReference<Nest::Transform> InstantiateMapEntity(Nest::EntitiesManager& entitiesManager, Map& map);
	void InstantiateTerrainEntities(
		Nest::ComponentReference<Nest::Transform> mapEntityTransform,
		Nest::EntitiesManager& entitiesManager,
		const IMapData* mapData,
		const TerrainView* terrainView
	);



	MapView::MapView(Map& map, Nest::EntitiesManager& entitiesManager, const Nest::MeshesFactory& meshesFactory) :
		m_Map(map),
		m_EntitiesManager(entitiesManager)
	{
		NestAssert(map.GetTerrain() != nullptr);
		m_TerrainView = std::make_unique<TerrainView>(*map.GetTerrain(), m_EntitiesManager, meshesFactory);
	}



	MapView::~MapView() = default;



	void MapView::InstantiateHierarchyOfEntities() const
	{
		const auto mapEntityTransform = InstantiateMapEntity(m_EntitiesManager, m_Map);
		InstantiateTerrainEntities(mapEntityTransform, m_EntitiesManager, m_Map.GetMapData(), m_TerrainView.get());
	}



	const ITerrainView* MapView::GetTerrainView() const
	{
		return m_TerrainView.get();
	}



	Nest::ComponentReference<Nest::Transform> InstantiateMapEntity(Nest::EntitiesManager& entitiesManager, Map& map)
	{
		auto mapEntity = entitiesManager.Instantiate().lock();
		auto mapComponent = mapEntity->AddComponent<MapComponent>();

		mapEntity->SetName("MapView");
		mapComponent->SetMap(&map);

		return mapEntity->AddComponent<Nest::Transform>();
	}



	void InstantiateTerrainEntities(
		Nest::ComponentReference<Nest::Transform> mapEntityTransform,
		Nest::EntitiesManager& entitiesManager,
		const IMapData* mapData,
		const TerrainView* terrainView
	)
	{
		NestAssert(mapData != nullptr);

		const auto tileset = mapData->GetTileset().lock();
		NestAssert(tileset != nullptr);
		NestAssert(terrainView != nullptr);

		terrainView->InstantiateHierarchyOfEntities(mapEntityTransform, *tileset);
	}
}

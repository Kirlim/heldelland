
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/MapsFactory.h"

#include<Nest/MeshesFactory.h>

#include"Game/Map/IMapData.h"
#include"Game/Map/Map.h"



namespace Game
{
	MapsFactory::MapsFactory(const Nest::MeshesFactory& meshesFactory) :
		m_MeshesFactory(meshesFactory)
	{
		// Nothing here
	}



	std::unique_ptr<Map> MapsFactory::Instantiate(std::unique_ptr<IMapData>& mapData) const
	{
		return std::make_unique<Map>(mapData, m_MeshesFactory, Map::passkey);
	}
}
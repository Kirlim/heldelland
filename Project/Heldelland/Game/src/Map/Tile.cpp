
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/Tile.h"



namespace Game
{
    void Tile::SetTile(const unsigned int tile)
    {
        m_Tile = tile;
    }
    
    
    
    unsigned int Tile::GetTile() const
    {
        return(m_Tile);
    }
    
    
    
    void Tile::SetOrientation(const TileOrientation orientation)
    {
        m_Orientation = orientation;
    }
    
    
    
    TileOrientation Tile::GetOrientation() const
    {
        return(m_Orientation);
    }
}
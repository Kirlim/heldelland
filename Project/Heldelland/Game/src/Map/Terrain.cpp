//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/Terrain.h"

#include<algorithm>

#include<Nest/Vector2.h>
#include<Nest/Vector3.h>
#include<Nest/MeshesFactory.h>

#include"Game/Map/Tile.h"
#include"Game/Map/IMapData.h"
#include"Game/Map/TerrainRegion.h"



namespace Game
{
	Terrain::Terrain(const IMapData& mapData) :
		m_TilesWidth(mapData.GetWidth()), m_TilesHeight(mapData.GetDepth()),
		m_TerrainStartPosX(0.0f), m_TerrainStartPosZ(0.0f),
		m_TerrainEndPosX(float(mapData.GetWidth()) * TileSize), m_TerrainEndPosZ(float(mapData.GetDepth()) * TileSize),
		m_RegionsCount(0)
	{
		BuildRegions(mapData);
	}



	Terrain::~Terrain() = default;



	int Terrain::GetWidth() const { return (m_TilesWidth); }
	int Terrain::GetHeight() const { return (m_TilesHeight); }



	float Terrain::GetPositionHeight(float x, float z) const
	{
		// Clamp coordinates
		auto posX = x >= m_TerrainStartPosX ? x : m_TerrainStartPosX;
		posX = posX < m_TerrainEndPosX ? posX : m_TerrainEndPosX - 0.001f;

		auto posZ = z >= m_TerrainStartPosZ ? z : m_TerrainStartPosZ;
		posZ = posZ < m_TerrainEndPosZ ? posZ : m_TerrainEndPosZ - 0.001f;

		// Detect tile of the coordinates
		const auto tileX = int(posX / TileSize);
		const auto tileZ = int(posZ / TileSize);


		// Detect the region of the coordinates
		const auto regionX = tileX / RegionSizeWidth;
		const auto regionZ = tileZ / RegionSizeHeigth;

		auto regionsCountWidth = m_TilesWidth / RegionSizeWidth;
		if (m_TilesWidth % RegionSizeWidth > 0) ++regionsCountWidth;

		const auto y = m_Regions[regionX + regionZ * regionsCountWidth].GetPositionHeight(posX, posZ, tileX, tileZ);
		return (y);
	}



	void Terrain::GetPositionsHeights(const std::vector<Nest::Vector2f>& positions, std::vector<float>& out_Heights) const
	{
		out_Heights.clear();

		for (const auto& position : positions)
		{
			out_Heights.emplace_back(GetPositionHeight(position.x, position.y));
		}
	}



	TerrainRegion* Terrain::GetRegions() const
	{
		return (m_Regions.get());
	}



	int Terrain::GetRegionsCount() const
	{
		return (m_RegionsCount);
	}



	bool Terrain::RaycastToTerrain(
		const Nest::Vector3<float>& rayOrigin, const Nest::Vector3<float>& rayDirection,
		int& outTileX, int& outTileZ
	) const
	{
		int x, z;
		auto lastSqrDistance = -1.0f;

		for (int i = 0; i < m_RegionsCount; ++i)
		{
			Nest::Vector3<float> intersectionPosition;
			const auto intersect = m_Regions[i].RaycastToTerrain(rayOrigin, rayDirection, /*out*/ x, /*out*/ z, /*out*/ intersectionPosition);

			if (intersect)
			{
				const auto distance = intersectionPosition - rayOrigin;
				const auto sqrDistance = distance.x * distance.x + distance.y * distance.y + distance.z * distance.z;

				if (lastSqrDistance < 0.0f || sqrDistance < lastSqrDistance * lastSqrDistance)
				{
					lastSqrDistance = sqrDistance;

					outTileX = x;
					outTileZ = z;
				}
			}
		}

		return (lastSqrDistance >= 0.0f);
	}



	void Terrain::ForEachTerrainRegion(const std::function<void(const TerrainRegion&)>& action) const
	{
		for (auto i = 0; i < m_RegionsCount; ++i)
		{
			action(m_Regions[i]);
		}
	}



	void Terrain::BuildRegions(const IMapData& mapData)
	{
		auto regionsX = m_TilesWidth / RegionSizeWidth;
		if (m_TilesWidth % RegionSizeWidth) ++regionsX;

		auto regionsY = m_TilesHeight / RegionSizeHeigth;
		if (m_TilesHeight % RegionSizeHeigth) ++regionsY;


		m_RegionsCount = regionsX * regionsY;
		m_Regions = std::make_unique<TerrainRegion[]>(m_RegionsCount);
		for (auto x = 0; x < regionsX; ++x)
		{
			for (auto y = 0; y < regionsY; ++y)
			{
				auto region = &m_Regions[y * regionsX + x];

				const auto startX = x * RegionSizeWidth;
				const auto startY = y * RegionSizeHeigth;
				const auto endX = std::min((x + 1) * RegionSizeWidth, m_TilesWidth) - 1;
				const auto endY = std::min((y + 1) * RegionSizeHeigth, m_TilesHeight) - 1;

				region->SetUp(*this, startX, startY, endX, endY, mapData);
			}
		}
	}



	void Terrain::RotateUv(
		const Nest::Vector2<float>& uvStart, const Nest::Vector2<float>& uvEnd,
		TileOrientation tileOrientation,
		Nest::Vector2<float>& outUvA, Nest::Vector2<float>& outUvB,
		Nest::Vector2<float>& outUvC, Nest::Vector2<float>& outUvD
	)
	{
		switch (tileOrientation)
		{
			case TileOrientation::East:
				outUvA = Nest::Vector2<float>(uvStart.x, uvStart.y);
				outUvB = Nest::Vector2<float>(uvEnd.x, uvStart.y);
				outUvC = Nest::Vector2<float>(uvEnd.x, uvEnd.y);
				outUvD = Nest::Vector2<float>(uvStart.x, uvEnd.y);
				break;
			case TileOrientation::North:
				outUvA = Nest::Vector2<float>(uvStart.x, uvEnd.y);
				outUvB = Nest::Vector2<float>(uvStart.x, uvStart.y);
				outUvC = Nest::Vector2<float>(uvEnd.x, uvStart.y);
				outUvD = Nest::Vector2<float>(uvEnd.x, uvEnd.y);
				break;
			case TileOrientation::West:
				outUvA = Nest::Vector2<float>(uvEnd.x, uvEnd.y);
				outUvB = Nest::Vector2<float>(uvStart.x, uvEnd.y);
				outUvC = Nest::Vector2<float>(uvStart.x, uvStart.y);
				outUvD = Nest::Vector2<float>(uvEnd.x, uvStart.y);
				break;
			case TileOrientation::South:
				outUvA = Nest::Vector2<float>(uvEnd.x, uvStart.y);
				outUvB = Nest::Vector2<float>(uvEnd.x, uvEnd.y);
				outUvC = Nest::Vector2<float>(uvStart.x, uvEnd.y);
				outUvD = Nest::Vector2<float>(uvStart.x, uvStart.y);
				break;
		}
	}
}

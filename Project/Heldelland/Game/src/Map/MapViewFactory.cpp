
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/MapViewFactory.h"

#include<Nest/EntitiesManager.h>
#include<Nest/MeshesFactory.h>

#include"Game/Map/Map.h"
#include"Game/Map/MapView.h"



namespace Game
{
	MapViewFactory::MapViewFactory(Nest::EntitiesManager& entitiesManager, const Nest::MeshesFactory& meshesFactory) :
		m_EntitiesManager(entitiesManager),
		m_MeshesFactory(meshesFactory)
	{
	}



	std::unique_ptr<MapView> MapViewFactory::Instantiate(Map& map) const
	{
		return std::make_unique<MapView>(map, m_EntitiesManager, m_MeshesFactory);
	}
}

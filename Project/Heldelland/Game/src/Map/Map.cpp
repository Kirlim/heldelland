//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/Map.h"

#include<Nest/Exceptions/NullArgumentException.h>

#include<Nest/EntitiesManager.h>
#include<Nest/MeshesFactory.h>
#include<Nest/Vector3.h>

#include"Game/Map/IMapData.h"
#include"Game/Map/Terrain.h"

#include<Nest/MeshFilter.h>



namespace Game
{
	const Map::Passkey Map::passkey{};



	Map::Map(std::unique_ptr<IMapData>& mapData, const Nest::MeshesFactory& meshesFactory, Passkey const&) :
		m_Terrain(std::make_unique<Terrain>(*mapData))
	{
		if (mapData == nullptr)
			ThrowNullArgumentExceptionWithLine("Map Data cannot be null when constructing a map.");

		m_MapData = std::move(mapData);
	}



	Map::~Map() = default;



	IMapData* Map::GetMapData() const
	{
		return m_MapData.get();
	}



	Terrain* Map::GetTerrain() const
	{
		return m_Terrain.get();
	}



	bool Map::RaycastToTerrain(
		const Nest::Vector3<float>& rayOrigin, const Nest::Vector3<float>& rayDirection,
		int& outTileX, int& outTileZ
	) const
	{
		if (m_Terrain == nullptr) return (false);

		return m_Terrain->RaycastToTerrain(rayOrigin, rayDirection, outTileX, outTileZ);
	}
}

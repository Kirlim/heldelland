
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/MapLoader.h"

#include<Nest/Log.h>

#include"Game/Map/IMapData.h"
#include"Game/Map/Tile.h"

#include"Game/Map/MapData.h"



namespace Game
{
	std::unique_ptr<IMapData> MapLoader::MakeTestMap() const
	{
		auto mapData = std::make_unique<MapData>();

		mapData->SetDimensions(9, 9);

		for (unsigned int i = 0; i <= mapData->GetWidth(); ++i)
		{
			for (unsigned int j = 0; j <= mapData->GetDepth(); ++j)
			{
				int h = 0;
				if ((i % 6) <= 2) h += 1;
				if ((j % 6) >= 3) h -= 1;

				if (!mapData->SetVertexHeight(i, j, h))
				{
					Nest::Log::Warning("Failed to set height at " + std::to_string(i) + "-" + std::to_string(j) + ".");
				}
			}
		}

		for (unsigned int i = 0; i < mapData->GetWidth(); ++i)
		{
			for (unsigned int j = 0; j < mapData->GetDepth(); ++j)
			{
				Tile gt{}; // Grass Tile
				Tile gr{}; // Grass with rock
				Tile dt{}; // Dirt tile
				Tile dr{}; // Dirt with rock

				gt.SetTile(0); gt.SetOrientation(TileOrientation::North);
				gr.SetTile(2); gr.SetOrientation(TileOrientation::North);
				dt.SetTile(1); dt.SetOrientation(TileOrientation::North);
				dr.SetTile(3); dr.SetOrientation(TileOrientation::North);

				// Random useless logic to pick random tile value.
				auto tile = ((i * j) / (i + 1)) * 7 + (j * (i + 2) / (j + 1)) * 3;
				tile = tile % 4;

				auto set = false;

				if (tile == 0) set = mapData->SetTile(i, j, gt);
				if (tile == 1) set = mapData->SetTile(i, j, gr);
				if (tile == 2) set = mapData->SetTile(i, j, dt);
				if (tile == 3) set = mapData->SetTile(i, j, dr);

				if (!set)
				{
					Nest::Log::Warning("Failed to set tile at " + std::to_string(i) + "-" + std::to_string(j) + ".");
				}
			}
		}

		return(mapData);
	}
}
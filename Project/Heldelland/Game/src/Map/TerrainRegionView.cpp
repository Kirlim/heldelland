
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/TerrainRegionView.h"

#include<Nest/MeshesFactory.h>
#include<Nest/IEntity.h>
#include<Nest/EntitiesManager.h>
#include<Nest/ComponentReference.h>

#include<Nest/Transform.h>
#include<Nest/MeshFilter.h>
#include<Nest/MeshRenderer.h>

#include"Game/Map/Tileset.h"
#include"Game/Map/TerrainRegion.h"



namespace Game
{
	TerrainRegionView::TerrainRegionView(
		const TerrainRegion& terrainRegion, 
		Nest::EntitiesManager& entitiesManager, 
		const Nest::MeshesFactory& meshesFactory
	) :
		m_ReferencedTerrainRegion(terrainRegion),
		m_EntitiesManager(entitiesManager),
		m_MeshesFactory(meshesFactory)
	{
		BuildTerrainRegionMesh();
	}



	void TerrainRegionView::InstantiateHierarchyOfEntities(
		const Nest::ComponentReference<Nest::Transform>& terrainTransform,
		const Tileset& tileset
	) const
	{
		const auto tilesetMaterial = tileset.GetMaterial();

		NestAssert(terrainTransform != nullptr);
		NestAssert(m_TerrainRegionMesh != nullptr);
		NestAssert(m_TerrainRegionMesh->IsLoaded());
		NestAssert(tilesetMaterial != nullptr);

		auto terrainRegionEntity = m_EntitiesManager.Instantiate().lock();
		terrainRegionEntity->SetName("TerrainRegionView");

		auto terrainRegionTransform = terrainRegionEntity->AddComponent<Nest::Transform>();
		terrainRegionTransform->SetParent(terrainTransform);

		auto terrainRegionMeshFilter = terrainRegionEntity->AddComponent<Nest::MeshFilter>();
		terrainRegionMeshFilter->SetMesh(m_TerrainRegionMesh.get());

		auto terrainRegionMeshRenderer = terrainRegionEntity->AddComponent<Nest::MeshRenderer>();
		terrainRegionMeshRenderer->SetMaterial(tilesetMaterial);
	}



	void TerrainRegionView::BuildTerrainRegionMesh()
	{
		const auto tilesCount = m_ReferencedTerrainRegion.GetTilesCount();

		std::vector<Nest::Vector3<float>> v;
		std::vector<Nest::Vector3<float>> n;
		std::vector<unsigned int> t;
		std::vector<Nest::Vector2<float>> uvs;

		v.reserve(tilesCount * 6);
		n.reserve(tilesCount * 6);
		t.reserve(tilesCount * 6);
		uvs.reserve(tilesCount * 6);

		for (auto i = 0; i < tilesCount * 6; ++i)
		{
			auto vertex = m_ReferencedTerrainRegion.m_Vertices[i];
			auto normal = m_ReferencedTerrainRegion.m_Normals[i];
			auto uv = m_ReferencedTerrainRegion.m_Uv[i];
			auto tri = m_ReferencedTerrainRegion.m_Triangles[i];

			v.emplace_back(vertex.x, vertex.y, vertex.z);
			n.emplace_back(normal.x, normal.y, normal.z);
			uvs.emplace_back(uv.x, uv.y);
			t.emplace_back(tri);
		}

		m_TerrainRegionMesh = m_MeshesFactory.InstantiateEmpty();
		m_TerrainRegionMesh->Make(v, n, uvs, t);
	}
}

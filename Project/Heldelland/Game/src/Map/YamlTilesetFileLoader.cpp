//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/YamlTilesetFileLoader.h"

#include<yaml-cpp/yaml.h>

#include<Nest/Vector2.h>
#include<Nest/IResourcesManager.h>
#include<Nest/Exceptions/BadFileException.h>
#include<Nest/Exceptions/InvalidDataException.h>
#include<Nest/Exceptions/InvalidFileStructureException.h>
#include<Nest/Exceptions/LogicErrorException.h>

#include"Game/Map/Tileset.h"



namespace Game
{
	YamlTilesetFileLoader::YamlTilesetFileLoader(Nest::IResourcesManager& resourcesManager) :
		m_ResourcesManager(resourcesManager)
	{
	}



	YamlTilesetFileLoader::~YamlTilesetFileLoader() = default;



	std::unique_ptr<Tileset> YamlTilesetFileLoader::Load(const std::string file)
	{
		auto mainNode = LoadYamlFile(file);


		// Verify that all the main keys and nodes exists
		if (!mainNode["TileSize"])
			ThrowInvalidFileStructureExceptionWithLine("The tileset file " + file + " does not have a TileSize node.");

		if (!mainNode["TilePixelRepeatSize"])
			ThrowInvalidFileStructureExceptionWithLine("The tileset file " + file + " does not have a TilePixelRepeatSize node.");

		if (!mainNode["TextureSize"])
			ThrowInvalidFileStructureExceptionWithLine("The tileset file " + file + " does not have a TextureSize node.");

		if (!mainNode["TextureSize"]["X"])
			ThrowInvalidFileStructureExceptionWithLine("The tileset file " + file + " does not have a X node for the TextureSize.");

		if (!mainNode["TextureSize"]["Y"])
			ThrowInvalidFileStructureExceptionWithLine("The tileset file " + file + " does not have a Y node for the TextureSize.");

		if (!mainNode["Tileset"])
			ThrowInvalidFileStructureExceptionWithLine("The tileset file " + file + " does not have a Tileset node.");


		// Material
		Nest::Material* material = nullptr;
		if (mainNode["Material"])
		{
			const std::string materialName = mainNode["Material"].as<std::string>();
			material = m_ResourcesManager.FetchMaterial(materialName);

			if (material == nullptr)
				ThrowLogicErrorExceptionWithLine("Tileset references material " + materialName + " that does not exists in the resources manager.");
		}


		// Tiles sizes
		const auto tileSize = mainNode["TileSize"].as<int>();
		const auto tilePixelRepeatSize = mainNode["TilePixelRepeatSize"].as<int>();

		int textureWidth = 0, textureHeight = 0;
		if (mainNode["TextureSize"])
		{
			auto textureSizeNode = mainNode["TextureSize"];
			textureWidth = textureSizeNode["X"].as<int>();
			textureHeight = textureSizeNode["Y"].as<int>();
		}

		if (tileSize <= 0)
			ThrowInvalidDataExceptionWithLine("Tile size [" + std::to_string(tileSize) + "] must be higher than 0.");

		if (tilePixelRepeatSize < 0)
			ThrowInvalidDataExceptionWithLine("Tile Pixel Repeat Size [" + std::to_string(tilePixelRepeatSize) + "] cannot be negative.");

		if (textureWidth <= 0)
			ThrowInvalidDataExceptionWithLine("Texture Width [" + std::to_string(textureWidth) + "] must be higher than 0.");

		if (textureHeight <= 0)
			ThrowInvalidDataExceptionWithLine("Tile size [" + std::to_string(textureHeight) + "] must be higher than 0.");


		// Load tiles data
		std::vector<unsigned int> ids;
		std::vector<Nest::Vector2f> uvsStarts, uvsEnds;

		for (auto node : mainNode["Tileset"])
		{
			if (!node["Id"])
				ThrowInvalidFileStructureExceptionWithLine("Found tile without an ID value.");


			const int id = node["Id"].as<int>();

			if (!node["xPos"])
				ThrowInvalidFileStructureExceptionWithLine("Tile of id [" + std::to_string(id) + "] does not have xPos.");

			if (!node["yPos"])
				ThrowInvalidFileStructureExceptionWithLine("Tile of id [" + std::to_string(id) + "] does not have yPos.");


			const int xPos = node["xPos"].as<int>();
			const int yPos = node["yPos"].as<int>();

			if (id < 0)
				ThrowInvalidDataExceptionWithLine("Tile id [" + std::to_string(id) + "] cannot be negative.");

			if (xPos < 0)
				ThrowInvalidDataExceptionWithLine("Tile of id [" + std::to_string(id) + "] has negative xPos.");

			if (yPos < 0)
				ThrowInvalidDataExceptionWithLine("Tile of id [" + std::to_string(id) + "] has negative yPos.");

			const int xCoordStart = (tileSize + 2 * tilePixelRepeatSize) * xPos + 1;
			const int xCoordEnd = xCoordStart + tileSize;

			const int yCoordStart = (tileSize + 2 * tilePixelRepeatSize) * yPos + 1;
			const int yCoordEnd = yCoordStart + tileSize;


			ids.push_back(id);
			uvsStarts.emplace_back(float(xCoordStart) / float(textureWidth), float(yCoordStart) / float(textureHeight));
			uvsEnds.emplace_back(float(xCoordEnd) / float(textureWidth), float(yCoordEnd) / float(textureHeight));
		}


		return (std::make_unique<Tileset>(ids.size(), ids, uvsStarts, uvsEnds, material));
	}



	YAML::Node YamlTilesetFileLoader::LoadYamlFile(const std::string& file) const
	{
		try
		{
			YAML::Node mainNode = YAML::LoadFile(file);
			return (mainNode);
		}
		catch (YAML::BadFile&)
		{
			ThrowBadFileExceptionWithLine("Bad File when loading YAML File");
		}
		catch (YAML::ParserException& parseException)
		{
			ThrowInvalidFileStructureExceptionWithLine("Loaded file " + file + " has invalid structure:\n" + parseException.msg);
		}
	}
}

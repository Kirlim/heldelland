
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/MapData.h"

#include"Game/Map/Tile.h"



namespace Game
{
    void MapData::SetDimensions(const unsigned int width, const unsigned int depth)
    {
        unsigned int newDepth = depth > 0 ? depth : 1;
        unsigned int newWidth = width > 0 ? width : 1;

        ResizeToDepth(newDepth);
        m_Depth = newDepth;

        ResizeToWidth(newWidth);
        m_Width = newWidth;

        TrimMapMatrices();
    }

    unsigned int MapData::GetWidth() const
    {
        return(m_Width);
    }

    unsigned int MapData::GetDepth() const
    {
        return(m_Depth);
    }



    bool MapData::SetVertexHeight(const unsigned int vertexX, const unsigned int vertexZ, const int height)
    {
        if (vertexX > m_Width || vertexZ > m_Depth) return(false);

        m_Heights[vertexX][vertexZ] = height;
        return(true);
    }

    bool MapData::GetVertexHeight(const unsigned int vertexX, const unsigned int vertexZ, int & outHeight) const
    {
        if (vertexX > m_Width || vertexZ > m_Depth) return(false);

        outHeight = m_Heights[vertexX][vertexZ];
        return(true);
    }



    bool MapData::SetTileId(const unsigned int tileX, const unsigned int tileZ, const unsigned int tileId)
    {
        if (tileX >= m_Width || tileZ >= m_Depth) return(false);

        m_Tiles[tileX][tileZ].SetTile(tileId);
        return(true);
    }

    bool MapData::GetTileId(const unsigned int tileX, const unsigned int tileZ, unsigned int & outTileId) const
    {
        if (tileX >= m_Width || tileZ >= m_Depth) return(false);

        outTileId = m_Tiles[tileX][tileZ].GetTile();
        return(true);
    }



    bool MapData::SetTileOrientation(const unsigned int tileX, const unsigned int tileZ, const TileOrientation orientation)
    {
        if (tileX >= m_Width || tileZ >= m_Depth) return(false);

        m_Tiles[tileX][tileZ].SetOrientation(orientation);
        return(true);
    }

    bool MapData::GetTileOrientation(const unsigned int tileX, const unsigned int tileZ, TileOrientation & outOrientation) const
    {
        if (tileX >= m_Width || tileZ >= m_Depth) return(false);

        outOrientation = m_Tiles[tileX][tileZ].GetOrientation();
        return(true);
    }



    bool MapData::SetTile(const unsigned int tileX, const unsigned int tileZ, const Tile tile)
    {
        if (tileX >= m_Width || tileZ >= m_Depth) return(false);

        m_Tiles[tileX][tileZ] = tile;
        return(true);
    }

    bool MapData::GetTile(const unsigned int tileX, const unsigned int tileZ, Tile & outTile) const
    {
        if (tileX >= m_Width || tileZ >= m_Depth) return(false);

        outTile = m_Tiles[tileX][tileZ];
        return(true);
    }



    void MapData::SetTileset(std::shared_ptr<Tileset> tileset)
    {
        m_Tileset = tileset;
    }

    std::weak_ptr<Tileset> MapData::GetTileset() const
    {
        return(std::weak_ptr<Tileset>(m_Tileset));
    }



    void MapData::ResizeToDepth(const unsigned int depth)
    {
        for (auto i = depth; i < m_Tiles.size(); ++i)
        {
            m_Tiles.pop_back();
        }

        for (auto i = depth + 1; i < m_Heights.size(); ++i)
        {
            m_Heights.pop_back();
        }



        for (auto i = m_Tiles.size(); i < depth; ++i)
        {
            m_Tiles.emplace_back();

            for (unsigned int j = 0; j < m_Width; ++j)
            {
                m_Tiles[i].emplace_back();
                m_Tiles[i][j].SetTile(0);
                m_Tiles[i][j].SetOrientation(TileOrientation::North);
            }
        }

        for (auto i = m_Heights.size(); i <= depth; ++i)
        {
            m_Heights.emplace_back();

            for (unsigned int j = 0; j <= m_Width; ++j)
            {
                m_Heights[i].emplace_back();
                m_Heights[i][j] = 0;
            }
        }
    }



    void MapData::ResizeToWidth(const unsigned int width)
    {
        for (auto& tilesList : m_Tiles)
        {
            while (tilesList.size() > width)
            {
                tilesList.pop_back();
            }

            for (auto i = tilesList.size(); i < width; ++i)
            {
                tilesList.emplace_back();
                tilesList[i].SetTile(0);
                tilesList[i].SetOrientation(TileOrientation::North);
            }
        }

        for (auto& heightsList : m_Heights)
        {
            while (heightsList.size() > width + 1)
            {
                heightsList.pop_back();
            }

            for (auto i = heightsList.size(); i <= width; ++i)
            {
                heightsList.emplace_back();
                heightsList[i] = 0;
            }
        }
    }



    void MapData::TrimMapMatrices()
    {
        TrimTileMatrix();
        TrimHeightsMatrix();
    }



    void MapData::TrimTileMatrix()
    {
        m_Tiles.shrink_to_fit();
        for (auto& tilesList : m_Tiles)
        {
            tilesList.shrink_to_fit();
        }
    }



    void MapData::TrimHeightsMatrix()
    {
        m_Heights.shrink_to_fit();
        for (auto& heightsList : m_Heights)
        {
            heightsList.shrink_to_fit();
        }
    }
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/MapComponent.h"

#include"Game/Map/Map.h"



namespace Game
{
    MapComponent::MapComponent() : Component(),
        m_Map(nullptr)
    {
        // Nothing here
    }



    MapComponent::~MapComponent()
    {
        // Nothing here
    }



    void MapComponent::SetMap(Map* map)
    {
        m_Map = map;
    }



    Map* MapComponent::GetMap() const
    {
        return(m_Map);
    }



    void MapComponent::OnDestroy()
    {
        // Nothing here
    }
}
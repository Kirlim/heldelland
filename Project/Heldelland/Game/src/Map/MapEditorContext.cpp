
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/MapEditorContext.h"

#include"Game/Map/MapEditor.h"



namespace Game
{
	MapEditorContext::MapEditorContext(const std::function<std::unique_ptr<MapEditor>()> mapEditorFactoryFunction) :
		m_MapEditorFactoryFunction(mapEditorFactoryFunction)
	{
		// Nothing here
	}
}

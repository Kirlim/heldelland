//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/Tileset.h"

#include<yaml-cpp/yaml.h>

#include<Nest/Material.h>
#include<Nest/Exceptions/LogicErrorException.h>



namespace Game
{
	Tileset::Tileset(
		const int tilesCount, const std::vector<unsigned int>& tilesIds,
		const std::vector<Nest::Vector2f>& uvsStarts, const std::vector<Nest::Vector2f>& uvsEnds,
		Nest::Material* material
	) : m_Material(material)
	{
		if (tilesCount == 0)
			ThrowLogicErrorExceptionWithLine("Tiles count must be higher than 0");

		if (tilesIds.size() != static_cast<unsigned int>(tilesCount))
			ThrowLogicErrorExceptionWithLine("The amount of tiles ids [" + std::to_string(tilesIds.size()) + "] differs from tiles count [" + std::to_string(tilesCount) + "].");

		if (uvsStarts.size() != static_cast<unsigned int>(tilesCount))
			ThrowLogicErrorExceptionWithLine("The amount of starting UVs [" + std::to_string(uvsStarts.size()) + "] differs from tiles count [" + std::to_string(tilesCount) + "].");

		if (uvsEnds.size() != static_cast<unsigned int>(tilesCount))
			ThrowLogicErrorExceptionWithLine("The amount of ending UVs [" + std::to_string(uvsEnds.size()) + "] differs from tiles count [" + std::to_string(tilesCount) + "].");


		for (int i = 0; i < tilesCount; ++i)
		{
			TileInfo tileInfo;
			tileInfo.uvStart = uvsStarts[i];
			tileInfo.uvEnd = uvsEnds[i];

			auto id = tilesIds[i];

			if (m_Tiles.count(id) > 0)
				ThrowLogicErrorExceptionWithLine("Duplicated tile id [" + std::to_string(id) + "].");

			m_Tiles[id] = tileInfo;
		}
	}



	Tileset::~Tileset() = default;



	void Tileset::SetMaterial(Nest::Material* material)
	{
		m_Material = material;
	}



	Nest::Material* Tileset::GetMaterial() const
	{
		return (m_Material);
	}



	void Tileset::SetTileInformation(const unsigned int tileId, Nest::Vector2f uvStart, Nest::Vector2f uvEnd)
	{
		if (m_Tiles.count(tileId) == 0)
			ThrowLogicErrorExceptionWithLine("The tile id [" + std::to_string(tileId) + "] does not belong to the tileset.");


		TileInfo info;
		info.uvStart = uvStart;
		info.uvEnd = uvEnd;

		m_Tiles[tileId] = info;
	}



	void Tileset::GetTileInformation(const unsigned int tileId, Nest::Vector2f& outUvStart, Nest::Vector2f& outUvEnd) const
	{
		if (m_Tiles.count(tileId) == 0)
			ThrowLogicErrorExceptionWithLine("The tile id [" + std::to_string(tileId) + "] does not belong to the tileset.");


		const TileInfo info = m_Tiles.at(tileId);
		outUvStart = info.uvStart;
		outUvEnd = info.uvEnd;
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/TerrainRegion.h"

#include<Nest/Math.h>
#include<Nest/Log.h>
#include<Nest/Vector2.h>
#include<Nest/Vector3.h>

#include<Nest/Exceptions/ValueOutOfRangeException.h>

#include"Game/Map/Terrain.h"
#include"Game/Map/Tileset.h"
#include"Game/Map/Tile.h"
#include"Game/Map/IMapData.h"



namespace Game
{
	TerrainRegion::TerrainRegion() :
		m_Owner(nullptr),
		m_StartX(0), m_StartY(0), m_EndX(0), m_EndY(0)
	{
	}



	TerrainRegion::~TerrainRegion() = default;



	void TerrainRegion::SetUp(
		const Terrain& terrain,
		const int startX, const int startY, const int endX, const int endY,
		const IMapData& mapData
	)
	{
		if (startX < 0 || startX >= terrain.GetWidth())
			ThrowValueOutOfRangeExceptionWithLine("Parameter startX is out of range.");
		if (startY < 0 || startY >= terrain.GetHeight())
			ThrowValueOutOfRangeExceptionWithLine("Parameter startY is out of range.");

		m_Owner = &terrain;

		m_StartX = startX;
		m_StartY = startY;
		m_EndX = endX;
		m_EndY = endY;

		BuildRegionMesh(mapData);
	}



	bool TerrainRegion::RaycastToTerrain(
		const Nest::Vector3<float>& rayOrigin, const Nest::Vector3<float>& rayDirection,
		int& outTileX, int& outTileZ,
		Nest::Vector3<float>& outIntersectionPosition
	) const
	{
		float smallestDistance = -1.0f;

		for (int x = m_StartX; x <= m_EndX; ++x)
		{
			for (int y = m_StartY; y <= m_EndY; ++y)
			{
				int v = GetTileFirstVertexIndex(x, y) * 6; // TODO : multiply by 6 inside the method. Remove by 6 multiplication that are outside of it
				float distance;

				bool intersect = Nest::Math::RayTriIntersect(
					rayOrigin, rayDirection,
					m_Vertices[m_Triangles[v]], m_Vertices[m_Triangles[v + 1]], m_Vertices[m_Triangles[v + 2]],
					0.0001f,
					/*out*/ distance
				);

				if (!intersect)
				{
					intersect = Nest::Math::RayTriIntersect(
						rayOrigin, rayDirection,
						m_Vertices[m_Triangles[v + 3]], m_Vertices[m_Triangles[v + 4]], m_Vertices[m_Triangles[v + 5]],
						0.0001f,
						/*out*/ distance
					);
				}

				if (intersect)
				{
					if (smallestDistance < 0.0f || distance < smallestDistance)
					{
						outTileX = x;
						outTileZ = y;
						smallestDistance = distance;
					}
				}
			}
		}


		if (smallestDistance >= 0.0f)
		{
			outIntersectionPosition = rayOrigin + (rayDirection * smallestDistance);
			return (true);
		}
		else
		{
			return (false);
		}
	}



	float TerrainRegion::GetPositionHeight(const float x, const float z, const int tileX, const int tileZ) const
	{
		int v = GetTileFirstVertexIndex(tileX, tileZ) * 6; // TODO : multiply by 6 inside the method. Remove multiplications outside

		// Get the highest vertex
		float highestVertex = m_Vertices[m_Triangles[v]].y * m_Owner->TileSize;
		float vertexHeight = 0.0f;

		for (int i = 1; i < 6; ++i)
		{
			vertexHeight = m_Vertices[m_Triangles[v + i]].y * m_Owner->TileSize;
			highestVertex = vertexHeight > highestVertex ? vertexHeight : highestVertex;
		}

		highestVertex += 1.0f;


		// TODO : octree to avoid these special cases
		// Raycast down
		float distance = -1.0f;
		Nest::Vector3<float> rayOrigin(x, highestVertex, z);
		Nest::Vector3<float> rayDirection(0.0f, -1.0f, 0.0f);


		bool intersect = Nest::Math::RayTriIntersect(
			rayOrigin, rayDirection,
			m_Vertices[m_Triangles[v]], m_Vertices[m_Triangles[v + 1]], m_Vertices[m_Triangles[v + 2]],
			0.0001f,
			/*out*/ distance
		);


		if (!intersect)
		{
			intersect = Nest::Math::RayTriIntersect(
				rayOrigin, rayDirection,
				m_Vertices[m_Triangles[v + 3]], m_Vertices[m_Triangles[v + 4]], m_Vertices[m_Triangles[v + 5]],
				0.0001f,
				/*out*/ distance
			);
		}


		float position = rayOrigin.y;
		if (intersect)
		{
			position -= distance;
		}

		return (position);
	}



	int TerrainRegion::GetTilesCount() const
	{
		return ((m_EndX + 1) - m_StartX) * ((m_EndY + 1) - m_StartY);
	}



	void TerrainRegion::BuildRegionMesh(const IMapData& mapData)
	{
		const auto tilesCount = GetTilesCount();

		// Alocate helping vectors
		m_Vertices = std::make_unique<VertexType[]>(tilesCount * 6);
		m_Normals = std::make_unique<NormalType[]>(tilesCount * 6);
		m_Uv = std::make_unique<UvType[]>(tilesCount * 6);
		m_Uv1 = std::make_unique<UvType[]>(tilesCount * 6);
		m_Colors = std::make_unique<ColorType[]>(tilesCount * 6);
		m_Triangles = std::make_unique<int[]>(tilesCount * 6);


		// Build the mesh data
		for (auto x = m_StartX; x <= m_EndX; ++x)
		{
			for (auto y = m_StartY; y <= m_EndY; ++y)
			{
				RefreshTileMesh(x, y, mapData);
			}
		}
	}



	void TerrainRegion::RefreshTileMesh(const int x, const int y, const IMapData& mapData)
	{
		UvType uvStart, uvEnd;
		UvType uvA, uvB, uvC, uvD;

		// Obtain the uv
		// TODO : verify that GetTileId has returned success
		Tile tile;
		mapData.GetTile(x, y, /*out*/ tile);

		// TODO : check that tileset is not nullptr
		// TODO : what to do if tile id does not exist in the tileset
		mapData.GetTileset().lock()->GetTileInformation(tile.GetTile(), /*out*/ uvStart, /*out*/ uvEnd);

		m_Owner->RotateUv(uvStart, uvEnd, tile.GetOrientation(), /*out*/ uvA, /*out*/ uvB, /*out*/ uvC, /*out*/ uvD); // TODO : get the tile orientation from somewhere

		// Set up the tile mesh
		TileShape tileShape = GetTileShape(x, y, mapData);
		switch (tileShape)
		{
			case TileShape::Plain: SetPlaneTile(x, y, uvA, uvB, uvC, uvD, mapData);
				break;
			case TileShape::SimpleSlope: SetPlaneTile(x, y, uvA, uvB, uvC, uvD, mapData);
				break;
			case TileShape::DiamondSlope: SetFoldedTile(x, y, uvA, uvB, uvC, uvD, mapData);
				break;
			default:
				Nest::Log::Error("Unknow tile shape found at [" + std::to_string(x) + "; " + std::to_string(y) + "].");
				break;
		}
	}



	auto TerrainRegion::GetTileShape(const int x, const int y, const IMapData& mapData) const -> TileShape
	{
		// Store vertices heights, for shorter reading and writing
		// TODO : what to do if get height fails
		int hA = 0, hB = 0, hC = 0, hD = 0;
		mapData.GetVertexHeight(x, y, /*out*/ hA);
		mapData.GetVertexHeight(x + 1, y, /*out*/ hB);
		mapData.GetVertexHeight(x + 1, y + 1, /*out*/ hC);
		mapData.GetVertexHeight(x, y + 1, /*out*/ hD);


		// Find the tile shape
		TileShape tileShape = TileShape::None;

		if (hA == hB && hB == hC && hC == hD) tileShape = TileShape::Plain;

		if (tileShape == TileShape::None)
		{
			if ((hA == hB && hC == hD) || (hA == hD && hB == hC)) tileShape = TileShape::SimpleSlope;
		}

		if (tileShape == TileShape::None)
		{
			if (hA == hC || hB == hD) tileShape = TileShape::DiamondSlope;
		}

		return (tileShape);
	}



	void TerrainRegion::SetPlaneTile(const int x, const int y, const UvType& uvA, const UvType& uvB, const UvType& uvC, const UvType& uvD, const IMapData& mapData)
	{
		// Tile vertices positioning
		//     2       5---4
		//   / |       | /
		// 0---1       3

		const float tx = float(x) * m_Owner->TileSize; // tile vertex coordinate
		const float ty = float(y) * m_Owner->TileSize;
		const float ts = m_Owner->TileSize; // Tile size

		const int v = GetTileFirstVertexIndex(x, y) * 6; // Vertex 0


		// Build values

		// TODO : what to do if get height fails
		int h0 = 0, h1 = 0, h2 = 0, h3 = 0, h4 = 0, h5 = 0;
		mapData.GetVertexHeight(x, y, /*out*/ h0);
		mapData.GetVertexHeight(x + 1, y, /*out*/ h1);
		mapData.GetVertexHeight(x + 1, y + 1, /*out*/ h2);
		mapData.GetVertexHeight(x, y, /*out*/ h3);
		mapData.GetVertexHeight(x + 1, y + 1, /*out*/ h4);
		mapData.GetVertexHeight(x, y + 1, /*out*/ h5);

		m_Vertices[v] = VertexType(tx, (float)h0 * ts, ty);
		m_Vertices[v + 1] = VertexType(tx + ts, (float)h1 * ts, ty);
		m_Vertices[v + 2] = VertexType(tx + ts, (float)h2 * ts, ty + ts);
		m_Vertices[v + 3] = VertexType(tx, (float)h3 * ts, ty);
		m_Vertices[v + 4] = VertexType(tx + ts, (float)h4 * ts, ty + ts);
		m_Vertices[v + 5] = VertexType(tx, (float)h5 * ts, ty + ts);


		m_Colors[v] = ColorType(1.0f, 1.0f, 1.0f);
		m_Colors[v + 1] = ColorType(1.0f, 1.0f, 1.0f);
		m_Colors[v + 2] = ColorType(1.0f, 1.0f, 1.0f);
		m_Colors[v + 3] = ColorType(1.0f, 1.0f, 1.0f);
		m_Colors[v + 4] = ColorType(1.0f, 1.0f, 1.0f);
		m_Colors[v + 5] = ColorType(1.0f, 1.0f, 1.0f);

		RefreshPlaneTileUvs(v, uvA, uvB, uvC, uvD);


		CalculateNormals(v);
		CalculateTriangles(v);
	}



	void TerrainRegion::SetFoldedTile(const int x, const int y, const UvType& uvA, const UvType& uvB, const UvType& uvC, const UvType& uvD, const IMapData& mapData)
	{
		// Tile vertices positioning (vertical cut)
		//     2       5---4
		//   / |       | /
		// 0---1       3

		// Tile vertices positioning (horizontal cut)
		// 2           5---4
		// | \           \ |
		// 0---1           3

		const float tx = float(x) * m_Owner->TileSize; // tile vertex coordinate
		const float ty = float(y) * m_Owner->TileSize;
		const float ts = m_Owner->TileSize; // Tile size

		const int v = GetTileFirstVertexIndex(x, y) * 6; // Vertex 0


		bool horizontalCut = IsAHorizontalCut(x, y, mapData);
		int h0 = 0, h1 = 0, h2 = 0, h3 = 0, h4 = 0, h5 = 0;

		if (horizontalCut)
		{
			// TODO : what to do if get height returns false
			mapData.GetVertexHeight(x, y, /*out*/ h0);
			mapData.GetVertexHeight(x + 1, y, /*out*/ h1);
			mapData.GetVertexHeight(x, y + 1, /*out*/ h2);
			mapData.GetVertexHeight(x + 1, y, /*out*/ h3);
			mapData.GetVertexHeight(x + 1, y + 1, /*out*/ h4);
			mapData.GetVertexHeight(x, y + 1, /*out*/ h5);

			m_Vertices[v] = VertexType(tx, (float)h0 * ts, ty);
			m_Vertices[v + 1] = VertexType(tx + ts, (float)h1 * ts, ty);
			m_Vertices[v + 2] = VertexType(tx, (float)h2 * ts, ty + ts);
			m_Vertices[v + 3] = VertexType(tx + ts, (float)h3 * ts, ty);
			m_Vertices[v + 4] = VertexType(tx + ts, (float)h4 * ts, ty + ts);
			m_Vertices[v + 5] = VertexType(tx, (float)h5 * ts, ty + ts);

			m_Colors[v] = ColorType(1.0f, 1.0f, 1.0f);
			m_Colors[v + 1] = ColorType(1.0f, 1.0f, 1.0f);
			m_Colors[v + 2] = ColorType(1.0f, 1.0f, 1.0f);
			m_Colors[v + 3] = ColorType(1.0f, 1.0f, 1.0f);
			m_Colors[v + 4] = ColorType(1.0f, 1.0f, 1.0f);
			m_Colors[v + 5] = ColorType(1.0f, 1.0f, 1.0f);
		}
		else
		{
			// TODO : what to do if get height returns false
			mapData.GetVertexHeight(x, y, /*out*/ h0);
			mapData.GetVertexHeight(x + 1, y, /*out*/ h1);
			mapData.GetVertexHeight(x + 1, y + 1, /*out*/ h2);
			mapData.GetVertexHeight(x, y, /*out*/ h3);
			mapData.GetVertexHeight(x + 1, y + 1, /*out*/ h4);
			mapData.GetVertexHeight(x, y + 1, /*out*/ h5);

			m_Vertices[v] = VertexType(tx, (float)h0 * ts, ty);
			m_Vertices[v + 1] = VertexType(tx + ts, (float)h1 * ts, ty);
			m_Vertices[v + 2] = VertexType(tx + ts, (float)h2 * ts, ty + ts);
			m_Vertices[v + 3] = VertexType(tx, (float)h3 * ts, ty);
			m_Vertices[v + 4] = VertexType(tx + ts, (float)h4 * ts, ty + ts);
			m_Vertices[v + 5] = VertexType(tx, (float)h5 * ts, ty + ts);


			m_Colors[v] = ColorType(1.0f, 1.0f, 1.0f);
			m_Colors[v + 1] = ColorType(1.0f, 1.0f, 1.0f);
			m_Colors[v + 2] = ColorType(1.0f, 1.0f, 1.0f);
			m_Colors[v + 3] = ColorType(1.0f, 1.0f, 1.0f);
			m_Colors[v + 4] = ColorType(1.0f, 1.0f, 1.0f);
			m_Colors[v + 5] = ColorType(1.0f, 1.0f, 1.0f);
		}

		RefreshFoldedTileUvs(v, uvA, uvB, uvC, uvD, horizontalCut);
		CalculateNormals(v);
		CalculateTriangles(v);
	}



	bool TerrainRegion::IsAHorizontalCut(const int x, const int y, const IMapData& mapData) const
	{
		int h0 = 0, h1 = 1;
		mapData.GetVertexHeight(x + 1, y, /*out*/ h0);
		mapData.GetVertexHeight(x, y + 1, /*out*/ h1);

		return (h0 == h1);
	}



	int TerrainRegion::GetTileFirstVertexIndex(const int x, const int y) const
	{
		return (
			((y - m_StartY) * ((m_EndX + 1) - m_StartX)) + (x - m_StartX)
		);
	}



	void TerrainRegion::RefreshPlaneTileUvs(
		const int baseVertex, const UvType& uvA, const UvType& uvB, const UvType& uvC, const UvType& uvD
	)
	{
		m_Uv[baseVertex] = UvType(uvA.x, uvA.y);
		m_Uv[baseVertex + 1] = UvType(uvB.x, uvB.y);
		m_Uv[baseVertex + 2] = UvType(uvC.x, uvC.y);
		m_Uv[baseVertex + 3] = UvType(uvA.x, uvA.y);
		m_Uv[baseVertex + 4] = UvType(uvC.x, uvC.y);
		m_Uv[baseVertex + 5] = UvType(uvD.x, uvD.y);

		m_Uv1[baseVertex] = m_Uv[baseVertex];
		m_Uv1[baseVertex + 1] = m_Uv[baseVertex + 1];
		m_Uv1[baseVertex + 2] = m_Uv[baseVertex + 2];
		m_Uv1[baseVertex + 3] = m_Uv[baseVertex + 3];
		m_Uv1[baseVertex + 4] = m_Uv[baseVertex + 4];
		m_Uv1[baseVertex + 5] = m_Uv[baseVertex + 5];
	}



	void TerrainRegion::RefreshFoldedTileUvs(
		const int baseVertex, const UvType& uvA, const UvType& uvB, const UvType& uvC, const UvType& uvD, bool horizontalCut
	)
	{
		if (horizontalCut)
		{
			m_Uv[baseVertex] = UvType(uvA.x, uvA.y);
			m_Uv[baseVertex + 1] = UvType(uvB.x, uvB.y);
			m_Uv[baseVertex + 2] = UvType(uvD.x, uvD.y);
			m_Uv[baseVertex + 3] = UvType(uvB.x, uvB.y);
			m_Uv[baseVertex + 4] = UvType(uvC.x, uvC.y);
			m_Uv[baseVertex + 5] = UvType(uvD.x, uvD.y);

			m_Uv1[baseVertex] = m_Uv[baseVertex];
			m_Uv1[baseVertex + 1] = m_Uv[baseVertex + 1];
			m_Uv1[baseVertex + 2] = m_Uv[baseVertex + 2];
			m_Uv1[baseVertex + 3] = m_Uv[baseVertex + 3];
			m_Uv1[baseVertex + 4] = m_Uv[baseVertex + 4];
			m_Uv1[baseVertex + 5] = m_Uv[baseVertex + 5];
		}
		else
		{
			m_Uv[baseVertex] = UvType(uvA.x, uvA.y);
			m_Uv[baseVertex + 1] = UvType(uvB.x, uvB.y);
			m_Uv[baseVertex + 2] = UvType(uvC.x, uvC.y);
			m_Uv[baseVertex + 3] = UvType(uvA.x, uvA.y);
			m_Uv[baseVertex + 4] = UvType(uvC.x, uvC.y);
			m_Uv[baseVertex + 5] = UvType(uvD.x, uvD.y);

			m_Uv1[baseVertex] = m_Uv[baseVertex];
			m_Uv1[baseVertex + 1] = m_Uv[baseVertex + 1];
			m_Uv1[baseVertex + 2] = m_Uv[baseVertex + 2];
			m_Uv1[baseVertex + 3] = m_Uv[baseVertex + 3];
			m_Uv1[baseVertex + 4] = m_Uv[baseVertex + 4];
			m_Uv1[baseVertex + 5] = m_Uv[baseVertex + 5];
		}
	}



	void TerrainRegion::CalculateNormals(const int baseVertex)
	{
		m_Normals[baseVertex] = Nest::Math::GetNormalizedNormal<float>(m_Vertices[baseVertex + 0], m_Vertices[baseVertex + 2], m_Vertices[baseVertex + 1]);
		m_Normals[baseVertex + 1] = Nest::Math::GetNormalizedNormal<float>(m_Vertices[baseVertex + 1], m_Vertices[baseVertex + 0], m_Vertices[baseVertex + 2]);
		m_Normals[baseVertex + 2] = Nest::Math::GetNormalizedNormal<float>(m_Vertices[baseVertex + 2], m_Vertices[baseVertex + 1], m_Vertices[baseVertex + 0]);
		m_Normals[baseVertex + 3] = Nest::Math::GetNormalizedNormal<float>(m_Vertices[baseVertex + 3], m_Vertices[baseVertex + 5], m_Vertices[baseVertex + 4]);
		m_Normals[baseVertex + 4] = Nest::Math::GetNormalizedNormal<float>(m_Vertices[baseVertex + 4], m_Vertices[baseVertex + 3], m_Vertices[baseVertex + 5]);
		m_Normals[baseVertex + 5] = Nest::Math::GetNormalizedNormal<float>(m_Vertices[baseVertex + 5], m_Vertices[baseVertex + 4], m_Vertices[baseVertex + 3]);
	}



	void TerrainRegion::CalculateTriangles(const int baseVertex)
	{
		m_Triangles[baseVertex] = baseVertex;
		m_Triangles[baseVertex + 1] = baseVertex + 2;
		m_Triangles[baseVertex + 2] = baseVertex + 1;
		m_Triangles[baseVertex + 3] = baseVertex + 3;
		m_Triangles[baseVertex + 4] = baseVertex + 5;
		m_Triangles[baseVertex + 5] = baseVertex + 4;
	}
}

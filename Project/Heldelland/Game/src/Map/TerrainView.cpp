//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Map/TerrainView.h"

#include<Nest/NestAssert.h>
#include<Nest/MeshesFactory.h>
#include<Nest/IEntity.h>
#include<Nest/EntitiesManager.h>
#include<Nest/Transform.h>

#include"Game/Map/Terrain.h"
#include"Game/Map/TerrainRegionView.h"



namespace Game
{
	TerrainView::TerrainView(const Terrain& terrain, Nest::EntitiesManager& entitiesManager, const Nest::MeshesFactory& meshesFactory) :
		m_Terrain(terrain),
		m_EntitiesManager(entitiesManager)
	{
		const auto terrainRegionsCount = terrain.GetRegionsCount();
		NestAssert(terrainRegionsCount > 0);

		m_TerrainRegionViews.reserve(terrainRegionsCount);
		terrain.ForEachTerrainRegion(
			[&](const TerrainRegion& terrainRegion)
			{
				m_TerrainRegionViews.emplace_back(std::make_unique<TerrainRegionView>(terrainRegion, entitiesManager, meshesFactory));
			}
		);
	}



	TerrainView::~TerrainView() = default;



	void TerrainView::InstantiateHierarchyOfEntities(
		const Nest::ComponentReference<Nest::Transform>& mapTransform,
		const Tileset& tileset
	) const
	{
		NestAssert(mapTransform != nullptr);

		auto terrainViewEntity = m_EntitiesManager.Instantiate().lock();
		terrainViewEntity->SetName("TerrainView");

		auto terrainEntityTransform = terrainViewEntity->AddComponent<Nest::Transform>();
		terrainEntityTransform->SetParent(mapTransform);

		for (const auto& terrainRegionView : m_TerrainRegionViews)
		{
			terrainRegionView->InstantiateHierarchyOfEntities(terrainEntityTransform, tileset);
		}
	}



	float TerrainView::GetPositionHeight(float x, float z) const
	{
		return m_Terrain.GetPositionHeight(x, z);
	}



	void TerrainView::GetPositionsHeights(const std::vector<Nest::Vector2f>& positions, std::vector<float>& out_Heights) const
	{
		m_Terrain.GetPositionsHeights(positions, out_Heights);
	}
}

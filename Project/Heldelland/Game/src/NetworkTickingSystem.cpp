
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/NetworkTickingSystem.h"

#include"Game/Network/INetworkSystem.h"
#include"Game/Network/IHostChannel.h"
#include"Game/Game.h"



namespace Game
{
    NetworkTickingSystem::NetworkTickingSystem() :
        System()
    {
        // Nothing here
    }



    NetworkTickingSystem::~NetworkTickingSystem()
    {
        // Nothing here
    }



    void NetworkTickingSystem::DoFixedUpdate(float fixedDeltaTime)
    {
        if (Network != nullptr)
        {
            Network->Tick();
        }
    }
}
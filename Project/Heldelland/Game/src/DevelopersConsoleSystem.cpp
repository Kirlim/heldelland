//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/DevelopersConsoleSystem.h"

#include<Nest/Exceptions/InvalidArgumentException.h>
#include<Nest/Exceptions/InvalidOperationException.h>

#include<Nest/Input.h>
#include<Nest/IComponentsContainers.h>
#include<Nest/ITemplatedSingleComponentsContainer.h>
#include<Nest/Helpers/StringHelper.h>
#include<Nest/Global.h>

#include"Game/DevelopersConsoleBehavior.h"



namespace Game
{
	DevelopersConsoleSystem::DevelopersConsoleSystem(const Nest::IComponentsContainers& componentsContainers) :
		m_ComponentsContainers(componentsContainers)
	{
	}



	DevelopersConsoleSystem::~DevelopersConsoleSystem() = default;



	void DevelopersConsoleSystem::RegisterCommand(const std::string& command, ActionFunction action)
	{
		// TODO : check if command follows proper syntax

		if (action == nullptr)
			ThrowInvalidArgumentExceptionWithLine("[Developers Console System] Action cannot be null.");

		if (command.empty())
			ThrowInvalidArgumentExceptionWithLine("[Developers Console System] Command cannot be empty");

		if (m_Actions.count(command) > 0)
			ThrowInvalidArgumentExceptionWithLine("[Developers Console System] Command " + command + " is already registered.");


		m_Actions[command] = action;
	}



	bool DevelopersConsoleSystem::IsCommandRegistered(std::string command) const
	{
		return (m_Actions.count(command) > 0);
	}



	void DevelopersConsoleSystem::UnregisterCommand(std::string command)
	{
		if (m_Actions.count(command) == 0)
			ThrowInvalidOperationExceptionWithLine("[Developers Console System] Command " + command + " cannot be unregistered, as it is not registered.");

		m_Actions.erase(command);
	}



	void DevelopersConsoleSystem::ExecuteLine(const std::string& line) const
	{
		// TODO : trim line?
		if (line.empty()) return;

		auto splittedString = Nest::Helpers::SplitString(line, ' ');
		if (splittedString.empty()) return;

		const auto& command = splittedString[0];
		if (m_Actions.count(command) == 0)
			throw std::logic_error("[Developers Console System] Command [" + command + "] was not found.");


		m_Actions.at(command)(splittedString);
	}



	void DevelopersConsoleSystem::WriteOutputLine(std::string line)
	{
		std::unique_lock<std::mutex> pendingOutputLinesLock{ m_PendingOutputLinesMutex };
		m_PendingOutputLines.emplace_back(std::move(line));
	}



	void DevelopersConsoleSystem::DoFixedUpdate(float deltaTime)
	{
		const auto consoles = m_ComponentsContainers.GetSingleContainer<DevelopersConsoleBehavior>();
		if (consoles->GetElementsCount() == 0)
			return;

		NestAssert(consoles->GetElementsCount() == 1);
		DevelopersConsoleBehavior* developersConsole = nullptr;
		for (auto& console : *consoles)
		{
			developersConsole = &console;
		}

		WritePendingLinesToConsole(*developersConsole);
		ConsoleVisibilityTogglingLogic(*developersConsole);
	}



	void DevelopersConsoleSystem::WritePendingLinesToConsole(DevelopersConsoleBehavior& developersConsole)
	{
		std::unique_lock<std::mutex> pendingOutputLinesLock{ m_PendingOutputLinesMutex };
		for (const auto& pendingOutputLine : m_PendingOutputLines)
		{
			developersConsole.WriteLine(pendingOutputLine);
		}

		m_PendingOutputLines.clear();
	}



	void DevelopersConsoleSystem::ConsoleVisibilityTogglingLogic(DevelopersConsoleBehavior& developersConsole)
	{
		const auto input = Nest::Global->GetInput();

		if (input->IsKeyOnDown(developersConsole.GetToggleKeyboardKey()))
		{
			developersConsole.ToggleVisibility();
		}

		if (developersConsole.IsShowing())
		{
			developersConsole.m_CommandTextMesh->SetText(*developersConsole.m_CommandString.get());

			if (input->IsKeyOnDown(Nest::KeyCode::Enter))
			{
				developersConsole.Commit();
			}
		}
	}
}

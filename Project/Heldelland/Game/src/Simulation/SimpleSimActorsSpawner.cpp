
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/SimpleSimActorsSpawner.h"

#include<stdexcept>

#include<Nest/IEntity.h>



namespace Game
{
    SimpleSimActorsSpawner::SimpleSimActorsSpawner()
    {
        // Nothing here
    }



    SimpleSimActorsSpawner::~SimpleSimActorsSpawner()
    {
        // Nothing here
    }



    Nest::IEntity* SimpleSimActorsSpawner::Spawn(ActorTypeId actorTypeId) const
    {
        if (m_SpawnFunctions.count(actorTypeId) == 0)
        {
            throw std::runtime_error("Trying to spawn from unknown actor type id.");
        }

        auto& function = m_SpawnFunctions.at(actorTypeId);
        return(function());
    }



    void SimpleSimActorsSpawner::SetSpawnFunction(ActorTypeId actorTypeId, std::function<Nest::IEntity*()> function)
    {
        if (m_SpawnFunctions.count(actorTypeId) > 0)
        {
            throw std::runtime_error("Trying to register spawn function with already registered actor type id.");
        }

        if (function == nullptr)
        {
            throw std::runtime_error("Trying to register null spawn function.");
        }

        m_SpawnFunctions[actorTypeId] = function;
    }
}

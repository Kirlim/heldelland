//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/PlayerSimulationCommandsQueuesManager.h"

#include<Nest/Exceptions/NullArgumentException.h>

#include"Game/Simulation/ClientSimulationCommandsQueue.h"
#include"Game/Simulation/ClientSimulationCommand.h"



namespace Game
{
	PlayerSimulationCommandsQueuesManager::PlayerSimulationCommandsQueuesManager()
		: m_CurrentQueue(std::make_unique<ClientSimulationCommandsQueue>())
	{
	}



	PlayerSimulationCommandsQueuesManager::~PlayerSimulationCommandsQueuesManager() = default;



	void PlayerSimulationCommandsQueuesManager::CommitQueue()
	{
		if (m_CurrentQueue->Size() > 0)
		{
			m_CommandsQueues.emplace(std::move(m_CurrentQueue));
			m_CurrentQueue = std::make_unique<ClientSimulationCommandsQueue>();
		}
	}



	void PlayerSimulationCommandsQueuesManager::AppendCommand(std::shared_ptr<ClientSimulationCommand> command)
	{
		if (command == nullptr)
		{
			ThrowNullArgumentExceptionWithLine("Command cannot be null.");
		}

		m_CurrentQueue->Enqueue(command);
	}



	unsigned int PlayerSimulationCommandsQueuesManager::GetCurrentQueueSize() const
	{
		return m_CurrentQueue->Size();
	}



	void PlayerSimulationCommandsQueuesManager::ClearQueue()
	{
		m_CurrentQueue->Clear();
	}



	bool PlayerSimulationCommandsQueuesManager::HasCommittedQueues() const
	{
		return !m_CommandsQueues.empty();
	}



	unsigned int PlayerSimulationCommandsQueuesManager::CommittedQueuesCount() const
	{
		return m_CommandsQueues.size();
	}



	// TODO : divide in Two methods (peek and pop), to make it safer against exceptions.
	std::unique_ptr<ClientSimulationCommandsQueue> PlayerSimulationCommandsQueuesManager::FetchNextCommittedQueue()
	{
		if (m_CommandsQueues.empty()) return (std::unique_ptr<ClientSimulationCommandsQueue>());

		auto result = std::move(m_CommandsQueues.front());
		m_CommandsQueues.pop();

		return result;
	}
}

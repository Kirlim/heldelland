
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/IGameClientSimulation.h"



namespace Game
{
	IGameClientSimulation::~IGameClientSimulation() = default;
}

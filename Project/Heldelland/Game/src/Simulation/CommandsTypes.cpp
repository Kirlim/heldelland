//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/CommandsTypes.h"



namespace Game
{
	bool IsActorSpawningCommand(NetworkMessageTypeId networkMessageTypeId)
	{
		const auto& idValue = networkMessageTypeId.value;

		return (
			idValue == SPAWN_PLAYER_CHARACTER_COMMAND_TYPE ||
			idValue == SPAWN_ACTOR_COMMAND_TYPE
		);
	}
}

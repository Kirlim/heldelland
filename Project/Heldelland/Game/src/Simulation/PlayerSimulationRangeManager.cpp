//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/PlayerSimulationRangeManager.h"

#include<algorithm>

#include<Nest/IEntity.h>
#include<Nest/ComponentReference.h>

#include"Game/SimulationCommons/SimulationActorsMovementData.h"

#include"Game/Simulation/SimActorMovementBehavior.h"



namespace Game
{
	PlayerSimulationRangeManager::PlayerSimulationRangeManager(const SimulationActorsMovementData& simulationActorsMovementData) :
		m_SimulationActorsMovementData(simulationActorsMovementData)
	{
	}



	PlayerSimulationRangeManager::~PlayerSimulationRangeManager() = default;



	void PlayerSimulationRangeManager::SetViewRadius(simfix64 viewRadius)
	{
		m_ViewRadius = viewRadius;
	}



	void PlayerSimulationRangeManager::SetPlayerPosition(Nest::Vector3<simfix64> playerPosition)
	{
		m_PlayerPosition = playerPosition;
	}



	bool PlayerSimulationRangeManager::IsEntityRegistered(ActorNetworkId actorNetworkId) const
	{
		return std::any_of(
			std::begin(m_RegisteredActorsNetworkIds),
			std::end(m_RegisteredActorsNetworkIds),
			[actorNetworkId](ActorNetworkId id) { return (actorNetworkId == id); }
		);
	}



	std::vector<ActorNetworkId> PlayerSimulationRangeManager::RegisterAndReturnMovingEntitiesThatCameWithinRange()
	{
		std::vector<ActorNetworkId> idsToRegister{};
		const auto& actorsNetworkIds = m_SimulationActorsMovementData.actorsNetworkIds;
		const auto& inFrameMovementPositions = m_SimulationActorsMovementData.actorsInFrameMovementPositions;

		for (decltype(actorsNetworkIds.size()) i = 0; i < actorsNetworkIds.size(); ++i)
		{
			if (IsEntityRegistered(actorsNetworkIds[i]))
				continue;

			const auto& inFramePosition = inFrameMovementPositions[i];
			const auto distanceX = inFramePosition.frameEndX - m_PlayerPosition.x;
			const auto distanceZ = inFramePosition.frameEndY - m_PlayerPosition.z;
			const auto sqrDistance = (distanceX * distanceX) + (distanceZ * distanceZ);

			if (sqrDistance <= (m_ViewRadius * m_ViewRadius))
			{
				idsToRegister.emplace_back(actorsNetworkIds[i]);
			}
		}

		for (const auto& idToRegister : idsToRegister)
			RegisterMovingEntity(idToRegister);

		return idsToRegister;
	}



	std::vector<ActorNetworkId> PlayerSimulationRangeManager::UnregisterAndReturnMovingEntitiesThatWentOutOfRange()
	{
		std::vector<int> toRemoveIndexes{};
		std::vector<ActorNetworkId> toRemoveIds{};

		const auto& inFrameMovementPositions = m_SimulationActorsMovementData.actorsInFrameMovementPositions;
		m_SimulationActorsMovementData.GetNetworkActorsIndexes(m_RegisteredActorsNetworkIds, toRemoveIndexes);

		for (decltype(m_RegisteredActorsNetworkIds.size()) i = 0; i < m_RegisteredActorsNetworkIds.size(); ++i)
		{
			for (const auto& networkActorIndex : toRemoveIndexes)
			{
				NestAssert(networkActorIndex >= 0);

				const auto& inFrameMovementPosition = inFrameMovementPositions[networkActorIndex];
				const auto distanceX = inFrameMovementPosition.frameEndX - m_PlayerPosition.x;
				const auto distanceZ = inFrameMovementPosition.frameEndY - m_PlayerPosition.z;
				const auto sqrDistance = (distanceX * distanceX) + (distanceZ * distanceZ);

				if (sqrDistance > (m_ViewRadius * m_ViewRadius))
				{
					toRemoveIds.emplace_back(m_RegisteredActorsNetworkIds[networkActorIndex]);
				}
			}
		}


		m_RegisteredActorsNetworkIds.erase(
			std::remove_if(
				std::begin(m_RegisteredActorsNetworkIds),
				std::end(m_RegisteredActorsNetworkIds),
				[&toRemoveIds](const ActorNetworkId& actorNetworkId)
				{
					return std::any_of(
						std::begin(toRemoveIds), std::end(toRemoveIds), [&actorNetworkId](const ActorNetworkId& id) { return (actorNetworkId == id); }
					);
				}
			),
			std::end(m_RegisteredActorsNetworkIds)
		);

		return toRemoveIds;
	}



	void PlayerSimulationRangeManager::GetRegisteredActorsNetworkIdsByAppending(std::vector<ActorNetworkId>& out_RegisteredActorsNetworkIds) const
	{
		out_RegisteredActorsNetworkIds.insert(
			std::end(out_RegisteredActorsNetworkIds),
			std::begin(m_RegisteredActorsNetworkIds), std::end(m_RegisteredActorsNetworkIds)
		);
	}



	void PlayerSimulationRangeManager::RegisterMovingEntity(ActorNetworkId actorNetworkId)
	{
		NestAssert(!IsEntityRegistered(actorNetworkId));
		m_RegisteredActorsNetworkIds.emplace_back(actorNetworkId);
	}
}

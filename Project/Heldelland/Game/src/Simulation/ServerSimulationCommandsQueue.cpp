
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/ServerSimulationCommandsQueue.h"



namespace Game
{
    ServerSimulationCommandsQueue::ServerSimulationCommandsQueue()
    {
        // Nothing here
    }



    ServerSimulationCommandsQueue::~ServerSimulationCommandsQueue()
    {
        // Nothing here
    }



    void ServerSimulationCommandsQueue::Enqueue(std::shared_ptr<ServerSimulationCommand> serverSimulationCommand)
    {
        if (serverSimulationCommand == nullptr)
        {
            throw std::logic_error("Cannot enqueue null server command.");
        }

        std::unique_lock<std::mutex> queueAccesslock(m_QueueAccessMutex, std::defer_lock);
        queueAccesslock.lock();

        m_CommandsQueue.push(serverSimulationCommand);
    }



    std::shared_ptr<ServerSimulationCommand> ServerSimulationCommandsQueue::PeekNext() const
    {
        std::unique_lock<std::mutex> queueAccesslock(m_QueueAccessMutex, std::defer_lock);
        queueAccesslock.lock();

        if (m_CommandsQueue.size() == 0)
        {
            return(nullptr);
        }
        else
        {
            return(m_CommandsQueue.front());
        }
    }



    void ServerSimulationCommandsQueue::DequeueFront()
    {
        std::unique_lock<std::mutex> queueAccesslock(m_QueueAccessMutex, std::defer_lock);
        queueAccesslock.lock();

        if (m_CommandsQueue.size() == 0)
        {
            throw std::logic_error("Cannot dequeue command from an empty queue.");
        }

        m_CommandsQueue.pop();
    }



    bool ServerSimulationCommandsQueue::HasCommands() const
    {
        std::unique_lock<std::mutex> queueAccesslock(m_QueueAccessMutex, std::defer_lock);
        queueAccesslock.lock();

        return(m_CommandsQueue.size() > 0);
    }



    size_t ServerSimulationCommandsQueue::Size() const
    {
        std::unique_lock<std::mutex> queueAccesslock(m_QueueAccessMutex, std::defer_lock);
        queueAccesslock.lock();

        return(m_CommandsQueue.size());
    }
}
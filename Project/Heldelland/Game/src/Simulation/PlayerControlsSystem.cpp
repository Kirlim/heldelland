//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/PlayerControlsSystem.h"

#include<memory>

// TODO : remove include when on click events are implemented
#include<Nest/Global.h>
#include<Nest/Input.h>
#include<Nest/World.h>
#include<Nest/RenderingManager.h>
#include<Nest/Ray.h>

#include<Nest/Vector2.h>
#include"Game/SimulationCommons/SimulationTypes.h"

#include"Game/Map/Map.h"
#include"Game/Map/Terrain.h"

#include"Game/Simulation/GameClientSimulationSystem.h"
#include"Game/Simulation/ClientGameSimulation.h"
#include"Game/Simulation/ServerSimulationCommandsQueue.h"

#include"Game/Simulation/ServerCommands/PlayerMoveCommand.h"



namespace Game
{
	PlayerControlsSystem::PlayerControlsSystem(
		Nest::RenderingManager& renderingManager,
		GameClientSimulationSystem& gameClientSimulationSystem
	) :
		m_RenderingManager(renderingManager),
		m_GameClientSimulationSystem(gameClientSimulationSystem)
	{
		// Nothing here
	}



	void PlayerControlsSystem::DoUpdate(const float deltaTime)
	{
		const auto clientGameSimulation = m_GameClientSimulationSystem.GetClientSimulation();
		if (clientGameSimulation == nullptr)
			return;


		const auto input = Nest::Global->GetInput();
		if (input->IsMousePressed(Nest::MouseButton::Left))
		{
			const auto map = clientGameSimulation->GetMap();
			NestAssert(map != nullptr);

			const auto terrain = map->GetTerrain();

			const auto camera = m_RenderingManager.GetActiveCamera();
			if (camera != nullptr)
			{
				const auto mousePosition = Nest::Global->GetInput()->GetMousePosition();
				const Nest::Vector2<float> floatMousePosition(float(mousePosition.x), float(mousePosition.y));

				const auto ray = camera->ScreenPointToRay(floatMousePosition);
				int outTileX, outTileY;
				if (terrain->RaycastToTerrain(ray.origin, ray.direction, outTileX, outTileY))
				{
					OnMapClickHandler(float(outTileX), float(outTileY));
				}
			}
		}
	}



	void PlayerControlsSystem::OnMapClickHandler(float mapX, float mapZ) const
	{
		const auto clientGameSimulation = m_GameClientSimulationSystem.GetClientSimulation();
		auto toServerCommandsQueue = clientGameSimulation->GetToServerSimulationCommandsQueue();

		auto playerMoveCommand = std::make_shared<PlayerMoveCommand>();
		playerMoveCommand->SetDestination(Nest::Vector2<simfix64>(simfix64(mapX), simfix64(mapZ)));

		toServerCommandsQueue->Enqueue(playerMoveCommand);
	}
}

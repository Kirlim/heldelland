//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/GameClientSimulationNetworkSystem.h"

#include<Nest/Exceptions/FailedToLoadException.h>
#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/NestAssert.h>
#include<Nest/Cast.h>

#include<Nest/Log.h>

#include"Game/Network/IHost.h"
#include"Game/Network/IHostChannel.h"
#include"Game/Network/ClientNetworkController.h"
#include"Game/Network/Messages/GenericMessagesHandler.h"
#include"Game/Network/INetworkMessagesHandlersManager.h"
#include"Game/Network/IHostsManager.h"
#include"Game/Network/INetworkSystem.h"
#include"Game/Simulation/ClientSimulationCommand.h"
#include"Game/Simulation/ServerSimulationCommand.h"
#include"Game/Simulation/PlayerSimulationCommandsQueuesManager.h"
#include"Game/Simulation/ServerSimulationCommandsQueue.h"
#include"Game/Simulation/IGameClientSimulation.h"

#include"Game/Network/Messages/GameSimulationSettingsMessage.h"
#include"Game/Network/Messages/JoinGameReplyMessage.h"
#include"Game/Simulation/ClientCommands/ForceNextTickValueCommand.h"
#include"Game/Simulation/ClientCommands/SimulationTickStartCommand.h"
#include"Game/Simulation/ClientCommands/SimulationTickEndCommand.h"
#include"Game/Simulation/ClientCommands/SetActorFullPathCommand.h"
#include"Game/Simulation/ClientCommands/SetActorMovementSpeedCommand.h"
#include"Game/Simulation/ClientCommands/SetActorPositionCommand.h"
#include"Game/Simulation/ClientCommands/SpawnActorCommand.h"
#include"Game/Simulation/ClientCommands/SpawnPlayerCharacterCommand.h"

#include"Game/Network/Messages/GameSimulationLoadedMessage.h"

#include"Game/Network/INetworkMessagesFactory.h"

#include"Game/Simulation/GameClientSimulationSystem.h"



namespace Game
{
	GameClientSimulationNetworkSystem::GameClientSimulationNetworkSystem(
		INetworkSystem& networkSystem,
		GameClientSimulationSystem& gameClientSimulationSystem,
		std::function<std::unique_ptr<IGameClientSimulation>(std::string /*mapName*/)> gameClientSimulationFactoryMethod,
		const INetworkMessagesFactory& networkMessagesFactory,
		const INetworkMessagesHandlersManager& networkMessagesHandlersManager
	) :
		m_NetworkSystem(networkSystem),
		m_GameClientSimulationSystem(gameClientSimulationSystem),
		m_GameClientSimulationFactoryMethod(gameClientSimulationFactoryMethod),
		m_NetworkMessagesFactory(networkMessagesFactory),
		m_NetworkMessagesHandlersManager(networkMessagesHandlersManager)
	{
		RegisterIncomingMessagesHandlers();
	}



	GameClientSimulationNetworkSystem::~GameClientSimulationNetworkSystem() = default;



	std::unique_ptr<Nest::EventsBusToken> GameClientSimulationNetworkSystem::RegisterToNewClientSimulationStartedEvent(
		std::function<void(const NewClientSimulationStartedEvt&)> action
	)
	{
		return m_NewClientSimulationStartedEventBus.Register(action);
	}



	void GameClientSimulationNetworkSystem::RegisterIncomingMessagesHandlers()
	{
		// TODO : consider optimizing shared_ptr with std::move
		const auto joinGameReplyMessagesHandler = DoCast<GenericMessagesHandler<NetworkMessage>>(m_NetworkMessagesHandlersManager.GetHandler(JoinGameReplyMessage::TypeId));
		auto& joinGameReplyMessagesHandlerEventsBus = joinGameReplyMessagesHandler->GetEventsBus();
		m_JoinGameReplyMessagesToken = joinGameReplyMessagesHandlerEventsBus.Register(
			[&](std::shared_ptr<NetworkMessage>& message) { HandleIncomingConnectionRequestReply(DoCast<JoinGameReplyMessage>(*message)); }
		);

		const auto gameSimulationSettingsMessagesHandler = DoCast<GenericMessagesHandler<NetworkMessage>>(m_NetworkMessagesHandlersManager.GetHandler(GameSimulationSettingsMessage::TypeId));
		auto& gameSimulationSettingsMessagesHandlerEventBus = gameSimulationSettingsMessagesHandler->GetEventsBus();
		m_GameSimulationSettingsMessageToken = gameSimulationSettingsMessagesHandlerEventBus.Register(
			[&](std::shared_ptr<NetworkMessage>& message) { HandleIncomingGameSimulationSettingsMessage(DoCast<GameSimulationSettingsMessage>(*message)); }
		);

		const auto forceNextTickValueCommandsHandler = DoCast<GenericMessagesHandler<NetworkMessage>>(m_NetworkMessagesHandlersManager.GetHandler(ForceNextTickValueCommand::TypeId));
		auto& forceNextTickValueCommandsHandlerEventsBus = forceNextTickValueCommandsHandler->GetEventsBus();
		m_ForceNextTickValueCommandsToken = forceNextTickValueCommandsHandlerEventsBus.Register(
			[&](std::shared_ptr<NetworkMessage>& command) { HandleIncomingForceNextTickValueCommand(DoCast<ForceNextTickValueCommand>(*command)); }
		);

		const auto simulationTickStartCommandsHandler = DoCast<GenericMessagesHandler<NetworkMessage>>(m_NetworkMessagesHandlersManager.GetHandler(SimulationTickStartCommand::TypeId));
		auto& simulationTickStartCommandsHandlerEventsBus = simulationTickStartCommandsHandler->GetEventsBus();
		m_SimulationTickStartCommandsToken = simulationTickStartCommandsHandlerEventsBus.Register(
			[&](std::shared_ptr<NetworkMessage>& command) { HandleIncomingClientSimulationCommand(std::dynamic_pointer_cast<ClientSimulationCommand>(command)); }
		);

		const auto simulationTickEndCommandsHandler = DoCast<GenericMessagesHandler<NetworkMessage>>(m_NetworkMessagesHandlersManager.GetHandler(SimulationTickEndCommand::TypeId));
		auto& simulationTickEndCommandsHandlerEventsBus = simulationTickEndCommandsHandler->GetEventsBus();
		m_SimulationTickEndCommandsToken = simulationTickEndCommandsHandlerEventsBus.Register(
			[&](std::shared_ptr<NetworkMessage>& command) { HandleIncomingSimulationTickEndCommand(std::dynamic_pointer_cast<SimulationTickEndCommand>(command)); }
		);


		SetGenericClientSimulationCommandToken(m_SetActorFullPathCommandsToken, SetActorFullPathCommand::TypeId);
		SetGenericClientSimulationCommandToken(m_SetActorMovementSpeedCommandsToken, SetActorMovementSpeedCommand::TypeId);
		SetGenericClientSimulationCommandToken(m_SetActorPositionCommandsToken, SetActorPositionCommand::TypeId);
		SetGenericClientSimulationCommandToken(m_SpawnActorCommandsToken, SpawnActorCommand::TypeId);
		SetGenericClientSimulationCommandToken(m_SpawnPlayerCharacterCommandsToken, SpawnPlayerCharacterCommand::TypeId);
	}



	void GameClientSimulationNetworkSystem::SetGenericClientSimulationCommandToken(
		std::unique_ptr<Nest::EventsBusToken>& token,
		NetworkMessageTypeId networkMessageTypeId
	)
	{
		const auto commandsHandler = DoCast<Game::GenericMessagesHandler<Game::NetworkMessage>>(
			m_NetworkMessagesHandlersManager.GetHandler(networkMessageTypeId)
		);

		auto& commandsHandlerEventsBus = commandsHandler->GetEventsBus();

		token = commandsHandlerEventsBus.Register(
			[&](std::shared_ptr<Game::NetworkMessage>& command)
			{
				HandleIncomingClientSimulationCommand(std::dynamic_pointer_cast<ClientSimulationCommand>(command));
			}
		);
	}



	void GameClientSimulationNetworkSystem::DoFixedUpdate(float)
	{
		if (!m_NetworkSystem.IsConnectedToServer())
			return;

		TickClientNetworkController();
		AddCommandsFromClientSimulationToServerChannel();
	}



	void GameClientSimulationNetworkSystem::TickClientNetworkController()
	{
		// TODO : this division of responsibilities is weird and must be refactored...
		m_NetworkSystem.ExecuteWithServerHostsChannels(
			[this](IHostChannel& fromServer, IHostChannel& toServer)
			{
				if (m_ClientNetworkController == nullptr)
					m_ClientNetworkController = std::make_unique<ClientNetworkController>(*m_NetworkSystem.GetMessagesFactory(), *m_NetworkSystem.GetMessagesHandlersManager());

				m_ClientNetworkController->Tick(fromServer, toServer);
			}
		);
	}



	void GameClientSimulationNetworkSystem::AddCommandsFromClientSimulationToServerChannel() const
	{
		const auto gameClientSimulation = m_GameClientSimulationSystem.GetClientSimulation();
		if (gameClientSimulation == nullptr)
			return;

		auto toServerQueue = gameClientSimulation->GetToServerSimulationCommandsQueue();

		auto command = toServerQueue->PeekNext();
		while (command != nullptr)
		{
			const auto commandAsNetwork = std::dynamic_pointer_cast<NetworkMessage>(command);
			GetSendToServerChannel()->AddMessage(commandAsNetwork);

			toServerQueue->DequeueFront();
			command = toServerQueue->PeekNext();
		}
	}



	void GameClientSimulationNetworkSystem::HandleIncomingConnectionRequestReply(JoinGameReplyMessage& joinGameReplyMessage)
	{
		// TODO : handle exceptional case of already being in a game

		if (joinGameReplyMessage.GetReply() != JoinGameReplyMessage::Reply::Accepted)
			ThrowFailedToLoadExceptionWithLine("Join game refused.");


		const auto replyHost = joinGameReplyMessage.GetRemoteHost();
		NestAssert(replyHost != nullptr);

		m_ServerHostId = replyHost->GetLocadId();
		m_HasServerId = true;

		Nest::Log::Message("Join Game Reply Message Received");
	}



	void GameClientSimulationNetworkSystem::HandleIncomingGameSimulationSettingsMessage(
		const GameSimulationSettingsMessage& gameSimulationSettingsMessage
	) const
	{
		// TODO : handle exceptional case of already being in a game (using m_HasServerId flag)

		// TODO : verify and test that factory method is not null
		// TODO : error if created simulation is null
		if (m_GameClientSimulationSystem.GetClientSimulation() != nullptr)
			ThrowLogicErrorExceptionWithLine("Received Game Simulation Settings, but there already is a simulation set up.");

		StartNewGameClientSimulation(gameSimulationSettingsMessage.GetMapName());
		DispatchGameSimulationLoadedMessage(gameSimulationSettingsMessage.GetRemoteHost());

		SignalNewClientSimulationStartedEvent();

		// TODO : undo the new client simulation if dispatch or messaging fails
	}



	void GameClientSimulationNetworkSystem::StartNewGameClientSimulation(std::string mapName) const
	{
		auto newGameClientSimulation = m_GameClientSimulationFactoryMethod(mapName);
		m_GameClientSimulationSystem.StartClientSimulation(newGameClientSimulation);

		Nest::Log::Message("Started the simulation at the client.");
	}



	void GameClientSimulationNetworkSystem::SignalNewClientSimulationStartedEvent() const
	{
		const NewClientSimulationStartedEvt newClientSimulationStartedEvt{ m_GameClientSimulationSystem.GetClientSimulation() };
		m_NewClientSimulationStartedEventBus.Signal(newClientSimulationStartedEvt);
	}



	void GameClientSimulationNetworkSystem::DispatchGameSimulationLoadedMessage(const IHost* host) const
	{
		NestAssert(host != nullptr);

		try
		{
			auto message = CreateGameSimulationLoadedNetworkMessage();
			SendNetworkMessageToServerChannel(message);
		}
		catch (...)
		{
			std::throw_with_nested(std::runtime_error("Failed to dispatch game simulation loaded message to server."));
		}
	}



	std::shared_ptr<NetworkMessage> GameClientSimulationNetworkSystem::CreateGameSimulationLoadedNetworkMessage() const
	{
		auto gameSimulationLoadedMessageBase = m_NetworkMessagesFactory.InstantiateMessage(GameSimulationLoadedMessage::TypeId);
		return gameSimulationLoadedMessageBase;
	}



	void GameClientSimulationNetworkSystem::HandleIncomingForceNextTickValueCommand(ForceNextTickValueCommand& command) const
	{
		const auto gameClientSimulation = m_GameClientSimulationSystem.GetClientSimulation();
		NestAssert(gameClientSimulation != nullptr);

		auto clientQueuesManager = gameClientSimulation->GetQueuesManager();

		//// TODO : decide on a way to not use copy. Maybe pass shared_ptr references as parameter?
		auto copy = std::make_shared<ForceNextTickValueCommand>();
		copy->SetNextTickValue(command.GetNextTickValue());

		clientQueuesManager->AppendCommand(copy);
		clientQueuesManager->CommitQueue();
	}



	void GameClientSimulationNetworkSystem::HandleIncomingSimulationTickEndCommand(std::shared_ptr<SimulationTickEndCommand> command) const
	{
		HandleIncomingClientSimulationCommand(std::dynamic_pointer_cast<ClientSimulationCommand>(command));

		const auto gameClientSimulation = m_GameClientSimulationSystem.GetClientSimulation();
		auto clientQueuesManager = gameClientSimulation->GetQueuesManager();
		clientQueuesManager->CommitQueue();
	}



	void GameClientSimulationNetworkSystem::HandleIncomingClientSimulationCommand(std::shared_ptr<ClientSimulationCommand> command) const
	{
		const auto gameClientSimulation = m_GameClientSimulationSystem.GetClientSimulation();
		NestAssert(gameClientSimulation != nullptr);

		auto clientQueuesManager = gameClientSimulation->GetQueuesManager();
		clientQueuesManager->AppendCommand(std::dynamic_pointer_cast<ClientSimulationCommand>(command));
	}



	IHostChannel* GameClientSimulationNetworkSystem::GetSendToServerChannel() const
	{
		if (!m_HasServerId)
			throw std::logic_error("Cannot fetch the send to server channel: no server id has been set.");

		IHostChannel* channel;

		const auto host = m_NetworkSystem.GetHostsManager()->GetHost(m_ServerHostId).lock();
		NestAssert(host != nullptr);

		if (m_ServerHostId == IHostsManager::LocalHostId)
		{
			// Server running in same executable instance. Client has "inverted" channels.
			channel = host->GetFromHostChannel();
		}
		else
		{
			channel = host->GetToHostChannel();
		}

		NestAssert(channel != nullptr);
		return channel;
	}



	void GameClientSimulationNetworkSystem::SendNetworkMessageToServerChannel(std::shared_ptr<NetworkMessage>& message) const
	{
		GetSendToServerChannel()->AddMessage(message);
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/PlayerSimulationInformation.h"

#include<stdexcept>

#include"Game/Simulation/IPlayerSimulationRangeManager.h"



namespace Game
{
	PlayerSimulationInformation::PlayerSimulationInformation(PlayerSimInfoId id, std::unique_ptr<IPlayerSimulationRangeManager> playerSimulationRangeManager) :
		m_Id(id),
		m_PlayerSimulationRangeManager(std::move(playerSimulationRangeManager))
	{
		if (m_PlayerSimulationRangeManager == nullptr)
		{
			throw std::logic_error("Player Simulation Range Manager is null.");
		}
	}



	PlayerSimulationInformation::~PlayerSimulationInformation()
	{
		// Nothing here
	}



	PlayerSimInfoId PlayerSimulationInformation::GetId() const
	{
		return m_Id;
	}



	void PlayerSimulationInformation::SetSimulationLoadedFlag(bool loaded)
	{
		m_HasSimulationBeenLoaded = loaded;
	}



	bool PlayerSimulationInformation::HasSimulationBeenLoaded() const
	{
		return m_HasSimulationBeenLoaded;
	}



	bool PlayerSimulationInformation::IsSpawned() const
	{
		return m_IsSpawned;
	}



	bool PlayerSimulationInformation::CanBeSpawned() const
	{
		return (m_HasSimulationBeenLoaded && !m_IsSpawned);
	}



	void PlayerSimulationInformation::SetAsSpawned()
	{
		m_IsSpawned = true;
	}



	void PlayerSimulationInformation::SetActorNetworkId(ActorNetworkId actorNetworkId)
	{
		m_ActorNetworkId = actorNetworkId;
	}



	ActorNetworkId PlayerSimulationInformation::GetActorNetworkId() const
	{
		return m_ActorNetworkId;
	}



	IPlayerSimulationRangeManager* PlayerSimulationInformation::GetPlayerSimulationRangeManager() const
	{
		return m_PlayerSimulationRangeManager.get();
	}
}

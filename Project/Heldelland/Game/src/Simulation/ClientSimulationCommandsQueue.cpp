
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/ClientSimulationCommandsQueue.h"

#include<stdexcept>

#include"Game/Simulation/ClientSimulationCommand.h"



namespace Game
{
    void ClientSimulationCommandsQueue::Enqueue(std::shared_ptr<ClientSimulationCommand> command)
    {
        if (command == nullptr)
        {
            throw std::runtime_error("Trying to enqueue a null command.");
        }

        m_Queue.emplace(command);
    }



    std::shared_ptr<ClientSimulationCommand> ClientSimulationCommandsQueue::Dequeue()
    {
        if (Size() > 0)
        {
            auto command = m_Queue.front();
            m_Queue.pop();
            return(command);
        }
        else
        {
            return(nullptr);
        }
    }



    void ClientSimulationCommandsQueue::SetReadyStatus(bool readyStatus)
    {
        m_Ready = readyStatus;
    }



    bool ClientSimulationCommandsQueue::IsReady() const
    {
        return(m_Ready);
    }



    void ClientSimulationCommandsQueue::Clear()
    {
        while (!m_Queue.empty())
        {
            m_Queue.pop();
        }
    }



    unsigned int ClientSimulationCommandsQueue::Size() const
    {
        return(m_Queue.size());
    }



    bool ClientSimulationCommandsQueue::HasCommands() const
    {
        return(m_Queue.size() > 0);
    }
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/GameServerSimulationSystem.h"

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NullArgumentException.h>

#include"Game/Simulation/IGameServerSimulation.h"



namespace Game
{
	GameServerSimulationSystem::~GameServerSimulationSystem() = default;



	void GameServerSimulationSystem::StartServerSimulation(std::unique_ptr<IGameServerSimulation>& gameServerSimulation)
	{
		if (gameServerSimulation == nullptr)
			ThrowNullArgumentExceptionWithLine("Game Server Simulation cannot be null");

		if (m_GameServerSimulation != nullptr)
			ThrowLogicErrorExceptionWithLine("There is already a Game Server Simulation set up");

		m_GameServerSimulation = std::move(gameServerSimulation);
	}



	IGameServerSimulation* GameServerSimulationSystem::GetServerSimulation() const
	{
		return m_GameServerSimulation.get();
	}



	void GameServerSimulationSystem::DoFixedUpdate(float fixedDeltaTime)
	{
		if (m_GameServerSimulation == nullptr) return;

		m_AccumulatedFixedDeltaTime += fixedDeltaTime;
		simfix64 fixedPointAccumulatedFixedDeltaTime{ m_AccumulatedFixedDeltaTime };

		while (fixedPointAccumulatedFixedDeltaTime >= TickDeltaTime)
		{
			fixedPointAccumulatedFixedDeltaTime -= TickDeltaTime;
			m_GameServerSimulation->Tick(TickDeltaTime);
		}

		m_AccumulatedFixedDeltaTime = fixedPointAccumulatedFixedDeltaTime.ToFloat();
	}
}

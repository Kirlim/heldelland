//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/SimActorMovementBehavior.h"

#include"Game/Pathfinding/Path.h"



namespace Game
{
	SimActorMovementBehavior::SimActorMovementBehavior() : Component()
	{
		// Nothing here
	}



	SimActorMovementBehavior::~SimActorMovementBehavior()
	{
		// Nothing here
	}



	void SimActorMovementBehavior::ResetPosition(Nest::Vector3<simfix64> position)
	{
		m_Position = position;
		m_Path = nullptr;
	}



	void SimActorMovementBehavior::MoveInPath(std::shared_ptr<Path> path, simfix64 startingDistance)
	{
		m_Path = path;
		m_NextPathNodeIndex = 0;

		MoveToDistance(startingDistance);

		m_IsMoving = true;
	}



	void SimActorMovementBehavior::SetMovementSpeed(simfix64 movementSpeed)
	{
		m_MovementSpeed = movementSpeed;
	}



	simfix64 SimActorMovementBehavior::GetMovementSpeed() const
	{
		return (m_MovementSpeed);
	}



	Nest::Vector3<simfix64> SimActorMovementBehavior::GetPosition() const
	{
		return (m_Position);
	}



	void SimActorMovementBehavior::MoveToDistance(simfix64 distance)
	{
		if (m_Path == nullptr) return;
		auto pos = m_Path->GetPositionByDistance(distance);

		m_Position.x = pos.x;
		m_Position.z = pos.y;

		if (distance >= m_Path->GetTotalDistance())
		{
			m_IsMoving = false;
			distance = m_Path->GetTotalDistance();
		}

		m_CurrentDistance = distance;
	}



	void SimActorMovementBehavior::Update(simfix64 deltaTime)
	{
		m_CurrentDistance += m_MovementSpeed * deltaTime;
		MoveToDistance(m_CurrentDistance);
	}



	void SimActorMovementBehavior::SetActorId(ActorId actorId)
	{
		m_ActorId = actorId;
	}



	ActorId SimActorMovementBehavior::GetActorId() const
	{
		return m_ActorId;
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/ServerGameSimulation.h"

#include<Nest/NestAssert.h>
#include<Nest/Exceptions/LogicErrorException.h>

#include<Nest/ITemplatedSingleComponentsContainer.h>
#include<Nest/Events/EventsBusToken.h>

#include<Nest/IEntity.h>
#include<Nest/Log.h>

#include"Game/SimulationCommons/ActorsManager.h"
#include"Game/SimulationCommons/ActorsTypes.h"
#include"Game/Simulation/ISimActorsSpawner.h"
#include"Game/Simulation/ServerSimulationCommandsQueue.h"
#include"Game/Simulation/ServerSimulationCommand.h"
#include"Game/Map/Map.h"
#include"Game/Pathfinding/AStar.h"
#include"Game/Pathfinding/Path.h"

#include"Game/Simulation/PlayersSimulationsManager.h"
#include"Game/Simulation/PlayerSimulationRangeManager.h"
#include"Game/Simulation/PlayerSimulationInformation.h"
#include"Game/Simulation/PlayerSimulationCommandsQueuesManager.h"

#include"Game/Simulation/ClientCommands/SetActorMovementSpeedCommand.h"
#include"Game/Simulation/ClientCommands/SetActorFullPathCommand.h"
#include"Game/Simulation/ClientCommands/SetActorPositionCommand.h"
#include"Game/Simulation/ClientCommands/SimulationTickStartCommand.h"
#include"Game/Simulation/ClientCommands/SimulationTickEndCommand.h"
#include"Game/Simulation/ClientCommands/ForceNextTickValueCommand.h"
#include"Game/Simulation/ClientCommands/SpawnActorCommand.h"
#include"Game/Simulation/ClientCommands/SpawnPlayerCharacterCommand.h"

#include"Game/SimulationCommons/ActorBehavior.h"
#include"Game/SimulationCommons/SimulationActorMovementSystem.h"
#include"Game/SimulationCommons/SimulationActorsMovementData.h"
#include"Game/Simulation/SimActorMovementBehavior.h"

#include"Game/Simulation/GameServerTickEventsContainer.h"



namespace Game
{
	ServerGameSimulation::ServerGameSimulation(
		std::unique_ptr<ActorsManager> actorsManager,
		std::unique_ptr<ISimActorsSpawner> simulationActorsSpawner,
		std::unique_ptr<Map> map,
		std::unique_ptr<AStar> pathfinding,
		Nest::ITemplatedSingleComponentsContainer<SimActorMovementBehavior>& simActorMovementBehaviorsContainer,
		std::unique_ptr<SimulationActorMovementSystem> simulationActorMovementSystem
	) :
		m_SimActorMovementBehaviorsContainer(simActorMovementBehaviorsContainer),
		m_ServerSimulationCommandsQueue(std::make_unique<ServerSimulationCommandsQueue>()),
		m_PlayersSimulationsManager(std::make_unique<PlayersSimulationsManager>()),
		m_BuildingPlayerMovementRequestsList(&m_FirstPlayerMovementRequestsList),
		m_GameServerTickEventsContainer(std::make_unique<GameServerTickEventsContainer>())
	{
		// TODO : accepting null is not a good solution. Create interfaces in order to work with stubs and mocks.
		if (actorsManager != nullptr) m_ActorsManager = std::move(actorsManager);
		if (simulationActorsSpawner != nullptr) m_SimulationActorsSpawner = std::move(simulationActorsSpawner);
		if (map != nullptr) m_Map = std::move(map);
		if (pathfinding != nullptr) m_Pathfinding = std::move(pathfinding);
		if (simulationActorMovementSystem != nullptr) m_SimulationActorMovementSystem = std::move(simulationActorMovementSystem);
	}



	ServerGameSimulation::~ServerGameSimulation() = default;



	ActorsManager* ServerGameSimulation::GetActorsManager() const
	{
		return m_ActorsManager.get();
	}



	Map* ServerGameSimulation::GetMap() const
	{
		return m_Map.get();
	}



	AStar* ServerGameSimulation::GetPathfinder() const
	{
		return m_Pathfinding.get();
	}



	PlayersSimulationsManager* ServerGameSimulation::GetPlayersSimulationsManager() const
	{
		return m_PlayersSimulationsManager.get();
	}



	SimulationActorMovementSystem* ServerGameSimulation::GetSimulationActorMovementSystem() const
	{
		return m_SimulationActorMovementSystem.get();
	}



	void ServerGameSimulation::RegisterCommand(std::shared_ptr<ServerSimulationCommand>& command)
	{
		m_ServerSimulationCommandsQueue->Enqueue(command);
	}



	bool ServerGameSimulation::RegisterPlayer(std::shared_ptr<PlayerSimulationInformation> playerInformation)
	{
		auto success = false;
		const auto playerId = playerInformation->GetId();

		if (!IsPlayerRegistered(playerId))
		{
			m_Players[playerId] = playerInformation;
			m_PlayersToReplyJoinGameRequests.emplace_back(playerInformation);
			success = true;
		}

		return success;
	}



	bool ServerGameSimulation::IsPlayerRegistered(PlayerSimInfoId playerId) const
	{
		return (m_Players.count(playerId) > 0);
	}



	void ServerGameSimulation::UnregisterPlayer(PlayerSimInfoId playerId)
	{
		if (IsPlayerRegistered(playerId))
		{
			m_Players.erase(playerId);
		}
	}



	std::weak_ptr<PlayerSimulationInformation> ServerGameSimulation::GetPlayerInformation(const PlayerSimInfoId playerId) const
	{
		if (IsPlayerRegistered(playerId))
		{
			return (m_Players.at(playerId));
		}
		else
		{
			return std::weak_ptr<PlayerSimulationInformation>();
		}
	}



	std::vector<PlayerSimInfoId> ServerGameSimulation::GetListRegisteredPlayersIds() const
	{
		std::vector<PlayerSimInfoId> result;

		for (auto& registeredPlayer : m_Players)
		{
			result.emplace_back(registeredPlayer.first);
		}

		return result;
	}



	void ServerGameSimulation::PlayerSimulationLoaded(PlayerSimInfoId playerId)
	{
		std::unique_lock<std::mutex> playerSimulationLoadedLock(m_PlayersSimulationsLoadedToProcessMutex, std::defer_lock);
		playerSimulationLoadedLock.lock();

		m_PlayersSimulationsLoadedToProcess.push_back(playerId);
	}



	bool ServerGameSimulation::HasJoinGameRepliesToDispatch() const
	{
		return (!m_PlayersToReplyJoinGameRequests.empty());
	}



	void ServerGameSimulation::DispatchJoinGameReplies(std::function<void(std::weak_ptr<PlayerSimulationInformation>)> dispatchFunction)
	{
		if (dispatchFunction == nullptr) return;

		for (auto& playerInfo : m_PlayersToReplyJoinGameRequests)
		{
			dispatchFunction(playerInfo);
		}

		m_PlayersToReplyJoinGameRequests.clear();
	}



	ActorId ServerGameSimulation::SpawnActor(ActorTypeId typeId, Nest::Vector2<simfix64> position)
	{
		// TODO : position parameter is not used. Validate if it can be removed
		if (m_SimulationActorsSpawner == nullptr)
			ThrowLogicErrorExceptionWithLine("Simulation Actors Spawner was null during spawn actor call, as server.");


		auto actorEntity = m_SimulationActorsSpawner->Spawn(typeId);
		const auto actorBehavior = actorEntity->GetComponent<ActorBehavior>();
		if (actorBehavior == nullptr)
			ThrowLogicErrorExceptionWithLine("Just spawned an actor in the server simulation, but actor is null.");


		return actorBehavior->GetActorId();
	}



	void ServerGameSimulation::RegisterPlayerMovementRequest(PlayerSimInfoId playerId, Nest::Vector2<simfix64> destination)
	{
		std::unique_lock<std::mutex> requestListLock(m_SwapPlayerMovementRequestsListsMutex, std::defer_lock);
		requestListLock.lock();

		PlayerMovementRequest movementRequest;
		movementRequest.playerId = playerId;
		movementRequest.destination = destination;

		m_BuildingPlayerMovementRequestsList->push(movementRequest);
	}



	void ServerGameSimulation::DoTick(simfix64 tickDeltaTime)
	{
		// Tick simulation and build order:
		// - tick start
		// - Players Actors Spawning
		// - Attacks orders handling
		// - Attacks
		// - Damage apply
		// - AI
		// - Movements orders handling
		// - Movement up to next tick
		// - tick end


		// ===== Run next Tick ===== //

		// ===== Tick Start ===== //

		m_GameServerTickEventsContainer->ClearAllRegisteredEvents();

		ProcessPlayersSimulationsLoaded();
		ExecuteReceivedServerCommands();

		m_SimulationTickValue.Increment();

		auto tickStartCommand = std::make_shared<SimulationTickStartCommand>();
		tickStartCommand->SetTickValue(m_SimulationTickValue.GetCurrent());
		AppendCommandToAllPlayers(tickStartCommand);

		// ===== Actors Spawning ===== //

		ExecuteSpawnPlayersActors();

		// ===== Movement Orders Handling ===== //

		ExecutePlayerMovementRequests();
		HandleActorsMovementEventsForSimulation();
		m_SimulationActorMovementSystem->UpdateActorsMovementData(tickDeltaTime);


		// ===== Data to be Sent to Players ===== //

		ExecutePlayerSimulationRangeManagersUpdate();
		ExecuteSendMovementUpdatesToPlayers();

		// ===== Tick End===== //

		auto tickEndCommand = std::make_shared<SimulationTickEndCommand>();
		tickEndCommand->SetTickValue(m_SimulationTickValue.GetCurrent());
		AppendCommandToAllPlayers(tickEndCommand);

		CommitAllPlayersQueues();
	}



	void ServerGameSimulation::ExecuteReceivedServerCommands()
	{
		auto command = m_ServerSimulationCommandsQueue->PeekNext();
		while (command != nullptr)
		{
			// TODO : timeout, to avoid starvation
			command->Execute(*this);

			m_ServerSimulationCommandsQueue->DequeueFront();
			command = m_ServerSimulationCommandsQueue->PeekNext();
		}
	}



	void ServerGameSimulation::ExecutePlayerMovementRequests()
	{
		auto requestsList = m_BuildingPlayerMovementRequestsList;
		SwapPlayerMovementRequestsList();

		while (!requestsList->empty())
		{
			const auto request = requestsList->top();

			const auto playerInformation = GetPlayerInformation(request.playerId).lock();
			NestAssert(playerInformation != nullptr);

			const auto playerActorNetworkId = playerInformation->GetActorNetworkId();
			NestAssert(playerActorNetworkId != InvalidActorNetworkId);

			const auto playerActorBehavior = m_ActorsManager->GetActorWithNetworkId(playerActorNetworkId);
			NestAssert(playerActorBehavior != nullptr);

			auto playerActorEntity = playerActorBehavior->GetOwner();
			const auto playerActorMovementBehavior = playerActorEntity->GetComponent<SimActorMovementBehavior>();
			NestAssert(playerActorMovementBehavior != nullptr);

			const auto path = std::make_shared<Path>();

			const auto& actorNextPosition = m_SimulationActorMovementSystem->GetActorNextPosition(playerActorNetworkId);
			const auto endX{ request.destination.x };
			const auto endZ{ request.destination.y };

			if (m_Pathfinding->FindPath(actorNextPosition.x, actorNextPosition.y, endX, endZ, /*out*/ *path))
			{
				m_GameServerTickEventsContainer->RegisterNewActorPathEvent(playerActorNetworkId, path);
				m_GameServerTickEventsContainer->RegisterNewActorMovementSpeedEvent(playerActorNetworkId, playerActorMovementBehavior->GetMovementSpeed());
			}

			requestsList->pop();
		}
	}



	void ServerGameSimulation::SwapPlayerMovementRequestsList()
	{
		std::unique_lock<std::mutex> requestListLock(m_SwapPlayerMovementRequestsListsMutex, std::defer_lock);
		requestListLock.lock();

		m_BuildingPlayerMovementRequestsList =
			(m_BuildingPlayerMovementRequestsList == &m_FirstPlayerMovementRequestsList) ? &m_SecondPlayerMovementRequestsList : &m_FirstPlayerMovementRequestsList;
	}



	void HandleNewActorPathEvents(GameServerTickEventsContainer& container, SimulationActorMovementSystem& actorsMovementSystem);
	void HandleNewActorMovementSpeedEvents(GameServerTickEventsContainer& container, SimulationActorMovementSystem& actorsMovementSystem);

	void ServerGameSimulation::HandleActorsMovementEventsForSimulation() const
	{
		HandleNewActorPathEvents(*m_GameServerTickEventsContainer, *m_SimulationActorMovementSystem);
		HandleNewActorMovementSpeedEvents(*m_GameServerTickEventsContainer, *m_SimulationActorMovementSystem);
	}



	void HandleNewActorPathEvents(GameServerTickEventsContainer& container, SimulationActorMovementSystem& actorsMovementSystem)
	{
		container.RunForNewActorPathEvents(
			[&actorsMovementSystem](auto beginIt, auto endIt)
			{
				while (beginIt != endIt)
				{
					actorsMovementSystem.SetActorPath(
						std::get<ActorNetworkId>(*beginIt),
						std::get<std::shared_ptr<Path>>(*beginIt)
					);
					++beginIt;
				}
			}
		);
	}



	void HandleNewActorMovementSpeedEvents(GameServerTickEventsContainer& container, SimulationActorMovementSystem& actorsMovementSystem)
	{
		container.RunForNewActorMovementSpeedEvents(
			[&actorsMovementSystem](auto beginIt, auto endIt)
			{
				while (beginIt != endIt)
				{
					actorsMovementSystem.SetActorMovementSpeed(
						std::get<ActorNetworkId>(*beginIt),
						std::get<simfix64>(*beginIt)
					);
					++beginIt;
				}
			}
		);
	}



	void ServerGameSimulation::AppendCommandToAllPlayers(const std::shared_ptr<ClientSimulationCommand>& simulationCommand)
	{
		m_PlayersSimulationsManager->ForEach(
			[&](PlayerSimulationCommandsQueuesManager* playerCommandsQueueManager) { AppendCommandToPlayer(playerCommandsQueueManager, simulationCommand); }
		);
	}



	void ServerGameSimulation::AppendCommandToPlayer(PlayerSimulationCommandsQueuesManager* playerCommandsQueueManager, const std::shared_ptr<ClientSimulationCommand>& simulationCommand)
	{
		NestAssert(playerCommandsQueueManager != nullptr);
		NestAssert(simulationCommand != nullptr);

		playerCommandsQueueManager->AppendCommand(simulationCommand);
	}



	void ServerGameSimulation::CommitAllPlayersQueues()
	{
		m_PlayersSimulationsManager->ForEach(
			[&](PlayerSimulationCommandsQueuesManager* playerCommandsQueueManager) { playerCommandsQueueManager->CommitQueue(); }
		);
	}



	void ServerGameSimulation::ProcessPlayersSimulationsLoaded()
	{
		std::unique_lock<std::mutex> playerSimulationLoadedLock(m_PlayersSimulationsLoadedToProcessMutex, std::defer_lock);

		if (playerSimulationLoadedLock.try_lock())
		{
			for (auto playerId : m_PlayersSimulationsLoadedToProcess)
			{
				m_PlayersSimulationsManager->Register(playerId);

				auto forceNextTickValueCommand = std::make_shared<ForceNextTickValueCommand>();
				forceNextTickValueCommand->SetNextTickValue(m_SimulationTickValue.GetNext());

				auto queuesManager = m_PlayersSimulationsManager->GetPlayerCommandsQueueManager(playerId);
				queuesManager->AppendCommand(forceNextTickValueCommand);

				queuesManager->CommitQueue();

				if (m_Players.count(playerId) == 0)
				{
					throw std::out_of_range("Could not find simulation information for player id " + std::to_string(playerId.value) + ".");
				}
				auto playerSimulationInformation = m_Players.at(playerId);
				playerSimulationInformation->SetSimulationLoadedFlag(true);
			}

			m_PlayersSimulationsLoadedToProcess.clear();
		}
	}



	void ServerGameSimulation::ExecuteSpawnPlayersActors()
	{
		ForEachPlayerActorThatNeedsToBeSpawned([&](PlayerSimInfoId id) { SpawnPlayerActor(id); });
	}



	void ServerGameSimulation::ForEachPlayerActorThatNeedsToBeSpawned(std::function<void(PlayerSimInfoId)> action)
	{
		NestAssert(action != nullptr);

		for (auto& playerKV : m_Players)
		{
			const auto playerSimulationInformation = playerKV.second.get();
			if (playerSimulationInformation->CanBeSpawned())
			{
				action(playerSimulationInformation->GetId());
			}
		}
	}



	void ServerGameSimulation::SpawnPlayerActor(PlayerSimInfoId id)
	{
		const Nest::Vector2<simfix64> spawnPosition(simfix64(0.0f), simfix64(0.0f));

		const auto playerActorId = SpawnActor(ActorTypeId{ ACTOR_TYPE_PLAYER }, spawnPosition);

		const auto playerActorBehavior = m_ActorsManager->GetActor(playerActorId);
		NestAssert(playerActorBehavior != nullptr);
		const auto playerActorNetworkId = playerActorBehavior->GetNetworkId();


		auto simulationInformation = m_Players.at(id);
		simulationInformation->SetAsSpawned();
		simulationInformation->SetActorNetworkId(playerActorNetworkId);
	}



	void ServerGameSimulation::ExecutePlayerSimulationRangeManagersUpdate()
	{
		for (auto& playerSimulationInfoKeyValuePair : m_Players)
		{
			const auto playerSimulationInfo = playerSimulationInfoKeyValuePair.second;
			if (!playerSimulationInfo->HasSimulationBeenLoaded()) continue;

			auto rangeManager = playerSimulationInfoKeyValuePair.second->GetPlayerSimulationRangeManager();
			const auto registeredActorsNetworkIds = rangeManager->RegisterAndReturnMovingEntitiesThatCameWithinRange();

			for (const auto& actorNetworkId : registeredActorsNetworkIds)
			{
				SendSpawnActorMessageOnPlayerRangeEnter(playerSimulationInfoKeyValuePair.first, actorNetworkId);
			}

			rangeManager->UnregisterAndReturnMovingEntitiesThatWentOutOfRange();
		}
	}



	void ServerGameSimulation::SendSpawnActorMessageOnPlayerRangeEnter(PlayerSimInfoId playerId, ActorNetworkId actorNetworkId)
	{
		const auto playerSimulation = m_Players[playerId];

		const auto queuesManager = m_PlayersSimulationsManager->GetPlayerCommandsQueueManager(playerId);
		NestAssert(queuesManager != nullptr);

		const auto spawnPosition = m_SimulationActorMovementSystem->GetActorNextPosition(actorNetworkId);

		if (playerSimulation->GetActorNetworkId() == actorNetworkId)
		{
			auto spawnCommand = std::make_shared<SpawnPlayerCharacterCommand>();
			spawnCommand->SetActorTypeId(ActorTypeId{ ACTOR_TYPE_PLAYER });
			spawnCommand->SetSpawnPosition(Nest::Vector2<simfix64>(spawnPosition.x, spawnPosition.y));
			spawnCommand->SetNetworkId(actorNetworkId);

			AppendCommandToPlayer(queuesManager, spawnCommand);
		}
		else
		{
			auto spawnCommand = std::make_shared<SpawnActorCommand>();

			spawnCommand->SetActorTypeId(ActorTypeId{ ACTOR_TYPE_PLAYER });
			spawnCommand->SetSpawnPosition(Nest::Vector2<simfix64>(spawnPosition.x, spawnPosition.y));
			spawnCommand->SetNetworkId(actorNetworkId);

			AppendCommandToPlayer(queuesManager, spawnCommand);
		}
	}



	std::vector<std::shared_ptr<SetActorMovementSpeedCommand>> BuildSetActorMovementSpeedCommandsList(
		GameServerTickEventsContainer& gameServerTickEventsContainer,
		const std::vector<ActorNetworkId>& actorsRegisteredInRangeManager
	);

	std::vector<std::shared_ptr<SetActorFullPathCommand>> BuildSetActorFullPathCommandsList(
		GameServerTickEventsContainer& gameServerTickEventsContainer,
		const std::vector<ActorNetworkId>& actorsRegisteredInRangeManager
	);

	void ServerGameSimulation::ExecuteSendMovementUpdatesToPlayers()
	{
		for (auto& playerSimulationInfoKeyValuePair : m_Players)
		{
			const auto playerId = playerSimulationInfoKeyValuePair.first;
			if (! m_PlayersSimulationsManager->IsRegistered(playerId))
				continue;

			const auto rangeManager = playerSimulationInfoKeyValuePair.second->GetPlayerSimulationRangeManager();
			NestAssert(rangeManager != nullptr);


			std::vector<ActorNetworkId> actorsRegisteredInRangeManager{};
			rangeManager->GetRegisteredActorsNetworkIdsByAppending(/*out*/ actorsRegisteredInRangeManager);

			const auto queuesManager = m_PlayersSimulationsManager->GetPlayerCommandsQueueManager(playerId);
			NestAssert(queuesManager != nullptr);



			auto setActorMovementSpeedCommands = BuildSetActorMovementSpeedCommandsList(
				*m_GameServerTickEventsContainer, actorsRegisteredInRangeManager
			);

			for (const auto& setPositionCommand : setActorMovementSpeedCommands)
				AppendCommandToPlayer(queuesManager, setPositionCommand);



			auto setActorFullPathCommands = BuildSetActorFullPathCommandsList(
				*m_GameServerTickEventsContainer, actorsRegisteredInRangeManager
			);

			for (const auto& setActorFullPathCommand : setActorFullPathCommands)
				AppendCommandToPlayer(queuesManager, setActorFullPathCommand);
		}

		// TODO : code for SetActorPositionCommand, or remove the command from the project
	}



	std::vector<std::shared_ptr<SetActorMovementSpeedCommand>> BuildSetActorMovementSpeedCommandsList(
		GameServerTickEventsContainer& gameServerTickEventsContainer,
		const std::vector<ActorNetworkId>& actorsRegisteredInRangeManager
	)
	{
		std::vector<std::shared_ptr<SetActorMovementSpeedCommand>> setActorMovementSpeedCommands{};

		gameServerTickEventsContainer.RunForNewActorMovementSpeedEvents(
			[&](auto beginIt, auto endIt)
			{
				while (beginIt != endIt)
				{
					const auto& actorNetworkId = std::get<ActorNetworkId>(*beginIt);
					const auto findResult = std::find_if(
						std::begin(actorsRegisteredInRangeManager), std::end(actorsRegisteredInRangeManager),
						[&](const ActorNetworkId& networkId) { return (networkId == actorNetworkId); }
					);

					if (findResult != std::end(actorsRegisteredInRangeManager))
					{
						setActorMovementSpeedCommands.emplace_back(std::make_shared<SetActorMovementSpeedCommand>());
						auto& setActorMovementSpeedCommand = **(setActorMovementSpeedCommands.end() - 1);

						setActorMovementSpeedCommand.SetActorNetworkId(actorNetworkId);
						setActorMovementSpeedCommand.SetMovementSpeed(std::get<simfix64>(*beginIt));
					}

					++beginIt;
				}
			}
		);

		return setActorMovementSpeedCommands;
	}



	std::vector<std::shared_ptr<SetActorFullPathCommand>> BuildSetActorFullPathCommandsList(
		GameServerTickEventsContainer& gameServerTickEventsContainer,
		const std::vector<ActorNetworkId>& actorsRegisteredInRangeManager
	)
	{
		std::vector<std::shared_ptr<SetActorFullPathCommand>> setActorFullPathCommands{};

		gameServerTickEventsContainer.RunForNewActorPathEvents(
			[&](auto beginIt, auto endIt)
			{
				while (beginIt != endIt)
				{
					const auto& actorNetworkId = std::get<ActorNetworkId>(*beginIt);
					const auto findResult = std::find_if(
						std::begin(actorsRegisteredInRangeManager), std::end(actorsRegisteredInRangeManager),
						[&](const ActorNetworkId& networkId) { return (networkId == actorNetworkId); }
					);

					if (findResult != std::end(actorsRegisteredInRangeManager))
					{
						setActorFullPathCommands.emplace_back(std::make_shared<SetActorFullPathCommand>());
						auto& setActorFullPathCommand = **(setActorFullPathCommands.end() - 1);

						setActorFullPathCommand.SetActorNetworkId(actorNetworkId);
						setActorFullPathCommand.SetPath(*std::get<std::shared_ptr<Path>>(*beginIt));
					}

					++beginIt;
				}
			}
		);

		return setActorFullPathCommands;
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/ClientGameSimulation.h"

#include<Nest/Exceptions/NullArgumentException.h>
#include<Nest/Log.h>
#include<Nest/NestAssert.h>
#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/IComponentsContainers.h>
#include<Nest/World.h>
// TODO : delete Nest includes when spawner builds full object
#include<Nest/IEntity.h>
#include<Nest/Transform.h>

#include"Game/Simulation/CommandsTypes.h"
#include"Game/SimulationCommons/SimulationTypes.h"
#include"Game/Simulation/ISimActorsSpawner.h"
#include"Game/SimulationCommons/ActorBehavior.h"
#include"Game/SimulationCommons/ActorsManager.h"
#include"Game/SimulationCommons/ActorsTypes.h"
#include"Game/SimulationCommons/SimulationActorMovementSystem.h"
#include"Game/Map/Map.h"
#include"Game/Pathfinding/AStar.h"

#include"Game/Simulation/ServerSimulationCommandsQueue.h"
#include"Game/Simulation/PlayerSimulationCommandsQueuesManager.h"
#include"Game/Simulation/ClientSimulationCommandsQueue.h"
#include"Game/Simulation/ClientSimulationCommand.h"
#include"Game/Network/NetworkMessage.h"

#include"Game/Simulation/ClientCommands/SimulationTickStartCommand.h"
#include"Game/Simulation/ClientCommands/SimulationTickEndCommand.h"
#include"Game/Simulation/ClientCommands/ForceNextTickValueCommand.h"



namespace Game
{
	ClientGameSimulation::ClientGameSimulation(
		const Nest::World& simulationWorld,
		std::unique_ptr<ActorsManager> actorsManager,
		std::unique_ptr<ISimActorsSpawner> simActorsSpawner,
		std::unique_ptr<Map> map,
		std::unique_ptr<AStar> pathfinding,
		std::unique_ptr<SimulationActorMovementSystem> simulationActorMovementSystem,
		simfix64 tickDeltaTime,
		const float maxDelayBeforeSpeedUp,
		const float timeSpeedUpFactor
	) :
		m_SimulationWorld(simulationWorld),
		m_TickDeltaTime(tickDeltaTime),
		m_MaxDelayBeforeSpeedUp(maxDelayBeforeSpeedUp),
		m_TimeSpeedUpFactor(timeSpeedUpFactor),
		m_PlayerSimulationCommandsQueues(std::make_unique<PlayerSimulationCommandsQueuesManager>()),
		m_ServerSimulationCommandsQueue(std::make_unique<ServerSimulationCommandsQueue>())
	{
		// TODO : unique_ptr received by reference, only move them if all of them are valid
		if (actorsManager != nullptr) m_ActorsManager = std::move(actorsManager);
		if (simActorsSpawner != nullptr) m_SimActorsSpawner = std::move(simActorsSpawner);
		if (map != nullptr) m_Map = std::move(map);
		if (pathfinding != nullptr) m_Pathfinding = std::move(pathfinding);
		if (simulationActorMovementSystem != nullptr) m_SimulationActorMovementSystem = std::move(simulationActorMovementSystem);
	}



	ClientGameSimulation::~ClientGameSimulation() = default;



	ActorsManager* ClientGameSimulation::GetActorsManager() const
	{
		return m_ActorsManager.get();
	}



	Map* ClientGameSimulation::GetMap() const
	{
		return m_Map.get();
	}



	AStar* ClientGameSimulation::GetPathfinder() const
	{
		return m_Pathfinding.get();
	}



	SimulationActorMovementSystem* ClientGameSimulation::GetSimulationActorMovementSystem() const
	{
		return m_SimulationActorMovementSystem.get();
	}



	PlayerSimulationCommandsQueuesManager* ClientGameSimulation::GetQueuesManager() const
	{
		return m_PlayerSimulationCommandsQueues.get();
	}



	ServerSimulationCommandsQueue* ClientGameSimulation::GetToServerSimulationCommandsQueue() const
	{
		return m_ServerSimulationCommandsQueue.get();
	}



	simfix64 ClientGameSimulation::GetTickDeltaTime() const
	{
		return m_TickDeltaTime;
	}



	const Nest::World& ClientGameSimulation::GetWorld() const
	{
		return m_SimulationWorld;
	}



	void ClientGameSimulation::ForceNextTickValue(unsigned int nextTickValue)
	{
		m_SimulationTickValue.SetCurrent(nextTickValue);
		m_SimulationTickValue.SetCurrent(m_SimulationTickValue.GetPrevious());
	}



	unsigned int ClientGameSimulation::GetCurrentTickValue() const
	{
		return m_SimulationTickValue.GetCurrent();
	}



	float ClientGameSimulation::GetCurrentTickElapsedTimePercentage() const
	{
		return m_SimulationTickElapsedPct;
	}



	void ClientGameSimulation::Update(float deltaTime)
	{
		const simfix64 situationalDeltaTime{
			(GetSimulationTicksTimeOnHold() < m_MaxDelayBeforeSpeedUp) ? deltaTime : deltaTime * m_TimeSpeedUpFactor
		};

		if (m_AccumulatedTime < m_TickDeltaTime)
		{
			m_AccumulatedTime += situationalDeltaTime;

			if (m_AccumulatedTime >= m_TickDeltaTime && !m_PlayerSimulationCommandsQueues->HasCommittedQueues())
			{
				m_AccumulatedTime = m_TickDeltaTime;
			}
		}

		if (m_AccumulatedTime >= m_TickDeltaTime)
		{
			if (m_PlayerSimulationCommandsQueues->HasCommittedQueues())
			{
				ExecuteSimulationTick();
				m_AccumulatedTime -= m_TickDeltaTime;
			}
		}

		m_SimulationTickElapsedPct = m_AccumulatedTime.ToFloat() / m_TickDeltaTime.ToFloat();
		m_SimulationTickElapsedPct = std::min(m_SimulationTickElapsedPct, 1.0f);
	}



	float ClientGameSimulation::GetSimulationTicksTimeOnHold() const
	{
		return (float(m_PlayerSimulationCommandsQueues->GetCurrentQueueSize()) * m_TickDeltaTime.ToFloat());
	}



	void ClientGameSimulation::SetPlayerActorNetworkId(ActorNetworkId actorNetworkId) noexcept
	{
		m_PlayerActorNetworkId = actorNetworkId;
	}



	ActorNetworkId ClientGameSimulation::GetPlayerActorNetworkId() const noexcept
	{
		return m_PlayerActorNetworkId;
	}



	ActorId ClientGameSimulation::SpawnActor(ActorTypeId typeId, Nest::Vector2<simfix64> position, bool hasNetworkId, ActorNetworkId networkId) const
	{
		NestAssert(m_SimActorsSpawner != nullptr);

		auto actorEntity = m_SimActorsSpawner->Spawn(typeId);

		const auto actorBehavior = actorEntity->GetComponent<ActorBehavior>();
		NestAssert(actorBehavior != nullptr);

		if (hasNetworkId)
			actorBehavior->SetNetworkId(networkId);

		const auto actorId = actorBehavior->GetActorId();

		// TODO : put event signaling on new method
		try
		{
			const ActorSpawnedEvt actorSpawnedEvent{ actorId, networkId, typeId };
			m_OnActorSpawnedEventsBus.Signal(actorSpawnedEvent);
		}
		catch (const std::exception& e)
		{
			Nest::Log::Error("[Game] Exception while signaling Actor Spawned Event:");
			Nest::Log::Error(" => " + std::string(e.what()));
		}

		return actorId;
	}



	std::unique_ptr<Nest::EventsBusToken> ClientGameSimulation::RegisterToActorSpawnedEvent(std::function<void(const ActorSpawnedEvt&)> listener)
	{
		if (listener == nullptr)
			ThrowNullArgumentExceptionWithLine("Listener cannot be null.");

		return m_OnActorSpawnedEventsBus.Register(listener);
	}



	void ExecuteQueueCoreCommands(
		ClientSimulationCommandsQueue& queue,
		ClientGameSimulation& clientGameSimulation
	);

	void ClientGameSimulation::ExecuteSimulationTick()
	{
		if (!m_PlayerSimulationCommandsQueues->HasCommittedQueues()) return;

		unsigned int tickStartValue;

		auto queue = m_PlayerSimulationCommandsQueues->FetchNextCommittedQueue();
		NestAssert(queue != nullptr);
		NestAssert(queue->Size() > 0);

		auto lastReadCommand = queue->Dequeue();
		auto asNetworkMessage = dynamic_cast<NetworkMessage*>(lastReadCommand.get());
		NestAssert(asNetworkMessage != nullptr);


		if (asNetworkMessage->GetTypeId() == ForceNextTickValueCommand::TypeId)
		{
			lastReadCommand->Execute(*this);
			if (queue->Size() != 0)
				ThrowLogicErrorExceptionWithLine("Received force tick command, but it was not the only command in the queue.");

			return;
		}

		if (asNetworkMessage->GetTypeId() == SimulationTickStartCommand::TypeId)
		{
			lastReadCommand->Execute(*this);

			const auto asTickStartCommand = dynamic_cast<SimulationTickStartCommand*>(asNetworkMessage);
			tickStartValue = asTickStartCommand->GetTickValue();

			if (tickStartValue != m_SimulationTickValue.GetNext())
			{
				ThrowLogicErrorExceptionWithLine(
					"Tick start command value [" + std::to_string(tickStartValue) +
					"] is different from the expected [" + std::to_string(m_SimulationTickValue.GetNext()) +
					"]."
				);
			}
		}
		else
		{
			ThrowLogicErrorExceptionWithLine("Commands queue did not start with a valid queue start command.");
		}


		try
		{
			ExecuteQueueCoreCommands(*queue, *this);
		}
		catch (...)
		{
			std::throw_with_nested(std::runtime_error("An error has occurred while executing enqueued core commands"));
		}


		NestAssert(queue->Size() == 1);
		lastReadCommand = queue->Dequeue();
		asNetworkMessage = dynamic_cast<NetworkMessage*>(lastReadCommand.get());

		if (asNetworkMessage == nullptr || asNetworkMessage->GetTypeId() != SimulationTickEndCommand::TypeId)
			ThrowLogicErrorExceptionWithLine("The commands queue that has been executed started with a tick start, but did not end with a tick end command.");


		const auto asTickEndCommand = dynamic_cast<SimulationTickEndCommand*>(asNetworkMessage);
		const unsigned int tickEndValue = asTickEndCommand->GetTickValue();


		if (tickStartValue != tickEndValue)
			ThrowLogicErrorExceptionWithLine("Tick start value and tick end values were not the same, while executing simulation tick.");

		m_SimulationTickValue.Increment();
	}



	void FeedCommandsVectorsFromQueue(
		ClientSimulationCommandsQueue& queue,
		std::vector<std::shared_ptr<ClientSimulationCommand>>& commandsThatSpawnActors,
		std::vector<std::shared_ptr<ClientSimulationCommand>>& otherCommands
	);

	void ExecuteCommandsVectors(
		ClientGameSimulation& clientGameSimulation,
		std::vector<std::shared_ptr<ClientSimulationCommand>>& commandsThatSpawnActors,
		std::vector<std::shared_ptr<ClientSimulationCommand>>& otherCommands
	);

	void ExecuteQueueCoreCommands(
		ClientSimulationCommandsQueue& queue,
		ClientGameSimulation& clientGameSimulation
	)
	{
		// TODO : put a using directive on ClientSimulationCommandsQueue to help with these types?
		std::vector<std::shared_ptr<ClientSimulationCommand>> commandsThatSpawnActors{};
		std::vector<std::shared_ptr<ClientSimulationCommand>> otherCommands{};

		FeedCommandsVectorsFromQueue(queue, /*out*/ commandsThatSpawnActors, /*out*/ otherCommands);
		ExecuteCommandsVectors(clientGameSimulation, commandsThatSpawnActors, otherCommands);
	}



	void FeedCommandsVectorsFromQueue(
		ClientSimulationCommandsQueue& queue,
		std::vector<std::shared_ptr<ClientSimulationCommand>>& out_CommandsThatSpawnActors,
		std::vector<std::shared_ptr<ClientSimulationCommand>>& out_OtherCommands
	)
	{
		while (queue.Size() > 1)
		{
			auto lastReadCommand = queue.Dequeue();
			NestAssert(lastReadCommand != nullptr);

			const auto networkMessage = dynamic_cast<NetworkMessage*>(lastReadCommand.get());
			NestAssert(networkMessage != nullptr);

			if (IsActorSpawningCommand(networkMessage->GetTypeId()))
			{
				out_CommandsThatSpawnActors.emplace_back(lastReadCommand);
			}
			else
			{
				out_OtherCommands.emplace_back(lastReadCommand);
			}
		}
	}



	void ExecuteCommandsVectors(
		ClientGameSimulation& clientGameSimulation,
		std::vector<std::shared_ptr<ClientSimulationCommand>>& commandsThatSpawnActors,
		std::vector<std::shared_ptr<ClientSimulationCommand>>& otherCommands
	)
	{
		SimulationActorMovementSystem* simulationActorMovementSystem = clientGameSimulation.GetSimulationActorMovementSystem();

		// TODO : This nullptr check should not be here, but due to way dependencies were implemented, I don't know how to refactor this code, for now.
		if (simulationActorMovementSystem == nullptr) return;
		// NestAssert(simulationActorMovementSystem != nullptr);


		for (auto& commandThatSpawnActors : commandsThatSpawnActors)
		{
			commandThatSpawnActors->Execute(clientGameSimulation);
		}

		for (auto& command : otherCommands)
		{
			command->Execute(clientGameSimulation);
		}
		simulationActorMovementSystem->UpdateActorsMovementData(clientGameSimulation.GetTickDeltaTime());
	}
}

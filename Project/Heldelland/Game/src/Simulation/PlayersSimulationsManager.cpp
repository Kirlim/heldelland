//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/PlayersSimulationsManager.h"

#include<Nest/NestAssert.h>
#include<Nest/Exceptions/LogicErrorException.h>

#include"Game/Simulation/PlayerSimulationCommandsQueuesManager.h"



namespace Game
{
	PlayersSimulationsManager::PlayersSimulationsManager() = default;



	PlayersSimulationsManager::~PlayersSimulationsManager() = default;



	void PlayersSimulationsManager::Register(PlayerSimInfoId playerId)
	{
		if (m_SimulationsQueuesManagers.count(playerId) > 0)
		{
			ThrowLogicErrorExceptionWithLine("Player id is already registered.");
		}


		auto queuesManager = std::make_unique<PlayerSimulationCommandsQueuesManager>();
		m_SimulationsQueuesManagers[playerId] = std::make_unique<PlayerSimulationCommandsQueuesManager>();
	}



	void PlayersSimulationsManager::Unregister(PlayerSimInfoId playerId)
	{
		if (m_SimulationsQueuesManagers.count(playerId) == 0)
		{
			ThrowLogicErrorExceptionWithLine("Player id is not registered.");
		}

		m_SimulationsQueuesManagers.erase(playerId);
	}



	bool PlayersSimulationsManager::IsRegistered(PlayerSimInfoId playerId) const
	{
		return (m_SimulationsQueuesManagers.count(playerId) > 0);
	}



	PlayerSimulationCommandsQueuesManager* PlayersSimulationsManager::GetPlayerCommandsQueueManager(PlayerSimInfoId playerId) const
	{
		PlayerSimulationCommandsQueuesManager* simulationQueuesManager = nullptr;

		if (IsRegistered(playerId))
		{
			simulationQueuesManager = m_SimulationsQueuesManagers.at(playerId).get();
		}

		return (simulationQueuesManager);
	}



	void PlayersSimulationsManager::ForEach(std::function<void(PlayerSimulationCommandsQueuesManager*)> action)
	{
		NestAssert(action != nullptr);

		for (auto& it : m_SimulationsQueuesManagers)
		{
			action(it.second.get());
		}
	}



	PlayersSimulationsManager::SimulationsContainer::iterator PlayersSimulationsManager::begin()
	{
		return (m_SimulationsQueuesManagers.begin());
	}



	PlayersSimulationsManager::SimulationsContainer::iterator PlayersSimulationsManager::end()
	{
		return (m_SimulationsQueuesManagers.end());
	}
}

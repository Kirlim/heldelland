//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/GameServerTickEventsContainer.h"



namespace Game
{
	GameServerTickEventsContainer::~GameServerTickEventsContainer() = default;



	void GameServerTickEventsContainer::RegisterNewActorMovementSpeedEvent(ActorNetworkId actorNetworkId, simfix64 movementSpeed)
	{
		m_NewActorMovementSpeedEvents.emplace_back(actorNetworkId, movementSpeed);
	}



	void GameServerTickEventsContainer::RegisterNewActorPathEvent(ActorNetworkId actorNetworkId, std::shared_ptr<Path> path)
	{
		m_NewActorPathEvents.emplace_back(actorNetworkId, std::move(path));
	}



	int GameServerTickEventsContainer::GetNewActorMovementSpeedEventsCount() const
	{
		return int(m_NewActorMovementSpeedEvents.size());
	}



	int GameServerTickEventsContainer::GetNewActorPathEventsCount() const
	{
		return int(m_NewActorPathEvents.size());
	}



	void GameServerTickEventsContainer::ClearAllRegisteredEvents()
	{
		m_NewActorMovementSpeedEvents.clear();
		m_NewActorPathEvents.clear();
	}



	void GameServerTickEventsContainer::RunForNewActorMovementSpeedEvents(
		std::function<void(NewActorMovementSpeedEventsIterator, NewActorMovementSpeedEventsIterator)> action
	)
	{
		action(std::begin(m_NewActorMovementSpeedEvents), std::end(m_NewActorMovementSpeedEvents));
	}



	void GameServerTickEventsContainer::RunForNewActorPathEvents(
		std::function<void(NewActorPathEventsIterator, NewActorPathEventsIterator)> action
	)
	{
		action(std::begin(m_NewActorPathEvents), std::end(m_NewActorPathEvents));
	}
}

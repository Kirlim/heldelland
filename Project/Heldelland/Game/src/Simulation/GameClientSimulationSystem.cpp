//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/GameClientSimulationSystem.h"

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NullArgumentException.h>

#include"Game/Simulation/IGameClientSimulation.h"



namespace Game
{
	GameClientSimulationSystem::~GameClientSimulationSystem() = default;



	void GameClientSimulationSystem::StartClientSimulation(std::unique_ptr<IGameClientSimulation>& gameClientSimulation)
	{
		if (gameClientSimulation == nullptr)
			ThrowNullArgumentExceptionWithLine("Game Client Simulation cannot be null");

		if (m_GameClientSimulation != nullptr)
			ThrowLogicErrorExceptionWithLine("There is already a Game Client Simulation set up");

		m_GameClientSimulation = std::move(gameClientSimulation);
	}



	IGameClientSimulation* GameClientSimulationSystem::GetClientSimulation() const
	{
		return m_GameClientSimulation.get();
	}



	void GameClientSimulationSystem::DoUpdate(float deltaTime)
	{
		if (m_GameClientSimulation == nullptr) return;
		m_GameClientSimulation->Update(deltaTime);
	}
}

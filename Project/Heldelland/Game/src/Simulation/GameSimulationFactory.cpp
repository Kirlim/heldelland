//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/GameSimulationFactory.h"

#include<Nest/Log.h>
#include<Nest/IResourcesManager.h>
#include<Nest/IComponentsContainers.h>
#include<Nest/ITemplatedSingleComponentsContainer.h>
#include<Nest/IEntity.h>


#include<Nest/World.h>


#include"Game/Map/Tileset.h"
#include"Game/Map/Terrain.h"
#include"Game/Map/Map.h"
#include"Game/Map/MapLoader.h"
#include"Game/Map/IMapData.h"
#include"Game/Map/YamlTilesetFileLoader.h"
#include"Game/Map/MapsFactory.h"

#include"Game/Pathfinding/AStar.h"
#include"Game/Pathfinding/SimpleStupidFunnelAlgorithm.h"

#include"Game/SimulationCommons/ActorsTypes.h"
#include"Game/SimulationCommons/ActorsManager.h"
#include"Game/SimulationCommons/SimulationActorsMovementData.h"
#include"Game/SimulationCommons/SimulationActorsMovementLogic.h"
#include"Game/SimulationCommons/SimulationActorMovementSystem.h"

#include"Game/Simulation/SimpleSimActorsSpawner.h"
#include"Game/Simulation/SimActorMovementBehavior.h"
#include"Game/Simulation/ClientGameSimulation.h"
#include"Game/Simulation/ServerGameSimulation.h"



namespace
{
	std::unique_ptr<Game::ActorsManager> InstantiateActorsManager(Nest::EntitiesManager& entitiesManager)
	{
		auto manager = std::make_unique<Game::ActorsManager>(entitiesManager);
		return manager;
	}



	std::unique_ptr<Game::SimpleSimActorsSpawner> InstantiateSimulationActorsSpawner(Game::ActorsManager& actorsManager)
	{
		auto spawner = std::make_unique<Game::SimpleSimActorsSpawner>();
		return spawner;
	}



	void SetUpSimulationActorsSpawnerFunctions(
		Game::SimpleSimActorsSpawner& spawner,
		Game::ActorsManager& actorsManager,
		Game::SimulationActorMovementSystem& actorsMovementSystem
	)
	{
		spawner.SetSpawnFunction(
			Game::ActorTypeId{ ACTOR_TYPE_PLAYER },
			[&]()
			{
				auto actorEntityId = actorsManager.InstantiateNew(true);
				auto actorBehavior = actorsManager.GetActor(actorEntityId);

				const auto actorNetworkId = actorBehavior->GetNetworkId();
				actorsMovementSystem.RegisterActorAsMovementCapable(actorNetworkId);

				auto actorEntity = actorBehavior->GetOwner();
				auto movementBehavior = actorEntity->AddComponent<Game::SimActorMovementBehavior>();
				movementBehavior->SetActorId(actorEntityId);
				movementBehavior->SetMovementSpeed(Game::simfix64(1));

				Nest::Log::Message(
					"Instantiated simulation player actor with id [" + std::to_string(actorBehavior->GetActorId().value) +
					"] and network id [" + std::to_string(actorBehavior->GetNetworkId().value) + "]."
				);

				return (actorEntity);
			});
	}



	std::unique_ptr<Game::Map> InstantiateMap(
		const Game::MapsFactory& mapsFactory, Nest::IResourcesManager& resourcesManager, const std::string mapName
	)
	{
		if (mapName != "testMap")
		{
			throw std::runtime_error("Only 'testMap' is implemented right now.");
		}


		Game::YamlTilesetFileLoader tilesetLoader{ resourcesManager };
		auto tilesetUPtr = tilesetLoader.Load("GameData/Tilesets/TestTileset.tls");
		const std::shared_ptr<Game::Tileset> tileset{ std::move(tilesetUPtr) };

		Game::MapLoader mapLoader;
		auto mapData = mapLoader.MakeTestMap();
		mapData->SetTileset(tileset);

		auto map = mapsFactory.Instantiate(mapData);
		return map;
	}



	std::unique_ptr<Game::AStar> InstantiatePathfinder(const Game::Map* map)
	{
		if (map == nullptr)
		{
			throw std::runtime_error("Map is null");
		}

		std::unique_ptr<Game::IFunnelAlgorithm> funnelAlgorithm = std::make_unique<Game::SimpleStupidFunnelAlgorithm>();
		auto pathfinder = std::make_unique<Game::AStar>(funnelAlgorithm);

		bool collisionMap[] = {
			false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false
		};

		pathfinder->SetUp(collisionMap, 9, 9, Game::simfix64(map->GetTerrain()->TileSize));
		return pathfinder;
	}



	std::unique_ptr<Game::SimulationActorMovementSystem> InstantiateSimulationActorMovementSystem(
		Game::SimulationActorsMovementData& simulationActorsMovementData,
		Game::SimulationActorsMovementDataManager& simulationActorsMovementDataManager,
		Game::SimulationActorsMovementLogic& simulationActorsMovementLogic
	)
	{
		return std::make_unique<Game::SimulationActorMovementSystem>(
			simulationActorsMovementData,
			simulationActorsMovementDataManager,
			simulationActorsMovementLogic
		);
	}
}



namespace Game
{
	GameSimulationFactory::GameSimulationFactory(
		Nest::EntitiesManager& entitiesManager,
		Nest::IResourcesManager& resourcesManager,
		const Nest::IComponentsContainers& componentsContainers,
		const MapsFactory& mapsFactory,
		SimulationActorsMovementData& simulationActorsMovementData,
		SimulationActorsMovementDataManager& simulationActorsMovementDataManager,
		SimulationActorsMovementLogic& simulationActorsMovementLogic
	) :
		m_EntitiesManager(entitiesManager), m_ResourcesManager(resourcesManager),
		m_ComponentsContainers(componentsContainers), m_MapsFactory(mapsFactory),
		m_SimulationActorsMovementData(simulationActorsMovementData),
		m_SimulationActorsMovementDataManager(simulationActorsMovementDataManager),
		m_SimulationActorsMovementLogic(simulationActorsMovementLogic)
	{
	}



	GameSimulationFactory::~GameSimulationFactory() = default;



	std::unique_ptr<IGameServerSimulation> GameSimulationFactory::InstantiateServerSimulation(std::string mapName) const
	{
		auto actorsManager = InstantiateActorsManager(m_EntitiesManager);
		auto simulationActorsSpawner = InstantiateSimulationActorsSpawner(*actorsManager);
		auto map = InstantiateMap(m_MapsFactory, m_ResourcesManager, mapName);
		auto pathfinder = InstantiatePathfinder(map.get());

		const auto simActorMovementBehaviorsContainer = m_ComponentsContainers.GetSingleContainer<SimActorMovementBehavior>();
		if (simActorMovementBehaviorsContainer == nullptr)
			ThrowLogicErrorExceptionWithLine("Could not find SimActorMovementBehavior container.");

		auto simulationActorMovementSystem = InstantiateSimulationActorMovementSystem(
			m_SimulationActorsMovementData, m_SimulationActorsMovementDataManager, m_SimulationActorsMovementLogic
		);


		SetUpSimulationActorsSpawnerFunctions(*simulationActorsSpawner, *actorsManager, *simulationActorMovementSystem);


		std::unique_ptr<IGameServerSimulation> serverSimulation = std::make_unique<ServerGameSimulation>(
			std::move(actorsManager),
			std::move(simulationActorsSpawner),
			std::move(map),
			std::move(pathfinder),
			*simActorMovementBehaviorsContainer,
			std::move(simulationActorMovementSystem)
		);

		return serverSimulation;
	}



	std::unique_ptr<IGameClientSimulation> GameSimulationFactory::InstantiateClientSimulation(
		const Nest::World& simulationWorld,
		std::string mapName,
		simfix64 tickDeltaTime,
		float clientMaxTimeBehindBeforeTimeSpeedUp,
		float clientTimeSpeedUpFactor
	) const
	{
		auto actorsManager = InstantiateActorsManager(m_EntitiesManager);
		auto simActorsSpawner = InstantiateSimulationActorsSpawner(*actorsManager);
		auto map = InstantiateMap(m_MapsFactory, m_ResourcesManager, mapName);
		auto pathfinder = InstantiatePathfinder(map.get());

		auto simulationActorMovementSystem = InstantiateSimulationActorMovementSystem(
			m_SimulationActorsMovementData, m_SimulationActorsMovementDataManager, m_SimulationActorsMovementLogic
		);


		SetUpSimulationActorsSpawnerFunctions(*simActorsSpawner, *actorsManager, *simulationActorMovementSystem);


		std::unique_ptr<IGameClientSimulation> clientSimulation = std::make_unique<ClientGameSimulation>(
			simulationWorld,
			std::move(actorsManager),
			std::move(simActorsSpawner),
			std::move(map),
			std::move(pathfinder),
			std::move(simulationActorMovementSystem),
			tickDeltaTime,
			clientMaxTimeBehindBeforeTimeSpeedUp,
			clientTimeSpeedUpFactor
		);


		return clientSimulation;
	}
}

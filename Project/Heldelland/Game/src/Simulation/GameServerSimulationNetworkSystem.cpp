//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/GameServerSimulationNetworkSystem.h"

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NullPointerException.h>
#include<Nest/NestAssert.h>
#include<Nest/Cast.h>

#include<Nest/Events/EventsBus.h>

#include<Nest/Log.h>

#include"Game/Network/IHost.h"
#include"Game/Network/IHostChannel.h"
#include"Game/Network/IHostsManager.h"
#include"Game/Network/NetworkMessage.h"
#include"Game/Network/Messages/GameSimulationSettingsMessage.h"
#include"Game/Network/Messages/GenericMessagesHandler.h"
#include"Game/Network/INetworkMessagesFactory.h"
#include"Game/Network/INetworkMessagesHandlersManager.h"
#include"Game/Network/HostGameInformation.h"
#include"Game/Network/ServerNetworkController.h"
#include"Game/Network/INetworkSystem.h"

#include"Game/Network/Messages/JoinGameRequestMessage.h"
#include"Game/Network/Messages/GameSimulationLoadedMessage.h"

#include"Game/Network/Messages/JoinGameReplyMessage.h"

#include"Game/Simulation/IGameServerSimulation.h"
#include"Game/Simulation/GameServerSimulationSystem.h"
#include"Game/Simulation/ClientSimulationCommand.h"
#include"Game/Simulation/ClientSimulationCommandsQueue.h"
#include"Game/Simulation/PlayerSimulationCommandsQueuesManager.h"
#include"Game/Simulation/PlayerSimulationInformation.h"
#include"Game/Simulation/PlayersSimulationsManager.h"
#include"Game/Simulation/PlayerSimulationRangeManager.h"
#include"Game/SimulationCommons/SimulationActorMovementSystem.h"

#include"Game/Simulation/ServerCommands/PlayerMoveCommand.h"


// TODO : delete include
#include"Game/Network/HostChannel.h"



namespace Game
{
	GameServerSimulationNetworkSystem::GameServerSimulationNetworkSystem(
		INetworkSystem& networkSystem,
		GameServerSimulationSystem& gameServerSimulationSystem,
		INetworkMessagesFactory& networkMessagesFactory,
		INetworkMessagesHandlersManager& networkMessagesHandlersManager,
		IHostsManager& hostsManager
	) :
		m_NetworkSystem(networkSystem),
		m_GameServerSimulationSystem(gameServerSimulationSystem),
		m_NetworkMessagesFactory(networkMessagesFactory),
		m_NetworkMessagesHandlersManager(networkMessagesHandlersManager),
		m_HostsManager(hostsManager)
	{
		RegisterIncomingMessagesHandlers();
	}



	GameServerSimulationNetworkSystem::~GameServerSimulationNetworkSystem() = default;



	void GameServerSimulationNetworkSystem::RegisterIncomingMessagesHandlers()
	{
		// TODO : consider optimizing shared_ptr with std::move
		// JoinGameRequestMessage
		const auto joinGameRequestMessagesHandler = DoCast<GenericMessagesHandler<NetworkMessage>>(m_NetworkMessagesHandlersManager.GetHandler(JoinGameRequestMessage::TypeId));
		auto& joinGameRequestMessagesHandlerEventsBus = joinGameRequestMessagesHandler->GetEventsBus();
		m_JoinGameRequestMessagesToken = joinGameRequestMessagesHandlerEventsBus.Register(
			[&](std::shared_ptr<NetworkMessage>& message) { HandleIncomingJoinGameRequest(*message); }
		);


		// GameSimulationLoadedMessage
		const auto gameSimulationLoadedMessagesHandler = DoCast<GenericMessagesHandler<NetworkMessage>>(m_NetworkMessagesHandlersManager.GetHandler(GameSimulationLoadedMessage::TypeId));
		auto& gameSimulationLoadedMesasgesHandlerEventBus = gameSimulationLoadedMessagesHandler->GetEventsBus();
		m_GameSimulationLoadedMessageToken = gameSimulationLoadedMesasgesHandlerEventBus.Register(
			[&](std::shared_ptr<NetworkMessage>& message) { HandleGameSimulationLoadedMessage(*message); }
		);


		// PlayerMoveCommand
		const auto playerMoveCommandsHandler = DoCast<GenericMessagesHandler<NetworkMessage>>(m_NetworkMessagesHandlersManager.GetHandler(PlayerMoveCommand::TypeId));
		auto& playerMoveCommandsHandlerEventsBus = playerMoveCommandsHandler->GetEventsBus();
		m_PlayerMoveCommandsToken = playerMoveCommandsHandlerEventsBus.Register(
			[&](std::shared_ptr<NetworkMessage>& command) { HandleServerSimulationCommand(std::dynamic_pointer_cast<ServerSimulationCommand>(command)); }
		);
	}



	void GameServerSimulationNetworkSystem::DoFixedUpdate(float)
	{
		TickServerNetworkController();

		if (m_GameServerSimulationSystem.GetServerSimulation() != nullptr)
			TickServerSimulation();
	}



	void GameServerSimulationNetworkSystem::TickServerNetworkController()
	{
		m_NetworkSystem.ExecuteWithClientHostsChannel([this](IHost& client, IHostChannel& fromClient, IHostChannel& toClient)
			{
				if (m_ServerNetworkController == nullptr)
				{
					m_ServerNetworkController = std::make_unique<ServerNetworkController>(*m_NetworkSystem.GetMessagesHandlersManager());
				}

				m_ServerNetworkController->Tick(client, fromClient, toClient);
			}
		);
	}



	void GameServerSimulationNetworkSystem::TickServerSimulation()
	{
		DispatchJoinGameRequestsReplies();

		SendPendingPlayersSimulationsMessages();
	}



	void GameServerSimulationNetworkSystem::SendPendingPlayersSimulationsMessages() const
	{
		const auto playersSimulationsManager = m_GameServerSimulationSystem.GetServerSimulation()->GetPlayersSimulationsManager();

		for (auto& playerSimulationIterator : *playersSimulationsManager)
		{
			const auto playerSimInfoId = playerSimulationIterator.first;
			auto playersQueuesManager = playerSimulationIterator.second.get();
			const auto commandsQueue = playersQueuesManager->FetchNextCommittedQueue();

			if (commandsQueue != nullptr)
				SendCommandsQueueToPlayer(playerSimInfoId, *commandsQueue);
		}
	}



	void GameServerSimulationNetworkSystem::SendCommandsQueueToPlayer(
		PlayerSimInfoId playerSimInfoId,
		ClientSimulationCommandsQueue& commandsQueue
	) const
	{
		NestAssert(m_PlayerIdToHostIdMap.count(playerSimInfoId) > 0);

		const auto hostId = m_PlayerIdToHostIdMap.at(playerSimInfoId);
		auto channelToClient = GetNetworkChannelToClient(hostId);

		auto command = commandsQueue.Dequeue();
		while (command != nullptr)
		{
			const auto message = std::dynamic_pointer_cast<NetworkMessage>(command);
			NestAssert(message != nullptr);

			channelToClient->AddMessage(message);

			command = commandsQueue.Dequeue();
		}
	}



	void GameServerSimulationNetworkSystem::DispatchJoinGameRequestsReplies()
	{
		auto serverGameSimulation = m_GameServerSimulationSystem.GetServerSimulation();

		try
		{
			if (serverGameSimulation->HasJoinGameRepliesToDispatch())
			{
				serverGameSimulation->DispatchJoinGameReplies(
					[&](std::weak_ptr<PlayerSimulationInformation> playerSimulationInformation)
					{
						auto playerHostId = GetHostIdFromPlayerSimulationInfo(playerSimulationInformation);
						AddJoinGameRequestReplyToClientChannel(playerHostId);
						AddGameSimulationSettingsToClientChannel(playerHostId);
					}
				);
			}
		}
		catch (...)
		{
			std::throw_with_nested(std::runtime_error("Failed to dispatch Join Game Request Reply to the client."));
		}
	}



	void GameServerSimulationNetworkSystem::AddJoinGameRequestReplyToClientChannel(HostId hostId) const
	{
		try
		{
			auto joinGameReplyMessage = CreateJoinGameReplyNetworkMessage();
			SendNetworkMessageToClientChannel(joinGameReplyMessage, hostId);
		}
		catch (...)
		{
			std::throw_with_nested(std::runtime_error{ "Failed to add a Join Game Request Reply to the client channel." });
		}
	}



	std::shared_ptr<NetworkMessage> GameServerSimulationNetworkSystem::CreateJoinGameReplyNetworkMessage() const
	{
		const auto joinGameReplyBase = m_NetworkMessagesFactory.InstantiateMessage(JoinGameReplyMessage::TypeId);

		auto joinGameReply = std::dynamic_pointer_cast<JoinGameReplyMessage>(joinGameReplyBase);
		NestAssert(joinGameReply != nullptr);

		joinGameReply->SetReply(JoinGameReplyMessage::Reply::Accepted);

		return joinGameReplyBase;
	}



	void GameServerSimulationNetworkSystem::AddGameSimulationSettingsToClientChannel(HostId hostId) const
	{
		try
		{
			auto gameSimulationSettingsMessage = CreateGameSimulationSettingsNetworkMessage();
			SendNetworkMessageToClientChannel(gameSimulationSettingsMessage, hostId);
		}
		catch (...)
		{
			std::throw_with_nested(std::runtime_error{ "Failed to add a Game Simulation Settings to the client channel." });
		}
	}



	std::shared_ptr<NetworkMessage> GameServerSimulationNetworkSystem::CreateGameSimulationSettingsNetworkMessage() const
	{
		const auto gameSimulationSettingsBase = m_NetworkMessagesFactory.InstantiateMessage(GameSimulationSettingsMessage::TypeId);
		auto gameSimulationSettings = std::dynamic_pointer_cast<GameSimulationSettingsMessage>(gameSimulationSettingsBase);
		NestAssert(gameSimulationSettings != nullptr);

		gameSimulationSettings->SetMapName("testMap");

		return gameSimulationSettingsBase;
	}



	HostId GameServerSimulationNetworkSystem::GetHostIdFromPlayerSimulationInfo(
		const std::weak_ptr<PlayerSimulationInformation>& playerSimulationInformation
	) const
	{
		const auto info = playerSimulationInformation.lock();

		if (info == nullptr)
			ThrowLogicErrorExceptionWithLine("Dispatch has received a null player simulation");

		const auto playerId = info->GetId();
		if (m_PlayerIdToHostIdMap.count(playerId) == 0)
			ThrowLogicErrorExceptionWithLine("Found player id without host while dispatching join game request replies.");

		const auto hostId = m_PlayerIdToHostIdMap.at(playerId);

		return hostId;
	}



	IHost* GameServerSimulationNetworkSystem::GetHost(HostId hostId) const
	{
		auto host = m_HostsManager.GetHost(hostId).lock();
		NestAssert(host != nullptr);

		return host.get();
	}



	IHostChannel* GameServerSimulationNetworkSystem::GetNetworkChannelToClient(HostId hostId) const
	{
		const auto host = GetHost(hostId);
		const auto channel = host->GetToHostChannel();

		NestAssert(channel != nullptr);
		return channel;
	}



	void GameServerSimulationNetworkSystem::SendNetworkMessageToClientChannel(
		std::shared_ptr<NetworkMessage>& message,
		HostId hostId
	) const
	{
		auto sendChannel = GetNetworkChannelToClient(hostId);
		sendChannel->AddMessage(message);
	}



	void GameServerSimulationNetworkSystem::HandleIncomingJoinGameRequest(const NetworkMessage& joinGameRequestMessage)
	{
		const auto& message = DoCast<const JoinGameRequestMessage>(joinGameRequestMessage);

		Nest::Log::Message("Received join game request.");

		try
		{
			const auto newPlayerSimInfo = RegisterNewPlayerSimulationInformationToServer();
			RegisterNewPlayerHostInformation(message, newPlayerSimInfo);
		}
		catch (...)
		{
			// TODO : remove simulation info from server, if was not registered, but then got registered with this call
			// TODO : remove from player to host id, if was not registered, but then got registered with this call
			std::throw_with_nested(std::runtime_error("Failed to handle incoming join game request."));
		}
	}



	std::shared_ptr<PlayerSimulationInformation> GameServerSimulationNetworkSystem::RegisterNewPlayerSimulationInformationToServer() const
	{
		static PlayerSimInfoId::underlying_type nextNewPlayerId = 1;

		auto serverSimulation = m_GameServerSimulationSystem.GetServerSimulation();
		NestAssert(serverSimulation != nullptr);

		// TODO : a factory to instantiante PlayerSimulationInformation would help in moving this instantiation code to a better place
		// Register the new player
		// TODO : reconnect logic
		// TODO : pass the PlayerSimulationInformation instantiation to the ServerGameSimulation class (so simulation can be more easily tied to worlds) [Analyse worthness]
		auto playerSimulationRangeManager = std::make_unique<PlayerSimulationRangeManager>(
			*serverSimulation->GetSimulationActorMovementSystem()->GetSimulationActorsMovementData()
		);
		playerSimulationRangeManager->SetViewRadius(simfix64(200)); // TODO : set in a better place. And dont use magic values.
		// TODO : update view position

		const auto playerSimulationInformation = std::make_shared<PlayerSimulationInformation>(
			PlayerSimInfoId{ nextNewPlayerId++ },
			std::move(playerSimulationRangeManager)
		);

		if (!serverSimulation->RegisterPlayer(playerSimulationInformation))
			ThrowLogicErrorExceptionWithLine("Failed to register player information to the server.");

		return playerSimulationInformation;
	}



	void GameServerSimulationNetworkSystem::RegisterNewPlayerHostInformation(
		const JoinGameRequestMessage& joinGameRequestMessage,
		const std::shared_ptr<PlayerSimulationInformation>& playerSimulationInformation
	)
	{
		auto remoteHost = joinGameRequestMessage.GetRemoteHost();
		NestAssert(remoteHost != nullptr);

		auto playerHostInformation = std::make_unique<HostGameInformation>();
		playerHostInformation->playerId = playerSimulationInformation->GetId();

		m_PlayerIdToHostIdMap[playerSimulationInformation->GetId()] = remoteHost->GetLocadId();
		remoteHost->SetGameInformation(playerHostInformation);
	}



	void GameServerSimulationNetworkSystem::HandleGameSimulationLoadedMessage(const NetworkMessage& gameSimulationLoadedMessage) const
	{
		const auto& message = DoCast<const GameSimulationLoadedMessage>(gameSimulationLoadedMessage);

		auto serverGameSimulation = m_GameServerSimulationSystem.GetServerSimulation();
		NestAssert(serverGameSimulation != nullptr);

		const auto host = message.GetRemoteHost();
		NestAssert(host != nullptr);

		const auto hostGameInformation = host->GetGameInformation();
		NestAssert(hostGameInformation != nullptr);

		serverGameSimulation->PlayerSimulationLoaded(hostGameInformation->playerId);
	}



	void GameServerSimulationNetworkSystem::HandleServerSimulationCommand(std::shared_ptr<ServerSimulationCommand> command) const
	{
		auto serverGameSimulation = m_GameServerSimulationSystem.GetServerSimulation();
		NestAssert(serverGameSimulation != nullptr);

		serverGameSimulation->RegisterCommand(command);
	}
}

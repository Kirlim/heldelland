//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/ClientCommands/SpawnPlayerCharacterCommand.h"

#include<string>

#include<Nest/Log.h>

#include"Game/Network/INetworkSerializer.h"
#include"Game/Simulation/IGameClientSimulation.h"

#include<Nest/IEntity.h>
#include<Nest/Transform.h>

#include"Game/Simulation/CommandsTypes.h"



namespace Game
{
	const NetworkMessageTypeId SpawnPlayerCharacterCommand::TypeId{ SPAWN_PLAYER_CHARACTER_COMMAND_TYPE };



	SpawnPlayerCharacterCommand::SpawnPlayerCharacterCommand() :
		ClientSimulationCommand(), NetworkMessage(NetworkMessageReliabilityType::Reliable)
	{
	}



	SpawnPlayerCharacterCommand::~SpawnPlayerCharacterCommand() = default;



	void SpawnPlayerCharacterCommand::Execute(IGameClientSimulation& simulation)
	{
		simulation.SetPlayerActorNetworkId(m_ActorNetworkId);
		const auto actorId = simulation.SpawnActor(this->GetActorTypeId(), this->GetSpawnPosition(), true, m_ActorNetworkId);

		Nest::Log::Message(
			"Instantiated view player actor with id [" + std::to_string(actorId.value) +
			"] and network id [" + std::to_string(m_ActorNetworkId.value) + "]."
		);
	}



	NetworkMessageTypeId SpawnPlayerCharacterCommand::GetTypeId() const
	{
		return TypeId;
	}



	void SpawnPlayerCharacterCommand::SetSpawnPosition(Nest::Vector2<simfix64> spawnPosition)
	{
		m_SpawnPosition = spawnPosition;
	}



	Nest::Vector2<simfix64> SpawnPlayerCharacterCommand::GetSpawnPosition() const
	{
		return m_SpawnPosition;
	}



	void SpawnPlayerCharacterCommand::SetActorTypeId(ActorTypeId actorTypeId)
	{
		m_ActorTypeId = actorTypeId;
	}



	ActorTypeId SpawnPlayerCharacterCommand::GetActorTypeId() const
	{
		return m_ActorTypeId;
	}



	void SpawnPlayerCharacterCommand::SetNetworkId(ActorNetworkId networkId)
	{
		m_ActorNetworkId = networkId;
	}



	ActorNetworkId SpawnPlayerCharacterCommand::GetNetworkId() const
	{
		return m_ActorNetworkId;
	}



	bool SpawnPlayerCharacterCommand::DoSerialize(INetworkSerializer& serializer)
	{
		auto typeId = uint32_t(TypeId.value);

		auto success = serializer.SerializeUInt32(typeId, NetworkMessage::TypeIdBitsSize);
		if (success) success = serializer.SerializeInt64(m_SpawnPosition.x.rawValue);
		if (success) success = serializer.SerializeInt64(m_SpawnPosition.y.rawValue);
		if (success) success = serializer.SerializeUInt16(m_ActorTypeId.value);
		if (success) success = serializer.SerializeUInt16(m_ActorNetworkId.value);

		return success;
	}
}

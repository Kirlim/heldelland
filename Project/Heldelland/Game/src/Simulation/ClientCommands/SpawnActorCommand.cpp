
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/ClientCommands/SpawnActorCommand.h"

#include<string>

#include<Nest/Log.h>

#include"Game/Network/INetworkSerializer.h"
#include"Game/Simulation/IGameClientSimulation.h"

#include"Game/Simulation/CommandsTypes.h"



namespace Game
{
	const NetworkMessageTypeId SpawnActorCommand::TypeId{ SPAWN_ACTOR_COMMAND_TYPE };



	SpawnActorCommand::SpawnActorCommand() :
		ClientSimulationCommand(), NetworkMessage(NetworkMessageReliabilityType::Reliable)
	{
		// Nothing here
	}



	SpawnActorCommand::~SpawnActorCommand()
	{
		// Nothing here
	}



	void SpawnActorCommand::Execute(IGameClientSimulation& simulation)
	{
		const auto actorId = simulation.SpawnActor(this->GetActorTypeId(), this->GetSpawnPosition(), true, m_ActorNetworkId);

		Nest::Log::Message(
			"Instantiated view actor with id [" + std::to_string(actorId.value) +
			"] and network id [" + std::to_string(m_ActorNetworkId.value) + "]."
		);
	}



	NetworkMessageTypeId SpawnActorCommand::GetTypeId() const
	{
		return TypeId;
	}



	void SpawnActorCommand::SetSpawnPosition(Nest::Vector2<simfix64> spawnPosition)
	{
		m_SpawnPosition = spawnPosition;
	}



	Nest::Vector2<simfix64> SpawnActorCommand::GetSpawnPosition() const
	{
		return m_SpawnPosition;
	}



	void SpawnActorCommand::SetActorTypeId(ActorTypeId actorTypeId)
	{
		m_ActorTypeId = actorTypeId;
	}



	ActorTypeId SpawnActorCommand::GetActorTypeId() const
	{
		return m_ActorTypeId;
	}



	void SpawnActorCommand::SetNetworkId(ActorNetworkId networkId)
	{
		m_ActorNetworkId = networkId;
	}



	ActorNetworkId SpawnActorCommand::GetNetworkId() const
	{
		return m_ActorNetworkId;
	}



	bool SpawnActorCommand::DoSerialize(INetworkSerializer& serializer)
	{
		auto typeId = uint32_t(TypeId.value);

		auto success = serializer.SerializeUInt32(typeId, NetworkMessage::TypeIdBitsSize);
		if (success) success = serializer.SerializeInt64(m_SpawnPosition.x.rawValue);
		if (success) success = serializer.SerializeInt64(m_SpawnPosition.y.rawValue);
		if (success) success = serializer.SerializeUInt16(m_ActorTypeId.value);
		if (success) success = serializer.SerializeUInt16(m_ActorNetworkId.value);

		return(success);
	}
}
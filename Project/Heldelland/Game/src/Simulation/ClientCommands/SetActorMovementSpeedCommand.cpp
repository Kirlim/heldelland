//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/ClientCommands/SetActorMovementSpeedCommand.h"

#include<Nest/NestAssert.h>

#include"Game/Network/INetworkSerializer.h"
#include"Game/Simulation/CommandsTypes.h"
#include"Game/Simulation/IGameClientSimulation.h"
#include"Game/SimulationCommons/SimulationActorMovementSystem.h"



namespace Game
{
	const NetworkMessageTypeId SetActorMovementSpeedCommand::TypeId{ SET_ACTOR_MOVEMENT_SPEED_COMMAND_TYPE };



	SetActorMovementSpeedCommand::SetActorMovementSpeedCommand() :
		NetworkMessage(NetworkMessageReliabilityType::Reliable)
	{
	}



	SetActorMovementSpeedCommand::~SetActorMovementSpeedCommand() = default;



	void SetActorMovementSpeedCommand::Execute(IGameClientSimulation& simulation)
	{
		const auto simulationActorMovementSystem = simulation.GetSimulationActorMovementSystem();
		NestAssert(simulationActorMovementSystem != nullptr);

		simulationActorMovementSystem->SetActorMovementSpeed(m_ActorNetworkId, m_MovementSpeed);
	}



	NetworkMessageTypeId SetActorMovementSpeedCommand::GetTypeId() const
	{
		return TypeId;
	}



	void SetActorMovementSpeedCommand::SetActorNetworkId(ActorNetworkId actorNetworkId)
	{
		m_ActorNetworkId = actorNetworkId;
	}



	void SetActorMovementSpeedCommand::SetMovementSpeed(simfix64 movementSpeed)
	{
		m_MovementSpeed = movementSpeed;
	}



	ActorNetworkId SetActorMovementSpeedCommand::GetActorNetworkId() const
	{
		return m_ActorNetworkId;
	}



	simfix64 SetActorMovementSpeedCommand::GetMovementSpeed() const
	{
		return m_MovementSpeed;
	}



	bool SetActorMovementSpeedCommand::DoSerialize(INetworkSerializer& serializer)
	{
		auto typeId = uint32_t(TypeId.value);

		bool failure = false;

		failure |= !serializer.SerializeUInt32(typeId, TypeIdBitsSize);
		failure |= !serializer.SerializeUInt16(m_ActorNetworkId.value);
		failure |= !serializer.SerializeInt64(m_MovementSpeed.rawValue);

		return !failure;
	}
}

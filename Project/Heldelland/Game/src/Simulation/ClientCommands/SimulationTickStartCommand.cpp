
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/ClientCommands/SimulationTickStartCommand.h"

#include"Game/Network/INetworkSerializer.h"

#include"Game/Simulation/CommandsTypes.h"



namespace Game
{
	const NetworkMessageTypeId SimulationTickStartCommand::TypeId{ SIMULATION_TICK_START_COMMAND_TYPE };



	SimulationTickStartCommand::SimulationTickStartCommand() :
		ClientSimulationCommand(), NetworkMessage(NetworkMessageReliabilityType::Reliable)
	{
		// Nothing here
	}



	SimulationTickStartCommand::~SimulationTickStartCommand() = default;



	void SimulationTickStartCommand::Execute(IGameClientSimulation& simulation)
	{
		// Nothing to be done
	}



	NetworkMessageTypeId SimulationTickStartCommand::GetTypeId() const
	{
		return TypeId;
	}



	void SimulationTickStartCommand::SetTickValue(unsigned int tickValue)
	{
		m_TickValue = tickValue;
	}



	unsigned int SimulationTickStartCommand::GetTickValue() const
	{
		return m_TickValue;
	}



	bool SimulationTickStartCommand::DoSerialize(INetworkSerializer& serializer)
	{
		auto typeId = uint32_t(TypeId.value);

		bool success = serializer.SerializeUInt32(typeId, TypeIdBitsSize);
		if (success) success = serializer.SerializeUInt32(m_TickValue);

		return(success);
	}
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/ClientCommands/SimulationTickEndCommand.h"

#include"Game/Network/INetworkSerializer.h"

#include"Game/Simulation/CommandsTypes.h"



namespace Game
{
	const NetworkMessageTypeId SimulationTickEndCommand::TypeId{ SIMULATION_TICK_END_COMMAND_TYPE };



	SimulationTickEndCommand::SimulationTickEndCommand() :
		ClientSimulationCommand(), NetworkMessage(NetworkMessageReliabilityType::Reliable)
	{
		// Nothing here
	}



	SimulationTickEndCommand::~SimulationTickEndCommand() = default;



	void SimulationTickEndCommand::Execute(IGameClientSimulation& simulation)
	{
		// Nothing to be done
	}



	NetworkMessageTypeId SimulationTickEndCommand::GetTypeId() const
	{
		return TypeId;
	}



	void SimulationTickEndCommand::SetTickValue(unsigned int tickValue)
	{
		m_TickValue = tickValue;
	}



	unsigned int SimulationTickEndCommand::GetTickValue() const
	{
		return m_TickValue;
	}



	bool SimulationTickEndCommand::DoSerialize(INetworkSerializer& serializer)
	{
		auto typeId = uint32_t(TypeId.value);

		bool success = serializer.SerializeUInt32(typeId, TypeIdBitsSize);
		if (success) success = serializer.SerializeUInt32(m_TickValue);

		return success;
	}
}
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/ClientCommands/SetActorPositionCommand.h"

#include<memory>

#include"Game/Network/INetworkSerializer.h"

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Log.h>

#include<Nest/IEntity.h>

#include"Game/Simulation/IGameClientSimulation.h"
#include"Game/Simulation/SimActorMovementBehavior.h"
#include"Game/SimulationCommons/ActorsManager.h"

#include"Game/Simulation/CommandsTypes.h"

#include"Game/Pathfinding/Path.h"



namespace Game
{
	const NetworkMessageTypeId SetActorPositionCommand::TypeId{ SET_ACTOR_POSITION_COMMAND_TYPE };



	SetActorPositionCommand::SetActorPositionCommand() :
		NetworkMessage(NetworkMessageReliabilityType::Reliable)
	{
	}



	SetActorPositionCommand::~SetActorPositionCommand() = default;



	void SetActorPositionCommand::Execute(IGameClientSimulation& simulation)
	{
		const auto actorsManager = simulation.GetActorsManager();
		if (actorsManager == nullptr)
			ThrowLogicErrorExceptionWithLine("Simulation has returned null actors manager.");

		const auto actorBehavior = actorsManager->GetActorWithNetworkId(m_ActorNetworkId);
		if (actorBehavior == nullptr)
			ThrowLogicErrorExceptionWithLine("Failed to find actor with network id.");

		auto actorEntity = actorBehavior->GetOwner();
		const auto actorMovementBehavior = actorEntity->GetComponent<SimActorMovementBehavior>();
		NestAssert(actorMovementBehavior != nullptr);

		// TODO : stop using new path to set a fixed position
		auto path = std::make_shared<Path>();
		path->PushBack(m_Position);

		actorMovementBehavior->MoveInPath(path, simfix64{ 0.0f });

		// TODO : make command be useful again, or delete it
	}



	NetworkMessageTypeId SetActorPositionCommand::GetTypeId() const
	{
		return TypeId;
	}



	void SetActorPositionCommand::SetActorNetworkId(ActorNetworkId actorNetworkId)
	{
		m_ActorNetworkId = actorNetworkId;
	}



	ActorNetworkId SetActorPositionCommand::GetActorNetworkId() const
	{
		return m_ActorNetworkId;
	}



	void SetActorPositionCommand::SetPosition(Nest::Vector2<simfix64> position)
	{
		m_Position = position;
	}



	Nest::Vector2<simfix64> SetActorPositionCommand::GetPosition() const
	{
		return m_Position;
	}



	bool SetActorPositionCommand::DoSerialize(INetworkSerializer& serializer)
	{
		auto typeId = uint32_t(TypeId.value);

		auto success = serializer.SerializeUInt32(typeId, TypeIdBitsSize);
		if (success) success = serializer.SerializeUInt16(m_ActorNetworkId.value);
		if (success) success = serializer.SerializeInt64(m_Position.x.rawValue);
		if (success) success = serializer.SerializeInt64(m_Position.y.rawValue);

		return (success);
	}
}

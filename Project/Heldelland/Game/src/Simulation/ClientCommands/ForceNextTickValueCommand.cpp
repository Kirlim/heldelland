
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/ClientCommands/ForceNextTickValueCommand.h"

#include"Game/Simulation/ClientGameSimulation.h"
#include"Game/Network/INetworkSerializer.h"

#include"Game/Simulation/CommandsTypes.h"



namespace Game
{
	const NetworkMessageTypeId ForceNextTickValueCommand::TypeId{ FORCE_NEXT_TICK_VALUE_COMMAND_TYPE };



	ForceNextTickValueCommand::ForceNextTickValueCommand() :
		NetworkMessage(NetworkMessageReliabilityType::Reliable)
	{
		// Nothing here
	}



	ForceNextTickValueCommand::~ForceNextTickValueCommand() = default;



	void ForceNextTickValueCommand::Execute(IGameClientSimulation& simulation)
	{
		simulation.ForceNextTickValue(m_NextTickValue);
	}



	NetworkMessageTypeId ForceNextTickValueCommand::GetTypeId() const
	{
		return TypeId;
	}



	void ForceNextTickValueCommand::SetNextTickValue(unsigned int nextTick)
	{
		m_NextTickValue = nextTick;
	}



	unsigned int ForceNextTickValueCommand::GetNextTickValue() const
	{
		return m_NextTickValue;
	}



	bool ForceNextTickValueCommand::DoSerialize(INetworkSerializer& serializer)
	{
		auto typeId = uint32_t(TypeId.value);

		bool success = serializer.SerializeUInt32(typeId, TypeIdBitsSize);
		if (success) success = serializer.SerializeUInt32(m_NextTickValue);

		return success;
	}
}

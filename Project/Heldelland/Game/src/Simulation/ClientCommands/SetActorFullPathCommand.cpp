//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/ClientCommands/SetActorFullPathCommand.h"

#include<Nest/NestAssert.h>
#include<Nest/Exceptions/ValueOutOfRangeException.h>

#include"Game/Pathfinding/Path.h"
#include"Game/Simulation/CommandsTypes.h"
#include"Game/Simulation/IGameClientSimulation.h"
#include"Game/SimulationCommons/SimulationActorMovementSystem.h"

#include"Game/Network/INetworkSerializer.h"



namespace Game
{
	const NetworkMessageTypeId SetActorFullPathCommand::TypeId{ SET_ACTOR_FULL_PATH_COMMAND_TYPE };



	SetActorFullPathCommand::SetActorFullPathCommand() :
		NetworkMessage(NetworkMessageReliabilityType::Reliable)
	{
	}



	SetActorFullPathCommand::~SetActorFullPathCommand() = default;



	void SetActorFullPathCommand::Execute(IGameClientSimulation& simulation)
	{
		const auto simulationActorMovementSystem = simulation.GetSimulationActorMovementSystem();
		NestAssert(simulationActorMovementSystem != nullptr);

		std::shared_ptr<Path> path = std::make_shared<Path>();
		for (const auto& m_Waypoint : m_Waypoints)
		{
			path->PushBack(m_Waypoint);
		}
		path->RecalculateDistances();

		simulationActorMovementSystem->SetActorPath(m_ActorNetworkId, path);
	}



	NetworkMessageTypeId SetActorFullPathCommand::GetTypeId() const
	{
		return TypeId;
	}



	void SetActorFullPathCommand::SetActorNetworkId(ActorNetworkId actorNetworkId)
	{
		m_ActorNetworkId = actorNetworkId;
	}



	void SetActorFullPathCommand::SetPath(const Path& path)
	{
		m_Waypoints.clear();

		for (int i = 0; i < path.GetSize(); ++i)
		{
			m_Waypoints.emplace_back(path.GetPositionByIndex(i));
		}
	}



	ActorNetworkId SetActorFullPathCommand::GetActorNetworkId() const
	{
		return m_ActorNetworkId;
	}



	int SetActorFullPathCommand::GetPathSize() const
	{
		return int(m_Waypoints.size());
	}



	Nest::Vector2<simfix64> SetActorFullPathCommand::GetWaypointAtIndex(int index) const
	{
		if (index < 0 || index >= int(m_Waypoints.size()))
			ThrowValueOutOfRangeExceptionWithLine("Waypoint index is out of range");

		return m_Waypoints[index];
	}



	bool SetActorFullPathCommand::DoSerialize(INetworkSerializer& serializer)
	{
		auto typeId = uint32_t(TypeId.value);
		int pathSize = int(m_Waypoints.size());

		bool failure = false;
		failure |= !serializer.SerializeUInt32(typeId, TypeIdBitsSize);
		failure |= !serializer.SerializeUInt16(m_ActorNetworkId.value);
		failure |= !serializer.SerializeInt32(pathSize);

		if (failure)
			throw std::runtime_error("Failed to serialize/deserialize SetActorFullPathCommand main data");

		for (int i = 0; i < pathSize; ++i)
		{
			if (m_Waypoints.size() == static_cast<decltype(m_Waypoints.size())>(i))
				m_Waypoints.emplace_back(Nest::Vector2<simfix64>{});

			failure |= !serializer.SerializeInt64(m_Waypoints[i].x.rawValue);
			failure |= !serializer.SerializeInt64(m_Waypoints[i].y.rawValue);
		}

		if (failure)
			throw std::runtime_error("Failed to serialize/deserialize SetActorFullPathCommand path information");

		return !failure;
	}
}

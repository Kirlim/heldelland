
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Game/Simulation/ServerCommands/PlayerMoveCommand.h"

#include"Game/Simulation/ServerGameSimulation.h"
#include"Game/Network/INetworkSerializer.h"
#include"Game/Network/IHost.h"
#include"Game/Network/HostGameInformation.h"

#include"Game/Simulation/CommandsTypes.h"



namespace Game
{
	const NetworkMessageTypeId PlayerMoveCommand::TypeId{ PLAYER_MOVE_COMMAND_TYPE };



	PlayerMoveCommand::PlayerMoveCommand()
		: ServerSimulationCommand(), NetworkMessage(NetworkMessageReliabilityType::Reliable)
	{
		// Nothing here
	}



	PlayerMoveCommand::~PlayerMoveCommand() = default;



	void PlayerMoveCommand::Execute(ServerGameSimulation& simulation)
	{
		const auto hostInformation = this->GetRemoteHost();
		const auto hostGameInformation = hostInformation->GetGameInformation();
		const auto playerId = hostGameInformation->playerId;

		simulation.RegisterPlayerMovementRequest(playerId, this->GetDestination());
	}



	NetworkMessageTypeId PlayerMoveCommand::GetTypeId() const
	{
		return TypeId;
	}



	void PlayerMoveCommand::SetDestination(Nest::Vector2<simfix64> destination)
	{
		m_Destination = destination;
	}



	Nest::Vector2<simfix64> PlayerMoveCommand::GetDestination() const
	{
		return m_Destination;
	}



	bool PlayerMoveCommand::DoSerialize(INetworkSerializer& serializer)
	{
		auto typeId = uint32_t(TypeId.value);

		bool success = serializer.SerializeUInt32(typeId, TypeIdBitsSize);
		if (success) success = serializer.SerializeInt64(m_Destination.x.rawValue);
		if (success) success = serializer.SerializeInt64(m_Destination.y.rawValue);

		return success;
	}
}
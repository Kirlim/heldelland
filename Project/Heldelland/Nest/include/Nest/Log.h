
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__LOG_H
#define NEST__LOG_H

#include<string>
#include<functional>



namespace Nest
{
    namespace Log
    {
        // Wrapper to call LogMessageFunction, if defined
        void Message(std::string message);

        // Wrapper to call LogWarningFunction, if defined
        void Warning(std::string message);

        // Wrapper to call LogErrorFunction, if defined
        void Error(std::string message);



        void SetLogMessageFunction(std::function<void(std::string)> function);
        void SetLogWarningFunction(std::function<void(std::string)> function);
        void SetLogErrorFunction(std::function<void(std::string)> function);
        


        extern std::function<void(std::string)> LogMessageFunction;
        extern std::function<void(std::string)> LogWarningFunction;
        extern std::function<void(std::string)> LogErrorFunction;
    }
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__ENTITY_ID_H
#define NEST__ENTITY_ID_H

#include"Nest/IdType.h"



namespace Nest
{
	class IEntity;

	template <typename IdTypeTraits>
	struct IdType;

	struct EntityIdTraits
	{
		using underlying_type = unsigned int;
	};



	extern template struct IdType<EntityIdTraits>;
	using EntityId = IdType<EntityIdTraits>;
}



#endif

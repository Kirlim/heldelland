//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__COMPONENT_H
#define NEST__COMPONENT_H



#include<string>

#include"Nest/Exceptions/LogicErrorException.h"
#include"Nest/ComponentId.h"



namespace Nest
{
	// Forward declarations
	class IEntity;
	class EntitiesAndComponentsRemap;
	class ComponentReferenceBase;



	class Component
	{
		friend class IEntity;

	public:
		Component();
		Component(const Component&) = delete;
		Component(Component&&) = default;

		virtual ~Component();

		virtual Component& operator=(const Component&) = delete;
		virtual Component& operator=(Component&&) = default;


		// Returns the name of the component type (for the name of the
		// component itself, in the entity, see GetName and SetName
		// methods). This should be unique per component type.
		static std::string GetTypeName() { return ("Component"); }

		// Sets the component name (not the type name).
		// This should be unique per entity.
		void SetName(std::string name);

		// Returns the name of this component (not the type name).
		std::string GetName() const;

		IEntity* GetOwner() const;
		ComponentId GetId() const;

		// This method calls RemoveComponent in the owning entity.
		void Destroy();

		// TODO : if possible, remove this method and pass the entity by constructor. However, how would it work with the storing factory?
		void SetOwner(IEntity* owner);

		virtual void CopyFrom(Component& other, EntitiesAndComponentsRemap* mappings = nullptr)
		{
			ThrowLogicErrorExceptionWithLine("CopyFrom not implemented for this component, or calling base CopyFrom accidentaly.");
		}



	public:

		// Flags whether the component is to be considered active or not. Has no
		// effects if the accessing system ignores this flag.
		bool enabled;



	protected:
		std::string m_Name;
		IEntity* m_Owner;
		ComponentId m_Id;

		virtual void OnCreate()
		{
		};

		virtual void OnDestroy()
		{
		};

		static ComponentId m_NextId;
		static ComponentId GetNewId();
	};
}



#endif

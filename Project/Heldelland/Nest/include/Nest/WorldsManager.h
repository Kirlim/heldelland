
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__WORLDS_MANAGER_H
#define NEST__WORLDS_MANAGER_H

#include<memory>
#include<vector>



namespace Nest
{
	struct WorldId
	{
		unsigned int id;
	};

	class World;



	class WorldsManager
	{
		struct WorldInformation;
		using WorldIterator = std::vector<WorldInformation>::const_iterator;
	public:
		void AddWorld(WorldId worldId, std::unique_ptr<World>& world);
		void RemoveWorld(WorldId worldId);
		World* GetWorld(WorldId worldId) const;

		// TODO : use appropriate iterator that returns World& (or const World&), not WorldInformation
		WorldIterator begin() const;
		WorldIterator end() const;



	private:
		struct WorldInformation
		{
			WorldId worldId;
			std::unique_ptr<World> world;
		};

		std::vector<WorldInformation> m_Worlds;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once



namespace Nest
{
    template <typename NumberType>
    struct SVector3
    {
        NumberType x;
        NumberType y;
        NumberType z;



        SVector3()
        {
            x = 0;
            y = 0;
            z = 0;
        }



        SVector3(NumberType x, NumberType y, NumberType z)
        {
            this->x = x;
            this->y = y;
            this->z = z;
        }
    };



    template <typename NumberType>
    NumberType dot(const SVector3<NumberType>& left, const SVector3<NumberType>& right)
    {
        return(
            (left.x * right.x) +
            (left.y * right.y) +
            (left.z * right.z)
            );
    }
}
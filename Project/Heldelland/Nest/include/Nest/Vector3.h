//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__VECTOR3_H
#define NEST__VECTOR3_H



namespace Nest
{
	template <class NumberType>
	class Vector3
	{
	public:
		Vector3() = default;
		Vector3(const Vector3&) = default;
		Vector3(Vector3&&) = default;
		Vector3(NumberType x, NumberType y, NumberType z);
		Vector3(NumberType values[3]);

		~Vector3() = default;

		Vector3& operator =(const Vector3&) = default;
		Vector3& operator =(Vector3&&) = default;


		void Set(NumberType x, NumberType y, NumberType z);
		void Set(NumberType values[3]);

		Vector3& operator +=(const Vector3& other);
		Vector3& operator -=(const Vector3& other);
		Vector3& operator *=(const NumberType& value);

		Vector3 operator +(const Vector3& other) const;
		Vector3 operator -(const Vector3& other) const;
		Vector3 operator *(const NumberType& value) const;

		void Normalize();

		NumberType x{ 0 };
		NumberType y{ 0 };
		NumberType z{ 0 };
	};



	// Extra methods
	template <typename NumberType>
	Vector3<NumberType> Cross(const Vector3<NumberType>& u, const Vector3<NumberType>& v) // TODO : unit testing
	{
		Vector3<NumberType> result(u.y * v.z - u.z * v.y, u.z * v.x - u.x * v.z, u.x * v.y - u.y * v.x);
		return (result);
	}



	template <typename NumberType>
	NumberType Dot(const Vector3<NumberType>& u, const Vector3<NumberType>& v) // TODO : unit testing
	{
		return (u.x * v.x + u.y * v.y + u.z * v.z);
	}



	template <typename NumberType>
	Vector3<NumberType> operator *(const NumberType& value, const Vector3<NumberType>& vec) // TODO : unit testing
	{
		Vector3<NumberType> result(value * vec.x, value * vec.y, value * vec.z);
		return (result);
	}



	template <typename NumberType>
	bool operator==(const Vector3<NumberType>& left, const Vector3<NumberType>& right) // TODO : unit testing
	{
		return (left.x == right.x && left.y == right.y && left.z == right.z);
	}



	// Class methods

	template <class NumberType>
	Vector3<NumberType>::Vector3(NumberType x, NumberType y, NumberType z) :
		x(x), y(y), z(z)
	{
	}



	template <class NumberType>
	Vector3<NumberType>::Vector3(NumberType values[3])
	{
		x = values[0];
		y = values[1];
		z = values[2];
	}



	template <class NumberType>
	void Vector3<NumberType>::Set(NumberType x, NumberType y, NumberType z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}



	template <class NumberType>
	void Vector3<NumberType>::Set(NumberType values[3])
	{
		this->x = values[0];
		this->y = values[1];
		this->z = values[2];
	}



	template <class NumberType>
	Vector3<NumberType>& Vector3<NumberType>::operator +=(const Vector3<NumberType>& other)
	{
		this->x += other.x;
		this->y += other.y;
		this->z += other.z;

		return (*this);
	}

	template <class NumberType>
	Vector3<NumberType>& Vector3<NumberType>::operator -=(const Vector3<NumberType>& other)
	{
		this->x -= other.x;
		this->y -= other.y;
		this->z -= other.z;

		return (*this);
	}

	template <class NumberType>
	Vector3<NumberType>& Vector3<NumberType>::operator *=(const NumberType& value)
	{
		this->x *= value;
		this->y *= value;
		this->z *= value;

		return (*this);
	}



	template <class NumberType>
	Vector3<NumberType> Vector3<NumberType>::operator +(const Vector3& other) const
	{
		Vector3<NumberType> v(*this);
		v += other;
		return (v);
	}

	template <class NumberType>
	Vector3<NumberType> Vector3<NumberType>::operator -(const Vector3& other) const
	{
		Vector3<NumberType> v(*this);
		v -= other;
		return (v);
	}

	template <class NumberType>
	Vector3<NumberType> Vector3<NumberType>::operator *(const NumberType& value) const // TODO : unit testing
	{
		Vector3<NumberType> v(*this);
		v *= value;
		return (v);
	}



	template <typename NumberType>
	void Vector3<NumberType>::Normalize()
	{
		NumberType factor = sqrt(x * x + y * y + z * z);
		x = x / factor;
		y = y / factor;
		z = z / factor;
	}



	// Common uses
	using Vector3f = Vector3<float>;
}



#endif

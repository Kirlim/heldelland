//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__MATH_H
#define NEST__MATH_H

#define NEST_PI 3.14159265358979323846f



namespace Nest
{
	// Forward declarations
	template <typename NumberType>
	class Vector2;
	template <typename NumberType>
	class Vector3;



	namespace Math
	{
		// Calculates the area of a 2D triangle using 3D cross product, leaning on z axis.
		// It is assumed a right-handed system (x right, y up, z coming out of screen),
		// and a negative area is possible, as it helps with detecting turn directions.
		// The vectors used are
		// a->b
		// a->c
		// Positive means counter-clockwise rotation.
		template <typename NumberType>
		NumberType GetTriangleArea(const Vector2<NumberType>& a, const Vector2<NumberType>& b, const Vector2<NumberType>& c)
		{
			const NumberType ax = b.x - a.x;
			const NumberType ay = b.y - a.y;
			const NumberType bx = c.x - a.x;
			const NumberType by = c.y - a.y;

			return ax * by - ay * bx;
		}




		template <typename NumberType>
		Vector3<NumberType> GetNormalizedNormal(const Vector3<NumberType>& vertex, const Vector3<NumberType>& leftPoint, const Vector3<NumberType>& rightPoint)
		{
			auto vl = leftPoint - vertex;
			auto vr = rightPoint - vertex;
			auto result = Cross(vl, vr);
			result.Normalize();

			return (result);
		}



		// Executes a raycast against a triangle. This function uses the 
		// Moller-Trumbore algorithm.
		// Ray is assumed to be normalized.
		template <typename NumberType>
		bool RayTriIntersect(
			const Vector3<NumberType>& rayOrigin,
			const Vector3<NumberType>& rayDirection,
			const Vector3<NumberType>& v1, const Vector3<NumberType>& v2, const Vector3<NumberType>& v3,
			const NumberType EPILSON,
			NumberType& outDistance
		)
		{
			Vector3<NumberType> edge1, edge2;
			Vector3<NumberType> P, Q, T;
			NumberType det, invDet, u, v;
			NumberType t;

			// Find vectors for two edges sharing V1
			edge1 = v2 - v1;
			edge2 = v3 - v1;

			// Begin calculating determinant, also used to calculate u parameter
			P = Cross(rayDirection, edge2);

			// If determinant is near zero, ray lies in the plane of the triangle
			det = Dot(edge1, P);

			// NOT CULLING
			if (det > -EPILSON && det < EPILSON) return (false);
			invDet = NumberType(1) / det;

			// Calculate the distance from v1 to ray origin
			T = rayOrigin - v1;

			// Calculate the u parameter and test
			u = Dot(T, P) * invDet;

			// The intersection lies outside of the triangle
			if (u < NumberType(0) || u > NumberType(1)) return (false);

			// Prepare to test the v parameter
			Q = Cross(T, edge1);

			// Calculate the V parameter and test
			v = Dot(rayDirection, Q) * invDet;

			// The intersection lies outside of the triangle
			if (v < NumberType(0) || u + v > NumberType(1)) return 0;


			t = Dot(edge2, Q) * invDet;

			if (t > EPILSON)
			{
				// Ray intersects
				outDistance = t;
				return (true);
			}
			else
			{
				return (false);
			}
		}
	}
}



#endif

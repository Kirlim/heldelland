
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__I_SINGLE_COMPONENTS_CONTAINER_H
#define NEST__I_SINGLE_COMPONENTS_CONTAINER_H

#include<memory>
#include<functional>



namespace Nest
{
	struct ComponentAddedEvent;
	struct ComponentDestroyedEvent;

	class EventsBusToken;



	class ISingleComponentsContainer
	{
	public:
		virtual ~ISingleComponentsContainer();

		int GetElementsCount() const;

		std::unique_ptr<EventsBusToken> RegisterOnComponentAddedListener(
			std::function<void(const ComponentAddedEvent&)>
		);
		std::unique_ptr<EventsBusToken> RegisterOnComponentDestroyedListener(
			std::function<void(const ComponentDestroyedEvent&)>
		);



	private:
		virtual int DoGetElementsCount() const = 0;
		virtual std::unique_ptr<EventsBusToken> DoRegisterOnComponentAddedListener(
			std::function<void(const ComponentAddedEvent&)> listener
		) = 0;
		virtual std::unique_ptr<EventsBusToken> DoRegisterOnComponentDestroyedListener(
			std::function<void(const ComponentDestroyedEvent&)> listener
		) = 0;
	};
}



#endif

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__TEXT_MESH_IMPL_OPENGL_H
#define NEST__TEXT_MESH_IMPL_OPENGL_H

#include"Nest/TextMeshImpl.h"

#include<GL/glew.h>



namespace Nest
{
	// Forward declarations
	class Font;
	class Material;



	class TextMeshImplOpenGL : public TextMeshImpl
	{
	public:
		TextMeshImplOpenGL();
		virtual ~TextMeshImplOpenGL();

		void RenderText(std::string text, const Font* font, float fontSize) override final;



	protected:
		GLuint m_VBO; // Vertex Buffer Object
		GLuint m_VAO; // Vertex Array Object
	};
}



#endif

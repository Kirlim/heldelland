
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<functional>
#include<memory>
#include<string>

#include"Nest/BasicComponentsFactory.h"



namespace Nest
{
    // Forward declarations
    class ComponentOwnerReferenceBase;

	
	// TODO : delete this class if it is not needed
    class PrefabComponentsFactory : public BasicComponentsFactory
    {
    public:
        PrefabComponentsFactory(const unsigned int typeId, const std::string typeName, const std::function<std::unique_ptr<ComponentOwnerReferenceBase>()> factoryFunction);
        virtual ~PrefabComponentsFactory();
        virtual std::string GetTypeName() const override final;
        virtual unsigned int GetTypeId() const override final;
        virtual std::unique_ptr<ComponentOwnerReferenceBase> Instantiate() override final;



    protected:
        const std::function<std::unique_ptr<ComponentOwnerReferenceBase>()> m_FactoryFunction;
        const unsigned int m_TypeId;
        const std::string m_TypeName;
    };
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include"Nest/ITemplatedSingleComponentsContainerIterator.h"
#include"Nest/StoringFactory.h"



namespace Nest
{
    template <typename ComponentType>
    class StoringFactoryComponentsContainerIteratorImpl : public ITemplatedSingleComponentsContainerIteratorImplBase<ComponentType>
    {
    public:
        StoringFactoryComponentsContainerIteratorImpl(StoringFactoryIterator<ComponentType> iterator);
        virtual ~StoringFactoryComponentsContainerIteratorImpl();

        virtual ComponentType& operator *() const override;
        virtual const ITemplatedSingleComponentsContainerIteratorImplBase<ComponentType>& operator ++() override;
        virtual bool operator !=(const ITemplatedSingleComponentsContainerIteratorImplBase<ComponentType>& other) const override;

        virtual std::unique_ptr<ITemplatedSingleComponentsContainerIteratorImplBase<ComponentType>> CloneUnique() const override;


    protected:
        StoringFactoryIterator<ComponentType> m_Iterator;
    };



    template <typename ComponentType>
    StoringFactoryComponentsContainerIteratorImpl<ComponentType>::StoringFactoryComponentsContainerIteratorImpl(StoringFactoryIterator<ComponentType> iterator) :
        ITemplatedSingleComponentsContainerIteratorImplBase<ComponentType>(),
        m_Iterator(iterator)
    {
        // Nothing here
    }



    template <typename ComponentType>
    StoringFactoryComponentsContainerIteratorImpl<ComponentType>::~StoringFactoryComponentsContainerIteratorImpl()
    {
        // Nothing here
    }



    template <typename ComponentType>
    ComponentType& StoringFactoryComponentsContainerIteratorImpl<ComponentType>::operator *() const 
    {
        return(*m_Iterator);
    }



    template <typename ComponentType>
    const ITemplatedSingleComponentsContainerIteratorImplBase<ComponentType>& StoringFactoryComponentsContainerIteratorImpl<ComponentType>::operator ++()
    {
        ++m_Iterator;
        return(*this);
    }



    template <typename ComponentType>
    bool StoringFactoryComponentsContainerIteratorImpl<ComponentType>::operator !=(const ITemplatedSingleComponentsContainerIteratorImplBase<ComponentType>& other) const
    {
        // TODO : dynamic cast on debug, static cast on release
        auto castedOther = dynamic_cast<StoringFactoryComponentsContainerIteratorImpl&>(
            const_cast<ITemplatedSingleComponentsContainerIteratorImplBase<ComponentType>&>(other)
            );
        return(m_Iterator != castedOther.m_Iterator);
    }



    template <typename ComponentType>
    std::unique_ptr<ITemplatedSingleComponentsContainerIteratorImplBase<ComponentType>> StoringFactoryComponentsContainerIteratorImpl<ComponentType>::CloneUnique() const
    {
        // TODO : verify if iterator is not null?
        return(std::make_unique<StoringFactoryComponentsContainerIteratorImpl<ComponentType>>(m_Iterator));
    }
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__COMPONENT_CONTAINER_EVENTS_H
#define NEST__COMPONENT_CONTAINER_EVENTS_H

#include"Nest/IEntity.h"
#include"Nest/ComponentId.h"



namespace Nest
{
	struct ComponentAddedEvent
	{
		ComponentId componentId;
	};

	struct ComponentDestroyedEvent
	{
		ComponentId componentId;
	};
}



#endif

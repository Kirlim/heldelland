
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once



namespace Nest
{
    template <typename NumberType>
    struct SVector2
    {
        NumberType x;
        NumberType y;



        SVector2()
        {
            x = 0;
            y = 0;
        }



        SVector2(NumberType x, NumberType y)
        {
            this->x = x;
            this->y = y;
        }
    };
}
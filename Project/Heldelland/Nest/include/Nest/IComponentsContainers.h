
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__I_COMPONENTS_CONTAINERS_H
#define NEST__I_COMPONENTS_CONTAINERS_H

#include<memory>

#include"Nest/ComponentTypeMap.h"



namespace Nest
{
	// Forward declarations
	class ISingleComponentsContainer;
	template <typename ComponentType> class ITemplatedSingleComponentsContainer;




	// Implementations of managers of (many) components containers for a World
	// should implement this interface. There are two types of containers:
	// Single Containers : containers of only a type of component. Technically, 
	//                     these will be directly tied to a factory
	class IComponentsContainers
	{
	public:
		virtual ~IComponentsContainers();

		// Register a container within this instance.
		bool RegisterSingleContainer(const unsigned int typeId, std::unique_ptr<ISingleComponentsContainer> container);

		// Returns an unsafe pointer to a registered container.
		// Unsafe because the use is not supposed to outlive the life of
		// the container.
		ISingleComponentsContainer* GetSingleContainer(const unsigned int typeId) const;


		// TODO : move template method to outside the class, as a helper.
		// Returns a strong typed single container, with its specific templated type
		template <typename ComponentType>
		ITemplatedSingleComponentsContainer<ComponentType>* GetSingleContainer() const
		{
			auto typeId = ComponentTypeMap::GetInstance()->GetTypeId<ComponentType>();

			auto container = GetSingleContainer(typeId);
			ITemplatedSingleComponentsContainer<ComponentType>* result = nullptr;

			if (container != nullptr)
			{
				result = dynamic_cast<ITemplatedSingleComponentsContainer<ComponentType>*>(container);

				if (result == nullptr)
				{
					// TODO : log error, maybe do something?
					throw - 1;
				}
			}

			// TODO : error if result is null, and create new method HasContainer()
			return(result);
		}
		



	protected:
		// Implement this method to add the container. The caller will not validate if
		// the typeId is valid, but will make sure that the container is not nullptr.
		virtual bool DoRegisterSingleContainer(const unsigned int typeId, std::unique_ptr<ISingleComponentsContainer> container) = 0;

		virtual ISingleComponentsContainer* DoGetSingleContainer(const unsigned int typeId) const = 0;
	};
}



#endif

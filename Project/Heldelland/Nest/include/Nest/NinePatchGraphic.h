
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__NINE_PATCH_GRAPHIC_H
#define NEST__NINE_PATCH_GRAPHIC_H

#include<string>
#include<memory>

#include"Nest/Vector2.h"
#include"Nest/Vector3.h"
#include"Nest/Component.h"



namespace Nest
{
	// Forward declarations
	class IEntity;
	class Material;



	class NinePatchGraphic : public Component
	{
	public:
		NinePatchGraphic();
		NinePatchGraphic(const NinePatchGraphic&) = delete;
		NinePatchGraphic(NinePatchGraphic&&) = default;

		~NinePatchGraphic() override;

		NinePatchGraphic& operator =(const NinePatchGraphic&) = delete;
		NinePatchGraphic& operator =(NinePatchGraphic&&) = default;


		static std::string GetTypeName() { return("NinePatchGraphic"); }

		// Defines the size of each side and corner of the surrounding frame of the
		// graphic. Sizes and widths are defined as "distances from the main body", meaning
		// that bigger sizes are more distant to the body of the graphic.
		void SetFrameSizes(
			Vector2f topLeftSize, float topWidth, Vector2f topRightSize,
			float leftWidth, float rightWidth,
			Vector2f bottomLeftSize, float bottomWidth, Vector2f bottomRightSize
		);

		void SetBodySize(Vector2f size);
		void SetBodySize(float width, float height);

		// Sets the materials to be used by the graphic. Being raw pointers, it is up to
		// the programmer to make sure that the materials outlive the graphic.
		void SetMaterials(
			Material* topLeftMaterial, Material* topMaterial, Material* topRightMaterial,
			Material* leftMaterial, Material* bodyMaterial, Material* rightMaterial,
			Material* bottomLeftMaterial, Material* bottomMaterial, Material* bottomRightMaterial
		);

		void SetPosition(Vector3f position) const;
		void SetPosition(float x, float y, float z) const;

		void SetCenter(Vector3f center);
		void SetCenter(float x, float y, float z);





	protected:
		using EntityReference = std::weak_ptr<IEntity>;

		void OnCreate() override final;
		void OnDestroy() override final;

		void SetUpGraphic();
		void SetUpGraphicAddComponents() const;
		void SetUpGraphicEntities();
		void SetUpGraphicTransforms() const;
		void SetUpGraphicMeshes() const;
		void SetUpGraphicRenderers() const;

		void UpdateEntityMeshScale(EntityReference entityRef, Vector2f scale) const;
		void UpdateEntityMaterial(EntityReference entityRef, Material* material) const;
		void RefreshEntityPosition(EntityReference entityRef, Vector3f position) const;

		void RefreshAllPositions() const;
		void RefreshAllMaterials() const;
		void RefreshAllMeshes() const;



	protected:
		Vector3f m_Center;
		Vector2f m_BodySize;

		// Weak pointers due to default copy and move operations
		EntityReference m_TopLeft;
		EntityReference m_Top;
		EntityReference m_TopRight;
		EntityReference m_Left;
		EntityReference m_Middle;
		EntityReference m_Right;
		EntityReference m_BottomLeft;
		EntityReference m_Bottom;
		EntityReference m_BottomRight;

		Material* m_MatTopLeft = nullptr;
		Material* m_MatTop = nullptr;
		Material* m_MatTopRight = nullptr;
		Material* m_MatLeft = nullptr;
		Material* m_MatMiddle = nullptr;
		Material* m_MatRight = nullptr;
		Material* m_MatBottomLeft = nullptr;
		Material* m_MatBottom = nullptr;
		Material* m_MatBottomRight = nullptr;

		// With hierarchical transforms, these can probably be removed
		Vector2f m_TopLeftSize;
		float m_TopWidth = 0;
		Vector2f m_TopRightSize;
		float m_LeftWidth = 0;
		float m_RightWidth = 0;
		Vector2f m_BottomLeftSize;
		float m_BottomWidth = 0;
		Vector2f m_BottomRightSize;
	};
}



#endif

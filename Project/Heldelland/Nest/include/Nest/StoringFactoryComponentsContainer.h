
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__STORING_FACTORY_COMPONENTS_CONTAINER_H
#define NEST__STORING_FACTORY_COMPONENTS_CONTAINER_H

#include<memory>
#include<functional>

#include"Nest/NestAssert.h"

#include"Nest/ComponentContainerEvents.h"
#include"Nest/StoringFactory.h"
#include"Nest/ITemplatedSingleComponentsContainer.h"
#include"Nest/StoringFactoryComponentsContainerIteratorImpl.h"
#include"Nest/Events/EventsBus.h"



namespace Nest
{
	// Forward declarations
	template <typename ComponentType> class StoringFactory;



	template <typename ComponentType>
	class StoringFactoryComponentsContainer : public ITemplatedSingleComponentsContainer<ComponentType>
	{
	public:
		StoringFactoryComponentsContainer(StoringFactory<ComponentType>* storingFactory);
		virtual ~StoringFactoryComponentsContainer();

		auto TemplatedBegin() const->ITemplatedSingleComponentsContainerIterator<ComponentType> override;
		auto TemplatedEnd() const->ITemplatedSingleComponentsContainerIterator<ComponentType> override;



	private:
		int DoGetElementsCount() const override;

		std::unique_ptr<EventsBusToken> DoRegisterOnComponentAddedListener(
			std::function<void(const ComponentAddedEvent&)> listener
		) override;
		std::unique_ptr<EventsBusToken> DoRegisterOnComponentDestroyedListener(
			std::function<void(const ComponentDestroyedEvent&)> listener
		) override;

		void OnComponentAddedOnFactory(const ComponentType& component) const;
		void OnComponentDeletedOnFactory(const ComponentType& component) const;



	private:
		StoringFactory<ComponentType>* m_StoringFactory;
		Nest::EventsBus<ComponentAddedEvent> m_ComponentAddedEventsBus;
		Nest::EventsBus<ComponentDestroyedEvent> m_ComponentDestroyedEventBus;

		std::unique_ptr<EventsBusToken> m_ComponentAddedOnFactoryToken;
		std::unique_ptr<EventsBusToken> m_ComponentDeletedOnFactoryToken;
	};



	template <typename ComponentType>
	StoringFactoryComponentsContainer<ComponentType>::StoringFactoryComponentsContainer(StoringFactory<ComponentType>* storingFactory) :
		ITemplatedSingleComponentsContainer<ComponentType>(),
		m_StoringFactory(storingFactory)
	{
		m_ComponentAddedOnFactoryToken =
			m_StoringFactory->RegisterOnDataAddedListener([&](const ComponentType& component) {OnComponentAddedOnFactory(component);});
		
		m_ComponentDeletedOnFactoryToken =
			m_StoringFactory->RegisterOnDataDeletedListener([&](const ComponentType& component) {OnComponentDeletedOnFactory(component);});
	}



	template <typename ComponentType>
	StoringFactoryComponentsContainer<ComponentType>::~StoringFactoryComponentsContainer()
	{
		// Nothing here
	}



	template <typename ComponentType>
	int StoringFactoryComponentsContainer<ComponentType>::DoGetElementsCount() const
	{
		NestAssert(m_StoringFactory != nullptr);
		return(m_StoringFactory->ElementsCount());
	}



	template <typename ComponentType>
	std::unique_ptr<EventsBusToken> StoringFactoryComponentsContainer<ComponentType>::DoRegisterOnComponentAddedListener(
		std::function<void(const ComponentAddedEvent&)> listener
	)
	{
		return m_ComponentAddedEventsBus.Register(listener);
	}



	template <typename ComponentType>
	std::unique_ptr<EventsBusToken> StoringFactoryComponentsContainer<ComponentType>::DoRegisterOnComponentDestroyedListener(
		std::function<void(const ComponentDestroyedEvent&)> listener
	)
	{
		return m_ComponentDestroyedEventBus.Register(listener);
	}



	template <typename ComponentType>
	void StoringFactoryComponentsContainer<ComponentType>::OnComponentAddedOnFactory(const ComponentType& component) const
	{
		ComponentAddedEvent evt;
		evt.componentId = component.GetId();

		m_ComponentAddedEventsBus.Signal(evt);
	}



	template <typename ComponentType>
	void StoringFactoryComponentsContainer<ComponentType>::OnComponentDeletedOnFactory(const ComponentType& component) const
	{
		ComponentDestroyedEvent evt;
		evt.componentId = component.GetId();

		m_ComponentDestroyedEventBus.Signal(evt);
	}



	template <typename ComponentType>
	auto StoringFactoryComponentsContainer<ComponentType>::TemplatedBegin() const->ITemplatedSingleComponentsContainerIterator<ComponentType>
	{
		NestAssert(m_StoringFactory != nullptr);

		auto storingFactoryIt = m_StoringFactory->begin();

		auto iteratorImpl = std::make_unique<StoringFactoryComponentsContainerIteratorImpl<ComponentType>>(storingFactoryIt);
		ITemplatedSingleComponentsContainerIterator<ComponentType> iterator(std::move(iteratorImpl));

		return(iterator);
	}



	template <typename ComponentType>
	auto StoringFactoryComponentsContainer<ComponentType>::TemplatedEnd() const->ITemplatedSingleComponentsContainerIterator<ComponentType>
	{
		NestAssert(m_StoringFactory != nullptr);

		auto storingFactoryIt = m_StoringFactory->end();

		auto iteratorImpl = std::make_unique<StoringFactoryComponentsContainerIteratorImpl<ComponentType>>(storingFactoryIt);
		ITemplatedSingleComponentsContainerIterator<ComponentType> iterator(std::move(iteratorImpl));

		return(iterator);
	}
}



#endif

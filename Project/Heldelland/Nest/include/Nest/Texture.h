
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<string>
#include<GL/glew.h>
#include<SOIL2/SOIL2.h>



namespace Nest
{
    // Forward declarations
    enum class GameRenderer;



    // Enums declarations
    enum class TextureFiltering { NearestNeighbor, Bilinear };
    enum class TextureWrapMode { Clamp, Repeat, RepeatMirrored };
    enum class TextureFormat { RGB, RGBA, Red, Green, Blue, Alpha };

    class Texture
    {
    public:
        Texture(); // Will call Texture(GameRenderer) by using Global to detect which renderer to use.
        Texture(GameRenderer gameRenderer);
        virtual ~Texture();

        // Loads a texture to memory.
        // Exceptions:
        // - FailedToLoadException: failed to load the texture file.
        void Load(std::string filename, TextureFiltering filtering, TextureFiltering mipmapFiltering, TextureWrapMode wrapMode, TextureFormat format);

        // Loads a texture from a bytes buffer.
        void Load(const unsigned char * buffer, int width, int height, TextureFiltering filtering, TextureFiltering mipmapFiltering, TextureWrapMode wrapMode, TextureFormat format);

        // Unloads the texture from memory. Texture should not be used
        // until loaded again.
        void Unload();


        TextureFiltering GetTextureFiltering() const;
        TextureFiltering GetMipmapFiltering() const;
        TextureWrapMode GetWrapMode() const;
        TextureFormat GetFormat() const;

        GLuint GetTextureId() const;



    protected:
        TextureFiltering m_InfoTextureFiltering = TextureFiltering::NearestNeighbor;
        TextureFiltering m_InfoMipmapFiltering = TextureFiltering::NearestNeighbor;
        TextureWrapMode m_InfoWrapMode = TextureWrapMode::Clamp;
        TextureFormat m_InfoTextureFormat = TextureFormat::RGB;

        GLenum m_TextureFiltering = 0;
        GLenum m_MipmapFiltering = 0;
        GLenum m_WrapMode = 0;
        GLenum m_TextureFormat = 0;

        GLuint m_TextureId = 0;

        bool m_Loaded = false;


        GLenum TranslateFiltering(const TextureFiltering filtering) const;
        GLenum TranslateWrapMode(const TextureWrapMode wrapMode) const;
        GLenum TranslateTextureFormatGL(const TextureFormat format) const;
        int TranslateTextureFormatSoil(const TextureFormat format) const;

        // TODO : delete this member variable when pimpl or inheritance is used
        GameRenderer m_GameRenderer;
    };
}

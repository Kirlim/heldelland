
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__CAST_H
#define NEST__CAST_H

#include<cassert>



// TODO : unleash ifdefs

template <typename ResultType, typename SourceType>
ResultType* DoCast(SourceType* source)
{
//#ifdef debug
    ResultType* result = dynamic_cast<ResultType*>(source);
    assert(result != nullptr);
    return result;
//#else
//    return static_cast<To>(p);
//#endif
}

template <typename ResultType, typename SourceType>
ResultType& DoCast(SourceType& source)
{
//#ifdef debug
    ResultType& result = dynamic_cast<ResultType&>(source);
    return result;
//#else
//    return static_cast<To>(p);
//#endif
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__RENDERING_SYSTEM_H
#define NEST__RENDERING_SYSTEM_H



#include"SDL.h"
#include"Nest/System.h"



namespace Nest
{
	class RenderingSystem : public System
	{
	public:
		explicit RenderingSystem(SDL_Window& sdlWindow);
		virtual ~RenderingSystem();


	protected:
		void Render(const float deltaTime);


		
	private:
		void DoUpdate(const float deltaTime) override;
		virtual void DoRender(float deltaTime) = 0;



	protected:
		SDL_Window& m_SdlWindow;
	};
}



#endif

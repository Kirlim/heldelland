//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__COMPONENT_REFERENCE_H
#define NEST__COMPONENT_REFERENCE_H



#include<cstddef>
#include<memory>

#include"Nest/Cast.h"
#include"Nest/NestAssert.h"
#include"Nest/Exceptions/NullPointerException.h"

#include"Nest/ComponentOwnerReferenceBase.h"



namespace Nest
{
	// Forward declarations
	class Component;
	class ComponentOwnerReferenceBase;



	class ComponentReferenceBase
	{
		friend class ComponentOwnerReferenceBase;
		template <typename ComponentType>
		friend class ComponentReference;

	public:
		virtual ~ComponentReferenceBase();

		// Calling reset will make this reference point to nullptr
		void Reset();

		virtual Component* Get() = 0;
		std::unique_ptr<ComponentReferenceBase> Clone();



	protected:
		void UpdateUnsafePointer(Component* pointer);

		void RegisterToOwnerReference(ComponentOwnerReferenceBase& other);
		void UnregisterFromOwnerReference();



	private:
		virtual std::unique_ptr<ComponentReferenceBase> DoClone() = 0;



	protected:
		Component* m_UnsafePointer = nullptr;
		ComponentOwnerReferenceBase* m_ComponentOwnerReference = nullptr;
	};



	template <typename ComponentType>
	class ComponentReference : public ComponentReferenceBase
	{
		template <typename CompType>
		friend class ComponentOwnerReference;
	public:
		ComponentReference();
		ComponentReference(std::nullptr_t);
		ComponentReference(ComponentOwnerReferenceBase& other);
		ComponentReference(ComponentReferenceBase& other);
		ComponentReference(const ComponentReference& other);
		ComponentReference(ComponentReference&& other);

		~ComponentReference();

		Component* Get() override;
		std::unique_ptr<ComponentReference<ComponentType>> Clone() const;

		ComponentReference& operator =(ComponentOwnerReferenceBase& other);
		ComponentReference& operator =(ComponentOwnerReferenceBase* other);
		ComponentReference& operator =(ComponentReference<ComponentType>& other);
		ComponentReference& operator =(ComponentReference<ComponentType>&& other);
		ComponentReference& operator =(std::nullptr_t nullPointer);

		bool operator ==(const ComponentOwnerReferenceBase& other) const;
		bool operator ==(const ComponentReference& other) const;
		bool operator ==(const ComponentType* const other) const;

		bool operator !=(const ComponentOwnerReferenceBase& other) const;
		bool operator !=(const ComponentReference& other) const;
		bool operator !=(const ComponentType* const other) const;

		bool operator ==(std::nullptr_t nullPointer) const;
		bool operator !=(std::nullptr_t nullPointer) const;

		ComponentType* operator->() const;
		ComponentType& operator*() const;



	private:
		std::unique_ptr<ComponentReferenceBase> DoClone() override;
	};



	template <typename ComponentType>
	ComponentReference<ComponentType>::ComponentReference() :
		ComponentReferenceBase()
	{
		// Nothing here
	}



	template <typename ComponentType>
	ComponentReference<ComponentType>::ComponentReference(std::nullptr_t) :
		ComponentOwnerReferenceBase()
	{
		// Nothing here
	}



	template <typename ComponentType>
	ComponentReference<ComponentType>::ComponentReference(ComponentOwnerReferenceBase& other) : ComponentReferenceBase()
	{
		NestAssert(dynamic_cast<ComponentType*>(other.Get()) != nullptr);
		RegisterToOwnerReference(other);
	}



	template <typename ComponentType>
	ComponentReference<ComponentType>::ComponentReference(ComponentReferenceBase& other) : ComponentReferenceBase()
	{
		NestAssert(
			other.m_ComponentOwnerReference == nullptr ||
			dynamic_cast<ComponentType*>(other.m_ComponentOwnerReference->Get()) != nullptr
		);

		if (other.m_ComponentOwnerReference != nullptr)
		{
			RegisterToOwnerReference(*other.m_ComponentOwnerReference);
		}
	}



	template <typename ComponentType>
	ComponentReference<ComponentType>::ComponentReference(const ComponentReference& other) : ComponentReferenceBase()
	{
		if (other.m_ComponentOwnerReference != nullptr)
		{
			RegisterToOwnerReference(*other.m_ComponentOwnerReference);
		}
	}



	template <typename ComponentType>
	ComponentReference<ComponentType>::ComponentReference(ComponentReference&& other) : ComponentReferenceBase()
	{
		if (other.m_ComponentOwnerReference != nullptr)
		{
			RegisterToOwnerReference(*other.m_ComponentOwnerReference);
		}
	}



	template <typename ComponentType>
	ComponentReference<ComponentType>::~ComponentReference()
	{
		// Nothing here
	}



	template <typename ComponentType>
	Component* ComponentReference<ComponentType>::Get()
	{
		// Not using dynamic or static cast since it felt like it was forcing
		// both types (the Component and ComponentType) to be included where
		// ComponentReference<T> were being declared.
		return (reinterpret_cast<Component*>(m_UnsafePointer));
	}



	template <typename ComponentType>
	std::unique_ptr<ComponentReference<ComponentType>> ComponentReference<ComponentType>::Clone() const
	{
		auto cloned = std::static_pointer_cast<ComponentReference<ComponentType>>(DoClone());
		return (std::move(cloned));
	}



	template <typename ComponentType>
	ComponentReference<ComponentType>& ComponentReference<ComponentType>::operator =(ComponentOwnerReferenceBase& other)
	{
		NestAssert(dynamic_cast<ComponentType>(other.Get()) != nullptr);

		if (m_ComponentOwnerReference != nullptr || m_UnsafePointer != nullptr)
		{
			UnregisterFromOwnerReference();
		}

		RegisterToOwnerReference(other);
	}



	template <typename ComponentType>
	ComponentReference<ComponentType>& ComponentReference<ComponentType>::operator =(ComponentOwnerReferenceBase* other)
	{
		NestAssert(other == nullptr || dynamic_cast<ComponentType>(other->Get()) != nullptr);

		if (m_ComponentOwnerReference != nullptr || m_UnsafePointer != nullptr)
		{
			UnregisterFromOwnerReference();
		}

		if (other != nullptr)
		{
			RegisterToOwnerReference(*other);
		}
	}



	template <typename ComponentType>
	ComponentReference<ComponentType>& ComponentReference<ComponentType>::operator =(ComponentReference<ComponentType>& other)
	{
		if (m_ComponentOwnerReference != nullptr || m_UnsafePointer != nullptr)
		{
			UnregisterFromOwnerReference();
		}

		if (other.m_ComponentOwnerReference != nullptr)
		{
			RegisterToOwnerReference(*other.m_ComponentOwnerReference);
		}

		return (*this);
	}



	template <typename ComponentType>
	ComponentReference<ComponentType>& ComponentReference<ComponentType>::operator =(ComponentReference<ComponentType>&& other)
	{
		if (m_ComponentOwnerReference != nullptr || m_UnsafePointer != nullptr)
		{
			UnregisterFromOwnerReference();
		}

		if (other.m_ComponentOwnerReference != nullptr)
		{
			// TODO : this move can be optimized
			RegisterToOwnerReference(*other.m_ComponentOwnerReference);
			other.UnregisterFromOwnerReference();
		}

		return (*this);
	}



	template <typename ComponentType>
	ComponentReference<ComponentType>& ComponentReference<ComponentType>::operator =(std::nullptr_t nullPointer)
	{
		if (m_ComponentOwnerReference != nullptr || m_UnsafePointer != nullptr)
		{
			UnregisterFromOwnerReference();
		}
		return (*this);
	}



	template <typename ComponentType>
	bool ComponentReference<ComponentType>::operator ==(const ComponentOwnerReferenceBase& other) const
	{
		return (this->m_UnsafePointer == other.Get());
	}



	template <typename ComponentType>
	bool ComponentReference<ComponentType>::operator ==(const ComponentReference<ComponentType>& other) const
	{
		return (this->m_UnsafePointer == other.m_UnsafePointer);
	}



	template <typename ComponentType>
	bool ComponentReference<ComponentType>::operator ==(const ComponentType* const other) const
	{
		return (this->m_UnsafePointer == static_cast<Component*>(const_cast<ComponentType*>(other)));
	}



	template <typename ComponentType>
	bool ComponentReference<ComponentType>::operator !=(const ComponentOwnerReferenceBase& other) const
	{
		return (this->m_UnsafePointer != other.Get());
	}



	template <typename ComponentType>
	bool ComponentReference<ComponentType>::operator !=(const ComponentReference& other) const
	{
		return (this->m_UnsafePointer != other.m_UnsafePointer);
	}



	template <typename ComponentType>
	bool ComponentReference<ComponentType>::operator !=(const ComponentType* const other) const
	{
		return (this->m_UnsafePointer != static_cast<Component*>(const_cast<ComponentType*>(other)));
	}



	template <typename ComponentType>
	bool ComponentReference<ComponentType>::operator ==(std::nullptr_t nullPointer) const
	{
		return (this->m_UnsafePointer == nullptr);
	}



	template <typename ComponentType>
	bool ComponentReference<ComponentType>::operator !=(std::nullptr_t nullPointer) const
	{
		return (this->m_UnsafePointer != nullptr);
	}



	template <typename ComponentType>
	ComponentType* ComponentReference<ComponentType>::operator->() const
	{
		if (m_UnsafePointer != nullptr)
		{
			return (dynamic_cast<ComponentType*>(m_UnsafePointer));
		}
		else
		{
			ThrowNullPointerExceptionWithLine("The component reference points to null address.");
		}
	}



	template <typename ComponentType>
	ComponentType& ComponentReference<ComponentType>::operator*() const
	{
		NestAssert(m_UnsafePointer != nullptr);
		return (*(DoCast<ComponentType>(m_UnsafePointer)));
	}



	template <typename ComponentType>
	std::unique_ptr<ComponentReferenceBase> ComponentReference<ComponentType>::DoClone()
	{
		auto newReference = std::make_unique<ComponentReference<ComponentType>>(*this);
		std::unique_ptr<ComponentReferenceBase> casted = std::move(newReference);
		return (std::move(casted));
	}
}



#endif

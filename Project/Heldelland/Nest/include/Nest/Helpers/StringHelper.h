
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__HELPERS__STRING_HELPER_H
#define NEST__HELPERS__STRING_HELPER_H

#include<string>
#include<vector>



namespace Nest
{
	namespace Helpers
	{
		std::vector<std::string> SplitString(const std::string &text, const char sep);

		std::vector<char32_t> Utf8ToUtf32(const char* text);
		std::vector<char32_t> Utf8ToUtf32(const std::string& text);

		bool CharUpperCompare(int left, int right);
		bool UpperCompare(const std::string left, const std::string right);
	}
}


#endif

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__COMPONENT_TYPE_MAP_H
#define NEST__COMPONENT_TYPE_MAP_H

#include<atomic>
#include<mutex>
#include<unordered_map>

#include"Nest/ComponentTypeId.h"



namespace Nest
{
	// Responsibility :
	//   Map components types to an identifier type.
	//
	// This class is a "fake singleton", meaning that the programmer
	// needs to instantiate it manually. Then, it can be accessed globally
	// by using ::. This has been chosen as acceptable as the class
	// main data is generated during compile time (the ids). Id to String
	// mapping is instance dependent and must be manually fed, but still,
	// it is supposed to be a single-time setup only. Might make unit tests
	// trickier, but overall, feels like a good trade.
	class ComponentTypeMap
	{
	public:
		explicit ComponentTypeMap(bool isSingleton = true);
		~ComponentTypeMap();

		// Forbid copying
		ComponentTypeMap(ComponentTypeMap const &) = delete;
		ComponentTypeMap& operator=(ComponentTypeMap const &) = delete;


		template <class ComponentType>
		ComponentTypeId GetTypeId()
		{
			return(GetComponentTypeId<ComponentType>());
		}



		// Returns the type id for the registered type name. If no name
		// was registered, will return 0
		// TODO : map name to id (instead of id to name)
		ComponentTypeId GetTypeId(std::string typeName) const;



	// Returns the current instance of the types map. Will
	// not instantiate it, if no instance is set.
		static ComponentTypeMap* GetInstance();



	private:
		bool m_IsSingleton;
		static std::atomic_uint m_LastTypeId;

		template <class ComponentType>
		inline static ComponentTypeId GetComponentTypeId()
		{
			static const unsigned int id = m_LastTypeId++;
			return(id);
		}

		// "Fake singleton"
		static ComponentTypeMap* m_Instance;
		static std::mutex m_InstanceMutex;

		// Key is an integer. Value is the component type name (string)
		std::unordered_map<ComponentTypeId, std::string> m_TypesMap;
	};
}



#endif

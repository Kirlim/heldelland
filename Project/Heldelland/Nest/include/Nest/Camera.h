//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__CAMERA_H
#define NEST__CAMERA_H

#include<GL/glew.h>
#include"Nest/Component.h"
#include"Nest/Matrix4.h"



namespace Nest
{
	// Forward declarations
	struct Ray;
	template <typename NumberType>
	class Vector2;
	template <typename NumberType>
	class Vector3;



	enum class ProjectionType
	{
		NotSet,
		Orthographic,
		Perspective
	};



	class Camera : public Component
	{
	public:
		Camera();
		Camera(const Camera&) = delete;
		Camera(Camera&&) = default;

		~Camera() override;

		Camera& operator =(const Camera&) = delete;
		Camera& operator =(Camera&&) = default;


		void SetAsOrthographic(float projectionWidth, float projectionHeight, float nearClipPlane, float farClipPlane);
		void SetAsPerspective(float projectionWidth, float projectionHeight, float nearClipPlane, float farClipPlane, float fovRadians);

		float GetProjectionWidth() const;
		float GetProjectionHeight() const;
		float GetNearClipPlane() const;
		float GetFarClipPlane() const;
		float GetFovRadians() const;
		ProjectionType GetProjectionType() const;

		void RecalculateMatrices();

		Matrix4<GLfloat> GetProjectionMatrix() const;
		Matrix4<GLfloat> GetViewMatrix() const;
		Matrix4<GLfloat> GetInverseProjectionMatrix() const;
		Matrix4<GLfloat> GetInverseViewMatrix() const;
		Matrix4<GLfloat> GetInverseViewProjectionMatrix() const;


		// Converts a screenpoint coordinate (pixels) to a world
		// coordinate. Will use last recalculated camera matrices.
		Vector3<float> ScreenPointToWorld(Vector2<float> screenpointPosition, float plane) const;

		// Converts a viewport coordinate (-1 to 1) to a world
		// coordinate. Will use last recalculated camera matrices.
		Vector3<float> ViewportToWorld(Vector2<float> viewportPosition, float plane) const;

		// Converts a screenpoint coordinate (pixels) to a ray of
		// unit length, with origin at the camera.
		Ray ScreenPointToRay(Vector2<float> screenpointPosition) const;



		static std::string GetTypeName() { return ("Camera"); }



	protected:
		void OnDestroy() override;



	protected:
		float m_ProjectionWidth;
		float m_ProjectionHeight;
		float m_NearClipPlane;
		float m_FarClipPlane;
		float m_FovRadians;

		ProjectionType m_ProjectionType = ProjectionType::NotSet;

		Matrix4<GLfloat> m_ProjectionMatrix;
		Matrix4<GLfloat> m_ViewMatrix;

		Matrix4<GLfloat> m_InverseProjectionMatrix;
		Matrix4<GLfloat> m_InverseViewMatrix;
		Matrix4<GLfloat> m_InverseViewProjectionMatrix;
	};
}



#endif

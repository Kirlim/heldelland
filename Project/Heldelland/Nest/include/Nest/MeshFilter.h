
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__MESH_FILTER_H
#define NEST__MESH_FILTER_H

#include<string>
#include"Nest/IEntity.h"
#include"Nest/Component.h"
#include"Nest/Mesh.h"



namespace Nest
{
    class MeshFilter : public Component
    {
    public:
        MeshFilter();
	MeshFilter(const MeshFilter&) = delete;
	MeshFilter(MeshFilter&&) = default;

        ~MeshFilter() override;

	MeshFilter& operator=(const MeshFilter&) = delete;
	MeshFilter& operator=(MeshFilter&&) = default;


        static std::string GetTypeName() { return("MeshFilter"); }

        Mesh* GetMesh() const;
        void SetMesh(Mesh* mesh);



    protected:
        Mesh* m_Mesh;

        void OnDestroy() final;
    };
}



#endif

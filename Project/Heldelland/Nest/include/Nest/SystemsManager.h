//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__SYSTEMS_MANAGER_H
#define NEST__SYSTEMS_MANAGER_H

#include<memory>
#include<functional>
#include<vector>

#include"Nest/System.h"



namespace Nest
{
	class SystemsManager
	{
	public:
		SystemsManager();
		~SystemsManager();

		// Adds a system to the managers list
		void AddSystem(std::unique_ptr<System> system);

		// Returns a system from the managers list. The system returned
		// will be the first one which comparison returns true.
		System* FindSystem(std::function<bool(System*)> comparison) const;


		// Call the FixedUpdate method of all registered systems
		void FixedUpdate(float fixedDeltaTime) const;

		// Call the Update method of all registered systems
		void Update(float deltaTime) const;



	protected:
		std::vector<std::unique_ptr<System>> m_Systems;
	};
}



#endif

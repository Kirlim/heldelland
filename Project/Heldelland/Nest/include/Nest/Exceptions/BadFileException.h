//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__BAD_FILE_EXCEPTION_H
#define NEST__EXCEPTIONS__BAD_FILE_EXCEPTION_H

#include<string>



namespace Nest
{
	// Means that a file could not be open or handled for some reason.
	class BadFileException : public std::runtime_error
	{
	public:
		explicit BadFileException(const std::string& message);
		explicit BadFileException(const char* message);
	};



#define TBFEWL_HELPER_S1(x) #x
#define TBFEWL_HELPER_S2(x) TBFEWL_HELPER_S1(x)
#define ThrowBadFileExceptionWithLine(message) \
	throw Nest::BadFileException("[" __FILE__ ":" TBFEWL_HELPER_S2(__LINE__) "] " message);
}



#endif

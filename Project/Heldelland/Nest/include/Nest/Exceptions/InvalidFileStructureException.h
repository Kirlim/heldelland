//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__INVALID_FILE_STRUCTURE_EXCEPTION_H
#define NEST__EXCEPTIONS__INVALID_FILE_STRUCTURE_EXCEPTION_H

#include<string>



namespace Nest
{
	// Means that a file exists and is being proccessed, but something
	// in its structure is invalid and thus, an operation cannot complete.
	class InvalidFileStructureException : public std::runtime_error
	{
	public:
		explicit InvalidFileStructureException(const std::string& message);
		explicit InvalidFileStructureException(const char* message);
	};



#define TIFSEWL_HELPER_S1(x) #x
#define TIFSEWL_HELPER_S2(x) TIFSEWL_HELPER_S1(x)
#define ThrowInvalidFileStructureExceptionWithLine(message) \
	throw Nest::InvalidFileStructureException("[" __FILE__ ":" TIFSEWL_HELPER_S2(__LINE__) "] " message);
}

#endif

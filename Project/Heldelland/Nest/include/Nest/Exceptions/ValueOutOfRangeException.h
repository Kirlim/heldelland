//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__VALUE_OUT_OF_RANGE_EXCEPTION_H
#define NEST__EXCEPTIONS__VALUE_OUT_OF_RANGE_EXCEPTION_H

#include<string>



namespace Nest
{
	// Means that a value received an invalid value, such as
	// a non-existing index of an array.
	class ValueOutOfRangeException : public std::logic_error
	{
	public:
		explicit ValueOutOfRangeException(const std::string& message);
		explicit ValueOutOfRangeException(const char* message);
	};



#define TVOOREWL_HELPER_S1(x) #x
#define TVOOREWL_HELPER_S2(x) TVOOREWL_HELPER_S1(x)
#define ThrowValueOutOfRangeExceptionWithLine(message) \
	throw Nest::ValueOutOfRangeException("[" __FILE__ ":" TVOOREWL_HELPER_S2(__LINE__) "] " message);
}



#endif

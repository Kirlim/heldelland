//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__NULL_POINTER_EXCEPTION_H
#define NEST__EXCEPTIONS__NULL_POINTER_EXCEPTION_H

#include<string>



namespace Nest
{
	// Means that a pointer is nullptr, when it is assumed not to be.
	class NullPointerException : public std::runtime_error
	{
	public:
		explicit NullPointerException(const std::string& message);
		explicit NullPointerException(const char* message);
	};



#define TNPEWL_HELPER_S1(x) #x
#define TNPEWL_HELPER_S2(x) TNPEWL_HELPER_S1(x)
#define ThrowNullPointerExceptionWithLine(message) \
	throw Nest::NullPointerException("[" __FILE__ ":" TNPEWL_HELPER_S2(__LINE__) "] " message);
}



#endif

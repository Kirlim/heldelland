//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__INVALID_ARGUMENT_EXCEPTION_H
#define NEST__EXCEPTIONS__INVALID_ARGUMENT_EXCEPTION_H

#include<string>



namespace Nest
{
	// Means that a parameter passed to a function or method
	// was invalid.
	class InvalidArgumentException : public std::runtime_error
	{
	public:
		explicit InvalidArgumentException(const std::string& message);
		explicit InvalidArgumentException(const char* message);
	};



#define TIAEWL_HELPER_S1(x) #x
#define TIAEWL_HELPER_S2(x) TIAEWL_HELPER_S1(x)
#define ThrowInvalidArgumentExceptionWithLine(message) \
	throw Nest::InvalidArgumentException("[" __FILE__ ":" TIAEWL_HELPER_S2(__LINE__) "] " message);
}



#endif

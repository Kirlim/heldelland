//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__ALREADY_REGISTERED_EXCEPTION_H
#define NEST__EXCEPTIONS__ALREADY_REGISTERED_EXCEPTION_H

#include<string>



namespace Nest
{
	// This exception means that something being registered into a class,
	// system, etc has already been registered - even if it is only the
	// identifier.
	class AlreadyRegisteredException : public std::logic_error
	{
	public:
		explicit AlreadyRegisteredException(const std::string& message);
		explicit AlreadyRegisteredException(const char* message);
	};



#define TAREWL_HELPER_S1(x) #x
#define TAREWL_HELPER_S2(x) TAREWL_HELPER_S1(x)
#define ThrowAlreadyRegisteredExceptionWithLine(message) \
	throw AlreadyRegisteredException("[" __FILE__ ":" TAREWL_HELPER_S2(__LINE__) "] " message);
}


#endif

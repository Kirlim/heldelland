//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__ASSERT_EXCEPTION_H
#define NEST__EXCEPTIONS__ASSERT_EXCEPTION_H

#include<string>



namespace Nest
{
	// This exception will be thrown by the NestAssert macro, when an assertion fails.
	class AssertException : public std::runtime_error
	{
	public:
		explicit AssertException(const std::string& message);
		explicit AssertException(const char* message);
	};



#define TAEWL_HELPER_S1(x) #x
#define TAEWL_HELPER_S2(x) TAEWL_HELPER_S1(x)
#define ThrowAssertExceptionWithLine(message) \
	throw AssertException("[" __FILE__ ":" TAEWL_HELPER_S2(__LINE__) "] " message);
}



#endif

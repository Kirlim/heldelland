//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__NOT_REGISTERED_EXCEPTION_H
#define NEST__EXCEPTIONS__NOT_REGISTERED_EXCEPTION_H

#include<string>



namespace Nest
{
	// This exception means that something being unregistered from a class,
	// system, etc is not currently registered - even if it is only the
	// identifier.
	class NotRegisteredException : public std::logic_error
	{
	public:
		explicit NotRegisteredException(const std::string& message);
		explicit NotRegisteredException(const char* message);
	};



#define TNREWL_HELPER_S1(x) #x
#define TNREWL_HELPER_S2(x) TNREWL_HELPER_S1(x)
#define ThrowNotRegisteredExceptionWithLine(message) \
	throw Nest::NotRegisteredException("[" __FILE__ ":" TNREWL_HELPER_S2(__LINE__) "] " message);
}


#endif

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__INVALID_DATA_EXCEPTION_H
#define NEST__EXCEPTIONS__INVALID_DATA_EXCEPTION_H

#include<string>



namespace Nest
{
	// Means that data coming from a source has invalid values,
	// to its meant purposes.
	class InvalidDataException : public std::runtime_error
	{
	public:
		explicit InvalidDataException(const std::string& message);
		explicit InvalidDataException(const char* message);
	};



#define TIDEWL_HELPER_S1(x) #x
#define TIDEWL_HELPER_S2(x) TIDEWL_HELPER_S1(x)
#define ThrowInvalidDataExceptionWithLine(message) \
	throw Nest::InvalidDataException("[" __FILE__ ":" TIDEWL_HELPER_S2(__LINE__) "] " message);
}



#endif

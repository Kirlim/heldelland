//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__LOGIC_ERROR_EXCEPTION_H
#define NEST__EXCEPTIONS__LOGIC_ERROR_EXCEPTION_H


#include<string>



namespace Nest
{
	class LogicErrorException : public std::logic_error
	{
	public:
		explicit LogicErrorException(const std::string& message);
		explicit LogicErrorException(const char* message);
	};



#define TLEEWL_HELPER_S1(x) #x
#define TLEEWL_HELPER_S2(x) TLEEWL_HELPER_S1(x)
#define ThrowLogicErrorExceptionWithLine(message) \
	throw Nest::LogicErrorException("[" __FILE__ ":" TLEEWL_HELPER_S2(__LINE__) "] " message);
}



#endif

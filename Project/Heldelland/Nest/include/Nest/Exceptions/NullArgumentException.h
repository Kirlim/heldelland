//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__NULL_ARGUMENT_EXCEPTION_H
#define NEST__EXCEPTIONS__NULL_ARGUMENT_EXCEPTION_H


#include<string>



namespace Nest
{
	// Means that a parameter passed to a function or
	// method was null, when it shouldn't be null.
	class NullArgumentException : public std::runtime_error
	{
	public:
		explicit NullArgumentException(const std::string& message);
		explicit NullArgumentException(const char* message);
	};



#define TNAEWL_HELPER_S1(x) #x
#define TNAEWL_HELPER_S2(x) TNAEWL_HELPER_S1(x)
#define ThrowNullArgumentExceptionWithLine(message) \
	throw Nest::NullArgumentException("[" __FILE__ ":" TNAEWL_HELPER_S2(__LINE__) "] " message);
}



#endif

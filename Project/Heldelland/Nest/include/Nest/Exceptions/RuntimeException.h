//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__RUNTIME_EXCEPTION_H
#define NEST__EXCEPTIONS__RUNTIME_EXCEPTION_H

#include<string>



namespace Nest
{
	class RuntimeException : public std::runtime_error
	{
	public:
		explicit RuntimeException(const std::string& message);
		explicit RuntimeException(const char* message);
	};



#define TREWL_HELPER_S1(x) #x
#define TREWL_HELPER_S2(x) TREWL_HELPER_S1(x)

#define RuntimeExceptionWithLine(message) \
	Nest::RuntimeException("[" __FILE__ ":" TREWL_HELPER_S2(__LINE__) "] " message)

#define ThrowRuntimeExceptionWithLine(message) \
	throw RuntimeExceptionWithLine(message)
}



#endif

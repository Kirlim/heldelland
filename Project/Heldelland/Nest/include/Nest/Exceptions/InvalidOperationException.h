//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__INVALID_OPERATION_EXCEPTION_H
#define NEST__EXCEPTIONS__INVALID_OPERATION_EXCEPTION_H

#include<string>



namespace Nest
{
	// Means that a operation is invalid, given the current
	// object state
	class InvalidOperationException : public std::runtime_error
	{
	public:
		explicit InvalidOperationException(const std::string& message);
		explicit InvalidOperationException(const char* message);
	};



#define TIOEWL_HELPER_S1(x) #x
#define TIOEWL_HELPER_S2(x) TIOEWL_HELPER_S1(x)
#define ThrowInvalidOperationExceptionWithLine(message) \
	throw Nest::InvalidOperationException("[" __FILE__ ":" TIOEWL_HELPER_S2(__LINE__) "] " message);
}



#endif

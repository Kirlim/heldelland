
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__MISSING_COMPONENT_EXCEPTION_H
#define NEST__EXCEPTIONS__MISSING_COMPONENT_EXCEPTION_H


#include<string>



namespace Nest
{
	// Means that a component that was expected to exists
	// was not found.
	class MissingComponentException : public std::logic_error
	{
	public:
		explicit MissingComponentException(const std::string& message);
		explicit MissingComponentException(const char* message);
	};



#define TMCEWL_HELPER_S1(x) #x
#define TMCEWL_HELPER_S2(x) TMCEWL_HELPER_S1(x)
#define ThrowMissingComponentExceptionWithLine(message) \
	throw Nest::MissingComponentException("[" __FILE__ ":" TMCEWL_HELPER_S2(__LINE__) "] " message);
}



#endif
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS__FAILED_TO_LOAD_EXCEPTION_H
#define NEST__EXCEPTIONS__FAILED_TO_LOAD_EXCEPTION_H

#include<string>



namespace Nest
{
	// Means that an loading operation, such as loading data
	// or setting up system has failed
	class FailedToLoadException : public std::runtime_error
	{
	public:
		explicit FailedToLoadException(const std::string& message);
		explicit FailedToLoadException(const char* message);
	};



#define TFTLEWL_HELPER_S1(x) #x
#define TFTLEWL_HELPER_S2(x) TFTLEWL_HELPER_S1(x)
#define ThrowFailedToLoadExceptionWithLine(message) \
	throw Nest::FailedToLoadException("[" __FILE__ ":" TFTLEWL_HELPER_S2(__LINE__) "] " message);
}



#endif

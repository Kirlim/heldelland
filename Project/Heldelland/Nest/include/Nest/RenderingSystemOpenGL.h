
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__RENDERING_SYSTEM_OPEN_GL_H
#define NEST__RENDERING_SYSTEM_OPEN_GL_H

#include"SDL.h"
#include<GL/glew.h>
#include"Nest/RenderingSystem.h"



namespace Nest
{
	// Forward declarations
	class IComponentsContainers;
	class Mesh;
	class ShaderProgram;
	class RenderingManager;



	class RenderingSystemOpenGL final : public RenderingSystem
	{
	public:
		RenderingSystemOpenGL(
			SDL_Window& sdlWindow, const Nest::IComponentsContainers& componentsContainers, const RenderingManager& renderingManager
		);
		virtual ~RenderingSystemOpenGL() = default;

		void Init();

		void SetDirectionalLightPassShader(ShaderProgram* directionalLightPassShader);
		void SetPointLightPassShader(ShaderProgram* pointLightPassShader);

		ShaderProgram* GetDirectionalLightPassShader() const;
		ShaderProgram* GetPointLightPassShader() const;

		void SetSphereLightVolume(Mesh* sphereVolumeMesh);
		Mesh* GetSphereLightVolume() const;
		


	private:
		void InitQuad();
		void InitGBuffer();
		void DoRender(const float deltaTime) override;
		void RenderQuad() const;

		void RenderWorld() const;
		void RenderGUI();

		void BindGBufferForWriting() const;
		void BindGBufferForReading() const;


	protected:
		const IComponentsContainers& m_ComponentsContainers;
		const RenderingManager& m_RenderingManager;

		bool m_QuadInitialized = false;
		GLuint m_QuadVAO = 0;
		GLuint m_QuadVBO = 0;

		bool m_GeometryBufferInitialized = false;
		GLuint m_GeometryBuffer = 0;
		GLuint m_GeometryPosition = 0;
		GLuint m_GeometryNormal = 0;
		GLuint m_GeometryAlbedoSpec = 0;

		ShaderProgram* m_DirectionalLightPassShader = nullptr;
		ShaderProgram* m_PointLightPassShader = nullptr;

		Mesh* m_SphereLightVolume = nullptr;
	};
}



#endif

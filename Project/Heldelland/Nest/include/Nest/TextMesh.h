
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<string>
#include<memory>

#include"Nest/Component.h"



namespace Nest
{
    // Forward declarations
    class Font;
    class Material;
    class TextMeshImpl;



    class TextMesh : public Component
    {
    public:
        TextMesh();
	TextMesh(const TextMesh&) = delete;
	TextMesh(TextMesh&&) = default;

        ~TextMesh() override;

	TextMesh& operator =(const TextMesh&) = delete;
	TextMesh& operator =(TextMesh&&) = default;


        static std::string GetTypeName() { return("TextMesh"); }

        void SetFont(Font* font); // TODO : figure out a better way for storing references to resources (maybe handles?)
        Font* GetFont() const;

        void SetFontSize(const float size);
        float GetFontSize() const;

        void SetText(const std::string& text);
        std::string GetText() const;

        void Render();



    protected:
        virtual void OnDestroy() final;

        void CreateMesh();



    protected:
        Font* m_Font;
        float m_FontSize;
        std::string m_Text;
        std::shared_ptr<TextMeshImpl> m_Mesh; // Shared due to copy problems if unique
    };
}

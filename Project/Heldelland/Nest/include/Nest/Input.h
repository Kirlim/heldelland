//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__INPUT_H
#define NEST__INPUT_H



#include<memory>
#include<string>
#include<functional>

#include"Nest/InputEnumerators.h"
#include"Nest/Vector2.h"
#include"Nest/InputEvent.h"
#include"Nest/Events/EventsBus.h"



namespace Nest
{
	class Input
	{
	public:
		Input();
		~Input();


		////////////////////////////////////////////////////////////////////////////////
		// Signal input events. Usually, it is a manager that will use these methods, //
		// but other classes might as well, in order to simulate input. In that case, //
		// be careful with interferences.                                             //
		////////////////////////////////////////////////////////////////////////////////

		void KeyPressed(KeyCode keyCode);
		void KeyReleased(KeyCode keyCode);


		void SetMousePosition(int mouseX, int mouseY);
		void SetMousePosition(Vector2<int> position);

		void AddMouseScroll(int deltaX, int deltaY);
		void AddMouseScroll(Vector2<int> delta);

		void MouseButtonPressed(MouseButton button);
		void MouseButtonReleased(MouseButton button);


		///////////////////////////////////
		// Normal input fetching methods //
		///////////////////////////////////

		// Will check if a key has just been pressed, since the last
		// refresh of the Input
		bool IsKeyOnDown(KeyCode keyCode) const;

		// Checks if a key is pressed.
		// Will return true even if the key is registered as just pressed, but
		// not if just released.
		bool IsKeyPressed(KeyCode keyCode) const;

		// Will check if a key has just been released, since the last
		// refresh of the input.
		bool IsKeyOnUp(KeyCode keyCode) const;



		// Returns the currently set mouse position
		Vector2<int> GetMousePosition() const;

		Vector2<int> GetDeltaMouseScrollSinceLastRefresh() const;

		// Will check if a mouse button has just been pressed, since the last
		// refresh of the input
		bool IsMouseOnDown(MouseButton button) const;

		// Checks if a mouse button is pressed. Will return true even if the
		// mouse button has just been registered as pressed, but not if
		// has just been released.
		bool IsMousePressed(MouseButton button) const;

		// Will check if a mouse button has just been released, since the last
		// refresh of the input.
		bool IsMouseOnUp(MouseButton button) const;



		/////////////////////////
		// Text Input handling //
		/////////////////////////

		// TODO : return error code if text input fail to start
		void SetStartTextInputMethod(std::function<void()> startTextInputFunction);
		void SetStopTextInputMethod(std::function<void()> stopTextInputFunction);

		void StartTextInput(std::shared_ptr<std::string> destinationString);
		std::weak_ptr<std::string> GetWorkingTextInputString() const;
		bool IsOnTextInput() const;
		void StopTextInput();

		void TextInputAppend(const char* character) const;
		void TextInputBackspace() const;



		///////////////////
		// Other methods //
		///////////////////

		// Updates the registered input events, and clear them up
		// if they "dont exist" anymore.
		void Refresh();

		EventsBus<InputEvent&>& GetInputEventsBus();


		static int KeyCodeToInt(KeyCode keyCode) { return static_cast<unsigned int>(keyCode); }
		static int MouseButtonToInt(MouseButton mouseButton) { return static_cast<unsigned int>(mouseButton); }



	private:
		void ClearAllKeyboardInput();
		void ClearAllMouseInput();


	private:
		bool m_KeysOnDown[static_cast<unsigned int>(KeyCode::KeyCodeSize)];
		bool m_KeysPressed[static_cast<unsigned int>(KeyCode::KeyCodeSize)];
		bool m_KeysOnUp[static_cast<unsigned int>(KeyCode::KeyCodeSize)];

		bool m_MouseButtonsOnDown[static_cast<unsigned int>(MouseButton::MouseButtonSize)];
		bool m_MouseButtonsPressed[static_cast<unsigned int>(MouseButton::MouseButtonSize)];
		bool m_MouseButtonsOnUp[static_cast<unsigned int>(MouseButton::MouseButtonSize)];

		Vector2<int> m_MousePosition;
		Vector2<int> m_MouseScroll;

		std::function<void()> m_StartTextInputFunction;
		std::function<void()> m_StopTextInputFunction;
		std::shared_ptr<std::string> m_TextInputString;

		EventsBus<InputEvent&> m_InputEventsBus;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include"Nest/ISingleComponentsContainer.h"
#include"Nest/ITemplatedSingleComponentsContainerIterator.h"



namespace Nest
{
    template <typename ComponentType>
    class ITemplatedSingleComponentsContainer : public ISingleComponentsContainer
    {
    public:
        virtual ~ITemplatedSingleComponentsContainer() { }

        virtual auto TemplatedBegin() const ->ITemplatedSingleComponentsContainerIterator<ComponentType> = 0;
        virtual auto TemplatedEnd() const->ITemplatedSingleComponentsContainerIterator<ComponentType> = 0;
    };



    template <typename ComponentType>
    auto begin(ITemplatedSingleComponentsContainer<ComponentType>& container)->ITemplatedSingleComponentsContainerIterator<ComponentType>
    {
        return(container.TemplatedBegin());
    }



    template <typename ComponentType>
    auto end(ITemplatedSingleComponentsContainer<ComponentType>& container)->ITemplatedSingleComponentsContainerIterator<ComponentType>
    {
        return(container.TemplatedEnd());
    }
}

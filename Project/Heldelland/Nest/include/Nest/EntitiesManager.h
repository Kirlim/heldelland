
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__ENTITIES_MANAGER_H
#define NEST__ENTITIES_MANAGER_H



#include<functional>
#include<vector>
#include<memory>

// TODO : consider putting EntityId into its own header, to reduce include hell.
#include"Nest/IEntity.h"



namespace Nest
{
	// Forward declarations
	class IComponentsFactory;
	class IEntity;
	class IResourcesManager;
	class World;



	// TODO : consider using an IEntitiesManager interface

	class EntitiesManager
	{
	public:
		explicit EntitiesManager(const World& world);
		~EntitiesManager() = default; // TODO : destroy all entities, calling destroy

		std::weak_ptr<IEntity> Instantiate();
		void Destroy(std::weak_ptr<IEntity> entity);
		void Destroy(EntityId entityId);

		std::weak_ptr<IEntity> GetById(EntityId entityId) const;

		const World* GetWorld() const;
		IComponentsFactory* GetComponentsFactory() const;
		IResourcesManager* GetResourcesManager() const;

		// Forbid copying
		EntitiesManager(EntitiesManager const &) = delete;
		EntitiesManager& operator=(EntitiesManager const &) = delete;


	protected:
		void UnloadAllEntities();

		// Stores the entity id and a reference to it
		struct EntityInformation { EntityId id; std::shared_ptr<IEntity> entity; };

		// Storage of the entities. Right now, a vector, but can be changed later.
		std::vector<EntityInformation> m_Entities;

		// Factory method to be used to create new instances that implements IEntity.
		std::function<std::shared_ptr<IEntity>()> m_EntityFactoryMethod;



	protected:
		const World& m_World;
	};
}



#endif

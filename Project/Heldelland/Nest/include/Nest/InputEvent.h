//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__INPUT_EVENT_H
#define NEST__INPUT_EVENT_H

#include"Nest/InputEnumerators.h"



namespace Nest
{
	enum class InputEventType
	{
		Undefined,

		MouseButtonDown,
		MouseButtonUp,
		MouseMovement,
		MouseScroll
	};



	struct InputEvent
	{
		InputEventType type;
		MouseButton mouseButton;
		int deltaX, deltaY;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__I_MODEL_LOADER_H
#define NEST__I_MODEL_LOADER_H

#include<string>
#include<vector>
#include<memory>



namespace Nest
{
	// Forward declarations
	class Model;
	class ModelNode;
	class IResourcesManager;
	enum class GameRenderer;



	// TODO : technical decision: should the model loaders load the models into the resources manager? Or return the resources and let the user decide?

	// Responsibility:
	//    Load a 3d model file (ex. fbx files) into one or more meshes, texture and/or
	//    materials in the resources manager.
	class IModelLoader
	{
	public:
		IModelLoader();
		virtual ~IModelLoader();

		// TODO : consider that the loader should return unique_ptr, instead of pushing the mesh into the resources manager

		// Loads a model and its resources, and then returns a
		// pointer to the loaded model resource. The model resource name will be @modelName,
		// and the other resources can have @modelName prepended to their name if
		// @prependResourcesFlags are set.
		// For now, only .fbx with relative paths to textures will work.
		//
		// IMPORTANT : meshes names will be generated with an implementation dependent
		//             algorithm, to try to guarantee unique names.
		//
		// Will throw:
		//    - Nest::FailedToLoadException if filename is empty or file is not found.
		virtual Model* Load(
			IResourcesManager& resourcesManager, GameRenderer gameRenderer,
			std::string filename, std::string modelName, unsigned int prependNamesFlags
		) = 0;



	protected:
		static std::unique_ptr<Model> BuildModel(std::unique_ptr<ModelNode>& rootModelNode);
	};
}



#endif

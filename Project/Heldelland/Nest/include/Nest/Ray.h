
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include"Nest/Vector3.h"



namespace Nest
{
    struct Ray
    {
        Vector3<float> origin;
        Vector3<float> direction;
    };
}

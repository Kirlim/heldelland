
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__ENTITY_H
#define NEST__ENTITY_H

#include<vector>
#include<memory>

#include"Nest/IEntity.h"

// TODO : pass component id to specific file, in order to ease up includes
#include"Nest/Component.h"



namespace Nest
{
	// Forward declarations
	class ComponentOwnerReferenceBase;
	class EntitiesManager;



	class Entity : public IEntity
	{
	public:
		explicit Entity(EntitiesManager* ownerEntitiesManager);

		Entity() = delete;
		Entity(const Entity&) = delete;
		Entity(Entity &&) = delete;

		int GetComponentsCount() const override;
		std::unique_ptr<ComponentReferenceBase> GetComponentAt(int index) const override;
		ComponentTypeId GetComponentTypeIdAt(int index) const override;

		void SetName(std::string name) override;
		std::string GetName() const override;

		std::weak_ptr<IEntity> Clone(EntitiesManager& owningManager) override;

		EntitiesManager* GetEntitiesManager() const override;



	protected:
		std::unique_ptr<ComponentOwnerReferenceBase> DoInstantiateComponent(ComponentTypeId typeId) const override final;
		std::unique_ptr<ComponentOwnerReferenceBase> DoInstantiateComponent(std::string typeName) const override final;

		void DoAddComponent(std::unique_ptr<ComponentOwnerReferenceBase>& componentReference, ComponentTypeId componentTypeId) override final;

		ComponentOwnerReferenceBase* DoGetComponent(ComponentTypeId componentTypeId) const override final;
		ComponentOwnerReferenceBase* DoGetComponent(std::string componentTypeName) const override final;
		ComponentOwnerReferenceBase* DoGetComponentByName(std::string componentName) const override final;
		ComponentOwnerReferenceBase* DoGetComponentById(ComponentId componentId) const override final;

		void DoRemoveComponent(ComponentTypeId componentTypeId) override final;
		void DoRemoveComponentByName(std::string componentName) override final;

		EntityId DoGetId() const override final;
		void DoSetId(EntityId id) override final;

		void DoDestroy() override final;



	protected:
		EntitiesManager* m_OwnerEntitiesManager;
		EntityId m_Id;
		std::string m_Name;

		// Store components of this entity
		struct ComponentInfo
		{
			std::unique_ptr<ComponentOwnerReferenceBase> reference;
			ComponentTypeId typeId;
		};
		std::vector<ComponentInfo> m_Components;
	};
}



#endif

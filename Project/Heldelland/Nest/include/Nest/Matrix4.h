//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__MATRIX4_H
#define NEST__MATRIX4_H

#include"Nest/Vector3.h"
#include"Nest/Vector4.h"



namespace Nest
{
	template <typename NumberType>
	class Matrix4
	{
	public:
		Matrix4();
		Matrix4(const Matrix4& other);
		Matrix4(const NumberType elementsToCopy[16]);
		Matrix4(
			NumberType m11, NumberType m12, NumberType m13, NumberType m14,
			NumberType m21, NumberType m22, NumberType m23, NumberType m24,
			NumberType m31, NumberType m32, NumberType m33, NumberType m34,
			NumberType m41, NumberType m42, NumberType m43, NumberType m44
		);




		void Set(const NumberType values[16]);
		void Set(
			NumberType m11, NumberType m12, NumberType m13, NumberType m14,
			NumberType m21, NumberType m22, NumberType m23, NumberType m24,
			NumberType m31, NumberType m32, NumberType m33, NumberType m34,
			NumberType m41, NumberType m42, NumberType m43, NumberType m44
		);
		// TODO : Set with initializel list and unit test


		// Operations between matrices
		Matrix4& operator +=(const Matrix4& other);
		Matrix4& operator -=(const Matrix4& other);
		Matrix4& operator *=(const Matrix4& other);

		Matrix4 operator +(const Matrix4& other);
		Matrix4 operator -(const Matrix4& other);
		Matrix4 operator *(const Matrix4& other);


		// Matrix information methods
		Matrix4 GetInverse() const;

		NumberType m11() const;
		NumberType m12() const;
		NumberType m13() const;
		NumberType m14() const;
		NumberType m21() const;
		NumberType m22() const;
		NumberType m23() const;
		NumberType m24() const;
		NumberType m31() const;
		NumberType m32() const;
		NumberType m33() const;
		NumberType m34() const;
		NumberType m41() const;
		NumberType m42() const;
		NumberType m43() const;
		NumberType m44() const;

		NumberType m11(NumberType value);
		NumberType m12(NumberType value);
		NumberType m13(NumberType value);
		NumberType m14(NumberType value);
		NumberType m21(NumberType value);
		NumberType m22(NumberType value);
		NumberType m23(NumberType value);
		NumberType m24(NumberType value);
		NumberType m31(NumberType value);
		NumberType m32(NumberType value);
		NumberType m33(NumberType value);
		NumberType m34(NumberType value);
		NumberType m41(NumberType value);
		NumberType m42(NumberType value);
		NumberType m43(NumberType value);
		NumberType m44(NumberType value);



		static Matrix4 Identity();


		// Data Storage
		NumberType data[16];
	};



	// Class methods

	template <typename NumberType>
	Matrix4<NumberType>::Matrix4()
	{
		for (int i = 0; i < 16; ++i)
		{
			data[i] = 0;
		}
	}



	template <typename NumberType>
	Matrix4<NumberType>::Matrix4(const Matrix4& other)
	{
		for (int i = 0; i < 16; ++i)
		{
			data[i] = other.data[i];
		}
	}



	template <typename NumberType>
	Matrix4<NumberType>::Matrix4(const NumberType elementsToCopy[16])
	{
		for (int i = 0; i < 16; ++i)
		{
			data[i] = elementsToCopy[i];
		}
	}



	template <typename NumberType>
	Matrix4<NumberType>::Matrix4(
		NumberType m11, NumberType m12, NumberType m13, NumberType m14,
		NumberType m21, NumberType m22, NumberType m23, NumberType m24,
		NumberType m31, NumberType m32, NumberType m33, NumberType m34,
		NumberType m41, NumberType m42, NumberType m43, NumberType m44
	)
	{
		data[0] = m11;
		data[1] = m12;
		data[2] = m13;
		data[3] = m14;
		data[4] = m21;
		data[5] = m22;
		data[6] = m23;
		data[7] = m24;
		data[8] = m31;
		data[9] = m32;
		data[10] = m33;
		data[11] = m34;
		data[12] = m41;
		data[13] = m42;
		data[14] = m43;
		data[15] = m44;
	}



	template <typename NumberType>
	void Matrix4<NumberType>::Set(const NumberType values[16])
	{
		for (int i = 0; i < 16; ++i)
		{
			data[i] = values[i];
		}
	}



	template <typename NumberType>
	void Matrix4<NumberType>::Set(
		NumberType m11, NumberType m12, NumberType m13, NumberType m14,
		NumberType m21, NumberType m22, NumberType m23, NumberType m24,
		NumberType m31, NumberType m32, NumberType m33, NumberType m34,
		NumberType m41, NumberType m42, NumberType m43, NumberType m44
	)
	{
		data[0] = m11;
		data[1] = m12;
		data[2] = m13;
		data[3] = m14;
		data[4] = m21;
		data[5] = m22;
		data[6] = m23;
		data[7] = m24;
		data[8] = m31;
		data[9] = m32;
		data[10] = m33;
		data[11] = m34;
		data[12] = m41;
		data[13] = m42;
		data[14] = m43;
		data[15] = m44;
	}

	// TODO : Set with initializel list and unit test



	template <typename NumberType>
	Matrix4<NumberType>& Matrix4<NumberType>::operator +=(const Matrix4& other)
	{
		for (int y = 0; y < 16; y += 4)
		{
			for (int x = 0; x < 4; ++x)
			{
				data[y + x] += other.data[y + x];
			}
		}

		return *this;
	}



	template <typename NumberType>
	Matrix4<NumberType>& Matrix4<NumberType>::operator -=(const Matrix4& other)
	{
		for (int y = 0; y < 16; y += 4)
		{
			for (int x = 0; x < 4; ++x)
			{
				data[y + x] -= other.data[y + x];
			}
		}

		return *this;
	}



	template <typename NumberType>
	Matrix4<NumberType>& Matrix4<NumberType>::operator *=(const Matrix4& other)
	{
		NumberType tmp[16];

		for (int row = 0; row < 16; row += 4)
		{
			for (int col = 0; col < 4; ++col)
			{
				NumberType value;
				value = 0;

				for (int i = 0; i < 4; ++i)
				{
					value += data[row + i] * other.data[col + i * 4];
				}

				tmp[row + col] = value;
			}
		}

		for (int i = 0; i < 16; ++i)
		{
			data[i] = tmp[i];
		}

		return *this;
	}

	template <typename NumberType>
	Matrix4<NumberType> Matrix4<NumberType>::operator +(const Matrix4& other)
	{
		Matrix4 m(*this);
		m += other;
		return m;
	}

	template <typename NumberType>
	Matrix4<NumberType> Matrix4<NumberType>::operator -(const Matrix4& other)
	{
		Matrix4 m(*this);
		m -= other;
		return m;
	}

	template <typename NumberType>
	Matrix4<NumberType> Matrix4<NumberType>::operator *(const Matrix4& other)
	{
		Matrix4 m(*this);
		m *= other;
		return m;
	}



	template <typename NumberType>
	Matrix4<NumberType> Matrix4<NumberType>::GetInverse() const
	{
		Matrix4<NumberType> inverse;
		NumberType* inv = inverse.data; // Pointer to inverse data, to save some typing

		inv[0] = data[5] * data[10] * data[15] -
			data[5] * data[11] * data[14] -
			data[9] * data[6] * data[15] +
			data[9] * data[7] * data[14] +
			data[13] * data[6] * data[11] -
			data[13] * data[7] * data[10];

		inv[4] = -data[4] * data[10] * data[15] +
			data[4] * data[11] * data[14] +
			data[8] * data[6] * data[15] -
			data[8] * data[7] * data[14] -
			data[12] * data[6] * data[11] +
			data[12] * data[7] * data[10];

		inv[8] = data[4] * data[9] * data[15] -
			data[4] * data[11] * data[13] -
			data[8] * data[5] * data[15] +
			data[8] * data[7] * data[13] +
			data[12] * data[5] * data[11] -
			data[12] * data[7] * data[9];

		inv[12] = -data[4] * data[9] * data[14] +
			data[4] * data[10] * data[13] +
			data[8] * data[5] * data[14] -
			data[8] * data[6] * data[13] -
			data[12] * data[5] * data[10] +
			data[12] * data[6] * data[9];

		inv[1] = -data[1] * data[10] * data[15] +
			data[1] * data[11] * data[14] +
			data[9] * data[2] * data[15] -
			data[9] * data[3] * data[14] -
			data[13] * data[2] * data[11] +
			data[13] * data[3] * data[10];

		inv[5] = data[0] * data[10] * data[15] -
			data[0] * data[11] * data[14] -
			data[8] * data[2] * data[15] +
			data[8] * data[3] * data[14] +
			data[12] * data[2] * data[11] -
			data[12] * data[3] * data[10];

		inv[9] = -data[0] * data[9] * data[15] +
			data[0] * data[11] * data[13] +
			data[8] * data[1] * data[15] -
			data[8] * data[3] * data[13] -
			data[12] * data[1] * data[11] +
			data[12] * data[3] * data[9];

		inv[13] = data[0] * data[9] * data[14] -
			data[0] * data[10] * data[13] -
			data[8] * data[1] * data[14] +
			data[8] * data[2] * data[13] +
			data[12] * data[1] * data[10] -
			data[12] * data[2] * data[9];

		inv[2] = data[1] * data[6] * data[15] -
			data[1] * data[7] * data[14] -
			data[5] * data[2] * data[15] +
			data[5] * data[3] * data[14] +
			data[13] * data[2] * data[7] -
			data[13] * data[3] * data[6];

		inv[6] = -data[0] * data[6] * data[15] +
			data[0] * data[7] * data[14] +
			data[4] * data[2] * data[15] -
			data[4] * data[3] * data[14] -
			data[12] * data[2] * data[7] +
			data[12] * data[3] * data[6];

		inv[10] = data[0] * data[5] * data[15] -
			data[0] * data[7] * data[13] -
			data[4] * data[1] * data[15] +
			data[4] * data[3] * data[13] +
			data[12] * data[1] * data[7] -
			data[12] * data[3] * data[5];

		inv[14] = -data[0] * data[5] * data[14] +
			data[0] * data[6] * data[13] +
			data[4] * data[1] * data[14] -
			data[4] * data[2] * data[13] -
			data[12] * data[1] * data[6] +
			data[12] * data[2] * data[5];

		inv[3] = -data[1] * data[6] * data[11] +
			data[1] * data[7] * data[10] +
			data[5] * data[2] * data[11] -
			data[5] * data[3] * data[10] -
			data[9] * data[2] * data[7] +
			data[9] * data[3] * data[6];

		inv[7] = data[0] * data[6] * data[11] -
			data[0] * data[7] * data[10] -
			data[4] * data[2] * data[11] +
			data[4] * data[3] * data[10] +
			data[8] * data[2] * data[7] -
			data[8] * data[3] * data[6];

		inv[11] = -data[0] * data[5] * data[11] +
			data[0] * data[7] * data[9] +
			data[4] * data[1] * data[11] -
			data[4] * data[3] * data[9] -
			data[8] * data[1] * data[7] +
			data[8] * data[3] * data[5];

		inv[15] = data[0] * data[5] * data[10] -
			data[0] * data[6] * data[9] -
			data[4] * data[1] * data[10] +
			data[4] * data[2] * data[9] +
			data[8] * data[1] * data[6] -
			data[8] * data[2] * data[5];

		NumberType det = data[0] * inv[0] + data[1] * inv[4] + data[2] * inv[8] + data[3] * inv[12];

		if (det == 0)
			return Identity(); // TODO : throw error?

		det = NumberType(1.0) / det;

		for (int i = 0; i < 16; i++)
			inv[i] = inv[i] * det;

		return inverse;
	}



	template <typename NumberType>
	NumberType Matrix4<NumberType>::m11() const { return data[0]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m12() const { return data[1]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m13() const { return data[2]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m14() const { return data[3]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m21() const { return data[4]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m22() const { return data[5]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m23() const { return data[6]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m24() const { return data[7]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m31() const { return data[8]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m32() const { return data[9]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m33() const { return data[10]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m34() const { return data[11]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m41() const { return data[12]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m42() const { return data[13]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m43() const { return data[14]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m44() const { return data[15]; }

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m11(NumberType value)
	{
		data[0] = value;
		return data[0];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m12(NumberType value)
	{
		data[1] = value;
		return data[1];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m13(NumberType value)
	{
		data[2] = value;
		return data[2];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m14(NumberType value)
	{
		data[3] = value;
		return data[3];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m21(NumberType value)
	{
		data[4] = value;
		return data[4];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m22(NumberType value)
	{
		data[5] = value;
		return data[5];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m23(NumberType value)
	{
		data[6] = value;
		return data[6];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m24(NumberType value)
	{
		data[7] = value;
		return data[7];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m31(NumberType value)
	{
		data[8] = value;
		return data[8];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m32(NumberType value)
	{
		data[9] = value;
		return data[9];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m33(NumberType value)
	{
		data[10] = value;
		return data[10];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m34(NumberType value)
	{
		data[11] = value;
		return data[11];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m41(NumberType value)
	{
		data[12] = value;
		return data[12];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m42(NumberType value)
	{
		data[13] = value;
		return data[13];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m43(NumberType value)
	{
		data[14] = value;
		return data[14];
	}

	template <typename NumberType>
	NumberType Matrix4<NumberType>::m44(NumberType value)
	{
		data[15] = value;
		return data[15];
	}



	template <typename NumberType>
	Matrix4<NumberType> Matrix4<NumberType>::Identity()
	{
		Matrix4 identity;
		identity.data[0] = 1;
		identity.data[5] = 1;
		identity.data[10] = 1;
		identity.data[15] = 1;

		return identity;
	}



	// Other operations methods

	// Operations with vectors
	template <typename NumberType>
	Vector3<NumberType> operator *(const Matrix4<NumberType>& m, const Vector3<NumberType>& v)
	{
		Vector3<NumberType> result(
			m.data[0] * v.x + m.data[1] * v.y + m.data[2] * v.z + m.data[3],
			m.data[4] * v.x + m.data[5] * v.y + m.data[6] * v.z + m.data[7],
			m.data[8] * v.x + m.data[9] * v.y + m.data[10] * v.z + m.data[11]
		);

		return result;
	}



	template <typename NumberType>
	Vector4<NumberType> operator *(const Matrix4<NumberType>& m, const Vector4<NumberType>& v)
	{
		Vector4<NumberType> result(
			m.data[0] * v.data[0] + m.data[1] * v.data[1] + m.data[2] * v.data[2] + m.data[3] * v.data[3],
			m.data[4] * v.data[0] + m.data[5] * v.data[1] + m.data[6] * v.data[2] + m.data[7] * v.data[3],
			m.data[8] * v.data[0] + m.data[9] * v.data[1] + m.data[10] * v.data[2] + m.data[11] * v.data[3],
			m.data[12] * v.data[0] + m.data[13] * v.data[1] + m.data[14] * v.data[2] + m.data[15] * v.data[3]
		);

		return result;
	}



	template <typename NumberType>
	Vector4<NumberType> operator *(const Vector4<NumberType>& v, const Matrix4<NumberType>& m)
	{
		Vector4<NumberType> result(
			v.data[0] * m.data[0] + v.data[1] * m.data[4] + v.data[2] * m.data[8] + v.data[3] + m.data[12],
			v.data[0] * m.data[1] + v.data[1] * m.data[5] + v.data[2] * m.data[9] + v.data[3] + m.data[1],
			v.data[0] * m.data[2] + v.data[1] * m.data[6] + v.data[2] * m.data[10] + v.data[3] + m.data[14],
			v.data[0] * m.data[3] + v.data[1] * m.data[7] + v.data[2] * m.data[11] + v.data[3] + m.data[15]
		);

		return result;
	}
}



#endif

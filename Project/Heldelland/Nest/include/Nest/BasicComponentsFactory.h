
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<memory>
#include<string>

#include"Nest/ComponentOwnerReferenceBase.h"



namespace Nest
{
    class BasicComponentsFactory
    {
    public:
        virtual ~BasicComponentsFactory() {}
        virtual std::string GetTypeName() const = 0;
        virtual unsigned int GetTypeId() const = 0;

        virtual std::unique_ptr<ComponentOwnerReferenceBase> Instantiate() = 0;
    };
}

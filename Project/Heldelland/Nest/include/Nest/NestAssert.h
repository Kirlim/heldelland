
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__NEST_ASSERT_H
#define NEST__NEST_ASSERT_H

#define ENABLE_NEST_ASSERT

#ifdef ENABLE_NEST_ASSERT

#include"Nest/Exceptions/AssertException.h"
#define NestAssert(EXPRESSION) ((EXPRESSION) ? (void)0 : (throw Nest::AssertException("Assertion failed at [" + std::string(__FILE__) + ":" + std::to_string(__LINE__) + "]: " + #EXPRESSION)))
#define NestAssertMsg(EXPRESSION, MESSAGE) ((EXPRESSION) ? (void)0 : (throw Nest::AssertException("Assertion failed at [" + std::string(__FILE__) + ":" + std::to_string(__LINE__) + "]: " + #MESSAGE)))

#else

#define NestAssert(EXPRESSION) (void)0

#endif



#endif
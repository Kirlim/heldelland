
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<memory>

#include"Nest/Camera.h"
#include"Nest/ComponentReference.h"



namespace Nest
{
    class RenderingManager
    {
    public:
        RenderingManager();
        ~RenderingManager();

        void SetActiveCamera(ComponentReference<Camera> cameraReference);
        ComponentReference<Camera> GetActiveCamera() const;

        void SetActiveGuiCamera(ComponentReference<Camera> guiCameraReference);
        ComponentReference<Camera> GetActiveGuiCamera() const;


    protected:
        ComponentReference<Camera> m_CameraReference;
        ComponentReference<Camera> m_GuiCameraReference;
    };
}

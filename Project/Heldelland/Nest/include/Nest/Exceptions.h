
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EXCEPTIONS_H
#define NEST__EXCEPTIONS_H



#include"Nest/Exceptions/AlreadyRegisteredException.h"
#include"Nest/Exceptions/AssertException.h"
#include"Nest/Exceptions/BadFileException.h"
#include"Nest/Exceptions/FailedToLoadException.h"
#include"Nest/Exceptions/InvalidArgumentException.h"
#include"Nest/Exceptions/InvalidFileStructureException.h"
#include"Nest/Exceptions/InvalidDataException.h"
#include"Nest/Exceptions/InvalidOperationException.h"
#include"Nest/Exceptions/LogicErrorException.h"
#include"Nest/Exceptions/MissingComponentException.h"
#include"Nest/Exceptions/NotRegisteredException.h"
#include"Nest/Exceptions/NullArgumentException.h"
#include"Nest/Exceptions/NullPointerException.h"
#include"Nest/Exceptions/ValueOutOfRangeException.h"



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<string>
#include<unordered_map>
#include<fstream>
#include<sstream>
#include<GL/glew.h>



namespace Nest
{
    // Forward declarations
    enum class GameRenderer;



    class ShaderProgram
    {
    public:
        ShaderProgram(); // Will use global to know which game renderer to use
        ShaderProgram(GameRenderer gameRenderer);
        ~ShaderProgram();

        void LoadFromFile(const std::string& vertexShaderFile, const std::string& fragmentShaderFile);
        void LoadFromString(const std::string& vertexShaderContens, const std::string& fragmentShaderContents);

        void SetMainTextureName(const std::string& mainTextureName);
        std::string GetMainTextureName() const;

        void AddTextureName(const std::string& textureName); // TODO : find a way to emit warnings if name was not found
        void RemoveTextureName(const std::string& textureName);
        GLint GetTextureSlot(const std::string& textureName) const;
        const std::unordered_map<std::string, GLint>* GetTexturesSlots() const;

        void SetTransparentFlag(bool transparent);
        bool IsTransparent() const;

        void Unload();

        bool IsLoaded() const;
        void Use() const;

        GLuint GetProgramId() const;



    protected:
        std::unordered_map<std::string, GLint> m_TexturesSlots;
        std::string m_MainTextureName;

        bool m_IsTransparent = false;

        GLuint m_Program = false;
        bool m_Loaded = false;

        // TODO : remove this when pimpl or inheritance is used (probably inheritance)
        GameRenderer m_GameRenderer;
    };
}

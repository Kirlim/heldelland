
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__RESOURCES_TYPES_H
#define NEST__RESOURCES_TYPES_H

#include<cstddef>
#include<string>



namespace Nest
{
	// Basic resource types known by the engine.
	enum class ResourceType
	{
		Undefined, Font, Material, Mesh, Model, Shader, Texture
	};

	struct ResourceTypeHash
	{
		std::size_t operator()(ResourceType resourceType) const
		{
			return(static_cast<std::size_t>(resourceType));
		}
	};

	ResourceType GetResourceTypeFromString(const std::string resourceTypeString);
}



#endif

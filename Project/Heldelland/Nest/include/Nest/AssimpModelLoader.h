//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__ASSIMP_MODEL_LOADER_H
#define NEST__ASSIMP_MODEL_LOADER_H

#include<vector>
#include<string>

#include<assimp/Importer.hpp>
#include<assimp/scene.h>

#include"Nest/IModelLoader.h"



namespace Nest
{
	// Forward declarations
	enum class GameRenderer;
	class Mesh;
	class MeshesFactory;
	class ModelNode;
	class IResourcesManager;
	class Texture;



	class AssimpModelLoader : public IModelLoader
	{
	public:
		explicit AssimpModelLoader(const MeshesFactory& meshesFactory);
		AssimpModelLoader(const AssimpModelLoader&) = delete;
		AssimpModelLoader(AssimpModelLoader&&) = delete;

		~AssimpModelLoader() override;

		AssimpModelLoader& operator=(const AssimpModelLoader&) = delete;
		AssimpModelLoader& operator=(AssimpModelLoader&&) = delete;


		Model* Load(
			IResourcesManager& resourcesManager, GameRenderer gameRenderer,
			std::string filename, std::string modelName, unsigned int prependNamesFlags
		) override final;



	protected:
		void LoadNode(
			IResourcesManager& resourcesManager, GameRenderer gameRenderer,
			std::string modelName, ModelNode* modelNode, const aiNode* node, const aiScene* scene, unsigned int prependNamesFlags,
			std::string textureFolderPath,
			std::vector<std::string>& refLoadedTextures, std::vector<std::string>& refLoadedMaterials, std::vector<std::string>& refLoadedMeshes
		) const;

		void LoadTextures(
			IResourcesManager& resourcesManager, GameRenderer gameRenderer,
			std::string modelName, const aiMaterial* material, aiTextureType textureType, std::string textureFolder,
			std::vector<std::string>& refLoadedTextures, bool prependTextureName
		) const;

		std::string LoadMaterial(
			IResourcesManager& resourcesManager,
			std::string modelName, const aiMaterial* material,
			std::vector<std::string>& refLoadedMaterials, bool prependTextureName, bool prependMaterialName
		) const;

		void LoadMesh(
			IResourcesManager& resourcesManager, GameRenderer gameRenderer,
			std::string modelName, ModelNode* modelNode, std::string meshName, aiMesh* mesh, const aiScene* scene, bool prependTextureName, bool prependMaterialName,
			std::string textureFolder,
			std::vector<std::string>& refLoadedTextures, std::vector<std::string>& refLoadedMaterials, std::vector<std::string>& refLoadedMeshes
		) const;

		std::string MakeMeshName(std::string modelName, std::string meshName, bool prependModelName) const;
		std::string MakeMaterialName(std::string modelName, std::string materialName, bool prependModelName) const;
		std::string MakeTextureName(std::string modelName, std::string path, bool prependModelName) const;



	public:
		const MeshesFactory& m_MeshesFactory;

		static const unsigned int NO_PREPEND = 0;
		static const unsigned int TEXTURE_NAME = 1;
		static const unsigned int MATERIAL_NAME = 2;
		static const unsigned int MESH_NAME = 4;
		static const unsigned int ALL_NAMES = 7;
	};
}



#endif

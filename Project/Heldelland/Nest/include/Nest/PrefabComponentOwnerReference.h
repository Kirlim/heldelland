
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__PREFAB_COMPONENT_OWNER_REFERENCE_H
#define NEST__PREFAB_COMPONENT_OWNER_REFERENCE_H

#include<memory>

#include"Nest/ComponentOwnerReferenceBase.h"



namespace Nest
{
	// Forward declarations
	class Component;



	// Stores a component for prefabs entities. The reference does, in fact,
	// owns the component, which should not be accessed by any system.
	class PrefabComponentOwnerReference : public ComponentOwnerReferenceBase
	{
	public:
		PrefabComponentOwnerReference(std::unique_ptr<Component> component, ComponentTypeId typeId);
		virtual ~PrefabComponentOwnerReference();

		virtual Component* Get() const override final;



	protected:
		virtual void RegisterComponentReference(ComponentReferenceBase& reference) override;
		virtual void UnregisterComponentReference(ComponentReferenceBase& reference) override;



	protected:
		ComponentTypeId m_TypeId;
		std::unique_ptr<Component> m_Component;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<memory>
#include<string>



namespace Nest
{
	// Forward declarations
	class BasicComponentsFactory;
	class ComponentOwnerReferenceBase;



	class IComponentsFactory
	{
	public:
		virtual ~IComponentsFactory();

		void RegisterFactory(const unsigned int factoryTypeId, const std::string typeName, std::unique_ptr<BasicComponentsFactory> factory);
		std::unique_ptr<ComponentOwnerReferenceBase> Instantiate(const unsigned int factoryTypeId);
		std::unique_ptr<ComponentOwnerReferenceBase> Instantiate(const std::string typeName);

		std::string GetTypeName(const unsigned int factoryTypeId) const;
		unsigned int GetFactoryTypeId(const std::string typeName) const;



	protected:
		virtual void DoRegisterFactory(const unsigned int factoryTypeId, const std::string typeName, std::unique_ptr<BasicComponentsFactory> factory) = 0;
		virtual std::unique_ptr<ComponentOwnerReferenceBase> DoInstantiate(const unsigned int factoryTypeId) = 0;
		virtual std::unique_ptr<ComponentOwnerReferenceBase> DoInstantiate(const std::string typeName) = 0;

		virtual std::string DoGetTypeName(const unsigned int factoryTypeId) const = 0;
		virtual unsigned int DoGetFactoryTypeId(const std::string typeName) const = 0;
	};
}

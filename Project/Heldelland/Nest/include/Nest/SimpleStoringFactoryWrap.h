
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<memory>

#include"Nest/BasicComponentsFactory.h"
#include"Nest/ComponentOwnerReference.h"
#include"Nest/StoringFactory.h"



namespace Nest
{
    // Wraps up a Storing Factory type, which does not inherit from BasicComponentsFactory,
    // inside a type that does, in order to better integrate with the engines "factories listing"
    template <typename ComponentType>
    class SimpleStoringFactoryWrap : public BasicComponentsFactory
    {
    public:
        SimpleStoringFactoryWrap(const unsigned int typeId, const std::string typeName, std::unique_ptr<StoringFactory<ComponentType>> factory)
            : m_TypeId(typeId), m_TypeName(typeName), m_Factory(std::move(factory)) { }
        virtual ~SimpleStoringFactoryWrap() { }

        std::string GetTypeName() const override
        {
            return(m_TypeName);
        }



        unsigned int GetTypeId() const override
        {
            return(m_TypeId);
        }



        std::unique_ptr<ComponentOwnerReferenceBase> Instantiate() override
        {
            // TODO : assert on debug that factory is valid
            auto handle = m_Factory->Instantiate();
            return(std::make_unique<Nest::ComponentOwnerReference<ComponentType>>(m_TypeId, handle));
        }


    protected:
        unsigned int m_TypeId;
        std::string m_TypeName;
        std::unique_ptr<StoringFactory<ComponentType>> m_Factory;
    };
}
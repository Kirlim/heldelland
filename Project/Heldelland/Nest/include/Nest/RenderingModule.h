
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include"SDL.h"



namespace Nest
{
    // Indicates the renderer used
    enum class GameRenderer { NotSet, OpenGL, Vulkan, Test };



    class RenderingModule
    {
    public:
        RenderingModule();
        ~RenderingModule();

        void Initialize(GameRenderer gameRenderer, int screenWidth, int screenHeight);

        int GetScreenWidth() const;
        int GetScreenHeight() const;
        GameRenderer GetGameRenderer() const;

        SDL_Window* GetSDLWindow() const;



    protected:
        bool SdlInit();
        bool OpenGlInit(int screenWidth, int screenHeight);
        bool VulkanInit(int screenWidth, int screenHeight);

        void OpenGlUnload();
        void VulkanUnload();



    protected:
        GameRenderer m_GameRenderer;

        SDL_Window* m_SdlWindow;
        int m_ScreenWidth;
        int m_ScreenHeight;
    };
}

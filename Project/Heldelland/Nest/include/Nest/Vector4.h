
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__VECTOR4_H
#define NEST__VECTOR4_H



namespace Nest
{
    template <typename NumberType>
    class Vector4
    {
    public:
        Vector4();
        Vector4(const Vector4 &other);
        Vector4(const NumberType elementsToCopy[4]);
        Vector4(const NumberType x, const NumberType y, const NumberType z, const NumberType w);


        inline void Set(NumberType values[4]);
        inline void Set(NumberType x, NumberType y, NumberType z, NumberType w);
        // TODO : Set with initializer list and unit test

        // Operators with other vector4
        inline Vector4& operator +=(const Vector4& other);
        inline Vector4& operator -=(const Vector4& other);

        inline Vector4 operator +(const Vector4& other) const;
        inline Vector4 operator -(const Vector4& other) const;

        
        // Operators with other number types
        inline Vector4& operator *=(const NumberType& v);
        inline Vector4& operator /=(const NumberType& v);

        inline Vector4 operator *(const NumberType& v) const;
        inline Vector4 operator /(const NumberType& v) const;


        // Other math operations
        inline NumberType Dot(const Vector4& other) const;

        // Gets specific elements
        inline NumberType x() const;
        inline NumberType y() const;
        inline NumberType z() const;
        inline NumberType w() const;

        // Sets specific elements
        inline NumberType x(NumberType value);
        inline NumberType y(NumberType value);
        inline NumberType z(NumberType value);
        inline NumberType w(NumberType value);

        // Data storage
        NumberType data[4];


        // Static methods
        static NumberType Dot(const Vector4& left, const Vector4& right);
    };



    template <typename NumberType>
    Vector4<NumberType>::Vector4()
    {
        data[0] = 0;
        data[1] = 0;
        data[2] = 0;
        data[3] = 0;
    }



    template <typename NumberType>
    Vector4<NumberType>::Vector4(const Vector4 &other)
    {
        data[0] = other.data[0];
        data[1] = other.data[1];
        data[2] = other.data[2];
        data[3] = other.data[3];
    }



    template <typename NumberType>
    Vector4<NumberType>::Vector4(const NumberType elementsToCopy[4])
    {
        data[0] = elementsToCopy[0];
        data[1] = elementsToCopy[1];
        data[2] = elementsToCopy[2];
        data[3] = elementsToCopy[3];
    }



    template <typename NumberType>
    Vector4<NumberType>::Vector4(const NumberType x, const NumberType y, const NumberType z, const NumberType w)
    {
        data[0] = x;
        data[1] = y;
        data[2] = z;
        data[3] = w;
    }



    template <typename NumberType>
    inline void Vector4<NumberType>::Set(NumberType values[4])
    {
        data[0] = values[0];
        data[1] = values[1];
        data[2] = values[2];
        data[3] = values[3];
    }



    template <typename NumberType>
    inline void Vector4<NumberType>::Set(NumberType x, NumberType y, NumberType z, NumberType w)
    {
        data[0] = x;
        data[1] = y;
        data[2] = z;
        data[3] = w;
    }



    template <typename NumberType>
    inline Vector4<NumberType>& Vector4<NumberType>::operator +=(const Vector4<NumberType>& other)
    {
        data[0] += other.data[0];
        data[1] += other.data[1];
        data[2] += other.data[2];
        data[3] += other.data[3];

        return(*this);
    }



    template <typename NumberType>
    inline Vector4<NumberType>& Vector4<NumberType>::operator -=(const Vector4<NumberType>& other)
    {
        data[0] -= other.data[0];
        data[1] -= other.data[1];
        data[2] -= other.data[2];
        data[3] -= other.data[3];

        return(*this);
    }



    template <typename NumberType>
    inline Vector4<NumberType> Vector4<NumberType>::operator +(const Vector4& other) const
    {
        NumberType tmp[4]{ data[0] + other.data[0], data[1] + other.data[1], data[2] + other.data[2], data[3] + other.data[3] };
        return(Vector4<NumberType>(tmp));
    }



    template <typename NumberType>
    inline Vector4<NumberType> Vector4<NumberType>::operator -(const Vector4& other) const
    {
        NumberType tmp[4]{ data[0] - other.data[0], data[1] - other.data[1], data[2] - other.data[2], data[3] - other.data[3] };
        return(Vector4<NumberType>(tmp));
    }



    template <typename NumberType>
    inline NumberType Vector4<NumberType>::Dot(const Vector4& other) const
    {
        return(data[0] * other.data[0] + data[1] * other.data[1] + data[2] * other.data[2] + data[3] * other.data[3]);
    }



    template <typename NumberType>
    Vector4<NumberType>& Vector4<NumberType>::operator *=(const NumberType& v)
    {
        data[0] *= v;
        data[1] *= v;
        data[2] *= v;
        data[3] *= v;

        return(*this);
    }



    template <typename NumberType>
    Vector4<NumberType>& Vector4<NumberType>::operator /=(const NumberType& v)
    {
        data[0] /= v;
        data[1] /= v;
        data[2] /= v;
        data[3] /= v;

        return(*this);
    }



    template <typename NumberType>
    Vector4<NumberType> Vector4<NumberType>::operator *(const NumberType& v) const
    {
        Vector4<NumberType> tmp(this->data);
        tmp *= v;
        return(tmp);
    }



    template <typename NumberType>
    Vector4<NumberType> Vector4<NumberType>::operator /(const NumberType& v) const
    {
        Vector4<NumberType> tmp(this->data);
        tmp /= v;
        return(tmp);
    }



    template <typename NumberType>
    inline NumberType Vector4<NumberType>::x() const
    {
        return(data[0]);
    }



    template <typename NumberType>
    inline NumberType Vector4<NumberType>::y() const
    {
        return(data[1]);
    }



    template <typename NumberType>
    inline NumberType Vector4<NumberType>::z() const
    {
        return(data[2]);
    }



    template <typename NumberType>
    inline NumberType Vector4<NumberType>::w() const
    {
        return(data[3]);
    }



    template <typename NumberType>
    inline NumberType Vector4<NumberType>::x(NumberType value)
    {
        return((data[0] += value));
    }



    template <typename NumberType>
    inline NumberType Vector4<NumberType>::y(NumberType value)
    {
        return((data[1] += value));
    }



    template <typename NumberType>
    inline NumberType Vector4<NumberType>::z(NumberType value)
    {
        return((data[2] += value));
    }



    template <typename NumberType>
    inline NumberType Vector4<NumberType>::w(NumberType value)
    {
        return((data[3] += value));
    }



    template <typename NumberType>
    NumberType Vector4<NumberType>::Dot(const Vector4& left, const Vector4& right)
    {
        return(left.data[0] * right.data[0] + left.data[1] * right.data[1] + left.data[2] * right.data[2] + left.data[3] * right.data[3]);
    }
}



#endif

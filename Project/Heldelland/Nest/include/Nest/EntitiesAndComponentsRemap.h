
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__ENTITIES_AND_COMPONENTS_REMAP_H
#define NEST__ENTITIES_AND_COMPONENTS_REMAP_H



#include<unordered_map>
#include<memory>

// TODO : pass entity id type to special file in order to easen up the weight of includes
#include"Nest/IEntity.h"

// TODO : pass a component id type to a special file in order to easen up the weight of includes
#include"Nest/Component.h"



namespace Nest
{
	// Forward declarations
	class ComponentReferenceBase;



	// Auxiliate on the handling of mappings "from id to entity/compononent",
	// useful for actions such as components copying or entities clonning.
	class EntitiesAndComponentsRemap
	{
	public:
		// Registers an entity id which can be remapped.
		// Throws LogicErrorException if the @entityId is already registered.
		void RegisterEntity(EntityId entityId);

		// Returns if the given @entityId is registered as remappable.
		bool IsEntityRegistered(EntityId entityId) const;

		// Unregisters an entity from the remappable list.
		// Throws LogicErrorException if no @entityId is registered.
		void UnregisterEntity(EntityId entityId);

		// Registers an component id which can be remapped.
		// Throws LogicErrorException if the @componentId is already registered.
		void RegisterComponent(ComponentId componentId);

		// Returns if the giver @componentId is registered as remappable.
		bool IsComponentRegistered(ComponentId componentId) const;

		// Unregisters an entity from the remappable list.
		// Throws LogicErrorException if no @componentId is registered.
		void UnregisterComponent(ComponentId componentId);



		// Will set all mappings of entities ids to null
		void ClearEntitiesMapping();

		// Will set all mappings of components to null
		void ClearComponentsMapping();

		// Short to ClearEntitiesMapping() and ClearComponentsMapping()
		void ClearMappings();



		// Maps a registered @sourceEntityId to a @targetEntity.
		// If @sourceEntityId is not registered, LogicErrorException is thrown.
		void MapEntity(EntityId sourceEntityId, std::weak_ptr<IEntity> targetEntity);

		// Verifies if a registered @sourceEntityId refers to a non-expired entity.
		// If @sourceEntityId is not registered, LogicErrorException is thrown.
		bool IsEntityMapped(EntityId sourceEntityId) const;

		// Returns the mapped entity tied to a registered @sourceEntityId, even if it is expired.
		// If @sourceEntityId is not registered, LogicErrorException is thrown.
		std::weak_ptr<IEntity> GetMappedEntity(EntityId sourceEntityId) const;



		void MapComponent(ComponentId sourceComponentId, ComponentReferenceBase& targetComponentReference);
		bool IsComponentMapped(ComponentId sourceComponentId) const;
		ComponentReferenceBase* GetMappedComponent(ComponentId sourceComponentId) const;



	protected:
		std::unordered_map<EntityId, std::weak_ptr<IEntity>> m_EntitiesMap;
		std::unordered_map<ComponentId, std::unique_ptr<ComponentReferenceBase>> m_ComponentsMap;
	};
}



#endif

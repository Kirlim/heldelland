
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#ifdef DISABLINGTHISFILEFORNOW

#include<memory>

#define VK_USE_PLATFORM_WIN32_KHR
#define VK_NO_PROTOTYPES            // do not declare prototypes, so I can load dynamically!
#include<string>
#include<vulkan.h>

#include"Nest/Log.h"



struct VulkanContext
{
    uint32_t width;
    uint32_t height;

    uint32_t presentQueueIdx;
    VkQueue presentQueue;

    VkInstance vkInstance;

    VkSurfaceKHR surface;
    VkSwapchainKHR swapChain;

    VkImage* presentImages;
    VkImage depthImage;
    VkFramebuffer* framebuffers;

    VkPhysicalDevice physicalDevice;
    VkPhysicalDeviceProperties physicalDeviceProperties;
    VkPhysicalDeviceMemoryProperties physicalDeviceMemoryProperties;
    
    VkDevice device;

    VkCommandBuffer setupCmdBuffer;
    VkCommandBuffer drawCmdBuffer;

    VkDebugReportCallbackEXT debugCallback;
};

VulkanContext vulkanContext;



void MyAssert(bool flag, const char* msg = "")
{
    if (!flag)
    {
        Nest::Log::Error("ASSERT: " + std::string(msg) + "\n");
        int *base = 0;
        *base = 1;
    }
}



void CheckVulkanResult(const VkResult& result, const char* msg)
{
    MyAssert(result == VK_SUCCESS, msg);
}



PFN_vkAllocateCommandBuffers vkAllocateCommandBuffers;
PFN_vkBeginCommandBuffer vkBeginCommandBuffer = nullptr;
PFN_vkCmdPipelineBarrier vkCmdPipelineBarrier = nullptr;
PFN_vkCreateCommandPool vkCreateCommandPool = nullptr;
PFN_vkCreateDevice vkCreateDevice = nullptr;
PFN_vkCreateFence vkCreateFence = nullptr;
PFN_vkCreateImageView vkCreateImageView = nullptr;
PFN_vkCreateInstance vkCreateInstance = nullptr;
PFN_vkEndCommandBuffer vkEndCommandBuffer = nullptr;
PFN_vkEnumerateInstanceExtensionProperties vkEnumerateInstanceExtensionProperties = nullptr;
PFN_vkEnumeratePhysicalDevices vkEnumeratePhysicalDevices = nullptr;
PFN_vkEnumerateInstanceLayerProperties vkEnumerateInstanceLayerProperties = nullptr;
PFN_vkGetDeviceQueue vkGetDeviceQueue = nullptr;
PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr = nullptr;
PFN_vkGetPhysicalDeviceProperties vkGetPhysicalDeviceProperties = nullptr;
PFN_vkGetPhysicalDeviceQueueFamilyProperties vkGetPhysicalDeviceQueueFamilyProperties = nullptr;
PFN_vkQueueSubmit vkQueueSubmit = nullptr;
PFN_vkResetCommandBuffer vkResetCommandBuffer = nullptr;
PFN_vkResetFences vkResetFences = nullptr;
PFN_vkWaitForFences vkWaitForFences = nullptr;



void win32_LoadVulkan()
{
    // Initialize modules
    HMODULE vulkanModule = LoadLibrary("vulkan-1.dll");
    MyAssert(vulkanModule == NULL, "Failed to load vulkan module");


    // Load Vulkan functions

    vkAllocateCommandBuffers = (PFN_vkAllocateCommandBuffers)GetProcAddress(vulkanModule, "vkAllocateCommandBuffers");
    MyAssert(vkAllocateCommandBuffers != NULL, "Failed to load vkAllocateCommandBuffers");

    vkBeginCommandBuffer = (PFN_vkBeginCommandBuffer)GetProcAddress(vulkanModule, "vkBeginCommandBuffer");
    MyAssert(vkBeginCommandBuffer != NULL, "Failed to load vkBeginCommandBuffer");

    vkCmdPipelineBarrier = (PFN_vkCmdPipelineBarrier)GetProcAddress(vulkanModule, "vkCmdPipelineBarrier");
    MyAssert(vkCmdPipelineBarrier != NULL, "Failed to load vkCmdPipelineBarrier");

    vkCreateCommandPool = (PFN_vkCreateCommandPool)GetProcAddress(vulkanModule, "vkCreateCommandPool");
    MyAssert(vkCreateCommandPool != NULL, "Failed to load vkCreateCommandPool");

    vkCreateDevice = (PFN_vkCreateDevice)GetProcAddress(vulkanModule, "vkCreateDevice");
    MyAssert(vkCreateDevice != NULL, "Failed to load vkCreateDevice");

    vkCreateFence = (PFN_vkCreateFence)GetProcAddress(vulkanModule, "vkCreateFence");
    MyAssert(vkCreateFence != NULL, "Failed to load vkCreateFence");

    vkCreateImageView = (PFN_vkCreateImageView)GetProcAddress(vulkanModule, "vkCreateImageView");
    MyAssert(vkCreateImageView != NULL, "Failed to load vkCreateImageView");

    vkCreateInstance = (PFN_vkCreateInstance)GetProcAddress(vulkanModule, "vkCreateInstance");
    MyAssert(vkCreateInstance != NULL, "Failed to load vkCreateInstance function pointer.");

    vkEndCommandBuffer = (PFN_vkEndCommandBuffer)GetProcAddress(vulkanModule, "vkEndCommandBuffer");
    MyAssert(vkEndCommandBuffer != NULL, "Failed to load vkEndCommandBuffer");

    vkEnumerateInstanceExtensionProperties = (PFN_vkEnumerateInstanceExtensionProperties)GetProcAddress(vulkanModule, "vkEnumerateInstanceExtensionProperties");
    MyAssert(vkEnumerateInstanceExtensionProperties != NULL, "Failed to load vkEnumerateInstanceExtensionProperties");

    vkEnumerateInstanceLayerProperties = (PFN_vkEnumerateInstanceLayerProperties)GetProcAddress(vulkanModule, "vkEnumerateInstanceLayerProperties");
    MyAssert(vkEnumerateInstanceLayerProperties != NULL, "Failed to load vkEnumerateInstanceLayerProperties function pointer.");

    vkEnumeratePhysicalDevices = (PFN_vkEnumeratePhysicalDevices)GetProcAddress(vulkanModule, "vkEnumeratePhysicalDevices");
    MyAssert(vkEnumeratePhysicalDevices != NULL, "Failed to load vkEnumeratePhysicalDevices");

    vkGetDeviceQueue = (PFN_vkGetDeviceQueue)GetProcAddress(vulkanModule, "vkGetDeviceQueue");
    MyAssert(vkGetDeviceQueue != NULL, "Failed to load vkGetDeviceQueue");

    vkGetInstanceProcAddr = (PFN_vkGetInstanceProcAddr)GetProcAddress(vulkanModule, "vkGetInstanceProcAddr");
    MyAssert(vkGetInstanceProcAddr != NULL, "Failed to load vkGetInstanceProcAddr");

    vkGetPhysicalDeviceProperties = (PFN_vkGetPhysicalDeviceProperties)GetProcAddress(vulkanModule, "vkGetPhysicalDeviceProperties");
    MyAssert(vkGetPhysicalDeviceProperties != NULL, "Failed to load vkGetPhysicalDeviceProperties");

    vkGetPhysicalDeviceQueueFamilyProperties = (PFN_vkGetPhysicalDeviceQueueFamilyProperties)GetProcAddress(vulkanModule, "vkGetPhysicalDeviceQueueFamilyProperties");
    MyAssert(vkGetPhysicalDeviceQueueFamilyProperties != NULL, "Failed to load vkGetPhysicalDeviceQueueFamilyProperties");

    vkResetCommandBuffer = (PFN_vkResetCommandBuffer)GetProcAddress(vulkanModule, "vkResetCommandBuffer");
    MyAssert(vkResetCommandBuffer != NULL, "Failed to load vkResetCommandBuffer");

    vkResetFences = (PFN_vkResetFences)GetProcAddress(vulkanModule, "vkResetFences");
    MyAssert(vkResetFences != NULL, "Failed to load vkResetFences");

    vkQueueSubmit = (PFN_vkQueueSubmit)GetProcAddress(vulkanModule, "vkQueueSubmit");
    MyAssert(vkQueueSubmit != NULL, "Failed to load vkQueueSubmit");

    vkWaitForFences = (PFN_vkWaitForFences)GetProcAddress(vulkanModule, "vkWaitForFences");
    MyAssert(vkWaitForFences != NULL, "Failed to load vkWaitForFences");
}



// Debug extension
PFN_vkCreateDebugReportCallbackEXT vkCreateDebugReportCallbackEXT = nullptr;
PFN_vkDestroyDebugReportCallbackEXT vkDestroyDebugReportCallbackEXT = nullptr;
PFN_vkDebugReportMessageEXT vkDebugReportMessageEXT = nullptr;


// Windows platform
PFN_vkCreateWin32SurfaceKHR vkCreateWin32SurfaceKHR = nullptr;
PFN_vkGetPhysicalDeviceSurfaceSupportKHR vkGetPhysicalDeviceSurfaceSupportKHR = nullptr;
PFN_vkGetPhysicalDeviceSurfaceFormatsKHR vkGetPhysicalDeviceSurfaceFormatsKHR = nullptr;
PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR vkGetPhysicalDeviceSurfaceCapabilitiesKHR = nullptr;
PFN_vkGetPhysicalDeviceSurfacePresentModesKHR vkGetPhysicalDeviceSurfacePresentModesKHR = nullptr;


// Swapchain extension
PFN_vkCreateSwapchainKHR vkCreateSwapchainKHR;
PFN_vkGetSwapchainImagesKHR vkGetSwapchainImagesKHR;



void win32_LoadVulkanExtensions(VulkanContext& context)
{
    *(void **)&vkCreateDebugReportCallbackEXT = vkGetInstanceProcAddr(context.vkInstance, "vkCreateDebugReportCallbackEXT");
    *(void **)&vkDestroyDebugReportCallbackEXT = vkGetInstanceProcAddr(context.vkInstance, "vkDestroyDebugReportCallbackEXT");
    *(void **)&vkDebugReportMessageEXT = vkGetInstanceProcAddr(context.vkInstance, "vkDebugReportMessageEXT");

    *(void **)&vkCreateSwapchainKHR = vkGetInstanceProcAddr(context.vkInstance, "vkCreateSwapchainKHR");
    *(void **)&vkCreateWin32SurfaceKHR = vkGetInstanceProcAddr(context.vkInstance, "vkCreateWin32SurfaceKHR");
    *(void **)&vkGetPhysicalDeviceSurfaceSupportKHR = vkGetInstanceProcAddr(context.vkInstance, "vkGetPhysicalDeviceSurfaceSupportKHR");
    *(void **)&vkGetPhysicalDeviceSurfaceFormatsKHR = vkGetInstanceProcAddr(context.vkInstance, "vkGetPhysicalDeviceSurfaceFormatsKHR");
    *(void **)&vkGetPhysicalDeviceSurfaceCapabilitiesKHR = vkGetInstanceProcAddr(context.vkInstance, "vkGetPhysicalDeviceSurfaceCapabilitiesKHR");
    *(void **)&vkGetPhysicalDeviceSurfacePresentModesKHR = vkGetInstanceProcAddr(context.vkInstance, "vkGetPhysicalDeviceSurfacePresentModesKHR");
    *(void **)&vkGetSwapchainImagesKHR = vkGetInstanceProcAddr(context.vkInstance, "vkGetSwapchainImagesKHR");
}



VKAPI_ATTR VkBool32 VKAPI_CALL MyDebugReportCallback(
    VkDebugReportFlagsEXT flags,
    VkDebugReportObjectTypeEXT objectType, uint64_t object, size_t location,
    int32_t messageCode, const char* pLayerPrefix, const char* pMessage, void* pUserData
    )
{
    OutputDebugStringA(pLayerPrefix);
    OutputDebugStringA(" ");
    OutputDebugStringA(pMessage);
    OutputDebugStringA("\n");
    return(VK_FALSE);
}



void InitVulkan(int width, int height, HWND windowHandle, HINSTANCE hInstance)
{
    VkResult result;

    vulkanContext.width = width;
    vulkanContext.height = height;

    // Validation layers
    uint32_t layerCount = 0;
    vkEnumerateInstanceLayerProperties(&layerCount, NULL);
    VkLayerProperties* layersAvailable = new VkLayerProperties[layerCount];
    vkEnumerateInstanceLayerProperties(&layerCount, layersAvailable);

    // TODO : When do we delete the layers available structure?

    bool foundValidation = false;
    for (unsigned int i = 0; i < layerCount; ++i)
    {
        if (strcmp(layersAvailable[i].layerName, "VK_LAYER_LUNARG_standard_validation") == 0)
        {
            foundValidation = true;
        }
    }
    MyAssert(foundValidation, "Could not find validation layer.");
    const char* layers[] = { "VK_LAYER_LUNARG_standard_validation" };


    // Extensions
    uint32_t extensionsCount = 0;
    vkEnumerateInstanceExtensionProperties(NULL, &extensionsCount, NULL);
    VkExtensionProperties* extensionsAvailable = new VkExtensionProperties[extensionsCount];
    vkEnumerateInstanceExtensionProperties(NULL, &extensionsCount, extensionsAvailable);

    // TODO : when do we delete the extensions available structure?

    const char* extensions[] = { "VK_KHR_surface", "VK_KHR_win32_surface", "VK_EXT_debug_report" };
    uint32_t extensionsFound = 0;
    for (unsigned int i = 0; i < extensionsCount; ++i)
    {
        if (strcmp(extensionsAvailable[i].extensionName, extensions[i]) == 0)
        {
            ++extensionsFound;
        }
    }
    MyAssert(extensionsFound == 3, "Could not find debug extension.");


    // Continue initializing Vulkan

    VkApplicationInfo applicationInfo = {};
    applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    applicationInfo.pNext = NULL;
    applicationInfo.pApplicationName = "Unnamed";
    applicationInfo.pEngineName = NULL;
    applicationInfo.engineVersion = 1;
    applicationInfo.apiVersion = VK_MAKE_VERSION(1, 0, 0);

    VkInstanceCreateInfo instanceInfo = {};
    instanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceInfo.pApplicationInfo = &applicationInfo;
    instanceInfo.enabledLayerCount = 1;
    instanceInfo.ppEnabledLayerNames = layers;
    instanceInfo.enabledExtensionCount = 3;
    instanceInfo.ppEnabledExtensionNames = extensions;

    result = vkCreateInstance(&instanceInfo, NULL, &vulkanContext.vkInstance);
    CheckVulkanResult(result, "Failed to create vulkan instance.");


    //
    win32_LoadVulkanExtensions(vulkanContext);


    // Set up debug callbacks
    VkDebugReportCallbackCreateInfoEXT callbackCreateInfo = {};
    callbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
    callbackCreateInfo.flags =
        VK_DEBUG_REPORT_ERROR_BIT_EXT |
        VK_DEBUG_REPORT_WARNING_BIT_EXT |
        VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
    callbackCreateInfo.pfnCallback = &MyDebugReportCallback;
    callbackCreateInfo.pUserData = NULL;

    result = vkCreateDebugReportCallbackEXT(vulkanContext.vkInstance, &callbackCreateInfo, NULL, &vulkanContext.debugCallback);
    CheckVulkanResult(result, "Failed to create debug report callback.");


    // Get a Windows surface to render into
    VkWin32SurfaceCreateInfoKHR surfaceCreateInfo = {};
    surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
    surfaceCreateInfo.hinstance = hInstance;
    surfaceCreateInfo.hwnd = windowHandle;

    result = vkCreateWin32SurfaceKHR(vulkanContext.vkInstance, &surfaceCreateInfo, NULL, &vulkanContext.surface);
    CheckVulkanResult(result, "Failed to create surface.");


    // Find a physical device that has a queue where we can present and do graphics
    uint32_t physicalDevicesCount = 0;
    vkEnumeratePhysicalDevices(vulkanContext.vkInstance, &physicalDevicesCount, NULL);
    std::unique_ptr<VkPhysicalDevice[]> physicalDevices = std::make_unique<VkPhysicalDevice[]>(physicalDevicesCount);
    vkEnumeratePhysicalDevices(vulkanContext.vkInstance, &physicalDevicesCount, physicalDevices.get());

    for (uint32_t i = 0; i < physicalDevicesCount; ++i)
    {
        VkPhysicalDeviceProperties deviceProperties = {};
        vkGetPhysicalDeviceProperties(physicalDevices[i], &deviceProperties);

        uint32_t queueFamilyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevices[i], &queueFamilyCount, NULL);
        std::unique_ptr<VkQueueFamilyProperties[]> queueFamilyProperties = std::make_unique<VkQueueFamilyProperties[]>(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevices[i], &queueFamilyCount, queueFamilyProperties.get());

        for (uint32_t j = 0; j < queueFamilyCount; ++j)
        {
            VkBool32 supportsPresent;
            vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevices[i], j, vulkanContext.surface, &supportsPresent);

            if (supportsPresent && queueFamilyProperties[j].queueFlags & VK_QUEUE_GRAPHICS_BIT)
            {
                vulkanContext.physicalDevice = physicalDevices[i];
                vulkanContext.physicalDeviceProperties = deviceProperties;
                vulkanContext.presentQueueIdx = j;
                break;
            }
        }

        if (vulkanContext.physicalDevice != NULL)
        {
            break;
        }
    }

    MyAssert(vulkanContext.physicalDevice == NULL, "No physical device present that can render and present Vulkan.");


    // Create our logical device
    VkDeviceQueueCreateInfo queueCreateInfo = {};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = vulkanContext.presentQueueIdx;
    queueCreateInfo.queueCount = 1;
    float queuePriorities[] = { 1.0f };
    queueCreateInfo.pQueuePriorities = queuePriorities;

    VkDeviceCreateInfo deviceInfo = {};
    deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceInfo.queueCreateInfoCount = 1;
    deviceInfo.pQueueCreateInfos = &queueCreateInfo;
    deviceInfo.enabledLayerCount = 1;
    deviceInfo.ppEnabledLayerNames = layers;

    const char* deviceExtensions[] = { "VK_KHR_swapchain" };
    deviceInfo.enabledExtensionCount = 1;
    deviceInfo.ppEnabledExtensionNames = deviceExtensions;

    VkPhysicalDeviceFeatures deviceFeatures = {};
    deviceFeatures.shaderClipDistance = VK_TRUE;
    deviceInfo.pEnabledFeatures = &deviceFeatures;

    result = vkCreateDevice(vulkanContext.physicalDevice, &deviceInfo, NULL, &vulkanContext.device);
    CheckVulkanResult(result, "Failed to create Vulkan logical device.");


    // Get the device queue
    vkGetDeviceQueue(vulkanContext.device, vulkanContext.presentQueueIdx, 0, &vulkanContext.presentQueue);

    // Create our command buffers
    VkCommandPoolCreateInfo commandPoolCreateInfo = {};
    commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    commandPoolCreateInfo.queueFamilyIndex = vulkanContext.presentQueueIdx;

    VkCommandPool commandPool;
    result = vkCreateCommandPool(vulkanContext.device, &commandPoolCreateInfo, NULL, &commandPool);
    CheckVulkanResult(result, "Failed to create the command pool.");

    VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
    commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    commandBufferAllocateInfo.commandPool = commandPool;
    commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    commandBufferAllocateInfo.commandBufferCount = 1;

    result = vkAllocateCommandBuffers(vulkanContext.device, &commandBufferAllocateInfo, &vulkanContext.setupCmdBuffer);
    CheckVulkanResult(result, "Failed to allocate setup command buffer.");

    result = vkAllocateCommandBuffers(vulkanContext.device, &commandBufferAllocateInfo, &vulkanContext.drawCmdBuffer);
    CheckVulkanResult(result, "Failed to allocate draw command buffer.");


    // Swap Chain creation
    uint32_t formatCount = 0;
    vkGetPhysicalDeviceSurfaceFormatsKHR(vulkanContext.physicalDevice, vulkanContext.surface, &formatCount, NULL);
    std::unique_ptr<VkSurfaceFormatKHR[]> surfaceFormats = std::make_unique<VkSurfaceFormatKHR[]>(formatCount);
    vkGetPhysicalDeviceSurfaceFormatsKHR(vulkanContext.physicalDevice, vulkanContext.surface, &formatCount, surfaceFormats.get());

    // If the format list includes just one enty of VK_FORMAT_UNDEFINED, the surface has no preferred format;
    // otherwise, at least one supported format will be returned
    VkFormat colorFormat;
    if (formatCount == 1 && surfaceFormats[0].format == VK_FORMAT_UNDEFINED)
    {
        colorFormat = VK_FORMAT_B8G8R8A8_UNORM;
    }
    else
    {
        colorFormat = surfaceFormats[0].format;
    }
    VkColorSpaceKHR colorSpace;
    colorSpace = surfaceFormats[0].colorSpace;


    VkSurfaceCapabilitiesKHR surfaceCapabilities = {};
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vulkanContext.physicalDevice, vulkanContext.surface, &surfaceCapabilities);

    // If surfaceCapabilities.maxImageCount == 0 there is actually no limit on the number of images.
    uint32_t desiredImageCount = 2;
    if (desiredImageCount < surfaceCapabilities.minImageCount)
    {
        desiredImageCount = surfaceCapabilities.minImageCount;
    }
    else if (surfaceCapabilities.maxImageCount != 0 && desiredImageCount > surfaceCapabilities.maxImageCount)
    {
        desiredImageCount = surfaceCapabilities.maxImageCount;
    }


    VkExtent2D surfaceResolution = surfaceCapabilities.currentExtent;
    if (surfaceResolution.width == -1)
    {
        surfaceResolution.width = vulkanContext.width;
        surfaceResolution.height = vulkanContext.height;
    }
    else
    {
        vulkanContext.width = surfaceResolution.width;
        vulkanContext.height = surfaceResolution.height;
    }


    VkSurfaceTransformFlagBitsKHR preTransform = surfaceCapabilities.currentTransform;
    if (surfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
    {
        preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    }


    uint32_t presentModeCount = 0;
    vkGetPhysicalDeviceSurfacePresentModesKHR(vulkanContext.physicalDevice, vulkanContext.surface, &presentModeCount, NULL);
    std::unique_ptr<VkPresentModeKHR[]> presentModes = std::make_unique<VkPresentModeKHR[]>(presentModeCount);
    vkGetPhysicalDeviceSurfacePresentModesKHR(vulkanContext.physicalDevice, vulkanContext.surface, &presentModeCount, presentModes.get());

    VkPresentModeKHR presentationMode = VK_PRESENT_MODE_FIFO_KHR; // Always supported
    for (uint32_t i = 0; i < presentModeCount; ++i)
    {
        if (presentModes[i] == VK_PRESENT_MODE_MAILBOX_KHR)
        {
            presentationMode = VK_PRESENT_MODE_MAILBOX_KHR;
        }
    }


    VkSwapchainCreateInfoKHR swapChainCreateInfo = {};
    swapChainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapChainCreateInfo.surface = vulkanContext.surface;
    swapChainCreateInfo.minImageCount = desiredImageCount;
    swapChainCreateInfo.imageFormat = colorFormat;
    swapChainCreateInfo.imageColorSpace = colorSpace;
    swapChainCreateInfo.imageExtent = surfaceResolution;
    swapChainCreateInfo.imageArrayLayers = 1;
    swapChainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    swapChainCreateInfo.preTransform = preTransform;
    swapChainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swapChainCreateInfo.presentMode = presentationMode;
    swapChainCreateInfo.clipped = true;
    swapChainCreateInfo.oldSwapchain = NULL;

    result = vkCreateSwapchainKHR(vulkanContext.device, &swapChainCreateInfo, NULL, &vulkanContext.swapChain);
    CheckVulkanResult(result, "Failed to create Swap Chain.");


    uint32_t imageCount = 0;
    vkGetSwapchainImagesKHR(vulkanContext.device, vulkanContext.swapChain, &imageCount, NULL);
    vulkanContext.presentImages = new VkImage[imageCount];
    vkGetSwapchainImagesKHR(vulkanContext.device, vulkanContext.swapChain, &imageCount, vulkanContext.presentImages);

    // Create VkImageView for our swap chaing VkImage buffers
    VkImageViewCreateInfo presentImagesViewCreateInfo = {};
    presentImagesViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    presentImagesViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    presentImagesViewCreateInfo.format = colorFormat;
    presentImagesViewCreateInfo.components = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };
    presentImagesViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    presentImagesViewCreateInfo.subresourceRange.baseMipLevel = 0;
    presentImagesViewCreateInfo.subresourceRange.levelCount = 1;
    presentImagesViewCreateInfo.subresourceRange.baseArrayLayer = 0;
    presentImagesViewCreateInfo.subresourceRange.layerCount = 1;

    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    VkFenceCreateInfo fenceCreateInfo = {};
    fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    VkFence submitFence;
    vkCreateFence(vulkanContext.device, &fenceCreateInfo, NULL, &submitFence);

    // TODO : continue from tutorial



    // TODO : when to unload vulkanContext.presentImages?
    // TODO : VkInstanceDestroy
}



void UnloadVulkan()
{
    vkDestroyDebugReportCallbackEXT(vulkanContext.vkInstance, vulkanContext.debugCallback, NULL);
}

#endif

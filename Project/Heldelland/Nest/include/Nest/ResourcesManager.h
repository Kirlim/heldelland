
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__RESOURCES_MANAGER_H
#define NEST__RESOURCES_MANAGER_H



#include<string>
#include<memory>
#include<unordered_map>

#include"Nest/IResourcesManager.h"

#include"Nest/Font.h"
#include"Nest/Material.h"
#include"Nest/Mesh.h"
#include"Nest/Model.h"
#include"Nest/ShaderProgram.h"
#include"Nest/Texture.h"



namespace Nest
{
    class ResourcesManager : public IResourcesManager
    {
    public:
        virtual ~ResourcesManager();


        
    protected:
        std::unordered_map<std::string, std::unique_ptr<Font>> m_FontsData;
        std::unordered_map<std::string, std::unique_ptr<Material>> m_MaterialsData;
        std::unordered_map<std::string, std::unique_ptr<Mesh>> m_MeshesData;
        std::unordered_map<std::string, std::unique_ptr<Model>> m_ModelsData;
        std::unordered_map<std::string, std::unique_ptr<ShaderProgram>> m_ShaderProgramsData;
        std::unordered_map<std::string, std::unique_ptr<Texture>> m_TexturesData;


        Font* DoFetchFont(const std::string resourceName) const override final;
        Material* DoFetchMaterial(const std::string resourceName) const override final;
        Mesh* DoFetchMesh(const std::string resourceName) const override final;
        Model* DoFetchModel(const std::string resourceName) const override final;
        ShaderProgram* DoFetchShaderProgram(const std::string resourceName) const override final;
        Texture* DoFetchTexture(const std::string resourceName) const override final;

        virtual void DoAddFont(const std::string resourceName, std::unique_ptr<Font>& font) override final;
        virtual void DoUnloadFont(const std::string resourceName) override final;

        virtual void DoAddMaterial(const std::string resourceName, std::unique_ptr<Material>& material) override final;
        virtual void DoUnloadMaterial(const std::string resourceName) override final;

        virtual void DoAddMesh(std::string resourceName, std::unique_ptr<Mesh>& mesh) override final;
        virtual void DoUnloadMesh(std::string resourceName) override final;

        virtual void DoAddModel(const std::string resourceName, std::unique_ptr<Model>& model) override final;
        virtual void DoUnloadModel(const std::string resourceName) override final;

        virtual void DoLoadShaderProgram(const std::string vertexShaderFile, const std::string fragmentShaderFile, const std::string resourceName) override final;
        virtual void DoLoadShaderProgramContents(const std::string vertexShaderContents, const std::string fragmentShaderContents, const std::string resourceName) override final;
        virtual void DoAddShaderProgram(const std::string resourceName, std::unique_ptr<ShaderProgram>& shaderProgram) override final;
        virtual void DoUnloadShaderProgram(const std::string resourceName) override final;

        virtual void DoAddTexture(const std::string resourceName, std::unique_ptr<Texture>& texture) final;
        virtual void DoUnloadTexture(const std::string resourceName) final;
    };
}



#endif

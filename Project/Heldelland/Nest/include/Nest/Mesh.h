
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__MESH_H
#define NEST__MESH_H

#include<vector>

#include"Nest/Vector2.h"
#include"Nest/Vector3.h"



namespace Nest
{
	class Mesh
	{
	public:
		Mesh() = default;
		virtual ~Mesh() = default;


		void Make(
			const std::vector<Vector3<float>>& vertices,
			const std::vector<Vector3<float>>& normals,
			const std::vector<Vector2<float>>& uvs,
			const std::vector<unsigned int>& triangles
		);
		void Unload();

		void Render() const;

		bool IsLoaded() const;



	private:
		virtual void DoMake(
			const std::vector<Vector3<float>>& vertices,
			const std::vector<Vector3<float>>& normals,
			const std::vector<Vector2<float>>& uvs,
			const std::vector<unsigned int>& triangles
		) = 0;
		virtual void DoUnload() = 0;
		virtual void DoRender() const = 0;
		virtual bool DoIsLoaded() const = 0;
	};
}



#endif

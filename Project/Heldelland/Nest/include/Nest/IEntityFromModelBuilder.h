
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__I_ENTITY_FROM_MODEL_BUILDER_H
#define NEST__I_ENTITY_FROM_MODEL_BUILDER_H



namespace Nest
{
	class IEntity;
	class Model;
	class ModelNode;



	class IEntityFromModelBuilder
	{
	public:
		virtual ~IEntityFromModelBuilder();
		IEntity* BuildVisualEntity(const Model& model);



	private:
		virtual IEntity* DoBuildVisualEntity(const ModelNode& modelRootNode) = 0;
	};
}



#endif

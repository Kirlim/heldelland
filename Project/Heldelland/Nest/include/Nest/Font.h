//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__FONT_H
#define NEST__FONT_H

#include<string>
#include<memory>
#include<unordered_map>

#include<ft2build.h>
#include FT_FREETYPE_H


#include<Nest/Vector2.h>
#include<Nest/Texture.h>



namespace Nest
{
	class IResourcesManager;



	class Font
	{
	public:
		explicit Font(IResourcesManager& resourcesManager);
		Font(const Font&) = delete;
		Font(Font&&) = default;

		~Font();

		Font& operator=(const Font&) = delete;
		Font& operator=(Font&&) = default;

		// Loads a font into memory. Each font character will have its texture
		// loaded with resource name fontName[character].
		void LoadFont(std::string fontName, std::string ttfFile);

		// Unloads this font, and all of its textures set in the resources manager.
		void UnloadFont();

		// Obtains information about a character. outSize, outBearing and outAdvance will contain
		// requested data if the method returns true. If it returns false, they'll keep the values
		// they already had.
		bool GetCharacterInfo(unsigned int character, Vector2<int>& outSize, Vector2<int>& outBearing, int& outAdvance) const;

		// Returns wether the fonst is already loaded with data.
		bool IsLoaded() const;

		// Returns the texture of the requested character, or nullptr if not found.
		Texture* GetCharacterTexture(unsigned int character) const;



	private:
		std::string MakeTextureName(unsigned int character) const;
		void LoadCharacter(unsigned int character, FT_Face& face);



	private:
		IResourcesManager& m_ResourcesManager;

		struct CharacterInfo
		{
			Vector2<int> size;
			Vector2<int> bearing;
			int advance = 0;

			Texture* texture = nullptr;
		};

		std::string m_FontName;
		std::unordered_map<unsigned int, std::unique_ptr<CharacterInfo>> m_Characters;

		bool m_IsLoaded;
	};
}



#endif

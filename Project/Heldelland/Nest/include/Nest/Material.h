
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<string>
#include<unordered_map>
#include<GL/glew.h>
#include"Nest/Texture.h"
#include"Nest/ShaderProgram.h"



namespace Nest
{
    class Material
    {
    public:
        Material();
        ~Material();

        void SetShaderProgram(ShaderProgram* shaderProgram);
        ShaderProgram* GetShaderProgram() const;

        void SetMainTexture(const Texture* texture);
        void SetTexture(std::string slotName, const Texture* texture);

        // Loads the shader program and other configurations
        // for the rendering system (such as setting states and
        // attributes)
        void LoadForRendering();

        // Reset changes made for rendering this material
        void UnloadFromRendering();

        void Clear();



    protected:
        ShaderProgram* m_ShaderProgram;
        std::unordered_map<GLint, const Texture*> m_TexturesSlots; // Slots are shader program texture indexes
    };
}

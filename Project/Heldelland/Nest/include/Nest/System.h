
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__SYSTEM_H
#define NEST__SYSTEM_H



namespace Nest
{
	class System
	{
	public:
		System();
		System(const System&) = delete;
		System(System&&) = default;

		System& operator=(const System&) = delete;
		System& operator=(System&&) = default;

		virtual ~System();

		void SetExecutionOrder(int newExecutionOrder);
		int GetExecutionOrder() const;

		// FixedUpdate tries to be called a fixed number of times per second, sacrificing
		// rendering and Update calls in-between. This method is useful for game logic that
		// needs to be deterministic.
		void FixedUpdate(float fixedDeltaTime);

		// Update is called between rendering frames. It is possible that multiple FixedUpdate
		// will be called before Update has a chance of being called again.
		void Update(float deltaTime);



	private:
		virtual void DoFixedUpdate(float fixedDeltaTime) {};
		virtual void DoUpdate(float deltaTime) {};



	private:
		int m_ExecutionOrder;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EGGS__YAML_ENTITIES_LOADER__LOADERS__YAML_VECTOR3_LOADER_H
#define NEST__EGGS__YAML_ENTITIES_LOADER__LOADERS__YAML_VECTOR3_LOADER_H



namespace YAML
{
	// Forward declarations
	class Node;
}



namespace Nest
{
	// Forward declarations
	template <typename NumberType>
	class Vector3;

	namespace Eggs
	{
		namespace YamlEntitiesLoader
		{
			// Attempts to load a Vector3 from a YAML node.
			// throws InvalidDataException if node is not in the correct format.
			// 
			// Order of values is not important. All X, Y and Z must exist.
			// There must be no other tags.
			//
			// NodeName:
			//   X: value
			//   Y: value
			//   Z: value
			Vector3<float> LoadVector3f(const YAML::Node& vectorNode);
		}

		void operator>> (const YAML::Node& node, Vector3<float>& vector);
	}
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EGGS__YAML_ENTITIES_LOADER__LOADERS__YAML_MESH_FILTER_COMPONENT_LOADER_H
#define NEST__EGGS__YAML_ENTITIES_LOADER__LOADERS__YAML_MESH_FILTER_COMPONENT_LOADER_H

#include"Nest/Eggs/YamlEntitiesLoader/YamlComponentLoader.h"



namespace YAML
{
	// Forward declarations
	class Node;
}



namespace Nest
{
	// Forward declarations
	class Component;
	class MeshFilter;
	class IResourcesManager;



	namespace Eggs
	{
		class YamlMeshFilterComponentLoader final : public YamlComponentLoader
		{
		public:
			virtual ~YamlMeshFilterComponentLoader() = default;
			void LoadAttributes(
				Component& component, const YAML::Node& mainComponentNode, 
				const EntitiesAndComponentsRemap& entitiesAndComponentsRemap, const IResourcesManager& resourcesManager
			) override;



		private:
			static void LoadMeshResource(
				MeshFilter& meshFilter, const YAML::Node& meshNode, const IResourcesManager& resourcesManager
			);
		};
	}
}



#endif

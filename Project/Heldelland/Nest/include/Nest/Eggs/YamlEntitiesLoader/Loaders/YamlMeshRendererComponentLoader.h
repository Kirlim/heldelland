
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EGGS__YAML_ENTITIES_LOADER__LOADERS__YAML_MESH_RENDERER_COMPONENT_LOADER_H
#define NEST__EGGS__YAML_ENTITIES_LOADER__LOADERS__YAML_MESH_RENDERER_COMPONENT_LOADER_H

#include"Nest/Eggs/YamlEntitiesLoader/YamlComponentLoader.h"



namespace Nest
{
	class MeshRenderer;



	namespace Eggs
	{
		class YamlMeshRendererComponentLoader final : public YamlComponentLoader
		{
		public:
			~YamlMeshRendererComponentLoader() = default;

			void LoadAttributes(
				Component& component, const YAML::Node& mainComponentNode,
				const EntitiesAndComponentsRemap& entitiesAndComponentsRemap, const IResourcesManager& resourcesManager
			) override;



		private:
			static void LoadMaterial(
				MeshRenderer& meshRenderer, const YAML::Node& mainComponentNode, const IResourcesManager& resourcesManager
			);
			
			static void LoadIsGuiFlag(
				MeshRenderer& meshRenderer, const YAML::Node& mainComponentNode
			);
		};
	}
}

#endif

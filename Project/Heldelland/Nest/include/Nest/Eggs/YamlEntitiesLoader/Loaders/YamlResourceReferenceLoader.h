
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EGGS__YAML_ENTITIES_LOADER__LOADERS__YAML_RESOURCE_REFERENCE_LOADER_H
#define NEST__EGGS__YAML_ENTITIES_LOADER__LOADERS__YAML_RESOURCE_REFERENCE_LOADER_H

#include<string>

#include"Nest/ResourcesTypes.h"



namespace YAML
{
	// Forward declarations
	class Node;
}



namespace Nest
{
	namespace Eggs
	{
		namespace YamlEntitiesLoader
		{
			struct ResourceReference
			{
				const ResourceType resourceType;
				const std::string resourceName;

				ResourceReference(const ResourceType resourceType, const std::string resourceName);
			};

			// Tags must be Type and ResourceName.
			// Boths tags must be available, and no other tags shall be set.
			//
			// AttributeNodeName:
			//     Type: Type #i.e Mesh, Texture
			//     ResourceName: "Name" #will be searched within a resources manager
			ResourceReference LoadResourceReference(const YAML::Node& resourceReferenceNode);
		}
	}
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EGGS__YAML_ENTITIES_LOADER__LOADERS__YAML_TRANSFORM_COMPONENT_LOADER_H
#define NEST__EGGS__YAML_ENTITIES_LOADER__LOADERS__YAML_TRANSFORM_COMPONENT_LOADER_H

#include"Nest/Eggs/YamlEntitiesLoader/YamlComponentLoader.h"



namespace Nest
{
	// Forward declarations
	class Component;
	class Transform;
	class EntitiesAndComponentsRemap;



	namespace Eggs
	{
		//	Transform:
		//		Name: Transform Test
		//		LocalPosition:
		//			X: <number>
		//			Y: <number>
		//			Z: <number>
		//		LocalRotation:
		//			X: <number>
		//			Y: <number>
		//			Z: <number>
		//		LocalScale:
		//			X: <number>
		//			Y: <number>
		//			Z: <number>
		class YamlTransformComponentLoader final : public YamlComponentLoader
		{
		public:
			virtual ~YamlTransformComponentLoader();

			// Loads the Name and Local Position, Rotation and Scale of a transform.
			// This method will not try to set the Parent.
			void LoadAttributes(
				Component& component, const YAML::Node& mainComponentNode, 
				const EntitiesAndComponentsRemap& entitiesAndComponentsRemap, const IResourcesManager& resourcesManager
			) override;



		private:
			void LoadLocalPosition(Transform& transform, const YAML::Node& localPositionNode);
			void LoadLocalRotation(Transform& transform, const YAML::Node& localRotationNode);
			void LoadLocalScale(Transform& transform, const YAML::Node& localScaleNode);
		};
	}
}



#endif

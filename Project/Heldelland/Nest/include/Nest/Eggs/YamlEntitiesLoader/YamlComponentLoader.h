
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EGGS__YAML_ENTITIES_LOADER__YAML_COMPONENT_LOADER_H
#define NEST__EGGS__YAML_ENTITIES_LOADER__YAML_COMPONENT_LOADER_H



namespace YAML
{
	// Forward declaration
	class Node;
}



namespace Nest
{
	// Forward declarations
	class Component;
	class EntitiesAndComponentsRemap;
	class IResourcesManager;



	namespace Eggs
	{
		class YamlComponentLoader
		{
		public:
			virtual ~YamlComponentLoader();
			virtual void LoadAttributes(
				Component& component, const YAML::Node& mainComponentNode, 
				const EntitiesAndComponentsRemap& entitiesAndComponentsRemap, const IResourcesManager& resourcesManager
			) = 0;



		protected:
			void LoadName(Component& component, const YAML::Node& mainComponentNode);
		};
	}
}



#endif

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EGGS__YAML_COMPONENTS_LOADER__YAML_ENTITIES_LOADER_H
#define NEST__EGGS__YAML_COMPONENTS_LOADER__YAML_ENTITIES_LOADER_H

#include<string>
#include<memory>
#include<vector>



namespace Nest
{
	// Forward declarations
	class EntitiesManager;
	class IEntity;
	class IResourcesManager;

	namespace Eggs
	{
		class YamlComponentsLoadersCollection;
	}



	// Helps loading an entity from a yaml file.
	// Structure:
	//
	// Entity:
	//     Name: "Entity Name"				#optional
	//     Components:						#optional. List of components
	//         -   (ComponentTypeName):
	//                 Component Specific Field
	//                 Component Specific Field
	//     Children:						#optional. List of entities
	//         -   Name: "Child Name"
	//             Components:
	//                 -   (ComponentTypeName):
	//                         Component Specific Field
	//                         Component Specific Field
	class YamlEntitiesLoader final
	{
	public:
		YamlEntitiesLoader(
			EntitiesManager& entitiesManager, const IResourcesManager& resourcesManager,
			std::unique_ptr<Eggs::YamlComponentsLoadersCollection>& componentsLoadersCollection
		);
		~YamlEntitiesLoader() = default;

		// Loads an entity from a yaml file.
		// Throws:
		//		- BadFileException: if the file failed to load.
		std::weak_ptr<IEntity> LoadEntity(std::string fileName) const;



	private:
		EntitiesManager& m_EntitiesManager;
		const IResourcesManager& m_ResourcesManager;
		std::unique_ptr<Eggs::YamlComponentsLoadersCollection> m_ComponentsLoadersCollection;
	};
}



#endif

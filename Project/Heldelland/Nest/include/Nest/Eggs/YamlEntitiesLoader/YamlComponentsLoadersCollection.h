
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EGGS__YAML_ENTITIES_LOADER__YAML_COMPONENTS_LOADER_COLLECTION_H
#define NEST__EGGS__YAML_ENTITIES_LOADER__YAML_COMPONENTS_LOADER_COLLECTION_H

#include<memory>
#include<unordered_map>

#include"Nest/ComponentTypeId.h"
#include"Nest/Eggs/YamlEntitiesLoader/YamlComponentLoader.h"



namespace Nest
{
	namespace Eggs
	{
		// Forward declarations
		class YamlComponentLoader;



		// Responsibility:
		//     Act as a container of YamlComponentLoader instances, so that they are easily
		//     accessible to helping classes to construct components from yaml files.
		class YamlComponentsLoadersCollection final
		{
		public:
			YamlComponentsLoadersCollection();

			// Registers a new loader into this collections. This method does not check
			// if the type id is the same of the component loader type.
			// Exceptions:
			//     - NullArgumentException: component pointer is null
			//     - LogicErrorException: type id is already registered
			void Register(const ComponentTypeId componentTypeId, std::unique_ptr<YamlComponentLoader>& componentLoader);

			// Unregisters a loader from this collection.
			// Exceptions:
			//     - LogicErrorException: @componentTypeId is not currently registered.
			void Unregister(const ComponentTypeId componentTypeId);

			bool IsRegistered(const ComponentTypeId componentTypeId) const;

			// Returns the loader registered with @componentTypeId
			YamlComponentLoader* GetLoader(const ComponentTypeId componentTypeId) const;



		protected:
			using ContainerType = std::unordered_map<ComponentTypeId, std::unique_ptr<YamlComponentLoader>>;
			std::unique_ptr<ContainerType> m_Loaders;
		};
	}
}



#endif

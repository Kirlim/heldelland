//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EGGS__COLLISION_SYSTEM__BOX_COLLIDER_SYSTEM_H
#define NEST__EGGS__COLLISION_SYSTEM__BOX_COLLIDER_SYSTEM_H

#include<vector>

#include"Nest/Matrix4.h"

#include"Nest/Eggs/CollisionSystem/ColliderId.h"
#include"Nest/Eggs/CollisionSystem/AABBCollider.h"
#include"Nest/Eggs/CollisionSystem/RaycastHitTime.h"



namespace Nest
{
	struct Ray;
}

namespace Nest::Eggs::CollisionSystem
{
	class AABoxColliderSystem
	{
	public:
		void Register(ColliderId colliderId, AABBColliderInfo colliderInfo);
		void Unregister(ColliderId colliderId);
		bool IsRegistered(ColliderId colliderId) const;

		bool IsApplyTranformEnqueued(ColliderId colliderId) const;
		void AppendCollidersIndexesToVectorPermissive(const std::vector<ColliderId>& ids, std::vector<int>& out_Indexes) const;
		void EnqueueApplyTransformPermissive(ColliderId colliderId, const Matrix4<float>& transformation);

		void ApplyEnqueuedTransforms();


		void RaycastTime(Ray ray, std::vector<RaycastHitTime>& out_AppendHits) const;



	public:
		std::vector<ColliderId> collidersIds;
		std::vector<AABBColliderInfo> collidersInfo;
		std::vector<AABBCollider> colliders;



	private:
		struct ApplyTransformInformation
		{
			std::vector<ColliderId> colliderIds;
			std::vector<Matrix4<float>> transformations;
		};

		ApplyTransformInformation applyTransformInformation;
	};
}



#endif

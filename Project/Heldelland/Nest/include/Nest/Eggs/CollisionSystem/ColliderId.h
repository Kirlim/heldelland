//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EGGS__COLLISION_SYSTEM__COLLIDER_ID_H
#define NEST__EGGS__COLLISION_SYSTEM__COLLIDER_ID_H

#include"Nest/IdType.h"



namespace Nest::Eggs::CollisionSystem
{
	struct ColliderIdTraits
	{
		using underlying_type = uint32_t;
		static constexpr uint32_t TypeMask{ 0xFF000000 };
		static constexpr uint32_t IdMask{ 0x00FFFFFF };

		static constexpr uint32_t AABBType{ 0x01000000 };
	};

	using ColliderId = IdType<ColliderIdTraits>;



	inline ColliderId MakeColliderId(uint32_t type, uint32_t id)
	{
		return ColliderId{ uint32_t{ type | id } };
	}

	inline uint32_t GetColliderType(ColliderId colliderId)
	{
		return (colliderId.value & ColliderIdTraits::TypeMask);
	}

	inline uint32_t GetColliderId(ColliderId colliderId)
	{
		return (colliderId.value & ColliderIdTraits::IdMask);
	}
}



#endif

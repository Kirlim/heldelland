//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EGGS__COLLISION_SYSTEM__AABB_COLLIDER_H
#define NEST__EGGS__COLLISION_SYSTEM__AABB_COLLIDER_H

#include<Nest/Vector3.h>



namespace Nest::Eggs::CollisionSystem
{
	struct AABBColliderInfo
	{
		Vector3f center;
		Vector3f halfSize;
	};



	struct AABBCollider
	{
		Vector3f min;
		Vector3f max;
	};
}



#endif

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EGGS__COLLISION_SYSTEM__RAYCAST_HIT_TIME_H
#define NEST__EGGS__COLLISION_SYSTEM__RAYCAST_HIT_TIME_H

#include<Nest/Eggs/CollisionSystem/ColliderId.h>



namespace Nest::Eggs::CollisionSystem
{
	struct RaycastHitTime
	{
		ColliderId colliderId;
		float time;
	};
}



#endif

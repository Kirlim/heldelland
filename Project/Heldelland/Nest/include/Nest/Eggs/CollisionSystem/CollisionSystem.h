//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EGGS__COLLISION_SYSTEM__COLLISION_SYSTEM_H
#define NEST__EGGS__COLLISION_SYSTEM__COLLISION_SYSTEM_H

#include<memory>

#include"Nest/Exceptions/InvalidArgumentException.h"

#include"Nest/Matrix4.h"
#include"Nest/Ray.h"
#include"Nest/Eggs/CollisionSystem/ColliderId.h"
#include"Nest/Eggs/CollisionSystem/AABBCollider.h"
#include"Nest/Eggs/CollisionSystem/RaycastHitTime.h"



namespace Nest::Eggs::CollisionSystem
{
	template <typename AABBColliderSystemType>
	class CollisionSystem
	{
	public:
		CollisionSystem();
		CollisionSystem(const CollisionSystem&) = delete;
		CollisionSystem(CollisionSystem&&) = default;

		~CollisionSystem();

		CollisionSystem& operator=(const CollisionSystem&) = delete;
		CollisionSystem& operator=(CollisionSystem&&) = default;


		void Register(ColliderId colliderId, AABBColliderInfo colliderInfo);
		void Unregister(ColliderId colliderId);
		bool IsRegistered(ColliderId colliderId) const;


		void EnqueueApplyTransformation(ColliderId colliderId, Matrix4<float> transformation) const;
		void ApplyAllEnqueuedTransformations() const;

		std::vector<RaycastHitTime> RaycastTime(Ray ray) const;


		AABBColliderSystemType* GetAxisAlignedBoxColliderSystem() const;



	private:
		std::unique_ptr<AABBColliderSystemType> m_AABoxColliderSystem;
	};



	template <typename AABoxColliderSystemType>
	CollisionSystem<AABoxColliderSystemType>::CollisionSystem() :
		m_AABoxColliderSystem(std::make_unique<AABoxColliderSystemType>())
	{
	}



	template <typename AABoxColliderSystemType>
	CollisionSystem<AABoxColliderSystemType>::~CollisionSystem() = default;



	template <typename AABoxColliderSystemType>
	void CollisionSystem<AABoxColliderSystemType>::Register(ColliderId colliderId, AABBColliderInfo colliderInfo)
	{
		if (GetColliderType(colliderId) != ColliderIdTraits::AABBType)
			ThrowInvalidArgumentExceptionWithLine("Collider Id must be of AABB type, to register an AABB Collider");

		m_AABoxColliderSystem->Register(colliderId, colliderInfo);
	}



	template <typename AABoxColliderSystemType>
	void CollisionSystem<AABoxColliderSystemType>::Unregister(ColliderId colliderId)
	{
		if (GetColliderType(colliderId) == ColliderIdTraits::AABBType)
		{
			m_AABoxColliderSystem->Unregister(colliderId);
		}
		else
		{
			ThrowInvalidArgumentExceptionWithLine("Invalid Collider Id");
		}
	}



	template <typename AABoxColliderSystemType>
	bool CollisionSystem<AABoxColliderSystemType>::IsRegistered(ColliderId colliderId) const
	{
		return m_AABoxColliderSystem->IsRegistered(colliderId);
	}



	template <typename AABoxColliderSystemType>
	void CollisionSystem<AABoxColliderSystemType>::EnqueueApplyTransformation(ColliderId colliderId, Matrix4<float> transformation) const
	{
		if (GetColliderType(colliderId) == ColliderIdTraits::AABBType)
		{
			m_AABoxColliderSystem->EnqueueApplyTransformPermissive(colliderId, transformation);
		}
		else
		{
			ThrowInvalidArgumentExceptionWithLine("Invalid Collider Id");
		}
	}



	template <typename AABoxColliderSystemType>
	void CollisionSystem<AABoxColliderSystemType>::ApplyAllEnqueuedTransformations() const
	{
		m_AABoxColliderSystem->ApplyEnqueuedTransforms();
	}



	template <typename AABoxColliderSystemType>
	std::vector<RaycastHitTime> CollisionSystem<AABoxColliderSystemType>::RaycastTime(Ray ray) const
	{
		std::vector<RaycastHitTime> hits;

		m_AABoxColliderSystem->RaycastTime(ray, /*out*/ hits);

		return hits;
	}



	template <typename AABoxColliderSystemType>
	AABoxColliderSystemType* CollisionSystem<AABoxColliderSystemType>::GetAxisAlignedBoxColliderSystem() const
	{
		return m_AABoxColliderSystem.get();
	}



	class AABoxColliderSystem;
	extern template class CollisionSystem<AABoxColliderSystem>;
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__ENGINE_H
#define NEST__ENGINE_H

#include<chrono>
#include<memory>

#include"Nest/GameEvents.h"
#include"Nest/ComponentTypeMap.h"
#include"Nest/WorldsManager.h"



namespace Nest
{
	// Forward declarations
	class IResourcesManager;
	class RenderingManager;
	class RenderingModule;

	class IModelLoader;



	class NestEngine
	{
	public:
		NestEngine(
			int fps,
			std::unique_ptr<IResourcesManager> resourcesManager,
			std::unique_ptr<RenderingManager> renderingManager,
			std::unique_ptr<RenderingModule> renderingModule,
			std::unique_ptr<IModelLoader> modelLoader
		);
		~NestEngine();

		void Execute();

		RenderingModule* GetRenderingModule() const;


		void AddWorld(WorldId worldId, std::unique_ptr<World>& world);
		void RemoveWorld(WorldId worldId);
		World* GetWorld(WorldId worldId) const;



	private:
		void FixedUpdate(float fixedDeltaTime);
		void Update(float deltaTime);

		void UpdateTimeVariables(); // TODO : pass time management to another class?



	private:
		bool m_Quit = false;

		std::unique_ptr<RenderingModule> m_RenderingModule;
		std::unique_ptr<IModelLoader> m_ModelLoader;

		std::chrono::time_point<std::chrono::system_clock> m_LastTimeUpdate;
		int m_PendingFixedFrames = 0;
		float m_DeltaTime;
		float m_FixedDeltaTime;
		float m_AccumulatedFixedTime;

		ComponentTypeMap m_ComponentTypeMap;

		// Main managers.
		GameEvents m_GameEvents;
		WorldsManager m_WorldsManager;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__TRANSFORM_H
#define NEST__TRANSFORM_H



#include<string>
#include<vector>

#include"Nest/Component.h"
#include"Nest/ComponentReference.h"
#include"Nest/Quaternion.h"
#include"Nest/Vector3.h"
#include"Nest/Matrix4.h"



namespace Nest
{
	// Forward declarations
	class EntitiesAndComponentsRemap;
	class ComponentReferenceBase;



	class Transform : public Component
	{
	public:
		Transform();
		Transform(const Transform&) = delete;
		Transform(Transform&&) = default;

		~Transform() override = default;

		Transform& operator=(const Transform&) = delete;
		Transform& operator=(Transform&&) = default;


		void SetParent(ComponentReference<Transform> parent);
		ComponentReference<Transform> GetParent();

		int GetChildCount() const;
		ComponentReference<Transform> GetChildAt(int childIndex) const;

		// Will recursively set the transform hierarchy as dirty.
		void SetDirty();

		void SetLocalPosition(Vector3<float> position);
		void SetLocalRotation(Quaternion<float> rotation);
		void SetLocalScale(Vector3<float> scale);

		Vector3<float> GetLocalPosition();
		Quaternion<float> GetLocalRotation();
		Vector3<float> GetLocalScale();

		Vector3<float> GetPosition();
		Quaternion<float> GetRotation();

		Matrix4<float> GetTransformMatrix();
		void RefreshTransformMatrix();

		Matrix4<float> GetLocalTranslationMatrix() const;
		Matrix4<float> GetLocalRotationMatrix() const;
		Matrix4<float> GetLocalScaleMatrix() const;

		void LocalTranslate(Vector3<float> localTranslation);
		void LocalRotate(Vector3<float> localRotation);

		inline bool IsDirty()
		{
			return(m_IsDirty);
		};

		static std::string GetTypeName() { return("Transform"); }

		void CopyFrom(Component& other, EntitiesAndComponentsRemap* mappings = nullptr) override;



	protected:
		void OnCreate() final;
		void OnDestroy() final;

		void AddChild(ComponentReference<Transform> transform);
		void RemoveChild(ComponentReference<Transform> transform);



	protected:
		ComponentReference<Transform> m_Parent;

		using ChildsListType = std::vector<ComponentReference<Transform>>;
		ChildsListType m_Childs;

		Vector3<float> m_LocalPosition;
		Quaternion<float> m_LocalRotation;
		Vector3<float> m_LocalScale;

		Matrix4<float> m_AccumulatedTransformMatrix;
		bool m_IsDirty;
	};
}



#endif

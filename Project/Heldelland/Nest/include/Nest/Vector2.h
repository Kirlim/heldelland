//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__VECTOR2_H
#define NEST__VECTOR2_H



namespace Nest
{
	template <class NumberType>
	class Vector2
	{
	public:
		Vector2() = default;
		Vector2(const Vector2&) = default;
		Vector2(Vector2&&) = default;
		Vector2(NumberType x, NumberType y);
		Vector2(NumberType values[]);

		~Vector2() = default;

		Vector2& operator =(const Vector2&) = default;
		Vector2& operator =(Vector2&&) = default;


		void Set(NumberType x, NumberType y);
		void Set(NumberType values[]);

		NumberType GetMagnitude() const;

		Vector2& operator +=(const Vector2& other);
		Vector2& operator -=(const Vector2& other);
		Vector2& operator *=(const NumberType& value);

		Vector2 operator +(const Vector2& other) const;
		Vector2 operator -(const Vector2& other) const;
		Vector2 operator *(const NumberType& value) const;



		NumberType x{ 0 };
		NumberType y{ 0 };
	};



	template <typename NumberType>
	Vector2<NumberType>::Vector2(NumberType x, NumberType y)
	{
		this->x = x;
		this->y = y;
	}



	template <typename NumberType>
	Vector2<NumberType>::Vector2(NumberType values[])
	{
		this->x = values[0];
		this->y = values[1];
	}



	template <typename NumberType>
	void Vector2<NumberType>::Set(NumberType x, NumberType y)
	{
		this->x = x;
		this->y = y;
	}



	template <typename NumberType>
	void Vector2<NumberType>::Set(NumberType values[])
	{
		this->x = values[0];
		this->y = values[1];
	}



	template <typename NumberType>
	NumberType Vector2<NumberType>::GetMagnitude() const
	{
		return (sqrt(x * x + y * y));
	}



	template <class NumberType>
	Vector2<NumberType>& Vector2<NumberType>::operator +=(const Vector2<NumberType>& other)
	{
		this->x += other.x;
		this->y += other.y;

		return (*this);
	}

	template <class NumberType>
	Vector2<NumberType>& Vector2<NumberType>::operator -=(const Vector2<NumberType>& other)
	{
		this->x -= other.x;
		this->y -= other.y;

		return (*this);
	}

	template <class NumberType>
	Vector2<NumberType>& Vector2<NumberType>::operator *=(const NumberType& value)
	{
		this->x *= value;
		this->y *= value;

		return (*this);
	}



	template <class NumberType>
	Vector2<NumberType> Vector2<NumberType>::operator +(const Vector2& other) const
	{
		Vector2<NumberType> v(*this);
		v += other;
		return (v);
	}

	template <class NumberType>
	Vector2<NumberType> Vector2<NumberType>::operator -(const Vector2& other) const
	{
		Vector2<NumberType> v(*this);
		v -= other;
		return (v);
	}

	template <class NumberType>
	Vector2<NumberType> Vector2<NumberType>::operator *(const NumberType& value) const // TODO : unit testing
	{
		Vector2<NumberType> v(*this);
		v *= value;
		return (v);
	}



	// Comparing methods
	template <class NumberType>
	bool operator==(const Vector2<NumberType>& lh, const Vector2<NumberType>& rh)
	{
		return (lh.x == rh.x && lh.y == rh.y);
	}



	template <class NumberType>
	bool operator!=(const Vector2<NumberType>& lh, const Vector2<NumberType>& rh)
	{
		return (!(lh == rh));
	}



	// Helping methods
	template <class NumberType>
	Vector2<NumberType> Lerp(const Vector2<NumberType>& a, const Vector2<NumberType>& b, NumberType t)
	{
		return ((a * (NumberType(1) - t)) + (b * t));
	}



	// Common types
	using Vector2f = Vector2<float>;
}



#endif

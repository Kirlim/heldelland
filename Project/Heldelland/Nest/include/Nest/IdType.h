//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__ID_TYPE_H
#define NEST__ID_TYPE_H

// TODO : analyse if it is better to move hash code to another header, so functional  wont be meaninglessly included
#include<functional>



namespace Nest
{
	template <typename IdTraits>
	struct IdType
	{
		using underlying_type = typename IdTraits::underlying_type;

		IdType()
		{
		};

		IdType(const IdType& other) : value(other.value)
		{
		}

		explicit IdType(underlying_type id) : value(id)
		{
		}


		bool operator==(const IdType& right) const
		{
			return value == right.value;
		}

		bool operator!=(const IdType& right) const
		{
			return !(*this == right);
		}

		bool operator<(const IdType& right) const
		{
			return (value < right.value);
		}


		// Not const nor private in order to make ids friendlier with the serialization
		// technique being used (same method for both read and write).
		underlying_type value;
	};



	template <typename IdTraits>
	std::size_t hash_code(const IdType<IdTraits>& idType)
	{
		return std::hash<typename IdTraits::underlying_type>()(idType.value);
	}
}



namespace std
{
	template <typename IdTraits>
	struct hash<Nest::IdType<IdTraits>>
	{
		using argument_type = Nest::IdType<IdTraits>;
		using result_type = std::size_t;

		result_type operator()(const argument_type& arg) const
		{
			return hash_code(arg);
		}
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__FIXED_POINT_H
#define NEST__FIXED_POINT_H

#include<cstddef>
#include<stdint.h>




// Fixed point implementation based on dflemstr's implementation
// https://gist.github.com/dflemstr/294959



// What does the A stands for?
template<bool sign, size_t A> struct FixedPointSizeInfo;

#define FIXEDP_SIZE(x) (sizeof(x)*8)
#define FIXEDP_NEXT_SIZE(x) (sizeof(x)*16)
#define FIXEDP_PREV_SIZE(x) (sizeof(x)*4)


// Define template type for signed 32 bits
template <> struct FixedPointSizeInfo<true, FIXEDP_SIZE(int32_t)>
{
		typedef int32_t ValueType;
		static const size_t ValueSize = FIXEDP_SIZE(int32_t);
		typedef FixedPointSizeInfo<true, FIXEDP_NEXT_SIZE(int32_t)> NEXT_SIZEINFO;
		typedef FixedPointSizeInfo<true, FIXEDP_PREV_SIZE(int32_t)> PREV_SIZEINFO; 
};

// Define template type for signed 64 bits
template <> struct FixedPointSizeInfo<true, FIXEDP_SIZE(int64_t)>
{
		typedef int64_t ValueType;
		static const size_t ValueSize = FIXEDP_SIZE(int64_t);
		typedef FixedPointSizeInfo<true, FIXEDP_NEXT_SIZE(int64_t)> NEXT_SIZEINFO;
		typedef FixedPointSizeInfo<true, FIXEDP_PREV_SIZE(int64_t)> PREV_SIZEINFO; 
};

// Define template type for unsigned 64 bits
template <> struct FixedPointSizeInfo<false, FIXEDP_SIZE(uint64_t)>
{
		typedef uint64_t ValueType;
		static const size_t ValueSize = FIXEDP_SIZE(uint64_t);
		typedef FixedPointSizeInfo<false, FIXEDP_NEXT_SIZE(uint64_t)> NEXT_SIZEINFO;
		typedef FixedPointSizeInfo<false, FIXEDP_PREV_SIZE(uint64_t)> PREV_SIZEINFO; 
};


#undef FIXEDP_SIZE
#undef FIXEDP_NEXT_SIZE
#undef FIXEDP_PREV_SIZE
#undef FIXEDP_MK_SIZE_INFO


// Forward FixedPoint declaration
template<bool isSigned, size_t intSize, size_t fractionSize> 
class FixedPoint;


// Forward friends function declaration
template<bool isSigned, size_t intSize, size_t fractionSize> 
static inline FixedPoint<isSigned, intSize, fractionSize> sqrt(const FixedPoint<isSigned, intSize, fractionSize> & n);


// Fixed point class
template<bool isSigned, size_t intSize, size_t fractionSize> 
class FixedPoint
{
public:
	static const size_t IntSize = intSize;
	static const size_t FractionSize = fractionSize;
	static const size_t TotalSize = IntSize + FractionSize;
	static const bool Signed = isSigned;

	// Value type information to be used.
	typedef FixedPointSizeInfo<Signed, TotalSize> ValueTypeInfo; // Gets the type that we can use
	typedef typename ValueTypeInfo::ValueType ValueType; // Type that will be used to store data (the fixed point value)
	static const size_t ValueSize = ValueTypeInfo::ValueSize;

	// Value of "1" in this fixed point format
	static const ValueType One = ValueType(1) << FractionSize;



	// Constructors
	FixedPoint() {};
	FixedPoint(const FixedPoint &other) : rawValue(other.rawValue) {};
	// FixedPoint(const ValueType &rhs) : rawValue(rhs) {}

	explicit FixedPoint(const int n) : rawValue(static_cast<ValueType>(n) << FractionSize) {}
	explicit FixedPoint(float n) : rawValue(static_cast<ValueType>(n * One)) {}
	explicit FixedPoint(double n) : rawValue(static_cast<ValueType>(n * One)) {}
	explicit FixedPoint(unsigned int n) : rawValue(static_cast<ValueType>(n) << FractionSize) {}


	// Simple operations
	inline FixedPoint &operator +=(const FixedPoint &n) { rawValue += n.rawValue; return(*this); }
	inline FixedPoint &operator -=(const FixedPoint &n) { rawValue -= n.rawValue; return(*this); }

	inline FixedPoint operator +(const FixedPoint &n) const { FixedPoint x(*this); x += n; return(x); }
	inline FixedPoint operator -(const FixedPoint &n) const { FixedPoint x(*this); x -= n; return(x); }


	inline FixedPoint &operator *=(const FixedPoint &n)	{ rawValue = ((rawValue * n.rawValue) >> FractionSize); return(*this); }
	inline FixedPoint operator *(const FixedPoint &n) const { FixedPoint x(*this); x *= n; return(x); }

	inline FixedPoint &operator /=(const FixedPoint &n)	{ rawValue = (rawValue << FractionSize) / n.rawValue; return(*this); }
	inline FixedPoint operator /(const FixedPoint &n) const { FixedPoint x(*this); x /= n; return(x); }



	// Attribution operations
	inline FixedPoint &operator =(const FixedPoint& n) { rawValue = n.rawValue; return(*this); }
	inline FixedPoint &operator =(const int& n) { rawValue = (n << FractionSize); return(*this); }
	inline FixedPoint &operator =(const double& n) { rawValue = static_cast<ValueType>(n * One); return(*this); }
	inline FixedPoint &operator =(const float& n) { rawValue = static_cast<ValueType>(n * One); return(*this); }


	// Comparing operations with FixedPoint
	inline bool operator ==(const FixedPoint &n) const { return(rawValue == n.rawValue); }
	inline bool operator !=(const FixedPoint &n) const { return(rawValue != n.rawValue); }
	inline bool operator >=(const FixedPoint &n) const { return(rawValue >= n.rawValue); }
	inline bool operator <=(const FixedPoint &n) const { return(rawValue <= n.rawValue); }
	inline bool operator >(const FixedPoint &n) const { return(rawValue > n.rawValue); }
	inline bool operator <(const FixedPoint &n) const { return(rawValue < n.rawValue); }

	// Comparing operations with integers
	inline bool operator ==(const int &n) const { return(rawValue == (static_cast<ValueType>(n) << FractionSize)); }
	inline bool operator !=(const int &n) const { return(rawValue != (static_cast<ValueType>(n) << FractionSize)); }
	inline bool operator >=(const int &n) const { return(rawValue >= (static_cast<ValueType>(n) << FractionSize)); }
	inline bool operator <=(const int &n) const { return(rawValue <= (static_cast<ValueType>(n) << FractionSize)); }
	inline bool operator >(const int &n) const { return(rawValue > (static_cast<ValueType>(n) << FractionSize)); }
	inline bool operator <(const int &n) const { return(rawValue < (static_cast<ValueType>(n) << FractionSize)); }

	// Comparing operations with floats
	inline bool operator ==(const float &n) const { return(rawValue == static_cast<ValueType>(n * One)); }
	inline bool operator !=(const float &n) const { return(rawValue != static_cast<ValueType>(n * One)); }
	inline bool operator >=(const float &n) const { return(rawValue >= static_cast<ValueType>(n * One)); }
	inline bool operator <=(const float &n) const { return(rawValue <= static_cast<ValueType>(n * One)); }
	inline bool operator >(const float &n) const { return(rawValue > static_cast<ValueType>(n * One)); }
	inline bool operator <(const float &n) const { return(rawValue < static_cast<ValueType>(n * One)); }

	// Comparing operations with doubles
	inline bool operator ==(const double &n) const { return(rawValue == static_cast<ValueType>(n * One)); }
	inline bool operator !=(const double &n) const { return(rawValue != static_cast<ValueType>(n * One)); }
	inline bool operator >=(const double &n) const { return(rawValue >= static_cast<ValueType>(n * One)); }
	inline bool operator <=(const double &n) const { return(rawValue <= static_cast<ValueType>(n * One)); }
	inline bool operator >(const double &n) const { return(rawValue > static_cast<ValueType>(n * One)); }
	inline bool operator <(const double &n) const { return(rawValue < static_cast<ValueType>(n * One)); }

	// Comparing operations with unsigned ints
	inline bool operator ==(const unsigned int &n) const { return(rawValue == (static_cast<ValueType>(n) << FractionSize)); }
	inline bool operator !=(const unsigned int &n) const { return(rawValue != (static_cast<ValueType>(n) << FractionSize)); }
	inline bool operator >=(const unsigned int &n) const { return(rawValue >= (static_cast<ValueType>(n) << FractionSize)); }
	inline bool operator <=(const unsigned int &n) const { return(rawValue <= (static_cast<ValueType>(n) << FractionSize)); }
	inline bool operator >(const unsigned int &n) const { return(rawValue > (static_cast<ValueType>(n) << FractionSize)); }
	inline bool operator <(const unsigned int &n) const { return(rawValue < (static_cast<ValueType>(n) << FractionSize)); }
	


	// Casting methods
	int ToInt() const { return((rawValue & (ValueType(-1) << FractionSize)) >> FractionSize); }
	float ToFloat() const { return(static_cast<float>(rawValue) / One); }
	double ToDouble() const { return(static_cast<double>(rawValue) / One); }


	// Friends functions
	//template<bool isSigned, size_t intSize, size_t fractionSize> 
	//friend static inline FixedPoint<isSigned, intSize, fractionSize> sqrt(const FixedPoint<isSigned, intSize, fractionSize> & n);
	friend FixedPoint sqrt<isSigned, intSize, fractionSize>(const FixedPoint& n);


public:
	ValueType rawValue;
};



template<bool isSigned, size_t intSize, size_t fractionSize> 
static FixedPoint<isSigned, intSize, fractionSize> operator *(const int& n, const FixedPoint<isSigned, intSize, fractionSize>& fixedNumber)
{
	FixedPoint<isSigned, intSize, fractionSize> result{ n };
	result *= fixedNumber;
	return(result);
}



template<bool isSigned, size_t intSize, size_t fractionSize> 
static FixedPoint<isSigned, intSize, fractionSize> operator +(const int& n, const FixedPoint<isSigned, intSize, fractionSize>& fixedNumber)
{
	FixedPoint<isSigned, intSize, fractionSize> result{ n };
	result += fixedNumber;
	return(result);
}



template<bool isSigned, size_t intSize, size_t fractionSize> 
static FixedPoint<isSigned, intSize, fractionSize> operator -(const int& n, const FixedPoint<isSigned, intSize, fractionSize>& fixedNumber)
{
	FixedPoint<isSigned, intSize, fractionSize> result{ n };
	result -= fixedNumber;
	return(result);
}




template<bool isSigned, size_t intSize, size_t fractionSize> 
static FixedPoint<isSigned, intSize, fractionSize> operator /(const int& n, const FixedPoint<isSigned, intSize, fractionSize>& fixedNumber)
{
	FixedPoint<isSigned, intSize, fractionSize> result{ n };
	result /= fixedNumber;
	return(result);
}



template<bool isSigned, size_t intSize, size_t fractionSize> 
static FixedPoint<isSigned, intSize, fractionSize> operator /(const FixedPoint<isSigned, intSize, fractionSize>& fixedNumber, const int& n)
{
	FixedPoint<isSigned, intSize, fractionSize> result{ fixedNumber };
	FixedPoint<isSigned, intSize, fractionSize> other{ n };

	result /= other;
	return(result);
}



template<bool isSigned, size_t intSize, size_t fractionSize> 
static FixedPoint<isSigned, intSize, fractionSize> sqrt(const FixedPoint<isSigned, intSize, fractionSize> & n)
{
	const FixedPoint<isSigned, intSize, fractionSize> half(0.5);
	if (n == 0) return(FixedPoint<isSigned, intSize, fractionSize>(0));

	FixedPoint<isSigned, intSize, fractionSize> guess{ n / 2 };
	FixedPoint<isSigned, intSize, fractionSize> previous;

	while ((previous.rawValue >> 1) != (guess.rawValue >> 1))
	{
		previous = guess;
		guess = half * (previous + (n / previous));
	}

	return(guess);
}



// Typedefs to allow easier use of Fixed Points.
typedef FixedPoint<true, 44, 20> sfix64;



#endif

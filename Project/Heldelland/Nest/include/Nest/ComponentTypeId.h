
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__COMPONENT_TYPE_ID_H
#define NEST__COMPONENT_TYPE_ID_H

namespace Nest
{
	using ComponentTypeId = unsigned int;
}

#endif

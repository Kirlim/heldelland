
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__I_TEMPLATED_SINGLE_COMPONENTS_CONTAINER_ITERATOR_H
#define NEST__I_TEMPLATED_SINGLE_COMPONENTS_CONTAINER_ITERATOR_H

#include<memory>

#include<Nest/NestAssert.h>



namespace Nest
{
	// TODO : pass impl base to its own file?
	template <typename ComponentType>
	class ITemplatedSingleComponentsContainerIteratorImplBase
	{
	public:
		virtual ~ITemplatedSingleComponentsContainerIteratorImplBase() {};

		virtual ComponentType& operator *() const = 0;
		virtual const ITemplatedSingleComponentsContainerIteratorImplBase& operator ++() = 0;
		virtual bool operator !=(const ITemplatedSingleComponentsContainerIteratorImplBase& other) const = 0;

		virtual std::unique_ptr<ITemplatedSingleComponentsContainerIteratorImplBase> CloneUnique() const = 0;
	};



	template <typename ComponentType>
	class ITemplatedSingleComponentsContainerIterator
	{
	public:
		ITemplatedSingleComponentsContainerIterator(const ITemplatedSingleComponentsContainerIterator<ComponentType>& other);
		ITemplatedSingleComponentsContainerIterator(std::unique_ptr<ITemplatedSingleComponentsContainerIteratorImplBase<ComponentType>> iteratorImplementation);
		~ITemplatedSingleComponentsContainerIterator() = default;

		ComponentType& operator *() const;
		const ITemplatedSingleComponentsContainerIterator& operator ++();
		bool operator !=(const ITemplatedSingleComponentsContainerIterator& other) const;



	protected:
		std::unique_ptr<ITemplatedSingleComponentsContainerIteratorImplBase<ComponentType>> m_Implementation;
	};



	template <typename ComponentType>
	ITemplatedSingleComponentsContainerIterator<ComponentType>::ITemplatedSingleComponentsContainerIterator(const ITemplatedSingleComponentsContainerIterator<ComponentType>& other) :
		m_Implementation(other.m_Implementation->CloneUnique())
	{
		NestAssert(m_Implementation != nullptr);
	}



	template <typename ComponentType>
	ITemplatedSingleComponentsContainerIterator<ComponentType>::ITemplatedSingleComponentsContainerIterator(std::unique_ptr<ITemplatedSingleComponentsContainerIteratorImplBase<ComponentType>> iteratorImplementation) :
		m_Implementation(std::move(iteratorImplementation))
	{
		// Nothing here
	}



	template <typename ComponentType>
	ComponentType& ITemplatedSingleComponentsContainerIterator<ComponentType>::operator *() const
	{
		NestAssert(m_Implementation != nullptr);
		return(*(*m_Implementation));
	}



	template <typename ComponentType>
	const ITemplatedSingleComponentsContainerIterator<ComponentType>& ITemplatedSingleComponentsContainerIterator<ComponentType>::operator ++()
	{
		NestAssert(m_Implementation != nullptr);
		++(*m_Implementation);
		return(*this);
	}



	template <typename ComponentType>
	bool ITemplatedSingleComponentsContainerIterator<ComponentType>::operator !=(const ITemplatedSingleComponentsContainerIterator<ComponentType>& other) const
	{
		NestAssert(m_Implementation != nullptr);
		return(*m_Implementation != *(other.m_Implementation));
	}
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__METADATA__TEXTURE_METADATA_H
#define NEST__METADATA__TEXTURE_METADATA_H



#include<string>
#include<yaml-cpp/yaml.h>

#include"Nest/ResourcesTypes.h"
#include"Nest/Metadata/IMetadataInformation.h"
#include"Nest/Texture.h"



namespace Nest
{
	// Forward declarations
	class IResourcesManager;



	class TextureMetadata : public IMetadataInformation
	{
	public:
		TextureMetadata(const std::string resourceName);
		virtual ~TextureMetadata();

		virtual ResourceType GetResourceType() const override;



	protected:
		virtual void LoadMetadataFromYamlNode(const YAML::Node& node, const std::string metadataPath) override;
		virtual void LoadResourceIntoManager(IResourcesManager& resourcesManager) override;

		void TryLoadFilterings(const YAML::Node& mainNode);
		void TryLoadWrapMode(const YAML::Node& mainNode);
		void TryLoadFormat(const YAML::Node& mainNode);



	protected:
		std::string m_TexturePath;
		TextureFiltering m_Filtering = TextureFiltering::Bilinear;
		TextureFiltering m_MipmapFiltering = TextureFiltering::NearestNeighbor;
		TextureWrapMode m_WrapMode = TextureWrapMode::Clamp;
		TextureFormat m_Format = TextureFormat::RGB;
	};
}



#endif


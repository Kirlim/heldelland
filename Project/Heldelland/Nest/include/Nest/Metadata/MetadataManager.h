
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__METADATA__METADATA_MANAGER_H
#define NEST__METADATA__METADATA_MANAGER_H



#include<unordered_map>
#include<string>
#include<functional>
#include<memory>

#include<yaml-cpp/yaml.h>

#include"Nest/ResourcesTypes.h"
#include"Nest/Metadata/IMetadataInformation.h"



namespace Nest
{
	// Forward declarations
	enum class ResourceType;
	class IResourcesManager;



	class MetadataManager
	{
	public:
		// If the factory function is null, will throw NullArgumentException
		MetadataManager(
			std::function<std::unique_ptr<IMetadataInformation>(ResourceType, std::string /*name*/)> metadataInformationFactoryFunction
		);
		~MetadataManager();

		// Sets the file extension to be searched as metadata files, when trying to automatically
		// load new files for contents. Throws InvalidArgumentException if extension is empty.
		void SetMetadataExtension(std::string extension);

		// Returns the current metadata file extension.
		std::string GetMetadataExtension() const;

		// Load all metafiles information from a given path. If
		// there are name and type collision, that meta will be ignored.
		void LoadMetadataFromPath(std::string path);

		// Load a resource based on its meta information. Will do nothing if
		// a resource of same type and name is already loaded.
		void LoadResource(ResourceType resourceType, std::string resourceName, IResourcesManager& resourcesManager);

		// Load all resources based on their meta information. Will do nothing if
		// a resource of same type and name is already loaded. 
		// TODO : Failed loadings will be logged into a logger.
		void LoadAllResources(IResourcesManager& resourcesManager);



	protected:
		void LoadMetadataFile(std::string file);
		void LoadMetadataInformationList(YAML::Node node, std::string metaFileName);

		// Exceptions:
		//  - InvalidOperationException: Resource name is invalid, resource path is empty,
		//    or resource type is invalid (using read information from the file). Will also
		//    be throw if there is already a resource with the given name for the given type
		//    loaded in the metadata map.
		//  - LogicErrorException: the expected metadata information intance type and the
		//    type from the file are not the same.
		//  - Others unexpected exceptions will bubble up.
		void LoadSingleMetadataInformation(const YAML::Node& node, std::string metaFileName);

		// Exceptions:
		//  - FailedToLoadException: if failed to open and read the metadata file.
		//  - Others unexpected exceptions will bubble up.
		void LoadIntoMetadataFromYamlFile(IMetadataInformation& metadata, std::string metadataFile);

		void LoadResourceFromMetadata(IMetadataInformation& metadata, IResourcesManager& resourcesManager);



	protected:
		using MetadataMapType =
			std::unordered_map<ResourceType, std::unordered_map<std::string /*resource name*/, std::unique_ptr<IMetadataInformation>>, ResourceTypeHash>;
		MetadataMapType m_MetadataMap;

		std::function<std::unique_ptr<IMetadataInformation>(ResourceType, std::string)> m_MetadataInformationFactoryFunction;

		std::string m_MetadataFileExtension;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__METADATA__SHADER_PROGRAM_METADATA_H
#define NEST__METADATA__SHADER_PROGRAM_METADATA_H



#include<string>
#include<vector>
#include<yaml-cpp/yaml.h>

#include"Nest/ResourcesTypes.h"
#include"Nest/Metadata/IMetadataInformation.h"



namespace Nest
{
	// Forward declarations
	class IResourcesManager;



	class ShaderProgramMetadata : public IMetadataInformation
	{
	public:
		ShaderProgramMetadata(const std::string resourceName);
		virtual ~ShaderProgramMetadata();

		virtual ResourceType GetResourceType() const override;



	protected:
		virtual void LoadMetadataFromYamlNode(const YAML::Node& node, const std::string metadataPath) override;
		virtual void LoadResourceIntoManager(IResourcesManager& resourcesManager) override;

		void LoadShaderFilesPaths(const YAML::Node& mainNode, const std::string metadataPath);
		void TryLoadMainTextureInformation(const YAML::Node& mainNode);
		void TryLoadTexturesInformation(const YAML::Node& mainNode);
		void TryLoadTransparencyFlag(const YAML::Node& mainNode);



	protected:
		std::string m_VertexShaderFile;
		std::string m_FragmentShaderFile;
		std::string m_MainTextureName;
		std::vector<std::string> m_TexturesNames;
		bool m_IsTransparent = 0;
	};
}



#endif

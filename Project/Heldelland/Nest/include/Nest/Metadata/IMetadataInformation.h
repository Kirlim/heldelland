
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__METADATA__IMETADATA_INFORMATION_H
#define NEST__METADATA__IMETADATA_INFORMATION_H



#include<string>
#include<yaml-cpp/yaml.h>

#include"Nest/ResourcesTypes.h"



namespace Nest
{
	// Forward declarations
	class IResourcesManager;



	class IMetadataInformation
	{
	public:
		IMetadataInformation(const std::string resourceName);
		virtual ~IMetadataInformation();

		void LoadMetadata(const YAML::Node& node, const std::string metadataPath);
		void LoadResource(IResourcesManager& resourcesManager);

		bool IsLoaded() const;
		virtual ResourceType GetResourceType() const = 0;



	protected:
		// Loads metadata information from file to memory. If this method does not
		// throw any exception, the IMetadataInformation will set m_IsLoaded to true.
		virtual void LoadMetadataFromYamlNode(const YAML::Node& node, const std::string metadataPath) = 0;
		virtual void LoadResourceIntoManager(IResourcesManager& resourcesManager) = 0;


		
	protected:
		bool m_IsLoaded = false;
		const std::string m_ResourceName;
		const std::string m_MetadataPath;
	};
}



#endif

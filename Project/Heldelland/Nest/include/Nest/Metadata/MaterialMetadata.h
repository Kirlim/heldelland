
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__METADATA__MATERIAL_METADATA_H
#define NEST__METADATA__MATERIAL_METADATA_H



#include<string>
#include<vector>
#include<yaml-cpp/yaml.h>

#include"Nest/Metadata/IMetadataInformation.h"



// Material properties
// Shader : "ShaderName"
// Textures:
//     - Slot : "SlotName"
//       Texture : "TextureResourceName"



namespace Nest
{
	// Forward declarations
	class IResourcesManager;



	class MaterialMetadata : public IMetadataInformation
	{
	public:
		MaterialMetadata(const std::string resourceName);
		virtual ~MaterialMetadata();

		virtual ResourceType GetResourceType() const override;



	protected:
		virtual void LoadMetadataFromYamlNode(const YAML::Node& node, const std::string metadataPath) override;
		virtual void LoadResourceIntoManager(IResourcesManager& resourcesManager) override;

		void LoadShaderName(const YAML::Node& mainNode);
		void TryLoadTextureData(const YAML::Node& mainNode);



	protected:
		std::string m_ShaderName;
		std::vector<std::string> m_TextureSlotNames;
		std::vector<std::string> m_TextureSlotResources;
	};
}



#endif

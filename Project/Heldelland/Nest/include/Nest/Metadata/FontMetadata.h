
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__METADATA__FONT_METADATA_H
#define NEST__METADATA__FONT_METADATA_H



#include<string>
#include<yaml-cpp/yaml.h>

#include"Nest/Metadata/IMetadataInformation.h"



namespace Nest
{
	// Forward declarations
	class IResourcesManager;



	class FontMetadata : public IMetadataInformation
	{
	public:
		FontMetadata(const std::string resourceName);
		virtual ~FontMetadata();

		virtual ResourceType GetResourceType() const override;



	protected:
		virtual void LoadMetadataFromYamlNode(const YAML::Node& node, const std::string metadataPath) override;
		virtual void LoadResourceIntoManager(IResourcesManager& resourcesManager) override;



	protected:
		std::string m_FontFile;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__MESH_RENDERER_H
#define NEST__MESH_RENDERER_H

#include"Nest/Component.h"



namespace Nest
{
	// Forward declarations
	class Material;



	class MeshRenderer final : public Component
	{
	public:
		MeshRenderer() = default;
		MeshRenderer(const MeshRenderer&) = delete;
		MeshRenderer(MeshRenderer&&) = default;

		~MeshRenderer() override = default;

		MeshRenderer& operator =(const MeshRenderer&) = delete;
		MeshRenderer& operator =(MeshRenderer&&) = default;


		static std::string GetTypeName() { return("MeshRenderer"); }

		void SetMaterial(Material* material);
		Material* GetMaterial() const;

		void SetGuiFlag(const bool isGui);
		bool IsGui() const;



	protected:
		void OnDestroy() override;
		
		

	protected:
		Material *m_Material = nullptr;
		bool m_IsGui = false;
	};
}



#endif

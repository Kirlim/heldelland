//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__QUATERNION_H
#define NEST__QUATERNION_H

#include"Nest/Vector3.h"
#include"Nest/Matrix4.h"



namespace Nest
{
	template <typename NumberType>
	class Quaternion
	{
	public:
		Quaternion();
		Quaternion(const Quaternion& other);
		Quaternion(NumberType w, NumberType x, NumberType y, NumberType z);
		Quaternion(const NumberType& radians, const Vector3<NumberType>& axis);

		void SetRotation(const NumberType& radians, const Vector3<NumberType>& axis);
		void SetRotation(const NumberType& cosineHalfRadian, const NumberType& sineHalfRadian, const Vector3<NumberType>& axis);

		void SetFromEuler(const NumberType& yaw, const NumberType& pitch, const NumberType& roll);
		static Quaternion FromEuler(const NumberType& yaw, const NumberType& pitch, const NumberType& roll);

		// Returns a Vector3 where:
		// x is roll in degrees
		// y is yaw in degrees
		// z is pitch in degrees
		Vector3<NumberType> GetEulerYawPitchRollDegrees() const;

		void Normalize();

		Quaternion GetInverse() const;

		Quaternion& operator *=(const Quaternion& other);
		Quaternion& operator =(const Quaternion& other);
		// TODO : Operator *
		// TODO : Operator =
		// TODO : multiply by vector?
		// TODO : unit tests

		// TODO : method and unit test
		Matrix4<NumberType> ToRotationMatrix() const;

		Vector3<NumberType> RotateVector(const Vector3<NumberType>& vec) const;
		void RotateVectorInplace(Vector3<NumberType>& vec) const;

		NumberType x;
		NumberType y;
		NumberType z;
		NumberType w;

		static Quaternion Identity();
	};



	// Extra methods
	template <typename NumberType>
	bool operator==(const Quaternion<NumberType>& left, const Quaternion<NumberType>& right)
	{
		return (left.x == right.x && left.y == right.y && left.z == right.z && left.w == right.w);
	}



	// Class methods

	template <typename NumberType>
	Quaternion<NumberType>::Quaternion() :
		x(0), y(0), z(0), w(1)
	{
		// Nothing here
	}



	template <typename NumberType>
	Quaternion<NumberType>::Quaternion(const Quaternion& other) :
		x(other.x), y(other.y), z(other.z), w(other.w)
	{
		// Nothing here
	}



	template <typename NumberType>
	Quaternion<NumberType>::Quaternion(NumberType w, NumberType x, NumberType y, NumberType z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}



	template <typename NumberType>
	Quaternion<NumberType>::Quaternion(const NumberType& radians, const Vector3<NumberType>& axis)
	{
		SetRotation(cos(radians * 0.5f), sin(radians * 0.5f), axis);
	}



	template <typename NumberType>
	void Quaternion<NumberType>::SetRotation(const NumberType& radians, const Vector3<NumberType>& axis)
	{
		SetRotation(cos(radians * 0.5f), sin(radians * 0.5f), axis);
	}



	template <typename NumberType>
	void Quaternion<NumberType>::SetRotation(const NumberType& cosineHalfRadian, const NumberType& sineHalfRadian, const Vector3<NumberType>& axis)
	{
		w = cosineHalfRadian;
		x = axis.x * sineHalfRadian;
		y = axis.y * sineHalfRadian;
		z = axis.z * sineHalfRadian;

		Normalize();
	}



	template <typename NumberType>
	void Quaternion<NumberType>::SetFromEuler(const NumberType& yawInDegrees, const NumberType& pitchInDegrees, const NumberType& rollInDegrees)
	{
		const NumberType ToRadians = NumberType(3.141592653589793238) / NumberType(180.0);

		NumberType yawInRadians = yawInDegrees * ToRadians;
		NumberType pitchInRadians = pitchInDegrees * ToRadians;
		NumberType rollInRadians = rollInDegrees * ToRadians;

		NumberType c1 = cos(yawInRadians * NumberType(0.5f));
		NumberType c2 = cos(pitchInRadians * NumberType(0.5f));
		NumberType c3 = cos(rollInRadians * NumberType(0.5f));
		NumberType s1 = sin(yawInRadians * NumberType(0.5f));
		NumberType s2 = sin(pitchInRadians * NumberType(0.5f));
		NumberType s3 = sin(rollInRadians * NumberType(0.5f));

		w = c1 * c2 * c3 - s1 * s2 * s3;
		x = s1 * s2 * c3 + c1 * c2 * s3;
		y = s1 * c2 * c3 + c1 * s2 * s3;
		z = c1 * s2 * c3 - s1 * c2 * s3;
	}



	template <typename NumberType>
	Quaternion<NumberType> Quaternion<NumberType>::FromEuler(const NumberType& yaw, const NumberType& pitch, const NumberType& roll)
	{
		Quaternion<NumberType> result;
		result.SetFromEuler(yaw, pitch, roll);
		return result;
	}



	template <typename NumberType>
	Vector3<NumberType> Quaternion<NumberType>::GetEulerYawPitchRollDegrees() const
	{
		NumberType ww = w * w;
		NumberType xx = x * x;
		NumberType yy = y * y;
		NumberType zz = z * z;
		NumberType unit = ww + xx + yy + zz;
		NumberType specialCaseTest = x * y + z * w;

		Vector3<NumberType> angles;

		if (specialCaseTest > NumberType(0.499) * unit)
		{
			angles.y = NumberType(2) * atan2(x, w);
			angles.z = NumberType(3.141592653589793238) * NumberType(0.5f);
			angles.x = NumberType(0.0f);
		}
		else if (specialCaseTest < NumberType(-0.499) * unit)
		{
			angles.y = NumberType(-2) * atan2(x, w);
			angles.z = NumberType(-3.141592653589793238) * NumberType(0.5f);
			angles.x = NumberType(0.0f);
		}
		else
		{
			angles.y = atan2(NumberType(2) * y * w - NumberType(2) * x * z, xx - yy - zz + ww);
			angles.z = asin(NumberType(2) * specialCaseTest / unit);
			angles.x = atan2(NumberType(2) * x * w - NumberType(2) * y * z, -xx + yy - zz + ww);
		}

		const NumberType ToDegrees = NumberType(180.0f) / NumberType(3.141592653589793238);
		angles.x = angles.x * ToDegrees;
		angles.y = angles.y * ToDegrees;
		angles.z = angles.z * ToDegrees;

		return (angles);
	}



	template <typename NumberType>
	void Quaternion<NumberType>::Normalize()
	{
		NumberType factor = w * w + x * x + y * y + z * z;
		factor = sqrt(factor);

		w /= factor;
		x /= factor;
		y /= factor;
		z /= factor;
	}



	template <typename NumberType>
	Quaternion<NumberType> Quaternion<NumberType>::GetInverse() const
	{
		// TODO : remove NumberType(0), and overload - unary
		return (Quaternion(w, NumberType(0) - x, NumberType(0) - y, NumberType(0) - z));
	}



	template <typename NumberType>
	Quaternion<NumberType>& Quaternion<NumberType>::operator *=(const Quaternion& other)
	{
		NumberType localW = (other.w * w) - (other.x * x) - (other.y * y) - (other.z * z);
		NumberType localX = (other.w * x) + (other.x * w) - (other.y * z) + (other.z * y);
		NumberType localY = (other.w * y) + (other.x * z) + (other.y * w) - (other.z * x);
		NumberType localZ = (other.w * z) - (other.x * y) + (other.y * x) + (other.z * w);

		this->x = localX;
		this->y = localY;
		this->z = localZ;
		this->w = localW;

		return (*this);
	}



	template <typename NumberType>
	Quaternion<NumberType>& Quaternion<NumberType>::operator =(const Quaternion& other)
	{
		this->w = other.w;
		this->x = other.x;
		this->y = other.y;
		this->z = other.z;

		return (*this);
	}



	template <typename NumberType>
	Matrix4<NumberType> Quaternion<NumberType>::ToRotationMatrix() const
	{
		Matrix4<NumberType> rotationMatrix(
			1 - 2 * (y * y + z * z), 2 * (x * y - w * z), 2 * (x * z + w * y), NumberType(0),
			2 * (x * y + w * z), 1 - 2 * (x * x + z * z), 2 * (y * z - w * x), NumberType(0),
			2 * (x * z - w * y), 2 * (y * z + w * x), 1 - 2 * (x * x + y * y), NumberType(0),
			NumberType(0), NumberType(0), NumberType(0), NumberType(1)
		);

		return (rotationMatrix);
	}



	template <typename NumberType>
	Vector3<NumberType> Quaternion<NumberType>::RotateVector(const Vector3<NumberType>& vec) const
	{
		Vector3<NumberType> quaternionVector(x, y, z);
		Vector3<NumberType> temp = NumberType(2) * Cross<NumberType>(quaternionVector, vec);
		Vector3<NumberType> result = vec + (this->w * temp) + Cross<NumberType>(quaternionVector, temp);
		return (result);
	}



	template <typename NumberType>
	void Quaternion<NumberType>::RotateVectorInplace(Vector3<NumberType>& vec) const
	{
		Vector3<NumberType> quaternionVector(x, y, z);
		Vector3<NumberType> temp = NumberType(2) * Cross<NumberType>(quaternionVector, vec);
		Vector3<NumberType> result = vec + (this->w * temp) + Cross<NumberType>(quaternionVector, temp);

		vec.x = result.x;
		vec.y = result.y;
		vec.z = result.z;
	}



	template <typename NumberType>
	Quaternion<NumberType> Quaternion<NumberType>::Identity()
	{
		// TODO : unit testing
		Quaternion<NumberType> result;
		return (result);
	}
}



#endif

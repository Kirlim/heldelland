
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__GLOBAL_H
#define NEST__GLOBAL_H

#include<memory>

#include"Nest/Metadata/MetadataManager.h"



namespace Nest
{
	// Forward declarations
	class NestEngine;
	class Input;
	class MetadataManager;

	// The Global class is a rather controversial class (being nice to it)
	// to hold variables and references to objects of global access. This class
	// is meant to hold things that might be rarely accessed anywhere, such as
	// current aspect ratio of the screen, or things that are way too common
	// to be passed by dependency injection, such as string-to-type translators.
	//
	// If this class starts to get crowded, consider serious refactoring of the code.
	// Types that are here for simple "speed of coding" (aka "temporary solution")
	// should be flagged as so, and be removed to "production quality" asap.
	class GlobalAccess
	{
		friend class NestEngine;

	public:
		NestEngine * GetEngine() const;

		Input* GetInput() const;
		MetadataManager* GetMetadataManager() const;


		// Forbid copying
		GlobalAccess(GlobalAccess const &) = delete;
		GlobalAccess& operator=(GlobalAccess const &) = delete;



	protected:
		GlobalAccess();
		~GlobalAccess();

		void SetEngine(NestEngine* nestEngine);

		void SetInput(std::unique_ptr<Input> input);
		void SetMetadataManager(std::unique_ptr<MetadataManager> metadataManager);


	protected:
		NestEngine * m_NestEngine;

		std::unique_ptr<Input> m_Input;
		std::unique_ptr<MetadataManager> m_MetadataManager;
	};



	// Game class will delete this on destruction
	extern GlobalAccess* Global;
}



#endif

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__TEXT_MESH_IMPL_H
#define NEST__TEXT_MESH_IMPL_H

#include<string>



namespace Nest
{
	// Forward declarations
	class Font;
	class Material;


	class TextMeshImpl
	{
	public:
		virtual ~TextMeshImpl();

		virtual void RenderText(std::string text, const Font* font, float size) = 0;
	};
}



#endif

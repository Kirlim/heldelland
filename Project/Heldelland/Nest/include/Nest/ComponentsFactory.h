
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<unordered_map>
#include<memory>
#include<string>

// TODO : include header that can disable all asserts at once

#include"Nest/IComponentsFactory.h"
#include"Nest/BasicComponentsFactory.h"



namespace Nest
{
	// TODO : component container class, that contains ranged for methods, and return a ComponentIterator instance (will need casting. blergh?)
	// TODO : ComponentIterator class
	// Forward declarations
	class ComponentOwnerReferenceBase;



	// TODO : documentation
	class ComponentsFactory : public IComponentsFactory
	{
	public:
		virtual ~ComponentsFactory();



	protected:
		// TODO : change to shared pointer?
		virtual void DoRegisterFactory(const unsigned int factoryTypeId, const std::string typeName, std::unique_ptr<BasicComponentsFactory> factory) override;
		virtual std::unique_ptr<ComponentOwnerReferenceBase> DoInstantiate(const unsigned int factoryTypeId) override;
		virtual std::unique_ptr<ComponentOwnerReferenceBase> DoInstantiate(const std::string typeName) override;

		virtual std::string DoGetTypeName(const unsigned int factoryTypeId) const override;
		virtual unsigned int DoGetFactoryTypeId(const std::string typeName) const override;



	protected:
		std::unordered_map<unsigned int, std::unique_ptr<BasicComponentsFactory>> m_Factories;
		std::unordered_map<std::string, unsigned int> m_TypeNameMap;
	};
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__IENTITY_H
#define NEST__IENTITY_H

#include<memory>
#include<string>

#include"Nest/Component.h" // TODO : pass ComponentType to another file, in order to reduce INCLUDE stress.
#include"Nest/ComponentTypeMap.h"
#include"Nest/ComponentReference.h"

#include"Nest/EntityId.h"



namespace Nest
{
	// Forward declarations
	class Component;
	class ComponentOwnerReferenceBase;
	class EntitiesManager;



	// TODO : use a type wrapper for componentTypeId, instead of a typedef (research if this does make sense)

	class IEntity
	{
		friend class EntitiesManager;

	public:
		IEntity(const IEntity&) = delete;
		IEntity(IEntity&&) = delete;

		virtual ~IEntity();


		// Adds the component referenced by @componentReference to the entity. On success, the
		// reference will be moved. Will return a pointer which can be used to make
		// ComponentReference instances. Thrown exceptions will depend on implementation.
		ComponentOwnerReferenceBase* AddComponent(std::unique_ptr<ComponentOwnerReferenceBase>& componentReference);

		// Adds a new component by using the @typeName string to find the component type.
		// Will return a unsafe pointer to the owning reference, which can be used to make safer
		// references. Thrown exceptions will depend on implementation.
		ComponentOwnerReferenceBase* AddComponent(std::string typeName);

		// Adds a new component by using the template type @ComponentType. The type is
		// assumed to be, indeed, a component. Will return a ComponentReference<T> to the
		// component added.
		template <typename ComponentType>
		ComponentReference<ComponentType> AddComponent()
		{
			auto typeId = ComponentTypeMap::GetInstance()->GetTypeId<ComponentType>();
			auto component = DoInstantiateComponent(typeId);

			NestAssert(component != nullptr);
			auto addedComponent = AddComponent(component);
			NestAssert(component == nullptr);

			return (ComponentReference<ComponentType>(*addedComponent));
		}

		// Returns the first component of ComponentType found in this entity.
		// Will return a ComponentReference that points to nullptr if no component
		// was found.
		template <typename ComponentType>
		ComponentReference<ComponentType> GetComponent()
		{
			ComponentTypeId typeId = ComponentTypeMap::GetInstance()->GetTypeId<ComponentType>();
			auto componentReference = DoGetComponent(typeId);

			if (componentReference != nullptr)
			{
				return ComponentReference<ComponentType>(*componentReference);
			}
			else
			{
				return ComponentReference<ComponentType>();
			}
		}


		// TODO : documentation
		template <typename ComponentType>
		ComponentReference<ComponentType> GetComponentReference(ComponentType* componentPointer)
		{
			// TODO : throw exception if non existing component
			// TODO : make sure that ComponentType is indeed a component.
			ComponentTypeId typeId = ComponentTypeMap::GetInstance()->GetTypeId<ComponentType>();
			auto componentReference = DoGetComponent(typeId);

			// Build the correct type reference
			if (componentReference != nullptr)
			{
				if (componentReference->Get() != dynamic_cast<Component*>(componentPointer))
				{
					// TODO : better error handling and logging
					throw - 1;
				}

				return ComponentReference<ComponentType>(*componentReference);
			}
			else
			{
				return ComponentReference<ComponentType>();
			}
		}

		// Returns a component reference based on the it's id. Will throw
		// LogicErrorException if the component was not found in this entity,
		// or if the cast to ComponentType is invalid.
		template <typename ComponentType>
		ComponentReference<ComponentType> GetComponentById(const ComponentId componentId) const
		{
			ComponentTypeId typeId = ComponentTypeMap::GetInstance()->GetTypeId<ComponentType>();
			const auto componentReference = DoGetComponentById(componentId);
			if (componentReference == nullptr)
				ThrowLogicErrorExceptionWithLine("Component was not found in this entity.");

			if (dynamic_cast<ComponentType*>(componentReference->Get()) == nullptr)
				ThrowLogicErrorExceptionWithLine("Component cannot be converted to component type.");

			return ComponentReference<ComponentType>(*componentReference);
		}

		// Returns the first component of the given @componetTypeName found in this entity.
		// Will return a ComponentReference that points to nullptr if no component
		// was found. If the type name is unknown, an exception is expected.
		ComponentOwnerReferenceBase* GetComponent(std::string componentTypeName) const;

		// Returns the first component of @componentTypeId found in this entity.
		// Will return a ComponentReference that points to nullptr if no component
		// was found.
		ComponentOwnerReferenceBase* GetComponent(ComponentTypeId componentTypeId) const;

		// Returns the first component of ComponentType found in this entity.
		// Will return a ComponentReference that points to nullptr if no component
		// was found.
		ComponentOwnerReferenceBase* GetComponentByName(std::string componentName) const;


		// TODO : documentation (NOTE: MIGHT CAUSE IMMEDIATE DESTRUCTION OF COMPONENT, OR NOT. SHARED_PTR
		void RemoveComponent(std::string componentTypeName);
		void RemoveComponent(ComponentTypeId componentTypeId);
		void RemoveComponentByName(std::string componentName);

		virtual int GetComponentsCount() const = 0;
		virtual std::unique_ptr<ComponentReferenceBase> GetComponentAt(int index) const = 0;
		virtual ComponentTypeId GetComponentTypeIdAt(int index) const = 0;

		virtual std::weak_ptr<IEntity> Clone(EntitiesManager& owningManager) = 0;

		virtual void SetName(std::string name) = 0;
		virtual std::string GetName() const = 0;

		virtual Nest::EntitiesManager* GetEntitiesManager() const = 0;


		EntityId Id() const;


	protected:
		IEntity();

		void SetId(EntityId id);
		void Destroy();


	private:
		virtual std::unique_ptr<ComponentOwnerReferenceBase> DoInstantiateComponent(ComponentTypeId typeId) const = 0;
		virtual std::unique_ptr<ComponentOwnerReferenceBase> DoInstantiateComponent(std::string typeName) const = 0;

		// TODO : comment about validations already made
		virtual void DoAddComponent(std::unique_ptr<ComponentOwnerReferenceBase>& componentReference, const ComponentTypeId componentTypeId) = 0;

		virtual ComponentOwnerReferenceBase* DoGetComponent(ComponentTypeId componentTypeId) const = 0;
		virtual ComponentOwnerReferenceBase* DoGetComponent(std::string componentTypeName) const = 0;
		virtual ComponentOwnerReferenceBase* DoGetComponentByName(std::string componentName) const = 0;
		virtual ComponentOwnerReferenceBase* DoGetComponentById(ComponentId componentId) const = 0;

		virtual void DoRemoveComponent(ComponentTypeId componentTypeId) = 0;
		virtual void DoRemoveComponentByName(std::string componentName) = 0;

		virtual EntityId DoGetId() const = 0;
		virtual void DoSetId(EntityId id) = 0;

		// Clean up all components and do specific behavior of the entity class (if any).
		// OnDestroy must be called on each component - the behavior for each component
		// should be the same as the interface's RemoveComponent methods:
		virtual void DoDestroy() = 0;
	};
}



#endif

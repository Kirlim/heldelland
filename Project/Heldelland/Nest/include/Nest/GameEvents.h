//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__GAME_EVENTS_H
#define NEST__GAME_EVENTS_H

#include<deque>
#include"SDL.h"




namespace Nest
{
	// Forward declarations
	enum class KeyCode;


	// Events that the GameEvents class can expose
	enum class GameEvent { None, Quit };

	// Stores information about a game event
	class GameEventInfo
	{
	public:
		GameEventInfo();
		GameEventInfo(GameEvent gameEvent);
		GameEvent ev;
	};


	// Handle basic game events detection and listing, such as "Close" events.
	// This class has the responsibility of messaging handlers (such as Input)
	// of events as keypresses, too. Such events won't be detected in the "Poll" method.
	class GameEvents
	{
	public:
		// Process pending events to be listed
		void Process();

		// Returns the next stored event
		bool Poll(GameEventInfo& eventInfo);



		// TODO : when get a chance, break this class and methods in a way that makes more sense
		void StartTextInput();
		void StopTextInput();



	private:
		void PushEvent(GameEventInfo eventInfo);
		const GameEventInfo PopEvent();

		KeyCode ConvertToKeyCode(SDL_Keycode sdlKeyCode) const;



	private:
		// TODO : deque as a queue for storing events
		std::deque<GameEventInfo> m_Events;
	};
}



#endif

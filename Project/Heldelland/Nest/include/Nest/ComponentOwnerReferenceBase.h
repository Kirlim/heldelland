
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__COMPONENT_OWNER_REFERENCE_BASE_H
#define NEST__COMPONENT_OWNER_REFERENCE_BASE_H



namespace Nest
{
	// Redeclaring typedef
	typedef unsigned int ComponentTypeId;

	// Forward declarations
	class Component;
	class ComponentReferenceBase;



	class ComponentOwnerReferenceBase
	{
		friend class ComponentReferenceBase;

	public:
		const ComponentTypeId typeId;

		ComponentOwnerReferenceBase(ComponentTypeId componentTypeId);

		virtual ~ComponentOwnerReferenceBase() = default;



		virtual Component* Get() const = 0;

		Component* operator->() const;



	protected:
		virtual void RegisterComponentReference(ComponentReferenceBase& reference) = 0;
		virtual void UnregisterComponentReference(ComponentReferenceBase& reference) = 0;

		void UpdateUnsafePointer(ComponentReferenceBase& reference, Component* component);
	};
}



#endif

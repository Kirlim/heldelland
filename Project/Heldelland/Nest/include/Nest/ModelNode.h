
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<vector>
#include<string>
#include<memory>

#include"Nest/Vector3.h"



namespace Nest
{
    // Forward declarations
    class Texture;
    class Mesh;
    class Material;



    // Responsibility:
    //    Help in making an hierarchical structure to store meshes of a model, their
    //    "children" nodes, material and textures.
    class ModelNode
    {
    public:
        ModelNode();
        ~ModelNode();


        // Data handling

        void SetPosition(Vector3f position);
        Vector3f GetPosition() const;

        void SetScale(Vector3f scale);
        Vector3f GetScale() const;

        // Registers a mesh name and material to be used, with this node. Returns the index
        // in which the data has been registered. If @meshResourceName or @materialResourceName
        // have size of 0, std::logic_error will be thrown.
        int RegisterMeshData(const std::string meshResourceName, const std::string materialResourceName);

        // Returns the mesh name of the mesh at @meshIndex. Will throw "out_of_range" if meshIndex is invalid.
        std::string GetMeshName(const int meshIndex) const;

        // Returns the material name for the mesh at @meshIndex. Will throw "out_of_range" if meshIndex is invalid.
        std::string GetMaterialName(const int meshIndex) const;

        // Returns the total of "mesh - material" pairs registered within this node.
        int GetMeshesDataCount() const;


        // Registers a child node, for the model hierarchy. This method will move the ownership
        // of @child if success. Returns the index of the added child. Will throw invalid_argument
        // if @child is null.
        int AddChild(std::unique_ptr<ModelNode>& child);

        // Returns the child stored at the given index. Will throw "out_of_range" if @childIndex is invalid
        ModelNode* GetChild(const int childIndex) const; // TODO : ut

        // Returns the number of direct child nodes from this node.
        int GetChildCount() const;


    protected:
        std::vector<std::unique_ptr<ModelNode>> m_Children;

        struct MeshData
        {
            const std::string meshResourceName;
            const std::string materialResourceName;

            MeshData(const std::string meshResourceName, const std::string materialResourceName) :
                meshResourceName(meshResourceName), materialResourceName(materialResourceName)
            { }
        };

        std::vector<MeshData> m_MeshData;
        Vector3f m_Position;
        Vector3f m_Scale;
    };
}

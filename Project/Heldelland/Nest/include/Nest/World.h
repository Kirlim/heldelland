
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__WORLD_H
#define NEST__WORLD_H

#include<memory>

#include"Nest/SystemsManager.h"
#include"Nest/RenderingManager.h"
#include"Nest/IResourcesManager.h"
#include"Nest/EntitiesManager.h"



namespace Nest
{
	// Forward declarations
	class IComponentsContainers;
	class IComponentsFactory;

	class EntitiesManager;
	class RenderingManager;
	class IResourcesManager;
	class SystemsManager;



	class World
	{
	public:
		// TODO : unit test constructor
		World(
			std::unique_ptr<IComponentsContainers>& componentsContainers,
			std::unique_ptr<IComponentsFactory>& componentsFactory,
			std::unique_ptr<IResourcesManager>& resourcesManager,
			std::unique_ptr<RenderingManager>& renderingManager,
			std::unique_ptr<SystemsManager>& systemsManager
		);
		
		~World();

		// TODO : consider if it does make sense to make resources manager owned by world. Shouldn't be by engine? 
		// If yes, does it make sense to be global? How to reduce coupling among many classes?
		IComponentsContainers* GetComponentsContainers() const;
		IComponentsFactory* GetComponentsFactory() const;
		EntitiesManager* GetEntitiesManager() const;
		IResourcesManager* GetResourcesManager() const;
		RenderingManager* GetRenderingManager() const;
		SystemsManager* GetSystemsManager() const;



	private:
		void InstantiateEntitiesManager();



	private:
		// NOTE : order of destruction matters (because of dependencies)
		// Containers and Factory need to be destroyed last.
		std::unique_ptr<IComponentsContainers> m_ComponentsContainers;
		std::unique_ptr<IComponentsFactory> m_ComponentsFactory;

		std::unique_ptr<IResourcesManager> m_ResourcesManager;
		std::unique_ptr<RenderingManager> m_RenderingManager;
		std::unique_ptr<SystemsManager> m_SystemsManager;
		std::unique_ptr<EntitiesManager> m_EntitiesManager;
	};
}



#endif

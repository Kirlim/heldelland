
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include"Nest/Vector3.h"
#include"Nest/Component.h"


namespace Nest
{
    class PointLightComponent : public Component
    {
    public:
        PointLightComponent();
	PointLightComponent(const PointLightComponent&) = delete;
	PointLightComponent(PointLightComponent&&) = default;

        ~PointLightComponent() override;

	PointLightComponent& operator =(const PointLightComponent&) = delete;
	PointLightComponent& operator =(PointLightComponent&&) = default;


        void SetColor(Vector3<float> color);
        Vector3<float> GetColor() const;

        void SetLinearAttenuation(float linearAttenuation);
        void SetQuadraticAttenuation(float quadraticAttenuation);

        float GetLinearAttenuation() const;
        float GetQuadraticAttenuation() const;
        float GetVolumeRadius() const;

        // The dark cut is the smallest light value considered when
        // cutting the light effects, to calculate its radius
        void SetDarkCut(float darkCut);
        float GetDarkCut() const;


        static std::string GetTypeName() { return("PointLightComponent"); }



    protected:
        void RecalculateLightRadius();

        virtual void OnDestroy() override;



    protected:
        Vector3<float> m_Color;
        float m_LinearAttenuation;
        float m_QuadraticAttenuation;
        float m_DarkCut;
        float m_Radius;
    };
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<memory>
#include<unordered_map>

#include"Nest/IComponentsContainers.h"
#include"Nest/ISingleComponentsContainer.h"



namespace Nest
{
    class ComponentsContainers : public IComponentsContainers
    {
    public:
        virtual ~ComponentsContainers();



    protected:
        virtual bool DoRegisterSingleContainer(const unsigned int typeId, std::unique_ptr<ISingleComponentsContainer> container) override;
        virtual ISingleComponentsContainer* DoGetSingleContainer(const unsigned int typeId) const override;



    protected:
        std::unordered_map<unsigned int, std::unique_ptr<ISingleComponentsContainer>> m_SingleContainers;
    };
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EVENTS__EVENTS_BUS_BASE_H
#define NEST__EVENTS__EVENTS_BUS_BASE_H

#include<memory>



namespace Nest
{
	class EventsBusBase;
	class EventsBusToken;



	struct EventsBusControlledPtr
	{
		// TODO : consider making ptr access thread safe
		EventsBusBase* ptr;
	};



	class EventsBusBase
	{
		friend class EventsBusToken;

	public:
		EventsBusBase();
		virtual ~EventsBusBase();

		std::size_t GetRegisteredHandlersCount() const;
		bool IsHandlerRegistered(unsigned int tokenId) const;



	protected:
		std::weak_ptr<EventsBusControlledPtr> GetControlledPointer() const;
		void Unregister(unsigned int tokenId);


	private:
		virtual std::size_t DoGetRegisteredHandlersCount() const = 0;
		virtual bool DoIsHandlerRegistered(unsigned int tokenId) const = 0;
		virtual void DoUnregister(unsigned int tokenId) = 0;

	private:
		std::shared_ptr<EventsBusControlledPtr> m_ControlledPtr = std::make_shared<EventsBusControlledPtr>();
	};

}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EVENTS__EVENTS_BUS
#define NEST__EVENTS__EVENTS_BUS

#include<memory>
#include<functional>
#include<vector>
#include<algorithm>

#include"Nest/Events/EventsBusBase.h"
#include"Nest/Events/EventsBusToken.h"



namespace Nest
{
	template <typename EventType>
	class EventsBus : public EventsBusBase
	{
		friend class EventsBusToken;
	public:
		~EventsBus() override;

		std::unique_ptr<EventsBusToken> Register(std::function<void(EventType)> handler);
		void Signal(EventType event) const;



	private:
		std::size_t DoGetRegisteredHandlersCount() const override;
		bool DoIsHandlerRegistered(unsigned int tokenId) const override;

		unsigned int GetNextTokenId();
		void DoUnregister(unsigned int tokenId) override;



	private:
		unsigned int m_LastTokenId = 0;

		struct RegisteredEventInfo
		{
			unsigned int tokenId;
			std::function<void(EventType)> handler;

			RegisteredEventInfo(unsigned int tokenId, std::function<void(EventType)> handler)
			{
				this->tokenId = tokenId;
				this->handler = handler;
			}
		};
		std::vector<RegisteredEventInfo> m_RegisteredHandlers;
	};



	template <typename EventType>
	EventsBus<EventType>::~EventsBus() = default;



	template <typename EventType>
	std::unique_ptr<EventsBusToken> EventsBus<EventType>::Register(std::function<void(EventType)> handler)
	{
		auto tokenId = GetNextTokenId();
		auto token = std::make_unique<EventsBusToken>(GetControlledPointer(), tokenId);
		m_RegisteredHandlers.emplace_back(tokenId, handler);

		return token;
	}



	template <typename EventType>
	void EventsBus<EventType>::Signal(EventType event) const
	{
		for (auto& handler : m_RegisteredHandlers)
		{
			if (handler.handler != nullptr)
			{
				handler.handler(event);
			}
		}
	}



	template <typename EventType>
	unsigned int EventsBus<EventType>::GetNextTokenId()
	{
		return ++m_LastTokenId;
	}



	template <typename EventType>
	void EventsBus<EventType>::DoUnregister(unsigned int tokenId)
	{
		auto found = std::find_if(
			m_RegisteredHandlers.begin(),
			m_RegisteredHandlers.end(),
			[&](RegisteredEventInfo& info) { return(info.tokenId == tokenId); }
		);

		if (found != m_RegisteredHandlers.end())
		{
			m_RegisteredHandlers.erase(found);
		}
	}



	template <typename EventType>
	std::size_t EventsBus<EventType>::DoGetRegisteredHandlersCount() const
	{
		return m_RegisteredHandlers.size();
	}



	template <typename EventType>
	bool EventsBus<EventType>::DoIsHandlerRegistered(unsigned int tokenId) const
	{
		auto found = std::find_if(
			m_RegisteredHandlers.cbegin(),
			m_RegisteredHandlers.cend(),
			[&](const RegisteredEventInfo& info) { return(info.tokenId == tokenId); }
		);

		return(found != m_RegisteredHandlers.end());
	}
}


#endif

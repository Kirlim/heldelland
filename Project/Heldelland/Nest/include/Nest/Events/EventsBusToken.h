
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__EVENTS__EVENTS_BUS_TOKEN
#define NEST__EVENTS__EVENTS_BUS_TOKEN

#include<memory>



namespace Nest
{
	class EventsBusBase;
	struct EventsBusControlledPtr;



	class EventsBusToken
	{
	public:
		EventsBusToken(std::weak_ptr<EventsBusControlledPtr> eventsBusControlledPtr, unsigned int tokenId);
		~EventsBusToken();

		bool IsValid() const;
		unsigned int GetId() const;



	protected:
		std::weak_ptr<EventsBusControlledPtr> m_EventsBusControlledPtr;
		const unsigned int m_TokenId;
	};
}



#endif

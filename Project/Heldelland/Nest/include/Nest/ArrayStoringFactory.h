//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__ARRAY_STORING_FACTORY_H
#define NEST__ARRAY_STORING_FACTORY_H

#include<vector>
#include<memory>
#include<functional>

#include"Nest/Exceptions/NullArgumentException.h"
#include"Nest/StoringFactory.h"
#include"Nest/NestAssert.h"
#include"Nest/Events/EventsBus.h"



namespace Nest
{
	// Forward declarations

	template <typename DataType>
	class ArrayStoringFactory;

	template <typename DataType>
	class ArrayStoringFactoryIteratorImpl;



	// Declarations

	template <typename DataType>
	class ArrayStoringFactory : public StoringFactory<DataType>
	{
		friend class ArrayStoringFactoryIteratorImpl<DataType>;
	public:
		ArrayStoringFactory();
		~ArrayStoringFactory() override;

		std::shared_ptr<StoringFactoryHandle<DataType>> Instantiate() override;

		template <typename... Args>
		std::shared_ptr<StoringFactoryHandle<DataType>> Emplace(Args&&... args);

		void DeleteData(StoringFactoryHandle<DataType>& handle) override;
		void DeleteHandle(StoringFactoryHandle<DataType>& handle) override;
		DataType* Get(const StoringFactoryHandle<DataType>& handle) const override;

		DataType* UnsafeGetByDataIndex(int index) const;
		DataType* SetNoDataPointer(std::unique_ptr<DataType> noDataPointer) override;

		int ElementsCount() const override;
		std::unique_ptr<EventsBusToken> RegisterOnDataAddedListener(std::function<void(DataType&)>) override;
		std::unique_ptr<EventsBusToken> RegisterOnDataDeletedListener(std::function<void(DataType&)>) override;

		// Iterators for ranged-for loops
		StoringFactoryIterator<DataType> begin() const override;
		StoringFactoryIterator<DataType> end() const override;



	protected:
		EventsBus<DataType&> m_OnDataAddedEventsBus;
		EventsBus<DataType&> m_OnDataDeletedEventsBus;

		// Pointer to use as the "no data pointer"
		std::unique_ptr<DataType> m_NoDataPointer;

		// Stores the data created with this container
		std::vector<DataType> m_Storage;

		// Used to know which data can be collapsed or "recreated" due to being "unused"
		std::vector<bool> m_StorageAlive;

		// Structure used to map handle values to arrays indexes, thus
		// allowing the container to reorder elements as needed (as long
		// as care with concurrency is taken).
		struct HandleMapping
		{
			handle_type handle;
			int targetIndex;
			bool alive;
			std::function<void()> changedEventHandler;
			std::function<void()> destroyedEventHandler;
		};

		std::vector<HandleMapping> m_HandleMapping;


		int GetFreeStorageIndex();
		int GetFreeHandleIndex();

		int GetNextAvailableDataIndex(int currentDataIndex) const;
	};



	template <typename DataType>
	class ArrayStoringFactoryIteratorImpl : public StoringFactoryIteratorImpl<DataType>
	{
	public:
		ArrayStoringFactoryIteratorImpl(int index, ArrayStoringFactory<DataType>* factory);

		~ArrayStoringFactoryIteratorImpl() override = default;

		DataType& Dereference() const override;
		const ArrayStoringFactoryIteratorImpl<DataType>& PreIncrement() override;
		bool IsDifferent(const StoringFactoryIteratorImpl<DataType>& other) const override;

		ArrayStoringFactoryIteratorImpl* clone() override;



	protected:
		int m_Index;
		ArrayStoringFactory<DataType>* m_Factory;
	};



	// Definitions

	/////////////////////////
	// ArrayStoringFactory //
	/////////////////////////

	template <typename DataType>
	ArrayStoringFactory<DataType>::ArrayStoringFactory() :
		StoringFactory<DataType>()
	{
		//
	}



	template <typename DataType>
	ArrayStoringFactory<DataType>::~ArrayStoringFactory() = default;



	template <typename DataType>
	std::shared_ptr<StoringFactoryHandle<DataType>> ArrayStoringFactory<DataType>::Instantiate()
	{
		auto handle = Emplace();
		m_OnDataAddedEventsBus.Signal(*handle->Get());

		return (handle);
	}



	template <typename DataType>
	template <typename... Args>
	std::shared_ptr<StoringFactoryHandle<DataType>> ArrayStoringFactory<DataType>::Emplace(Args&&... args)
	{
		// Create new DataType instance
		int storageIndex = GetFreeStorageIndex();
		if (storageIndex == -1)
		{
			m_Storage.emplace_back(std::forward<Args>(args)...);
			m_StorageAlive.emplace_back(true);
			storageIndex = int(m_Storage.size()) - 1;
		}
		else
		{
			DataType d(std::forward<Args>(args)...);
			m_Storage[storageIndex] = std::move(d);
			m_StorageAlive[storageIndex] = true;
		}

		// Create new handle mapping
		int handleIndex = GetFreeHandleIndex();
		if (handleIndex == -1)
		{
			const bool realloc = (m_HandleMapping.size() == m_HandleMapping.capacity());
			if (realloc)
			{
				for (unsigned int i = 0; i < m_HandleMapping.size(); ++i)
				{
					if (m_HandleMapping[i].alive)
					{
						if (m_HandleMapping[i].changedEventHandler != nullptr) m_HandleMapping[i].changedEventHandler();
					}
				}
			}

			m_HandleMapping.emplace_back();
			handleIndex = int(m_HandleMapping.size()) - 1;
		}

		// Link the handle to the new data
		auto handle = std::make_shared<StoringFactoryHandle<DataType>>(handleIndex, this);
		std::weak_ptr<StoringFactoryHandle<DataType>> weakHandle(handle);

		NestAssert(handle != nullptr && !weakHandle.expired());

		m_HandleMapping[handleIndex].handle = handleIndex;
		m_HandleMapping[handleIndex].alive = true;
		m_HandleMapping[handleIndex].targetIndex = storageIndex;
		m_HandleMapping[handleIndex].changedEventHandler = ([weakHandle]() { if (!weakHandle.expired()) weakHandle.lock()->OnHandleMoved(); });
		m_HandleMapping[handleIndex].destroyedEventHandler = ([weakHandle]() { if (!weakHandle.expired()) weakHandle.lock()->OnHandleDestroyed(); });

		return (handle);
	}



	template <typename DataType>
	void ArrayStoringFactory<DataType>::DeleteData(StoringFactoryHandle<DataType>& handle)
	{
		// TODO : validate handle
		int targetIndex = m_HandleMapping[handle.GetHandleValue()].targetIndex;
		if (targetIndex < 0) return;

		m_OnDataDeletedEventsBus.Signal(*handle.Get());

		m_StorageAlive[targetIndex] = false;
		m_HandleMapping[handle.GetHandleValue()].targetIndex = -1;
	}



	template <typename DataType>
	void ArrayStoringFactory<DataType>::DeleteHandle(StoringFactoryHandle<DataType>& handle)
	{
		// TODO : validate handle and range

		DeleteData(handle);
		m_HandleMapping[handle.GetHandleValue()].alive = false;
	}



	template <typename DataType>
	DataType* ArrayStoringFactory<DataType>::Get(const StoringFactoryHandle<DataType>& handle) const
	{
		// TODO : validate handle and range
		const int targetIndex = m_HandleMapping[handle.GetHandleValue()].targetIndex;
		if (targetIndex < 0)
		{
			return (m_NoDataPointer.get());
		}
		else
		{
			auto* address = const_cast<DataType*>(&(m_Storage[targetIndex]));
			return (address);
		}
	}



	template <typename DataType>
	DataType* ArrayStoringFactory<DataType>::UnsafeGetByDataIndex(int index) const
	{
		return (const_cast<DataType*>(&m_Storage[index]));
	}



	template <typename DataType>
	DataType* ArrayStoringFactory<DataType>::SetNoDataPointer(std::unique_ptr<DataType> noDataPointer)
	{
		m_NoDataPointer.reset();
		m_NoDataPointer.swap(noDataPointer);
		return (m_NoDataPointer.get());
	}



	template <typename DataType>
	int ArrayStoringFactory<DataType>::ElementsCount() const
	{
		int count = 0;

		for (auto alive : m_StorageAlive)
		{
			if (alive) ++count;
		}

		return (count);
	}



	template <typename DataType>
	std::unique_ptr<EventsBusToken> ArrayStoringFactory<DataType>::RegisterOnDataAddedListener(std::function<void(DataType&)> listener)
	{
		if (listener == nullptr)
			ThrowNullArgumentExceptionWithLine("Listener cannot be null");

		return m_OnDataAddedEventsBus.Register(listener);
	}



	template <typename DataType>
	std::unique_ptr<EventsBusToken> ArrayStoringFactory<DataType>::RegisterOnDataDeletedListener(std::function<void(DataType&)> listener)
	{
		if (listener == nullptr)
			ThrowNullArgumentExceptionWithLine("Listener cannot be null");

		return m_OnDataDeletedEventsBus.Register(listener);
	}



	template <typename DataType>
	StoringFactoryIterator<DataType> ArrayStoringFactory<DataType>::begin() const
	{
		StoringFactoryIterator<DataType> it{ std::make_unique<ArrayStoringFactoryIteratorImpl<DataType>>(-1, const_cast<ArrayStoringFactory*>(this)) };
		++it;
		return it;
	}



	template <typename DataType>
	StoringFactoryIterator<DataType> ArrayStoringFactory<DataType>::end() const
	{
		return (StoringFactoryIterator<DataType>(std::make_unique<ArrayStoringFactoryIteratorImpl<DataType>>(int(m_Storage.size()), const_cast<ArrayStoringFactory*>(this))));
	}



	template <typename DataType>
	inline int ArrayStoringFactory<DataType>::GetFreeStorageIndex()
	{
		int index = -1;

		for (unsigned int i = 0; i < m_Storage.size(); ++i)
		{
			if (m_StorageAlive[i] == false)
			{
				index = i;
				break;
			}
		}

		return (index);
	}



	template <typename DataType>
	inline int ArrayStoringFactory<DataType>::GetFreeHandleIndex()
	{
		int index = -1;

		for (unsigned int i = 0; i < m_HandleMapping.size(); ++i)
		{
			if (m_HandleMapping[i].alive == false)
			{
				index = i;
				break;
			}
		}

		return (index);
	}



	template <typename DataType>
	int ArrayStoringFactory<DataType>::GetNextAvailableDataIndex(int currentDataIndex) const
	{
		int nextDataIndex = int(m_Storage.size());

		for (unsigned int i = currentDataIndex + 1; i < m_Storage.size(); ++i)
		{
			if (m_StorageAlive[i])
			{
				nextDataIndex = i;
				break;
			}
		}

		return (nextDataIndex);
	}



	/////////////////////////////////////
	// ArrayStoringFactoryIteratorImpl //
	/////////////////////////////////////

	template <typename DataType>
	ArrayStoringFactoryIteratorImpl<DataType>::ArrayStoringFactoryIteratorImpl(int index, ArrayStoringFactory<DataType>* factory) :
		m_Index(index),
		m_Factory(factory)
	{
		//
	}



	template <typename DataType>
	DataType& ArrayStoringFactoryIteratorImpl<DataType>::Dereference() const
	{
		return (*m_Factory->UnsafeGetByDataIndex(m_Index));
	}



	template <typename DataType>
	const ArrayStoringFactoryIteratorImpl<DataType>& ArrayStoringFactoryIteratorImpl<DataType>::PreIncrement()
	{
		m_Index = m_Factory->GetNextAvailableDataIndex(m_Index);
		return (*this);
	}



	template <typename DataType>
	bool ArrayStoringFactoryIteratorImpl<DataType>::IsDifferent(const StoringFactoryIteratorImpl<DataType>& other) const
	{
		// TODO : check if cast is valid
		return (m_Index != ((ArrayStoringFactoryIteratorImpl<DataType>&)other).m_Index);
	}



	template <typename DataType>
	ArrayStoringFactoryIteratorImpl<DataType>* ArrayStoringFactoryIteratorImpl<DataType>::clone()
	{
		return (new ArrayStoringFactoryIteratorImpl<DataType>(m_Index, m_Factory));
	}
}



#endif

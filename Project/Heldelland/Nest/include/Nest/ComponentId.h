
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__COMPONENT_ID_H
#define NEST__COMPONENT_ID_H

#include"Nest/IdType.h"



namespace Nest
{
	struct ComponentIdTraits
	{
		using underlying_type = unsigned int;
	};
	using ComponentId = IdType<ComponentIdTraits>;
}



#endif

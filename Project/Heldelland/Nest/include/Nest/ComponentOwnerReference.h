//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__COMPONENT_OWNER_REFERENCES_H
#define NEST__COMPONENT_OWNER_REFERENCES_H

#include<memory>
#include<vector>
#include<algorithm>

#include"Nest/StoringFactory.h"
#include"Nest/ComponentOwnerReferenceBase.h"
#include"Nest/ComponentReference.h"
#include"Nest/NestAssert.h"
#include"Nest/Cast.h"



namespace Nest
{
	// Forward declaration
	template <typename ComponentType>
	class ComponentReference;



	template <typename ComponentType>
	class ComponentOwnerReference : public ComponentOwnerReferenceBase
	{
		friend class ComponentReference<ComponentType>;

	public:
		ComponentOwnerReference(ComponentTypeId componentTypeId, std::shared_ptr<StoringFactoryHandle<ComponentType>> handle);
		virtual ~ComponentOwnerReference() final;

		Component* Get() const override final;



	protected:
		virtual void RegisterComponentReference(ComponentReferenceBase& reference) override;
		virtual void UnregisterComponentReference(ComponentReferenceBase& reference) override;

		void OnHandleMoved();
		void OnHandleDestroyed();



	protected:
		std::shared_ptr<StoringFactoryHandle<ComponentType>> m_Handle;
		std::vector<ComponentReferenceBase*> m_DerivedReferences;
	};



	template <typename ComponentType>
	ComponentOwnerReference<ComponentType>::ComponentOwnerReference(ComponentTypeId componentTypeId, std::shared_ptr<StoringFactoryHandle<ComponentType>> handle) :
		ComponentOwnerReferenceBase(componentTypeId),
		m_Handle(handle)
	{
		if (m_Handle == nullptr)
			ThrowNullPointerExceptionWithLine("componentHandle.get()");

		m_Handle->SetOnHandleMovedEvent([=]() { this->OnHandleMoved(); });
		m_Handle->SetOnHandleDestroyedEvent([=]() { this->OnHandleDestroyed(); });
	}



	template <typename ComponentType>
	ComponentOwnerReference<ComponentType>::~ComponentOwnerReference()
	{
		if (m_Handle != nullptr)
		{
			m_Handle->SetOnHandleMovedEvent(nullptr);
			m_Handle->SetOnHandleDestroyedEvent(nullptr);
		}

		while (!m_DerivedReferences.empty())
		{
			m_DerivedReferences[0]->Reset();
		}

		m_DerivedReferences.clear();
	}



	template <typename ComponentType>
	Component* ComponentOwnerReference<ComponentType>::Get() const
	{
		auto comp = DoCast<Component>(m_Handle->Get());
		return (comp);
	}



	template <typename ComponentType>
	void ComponentOwnerReference<ComponentType>::RegisterComponentReference(ComponentReferenceBase& reference)
	{
		NestAssert(std::find_if(
				m_DerivedReferences.begin(),
				m_DerivedReferences.end(),
				[&](ComponentReferenceBase* ref) { return(ref == &reference);}) == m_DerivedReferences.end()
		);
		m_DerivedReferences.emplace_back(&reference);
	}



	template <typename ComponentType>
	void ComponentOwnerReference<ComponentType>::UnregisterComponentReference(ComponentReferenceBase& reference)
	{
		NestAssert(std::find_if(
				m_DerivedReferences.begin(),
				m_DerivedReferences.end(),
				[&](ComponentReferenceBase* ref) { return(ref == &reference);}) != m_DerivedReferences.end()
		);
		if (!m_DerivedReferences.empty())
		{
			const auto iterator = std::find(m_DerivedReferences.begin(), m_DerivedReferences.end(), &reference);
			if (iterator != m_DerivedReferences.end()) m_DerivedReferences.erase(iterator);
		}
	}



	template <typename ComponentType>
	void ComponentOwnerReference<ComponentType>::OnHandleMoved()
	{
		for (auto derivedReference : m_DerivedReferences)
		{
			auto unsafePointer = DoCast<ComponentType>(Get());
			UpdateUnsafePointer(*derivedReference, unsafePointer);
		}
	}



	template <typename ComponentType>
	void ComponentOwnerReference<ComponentType>::OnHandleDestroyed()
	{
		while (!m_DerivedReferences.empty())
		{
			m_DerivedReferences[0]->Reset();
		}
	}
}



#endif

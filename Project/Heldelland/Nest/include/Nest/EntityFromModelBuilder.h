
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__ENTITY_FROM_MODEL_BULIDER_H
#define NEST__ENTITY_FROM_MODEL_BULIDER_H

#include"Nest/IEntityFromModelBuilder.h"



namespace Nest
{
	class EntitiesManager;
	class IResourcesManager;

	class ModelNode;
	class Transform;
	template <typename ComponentType> class ComponentReference;



	class EntityFromModelBuilder : public IEntityFromModelBuilder
	{
	public:
		EntityFromModelBuilder(EntitiesManager& entitiesManager, const IResourcesManager& resourcesManager);
		~EntityFromModelBuilder() override;



	private:
		IEntity* DoBuildVisualEntity(const ModelNode& modelRootNode) override;
		void BuildVisualEntityFromNode(const ModelNode* modelNode, ComponentReference<Transform> parentTransform) const;



	private:
		EntitiesManager& m_EntitiesManager;
		const IResourcesManager& m_ResourcesManager;
	};
}



#endif

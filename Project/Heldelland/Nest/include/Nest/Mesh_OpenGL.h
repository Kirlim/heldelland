
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__MESH_OPEN_GL_H
#define NEST__MESH_OPEN_GL_H

#include"Nest/Mesh.h"

#include<GL/glew.h>



namespace Nest
{
	class Mesh_OpenGL final : public Mesh
	{
	public:
		virtual ~Mesh_OpenGL() = default;



	private:
		void DoMake(
			const std::vector<Vector3<float>>& vertices,
			const std::vector<Vector3<float>>& normals,
			const std::vector<Vector2<float>>& uvs,
			const std::vector<unsigned int>& triangles
		) override;

		void DoUnload() override;

		void DoRender() const override;

		bool DoIsLoaded() const override;



	private:
		bool m_IsLoaded = false;
		GLuint m_VBO = 0; // Vertex Buffer Object
		GLuint m_VAO = 0; // Vertex Array Object
		GLuint m_EBO = 0; // Element Buffer Object

		std::vector<GLfloat> m_VerticesData;
		std::vector<GLuint> m_Triangles;

		bool m_HasUVs = false;
		bool m_HasNormals = false;

		unsigned int m_VerticesCount = 0;
		unsigned int m_VertexPropertiesSize = 0;

		unsigned int m_NormalsAttributeIndex = 0;
		unsigned int m_NormalsStartOffset = 0;

		unsigned int m_UVsAttributeIndex = 0;
		unsigned int m_UVsStartOffset = 0;
	};
}



#endif

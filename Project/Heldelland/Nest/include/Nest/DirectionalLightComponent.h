
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<cmath> // Included to pass sqrt to Vector3
#include"Vector3.h"
#include"Nest/Component.h"



namespace Nest
{
    class DirectionalLightComponent : public Component
    {
    public:
        DirectionalLightComponent();
	DirectionalLightComponent(const DirectionalLightComponent&) = delete;
	DirectionalLightComponent(DirectionalLightComponent&&) = default;

        ~DirectionalLightComponent() override;

	DirectionalLightComponent& operator =(const DirectionalLightComponent&) = delete;
	DirectionalLightComponent& operator =(DirectionalLightComponent&&) = default;


        void SetColor(Vector3<float> color);

        Vector3<float> GetColor() const;
        Vector3<float> GetDirection() const;

        static std::string GetTypeName() { return("DirectionalLightComponent"); }



    protected:
        virtual void OnDestroy() override;



    protected:
        Vector3<float> m_Color;
    };
}

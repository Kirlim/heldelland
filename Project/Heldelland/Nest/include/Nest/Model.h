
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__MODEL_H
#define NEST__MODEL_H

#include<memory>



namespace Nest
{
	// Forward declarations
	class ModelNode;
	class IModelLoader;
	class IEntity;
	class Transform;



	class Model
	{
		friend class IModelLoader;
		friend class IEntityFromModelBuilder;
	public:
		// Constructs a not loaded model. This constructor is here
		// to help with automatic testing
		Model();
		~Model();
		bool IsLoaded() const;



	protected:
		Model(std::unique_ptr<ModelNode>& rootNode);
		static std::unique_ptr<Model> MakeUnique(std::unique_ptr<ModelNode>& rootModelNode);



	protected:
		std::unique_ptr<ModelNode> m_RootNode;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__INPUT_ENUMERATORS_H
#define NEST__INPUT_ENUMERATORS_H



namespace Nest
{
	enum class KeyCode
	{
		A = 0, B = 1, C = 2, D = 3, E = 4, F = 5, G = 6, H = 7, I = 8, J = 9, K = 10,
		L = 11, M = 12, N = 13, O = 14, P = 15, Q = 16, R = 17, S = 18, T = 19, U = 20,
		V = 21, W = 22, X = 23, Y = 24, Z = 25,
		LeftArrow = 26, RightArrow = 27, UpArrow = 28, DownArrow = 29,
		LeftCtrl = 30, RightCtrl = 31,
		LeftShift = 32, RightShift = 33,
		LeftAlt = 34, RightAlt = 35,
		Space = 36, Backspace = 37, Enter = 38, Escape = 39,
		Alpha0 = 40, Alpha1 = 41, Alpha2 = 42, Alpha3 = 43, Alpha4 = 44,
		Alpha5 = 45, Alpha6 = 46, Alpha7 = 47, Alpha8 = 48, Alpha9 = 49,
		Backquote = 50,

		F1 = 51, F2 = 52, F3 = 53, F4 = 54, F5 = 55, F6 = 56,
		F7 = 57, F8 = 58, F9 = 59, F10 = 60, F11 = 61, F12 = 62,

		Unknow = 63,

		KeyCodeSize = 64
	};



	enum class MouseButton
	{
		Left = 0, Right = 1,
		
		Unknown = 2,

		MouseButtonSize = 3
	};
}



#endif

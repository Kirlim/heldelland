
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__STORING_FACTORY_H
#define NEST__STORING_FACTORY_H

#include<memory>
#include<functional>

#include"Nest/Events/EventsBus.h"



namespace Nest
{
	// Simple typedefs
	using handle_type = unsigned int;



	// Forward declarations

	template <class DataType>
	class StoringFactoryIterator;

	template <class DataType>
	class StoringFactoryIteratorImpl;

	template <class DataType>
	class StoringFactory;

	template <class DataType>
	class StoringFactoryHandle;



	// Classes declarations
	template <class DataType>
	class StoringFactoryIterator
	{
	public:
		StoringFactoryIterator(const StoringFactoryIterator<DataType>& other);
		StoringFactoryIterator(std::unique_ptr<StoringFactoryIteratorImpl<DataType>> iteratorImplementation);
		~StoringFactoryIterator() = default;

		DataType& operator *() const;
		const StoringFactoryIterator& operator ++();
		bool operator !=(const StoringFactoryIterator& other) const;



	protected:
		std::unique_ptr<StoringFactoryIteratorImpl<DataType>> m_StoringFactoryIteratorImpl;
	};



	template <class DataType>
	class StoringFactoryIteratorImpl
	{
	public:
		StoringFactoryIteratorImpl() {}
		virtual ~StoringFactoryIteratorImpl() {}

		virtual DataType& Dereference() const = 0;
		virtual const StoringFactoryIteratorImpl& PreIncrement() = 0;
		virtual bool IsDifferent(const StoringFactoryIteratorImpl& other) const = 0;

		virtual StoringFactoryIteratorImpl* clone() = 0;
	};



	template <class DataType>
	class StoringFactory
	{
	public:
		virtual ~StoringFactory() {}

		// Emplaces a new instance. This method forces the DataType class to have
		// an parameterless constructor...
		virtual std::shared_ptr<StoringFactoryHandle<DataType>> Instantiate() = 0;

		// Deletes the data pointed by a handle. The handle won't be discarded,
		// so any holding handle to the same data will still be "valid", and
		// return noDataPointer.
		virtual void DeleteData(StoringFactoryHandle<DataType>& handle) = 0;

		// Deletes the data pointed by a handle, and the handle itself. Using the
		// same holding handle after deleting it will result in undefined behavior.
		virtual void DeleteHandle(StoringFactoryHandle<DataType>& handle) = 0;
		virtual DataType* Get(const StoringFactoryHandle<DataType>& handle) const = 0;

		virtual DataType* SetNoDataPointer(std::unique_ptr<DataType> noDataPointer) = 0;

		// Returns the number of registered elements in this container. The name is not
		// size as it could be misleading.
		virtual int ElementsCount() const = 0;

		virtual std::unique_ptr<EventsBusToken> RegisterOnDataAddedListener(std::function<void(DataType&)>) = 0;
		virtual std::unique_ptr<EventsBusToken> RegisterOnDataDeletedListener(std::function<void(DataType&)>) = 0;

		// Iterators for ranged-for loops
		virtual StoringFactoryIterator<DataType> begin() const = 0;
		virtual StoringFactoryIterator<DataType> end() const = 0;
	};



	template <class DataType>
	class StoringFactoryHandle
	{
	public:
		StoringFactoryHandle(handle_type handle, StoringFactory<DataType>* factory);
		~StoringFactoryHandle();

		inline DataType* Get() const;
		inline handle_type GetHandleValue() const; // TODO : restrict creation access

		void SetOnHandleMovedEvent(std::function<void()> eventHandler);
		void SetOnHandleDestroyedEvent(std::function<void()> eventHandler);

		void OnHandleMoved();
		void OnHandleDestroyed();



	protected:
		StoringFactory<DataType>* m_Factory;
		handle_type m_Handle;

		std::function<void()> m_OnHandleMovedEvent;
		std::function<void()> m_OnHandleDestroyedEvent;
	};



	// Classes definitions

	////////////////////////////
	// StoringFactoryIterator //
	////////////////////////////

	template <class DataType>
	StoringFactoryIterator<DataType>::StoringFactoryIterator(const StoringFactoryIterator<DataType>& other)
	{
		m_StoringFactoryIteratorImpl.reset(other.m_StoringFactoryIteratorImpl->clone());
	}

	template <class DataType>
	StoringFactoryIterator<DataType>::StoringFactoryIterator(std::unique_ptr<StoringFactoryIteratorImpl<DataType>> iteratorImplementation) :
		m_StoringFactoryIteratorImpl(std::move(iteratorImplementation))
	{
		//
	}



	template <class DataType>
	DataType& StoringFactoryIterator<DataType>::operator *() const
	{
		return(m_StoringFactoryIteratorImpl.get()->Dereference());
	}



	template <class DataType>
	const StoringFactoryIterator<DataType>& StoringFactoryIterator<DataType>::operator ++()
	{
		// TODO : Assert for nullptr
		m_StoringFactoryIteratorImpl.get()->PreIncrement();
		return(*this);
	}



	template <class DataType>
	bool StoringFactoryIterator<DataType>::operator !=(const StoringFactoryIterator& other) const
	{
		return(m_StoringFactoryIteratorImpl.get()->IsDifferent(*(other.m_StoringFactoryIteratorImpl.get())));
	}



	//////////////////////////
	// StoringFactoryHandle //
	//////////////////////////

	template <class DataType>
	StoringFactoryHandle<DataType>::StoringFactoryHandle(handle_type handle, StoringFactory<DataType>* factory) :
		m_Factory(factory),
		m_Handle(handle),
		m_OnHandleMovedEvent(nullptr),
		m_OnHandleDestroyedEvent(nullptr)
	{
		//
	}



	template <class DataType>
	StoringFactoryHandle<DataType>::~StoringFactoryHandle()
	{
		// TODO : assert valid on debug
		m_Factory->DeleteHandle(*this);
	}



	template <class DataType>
	inline DataType* StoringFactoryHandle<DataType>::Get() const
	{
		return(m_Factory->Get(*this));
	}



	template <class DataType>
	inline handle_type StoringFactoryHandle<DataType>::GetHandleValue() const
	{
		return(m_Handle);
	}



	template <class DataType>
	void StoringFactoryHandle<DataType>::SetOnHandleMovedEvent(std::function<void()> eventHandler)
	{
		m_OnHandleMovedEvent = eventHandler;
	}



	template <class DataType>
	void StoringFactoryHandle<DataType>::SetOnHandleDestroyedEvent(std::function<void()> eventHandler)
	{
		m_OnHandleDestroyedEvent = eventHandler;
	}



	template <class DataType>
	void StoringFactoryHandle<DataType>::OnHandleMoved()
	{
		if (m_OnHandleMovedEvent != nullptr)
		{
			m_OnHandleMovedEvent();
		}
	}



	template <class DataType>
	void StoringFactoryHandle<DataType>::OnHandleDestroyed()
	{
		if (m_OnHandleDestroyedEvent != nullptr)
		{
			m_OnHandleDestroyedEvent();
		}
	}
}



#endif

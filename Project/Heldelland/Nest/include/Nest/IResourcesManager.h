//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef  NEST__IRESOURCES_MANAGER_H
#define NEST__IRESOURCES_MANAGER_H



#include<string>
#include<memory>
#include<unordered_map>



namespace Nest
{
	// Forward declarations
	class Font;
	class Material;
	class Mesh;
	class Model;
	class ShaderProgram;
	class Texture;



	// Handles the storing of resources currently in memory.
	//
	// When adding a new resource to the manager, the following
	// exceptions may occur:
	//	- LogicErrorException: resource name is empty.
	//  - AlreadyRegisteredException: a resource of the same name is already registered.
	//  - NullArgumentException: the resourced passed to the add method is
	//    null.
	class IResourcesManager
	{
	public:
		virtual ~IResourcesManager();

		IResourcesManager(IResourcesManager const&) = delete;
		IResourcesManager(IResourcesManager&&) = default;

		IResourcesManager& operator=(IResourcesManager const&) = delete;
		IResourcesManager& operator=(IResourcesManager&&) = default;

		// Methods
		void AddFont(std::string resourceName, std::unique_ptr<Font>& font);
		void UnloadFont(std::string resourceName);

		void AddMaterial(std::string resourceName, std::unique_ptr<Material>& material);
		void UnloadMaterial(std::string resourceName);

		void AddMesh(std::string resourceName, std::unique_ptr<Mesh>& mesh);
		void UnloadMesh(std::string resourceName);

		void AddModel(std::string resourceName, std::unique_ptr<Model>& model);
		void UnloadModel(std::string resourceName);

		void LoadShaderProgram(std::string vertexShaderFile, std::string fragmentShaderFile, std::string resourceName);
		void LoadShaderProgramContents(std::string vertexShaderContents, std::string fragmentShaderContents, std::string resourceName);
		void AddShaderProgram(std::string resourceName, std::unique_ptr<ShaderProgram>& shaderProgram);
		void UnloadShaderProgram(std::string resourceName);

		void AddTexture(std::string resourceName, std::unique_ptr<Texture>& texture);
		void UnloadTexture(std::string resourceName);


		Font* FetchFont(std::string resourceName) const;
		Material* FetchMaterial(std::string resourceName) const;
		Mesh* FetchMesh(std::string resourceName) const;
		Model* FetchModel(std::string resourceName) const;
		ShaderProgram* FetchShaderProgram(std::string resourceName) const;
		Texture* FetchTexture(std::string resourceName) const;



	protected:
		IResourcesManager() = default;



	private:
		virtual void DoAddFont(std::string resourceName, std::unique_ptr<Font>& font) = 0;
		virtual void DoUnloadFont(std::string resourceName) = 0;

		virtual void DoAddMaterial(std::string resourceName, std::unique_ptr<Material>& material) = 0;
		virtual void DoUnloadMaterial(std::string resourceName) = 0;

		virtual void DoAddMesh(std::string resourceName, std::unique_ptr<Mesh>& mesh) = 0;
		virtual void DoUnloadMesh(std::string resourceName) = 0;

		virtual void DoAddModel(std::string resourceName, std::unique_ptr<Model>& model) = 0;
		virtual void DoUnloadModel(std::string resourceName) = 0;

		virtual void DoLoadShaderProgram(std::string vertexShaderFile, std::string fragmentShaderFile, std::string resourceName) = 0;
		virtual void DoLoadShaderProgramContents(std::string vertexShaderContents, std::string fragmentShaderContents, std::string resourceName) = 0;
		virtual void DoAddShaderProgram(std::string resourceName, std::unique_ptr<ShaderProgram>& shaderProgram) = 0;
		virtual void DoUnloadShaderProgram(std::string resourceName) = 0;

		virtual void DoAddTexture(std::string resourceName, std::unique_ptr<Texture>& texture) = 0;
		virtual void DoUnloadTexture(std::string resourceName) = 0;

		virtual Font* DoFetchFont(std::string resourceName) const = 0;
		virtual Material* DoFetchMaterial(std::string resourceName) const = 0;
		virtual Mesh* DoFetchMesh(std::string resourceName) const = 0;
		virtual Model* DoFetchModel(std::string resourceName) const = 0;
		virtual ShaderProgram* DoFetchShaderProgram(std::string resourceName) const = 0;
		virtual Texture* DoFetchTexture(std::string resourceName) const = 0;
	};
}



#endif

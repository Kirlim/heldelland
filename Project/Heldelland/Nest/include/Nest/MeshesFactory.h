
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef NEST__MESHES_FACTORY_H
#define NEST__MESHES_FACTORY_H

#include<memory>
#include<functional>

// TODO : figure out a way of this include not being here
#include"Nest/Mesh.h"



namespace Nest
{
	// Forward declarations
	template <typename NumberType> class Vector2;
	template <typename NumberType> class Vector3;



	class MeshesFactory
	{
	public:
		explicit MeshesFactory(const std::function<std::unique_ptr<Mesh>()> meshInstantiationFunction);
		~MeshesFactory() = default;

		std::unique_ptr<Mesh> InstantiateEmpty() const;

		std::unique_ptr<Mesh> InstantiateRectangle(const Vector3<float>& size, const Vector3<float>& center) const;
		std::unique_ptr<Mesh> InstantiateBox(const Vector3<float>& size, const Vector3<float>& center) const;
		std::unique_ptr<Mesh> InstantiateSphere(const float radius, const int horizontalSlices, const int verticalSlices) const;



	private:
		std::function<std::unique_ptr<Mesh>()> m_MeshInstantiationFunction;
	};
}



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#pragma once

#include<vector>
#include<string>



namespace Nest
{
    // Handles assets paths and the redirection of meta data to their
    // specific loaders.
    class AssetsManager
    {
    public:
        AssetsManager();
        ~AssetsManager();

        // Adds the assets path to the paths list and recursively try
        // to load all meta data. If the path has already been loaded
        // once, this method does nothing.
        void LoadAssetsPath(std::string);



    protected:
        std::vector<std::string> m_AssetsPaths;
    };
}

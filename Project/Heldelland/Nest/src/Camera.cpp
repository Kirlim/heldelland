//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Camera.h"

#include<cmath>

#include"Nest/Vector2.h"
#include"Nest/Vector3.h"
#include"Nest/Vector4.h"
#include"Nest/Ray.h"
#include"Nest/Transform.h"
#include"Nest/IEntity.h"
#include"Nest/Engine.h"
#include"Nest/RenderingModule.h"
#include"Nest/Global.h"



namespace Nest
{
	Camera::Camera() :
		m_ProjectionWidth(0),
		m_ProjectionHeight(0),
		m_NearClipPlane(0),
		m_FarClipPlane(0),
		m_FovRadians(0)
	{
		// Nothing here
	}



	Camera::~Camera() = default;



	void Camera::SetAsOrthographic(float projectionWidth, float projectionHeight, float nearClipPlane, float farClipPlane)
	{
		// TODO : find and fix cause for inverted near and far clip planes
		m_ProjectionWidth = projectionWidth;
		m_ProjectionHeight = projectionHeight;
		m_NearClipPlane = nearClipPlane;
		m_FarClipPlane = farClipPlane;

		const float right = m_ProjectionWidth * 0.5f;
		const float left = m_ProjectionWidth * -0.5f;
		const float top = m_ProjectionHeight * 0.5f;
		const float bottom = m_ProjectionHeight * -0.5f;

		const float far = m_FarClipPlane;
		const float near = m_NearClipPlane;

		m_ProjectionType = ProjectionType::Orthographic;

		m_ProjectionMatrix.Set
		(
			2 / (right - left), 0, 0, - ((right + left) / (right - left)),
			0, 2 / (top - bottom), 0, - ((top + bottom) / (top - bottom)),
			0, 0, - 2 / (far - near), - ((far + near) / (far - near)),
			0, 0, 0, 1
		);

		RecalculateMatrices();
	}



	void Camera::SetAsPerspective(float projectionWidth, float projectionHeight, float nearClipPlane, float farClipPlane, float fovRadians)
	{
		m_ProjectionWidth = projectionWidth;
		m_ProjectionHeight = projectionHeight;
		m_NearClipPlane = nearClipPlane;
		m_FarClipPlane = farClipPlane;
		m_FovRadians = fovRadians;

		const float aspect = m_ProjectionWidth / m_ProjectionHeight;
		const float top = m_NearClipPlane * tan(m_FovRadians * 0.5f);
		const float bottom = -top;
		const float right = top * aspect;
		const float left = -right;

		m_ProjectionType = ProjectionType::Perspective;

		m_ProjectionMatrix.Set
		(
			(2 * m_NearClipPlane) / (right - left), 0, (right + left) / (right - left), 0,
			0, (2 * m_NearClipPlane) / (top - bottom), (top + bottom) / (top - bottom), 0,
			0, 0, -((m_FarClipPlane + m_NearClipPlane) / (m_FarClipPlane - m_NearClipPlane)), -((2 * m_FarClipPlane * m_NearClipPlane) / (m_FarClipPlane - m_NearClipPlane)),
			0, 0, -1, 0
		);

		RecalculateMatrices();
	}



	float Camera::GetProjectionWidth() const { return (m_ProjectionWidth); }
	float Camera::GetProjectionHeight() const { return (m_ProjectionHeight); }
	float Camera::GetNearClipPlane() const { return (m_NearClipPlane); }
	float Camera::GetFarClipPlane() const { return (m_FarClipPlane); }
	float Camera::GetFovRadians() const { return (m_FovRadians); }
	ProjectionType Camera::GetProjectionType() const { return (m_ProjectionType); }



	void Camera::RecalculateMatrices()
	{
		m_InverseProjectionMatrix = m_ProjectionMatrix.GetInverse();


		// The other matrices need a transform component
		auto transform = GetOwner()->GetComponent<Transform>();
		if (transform == nullptr) return;

		auto m_InverseViewMatrix = transform->GetTransformMatrix(); // The view matrix is the inverse of the transform matrix
		m_ViewMatrix = m_InverseViewMatrix.GetInverse();
		m_InverseViewProjectionMatrix = (m_ProjectionMatrix * m_ViewMatrix).GetInverse();
	}



	Matrix4<GLfloat> Camera::GetProjectionMatrix() const { return (m_ProjectionMatrix); }
	Matrix4<GLfloat> Camera::GetViewMatrix() const { return (m_ViewMatrix); }
	Matrix4<GLfloat> Camera::GetInverseProjectionMatrix() const { return (m_InverseProjectionMatrix); }
	Matrix4<GLfloat> Camera::GetInverseViewMatrix() const { return (m_InverseViewMatrix); }
	Matrix4<GLfloat> Camera::GetInverseViewProjectionMatrix() const { return (m_InverseViewProjectionMatrix); }



	Vector3<float> Camera::ScreenPointToWorld(Vector2<float> screenpointPosition, float plane) const
	{
		// Convert screenpoint to viewport
		const auto renderingModule = Global->GetEngine()->GetRenderingModule();

		screenpointPosition.x -= renderingModule->GetScreenWidth() * 0.5f;
		screenpointPosition.y -= renderingModule->GetScreenHeight() * 0.5f;

		screenpointPosition.x /= renderingModule->GetScreenWidth() * 0.5f;
		screenpointPosition.y /= -renderingModule->GetScreenHeight() * 0.5f; // Screenpoint starts at top left

		// Return
		return (ViewportToWorld(screenpointPosition, plane));
	}



	Vector3<float> Camera::ViewportToWorld(Vector2<float> viewportPosition, float plane) const
	{
		// Find plane in the projection
		Vector4<float> planeInProjection(0, 0, plane, 1.0f);
		planeInProjection = m_ProjectionMatrix * planeInProjection; // This can be optimized
		planeInProjection /= planeInProjection.w();


		// Calculate world point
		Vector4<GLfloat> point(viewportPosition.x, viewportPosition.y, planeInProjection.z(), 1.0f);
		point = m_InverseViewProjectionMatrix * point;
		point /= point.w();

		return (Vector3<float>(point.x(), point.y(), point.z()));
	}



	Ray Camera::ScreenPointToRay(Vector2<float> screenpointPosition) const
	{
		const auto worldPoint = ScreenPointToWorld(screenpointPosition, -1.0f);
		// TODO : treat situation where camera might, somehow, not have a transform.
		const auto cameraPosition = GetOwner()->GetComponent<Transform>()->GetPosition();

		auto direction = worldPoint - cameraPosition;
		direction.Normalize();

		Ray ray;
		ray.origin = cameraPosition;
		ray.direction = direction;
		return (ray);
	}



	void Camera::OnDestroy()
	{
		// Nothing here
	}
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/PrefabComponentsFactory.h"



namespace Nest
{
    PrefabComponentsFactory::PrefabComponentsFactory(const unsigned int typeId, const std::string typeName, const std::function<std::unique_ptr<ComponentOwnerReferenceBase>()> factoryFunction) :
        BasicComponentsFactory(),
        m_FactoryFunction(factoryFunction), m_TypeId(typeId), m_TypeName(typeName)
    {
        // Nothing here
    }



    PrefabComponentsFactory::~PrefabComponentsFactory()
    {
        // Nothing here
    }



    std::string PrefabComponentsFactory::GetTypeName() const
    {
        return(m_TypeName);
    }



    unsigned int PrefabComponentsFactory::GetTypeId() const
    {
        return(m_TypeId);
    }



    std::unique_ptr<ComponentOwnerReferenceBase> PrefabComponentsFactory::Instantiate()
    {
        // TODO : assert, on debug, that the factory function is not null
        return(m_FactoryFunction());
    }
}
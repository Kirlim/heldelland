//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/TextMeshImplOpenGL.h"

#include"Nest/Vector2.h"

#include"Nest/Exceptions/InvalidOperationException.h"
#include"Nest/Font.h"
#include"Nest/Material.h"

#include"Nest/Helpers/StringHelper.h"



namespace Nest
{
	TextMeshImplOpenGL::TextMeshImplOpenGL()
	{
		glGenVertexArrays(1, &m_VAO);
		glGenBuffers(1, &m_VBO);

		glBindVertexArray(m_VAO);
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, nullptr, GL_DYNAMIC_DRAW);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}



	TextMeshImplOpenGL::~TextMeshImplOpenGL() = default;



	void TextMeshImplOpenGL::RenderText(std::string text, const Font* font, float fontSize)
	{
		if (text.empty() || font == nullptr) return;

		glActiveTexture(GL_TEXTURE0);
		glBindVertexArray(m_VAO);

		decltype(Helpers::Utf8ToUtf32(text)) utf32text;
		try
		{
			utf32text = Helpers::Utf8ToUtf32(text);
		}
		catch (...)
		{
			// TODO : consider making the setters of the text to guarantee that the text is UTF8? If so, remember to rename the parameter
			// Assume ISO-8859-1. 
			for (const auto& c : text)
			{
				utf32text.emplace_back(c);
			}
		}

		GLfloat x = 0.0f;
		const GLfloat y = 0.0f;

		for (const auto c : utf32text)
		{
			Vector2<int> size;
			Vector2<int> bearing;
			int advance = 0;

			if (!font->GetCharacterInfo(c, /*out*/ size, /*out*/ bearing, /*out*/ advance))
			{
				// TODO : Decide on rendering a "character not found" texture?
				continue;
			}

			Texture* texture = font->GetCharacterTexture(c);
			if (texture == nullptr)
				ThrowInvalidOperationExceptionWithLine("Null texture for character of decimal number " + std::to_string(c));


			const GLfloat xpos = x + GLfloat(bearing.x) * GLfloat(fontSize);
			const GLfloat ypos = y - (GLfloat(size.y) - GLfloat(bearing.y)) * GLfloat(fontSize);

			const GLfloat w = GLfloat(size.x) * GLfloat(fontSize);
			const GLfloat h = GLfloat(size.y) * GLfloat(fontSize);


			// Update the VBO
			GLfloat v[6][4] = {
				{ xpos, ypos + h, 0.0f, 0.0f },
				{ xpos, ypos, 0.0f, 1.0f },
				{ xpos + w, ypos, 1.0f, 1.0f },

				{ xpos, ypos + h, 0.0f, 0.0f },
				{ xpos + w, ypos, 1.0f, 1.0f },
				{ xpos + w, ypos + h, 1.0f, 0.0f }
			};


			// Render quad
			glBindTexture(GL_TEXTURE_2D, texture->GetTextureId());
			glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
			glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(v), v);

			glBindBuffer(GL_ARRAY_BUFFER, 0);

			glDrawArrays(GL_TRIANGLES, 0, 6);


			// Advance the text
			x += GLfloat(advance >> 6) * GLfloat(fontSize);
		}

		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

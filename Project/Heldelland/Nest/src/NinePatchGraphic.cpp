
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/NinePatchGraphic.h"

#include"Nest/NestAssert.h"
#include"Nest/World.h"
#include"Nest/IResourcesManager.h"
#include"Nest/IEntity.h"
#include"Nest/EntitiesManager.h"
#include"Nest/Transform.h"
#include"Nest/MeshFilter.h"
#include"Nest/MeshRenderer.h"



namespace Nest
{
	NinePatchGraphic::NinePatchGraphic() : Component()
	{
		// Nothing here
	}



	NinePatchGraphic::~NinePatchGraphic()
	{
		// Nothing here
	}



	void NinePatchGraphic::SetFrameSizes(
		Vector2f topLeftSize, float topWidth, Vector2f topRightSize,
		float leftWidth, float rightWidth,
		Vector2f bottomLeftSize, float bottomWidth, Vector2f bottomRightSize
	)
	{
		m_TopLeftSize = topLeftSize;
		m_TopWidth = topWidth;
		m_TopRightSize = topRightSize;
		m_LeftWidth = leftWidth;
		m_RightWidth = rightWidth;
		m_BottomLeftSize = bottomLeftSize;
		m_BottomWidth = bottomWidth;
		m_BottomRightSize = bottomRightSize;

		RefreshAllMeshes();
	}



	void NinePatchGraphic::SetBodySize(Vector2f size)
	{
		m_BodySize = size;

		RefreshAllMeshes();
		RefreshAllPositions();
	}



	void NinePatchGraphic::SetBodySize(float width, float height)
	{
		SetBodySize(Vector2f(width, height));
	}



	void NinePatchGraphic::SetMaterials(
		Material* topLeftMaterial, Material* topMaterial, Material* topRightMaterial,
		Material* leftMaterial, Material* bodyMaterial, Material* rightMaterial,
		Material* bottomLeftMaterial, Material* bottomMaterial, Material* bottomRightMaterial
	)
	{
		m_MatTopLeft = topLeftMaterial;
		m_MatTop = topMaterial;
		m_MatTopRight = topRightMaterial;
		m_MatLeft = leftMaterial;
		m_MatMiddle = bodyMaterial;
		m_MatRight = rightMaterial;
		m_MatBottomLeft = bottomLeftMaterial;
		m_MatBottom = bottomMaterial;
		m_MatBottomRight = bottomRightMaterial;

		RefreshAllMaterials();
	}



	void NinePatchGraphic::SetPosition(Vector3f position) const
	{
		auto transform = m_Owner->GetComponent<Transform>();

		NestAssert(transform != nullptr);
		transform->SetLocalPosition(position);
	}

	void NinePatchGraphic::SetPosition(float x, float y, float z) const
	{
		SetPosition(Vector3f(x, y, z));
	}



	void NinePatchGraphic::SetCenter(Vector3f center)
	{
		m_Center = center;
		RefreshAllPositions();
	}

	void NinePatchGraphic::SetCenter(float x, float y, float z)
	{
		SetCenter(Vector3f(x, y, z));
	}



	void NinePatchGraphic::OnCreate()
	{
		SetUpGraphic();

		m_MatTopLeft = nullptr;
		m_MatTop = nullptr;
		m_MatTopRight = nullptr;
		m_MatLeft = nullptr;
		m_MatMiddle = nullptr;
		m_MatRight = nullptr;
		m_MatBottomLeft = nullptr;
		m_MatBottom = nullptr;
		m_MatBottomRight = nullptr;

		RefreshAllMaterials();
		RefreshAllMeshes();
		RefreshAllPositions();
	}



	void NinePatchGraphic::OnDestroy()
	{
		// Nothing here
	}



	void NinePatchGraphic::SetUpGraphic()
	{
		// Guarantee that we have a transform.
		auto transform = m_Owner->GetComponent<Transform>();

		if (transform == nullptr)
		{
			transform = m_Owner->AddComponent<Transform>();
		}

		SetUpGraphicEntities();
		SetUpGraphicAddComponents();
		SetUpGraphicTransforms();
		SetUpGraphicMeshes();
		SetUpGraphicRenderers();
	}



	void NinePatchGraphic::SetUpGraphicEntities()
	{
		auto entitiesManager = GetOwner()->GetEntitiesManager();

		m_TopLeft = entitiesManager->Instantiate();
		m_Top = entitiesManager->Instantiate();
		m_TopRight = entitiesManager->Instantiate();
		m_Left = entitiesManager->Instantiate();
		m_Middle = entitiesManager->Instantiate();
		m_Right = entitiesManager->Instantiate();
		m_BottomLeft = entitiesManager->Instantiate();
		m_Bottom = entitiesManager->Instantiate();
		m_BottomRight = entitiesManager->Instantiate();
	}



	void NinePatchGraphic::SetUpGraphicAddComponents() const
	{
		auto topLeftEntity = m_TopLeft.lock();
		auto topEntity = m_Top.lock();
		auto topRightEntity = m_TopRight.lock();
		auto leftEntity = m_Left.lock();
		auto middleEntity = m_Middle.lock();
		auto rightEntity = m_Right.lock();
		auto bottomLeftEntity = m_BottomLeft.lock();
		auto bottomEntity = m_Bottom.lock();
		auto bottomRightEntity = m_BottomRight.lock();

		topLeftEntity->AddComponent<Transform>();
		topEntity->AddComponent<Transform>();
		topRightEntity->AddComponent<Transform>();
		leftEntity->AddComponent<Transform>();
		middleEntity->AddComponent<Transform>();
		rightEntity->AddComponent<Transform>();
		bottomLeftEntity->AddComponent<Transform>();
		bottomEntity->AddComponent<Transform>();
		bottomRightEntity->AddComponent<Transform>();

		topLeftEntity->AddComponent<MeshFilter>();
		topEntity->AddComponent<MeshFilter>();
		topRightEntity->AddComponent<MeshFilter>();
		leftEntity->AddComponent<MeshFilter>();
		middleEntity->AddComponent<MeshFilter>();
		rightEntity->AddComponent<MeshFilter>();
		bottomLeftEntity->AddComponent<MeshFilter>();
		bottomEntity->AddComponent<MeshFilter>();
		bottomRightEntity->AddComponent<MeshFilter>();

		topLeftEntity->AddComponent<MeshRenderer>();
		topEntity->AddComponent<MeshRenderer>();
		topRightEntity->AddComponent<MeshRenderer>();
		leftEntity->AddComponent<MeshRenderer>();
		middleEntity->AddComponent<MeshRenderer>();
		rightEntity->AddComponent<MeshRenderer>();
		bottomLeftEntity->AddComponent<MeshRenderer>();
		bottomEntity->AddComponent<MeshRenderer>();
		bottomRightEntity->AddComponent<MeshRenderer>();
	}



	void NinePatchGraphic::SetUpGraphicTransforms() const
	{
		const auto transform = m_Owner->GetComponent<Transform>();

		auto topLeftEntity = m_TopLeft.lock();
		auto topEntity = m_Top.lock();
		auto topRightEntity = m_TopRight.lock();
		auto leftEntity = m_Left.lock();
		auto middleEntity = m_Middle.lock();
		auto rightEntity = m_Right.lock();
		auto bottomLeftEntity = m_BottomLeft.lock();
		auto bottomEntity = m_Bottom.lock();
		auto bottomRightEntity = m_BottomRight.lock();

		auto topLeftTransform = topLeftEntity->GetComponent<Transform>();
		auto topTransform = topEntity->GetComponent<Transform>();
		auto topRightTransform = topRightEntity->GetComponent<Transform>();
		auto leftTransform = leftEntity->GetComponent<Transform>();
		auto middleTransform = middleEntity->GetComponent<Transform>();
		auto rightTransform = rightEntity->GetComponent<Transform>();
		auto bottomLeftTransform = bottomLeftEntity->GetComponent<Transform>();
		auto bottomTransform = bottomEntity->GetComponent<Transform>();
		auto bottomRightTransform = bottomRightEntity->GetComponent<Transform>();

		topLeftTransform->SetParent(transform);
		topTransform->SetParent(transform);
		topRightTransform->SetParent(transform);
		leftTransform->SetParent(transform);
		middleTransform->SetParent(transform);
		rightTransform->SetParent(transform);
		bottomLeftTransform->SetParent(transform);
		bottomTransform->SetParent(transform);
		bottomRightTransform->SetParent(transform);
	}



	void NinePatchGraphic::SetUpGraphicMeshes() const
	{
		auto topLeftEntity = m_TopLeft.lock();
		auto topEntity = m_Top.lock();
		auto topRightEntity = m_TopRight.lock();
		auto leftEntity = m_Left.lock();
		auto middleEntity = m_Middle.lock();
		auto rightEntity = m_Right.lock();
		auto bottomLeftEntity = m_BottomLeft.lock();
		auto bottomEntity = m_Bottom.lock();
		auto bottomRightEntity = m_BottomRight.lock();

		auto topLeftMeshFilter = topLeftEntity->GetComponent<MeshFilter>();
		auto topMeshFilter = topEntity->GetComponent<MeshFilter>();
		auto topRightMeshFilter = topRightEntity->GetComponent<MeshFilter>();
		auto leftMeshFilter = leftEntity->GetComponent<MeshFilter>();
		auto middleMeshFilter = middleEntity->GetComponent<MeshFilter>();
		auto rightMeshFilter = rightEntity->GetComponent<MeshFilter>();
		auto bottomLeftMeshFilter = bottomLeftEntity->GetComponent<MeshFilter>();
		auto bottomMeshFilter = bottomEntity->GetComponent<MeshFilter>();
		auto bottomRightMeshFilter = bottomRightEntity->GetComponent<MeshFilter>();

		// TODO : more failproof way of getting the mesh
		const auto resourcesManager = GetOwner()->GetEntitiesManager()->GetResourcesManager();
		const auto mesh = resourcesManager->FetchMesh("Square1x1Centered");
		topLeftMeshFilter->SetMesh(mesh);
		topMeshFilter->SetMesh(mesh);
		topRightMeshFilter->SetMesh(mesh);
		leftMeshFilter->SetMesh(mesh);
		middleMeshFilter->SetMesh(mesh);
		rightMeshFilter->SetMesh(mesh);
		bottomLeftMeshFilter->SetMesh(mesh);
		bottomMeshFilter->SetMesh(mesh);
		bottomRightMeshFilter->SetMesh(mesh);
	}



	void NinePatchGraphic::SetUpGraphicRenderers() const
	{
		auto topLeftEntity = m_TopLeft.lock();
		auto topEntity = m_Top.lock();
		auto topRightEntity = m_TopRight.lock();
		auto leftEntity = m_Left.lock();
		auto middleEntity = m_Middle.lock();
		auto rightEntity = m_Right.lock();
		auto bottomLeftEntity = m_BottomLeft.lock();
		auto bottomEntity = m_Bottom.lock();
		auto bottomRightEntity = m_BottomRight.lock();

		auto topLeftMeshRenderer = topLeftEntity->GetComponent<MeshRenderer>();
		auto topMeshRenderer = topEntity->GetComponent<MeshRenderer>();
		auto topRightMeshRenderer = topRightEntity->GetComponent<MeshRenderer>();
		auto leftMeshRenderer = leftEntity->GetComponent<MeshRenderer>();
		auto middleMeshRenderer = middleEntity->GetComponent<MeshRenderer>();
		auto rightMeshRenderer = rightEntity->GetComponent<MeshRenderer>();
		auto bottomLeftMeshRenderer = bottomLeftEntity->GetComponent<MeshRenderer>();
		auto bottomMeshRenderer = bottomEntity->GetComponent<MeshRenderer>();
		auto bottomRightMeshRenderer = bottomRightEntity->GetComponent<MeshRenderer>();

		topLeftMeshRenderer->SetGuiFlag(true);
		topMeshRenderer->SetGuiFlag(true);
		topRightMeshRenderer->SetGuiFlag(true);
		leftMeshRenderer->SetGuiFlag(true);
		middleMeshRenderer->SetGuiFlag(true);
		rightMeshRenderer->SetGuiFlag(true);
		bottomLeftMeshRenderer->SetGuiFlag(true);
		bottomMeshRenderer->SetGuiFlag(true);
		bottomRightMeshRenderer->SetGuiFlag(true);
	}



	void NinePatchGraphic::UpdateEntityMeshScale(EntityReference entityRef, Vector2f scale) const
	{
		NestAssert(!entityRef.expired());
		auto entity = entityRef.lock();
		auto transform = entity->GetComponent<Transform>();

		NestAssert(transform != nullptr);
		transform->SetLocalScale(Vector3f(scale.x, scale.y, 1.0f));
	}



	void NinePatchGraphic::UpdateEntityMaterial(EntityReference entityRef, Material* material) const
	{
		NestAssert(!entityRef.expired());
		auto entity = entityRef.lock();
		auto meshRenderer = entity->GetComponent<MeshRenderer>();

		NestAssert(meshRenderer != nullptr);
		meshRenderer->SetMaterial(material);
	}



	void NinePatchGraphic::RefreshEntityPosition(EntityReference entityRef, Vector3f position) const
	{
		NestAssert(!entityRef.expired());
		auto entity = entityRef.lock();
		auto transform = entity->GetComponent<Transform>();

		NestAssert(transform != nullptr);
		transform->SetLocalPosition(position);
	}



	void NinePatchGraphic::RefreshAllPositions() const
	{
		const Vector2f halfSize = m_BodySize * 0.5f;

		RefreshEntityPosition(m_TopLeft, Vector3f(-halfSize.x - m_TopLeftSize.x * 0.5f, halfSize.y + m_TopLeftSize.y * 0.5f, 0.0f));
		RefreshEntityPosition(m_Top, Vector3f(0.0f, halfSize.y + m_TopWidth * 0.5f, 0.0f));
		RefreshEntityPosition(m_TopRight, Vector3f(halfSize.x + m_TopRightSize.x * 0.5f, halfSize.y + m_TopRightSize.y * 0.5f, 0.0f));
		RefreshEntityPosition(m_Left, Vector3f(-halfSize.x - m_LeftWidth * 0.5f, 0.0f, 0.0f));
		RefreshEntityPosition(m_Middle, Vector3f(0.0f, 0.0f, 0.0f));
		RefreshEntityPosition(m_Right, Vector3f(halfSize.x + m_RightWidth * 0.5f, 0.0f, 0.0f));
		RefreshEntityPosition(m_BottomLeft, Vector3f(-halfSize.x - m_BottomLeftSize.x * 0.5f, -halfSize.y - m_BottomLeftSize.y * 0.5f, 0.0f));
		RefreshEntityPosition(m_Bottom, Vector3f(0.0f, -halfSize.y - m_BottomWidth * 0.5f, 0.0f));
		RefreshEntityPosition(m_BottomRight, Vector3f(halfSize.x + m_BottomRightSize.x * 0.5f, -halfSize.y - m_BottomRightSize.y * 0.5f, 0.0f));
	}



	void NinePatchGraphic::RefreshAllMaterials() const
	{
		UpdateEntityMaterial(m_TopLeft, m_MatTopLeft);
		UpdateEntityMaterial(m_Top, m_MatTop);
		UpdateEntityMaterial(m_TopRight, m_MatTopRight);
		UpdateEntityMaterial(m_Left, m_MatLeft);
		UpdateEntityMaterial(m_Middle, m_MatMiddle);
		UpdateEntityMaterial(m_Right, m_MatRight);
		UpdateEntityMaterial(m_BottomLeft, m_MatBottomLeft);
		UpdateEntityMaterial(m_Bottom, m_MatBottom);
		UpdateEntityMaterial(m_BottomRight, m_MatBottomRight);
	}



	void NinePatchGraphic::RefreshAllMeshes() const
	{
		UpdateEntityMeshScale(m_TopLeft, m_TopLeftSize);
		UpdateEntityMeshScale(m_Top, Vector2f(m_BodySize.x, m_TopWidth));
		UpdateEntityMeshScale(m_TopRight, m_TopRightSize);
		UpdateEntityMeshScale(m_Left, Vector2f(m_LeftWidth, m_BodySize.y));
		UpdateEntityMeshScale(m_Middle, m_BodySize);
		UpdateEntityMeshScale(m_Right, Vector2f(m_RightWidth, m_BodySize.y));
		UpdateEntityMeshScale(m_BottomLeft, m_BottomLeftSize);
		UpdateEntityMeshScale(m_Bottom, Vector2f(m_BodySize.x, m_BottomWidth));
		UpdateEntityMeshScale(m_BottomRight, m_BottomRightSize);
	}
}
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Input.h"

#include"Nest/Exceptions/RuntimeException.h"



namespace Nest
{
	Input::Input()
	{
		ClearAllKeyboardInput();
		ClearAllMouseInput();
	}



	void Input::KeyPressed(KeyCode keyCode)
	{
		const int code = KeyCodeToInt(keyCode);
		m_KeysOnDown[code] = true;
		m_KeysPressed[code] = true;
	}



	void Input::KeyReleased(KeyCode keyCode)
	{
		const int code = KeyCodeToInt(keyCode);
		m_KeysOnUp[code] = true;
		m_KeysPressed[code] = false;
	}



	void Input::SetMousePosition(int mouseX, int mouseY)
	{
		InputEvent inputEvent;
		inputEvent.type = InputEventType::MouseMovement;
		inputEvent.deltaX = mouseX - m_MousePosition.x;
		inputEvent.deltaY = mouseY - m_MousePosition.y;

		m_MousePosition.Set(mouseX, mouseY);

		try
		{
			m_InputEventsBus.Signal(inputEvent);
		}
		catch (...)
		{
			std::throw_with_nested(RuntimeExceptionWithLine("Unhandled exception while dispatching Mouse Movement event"));
		}
	}



	void Input::SetMousePosition(Vector2<int> position)
	{
		SetMousePosition(position.x, position.y);
	}



	void Input::AddMouseScroll(int deltaX, int deltaY)
	{
		m_MouseScroll.x += deltaX;
		m_MouseScroll.y += deltaY;

		InputEvent inputEvent;
		inputEvent.type = InputEventType::MouseScroll;
		inputEvent.deltaX = deltaX;
		inputEvent.deltaY = deltaY;

		try
		{
			m_InputEventsBus.Signal(inputEvent);
		}
		catch (...)
		{
			std::throw_with_nested(RuntimeExceptionWithLine("Unhandled exception while dispatching Mouse Scroll event"));
		}
	}



	void Input::AddMouseScroll(Vector2<int> delta)
	{
		AddMouseScroll(delta.x, delta.y);
	}



	void Input::MouseButtonPressed(MouseButton button)
	{
		const auto code = MouseButtonToInt(button);
		m_MouseButtonsOnDown[code] = true;
		m_MouseButtonsPressed[code] = true;

		InputEvent inputEvent;
		inputEvent.type = InputEventType::MouseButtonDown;
		inputEvent.mouseButton = button;

		try
		{
			m_InputEventsBus.Signal(inputEvent);
		}
		catch (...)
		{
			std::throw_with_nested(RuntimeExceptionWithLine("Unhandled exception while dispatching Mouse Button Pressed event"));
		}
	}



	void Input::MouseButtonReleased(MouseButton button)
	{
		const auto code = MouseButtonToInt(button);
		m_MouseButtonsOnUp[code] = true;
		m_MouseButtonsPressed[code] = false;

		InputEvent inputEvent;
		inputEvent.type = InputEventType::MouseButtonUp;
		inputEvent.mouseButton = button;

		try
		{
			m_InputEventsBus.Signal(inputEvent);
		}
		catch (...)
		{
			std::throw_with_nested(RuntimeExceptionWithLine("Unhandled exception while dispatching Mouse Button Released event"));
		}
	}



	bool Input::IsKeyOnDown(KeyCode keyCode) const
	{
		return m_KeysOnDown[KeyCodeToInt(keyCode)];
	}



	bool Input::IsKeyPressed(KeyCode keyCode) const
	{
		return m_KeysPressed[KeyCodeToInt(keyCode)];
	}



	bool Input::IsKeyOnUp(KeyCode keyCode) const
	{
		return m_KeysOnUp[KeyCodeToInt(keyCode)];
	}



	Vector2<int> Input::GetMousePosition() const
	{
		return m_MousePosition;
	}



	Vector2<int> Input::GetDeltaMouseScrollSinceLastRefresh() const
	{
		return m_MouseScroll;
	}



	bool Input::IsMouseOnDown(MouseButton button) const
	{
		return m_MouseButtonsOnDown[MouseButtonToInt(button)];
	}



	bool Input::IsMousePressed(MouseButton button) const
	{
		return m_MouseButtonsPressed[MouseButtonToInt(button)];
	}



	bool Input::IsMouseOnUp(MouseButton button) const
	{
		return m_MouseButtonsOnUp[MouseButtonToInt(button)];
	}



	void Input::SetStartTextInputMethod(std::function<void()> startTextInputFunction)
	{
		m_StartTextInputFunction = startTextInputFunction;
	}



	void Input::SetStopTextInputMethod(std::function<void()> stopTextInputFunction)
	{
		m_StopTextInputFunction = stopTextInputFunction;
	}



	void Input::StartTextInput(std::shared_ptr<std::string> destinationString)
	{
		if (m_StartTextInputFunction != nullptr && destinationString != nullptr)
		{
			m_StartTextInputFunction();
			m_TextInputString = destinationString;
		}
	}



	std::weak_ptr<std::string> Input::GetWorkingTextInputString() const
	{
		if (m_TextInputString != nullptr)
		{
			return std::weak_ptr<std::string>(m_TextInputString);
		}
		else
		{
			return std::weak_ptr<std::string>();
		}
	}



	bool Input::IsOnTextInput() const
	{
		return m_TextInputString != nullptr;
	}



	void Input::StopTextInput()
	{
		if (IsOnTextInput())
		{
			m_TextInputString = nullptr;
			if (m_StopTextInputFunction != nullptr)
			{
				m_StopTextInputFunction();
			}
		}
	}



	void Input::TextInputAppend(const char* character) const
	{
		if (IsOnTextInput())
		{
			*m_TextInputString += character;
		}
	}



	void Input::TextInputBackspace() const
	{
		if (IsOnTextInput())
		{
			// The loop is needed, since we expect UTF8 encoding
			while (m_TextInputString->length() > 0)
			{
				unsigned char* c = reinterpret_cast<unsigned char*>(&m_TextInputString->at(m_TextInputString->length() - 1));
				m_TextInputString->pop_back();

				if (*c >> 7 == 0x00 || *c >> 6 == 0x03)
					break;
			}
		}
	}



	void Input::Refresh()
	{
		for (int i = 0; i < KeyCodeToInt(KeyCode::KeyCodeSize); ++i)
		{
			m_KeysOnDown[i] = false;
			m_KeysOnUp[i] = false;
		}

		for (int i = 0; i < MouseButtonToInt(MouseButton::MouseButtonSize); ++i)
		{
			m_MouseButtonsOnDown[i] = false;
			m_MouseButtonsOnUp[i] = false;
		}

		m_MouseScroll.x = 0;
		m_MouseScroll.y = 0;
	}



	EventsBus<InputEvent&>& Input::GetInputEventsBus()
	{
		return m_InputEventsBus;
	}



	void Input::ClearAllKeyboardInput()
	{
		for (int i = 0; i < KeyCodeToInt(KeyCode::KeyCodeSize); ++i)
		{
			// TODO : change to a Enum iterator and ranged-for? How to handle "missing" enum integers?
			m_KeysOnDown[i] = false;
			m_KeysPressed[i] = false;
			m_KeysOnUp[i] = false;
		}
	}



	void Input::ClearAllMouseInput()
	{
		for (int i = 0; i < MouseButtonToInt(MouseButton::MouseButtonSize); ++i)
		{
			// TODO : change to a Enum iterator and ranged-for? How to handle "missing" enum integers?
			m_MouseButtonsOnDown[i] = false;
			m_MouseButtonsPressed[i] = false;
			m_MouseButtonsOnUp[i] = false;
		}
	}



	Input::~Input()
	{
	}
}

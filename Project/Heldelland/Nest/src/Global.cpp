
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Global.h"

#include"Nest/IComponentsFactory.h"
#include"Nest/Input.h"
#include"Nest/Engine.h"



namespace Nest
{
	GlobalAccess::GlobalAccess() :
		m_NestEngine(nullptr)
	{
		// Nothing here
	};



	NestEngine* GlobalAccess::GetEngine() const
	{
		return(m_NestEngine);
	}



	auto GlobalAccess::GetInput() const -> Input*
	{
		return(m_Input.get());
	}



	auto GlobalAccess::GetMetadataManager() const -> MetadataManager*
	{
		return(m_MetadataManager.get());
	}



	//auto GlobalAccess::GetPrefabComponentsFactory() const -> IComponentsFactory*
	//{
	//    return(m_PrefabComponentsFactory.get());
	//}



	//World* GlobalAccess::GetWorld() const
	//{
	//    return(m_World.get());
	//}



	void GlobalAccess::SetEngine(NestEngine* nestEngine)
	{
		m_NestEngine = nestEngine;
	}



	void GlobalAccess::SetInput(std::unique_ptr<Input> input)
	{
		m_Input.swap(input);
	}



	void GlobalAccess::SetMetadataManager(std::unique_ptr<MetadataManager> metadataManager)
	{
		m_MetadataManager.swap(metadataManager);
	}



	//void GlobalAccess::SetPrefabComponentsFactory(std::unique_ptr<IComponentsFactory> prefabComponentsFactory)
	//{
	//    m_PrefabComponentsFactory.swap(prefabComponentsFactory);
	//}



	//void GlobalAccess::SetWorld(std::unique_ptr<World> world)
	//{
	//    m_World.swap(world);
	//}



	GlobalAccess::~GlobalAccess() {}



	GlobalAccess* Global = nullptr;
}

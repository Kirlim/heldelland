
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/EntitiesManager.h"

#include<algorithm>

#include"Nest/IComponentsFactory.h"
#include"Nest/IEntity.h"
#include"Nest/Entity.h"
#include"Nest/World.h"



namespace Nest
{
	EntitiesManager::EntitiesManager(const World& world) :
		m_World(world)
	{
	}



	auto EntitiesManager::Instantiate() -> std::weak_ptr<IEntity>
	{
		static EntityId::underlying_type nextId = 1;

		auto entity = std::make_shared<Entity>(this);
		entity->SetId(EntityId{ nextId++ });

		EntityInformation entityInformation;
		entityInformation.id = entity->Id();
		entityInformation.entity = entity;

		m_Entities.push_back(entityInformation);
		return(std::weak_ptr<IEntity>(m_Entities[m_Entities.size() - 1].entity));
	}



	void EntitiesManager::Destroy(std::weak_ptr<IEntity> entity)
	{
		if (auto entityInstance = entity.lock())
		{
			// Find the entity in the storing structure
			const auto iterator = std::find_if(
				m_Entities.begin(),
				m_Entities.end(),
				[entityInstance](const EntityInformation& information) { return(information.entity->Id() == entityInstance->Id()); }
			);

			if (iterator == m_Entities.end())
			{
				throw std::logic_error("Entity to be destroyed has not been found");
			}

			entityInstance->Destroy();
			m_Entities.erase(iterator);
		}
		else
		{
			throw std::logic_error("Entity pointer passed by parameter is null");
		}
	}



	void EntitiesManager::Destroy(EntityId entityId)
	{
		// Find the entity in the storing structure
		auto iterator = std::find_if(
			m_Entities.begin(),
			m_Entities.end(),
			[entityId](const EntityInformation& information) { return(information.entity->Id() == entityId); }
		);

		if (iterator == m_Entities.end())
		{
			throw std::logic_error("Entity to be destroyed has not been found");
		}

		iterator->entity->Destroy();
		m_Entities.erase(iterator);
	}



	std::weak_ptr<IEntity> EntitiesManager::GetById(EntityId entityId) const
	{
		for (auto& entityInformation: m_Entities)
		{
			if (entityInformation.id == entityId)
			{
				return(std::weak_ptr<IEntity>(entityInformation.entity));
			}
		}

		return std::weak_ptr<IEntity>();
	}



	const World* EntitiesManager::GetWorld() const
	{
		return &m_World;
	}



	IComponentsFactory* EntitiesManager::GetComponentsFactory() const
	{
		return m_World.GetComponentsFactory();
	}



	IResourcesManager* EntitiesManager::GetResourcesManager() const
	{
		return m_World.GetResourcesManager();
	}



	void EntitiesManager::UnloadAllEntities()
	{
		while (m_Entities.size() > 0)
		{
			const auto index = m_Entities.size() - 1;
			m_Entities[index].entity->Destroy();
			m_Entities.pop_back();
		}
	}
}

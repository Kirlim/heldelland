//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/IResourcesManager.h"

#include"Nest/Font.h"
#include"Nest/Material.h"
#include"Nest/Mesh.h"
#include"Nest/Model.h"
#include"Nest/ShaderProgram.h"
#include"Nest/Texture.h"



namespace Nest
{
	void IResourcesManager::AddFont(std::string resourceName, std::unique_ptr<Font>& font)
	{
		return DoAddFont(std::move(resourceName), font);
	}



	void IResourcesManager::UnloadFont(std::string resourceName)
	{
		DoUnloadFont(std::move(resourceName));
	}



	void IResourcesManager::AddMaterial(std::string resourceName, std::unique_ptr<Material>& material)
	{
		return DoAddMaterial(std::move(resourceName), material);
	}



	void IResourcesManager::UnloadMaterial(std::string resourceName)
	{
		DoUnloadMaterial(std::move(resourceName));
	}



	void IResourcesManager::AddMesh(std::string resourceName, std::unique_ptr<Mesh>& mesh)
	{
		return DoAddMesh(std::move(resourceName), mesh);
	}



	void IResourcesManager::UnloadMesh(std::string resourceName)
	{
		DoUnloadMesh(std::move(resourceName));
	}



	void IResourcesManager::AddModel(std::string resourceName, std::unique_ptr<Model>& model)
	{
		DoAddModel(std::move(resourceName), model);
	}



	void IResourcesManager::UnloadModel(std::string resourceName)
	{
		DoUnloadModel(std::move(resourceName));
	}



	void IResourcesManager::LoadShaderProgram(std::string vertexShaderFile, std::string fragmentShaderFile, std::string resourceName)
	{
		return DoLoadShaderProgram(std::move(vertexShaderFile), std::move(fragmentShaderFile), std::move(resourceName));
	}



	void IResourcesManager::LoadShaderProgramContents(std::string vertexShaderContents, std::string fragmentShaderContents, std::string resourceName)
	{
		return DoLoadShaderProgramContents(std::move(vertexShaderContents), std::move(fragmentShaderContents), std::move(resourceName));
	}



	void IResourcesManager::AddShaderProgram(std::string resourceName, std::unique_ptr<ShaderProgram>& shaderProgram)
	{
		return DoAddShaderProgram(std::move(resourceName), shaderProgram);
	}



	void IResourcesManager::UnloadShaderProgram(std::string resourceName)
	{
		DoUnloadShaderProgram(std::move(resourceName));
	}



	void IResourcesManager::AddTexture(std::string resourceName, std::unique_ptr<Texture>& texture)
	{
		return DoAddTexture(std::move(resourceName), texture);
	}



	void IResourcesManager::UnloadTexture(std::string resourceName)
	{
		DoUnloadTexture(std::move(resourceName));
	}



	Font* IResourcesManager::FetchFont(std::string resourceName) const
	{
		return DoFetchFont(std::move(resourceName));
	}



	Material* IResourcesManager::FetchMaterial(std::string resourceName) const
	{
		return DoFetchMaterial(std::move(resourceName));
	}



	Mesh* IResourcesManager::FetchMesh(std::string resourceName) const
	{
		return DoFetchMesh(std::move(resourceName));
	}



	Model* IResourcesManager::FetchModel(std::string resourceName) const
	{
		return DoFetchModel(std::move(resourceName));
	}



	ShaderProgram* IResourcesManager::FetchShaderProgram(std::string resourceName) const
	{
		return DoFetchShaderProgram(std::move(resourceName));
	}



	Texture* IResourcesManager::FetchTexture(std::string resourceName) const
	{
		return DoFetchTexture(std::move(resourceName));
	}



	IResourcesManager::~IResourcesManager() = default;
}

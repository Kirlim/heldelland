//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Font.h"

#include"Nest/Exceptions/InvalidOperationException.h"

#include"Nest/IResourcesManager.h"
#include"Nest/World.h"

#include<Nest/Log.h>



namespace Nest
{
	Font::Font(IResourcesManager& resourcesManager) :
		m_ResourcesManager(resourcesManager),
		m_IsLoaded(false)
	{
	}



	Font::~Font()
	{
		UnloadFont();
	}



	void Font::LoadFont(std::string fontName, std::string ttfFile)
	{
		if (m_IsLoaded)
			ThrowInvalidOperationExceptionWithLine("Font is already loaded with data.");


		m_FontName = std::move(fontName);

		FT_Library ft;
		if (FT_Init_FreeType(&ft) != 0)
		{
			Log::Error("Failed to init Freetype library.");
			return;
		}

		// Load font as a face
		FT_Face face;
		if (FT_New_Face(ft, ttfFile.c_str(), 0, &face) != 0)
		{
			Log::Error("Failed to load font with Freetype.");
			return;
		}

		// Load the font

		// Basic ASCII Characters
		FT_Set_Pixel_Sizes(face, 0, 48);

		for (unsigned char c = 0; c < 128; ++c)
		{
			LoadCharacter(c, face);
		}

		// Some of the latin1-supplement characters
		for (unsigned int c = 161; c < 383; ++c)
		{
			LoadCharacter(c, face);
		}


		m_IsLoaded = true;

		FT_Done_Face(face);
		FT_Done_FreeType(ft);
	}



	void Font::UnloadFont()
	{
		if (!m_IsLoaded) return;

		for (auto& c : m_Characters)
		{
			const auto resourceName = MakeTextureName(c.first);
			m_ResourcesManager.UnloadTexture(resourceName);
		}

		m_Characters.clear();
		m_FontName = "";
	}



	bool Font::GetCharacterInfo(unsigned int character, Vector2<int>& outSize, Vector2<int>& outBearing, int& outAdvance) const
	{
		if (m_Characters.count(character) > 0)
		{
			auto& c = m_Characters.at(character);
			NestAssert(c != nullptr);

			outSize = c->size;
			outBearing = c->bearing;
			outAdvance = c->advance;

			return (true);
		}
		else
		{
			return (false);
		}
	}



	bool Font::IsLoaded() const
	{
		return (m_IsLoaded);
	}



	std::string Font::MakeTextureName(unsigned int character) const
	{
		const std::string charNumber = std::to_string(character);
		std::string name = m_FontName + "[" + charNumber + "]";
		return (name);
	}



	void Font::LoadCharacter(unsigned int character, FT_Face& face)
	{
		// Load character glyph
		if (FT_Load_Char(face, character, FT_LOAD_RENDER))
		{
			const std::string errorMessage =
				"Failed to load glyph [" + std::to_string(character) + "] of font [" + m_FontName + "]";

			Log::Error(errorMessage);

			return;
		}

		// Load the texture
		std::unique_ptr<Texture> charTexture = std::make_unique<Texture>();
		charTexture->Load(
			face->glyph->bitmap.buffer, face->glyph->bitmap.width, face->glyph->bitmap.rows,
			TextureFiltering::Bilinear, TextureFiltering::Bilinear, TextureWrapMode::Clamp, TextureFormat::Red
		);

		// Register the texture in the resources manager
		m_ResourcesManager.AddTexture(MakeTextureName(character), charTexture);

		// Create and store the character information
		auto charInfo = std::make_unique<CharacterInfo>();
		charInfo->advance = face->glyph->advance.x;
		charInfo->size = Vector2<int>(face->glyph->bitmap.width, face->glyph->bitmap.rows);
		charInfo->bearing = Vector2<int>(face->glyph->bitmap_left, face->glyph->bitmap_top);
		charInfo->texture = m_ResourcesManager.FetchTexture(MakeTextureName(character));

		m_Characters[character] = std::move(charInfo);
	}



	Texture* Font::GetCharacterTexture(unsigned int character) const
	{
		Texture* result = nullptr;

		if (m_Characters.count(character) > 0)
		{
			result = m_Characters.at(character)->texture;
		}

		return (result);
	}
}

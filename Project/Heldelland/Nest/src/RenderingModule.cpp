
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/RenderingModule.h"

#include"SDL_video.h"
#include"SDL_syswm.h"
#include<GL/glew.h>
#include"SDL_opengl.h"
#include<GL/glu.h>

#include"Nest/VulkanWrapper.h"
#include"Nest/Log.h"



namespace Nest
{
    RenderingModule::RenderingModule() :
        m_GameRenderer(GameRenderer::NotSet),
        m_SdlWindow(NULL),
        m_ScreenWidth(0),
        m_ScreenHeight(0)
    {
        // Nothing here
    }



    RenderingModule::~RenderingModule()
    {
        switch (m_GameRenderer)
        {
        case GameRenderer::OpenGL:
            OpenGlUnload();
            break;

        case GameRenderer::Vulkan:
            VulkanUnload();
            break;

        default:
            break;
        }


        if (m_SdlWindow != NULL)
        {
            SDL_DestroyWindow(m_SdlWindow);
        }

        // TODO : quit sdl only if possible
        SDL_Quit();
    }



    void RenderingModule::Initialize(GameRenderer gameRenderer, int screenWidth, int screenHeight)
    {
        m_GameRenderer = gameRenderer;
        m_ScreenWidth = screenWidth;
        m_ScreenHeight = screenHeight;

        bool success = true;

        if (SdlInit())
        {
            switch (m_GameRenderer)
            {
            case GameRenderer::OpenGL:
                success = OpenGlInit(screenWidth, screenHeight);
                break;

            case GameRenderer::Vulkan:
                success = VulkanInit(screenWidth, screenHeight);
                break;

            default:
                Log::Error("Failed to initialize renderer: unknown rendering type.");
                success = false;
                break;
            }
        }


        if (!success)
        {
            Log::Error("Renderer has not been initialized.");
        }
    }



    int RenderingModule::GetScreenWidth() const
    {
        return(m_ScreenWidth);
    }



    int RenderingModule::GetScreenHeight() const
    {
        return(m_ScreenHeight);
    }



    GameRenderer RenderingModule::GetGameRenderer() const
    {
        return(m_GameRenderer);
    }



    SDL_Window* RenderingModule::GetSDLWindow() const
    {
        return(m_SdlWindow);
    }



    bool RenderingModule::SdlInit()
    {
        // TODO : error if SDL already initialized
        bool success = true;

        if (SDL_Init(SDL_INIT_VIDEO) < 0)
        {
            Log::Error("Failed to initialize SDL Video. SDL Error:\n" + std::string(SDL_GetError()) + "\n");
            success = false;
        }

        return(success);
    }



    bool RenderingModule::OpenGlInit(int screenWidth, int screenHeight)
    {
        bool success = true;

        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

        m_SdlWindow = SDL_CreateWindow(
            "Unnamed",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            screenWidth, screenHeight,
            SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN
            );

        if (m_SdlWindow == NULL)
        {
            Log::Error("Failed to create SDL Window. SDL Error:\n" + std::string(SDL_GetError()) + "\n");
            success = false;
        }
        else
        {
            auto glContext = SDL_GL_CreateContext(m_SdlWindow);
            if (glContext == NULL)
            {
                Log::Error("Failed to creat GL Context. SDL Error:\n" + std::string(SDL_GetError()) + "\n");
                success = false;
            }
            else
            {
                // Initialize GLEW
                glewExperimental = GL_TRUE;
                GLenum glewError = glewInit();

                if (glewError != GLEW_OK)
                {
                    Log::Error("Error initializing GLEW: " + std::string((char*)glewGetErrorString(glewError)) + "\n");
                    success = false;
                }

                // Use vsync
                if (SDL_GL_SetSwapInterval(1) < 0)
                {
                    Log::Warning("Warning: unable to set vsync. SDL Error:\n" + std::string(SDL_GetError()) + "\n");
                }
            }
        }

        return(success);
    }



    bool RenderingModule::VulkanInit(int screenWidth, int screenHeight)
    {
        //SDL_SysWMinfo info;
        //SDL_VERSION(&info.version);
        //SDL_GetWindowWMInfo(m_SdlWindow, &info);
        //TCHAR className[256];
        //GetClassName(info.info.win.window, className, 256);
        //WNDCLASS wce;
        //GetClassInfo(GetModuleHandle(NULL), className, &wce);
//
        //win32_LoadVulkan();
        //InitVulkan(screenWidth = screenWidth, screenHeight = screenHeight, info.info.win.window, wce.hInstance);
//
        Log::Error("Vulkan initialization not implemented yet.");
        return(false);
    }



    void RenderingModule::OpenGlUnload()
    {
        // Nothing here
    }



    void RenderingModule::VulkanUnload()
    {
        // UnloadVulkan();
    }
}

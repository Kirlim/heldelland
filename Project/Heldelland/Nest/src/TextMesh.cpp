
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/TextMesh.h"

#include<stdexcept>

#include"Nest/TextMeshImpl.h"
#include"Nest/TextMeshImplOpenGL.h"

#include"Nest/RenderingModule.h"
#include"Nest/Engine.h"
#include"Nest/Global.h"



namespace Nest
{
    TextMesh::TextMesh() : Component(),
        m_Font(nullptr),
        m_FontSize(1.0f),
        m_Text(),
        m_Mesh()
    {
        // Nothing here
    }



    TextMesh::~TextMesh()
    {
        // Nothing here
    }



    void TextMesh::SetFont(Font* font)
    {
        m_Font = font;
    }



    Font* TextMesh::GetFont() const
    {
        return(m_Font);
    }



    void TextMesh::SetFontSize(const float size)
    {
        m_FontSize = size;
    }



    float TextMesh::GetFontSize() const
    {
        return(m_FontSize);
    }



    void TextMesh::SetText(const std::string& text)
    {
        if (m_Mesh == nullptr)
        {
            CreateMesh();
            if (m_Mesh == nullptr)
            {
                throw std::runtime_error("Failed to create mesh.");
            }
        }

        m_Text = text;
    }



    std::string TextMesh::GetText() const
    {
        return(m_Text);
    }



    void TextMesh::Render()
    {
        if (m_Mesh == nullptr || m_Text.length() == 0) return;

        m_Mesh->RenderText(m_Text, m_Font, m_FontSize);
    }



    void TextMesh::OnDestroy()
    {
        // Nothing here
    }



    void TextMesh::CreateMesh()
    {
        if (Global->GetEngine()->GetRenderingModule()->GetGameRenderer() == GameRenderer::OpenGL)
        {
            m_Mesh.reset(new TextMeshImplOpenGL());
        }
        else
        {
            // TODO : not implemented exception
            throw - 1;
        }
    }
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Entity.h"

#include<stack>
#include<algorithm>

#include"Nest/NestAssert.h"
#include"Nest/Component.h"
#include"Nest/ComponentOwnerReferenceBase.h"
#include"Nest/IComponentsFactory.h"
#include"Nest/EntitiesManager.h"
#include"Nest/EntitiesAndComponentsRemap.h"
#include"Nest/Transform.h"



namespace Nest
{
	Entity::Entity(EntitiesManager* ownerEntitiesManager) :
		m_OwnerEntitiesManager(ownerEntitiesManager)
		
	{
		// Nothing here
	}



	int Entity::GetComponentsCount() const
	{
		return int(m_Components.size());
	}



	std::unique_ptr<ComponentReferenceBase> Entity::GetComponentAt(int index) const
	{
		NestAssert(index >= 0 && index < GetComponentsCount());
		auto reference = std::make_unique<ComponentReference<Component>>(*m_Components[index].reference);
		return std::move(reference);
	}



	ComponentTypeId Entity::GetComponentTypeIdAt(int index) const
	{
		NestAssert(index >= 0 && index < GetComponentsCount());
		return m_Components[index].typeId;
	}



	void Entity::SetName(std::string name)
	{
		m_Name = name;
	}



	std::string Entity::GetName() const
	{
		return m_Name;
	}



	std::weak_ptr<IEntity> Entity::Clone(EntitiesManager& owningManager)
	{
		std::weak_ptr<IEntity> rootClonedEntity;

		EntitiesAndComponentsRemap remap{};
		std::stack<IEntity*> entities;
		std::vector<ComponentReference<Component>> sourceComponents;
		std::vector<ComponentReference<Component>> clonedComponents;

		entities.push(static_cast<IEntity*>(this));


		while (!entities.empty())
		{
			auto entity = DoCast<Entity>(entities.top());
			entities.pop();

			auto newEntity = owningManager.Instantiate().lock();
			if (rootClonedEntity.expired()) rootClonedEntity = newEntity;

			for (int i = 0; i < entity->GetComponentsCount(); ++i)
			{
				const auto componentType = entity->GetComponentTypeIdAt(i);
				auto sourceComponent = entity->GetComponentById<Component>(entity->m_Components[i].reference->Get()->GetId());

				auto newComponent = entity->DoInstantiateComponent(componentType);
				const auto clonedComponent = newEntity->AddComponent(newComponent);
				auto castedClonedComponent = newEntity->GetComponentById<Component>(clonedComponent->Get()->GetId());

				sourceComponents.push_back(sourceComponent);
				clonedComponents.push_back(castedClonedComponent);

				remap.RegisterComponent(sourceComponent->GetId());
				remap.MapComponent(sourceComponent->GetId(), castedClonedComponent);

				const auto componentAsTransform = dynamic_cast<Transform*>(sourceComponent.Get());
				if (componentAsTransform != nullptr)
				{
					for (int j = 0; j < componentAsTransform->GetChildCount(); ++j)
					{
						const auto child = componentAsTransform->GetChildAt(j);
						entities.push(child->GetOwner());
					}
				}
			}
		}


		NestAssert(sourceComponents.size() == clonedComponents.size());
		for (unsigned int i = 0; i < sourceComponents.size(); ++i)
		{
			clonedComponents[i]->CopyFrom(*sourceComponents[i].Get(), &remap);
		}

		NestAssert(!rootClonedEntity.expired());
		return rootClonedEntity;
	}



	EntitiesManager* Entity::GetEntitiesManager() const
	{
		return m_OwnerEntitiesManager;
	}



	std::unique_ptr<ComponentOwnerReferenceBase> Entity::DoInstantiateComponent(ComponentTypeId typeId) const
	{
		return m_OwnerEntitiesManager->GetComponentsFactory()->Instantiate(typeId);
	}



	std::unique_ptr<ComponentOwnerReferenceBase> Entity::DoInstantiateComponent(std::string typeName) const
	{
		return m_OwnerEntitiesManager->GetComponentsFactory()->Instantiate(typeName);
	}



	void Entity::DoAddComponent(std::unique_ptr<ComponentOwnerReferenceBase>& componentReference, ComponentTypeId componentTypeId)
	{
		NestAssert(componentReference != nullptr);
		m_Components.push_back(ComponentInfo{ std::move(componentReference), componentTypeId });
	}



	auto Entity::DoGetComponent(ComponentTypeId componentTypeId) const -> ComponentOwnerReferenceBase*
	
	{
		ComponentOwnerReferenceBase* result = nullptr;

		for (auto& componentInfo : m_Components)
		{
			if (componentInfo.typeId == componentTypeId)
			{
				result = componentInfo.reference.get();
				break;
			}
		}

		return (result);
	}



	auto Entity::DoGetComponent(std::string componentTypeName) const -> ComponentOwnerReferenceBase*
	
	{
		const auto componentsFactory = m_OwnerEntitiesManager->GetComponentsFactory();
		NestAssert(componentsFactory != nullptr);

		const auto typeId = componentsFactory->GetFactoryTypeId(componentTypeName);
		return (DoGetComponent(typeId));
	}



	auto Entity::DoGetComponentByName(std::string componentName) const -> ComponentOwnerReferenceBase*
	
	{
		ComponentOwnerReferenceBase* result = nullptr;

		for (auto& componentInfo : m_Components)
		{
			if (componentInfo.reference->Get()->GetName() == componentName)
			{
				result = componentInfo.reference.get();
				break;
			}
		}

		return (result);
	}



	auto Entity::DoGetComponentById(ComponentId componentId) const -> ComponentOwnerReferenceBase*
	
	{
		ComponentOwnerReferenceBase* result = nullptr;

		for (auto& componentInfo : m_Components)
		{
			if (componentInfo.reference->Get()->GetId() == componentId)
			{
				result = componentInfo.reference.get();
				break;
			}
		}

		return (result);
	}



	void Entity::DoRemoveComponent(ComponentTypeId componentTypeId)
	{
		const auto it = std::find_if(
			m_Components.begin(),
			m_Components.end(),
			[componentTypeId](const ComponentInfo& c) { return (c.typeId == componentTypeId); }
		);

		if (it != m_Components.end())
		{
			m_Components.erase(it);
		}
	}



	void Entity::DoRemoveComponentByName(std::string componentName)
	{
		const auto it = std::find_if(
			m_Components.begin(),
			m_Components.end(),
			[componentName](const ComponentInfo& c) { return (c.reference->Get()->GetName() == componentName); }
		);

		if (it != m_Components.end())
		{
			m_Components.erase(it);
		}
	}



	EntityId Entity::DoGetId() const
	{
		return m_Id;
	}



	void Entity::DoSetId(EntityId id)
	{
		m_Id = id;
	}



	void Entity::DoDestroy()
	{
		while (!m_Components.empty())
		{
			RemoveComponent(m_Components.begin()->typeId);
		}
	}
}

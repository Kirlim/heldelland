//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/AssimpModelLoader.h"

#include<algorithm>

#include<assimp/Importer.hpp>
#include<assimp/scene.h>
#include<assimp/postprocess.h>

#include"Nest/Log.h"
#include"Nest/IResourcesManager.h"
#include"Nest/Mesh.h"
#include"Nest/Texture.h"
#include"Nest/Material.h"
#include"Nest/Vector3.h"
#include"Nest/Vector2.h"
#include"Nest/ModelNode.h"
#include"Nest/Model.h"
#include"Nest/RenderingModule.h"

#include"Nest/MeshesFactory.h"

#include"Nest/Exceptions/FailedToLoadException.h"



namespace Nest
{
	AssimpModelLoader::AssimpModelLoader(const MeshesFactory& meshesFactory) :
		m_MeshesFactory(meshesFactory)
	{
	}



	AssimpModelLoader::~AssimpModelLoader() = default;



	Model* AssimpModelLoader::Load(
		IResourcesManager& resourcesManager, GameRenderer gameRenderer,
		std::string filename, std::string modelName, unsigned int prependNamesFlags
	)
	{
		if (resourcesManager.FetchModel(modelName) != nullptr)
			ThrowFailedToLoadExceptionWithLine("[Assimp Mesh Loader] There is already a model with name " + modelName + " loaded.");


		std::string normalizedFilename = filename;
		std::replace(normalizedFilename.begin(), normalizedFilename.end(), '\\', '/');

		Assimp::Importer importer;
		const aiScene* scene = importer.ReadFile(normalizedFilename, aiProcess_Triangulate | aiProcess_FlipUVs);

		std::string textureFolderPath = normalizedFilename.substr(0, normalizedFilename.find_last_of('/'));
		textureFolderPath += '/';

		// TODO : create a variation with textureFolderPath (makes it easier to share textures). Disallow texture prepending for this situation.
		// TODO : create a variation with material folder as well, making it easier to share materials. (group by name, shader and texture?)
		// TODO : allow to use relative or absolute texture path (as in the model) In case of absolute, ignore texture folder

		if (scene == nullptr || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || scene->mRootNode == nullptr)
			ThrowFailedToLoadExceptionWithLine("[Assimp Mesh Loader] Failed to load model scene: " + std::string(importer.GetErrorString()));


		// Keep up loaded resources names, so we can unload them in case of exception
		std::vector<std::string> loadedTextures;
		std::vector<std::string> loadedMaterials;
		std::vector<std::string> loadedMeshes;

		try
		{
			// Building the model is building the nodes hierarchy
			auto rootModelNode = std::make_unique<Nest::ModelNode>();
			LoadNode(
				resourcesManager, gameRenderer,
				modelName, rootModelNode.get(), scene->mRootNode, scene,
				prependNamesFlags, textureFolderPath, /*ref*/ loadedTextures, /*ref*/ loadedMaterials, /*ref*/ loadedMeshes
			);

			auto model = IModelLoader::BuildModel(rootModelNode);
			const auto pointerToModel = model.get();
			resourcesManager.AddModel(modelName, model);

			return pointerToModel;
		}
		catch (std::exception&)
		{
			if (resourcesManager.FetchModel(modelName) != nullptr)
			{
				resourcesManager.UnloadModel(modelName);
			}

			for (auto& mesh : loadedMeshes)
			{
				if (resourcesManager.FetchMesh(mesh) != nullptr)
				{
					resourcesManager.UnloadMesh(mesh);
				}
			}

			for (auto& material : loadedMaterials)
			{
				if (resourcesManager.FetchMaterial(material) != nullptr)
				{
					resourcesManager.UnloadMaterial(material);
				}
			}

			for (auto& texture : loadedTextures)
			{
				if (resourcesManager.FetchTexture(texture) != nullptr)
				{
					resourcesManager.UnloadTexture(texture);
				}
			}

			throw;
		}
	}



	void AssimpModelLoader::LoadNode(
		IResourcesManager& resourcesManager, GameRenderer gameRenderer,
		std::string modelName, ModelNode* modelNode, const aiNode* node, const aiScene* scene, unsigned int prependNamesFlags,
		std::string textureFolderPath,
		std::vector<std::string>& refLoadedTextures, std::vector<std::string>& refLoadedMaterials, std::vector<std::string>& refLoadedMeshes
	) const
	{
		aiVector3D aiScaling;
		aiQuaterniont<float> aiRotation;
		aiVector3D aiPosition;
		node->mTransformation.Decompose(aiScaling, aiRotation, aiPosition);
		// TODO : test rotation importing


		for (unsigned int i = 0; i < node->mNumMeshes; ++i)
		{
			const auto meshIndex = node->mMeshes[i];
			const auto assimpMesh = scene->mMeshes[meshIndex];
			auto assimpMeshName = assimpMesh->mName;

			const auto meshName = MakeMeshName(modelName, assimpMeshName.C_Str(), ((prependNamesFlags & MESH_NAME) != 0));
			if (resourcesManager.FetchMesh(meshName) == nullptr)
			{
				const bool prependTextureName = ((prependNamesFlags & TEXTURE_NAME) != 0);
				const bool prependMaterialName = ((prependNamesFlags & MATERIAL_NAME) != 0);

				LoadMesh(
					resourcesManager, gameRenderer,
					modelName, modelNode, meshName, assimpMesh, scene,
					prependTextureName, prependMaterialName, textureFolderPath, /*ref*/ refLoadedTextures, /*ref*/ refLoadedMaterials, /*ref*/ refLoadedMeshes
				);

				modelNode->SetPosition(Vector3f(aiPosition.x, aiPosition.y, aiPosition.z));
				modelNode->SetScale(Vector3f(aiScaling.x, aiScaling.y, aiScaling.z));
			}
			else
			{
				Log::Warning("[Assimp Mesh Loader] Mesh with same name " + meshName + " is already loaded, so the mesh has been ignored.");
			}
		}

		for (unsigned int i = 0; i < node->mNumChildren; ++i)
		{
			auto childNode = std::make_unique<Nest::ModelNode>();
			LoadNode(
				resourcesManager, gameRenderer,
				modelName, childNode.get(), node->mChildren[i], scene,
				prependNamesFlags, textureFolderPath, /*ref*/ refLoadedTextures, /*ref*/ refLoadedMaterials, /*ref*/ refLoadedMeshes
			);
			modelNode->AddChild(childNode);
		}
	}



	void AssimpModelLoader::LoadTextures(
		IResourcesManager& resourcesManager, GameRenderer gameRenderer,
		std::string modelName, const aiMaterial* material, aiTextureType textureType, std::string textureFolder,
		std::vector<std::string>& refLoadedTextures, bool prependTextureName
	) const
	{
		for (unsigned int i = 0; i < material->GetTextureCount(textureType); ++i)
		{
			aiString aiTexturePath;
			material->GetTexture(textureType, i, &aiTexturePath);

			std::string texturePath(aiTexturePath.C_Str());
			// TODO : normalize \ to /
			while (!texturePath.empty() && (texturePath[0] == '/' || texturePath[0] == '\\'))
			{
				texturePath.erase(0);
			}

			const std::string textureName = MakeTextureName(modelName, texturePath, prependTextureName);

			auto textureResource = resourcesManager.FetchTexture(textureName);
			if (textureResource == nullptr)
			{
				const std::string textureLoadPath = textureFolder + texturePath;

				auto texture = std::make_unique<Texture>(gameRenderer);
				// TODO : get the filtering from material
				// TODO : flag to throw exception if texture is not registered, instead of trying to load IModelLoader::Abort_if_texture_not_found
				// TODO : flag to throw exception if tex not registered, and failed to load IModelLoader::TryToLoad (maybe we can use a single flag)
				// TODO : flag if texture is already loaded, but with different settings, to show a warning
				texture->Load(textureLoadPath, TextureFiltering::Bilinear, TextureFiltering::Bilinear, TextureWrapMode::Repeat, TextureFormat::RGB);
				refLoadedTextures.push_back(textureName);
				resourcesManager.AddTexture(textureName, texture);

				textureResource = resourcesManager.FetchTexture(textureName);
			}
		}
	}



	std::string AssimpModelLoader::LoadMaterial(
		IResourcesManager& resourcesManager,
		std::string modelName, const aiMaterial* material,
		std::vector<std::string>& refLoadedMaterials, bool prependTextureName, bool prependMaterialName
	) const
	{
		aiString loadedMaterialName;
		aiGetMaterialString(material, AI_MATKEY_NAME, &loadedMaterialName);

		auto materialName = MakeMaterialName(modelName, std::string(loadedMaterialName.C_Str()), prependMaterialName);

		const auto materialResource = resourcesManager.FetchMaterial(materialName);
		if (materialResource == nullptr)
		{
			// TODO : flag to throw exception if material is not registered, instead of trying to load IModelLoader::Abort_if_material_not_found
			// TODO : flag to throw exception if material not registered, and failed to load IModelLoader::TryToLoad (maybe we can use a single flag)
			// TODO : flag if material is already loaded, but with different settings, to show a warning

			auto newMaterial = std::make_unique<Material>();
			// TODO : Set material shader program

			const auto shader = resourcesManager.FetchShaderProgram("gSimpleDiffuse"); // TODO : dont use magic values
			if (shader == nullptr)
			{
				throw std::logic_error("[Assimp Mesh Loader] Shader gSimpleDiffuse could not be found.");
			}
			newMaterial->SetShaderProgram(shader);

			// TODO : use textureTypes other than DIFFUSE
			const auto textureCount = material->GetTextureCount(aiTextureType_DIFFUSE);
			if (textureCount > 0)
			{
				aiString texturePath;
				material->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath);

				const std::string textureName = MakeTextureName(modelName, texturePath.C_Str(), prependTextureName);
				const auto texture = resourcesManager.FetchTexture(textureName);

				if (texture == nullptr)
				{
					throw std::logic_error("[Assimp Mesh Loader] Texture resource " + textureName + " was not found.");
				}

				// TODO : textures other than main
				newMaterial->SetMainTexture(texture);

				refLoadedMaterials.push_back(materialName);
			}

			resourcesManager.AddMaterial(materialName, newMaterial);
		}

		return materialName;
	}



	void AssimpModelLoader::LoadMesh(
		IResourcesManager& resourcesManager, GameRenderer gameRenderer,
		std::string modelName, ModelNode* modelNode, std::string meshName, aiMesh* mesh, const aiScene* scene, bool prependTextureName, bool prependMaterialName,
		std::string textureFolder,
		std::vector<std::string>& refLoadedTextures, std::vector<std::string>& refLoadedMaterials, std::vector<std::string>& refLoadedMeshes
	) const
	{
		auto newLoadedMesh = m_MeshesFactory.InstantiateEmpty();

		std::vector<Vector3f> vertices;
		std::vector<Vector3f> normals;
		std::vector<Vector2f> uvs;
		std::vector<unsigned int> tris;

		// Vertices data
		for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
		{
			// Note : not using emplace because of error "cannot bind packed field" on g++
			vertices.emplace_back(Vector3f{ mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z });
			normals.emplace_back(Vector3f{ mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z });

			if (mesh->mTextureCoords[0] != nullptr)
			{
				uvs.emplace_back(Vector2f{ mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y });
			}
			else
			{
				uvs.emplace_back(Vector2f{ 0.0f, 0.0f });
			}
		}

		// Faces data
		for (unsigned int i = 0; i < mesh->mNumFaces; ++i)
		{
			const aiFace face = mesh->mFaces[i];

			for (unsigned int j = 0; j < face.mNumIndices; ++j)
			{
				tris.emplace_back(face.mIndices[j]);
			}
		}

		newLoadedMesh->Make(vertices, normals, uvs, tris);

		std::string materialName{};
		if (mesh->mMaterialIndex >= 0)
		{
			const auto material = scene->mMaterials[mesh->mMaterialIndex];

			LoadTextures(resourcesManager, gameRenderer, modelName, material, aiTextureType_DIFFUSE, std::move(textureFolder), /*ref*/ refLoadedTextures, prependTextureName);
			materialName = LoadMaterial(resourcesManager, modelName, material, /*ref*/ refLoadedMaterials, prependTextureName, prependMaterialName);
		}

		refLoadedMeshes.push_back(meshName);
		resourcesManager.AddMesh(meshName, newLoadedMesh);

		modelNode->RegisterMeshData(meshName, materialName);
	}



	std::string AssimpModelLoader::MakeMeshName(std::string modelName, std::string meshName, bool prependModelName) const
	{
		static unsigned int identifier = 0; // TODO : use a GUID instead
		// TODO : (UPDATED) decided to only use identifiers if the mesh has no names. Otherwise, exclusive names within the model file is to be followed.

		std::string name{};
		if (prependModelName) name += "[" + modelName + "]";
		name += meshName + "_" + std::to_string(identifier++);

		return name;
	}



	std::string AssimpModelLoader::MakeMaterialName(std::string modelName, std::string materialName, bool prependModelName) const
	{
		std::string name{};
		if (prependModelName) name += "[" + modelName + "]";
		name += materialName;

		return name;
	}



	std::string AssimpModelLoader::MakeTextureName(std::string modelName, std::string path, bool prependModelName) const
	{
		std::string correctedPath = path;
		std::replace(correctedPath.begin(), correctedPath.end(), '\\', '/');

		std::string textureName = correctedPath.substr(correctedPath.find_last_of('/') + 1, correctedPath.size());
		textureName = textureName.substr(0, textureName.find_last_of('.'));
		if (prependModelName) textureName = "[" + modelName + "]" + textureName;

		return textureName;
	}
}

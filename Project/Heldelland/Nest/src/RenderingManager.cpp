//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/RenderingManager.h"

#include"Nest/Exceptions/NullArgumentException.h"

#include"Nest/Component.h"



namespace Nest
{
	RenderingManager::RenderingManager() = default;



	RenderingManager::~RenderingManager() = default;



	void RenderingManager::SetActiveCamera(ComponentReference<Camera> cameraReference)
	{
		if (cameraReference == nullptr)
		{
			ThrowNullArgumentExceptionWithLine("cameraReference");
		}

		m_CameraReference = cameraReference;
	}



	ComponentReference<Camera> RenderingManager::GetActiveCamera() const
	{
		return (m_CameraReference);
	}



	void RenderingManager::SetActiveGuiCamera(ComponentReference<Camera> guiCameraReference)
	{
		if (guiCameraReference == nullptr)
		{
			ThrowNullArgumentExceptionWithLine("guiCameraReference");
		}

		m_GuiCameraReference = guiCameraReference;
	}



	ComponentReference<Camera> RenderingManager::GetActiveGuiCamera() const
	{
		return (m_GuiCameraReference);
	}
}

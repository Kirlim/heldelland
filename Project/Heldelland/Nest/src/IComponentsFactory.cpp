
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/IComponentsFactory.h"

#include"Nest/BasicComponentsFactory.h"
#include"Nest/ComponentOwnerReferenceBase.h"



namespace Nest
{
	IComponentsFactory::~IComponentsFactory()
	{
		// Nothnig here
	}



	void IComponentsFactory::RegisterFactory(const unsigned int factoryTypeId, const std::string typeName, std::unique_ptr<BasicComponentsFactory> factory)
	{
		if (factory == nullptr)
		{
			// TODO : throw assertion in debug builds
			throw - 1;
		}

		DoRegisterFactory(factoryTypeId, typeName, std::move(factory));
	}



	std::unique_ptr<ComponentOwnerReferenceBase> IComponentsFactory::Instantiate(const unsigned int factoryTypeId)
	{
		// TODO : way to validate if factory type id is valid
		return(DoInstantiate(factoryTypeId));
	}



	std::unique_ptr<ComponentOwnerReferenceBase> IComponentsFactory::Instantiate(const std::string typeName)
	{
		// TODO : way to validate if factory type id is valid
		return(DoInstantiate(typeName));
	}



	std::string IComponentsFactory::GetTypeName(const unsigned int factoryTypeId) const
	{
		return(DoGetTypeName(factoryTypeId));
	}



	unsigned int IComponentsFactory::GetFactoryTypeId(const std::string typeName) const
	{
		return(DoGetFactoryTypeId(typeName));
	}
}
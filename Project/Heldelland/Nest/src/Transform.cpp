//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Transform.h"

#include<algorithm>

#include"Nest/Cast.h"
#include"Nest/IEntity.h"
#include"Nest/EntitiesAndComponentsRemap.h"



namespace
{
	Nest::Matrix4<float> MakeTranslationMatrix(float x, float y, float z)
	{
		return Nest::Matrix4<float>{
			1.0f, 0.0f, 0.0f, x,
			0.0f, 1.0f, 0.0f, y,
			0.0f, 0.0f, 1.0f, z,
			0.0f, 0.0f, 0.0f, 1.0f
		};
	}
}



namespace Nest
{
	Transform::Transform() : Component(),
		m_LocalPosition(), m_LocalRotation(), m_LocalScale(float(1), float(1), float(1)), m_AccumulatedTransformMatrix(), m_IsDirty(false)
	{
		// Nothing here
	}



	void Transform::SetParent(ComponentReference<Transform> parent)
	{
		// TODO : keep world position and rotation
		auto thisReference = m_Owner->GetComponentReference(this);

		if (m_Parent != nullptr)
		{
			m_Parent->RemoveChild(thisReference);
		}

		m_Parent = parent;

		if (m_Parent != nullptr)
		{
			m_Parent->AddChild(thisReference);
		}

		SetDirty();
	}



	ComponentReference<Transform> Transform::GetParent()
	{
		return (m_Parent);
	}



	int Transform::GetChildCount() const
	{
		return (static_cast<int>(m_Childs.size()));
	}



	ComponentReference<Transform> Transform::GetChildAt(int childIndex) const
	{
		NestAssert(childIndex >= 0 && childIndex < static_cast<int>(m_Childs.size()));
		return (m_Childs[childIndex]);
	}



	void Transform::SetDirty()
	{
		m_IsDirty = true;

		for (auto& child : m_Childs)
		{
			child->SetDirty();
		}
	}



	void Transform::SetLocalPosition(Vector3<float> position)
	{
		m_LocalPosition = position;
		SetDirty();
	}



	void Transform::SetLocalRotation(Quaternion<float> rotation)
	{
		m_LocalRotation = rotation;
		SetDirty();
	}



	void Transform::SetLocalScale(Vector3<float> scale)
	{
		m_LocalScale = scale;
		SetDirty();
	}



	Vector3<float> Transform::GetLocalPosition()
	{
		return (m_LocalPosition);
	}



	Quaternion<float> Transform::GetLocalRotation()
	{
		return (m_LocalRotation);
	}



	Vector3<float> Transform::GetLocalScale()
	{
		return (m_LocalScale);
	}



	Vector3<float> Transform::GetPosition()
	{
		if (IsDirty())
		{
			RefreshTransformMatrix();
		}

		Vector3f position(
			m_AccumulatedTransformMatrix.data[3],
			m_AccumulatedTransformMatrix.data[7],
			m_AccumulatedTransformMatrix.data[11]);

		return (position);
	}



	Quaternion<float> Transform::GetRotation()
	{
		if (IsDirty())
		{
			RefreshTransformMatrix();
		}

		Quaternion<float> rotation;

		if (m_Parent != nullptr)
		{
			rotation *= m_Parent->GetRotation();
		}

		rotation *= m_LocalRotation;
		return (rotation);
	}



	Matrix4<float> Transform::GetTransformMatrix()
	{
		if (IsDirty())
		{
			RefreshTransformMatrix();
		}

		return (m_AccumulatedTransformMatrix);
	}



	void Transform::RefreshTransformMatrix()
	{
		// TODO : ToIdentity matrix method?
		m_AccumulatedTransformMatrix = Matrix4<float>::Identity();

		Matrix4<float> translationMatrix = GetLocalTranslationMatrix();
		Matrix4<float> rotationMatrix = GetLocalRotationMatrix();
		Matrix4<float> scaleMatrix = GetLocalScaleMatrix();

		if (m_Parent != nullptr)
		{
			m_AccumulatedTransformMatrix = m_Parent->GetTransformMatrix();
		}

		m_AccumulatedTransformMatrix *= translationMatrix;
		m_AccumulatedTransformMatrix *= rotationMatrix;
		m_AccumulatedTransformMatrix *= scaleMatrix;

		m_IsDirty = false;
	}



	Matrix4<float> Transform::GetLocalTranslationMatrix() const
	{
		return MakeTranslationMatrix(m_LocalPosition.x, m_LocalPosition.y, m_LocalPosition.z);
	}



	Matrix4<float> Transform::GetLocalRotationMatrix() const
	{
		Matrix4<float> rotationMatrix = m_LocalRotation.ToRotationMatrix();
		return (rotationMatrix);
	}



	Matrix4<float> Transform::GetLocalScaleMatrix() const
	{
		Matrix4<float> scaleMatrix;
		scaleMatrix.m11(m_LocalScale.x);
		scaleMatrix.m22(m_LocalScale.y);
		scaleMatrix.m33(m_LocalScale.z);
		scaleMatrix.m44(float(1));

		return (scaleMatrix);
	}



	void Transform::LocalTranslate(Vector3<float> localTranslation)
	{
		m_LocalPosition += localTranslation;
	}



	void Transform::LocalRotate(Vector3<float> localRotation)
	{
		// TODO : method
		throw - 1;
	}



	void Transform::CopyFrom(Component& other, EntitiesAndComponentsRemap* mappings)
	{
		auto& otherTransformRef = DoCast<Transform>(other);

		m_IsDirty = true;
		m_LocalPosition = otherTransformRef.m_LocalPosition;
		m_LocalRotation = otherTransformRef.m_LocalRotation;
		m_LocalScale = otherTransformRef.m_LocalScale;

		if (
			mappings == nullptr ||
			otherTransformRef.GetParent() == nullptr ||
			!mappings->IsComponentRegistered(otherTransformRef.GetParent()->GetId()) ||
			!mappings->IsComponentMapped(otherTransformRef.GetParent()->GetId())
		)
		{
			m_Parent = otherTransformRef.m_Parent;
		}
		else
		{
			SetParent(*mappings->GetMappedComponent(otherTransformRef.GetParent()->GetId()));
		}
	}



	void Transform::OnCreate()
	{
		SetDirty();
	}



	void Transform::OnDestroy()
	{
		// Nothing here
		throw - 1;

		// TODO : ensure that OnDestroy is being called

		if (m_Parent != nullptr)
		{
			// TODO : get component reference to this transform
			//m_Parent->RemoveChild(this);
			m_Parent = nullptr;
			throw - 1;
		}
	}



	void Transform::AddChild(ComponentReference<Transform> transform)
	{
		auto it = std::find_if(m_Childs.begin(), m_Childs.end(), [&transform](ComponentReference<Transform> child)
		{
			return (child == transform);
		});

		if (it != m_Childs.end())
		{
			// TODO : better error treating
			throw - 1;
		}

		m_Childs.push_back(transform);
	}



	void Transform::RemoveChild(ComponentReference<Transform> transform)
	{
		auto it = std::find_if(m_Childs.begin(), m_Childs.end(), [&transform](ComponentReference<Transform> child)
		{
			return (child == transform);
		});

		if (it == m_Childs.end())
		{
			// TODO : better error treating
			throw - 1;
		}

		m_Childs.erase(it);
	}
}

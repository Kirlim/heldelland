//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/ResourcesManager.h"

#include"Nest/Exceptions/AlreadyRegisteredException.h"
#include"Nest/Exceptions/NotRegisteredException.h"
#include"Nest/Exceptions/NullArgumentException.h"
#include"Nest/Exceptions/LogicErrorException.h"



namespace Nest
{
	Font* ResourcesManager::DoFetchFont(const std::string resourceName) const
	{
		Font* font = nullptr;

		if (m_FontsData.count(resourceName) > 0)
		{
			font = m_FontsData.at(resourceName).get();
		}

		return (font);
	}



	Material* ResourcesManager::DoFetchMaterial(const std::string resourceName) const
	{
		Material* material = nullptr;

		if (m_MaterialsData.count(resourceName) > 0)
		{
			material = m_MaterialsData.at(resourceName).get();
		}

		return (material);
	}



	Mesh* ResourcesManager::DoFetchMesh(const std::string resourceName) const
	{
		Mesh* mesh = nullptr;

		if (m_MeshesData.count(resourceName) > 0)
		{
			mesh = m_MeshesData.at(resourceName).get();
		}

		return (mesh);
	}



	Model* ResourcesManager::DoFetchModel(const std::string resourceName) const
	{
		Model* model = nullptr;

		if (m_ModelsData.count(resourceName) > 0)
		{
			model = m_ModelsData.at(resourceName).get();
		}

		return (model);
	}



	ShaderProgram* ResourcesManager::DoFetchShaderProgram(const std::string resourceName) const
	{
		ShaderProgram* shaderProgram = nullptr;

		if (m_ShaderProgramsData.count(resourceName) > 0)
		{
			shaderProgram = m_ShaderProgramsData.at(resourceName).get();
		}

		return (shaderProgram);
	}



	Texture* ResourcesManager::DoFetchTexture(const std::string resourceName) const
	{
		Texture* texture = nullptr;

		if (m_TexturesData.count(resourceName) > 0)
		{
			texture = m_TexturesData.at(resourceName).get();
		}

		return (texture);
	}



	void ResourcesManager::DoAddFont(const std::string resourceName, std::unique_ptr<Font>& font)
	{
		if (font == nullptr)
			ThrowNullArgumentExceptionWithLine("Cannot add null font resource");
		if (resourceName.empty())
			ThrowLogicErrorExceptionWithLine("Font resource name cannot have size 0");
		if (m_FontsData.count(resourceName) > 0)
			ThrowAlreadyRegisteredExceptionWithLine("There is already a font resource with same resource name added");

		m_FontsData[resourceName].swap(font);
	}



	void ResourcesManager::DoUnloadFont(const std::string resourceName)
	{
		if (m_FontsData.count(resourceName) > 0)
		{
			m_FontsData.erase(resourceName);
		}
		else
		{
			ThrowNotRegisteredExceptionWithLine("There is not a font with resource name [" + resourceName + "] added");
		}
	}



	void ResourcesManager::DoAddMaterial(const std::string resourceName, std::unique_ptr<Material>& material)
	{
		if (material == nullptr)
			ThrowNullArgumentExceptionWithLine("Cannot add null material resource");
		if (resourceName.empty())
			ThrowLogicErrorExceptionWithLine("Material resource name cannot have size 0");
		if (m_MaterialsData.count(resourceName) > 0)
			ThrowAlreadyRegisteredExceptionWithLine("There is already a material resource with same resource name added");

		m_MaterialsData[resourceName].swap(material);
	}



	void ResourcesManager::DoUnloadMaterial(const std::string resourceName)
	{
		if (m_MaterialsData.count(resourceName) > 0)
		{
			m_MaterialsData.erase(resourceName);
		}
		else
		{
			ThrowNotRegisteredExceptionWithLine("There is not a material with resource name [" + resourceName + "] added");
		}
	}



	void ResourcesManager::DoAddMesh(std::string resourceName, std::unique_ptr<Mesh>& mesh)
	{
		if (mesh == nullptr)
			ThrowNullArgumentExceptionWithLine("Cannot add null mesh resource");
		if (resourceName.empty())
			ThrowLogicErrorExceptionWithLine("Mesh resource name cannot have size 0");
		if (m_MeshesData.count(resourceName) > 0)
			ThrowAlreadyRegisteredExceptionWithLine("There is already a mesh resource with same resource name added");

		m_MeshesData[resourceName].swap(mesh);
	}



	void ResourcesManager::DoUnloadMesh(std::string resourceName)
	{
		if (m_MeshesData.count(resourceName) > 0)
		{
			m_MeshesData.erase(resourceName);
		}
		else
		{
			ThrowNotRegisteredExceptionWithLine("There is not a mesh with resource name [" + resourceName + "] added");
		}
	}



	void ResourcesManager::DoAddModel(const std::string resourceName, std::unique_ptr<Model>& model)
	{
		if (model == nullptr)
			ThrowNullArgumentExceptionWithLine("Cannot add null model resource");
		if (resourceName.empty())
			ThrowLogicErrorExceptionWithLine("Model resource name cannot have size 0");
		if (m_ModelsData.count(resourceName) > 0)
			ThrowAlreadyRegisteredExceptionWithLine("There is already a model resource with same resource name added");

		m_ModelsData[resourceName].swap(model);
	}



	void ResourcesManager::DoUnloadModel(const std::string resourceName)
	{
		if (m_ModelsData.count(resourceName) > 0)
		{
			m_ModelsData.erase(resourceName);
		}
		else
		{
			ThrowNotRegisteredExceptionWithLine("There is not a model with resource name [" + resourceName + "] added");
		}
	}



	void ResourcesManager::DoLoadShaderProgram(const std::string vertexShaderFile, const std::string fragmentShaderFile, const std::string resourceName)
	{
		if (m_ShaderProgramsData.count(resourceName) > 0)
		{
			// TODO : launch exception
		}

		m_ShaderProgramsData[resourceName].reset(new ShaderProgram());
		m_ShaderProgramsData[resourceName].get()->LoadFromFile(vertexShaderFile, fragmentShaderFile);

		// TODO : handle shader load exception????

		if (!m_ShaderProgramsData[resourceName].get()->IsLoaded())
		{
			m_ShaderProgramsData.erase(resourceName);
		}
		else
		{
			// TODO : throw exception
		}
	}



	void ResourcesManager::DoLoadShaderProgramContents(const std::string vertexShaderContents, const std::string fragmentShaderContents, const std::string resourceName)
	{
		if (m_ShaderProgramsData.count(resourceName) > 0)
		{
			// TODO : throw exception
		}

		m_ShaderProgramsData[resourceName].reset(new ShaderProgram());
		m_ShaderProgramsData[resourceName].get()->LoadFromString(vertexShaderContents, fragmentShaderContents);

		// TODO : handle shader load exception????

		if (!m_ShaderProgramsData[resourceName].get()->IsLoaded())
		{
			m_ShaderProgramsData.erase(resourceName);
		}
		else
		{
			// TODO : throw exception
		}
	}



	void ResourcesManager::DoAddShaderProgram(const std::string resourceName, std::unique_ptr<ShaderProgram>& shaderProgram)
	{
		if (shaderProgram == nullptr)
			ThrowNullArgumentExceptionWithLine("Cannot add null shader program resource");
		if (resourceName.empty())
			ThrowLogicErrorExceptionWithLine("Shader program resource name cannot have size 0");
		if (m_ShaderProgramsData.count(resourceName) > 0)
			ThrowAlreadyRegisteredExceptionWithLine("There is already a shader program resource with same resource name added");

		m_ShaderProgramsData[resourceName].swap(shaderProgram);
	}



	void ResourcesManager::DoUnloadShaderProgram(const std::string resourceName)
	{
		if (m_ShaderProgramsData.count(resourceName) > 0)
		{
			m_ShaderProgramsData.erase(resourceName);
		}
		else
		{
			ThrowNotRegisteredExceptionWithLine("There is not a shader program with resource name [" + resourceName + "] added");
		}
	}



	void ResourcesManager::DoAddTexture(const std::string resourceName, std::unique_ptr<Texture>& texture)
	{
		if (texture == nullptr)
			ThrowNullArgumentExceptionWithLine("Cannot add null texture resource");
		if (resourceName.empty())
			ThrowLogicErrorExceptionWithLine("Texture resource name cannot have size 0");
		if (m_TexturesData.count(resourceName) > 0)
			ThrowAlreadyRegisteredExceptionWithLine("There is already a texture resource with same resource name added");

		m_TexturesData[resourceName].swap(texture);
	}



	void ResourcesManager::DoUnloadTexture(const std::string resourceName)
	{
		if (m_TexturesData.count(resourceName) > 0)
		{
			m_TexturesData.erase(resourceName);
		}
		else
		{
			ThrowNotRegisteredExceptionWithLine("There is not a texture with resource name [" + resourceName + "] added");
		}
	}



	ResourcesManager::~ResourcesManager()
	{
		// Be careful, the order matters
		m_FontsData.clear();
		m_MeshesData.clear();
		m_MaterialsData.clear();
		m_TexturesData.clear();
		m_ShaderProgramsData.clear();
	}
}

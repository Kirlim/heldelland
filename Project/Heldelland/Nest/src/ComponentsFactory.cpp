//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/ComponentsFactory.h"

#include<algorithm>

#include"Nest/NestAssert.h"
#include"Nest/Exceptions/NullArgumentException.h"
#include"Nest/Exceptions/LogicErrorException.h"

#include"Nest/ComponentOwnerReferenceBase.h"



namespace Nest
{
	ComponentsFactory::~ComponentsFactory() = default;



	void ComponentsFactory::DoRegisterFactory(const unsigned int factoryTypeId, const std::string typeName, std::unique_ptr<BasicComponentsFactory> factory)
	{
		if (factory == nullptr)
			ThrowNullArgumentExceptionWithLine("The parameter [factory] cannot be null.");

		if (typeName.empty())
			ThrowLogicErrorExceptionWithLine("The type name cannot be empty.");

		if (m_Factories.count(factoryTypeId) > 0)
			ThrowLogicErrorExceptionWithLine("There is already an factory with type id [" + std::to_string(factoryTypeId) + "] registered.");

		if (m_TypeNameMap.count(typeName) > 0)
			ThrowLogicErrorExceptionWithLine("There is already an factory with type name [" + typeName + "] registered.");


		m_Factories[factoryTypeId] = std::move(factory);
		m_TypeNameMap[typeName] = factoryTypeId;
	}



	std::unique_ptr<ComponentOwnerReferenceBase> ComponentsFactory::DoInstantiate(const unsigned int factoryTypeId)
	{
		NestAssert(m_Factories.count(factoryTypeId) > 0);
		return (m_Factories[factoryTypeId]->Instantiate());
	}



	std::unique_ptr<ComponentOwnerReferenceBase> ComponentsFactory::DoInstantiate(const std::string typeName)
	{
		NestAssert(m_TypeNameMap.count(typeName) > 0);

		const int typeId = m_TypeNameMap.at(typeName);
		return (DoInstantiate(typeId));
	}



	std::string ComponentsFactory::DoGetTypeName(const unsigned int factoryTypeId) const
	{
		const auto found = std::find_if(
			m_TypeNameMap.begin(),
			m_TypeNameMap.end(),
			[&](auto& pair)
			{
				return (std::get<unsigned int>(pair) == factoryTypeId);
			});

		if (found == m_TypeNameMap.end())
			ThrowLogicErrorExceptionWithLine("Factory Type Id [" + std::to_string(factoryTypeId) + "] was not found in the components factory.");

		return (found->first);
	}



	unsigned int ComponentsFactory::DoGetFactoryTypeId(const std::string typeName) const
	{
		if (m_TypeNameMap.count(typeName) == 0)
			ThrowLogicErrorExceptionWithLine("Type name [" + typeName + "] is not registered in the components factory.");

		return (m_TypeNameMap.at(typeName));
	}
}

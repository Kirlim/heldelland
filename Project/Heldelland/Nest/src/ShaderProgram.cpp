//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/ShaderProgram.h"

#include"Nest/Exceptions/FailedToLoadException.h"
#include"Nest/Exceptions/InvalidArgumentException.h"
#include"Nest/RenderingModule.h"
#include"Nest/Engine.h"
#include"Nest/Global.h"



namespace Nest
{
	ShaderProgram::ShaderProgram()
		: ShaderProgram(Global->GetEngine()->GetRenderingModule()->GetGameRenderer())
	{
	}



	ShaderProgram::ShaderProgram(GameRenderer gameRenderer) :
		m_GameRenderer(gameRenderer)
	{
	}



	ShaderProgram::~ShaderProgram()
	{
		if (m_Loaded)
		{
			Unload();
		}
	}



	void ShaderProgram::LoadFromFile(const std::string& vertexShaderFileName, const std::string& fragmentShaderFileName)
	{
		if (m_Loaded)
			ThrowFailedToLoadExceptionWithLine("Failed to load shader program: another program is already loaded in this object");


		std::string vertexShaderCode;
		std::string fragmentShaderCode;
		std::ifstream vertexShaderFile;
		std::ifstream fragmentShaderFile;

		vertexShaderFile.exceptions(std::ifstream::badbit | std::ifstream::failbit);
		fragmentShaderFile.exceptions(std::ifstream::badbit | std::ifstream::failbit);

		try
		{
			vertexShaderFile.open(vertexShaderFileName);
			fragmentShaderFile.open(fragmentShaderFileName);

			std::stringstream vertexShaderStream;
			std::stringstream fragmentShaderStream;

			vertexShaderStream << vertexShaderFile.rdbuf();
			fragmentShaderStream << fragmentShaderFile.rdbuf();

			vertexShaderFile.close();
			fragmentShaderFile.close();

			vertexShaderCode = vertexShaderStream.str();
			fragmentShaderCode = fragmentShaderStream.str();
		}
		catch (std::ifstream::failure&)
		{
			ThrowFailedToLoadExceptionWithLine("Failed to load shader program: file not successfully loaded.");
		}


		if (m_GameRenderer != GameRenderer::OpenGL) return;

		LoadFromString(vertexShaderCode, fragmentShaderCode);
	}



	void ShaderProgram::LoadFromString(const std::string& vertexShaderCode, const std::string& fragmentShaderCode)
	{
		if (m_Loaded)
			ThrowFailedToLoadExceptionWithLine("Failed to load shader program: another program is already loaded in this object");


		const GLchar* vertexCode = vertexShaderCode.c_str();
		const GLchar* fragmentCode = fragmentShaderCode.c_str();

		GLint success;

		GLchar infoLog[512];


		const GLuint vertex = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertex, 1, &vertexCode, nullptr);
		glCompileShader(vertex);

		glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(vertex, 512, nullptr, infoLog);
			glDeleteShader(vertex);

			throw FailedToLoadException(std::string("Failed to load shader program: ") + infoLog);
		}


		const GLuint fragment = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragment, 1, &fragmentCode, nullptr);
		glCompileShader(fragment);

		glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(fragment, 512, nullptr, infoLog);
			glDeleteShader(fragment);
			glDeleteShader(vertex);

			throw FailedToLoadException(std::string("Failed to load shader program: ") + infoLog);
		}


		m_Program = glCreateProgram();
		glAttachShader(m_Program, vertex);
		glAttachShader(m_Program, fragment);
		glLinkProgram(m_Program);

		glGetProgramiv(m_Program, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(m_Program, 512, nullptr, infoLog);
			glDetachShader(m_Program, vertex);
			glDetachShader(m_Program, fragment);
			glDeleteShader(fragment);
			glDeleteShader(vertex);
			glDeleteProgram(m_Program);

			throw FailedToLoadException(std::string("Failed to load shader program: ") + infoLog);
		}


		glDetachShader(m_Program, vertex);
		glDetachShader(m_Program, fragment);
		glDeleteShader(fragment);
		glDeleteShader(vertex);

		m_Loaded = true;
	}



	void ShaderProgram::SetMainTextureName(const std::string& mainTextureName)
	{
		this->m_MainTextureName = mainTextureName;
	}



	std::string ShaderProgram::GetMainTextureName() const
	{
		return m_MainTextureName;
	}



	void ShaderProgram::AddTextureName(const std::string& textureName) // TODO : find a way to emit warnings if name was not found
	{
		if (m_TexturesSlots.count(textureName) > 0)
			ThrowInvalidArgumentExceptionWithLine("Texture of name '" + textureName + "' has already been registered.");


		const GLint location = 0;
		if (m_GameRenderer == GameRenderer::OpenGL) glGetUniformLocation(m_Program, textureName.c_str());
		m_TexturesSlots[textureName] = location;
	}



	void ShaderProgram::RemoveTextureName(const std::string& textureName)
	{
		if (m_TexturesSlots.count(textureName) == 0)
			ThrowInvalidArgumentExceptionWithLine("There is no texture slot of name '" + textureName + "' registered.");

		m_TexturesSlots.erase(textureName);
	}



	GLint ShaderProgram::GetTextureSlot(const std::string& textureName) const
	{
		if (m_TexturesSlots.count(textureName) == 0)
			ThrowInvalidArgumentExceptionWithLine("There is no texture slot of name '" + textureName + "' registered.");

		return m_TexturesSlots.at(textureName);
	}



	const std::unordered_map<std::string, GLint>* ShaderProgram::GetTexturesSlots() const
	{
		return &m_TexturesSlots;
	}



	void ShaderProgram::SetTransparentFlag(bool transparent)
	{
		m_IsTransparent = transparent;
	}

	bool ShaderProgram::IsTransparent() const
	{
		return m_IsTransparent;
	}



	void ShaderProgram::Unload()
	{
		if (m_Loaded)
		{
			if (m_GameRenderer == GameRenderer::OpenGL) glDeleteProgram(m_Program);

			m_MainTextureName.clear();
			m_TexturesSlots.clear();

			m_IsTransparent = false;

			m_Loaded = false;
		}
	}



	bool ShaderProgram::IsLoaded() const
	{
		return m_Loaded;
	}



	void ShaderProgram::Use() const
	{
		if (m_Loaded)
		{
			if (m_GameRenderer == GameRenderer::OpenGL) glUseProgram(m_Program);
		}
	}



	GLuint ShaderProgram::GetProgramId() const
	{
		// TODO : throw exception if not loaded, in debug builds
		return m_Program;
	}
}

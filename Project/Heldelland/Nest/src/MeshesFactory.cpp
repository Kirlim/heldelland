//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/MeshesFactory.h"

#include<cmath>

#include"Nest/Vector2.h"
#include"Nest/Vector3.h"

#include"Nest/Exceptions/NullArgumentException.h"



namespace Nest
{
	MeshesFactory::MeshesFactory(const std::function<std::unique_ptr<Mesh>()> meshInstantiationFunction) :
		m_MeshInstantiationFunction(meshInstantiationFunction)
	{
		if (m_MeshInstantiationFunction == nullptr)
			ThrowNullArgumentExceptionWithLine("Mesh Instantiation function must be not null.");
	}



	std::unique_ptr<Mesh> MeshesFactory::InstantiateEmpty() const
	{
		return m_MeshInstantiationFunction();
	}



	std::unique_ptr<Mesh> MeshesFactory::InstantiateRectangle(
		const Vector3<float>& size, const Vector3<float>& center
	) const
	{
		// Vertices
		std::vector<Vector3<float>> vertices;

		vertices.emplace_back(center.x - size.x * 0.5f, center.y - size.y * 0.5f, center.z + size.z * 0.5f);
		vertices.emplace_back(center.x + size.x * 0.5f, center.y - size.y * 0.5f, center.z + size.z * 0.5f);
		vertices.emplace_back(center.x + size.x * 0.5f, center.y + size.y * 0.5f, center.z - size.z * 0.5f);
		vertices.emplace_back(center.x - size.x * 0.5f, center.y + size.y * 0.5f, center.z - size.z * 0.5f);


		// Normals
		std::vector<Vector3<float>> normals;
		normals.emplace_back(0.0f, 0.0f, 1.0f);
		normals.emplace_back(0.0f, 0.0f, 1.0f);
		normals.emplace_back(0.0f, 0.0f, 1.0f);
		normals.emplace_back(0.0f, 0.0f, 1.0f);


		// UVs
		std::vector<Vector2<float>> uvs;

		uvs.emplace_back(0.0f, 0.0f);
		uvs.emplace_back(1.0f, 0.0f);
		uvs.emplace_back(1.0f, 1.0f);
		uvs.emplace_back(0.0f, 1.0f);


		// Triangles
		std::vector<unsigned int> triangles;
		triangles.emplace_back(0);
		triangles.emplace_back(1);
		triangles.emplace_back(2);

		triangles.emplace_back(0);
		triangles.emplace_back(2);
		triangles.emplace_back(3);


		auto result = m_MeshInstantiationFunction();
		result->Make(vertices, normals, uvs, triangles);

		return (result);
	}



	std::unique_ptr<Mesh> MeshesFactory::InstantiateBox(
		const Vector3<float>& size, const Vector3<float>& center
	) const
	{
		// Data building
		std::vector<Vector3<float>> vertices;
		std::vector<Vector3<float>> normals;
		std::vector<Vector2<float>> uvs;
		std::vector<unsigned int> triangles;

		// Front Face (facing the viewer)
		vertices.emplace_back(center.x - size.x * 0.5f, center.y - size.y * 0.5f, center.z + size.z * 0.5f); // 0
		vertices.emplace_back(center.x + size.x * 0.5f, center.y - size.y * 0.5f, center.z + size.z * 0.5f); // 1
		vertices.emplace_back(center.x + size.x * 0.5f, center.y + size.y * 0.5f, center.z + size.z * 0.5f); // 2
		vertices.emplace_back(center.x - size.x * 0.5f, center.y + size.y * 0.5f, center.z + size.z * 0.5f); // 3

		normals.emplace_back(0.0f, 0.0f, 1.0f);
		normals.emplace_back(0.0f, 0.0f, 1.0f);
		normals.emplace_back(0.0f, 0.0f, 1.0f);
		normals.emplace_back(0.0f, 0.0f, 1.0f);

		uvs.emplace_back(0.0f, 0.0f);
		uvs.emplace_back(1.0f, 0.0f);
		uvs.emplace_back(1.0f, 1.0f);
		uvs.emplace_back(0.0f, 1.0f);

		triangles.emplace_back(0);
		triangles.emplace_back(1);
		triangles.emplace_back(2);

		triangles.emplace_back(0);
		triangles.emplace_back(2);
		triangles.emplace_back(3);



		// Back Face
		vertices.emplace_back(center.x + size.x * 0.5f, center.y - size.y * 0.5f, center.z - size.z * 0.5f); // 4
		vertices.emplace_back(center.x - size.x * 0.5f, center.y - size.y * 0.5f, center.z - size.z * 0.5f); // 5
		vertices.emplace_back(center.x - size.x * 0.5f, center.y + size.y * 0.5f, center.z - size.z * 0.5f); // 6
		vertices.emplace_back(center.x + size.x * 0.5f, center.y + size.y * 0.5f, center.z - size.z * 0.5f); // 7

		normals.emplace_back(0.0f, 0.0f, -1.0f);
		normals.emplace_back(0.0f, 0.0f, -1.0f);
		normals.emplace_back(0.0f, 0.0f, -1.0f);
		normals.emplace_back(0.0f, 0.0f, -1.0f);

		uvs.emplace_back(0.0f, 0.0f);
		uvs.emplace_back(1.0f, 0.0f);
		uvs.emplace_back(1.0f, 1.0f);
		uvs.emplace_back(0.0f, 1.0f);

		triangles.emplace_back(4);
		triangles.emplace_back(5);
		triangles.emplace_back(6);

		triangles.emplace_back(4);
		triangles.emplace_back(6);
		triangles.emplace_back(7);



		// Left Face
		vertices.emplace_back(center.x - size.x * 0.5f, center.y - size.y * 0.5f, center.z - size.z * 0.5f); // 8
		vertices.emplace_back(center.x - size.x * 0.5f, center.y - size.y * 0.5f, center.z + size.z * 0.5f); // 9
		vertices.emplace_back(center.x - size.x * 0.5f, center.y + size.y * 0.5f, center.z + size.z * 0.5f); // 10
		vertices.emplace_back(center.x - size.x * 0.5f, center.y + size.y * 0.5f, center.z - size.z * 0.5f); // 11

		normals.emplace_back(-1.0f, 0.0f, 0.0f);
		normals.emplace_back(-1.0f, 0.0f, 0.0f);
		normals.emplace_back(-1.0f, 0.0f, 0.0f);
		normals.emplace_back(-1.0f, 0.0f, 0.0f);

		uvs.emplace_back(0.0f, 0.0f);
		uvs.emplace_back(1.0f, 0.0f);
		uvs.emplace_back(1.0f, 1.0f);
		uvs.emplace_back(0.0f, 1.0f);

		triangles.emplace_back(8);
		triangles.emplace_back(9);
		triangles.emplace_back(10);

		triangles.emplace_back(8);
		triangles.emplace_back(10);
		triangles.emplace_back(11);



		// Right Face
		vertices.emplace_back(center.x + size.x * 0.5f, center.y - size.y * 0.5f, center.z + size.z * 0.5f); // 12
		vertices.emplace_back(center.x + size.x * 0.5f, center.y - size.y * 0.5f, center.z - size.z * 0.5f); // 13
		vertices.emplace_back(center.x + size.x * 0.5f, center.y + size.y * 0.5f, center.z - size.z * 0.5f); // 14
		vertices.emplace_back(center.x + size.x * 0.5f, center.y + size.y * 0.5f, center.z + size.z * 0.5f); // 15

		normals.emplace_back(1.0f, 0.0f, 0.0f);
		normals.emplace_back(1.0f, 0.0f, 0.0f);
		normals.emplace_back(1.0f, 0.0f, 0.0f);
		normals.emplace_back(1.0f, 0.0f, 0.0f);

		uvs.emplace_back(0.0f, 0.0f);
		uvs.emplace_back(1.0f, 0.0f);
		uvs.emplace_back(1.0f, 1.0f);
		uvs.emplace_back(0.0f, 1.0f);

		triangles.emplace_back(12);
		triangles.emplace_back(13);
		triangles.emplace_back(14);

		triangles.emplace_back(12);
		triangles.emplace_back(14);
		triangles.emplace_back(15);



		// Top face
		vertices.emplace_back(center.x - size.x * 0.5f, center.y + size.y * 0.5f, center.z + size.z * 0.5f); // 16
		vertices.emplace_back(center.x + size.x * 0.5f, center.y + size.y * 0.5f, center.z + size.z * 0.5f); // 17
		vertices.emplace_back(center.x + size.x * 0.5f, center.y + size.y * 0.5f, center.z - size.z * 0.5f); // 18
		vertices.emplace_back(center.x - size.x * 0.5f, center.y + size.y * 0.5f, center.z - size.z * 0.5f); // 19

		normals.emplace_back(0.0f, 1.0f, 0.0f);
		normals.emplace_back(0.0f, 1.0f, 0.0f);
		normals.emplace_back(0.0f, 1.0f, 0.0f);
		normals.emplace_back(0.0f, 1.0f, 0.0f);

		uvs.emplace_back(0.0f, 0.0f);
		uvs.emplace_back(1.0f, 0.0f);
		uvs.emplace_back(1.0f, 1.0f);
		uvs.emplace_back(0.0f, 1.0f);

		triangles.emplace_back(16);
		triangles.emplace_back(17);
		triangles.emplace_back(18);

		triangles.emplace_back(16);
		triangles.emplace_back(18);
		triangles.emplace_back(19);



		// Bottom Face
		vertices.emplace_back(center.x - size.x * 0.5f, center.y - size.y * 0.5f, center.z - size.z * 0.5f); // 20
		vertices.emplace_back(center.x + size.x * 0.5f, center.y - size.y * 0.5f, center.z - size.z * 0.5f); // 21
		vertices.emplace_back(center.x + size.x * 0.5f, center.y - size.y * 0.5f, center.z + size.z * 0.5f); // 22
		vertices.emplace_back(center.x - size.x * 0.5f, center.y - size.y * 0.5f, center.z + size.z * 0.5f); // 23

		normals.emplace_back(0.0f, -1.0f, 0.0f);
		normals.emplace_back(0.0f, -1.0f, 0.0f);
		normals.emplace_back(0.0f, -1.0f, 0.0f);
		normals.emplace_back(0.0f, -1.0f, 0.0f);

		uvs.emplace_back(0.0f, 0.0f);
		uvs.emplace_back(1.0f, 0.0f);
		uvs.emplace_back(1.0f, 1.0f);
		uvs.emplace_back(0.0f, 1.0f);

		triangles.emplace_back(20);
		triangles.emplace_back(21);
		triangles.emplace_back(22);

		triangles.emplace_back(20);
		triangles.emplace_back(22);
		triangles.emplace_back(23);


		auto result = m_MeshInstantiationFunction();
		result->Make(vertices, normals, uvs, triangles);

		return (result);
	}



	std::unique_ptr<Mesh> MeshesFactory::InstantiateSphere(
		const float radius, const int horizontalSlices, const int verticalSlices
	) const
	{
		const float pi = 3.1415927f;

		// Vertices
		std::vector<Vector3<float>> vertices;
		std::vector<Vector3<float>> normals;
		std::vector<Vector2<float>> uvs;


		for (auto y = 0; y <= verticalSlices; ++y)
		{
			float yPct = float(y) / float(verticalSlices);
			float yCoord = sin((yPct * pi) - (pi * 0.5f));

			for (auto x = 0; x <= horizontalSlices; ++x)
			{
				float xPct = float(x) / float(horizontalSlices);

				float xCoord = cos(xPct * pi * 2.0f) * sin(yPct * pi);
				float zCoord = -sin(xPct * pi * 2.0f) * sin(yPct * pi);

				vertices.emplace_back(xCoord * radius, yCoord * radius, zCoord * radius);
				normals.emplace_back(xCoord, yCoord, zCoord);
				uvs.emplace_back(xPct, yPct);
			}
		}


		// Triangles
		std::vector<unsigned int> tris;
		for (auto y = 0; y < verticalSlices; ++y)
		{
			for (auto x = 0; x < horizontalSlices; ++x)
			{
				auto v = y * (horizontalSlices + 1) + x;

				tris.emplace_back(v);
				tris.emplace_back(v + 1);
				tris.emplace_back(v + horizontalSlices + 2);

				tris.emplace_back(v);
				tris.emplace_back(v + horizontalSlices + 2);
				tris.emplace_back(v + horizontalSlices + 1);
			}
		}


		// Make and return the mesh
		auto result = m_MeshInstantiationFunction();
		result->Make(vertices, normals, uvs, tris);

		return (result);
	}
}

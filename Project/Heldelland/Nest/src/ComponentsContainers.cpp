
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/ComponentsContainers.h"



namespace Nest
{
    ComponentsContainers::~ComponentsContainers()
    {
        // Nothing here
    }



    bool ComponentsContainers::DoRegisterSingleContainer(const unsigned int typeId, std::unique_ptr<ISingleComponentsContainer> container)
    {
        bool success = false;

        if (m_SingleContainers.count(typeId) == 0)
        {
            m_SingleContainers[typeId] = std::move(container);
            success = true;
        }

        return(success);
    }



    ISingleComponentsContainer* ComponentsContainers::DoGetSingleContainer(const unsigned int typeId) const
    {
        ISingleComponentsContainer* result = nullptr;

        if (m_SingleContainers.count(typeId) > 0)
        {
            result = m_SingleContainers.at(typeId).get();
        }

        return(result);
    }
}
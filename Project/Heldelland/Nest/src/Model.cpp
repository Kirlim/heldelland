//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Model.h"

#include<Nest/Exceptions/LogicErrorException.h>

#include"Nest/ModelNode.h"
#include"Nest/IEntity.h"



namespace Nest
{
	Model::Model() = default;



	Model::~Model() = default;



	bool Model::IsLoaded() const
	{
		return (m_RootNode != nullptr);
	}



	Model::Model(std::unique_ptr<ModelNode>& rootNode)
	{
		if (rootNode == nullptr)
		{
			throw std::logic_error("Failed to load model: cannot set null root node.");
		}

		m_RootNode = std::move(rootNode);
	}



	// TODO : this make unique is suspicious. Investigate why this was done, and finde a better way.
	std::unique_ptr<Model> Model::MakeUnique(std::unique_ptr<ModelNode>& rootModelNode)
	{
		const auto newModel = new(std::nothrow) Model{ rootModelNode };

		if (newModel == nullptr)
			ThrowLogicErrorExceptionWithLine("Model has failed to create a new instance.");


		return std::unique_ptr<Model>(newModel);
	}
}

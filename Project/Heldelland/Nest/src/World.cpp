//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/World.h"

#include"Nest/Exceptions/NullPointerException.h"
#include"Nest/IComponentsContainers.h"
#include"Nest/IComponentsFactory.h"



namespace Nest
{
	World::World(
		std::unique_ptr<IComponentsContainers>& componentsContainers,
		std::unique_ptr<IComponentsFactory>& componentsFactory,
		std::unique_ptr<IResourcesManager>& resourcesManager,
		std::unique_ptr<RenderingManager>& renderingManager,
		std::unique_ptr<SystemsManager>& systemsManager
	)
	{
		if (componentsContainers == nullptr)
			ThrowNullPointerExceptionWithLine("Components Containers cannot be null.");

		if (componentsFactory == nullptr)
			ThrowNullPointerExceptionWithLine("Components Factory cannot be null.");

		if (resourcesManager == nullptr)
			ThrowNullPointerExceptionWithLine("Resources Manager cannot be null.");

		if (renderingManager == nullptr)
			ThrowNullPointerExceptionWithLine("Rendering Manager cannot be null.");

		if (systemsManager == nullptr)
			ThrowNullPointerExceptionWithLine("Systems Manager cannot be null.");


		m_ResourcesManager = std::move(resourcesManager);
		m_RenderingManager = std::move(renderingManager);
		m_SystemsManager = std::move(systemsManager);
		m_ComponentsFactory = std::move(componentsFactory);
		m_ComponentsContainers = std::move(componentsContainers);

		InstantiateEntitiesManager();
	}



	World::~World() = default;



	IComponentsContainers* World::GetComponentsContainers() const
	{
		return m_ComponentsContainers.get();
	}



	IComponentsFactory* World::GetComponentsFactory() const
	{
		return m_ComponentsFactory.get();
	}



	EntitiesManager* World::GetEntitiesManager() const
	{
		return m_EntitiesManager.get();
	}



	IResourcesManager* World::GetResourcesManager() const
	{
		return m_ResourcesManager.get();
	}



	RenderingManager* World::GetRenderingManager() const
	{
		return m_RenderingManager.get();
	}



	SystemsManager* World::GetSystemsManager() const
	{
		return m_SystemsManager.get();
	}



	void World::InstantiateEntitiesManager()
	{
		m_EntitiesManager = std::make_unique<EntitiesManager>(*this);
	}
}

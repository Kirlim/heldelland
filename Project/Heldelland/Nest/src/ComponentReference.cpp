
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/ComponentReference.h"



namespace Nest
{
	ComponentReferenceBase::~ComponentReferenceBase()
	{
		if (m_ComponentOwnerReference != nullptr)
		{
			m_ComponentOwnerReference->UnregisterComponentReference(*this);
			m_ComponentOwnerReference = nullptr;
		}
	}



	void ComponentReferenceBase::Reset()
	{
		if (m_ComponentOwnerReference != nullptr || m_UnsafePointer != nullptr)
		{
			UnregisterFromOwnerReference();
		}
	}






	std::unique_ptr<ComponentReferenceBase> ComponentReferenceBase::Clone()
	{
		return(DoClone());
	};



	void ComponentReferenceBase::UpdateUnsafePointer(Component* pointer)
	{
		m_UnsafePointer = pointer;
	}



	void ComponentReferenceBase::RegisterToOwnerReference(ComponentOwnerReferenceBase& other)
	{
		NestAssert(m_ComponentOwnerReference == nullptr && m_UnsafePointer == nullptr);

		m_ComponentOwnerReference = &other;
		m_UnsafePointer = other.Get();

		other.RegisterComponentReference(*this);
	}



	void ComponentReferenceBase::UnregisterFromOwnerReference()
	{
		NestAssert(m_ComponentOwnerReference != nullptr && m_UnsafePointer != nullptr);

		m_ComponentOwnerReference->UnregisterComponentReference(*this);

		m_ComponentOwnerReference = nullptr;
		m_UnsafePointer = nullptr;
	}
}

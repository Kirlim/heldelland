
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/IComponentsContainers.h"

#include"Nest/ISingleComponentsContainer.h"



namespace Nest
{
    IComponentsContainers::~IComponentsContainers()
    {
        // Nothing here
    }



    bool IComponentsContainers::RegisterSingleContainer(const unsigned int typeId, std::unique_ptr<ISingleComponentsContainer> container)
    {
        return(DoRegisterSingleContainer(typeId, std::move(container)));
    }



    ISingleComponentsContainer* IComponentsContainers::GetSingleContainer(const unsigned int typeId) const
    {
        return(DoGetSingleContainer(typeId));
    }
}
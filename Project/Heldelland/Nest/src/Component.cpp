
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Component.h"

#include"Nest/IEntity.h"



namespace Nest
{
	ComponentId Component::m_NextId{ 1 };



	Component::Component() :
		enabled(true),
		m_Name(""),
		m_Owner(nullptr),
		m_Id(GetNewId())
	{
		// Nothing here
	}



	Component::~Component()
	{
		//
	}



	void Component::SetName(std::string name)
	{
		m_Name = name;
	}



	std::string Component::GetName() const
	{
		return m_Name;
	}



	IEntity* Component::GetOwner() const
	{
		return m_Owner;
	}



	ComponentId Component::GetId() const
	{
		return m_Id;
	}



	void Component::Destroy()
	{
		// TODO : use owning entity to remove this component.

		// TODO : think of a way to be able to do this method
	}



	void Component::SetOwner(IEntity* owner)
	{
		this->m_Owner = owner;
	}



	ComponentId Component::GetNewId()
	{
		return ComponentId{ m_NextId.value++ };
	}
}

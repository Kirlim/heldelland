//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/IEntityFromModelBuilder.h"

#include"Nest/Exceptions/LogicErrorException.h"
#include"Nest/IEntity.h"
#include"Nest/Model.h"



namespace Nest
{
	IEntityFromModelBuilder::~IEntityFromModelBuilder() = default;



	IEntity* IEntityFromModelBuilder::BuildVisualEntity(const Model& model)
	{
		if (!model.IsLoaded())
			ThrowLogicErrorExceptionWithLine("Cannot build entity from a model that is not loaded.");

		return DoBuildVisualEntity(*model.m_RootNode);
	}
}

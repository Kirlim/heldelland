
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/PointLightComponent.h"

#include<cmath>



namespace Nest
{
    PointLightComponent::PointLightComponent() : Component(),
        m_Color(),
        m_LinearAttenuation(),
        m_QuadraticAttenuation(),
        m_DarkCut(),
        m_Radius()
    {
        // Nothing here
    }



    PointLightComponent::~PointLightComponent()
    {
        // Nothing here
    }



    void PointLightComponent::SetColor(Vector3<float> color)
    {
        m_Color = color;
        RecalculateLightRadius();
    }



    Vector3<float> PointLightComponent::GetColor() const
    {
        return(m_Color);
    }



    void PointLightComponent::SetLinearAttenuation(float linearAttenuation)
    {
        m_LinearAttenuation = linearAttenuation;
        RecalculateLightRadius();
    }



    void PointLightComponent::SetQuadraticAttenuation(float quadraticAttenuation)
    {
        m_QuadraticAttenuation = quadraticAttenuation;
        RecalculateLightRadius();
    }



    float PointLightComponent::GetLinearAttenuation() const
    {
        return(m_LinearAttenuation);
    }



    float PointLightComponent::GetQuadraticAttenuation() const
    {
        return(m_QuadraticAttenuation);
    }



    float PointLightComponent::GetVolumeRadius() const
    {
        return(m_Radius);
    }



    void PointLightComponent::SetDarkCut(float darkCut)
    {
        m_DarkCut = darkCut;
        RecalculateLightRadius();
    }



    float PointLightComponent::GetDarkCut() const
    {
        return(m_DarkCut);
    }



    void PointLightComponent::RecalculateLightRadius()
    {
        float lightMax = m_Color.x > m_Color.y ? m_Color.x : m_Color.y;
        lightMax = m_Color.z > lightMax ? m_Color.z : lightMax;

        m_Radius =
            (-m_LinearAttenuation + sqrtf(m_LinearAttenuation * m_LinearAttenuation - 4 * m_QuadraticAttenuation * (1.0f - (1.0f / m_DarkCut) * lightMax)))
            / (2.0f * m_QuadraticAttenuation);
    }



    void PointLightComponent::OnDestroy()
    {
        // Nothing here
    }
}

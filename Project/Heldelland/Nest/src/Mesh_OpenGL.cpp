//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Mesh_OpenGL.h"

#include"Nest/Vector2.h"
#include"Nest/Vector3.h"

#include"Nest/Exceptions/InvalidArgumentException.h"



namespace Nest
{
	void Mesh_OpenGL::DoMake(
		const std::vector<Vector3<float>>& vertices,
		const std::vector<Vector3<float>>& normals,
		const std::vector<Vector2<float>>& uvs,
		const std::vector<unsigned int>& triangles
	)
	{
		if (m_IsLoaded) Unload();

		m_VertexPropertiesSize = 3; // Vertex
		m_NormalsStartOffset = 0;
		m_NormalsAttributeIndex = 0;
		m_UVsStartOffset = 0;
		m_UVsAttributeIndex = 0;

		m_VerticesCount = static_cast<unsigned int>(vertices.size());
		m_HasNormals = (!normals.empty());
		m_HasUVs = (!uvs.empty());

		if (m_HasNormals)
		{
			if (vertices.size() != normals.size())
				ThrowInvalidArgumentExceptionWithLine("Count of vertices and normals mismatch.");

			m_NormalsStartOffset = m_VertexPropertiesSize;
			m_VertexPropertiesSize += 3; // Add normal
		}

		if (m_HasUVs)
		{
			if (vertices.size() != uvs.size())
				ThrowInvalidArgumentExceptionWithLine("Count of vertices and uvs mismatch.");

			m_UVsStartOffset = m_VertexPropertiesSize;
			m_VertexPropertiesSize += 2; // Add UV
		}

		m_VerticesData.clear();
		for (unsigned int i = 0; i < m_VerticesCount; ++i)
		{
			m_VerticesData.push_back(vertices[i].x);
			m_VerticesData.push_back(vertices[i].y);
			m_VerticesData.push_back(vertices[i].z);

			if (m_HasNormals)
			{
				m_VerticesData.push_back(normals[i].x);
				m_VerticesData.push_back(normals[i].y);
				m_VerticesData.push_back(normals[i].z);
			}

			if (m_HasUVs)
			{
				m_VerticesData.push_back(uvs[i].x);
				m_VerticesData.push_back(uvs[i].y);
			}
		}


		m_Triangles = triangles;


		const unsigned currentAttributeIndex = 0;

		glGenVertexArrays(1, &m_VAO);

		glGenBuffers(1, &m_VBO);
		glGenBuffers(1, &m_EBO);

		glBindVertexArray(m_VAO);

		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
		glBufferData(GL_ARRAY_BUFFER, m_VerticesData.size() * sizeof(GLfloat), &m_VerticesData[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_Triangles.size() * sizeof(GLuint), &m_Triangles[0], GL_STATIC_DRAW);

		// Position
		glVertexAttribPointer(currentAttributeIndex, 3, GL_FLOAT, GL_FALSE, m_VertexPropertiesSize * sizeof(GL_FLOAT), static_cast<GLvoid*>(nullptr));
		glEnableVertexAttribArray(0);

		// Normals
		if (m_HasNormals)
		{
			m_NormalsAttributeIndex = 1;
			glVertexAttribPointer(m_NormalsAttributeIndex, 3, GL_FLOAT, GL_FALSE, m_VertexPropertiesSize * sizeof(GL_FLOAT), reinterpret_cast<GLvoid*>(m_NormalsStartOffset * sizeof(GLfloat)));
			glEnableVertexAttribArray(1);
		}

		// UVs
		if (m_HasUVs)
		{
			// TODO : make attribute index constant.
			m_UVsAttributeIndex = 2; //++currentAttributeIndex;
			glVertexAttribPointer(m_UVsAttributeIndex, 2, GL_FLOAT, GL_FALSE, m_VertexPropertiesSize * sizeof(GL_FLOAT), reinterpret_cast<GLvoid*>(m_UVsStartOffset * sizeof(GLfloat)));
			glEnableVertexAttribArray(2);
		}

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

		m_IsLoaded = true;
	}



	void Mesh_OpenGL::DoUnload()
	{
		glDeleteVertexArrays(1, &m_VAO);
		glDeleteBuffers(1, &m_VBO);
		glDeleteBuffers(1, &m_EBO);

		m_IsLoaded = false;
		m_HasUVs = false;
	}



	void Mesh_OpenGL::DoRender() const
	{
		glBindVertexArray(m_VAO);
		glDrawElements(GL_TRIANGLES, static_cast<unsigned int>(m_Triangles.size()), GL_UNSIGNED_INT, nullptr);
		glBindVertexArray(0);
	}



	bool Mesh_OpenGL::DoIsLoaded() const
	{
		return m_IsLoaded;
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/EntityFromModelBuilder.h"

#include"Nest/ComponentReference.h"
#include"Nest/IEntity.h"
#include"Nest/EntitiesManager.h"
#include"Nest/IResourcesManager.h"
#include"Nest/ModelNode.h"

#include"Nest/Transform.h"
#include"Nest/MeshFilter.h"
#include"Nest/MeshRenderer.h"



namespace Nest
{
	EntityFromModelBuilder::EntityFromModelBuilder(
		EntitiesManager& entitiesManager, const IResourcesManager& resourcesManager
	) :
		m_EntitiesManager(entitiesManager),
		m_ResourcesManager(resourcesManager)
	{
	}



	EntityFromModelBuilder::~EntityFromModelBuilder() = default;



	IEntity* EntityFromModelBuilder::DoBuildVisualEntity(const ModelNode& rootModelNode)
	{
		auto rootEntity = m_EntitiesManager.Instantiate().lock();
		const auto transform = rootEntity->AddComponent<Transform>();

		BuildVisualEntityFromNode(&rootModelNode, transform);

		return rootEntity.get();
	}



	void EntityFromModelBuilder::BuildVisualEntityFromNode(
		const ModelNode* modelNode, ComponentReference<Transform> parentTransform
	) const
	{
		// TODO : attempt to return to no entities loaded, in case of exception

		if (modelNode == nullptr) return;

		if (parentTransform == nullptr)
			ThrowLogicErrorExceptionWithLine("Parent transform cannot be null.");


		ComponentReference<Transform> baseTransform; // Transform that'll be used as a parent for the child nodes

		const auto meshesDataCount = modelNode->GetMeshesDataCount();
		if (meshesDataCount == 0)
		{
			auto entity = m_EntitiesManager.Instantiate().lock();
			baseTransform = entity->AddComponent<Transform>();
			baseTransform->SetLocalPosition(modelNode->GetPosition());
			baseTransform->SetLocalScale(modelNode->GetScale());
		}
		else
		{
			// TODO : consider changing logic to if meshCount <= 1, mesh (or nothing) on baseTransform. Else, no mesh on base, all meshes on other child.
			for (int i = 0; i < meshesDataCount; ++i)
			{
				auto entity = m_EntitiesManager.Instantiate().lock();

				auto transform = entity->AddComponent<Transform>();
				auto meshFilter = entity->AddComponent<MeshFilter>();
				auto meshRenderer = entity->AddComponent<MeshRenderer>();

				transform->SetLocalPosition(modelNode->GetPosition());
				transform->SetLocalScale(modelNode->GetScale());

				const auto material = m_ResourcesManager.FetchMaterial(modelNode->GetMaterialName(i));
				const auto mesh = m_ResourcesManager.FetchMesh(modelNode->GetMeshName(i));

				if (material == nullptr)
					ThrowLogicErrorExceptionWithLine("Material not found");

				if (mesh == nullptr)
					ThrowLogicErrorExceptionWithLine("Mesh not found");

				meshFilter->SetMesh(mesh);
				meshRenderer->SetMaterial(material);
				if (i == 0) { baseTransform = transform; }
			}
		}

		if (baseTransform == nullptr)
			ThrowLogicErrorExceptionWithLine("Base transform is null");


		baseTransform->SetParent(parentTransform);
		for (int i = 0; i < modelNode->GetChildCount(); ++i)
		{
			BuildVisualEntityFromNode(modelNode->GetChild(i), baseTransform);
		}
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/GameEvents.h"

#include"Nest/InputEnumerators.h"
#include"Nest/Input.h"
#include"Nest/Global.h"



namespace Nest
{
	GameEventInfo::GameEventInfo() : ev(GameEvent::None)
	{
	}



	GameEventInfo::GameEventInfo(GameEvent gameEvent) : ev(gameEvent)
	{
	}



	void GameEvents::Process()
	{
		SDL_Event e;
		while (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				PushEvent(GameEventInfo(GameEvent::Quit));
			}
			else
			{
				switch (e.type)
				{
					case SDL_MOUSEMOTION:
					{
						int mouseX = 0;
						int mouseY = 0;
						SDL_GetMouseState(&mouseX, &mouseY);

						Global->GetInput()->SetMousePosition(mouseX, mouseY);
					}
						break;


					case SDL_MOUSEBUTTONDOWN:
					{
						switch (e.button.button)
						{
							case SDL_BUTTON_LEFT:
								Global->GetInput()->MouseButtonPressed(MouseButton::Left);
								break;
							case SDL_BUTTON_RIGHT:
								Global->GetInput()->MouseButtonPressed(MouseButton::Right);
								break;

							default:
								break;
						}
					}
						break;


					case SDL_MOUSEBUTTONUP:
					{
						switch (e.button.button)
						{
							case SDL_BUTTON_LEFT:
								Global->GetInput()->MouseButtonReleased(MouseButton::Left);
								break;
							case SDL_BUTTON_RIGHT:
								Global->GetInput()->MouseButtonReleased(MouseButton::Right);
								break;

							default: break;
						}
					}
						break;


					case SDL_MOUSEWHEEL:
					{
						auto deltaX = e.wheel.x;
						auto deltaY = e.wheel.y;

						if (e.wheel.direction == SDL_MOUSEWHEEL_FLIPPED)
						{
							deltaX = 0 - deltaX;
							deltaY = 0 - deltaY;
						}

						Global->GetInput()->AddMouseScroll(deltaX, deltaY);
					}
						break;


					case SDL_KEYDOWN:
					{
						Global->GetInput()->KeyPressed(ConvertToKeyCode(e.key.keysym.sym));

						// TODO : find better place to put this event
						switch (e.key.keysym.sym)
						{
							case SDLK_ESCAPE:
								PushEvent(GameEventInfo(GameEvent::Quit));
								break;

							case SDLK_BACKSPACE:
								if (Global->GetInput()->IsOnTextInput())
								{
									Global->GetInput()->TextInputBackspace();
								}
								break;

							default:
								break;
						}
					}
						break;


					case SDL_KEYUP:
						Global->GetInput()->KeyReleased(ConvertToKeyCode(e.key.keysym.sym));
						break;


					case SDL_TEXTINPUT:
						Global->GetInput()->TextInputAppend(e.text.text);
						break;


					default:
						break;
				}
			}
		}
	}



	bool GameEvents::Poll(GameEventInfo& eventInfo)
	{
		if (!m_Events.empty())
		{
			// TODO : make a copy constructor?
			const auto e = PopEvent();
			eventInfo.ev = e.ev;
			return true;
		}

		return false;
	}



	void GameEvents::StartTextInput()
	{
		SDL_StartTextInput();
	}



	void GameEvents::StopTextInput()
	{
		SDL_StopTextInput();
	}



	void GameEvents::PushEvent(GameEventInfo eventInfo)
	{
		// TODO : limit max events queue size
		m_Events.push_back(eventInfo);
	}



	auto GameEvents::PopEvent() -> const GameEventInfo
	{
		const GameEventInfo e{ m_Events.front() };
		m_Events.pop_front();
		return e;
	}



	auto GameEvents::ConvertToKeyCode(SDL_Keycode sdlKeyCode) const -> KeyCode
	{
		switch (sdlKeyCode)
		{
			case SDLK_a: return (KeyCode::A);
			case SDLK_b: return (KeyCode::B);
			case SDLK_c: return (KeyCode::C);
			case SDLK_d: return (KeyCode::D);
			case SDLK_e: return (KeyCode::E);
			case SDLK_f: return (KeyCode::F);
			case SDLK_g: return (KeyCode::G);
			case SDLK_h: return (KeyCode::H);
			case SDLK_i: return (KeyCode::I);
			case SDLK_j: return (KeyCode::J);
			case SDLK_k: return (KeyCode::K);
			case SDLK_l: return (KeyCode::L);
			case SDLK_m: return (KeyCode::M);
			case SDLK_n: return (KeyCode::N);
			case SDLK_o: return (KeyCode::O);
			case SDLK_p: return (KeyCode::P);
			case SDLK_q: return (KeyCode::Q);
			case SDLK_r: return (KeyCode::R);
			case SDLK_s: return (KeyCode::S);
			case SDLK_t: return (KeyCode::T);
			case SDLK_u: return (KeyCode::U);
			case SDLK_v: return (KeyCode::V);
			case SDLK_w: return (KeyCode::W);
			case SDLK_x: return (KeyCode::X);
			case SDLK_y: return (KeyCode::Y);
			case SDLK_z: return (KeyCode::Z);

			case SDLK_LEFT: return (KeyCode::LeftArrow);
			case SDLK_RIGHT: return (KeyCode::RightArrow);
			case SDLK_UP: return (KeyCode::UpArrow);
			case SDLK_DOWN: return (KeyCode::DownArrow);

			case SDLK_LCTRL: return (KeyCode::LeftCtrl);
			case SDLK_RCTRL: return (KeyCode::RightCtrl);
			case SDLK_LALT: return (KeyCode::LeftAlt);
			case SDLK_RALT: return (KeyCode::RightAlt);
			case SDLK_LSHIFT: return (KeyCode::LeftShift);
			case SDLK_RSHIFT: return (KeyCode::RightShift);

			case SDLK_SPACE: return (KeyCode::Space);
			case SDLK_BACKSPACE: return (KeyCode::Backspace);
			case SDLK_RETURN: return (KeyCode::Enter);
			case SDLK_ESCAPE: return (KeyCode::Escape);

			case SDLK_0: return (KeyCode::Alpha0);
			case SDLK_1: return (KeyCode::Alpha1);
			case SDLK_2: return (KeyCode::Alpha2);
			case SDLK_3: return (KeyCode::Alpha3);
			case SDLK_4: return (KeyCode::Alpha4);
			case SDLK_5: return (KeyCode::Alpha5);
			case SDLK_6: return (KeyCode::Alpha6);
			case SDLK_7: return (KeyCode::Alpha7);
			case SDLK_8: return (KeyCode::Alpha8);
			case SDLK_9: return (KeyCode::Alpha9);

			case SDLK_F1: return (KeyCode::F1);
			case SDLK_F2: return (KeyCode::F2);
			case SDLK_F3: return (KeyCode::F3);
			case SDLK_F4: return (KeyCode::F4);
			case SDLK_F5: return (KeyCode::F5);
			case SDLK_F6: return (KeyCode::F6);
			case SDLK_F7: return (KeyCode::F7);
			case SDLK_F8: return (KeyCode::F8);
			case SDLK_F9: return (KeyCode::F9);
			case SDLK_F10: return (KeyCode::F10);
			case SDLK_F11: return (KeyCode::F11);
			case SDLK_F12: return (KeyCode::F12);

			case SDLK_BACKQUOTE: return (KeyCode::Backquote);

				// TODO : Log unknow keyCode
			default: return (KeyCode::Unknow);
		}
	}
}

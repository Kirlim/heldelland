#include"Nest/Eggs/YamlEntitiesLoader/YamlComponentLoader.h"

#include<yaml-cpp/yaml.h>

#include"Nest/Component.h"

#define NAME_TAG	"Name"



namespace Nest
{
	namespace Eggs
	{
		YamlComponentLoader::~YamlComponentLoader()
		{
			// Nothing here
		}



		void YamlComponentLoader::LoadName(Component& component, const YAML::Node& mainComponentNode)
		{
			if (mainComponentNode[NAME_TAG])
			{
				auto nameNode = mainComponentNode[NAME_TAG];
				component.SetName(nameNode.as<std::string>());
			}
		}
	}
}
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Eggs/YamlEntitiesLoader/YamlEntitiesLoader.h"

#include<stack>

#include<yaml-cpp/yaml.h>

#include"Nest/Exceptions/BadFileException.h"
#include"Nest/Exceptions/InvalidDataException.h"
#include"Nest/Exceptions/InvalidFileStructureException.h"
#include"Nest/Exceptions/NullArgumentException.h"

#include"Nest/IComponentsFactory.h"

#include"Nest/IEntity.h"
#include"Nest/EntitiesManager.h"
#include"Nest/EntitiesAndComponentsRemap.h"
#include"Nest/Eggs/YamlEntitiesLoader/YamlComponentsLoadersCollection.h"

#include"Nest/Transform.h"


#define ENTITY_TAG					"Entity"
#define ENTITY_NAME_TAG				"Name"
#define ENTITY_COMPONENTS_TAG		"Components"
#define ENTITY_CHILDREN_TAG			"Children"



namespace
{
	struct CreatedComponentContainer
	{
		const Nest::ComponentTypeId componentTypeId;
		Nest::ComponentOwnerReferenceBase* const componentOwnerReference;
		const YAML::Node node;

		CreatedComponentContainer(
			Nest::ComponentTypeId typeId,
			Nest::ComponentOwnerReferenceBase* const ownerReference,
			YAML::Node yamlNode
		) :
			componentTypeId(typeId),
			componentOwnerReference(ownerReference),
			node(yamlNode)
		{
		}
	};



	struct EntityNodeContainer
	{
		const YAML::Node entityNode;
		const std::weak_ptr<Nest::IEntity> parentEntity;

		EntityNodeContainer(YAML::Node node, std::weak_ptr<Nest::IEntity> parent) :
			entityNode(node), parentEntity(std::move(parent))
		{
		}
	};



	// Functions forward declarations

	std::shared_ptr<Nest::IEntity> LoadEntityAndInstantiateComponentsFromNode(
		Nest::EntitiesManager& entitiesManager,
		const EntityNodeContainer& entityNodeContainer,
		/*ref*/ std::vector<CreatedComponentContainer>& createdComponents,
		/*ref*/ std::stack<EntityNodeContainer>& entitiesNodesToLoad
	);

	void LoadEntityNameAttribute(const std::shared_ptr<Nest::IEntity>& entity, const YAML::Node& entityNode);

	void EnqueueChildrenToBeInstantiated(
		std::shared_ptr<Nest::IEntity> entity,
		const YAML::Node& entityNode,
		/*ref*/ std::stack<EntityNodeContainer>& entitiesNodesToLoad
	);

	void InstantiateEntityComponents(
		Nest::EntitiesManager& entitiesManager,
		std::shared_ptr<Nest::IEntity> entity,
		std::weak_ptr<Nest::IEntity> parentEntity,
		const YAML::Node& entityNode,
		/*ref*/ std::vector<CreatedComponentContainer>& createdComponents
	);

	bool IsComponentTypeTransform(Nest::ComponentOwnerReferenceBase* componentReference);

	void SetTransformParent(const std::weak_ptr<Nest::IEntity>& parentEntity, Nest::ComponentOwnerReferenceBase* componentReference);

	void InstantiateEntityComponentsFromNode(
		Nest::EntitiesManager& entitiesManager,
		const std::shared_ptr<Nest::IEntity>& entity,
		std::weak_ptr<Nest::IEntity> parentEntity,
		const YAML::Node& componentsNode,
		/*ref*/ std::vector<CreatedComponentContainer>& createdComponents
	);

	void LoadComponentsAttributes(
		const std::vector<CreatedComponentContainer>& createdComponents,
		const Nest::EntitiesAndComponentsRemap& entitiesAndComponentsRemap,
		const Nest::Eggs::YamlComponentsLoadersCollection& componentsLoadersCollection,
		const Nest::IResourcesManager& resourcesManager
	);
}



namespace Nest
{
	YamlEntitiesLoader::YamlEntitiesLoader(
		EntitiesManager& entitiesManager, const IResourcesManager& resourcesManager,
		std::unique_ptr<Eggs::YamlComponentsLoadersCollection>& componentsLoadersCollection
	) : m_EntitiesManager(entitiesManager), m_ResourcesManager(resourcesManager)
	{
		if (componentsLoadersCollection == nullptr)
			ThrowNullArgumentExceptionWithLine("Parameter componentsLoadersCollection cannot be null.");

		m_ComponentsLoadersCollection = std::move(componentsLoadersCollection);
	}



	std::weak_ptr<IEntity> YamlEntitiesLoader::LoadEntity(std::string fileName) const
	{
		YAML::Node mainNode;

		try
		{
			mainNode = YAML::LoadFile(fileName);
		}
		catch (YAML::BadFile&)
		{
			ThrowBadFileExceptionWithLine("Bad File Exception when loading Entity YAML File");
		}
		catch (YAML::ParserException& ex)
		{
			ThrowInvalidFileStructureExceptionWithLine("Invalid File Structure Exception [" + fileName + "]\n" + ex.msg);
		}


		if (!mainNode[ENTITY_TAG])
			ThrowInvalidDataExceptionWithLine("Missing [" ENTITY_TAG "] tag in the file [" + fileName + "]");


		const EntitiesAndComponentsRemap entitiesAndComponentsRemap{};
		std::stack<::EntityNodeContainer> entitiesNodesToLoad;
		std::vector<::CreatedComponentContainer> createdComponents{};

		entitiesNodesToLoad.push(::EntityNodeContainer{ mainNode[ENTITY_TAG], std::weak_ptr<IEntity>() });


		std::weak_ptr<IEntity> rootEntity{};
		while (!entitiesNodesToLoad.empty())
		{
			const auto entityNodeToLoad = entitiesNodesToLoad.top();
			entitiesNodesToLoad.pop();

			const auto loadedEntity = LoadEntityAndInstantiateComponentsFromNode(
				m_EntitiesManager, entityNodeToLoad,
				/*ref*/ createdComponents,
				/*ref*/ entitiesNodesToLoad
			);

			if (rootEntity.expired())
				rootEntity = loadedEntity;
		}

		LoadComponentsAttributes(createdComponents, entitiesAndComponentsRemap, *m_ComponentsLoadersCollection, m_ResourcesManager);


		return rootEntity;
	}
}



namespace
{
	std::shared_ptr<Nest::IEntity> LoadEntityAndInstantiateComponentsFromNode(
		Nest::EntitiesManager& entitiesManager,
		const EntityNodeContainer& entityNodeContainer,
		/*ref*/ std::vector<CreatedComponentContainer>& createdComponents,
		/*ref*/ std::stack<EntityNodeContainer>& entitiesNodesToLoad
	)
	{
		// Load structure
		auto entity = entitiesManager.Instantiate().lock();
		const auto entityNode = entityNodeContainer.entityNode;

		LoadEntityNameAttribute(entity, entityNode);
		InstantiateEntityComponents(entitiesManager, entity, entityNodeContainer.parentEntity, /*ref*/ entityNode, /*ref*/ createdComponents);
		EnqueueChildrenToBeInstantiated(entity, entityNode, /*ref*/ entitiesNodesToLoad);

		return entity;
	}



	void LoadEntityNameAttribute(const std::shared_ptr<Nest::IEntity>& entity, const YAML::Node& entityNode)
	{
		if (entityNode[ENTITY_NAME_TAG])
		{
			entity->SetName(entityNode[ENTITY_NAME_TAG].as<std::string>());
		}
	}



	void EnqueueChildrenToBeInstantiated(
		std::shared_ptr<Nest::IEntity> entity,
		const YAML::Node& entityNode,
		std::stack<EntityNodeContainer>& entitiesNodesToLoad
	)
	{
		if (entityNode[ENTITY_CHILDREN_TAG])
		{
			auto& childrenNode = entityNode[ENTITY_CHILDREN_TAG];

			if (!childrenNode.IsSequence())
				ThrowInvalidDataExceptionWithLine("Found a [" ENTITY_CHILDREN_TAG "] node that is not a sequence.");

			for (const auto& childNode : childrenNode)
			{
				entitiesNodesToLoad.push(EntityNodeContainer{ childNode, entity });
			}
		}
	}



	void InstantiateEntityComponents(
		Nest::EntitiesManager& entitiesManager,
		std::shared_ptr<Nest::IEntity> entity,
		std::weak_ptr<Nest::IEntity> parentEntity,
		const YAML::Node& entityNode,
		/*ref*/ std::vector<CreatedComponentContainer>& createdComponents
	)
	{
		if (entityNode[ENTITY_COMPONENTS_TAG])
		{
			auto& componentsNode = entityNode[ENTITY_COMPONENTS_TAG];

			if (!componentsNode.IsSequence())
				ThrowInvalidDataExceptionWithLine("Found components list is not a sequence.");

			InstantiateEntityComponentsFromNode(entitiesManager, entity, parentEntity, componentsNode, createdComponents);
		}
	}



	bool IsComponentTypeTransform(Nest::ComponentOwnerReferenceBase* componentReference)
	{
		return dynamic_cast<Nest::Transform*>(componentReference->Get()) != nullptr;
	}



	void SetTransformParent(const std::weak_ptr<Nest::IEntity>& parentEntity, Nest::ComponentOwnerReferenceBase* componentReference)
	{
		if (!parentEntity.expired())
		{
			const auto parentTransform = parentEntity.lock()->GetComponent<Nest::Transform>();
			auto thisTransform = DoCast<Nest::Transform>(componentReference->Get());

			NestAssert(parentTransform != nullptr);
			NestAssert(thisTransform != nullptr);

			thisTransform->SetParent(parentTransform);
		}
	}



	void InstantiateEntityComponentsFromNode(
		Nest::EntitiesManager& entitiesManager,
		const std::shared_ptr<Nest::IEntity>& entity,
		std::weak_ptr<Nest::IEntity> parentEntity,
		const YAML::Node& componentsNode,
		/*ref*/ std::vector<CreatedComponentContainer>& createdComponents
	)
	{
		for (const auto& componentNode : componentsNode)
		{
			if (componentNode.size() != 1)
				ThrowInvalidDataExceptionWithLine("Too many entries in a node of Components sequence. There should be only one");


			auto node = componentNode.begin();

			const auto componentTypeName = node->first.as<std::string>();
			const auto componentTypeId = entitiesManager.GetComponentsFactory()->GetFactoryTypeId(componentTypeName);
			const auto componentReference = entity->AddComponent(componentTypeName);

			createdComponents.emplace_back(CreatedComponentContainer{ componentTypeId, componentReference, node->second });

			if (IsComponentTypeTransform(componentReference))
			{
				SetTransformParent(parentEntity, componentReference);
			}
		}
	}



	void LoadComponentsAttributes(
		const std::vector<CreatedComponentContainer>& createdComponents,
		const Nest::EntitiesAndComponentsRemap& entitiesAndComponentsRemap,
		const Nest::Eggs::YamlComponentsLoadersCollection& componentsLoadersCollection,
		const Nest::IResourcesManager& resourcesManager
	)
	{
		for (const auto& componentContainer : createdComponents)
		{
			const auto componentTypeId = componentContainer.componentTypeId;
			const auto componentReference = componentContainer.componentOwnerReference;
			const auto componentNode = componentContainer.node;

			auto loader = componentsLoadersCollection.GetLoader(componentTypeId);
			loader->LoadAttributes(*(componentReference->Get()), componentNode, entitiesAndComponentsRemap, resourcesManager);
		}
	}
}

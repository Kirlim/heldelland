//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Eggs/YamlEntitiesLoader/Loaders/YamlMeshRendererComponentLoader.h"

#include<yaml-cpp/yaml.h>

#include"Nest/Cast.h"
#include"Nest/Exceptions/InvalidDataException.h"
#include"Nest/Exceptions/LogicErrorException.h"

#include"Nest/IResourcesManager.h"
#include"Nest/MeshRenderer.h"

#include"Nest/Eggs/YamlEntitiesLoader/Loaders/YamlResourceReferenceLoader.h"

#define MATERIAL_TAG                                "Material"
#define IS_GUI_FLAG_TAG                             "IsGui"



namespace Nest
{
	namespace Eggs
	{
		void YamlMeshRendererComponentLoader::LoadAttributes(
			Component& component, const YAML::Node& mainComponentNode,
			const EntitiesAndComponentsRemap& entitiesAndComponentsRemap, const IResourcesManager& resourcesManager
		)
		{
			LoadName(component, mainComponentNode);

			auto& meshRenderer = DoCast<MeshRenderer>(component);
			LoadMaterial(meshRenderer, mainComponentNode, resourcesManager);
			LoadIsGuiFlag(meshRenderer, mainComponentNode);
		}



		void YamlMeshRendererComponentLoader::LoadMaterial(
			MeshRenderer& meshRenderer, const YAML::Node& mainComponentNode, const IResourcesManager& resourcesManager
		)
		{
			if (mainComponentNode[MATERIAL_TAG])
			{
				const auto resourceReferenceInformation =
					YamlEntitiesLoader::LoadResourceReference(mainComponentNode[MATERIAL_TAG]);

				if (resourceReferenceInformation.resourceType != ResourceType::Material)
					ThrowInvalidDataExceptionWithLine("Mesh Renderer material resource must be a Material");

				const auto material = resourcesManager.FetchMaterial(resourceReferenceInformation.resourceName);
				if (material == nullptr)
					ThrowLogicErrorExceptionWithLine("Mesh Renderer material resource was not found in the given Resources Manager");

				meshRenderer.SetMaterial(material);
			}
		}



		void YamlMeshRendererComponentLoader::LoadIsGuiFlag(
			MeshRenderer& meshRenderer, const YAML::Node& mainComponentNode
		)
		{
			if (mainComponentNode[IS_GUI_FLAG_TAG])
			{
				const auto flagValue = mainComponentNode[IS_GUI_FLAG_TAG].as<bool>();
				meshRenderer.SetGuiFlag(flagValue);
			}
		}
	}
}

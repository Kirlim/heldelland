
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Eggs/YamlEntitiesLoader/Loaders/YamlMeshFilterComponentLoader.h"

#include<yaml-cpp/yaml.h>

#include"Nest/MeshFilter.h"
#include"Nest/IResourcesManager.h"
#include"Nest/Eggs/YamlEntitiesLoader/Loaders/YamlResourceReferenceLoader.h"
#include"Nest/Cast.h"

#define MESH_TAG    "Mesh"



namespace Nest
{
	namespace Eggs
	{
		void YamlMeshFilterComponentLoader::LoadAttributes(
			Component& component, const YAML::Node& mainComponentNode,
			const EntitiesAndComponentsRemap& entitiesAndComponentsRemap, const IResourcesManager& resourcesManager
		)
		{
			auto& meshFilter = DoCast<MeshFilter>(component);

			LoadName(component, mainComponentNode);

			if(mainComponentNode[MESH_TAG])
				LoadMeshResource(meshFilter, mainComponentNode[MESH_TAG], resourcesManager);
		}



		void YamlMeshFilterComponentLoader::LoadMeshResource(
			MeshFilter& meshFilter, const YAML::Node& meshNode, const IResourcesManager& resourcesManager
		)
		{
			const auto resourceReference = YamlEntitiesLoader::LoadResourceReference(meshNode);
			const auto meshResource = resourcesManager.FetchMesh(resourceReference.resourceName);
			meshFilter.SetMesh(meshResource);
		}
	}
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Eggs/YamlEntitiesLoader/Loaders/YamlTransformComponentLoader.h"

#include<cmath>

#include<yaml-cpp/yaml.h>

#include"Nest/Cast.h"
#include"Nest/Eggs/YamlEntitiesLoader/Loaders/YamlVector3fLoader.h"
#include"Nest/Transform.h"

#define LOCAL_POSITION_TAG		"LocalPosition"
#define LOCAL_ROTATION_TAG		"LocalRotation"
#define LOCAL_SCALE_TAG			"LocalScale"



namespace Nest
{
	namespace Eggs
	{
		YamlTransformComponentLoader::~YamlTransformComponentLoader()
		{
			// Nothing here
		}



		void YamlTransformComponentLoader::LoadAttributes(
			Component& component, const YAML::Node& mainComponentNode, 
			const EntitiesAndComponentsRemap& entitiesAndComponentsRemap, const IResourcesManager& resourcesManager
		)
		{
			if (mainComponentNode.IsNull()) return;

			auto& transform = DoCast<Transform>(component);

			LoadName(component, mainComponentNode);

			if (mainComponentNode[LOCAL_POSITION_TAG]) LoadLocalPosition(transform, mainComponentNode[LOCAL_POSITION_TAG]);
			if (mainComponentNode[LOCAL_ROTATION_TAG]) LoadLocalRotation(transform, mainComponentNode[LOCAL_ROTATION_TAG]);
			if (mainComponentNode[LOCAL_SCALE_TAG]) LoadLocalScale(transform, mainComponentNode[LOCAL_SCALE_TAG]);
		}



		void YamlTransformComponentLoader::LoadLocalPosition(Transform& transform, const YAML::Node& localPositionNode)
		{
			const auto localPosition = YamlEntitiesLoader::LoadVector3f(localPositionNode);
			transform.SetLocalPosition(localPosition);
		}



		void YamlTransformComponentLoader::LoadLocalRotation(Transform& transform, const YAML::Node& localRotationNode)
		{
			const auto localRotation = YamlEntitiesLoader::LoadVector3f(localRotationNode);
			transform.SetLocalRotation(Quaternion<float>::FromEuler(localRotation.y, localRotation.z, localRotation.x));
		}



		void YamlTransformComponentLoader::LoadLocalScale(Transform& transform, const YAML::Node& localScaleNode)
		{
			const auto localScale = YamlEntitiesLoader::LoadVector3f(localScaleNode);
			transform.SetLocalScale(localScale);
		}
	}
}

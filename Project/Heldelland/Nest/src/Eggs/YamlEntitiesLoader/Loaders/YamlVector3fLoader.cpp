//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Eggs/YamlEntitiesLoader/Loaders/YamlVector3fLoader.h"

#include<yaml-cpp/yaml.h>

#include"Nest/Exceptions/InvalidDataException.h"
#include"Nest/Vector3.h"



namespace Nest
{
	namespace Eggs
	{
		namespace YamlEntitiesLoader
		{
			Vector3f LoadVector3f(const YAML::Node& vectorNode)
			{
				if (vectorNode.IsNull())
					ThrowInvalidDataExceptionWithLine("A null Vector3 yaml node has been attempted to be read.");

				if (!vectorNode["X"])
					ThrowInvalidDataExceptionWithLine("A Vector3 yaml node does not have the X field.");

				if (!vectorNode["Y"])
					ThrowInvalidDataExceptionWithLine("A Vector3 yaml node does not have the Y field.");

				if (!vectorNode["Z"])
					ThrowInvalidDataExceptionWithLine("A Vector3 yaml node does not have the Z field.");

				for (auto& node : vectorNode)
				{
					if (node.first.as<std::string>() != "X" && node.first.as<std::string>() != "Y" && node.first.as<std::string>() != "Z")
						ThrowInvalidDataExceptionWithLine("Invalid [" + node.first.as<std::string>() + "] field in Vector3 yaml node.");
				}

				return Vector3f{ vectorNode["X"].as<float>(), vectorNode["Y"].as<float>(), vectorNode["Z"].as<float>() };
			}
		}



		void operator>>(const YAML::Node& node, Vector3<float>& vector)
		{
			vector = YamlEntitiesLoader::LoadVector3f(node);
		}
	}
}

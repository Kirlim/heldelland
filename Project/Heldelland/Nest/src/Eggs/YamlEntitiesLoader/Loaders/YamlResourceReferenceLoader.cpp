//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Eggs/YamlEntitiesLoader/Loaders/YamlResourceReferenceLoader.h"

#include<yaml-cpp/yaml.h>

#include"Nest/Exceptions/InvalidDataException.h"
#include"Nest/Exceptions/InvalidFileStructureException.h"
#include"Nest/Exceptions/InvalidArgumentException.h"

#define AMOUNT_OF_RESOURCE_REFERENCE_KEYS					2



namespace Nest
{
	namespace Eggs
	{
		namespace YamlEntitiesLoader
		{
			void ValidateResourceReferenceNode(const YAML::Node& resourceReferenceNode);
			ResourceReference LoadResourceReferenceFromNode(const YAML::Node& resourceReferenceNode);
			ResourceType GetResourceTypeFromValue(std::string value);
			std::string GetResourceNameFromValue(std::string value);



			ResourceReference::ResourceReference(const ResourceType resourceType, const std::string resourceName) :
				resourceType(resourceType), resourceName(resourceName)
			{
				// Nothing here
			}



			ResourceReference LoadResourceReference(const YAML::Node& resourceReferenceNode)
			{
				ValidateResourceReferenceNode(resourceReferenceNode);
				return LoadResourceReferenceFromNode(resourceReferenceNode);
			}



			void ValidateResourceReferenceNode(const YAML::Node& resourceReferenceNode)
			{
				if (resourceReferenceNode.IsNull())
					ThrowInvalidDataExceptionWithLine("Resource Reference node cannot be null.");

				if (!resourceReferenceNode.IsMap())
					ThrowInvalidFileStructureExceptionWithLine("Resource Reference must be a map.");

				if (resourceReferenceNode.size() != AMOUNT_OF_RESOURCE_REFERENCE_KEYS)
					ThrowInvalidFileStructureExceptionWithLine(
					"Resource Reference node must have exactly " +
					std::to_string(AMOUNT_OF_RESOURCE_REFERENCE_KEYS) + " keys." +
					"(Found " + std::to_string(resourceReferenceNode.size()) + ")");
			}



			ResourceReference LoadResourceReferenceFromNode(const YAML::Node& resourceReferenceNode)
			{
				auto resourceName = std::string{};
				auto resourceType = ResourceType::Undefined;

				for (auto it = resourceReferenceNode.begin(); it != resourceReferenceNode.end(); ++it)
				{
					const auto key = it->first.as<std::string>();
					const auto value = it->second.as<std::string>();

					if (key == "Type")
					{
						resourceType = GetResourceTypeFromValue(value);
					}
					else if (key == "Name")
					{
						resourceName = GetResourceNameFromValue(value);
					}
					else
					{
						ThrowInvalidFileStructureExceptionWithLine("Found incorrect key '" + key + "' while reading resource information.");
					}
				}

				return ResourceReference{ resourceType, resourceName };
			}



			ResourceType GetResourceTypeFromValue(std::string value)
			{
				try
				{
					return GetResourceTypeFromString(value);
				}
				catch (InvalidArgumentException&)
				{
					ThrowInvalidDataExceptionWithLine("Failed to translate string '" + value + "' to a resource type.");
				}
			}



			std::string GetResourceNameFromValue(std::string value)
			{
				return value;
			}
		}
	}
}

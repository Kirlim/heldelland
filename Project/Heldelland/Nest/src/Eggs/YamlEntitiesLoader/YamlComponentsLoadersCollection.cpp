//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Eggs/YamlEntitiesLoader/YamlComponentsLoadersCollection.h"

#include"Nest/Exceptions/LogicErrorException.h"
#include"Nest/Exceptions/NullArgumentException.h"
#include"Nest/Eggs/YamlEntitiesLoader/YamlComponentLoader.h"



namespace Nest
{
	namespace Eggs
	{
		YamlComponentsLoadersCollection::YamlComponentsLoadersCollection() :
			m_Loaders(std::make_unique<ContainerType>())
		{
			// Nothing here
		}



		void YamlComponentsLoadersCollection::Register(const ComponentTypeId componentTypeId, std::unique_ptr<YamlComponentLoader>& componentLoader)
		{
			if (componentLoader == nullptr)
				ThrowNullArgumentExceptionWithLine("Component Loader cannot be null.");

			if (m_Loaders->count(componentTypeId) > 0)
				ThrowLogicErrorExceptionWithLine("Type id of [" + std::to_string(componentTypeId) + "] is already registered.");

			m_Loaders->emplace(std::make_pair(componentTypeId, std::move(componentLoader)));
		}



		void YamlComponentsLoadersCollection::Unregister(const ComponentTypeId componentTypeId)
		{
			if (m_Loaders->count(componentTypeId) == 0)
				ThrowLogicErrorExceptionWithLine("Type id of [" + std::to_string(componentTypeId) + "] is not registered.");

			m_Loaders->erase(componentTypeId);
		}



		bool YamlComponentsLoadersCollection::IsRegistered(const ComponentTypeId componentTypeId) const
		{
			return (m_Loaders->count(componentTypeId) > 0);
		}



		YamlComponentLoader* YamlComponentsLoadersCollection::GetLoader(const ComponentTypeId componentTypeId) const
		{
			if (m_Loaders->count(componentTypeId) == 0)
				ThrowLogicErrorExceptionWithLine("Type id of [" + std::to_string(componentTypeId) + "] is not registered.");

			return m_Loaders->at(componentTypeId).get();
		}
	}
}

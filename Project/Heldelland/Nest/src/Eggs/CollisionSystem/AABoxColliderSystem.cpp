//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Eggs/CollisionSystem/AABoxColliderSystem.h"

#include<algorithm>

#include"Nest/Exceptions/InvalidArgumentException.h"
#include"Nest/Exceptions/LogicErrorException.h"

#include"Nest/Ray.h"



namespace Nest::Eggs::CollisionSystem
{
	void AABoxColliderSystem::Register(ColliderId colliderId, AABBColliderInfo colliderInfo)
	{
		if (GetColliderType(colliderId) != ColliderIdTraits::AABBType)
			ThrowInvalidArgumentExceptionWithLine("Collider Id is not of Axis Aligned Bounding Box");

		if (IsRegistered(colliderId))
			ThrowLogicErrorExceptionWithLine("Received colliderId is already registered");

		collidersIds.emplace_back(colliderId);
		collidersInfo.emplace_back(colliderInfo);

		colliders.emplace_back();
		auto& collider = colliders[colliders.size() - 1];
		collider.min = colliderInfo.center - colliderInfo.halfSize;
		collider.max = colliderInfo.center + colliderInfo.halfSize;
	}



	void AABoxColliderSystem::Unregister(ColliderId colliderId)
	{
		const auto findResult = std::find_if(
			std::begin(collidersIds), std::end(collidersIds),
			[colliderId](const ColliderId& id) { return (colliderId == id); }
		);

		if (findResult == std::end(collidersIds))
			ThrowLogicErrorExceptionWithLine("Cannot unregister a collider whose identifier is not registered");


		const auto index = findResult - std::begin(collidersIds);

		collidersIds.erase(findResult);
		collidersInfo.erase(std::begin(collidersInfo) + index);
		colliders.erase(std::begin(colliders) + index);
	}



	bool AABoxColliderSystem::IsRegistered(ColliderId colliderId) const
	{
		const auto findResult = std::find_if(
			std::begin(collidersIds), std::end(collidersIds),
			[colliderId](const ColliderId& id) { return (colliderId == id); }
		);

		return (findResult != std::end(collidersIds));
	}



	bool AABoxColliderSystem::IsApplyTranformEnqueued(ColliderId colliderId) const
	{
		const auto findResult = std::find_if(
			std::begin(applyTransformInformation.colliderIds), std::end(applyTransformInformation.colliderIds),
			[colliderId](const ColliderId& id) { return (colliderId == id); }
		);

		return (findResult != std::end(applyTransformInformation.colliderIds));
	}



	void AABoxColliderSystem::AppendCollidersIndexesToVectorPermissive(
		const std::vector<ColliderId>& ids, std::vector<int>& out_Indexes
	) const
	{
		for (const auto& id : ids)
		{
			const auto findResult = std::find_if(
				std::begin(collidersIds), std::end(collidersIds),
				[&id](const ColliderId& registeredId) { return (id == registeredId); }
			);

			if (findResult != std::end(collidersIds))
				out_Indexes.emplace_back(findResult - std::begin(collidersIds));
			else
				out_Indexes.emplace_back(-1);
		}
	}



	void AABoxColliderSystem::EnqueueApplyTransformPermissive(ColliderId colliderId, const Matrix4<float>& transformation)
	{
		applyTransformInformation.colliderIds.emplace_back(colliderId);
		applyTransformInformation.transformations.emplace_back(transformation);
	}



	void AABoxColliderSystem::ApplyEnqueuedTransforms()
	{
		static std::vector<int> indexes{};

		if (applyTransformInformation.colliderIds.size() == 0)
			return;


		AppendCollidersIndexesToVectorPermissive(applyTransformInformation.colliderIds, /*out*/ indexes);

		for (int i = 0; i < indexes.size(); ++i)
		{
			if (indexes[i] < 0)
				continue;

			const auto& index = indexes[i];
			const auto& colliderInfo = collidersInfo[index];
			auto& collider = colliders[index];

			const auto translatedCenter = applyTransformInformation.transformations[i] * colliderInfo.center;

			collider.min = translatedCenter - colliderInfo.halfSize;
			collider.max = translatedCenter + colliderInfo.halfSize;
		}


		applyTransformInformation.colliderIds.clear();
		applyTransformInformation.transformations.clear();
	}



	void AABoxColliderSystem::RaycastTime(Ray ray, std::vector<RaycastHitTime>& out_AppendHits) const
	{
		float enterTime, exitTime;
		float enterTimeAux, exitTimeAux;


		for (decltype(colliders.size()) i = 0; i < colliders.size(); ++i)
		{
			const auto& collider = colliders[i];


			if (std::abs(ray.direction.x) < 1.0e-9f)
			{
				if (ray.origin.x < collider.min.x || ray.origin.x > collider.max.x)
					continue;

				enterTime = std::numeric_limits<float>::min();
				exitTime = std::numeric_limits<float>::max();
			}
			else
			{
				enterTime = (collider.min.x - ray.origin.x) / ray.direction.x;
				exitTime = (collider.max.x - ray.origin.x) / ray.direction.x;

				if (enterTime > exitTime)
					std::swap(enterTime, exitTime);
			}


			if (std::abs(ray.direction.y) < 1.0e-9f)
			{
				if (ray.origin.y < collider.min.y || ray.origin.y > collider.max.y)
					continue;
			}
			else
			{
				enterTimeAux = (collider.min.y - ray.origin.y) / ray.direction.y;
				exitTimeAux = (collider.max.y - ray.origin.y) / ray.direction.y;

				if (enterTimeAux > exitTimeAux)
					std::swap(enterTimeAux, exitTimeAux);

				enterTime = std::max(enterTime, enterTimeAux);
				exitTime = std::min(exitTime, exitTimeAux);
			}


			if (std::abs(ray.direction.z) < 1.0e-9f)
			{
				if (ray.origin.z < collider.min.z || ray.origin.z > collider.max.z)
					continue;
			}
			else
			{
				enterTimeAux = (collider.min.z - ray.origin.z) / ray.direction.z;
				exitTimeAux = (collider.max.z - ray.origin.z) / ray.direction.z;

				if (enterTimeAux > exitTimeAux)
					std::swap(enterTimeAux, exitTimeAux);

				enterTime = std::max(enterTime, enterTimeAux);
				exitTime = std::min(exitTime, exitTimeAux);
			}


			if (enterTime <= exitTime)
			{
				out_AppendHits.emplace_back(RaycastHitTime{ collidersIds[i], enterTime });
			}
		}
	}
}

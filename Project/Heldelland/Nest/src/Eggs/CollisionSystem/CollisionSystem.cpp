//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Eggs/CollisionSystem/CollisionSystem.h"

#include<Nest/Eggs/CollisionSystem/AABoxColliderSystem.h>



namespace Nest::Eggs::CollisionSystem
{
	template class CollisionSystem<AABoxColliderSystem>;
}

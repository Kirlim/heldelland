
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/DirectionalLightComponent.h"

#include"Nest/Quaternion.h"
#include"Nest/Transform.h"
#include"Nest/IEntity.h"



namespace Nest
{
    DirectionalLightComponent::DirectionalLightComponent() :
        m_Color(1.0f, 1.0f, 1.0f)
    {
        // Nothing here
    }



    DirectionalLightComponent::~DirectionalLightComponent()
    {
        // Nothing here
    }



    void DirectionalLightComponent::SetColor(Vector3<float> color)
    {
        m_Color = color;
    }



    Vector3<float> DirectionalLightComponent::GetColor() const
    {
        return(m_Color);
    }



    Vector3<float> DirectionalLightComponent::GetDirection() const
    {
        auto transform = GetOwner()->GetComponent<Transform>();
        if (transform == nullptr) return(Vector3<float>(0.0f, 0.0f, -1.0f));

        Vector3<float> forward = Vector3<float>(0.0f, 0.0f, -1.0f);
        auto result = transform->GetRotation().RotateVector(forward);
        result.Normalize();

        return(result);
    }



    void DirectionalLightComponent::OnDestroy()
    {
        // Nothing here
    }
}

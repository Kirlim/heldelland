
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/ComponentOwnerReferenceBase.h"

#include"Nest/ComponentReference.h"
#include"Nest/Component.h"



namespace Nest
{
	ComponentOwnerReferenceBase::ComponentOwnerReferenceBase(ComponentTypeId componentTypeId) :
		typeId(componentTypeId)
	{
		// Nothing here
	}



	Component* ComponentOwnerReferenceBase::operator->() const
	{
		return(Get());
	}



	void ComponentOwnerReferenceBase::UpdateUnsafePointer(ComponentReferenceBase& reference, Component* component)
	{
		reference.UpdateUnsafePointer(component);
	}
}

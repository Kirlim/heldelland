//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Engine.h"
#include"Nest/NestAssert.h"

#include"Nest/IResourcesManager.h"
#include"Nest/RenderingManager.h"
#include"Nest/RenderingModule.h"
#include"Nest/Global.h"
#include"Nest/Input.h"
#include"Nest/IModelLoader.h"
#include"Nest/ComponentsContainers.h"
#include"Nest/ComponentsFactory.h"

#include"Nest/World.h"

// TODO : put these includes somewhere else after deciding the best place to put the resources factory method
#include"Nest/Metadata/FontMetadata.h"
#include"Nest/Metadata/MaterialMetadata.h"
#include"Nest/Metadata/ModelMetadata.h"
#include"Nest/Metadata/ShaderProgramMetadata.h"
#include"Nest/Metadata/TextureMetadata.h"
#include"Nest/Exceptions/LogicErrorException.h"



namespace Nest
{
	NestEngine::NestEngine(
		int fps,
		std::unique_ptr<IResourcesManager> resourcesManager,
		std::unique_ptr<RenderingManager> renderingManager,
		std::unique_ptr<RenderingModule> renderingModule,
		std::unique_ptr<IModelLoader> modelLoader
	) :
		m_DeltaTime(0.0),
		m_FixedDeltaTime(1.0f / float(fps)),
		m_AccumulatedFixedTime(0.0),
		m_GameEvents()
	{
		NestAssert(resourcesManager != nullptr);
		NestAssert(renderingManager != nullptr);
		NestAssert(renderingModule != nullptr);
		NestAssert(modelLoader != nullptr);

		m_RenderingModule = std::move(renderingModule);
		m_ModelLoader = std::move(modelLoader);

		auto componentsContainers = std::make_unique<ComponentsContainers>();
		auto componentsFactory = std::make_unique<ComponentsFactory>();
		auto input = std::make_unique<Input>();
		auto metadataManager = std::make_unique<MetadataManager>(
			[&](const ResourceType resourceType, const std::string resourceName)
			{
				std::unique_ptr<IMetadataInformation> metadataInfo;

				switch (resourceType)
				{
					case ResourceType::Font: metadataInfo = std::make_unique<FontMetadata>(resourceName);
						break;
					case ResourceType::Material: metadataInfo = std::make_unique<MaterialMetadata>(resourceName);
						break;
					case ResourceType::Model: metadataInfo = std::make_unique<ModelMetadata>(resourceName, *m_ModelLoader.get(), m_RenderingModule->GetGameRenderer());
						break;
					case ResourceType::Shader: metadataInfo = std::make_unique<ShaderProgramMetadata>(resourceName);
						break;
					case ResourceType::Texture: metadataInfo = std::make_unique<TextureMetadata>(resourceName);
						break;
					default: ThrowLogicErrorExceptionWithLine("Unknown resource type for instantiating");
						break;
				}

				return (metadataInfo);
			});
		auto prefabComponentsFactory = std::make_unique<ComponentsFactory>();


		// Prepare global access variable.
		Global = new GlobalAccess();

		// Prepare other access modules
		Global->SetEngine(this);
		Global->SetInput(std::move(input));
		Global->SetMetadataManager(std::move(metadataManager));

		Global->GetInput()->SetStartTextInputMethod([&]() { m_GameEvents.StartTextInput(); });
		Global->GetInput()->SetStopTextInputMethod([&]() { m_GameEvents.StopTextInput(); });
	}



	void NestEngine::Execute()
	{
		m_LastTimeUpdate = std::chrono::system_clock::now();

		while (!m_Quit)
		{
			// Will update fps and frame duration stuff related to time
			UpdateTimeVariables();

			// Execute accumulated fixed frames
			m_AccumulatedFixedTime += m_DeltaTime;
			int accumulatedFixedFrames = int(m_AccumulatedFixedTime / m_FixedDeltaTime);
			m_AccumulatedFixedTime -= m_FixedDeltaTime * float(accumulatedFixedFrames);

			if (accumulatedFixedFrames > 30) accumulatedFixedFrames = 30; // TODO : more legibility (using max). Dont use magic value.

			for (int i = 0; i < accumulatedFixedFrames; ++i)
			{
				FixedUpdate(m_FixedDeltaTime);
			}
			Update(m_DeltaTime);

			SDL_Delay(1);
		}
	}



	RenderingModule* NestEngine::GetRenderingModule() const
	{
		return (m_RenderingModule.get());
	}



	void NestEngine::AddWorld(WorldId worldId, std::unique_ptr<World>& world)
	{
		m_WorldsManager.AddWorld(worldId, world);
	}



	void NestEngine::RemoveWorld(WorldId worldId)
	{
		m_WorldsManager.RemoveWorld(worldId);
	}



	World* NestEngine::GetWorld(WorldId worldId) const
	{
		return m_WorldsManager.GetWorld(worldId);
	}



	void NestEngine::FixedUpdate(float fixedDeltaTime)
	{
		Global->GetInput()->Refresh();
		m_GameEvents.Process();

		GameEventInfo gameEventInfo;
		while (m_GameEvents.Poll(gameEventInfo))
		{
			if (gameEventInfo.ev == GameEvent::Quit)
			{
				m_Quit = true;
				return;
			}
		}

		for (const auto& world : m_WorldsManager)
		{
			const auto worldSystemsManager = world.world->GetSystemsManager();
			worldSystemsManager->FixedUpdate(fixedDeltaTime);
		}
	}



	void NestEngine::Update(float deltaTime)
	{
		Global->GetInput()->Refresh();
		m_GameEvents.Process();

		GameEventInfo gameEventInfo;
		while (m_GameEvents.Poll(gameEventInfo))
		{
			if (gameEventInfo.ev == GameEvent::Quit)
			{
				m_Quit = true;
				return;
			}
		}


		for (const auto& world : m_WorldsManager)
		{
			const auto worldSystemsManager = world.world->GetSystemsManager();
			worldSystemsManager->Update(deltaTime);
		}
	}



	void NestEngine::UpdateTimeVariables()
	{
		const auto currentTime = std::chrono::system_clock::now();
		auto timeDiff = std::chrono::duration<double>(currentTime - m_LastTimeUpdate);

		m_DeltaTime = float(timeDiff.count());
		m_LastTimeUpdate = currentTime;
	}



	NestEngine::~NestEngine()
	{
		if (Global != nullptr)
		{
			delete Global;
		}
	}
}

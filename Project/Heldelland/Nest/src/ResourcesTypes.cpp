
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/ResourcesTypes.h"

#include"Nest/Exceptions/InvalidArgumentException.h"
#include"Nest/Helpers/StringHelper.h"



namespace Nest
{
	ResourceType GetResourceTypeFromString(const std::string resourceTypeString)
	{
		static const std::string font{ "FONT" };
		static const std::string material{ "MATERIAL" };
		static const std::string mesh{ "MESH" };
		static const std::string model{ "MODEL" };
		static const std::string texture{ "TEXTURE" };
		static const std::string shader{ "SHADER" };

		if (resourceTypeString.empty())
			ThrowInvalidArgumentExceptionWithLine("Resource type string cannot be empty");

		ResourceType resourceType{ ResourceType::Undefined };

		if (Helpers::UpperCompare(resourceTypeString, shader))
		{
			resourceType = ResourceType::Shader;
		}
		else if (Helpers::UpperCompare(resourceTypeString, material))
		{
			resourceType = ResourceType::Material;
		}
		else if (Helpers::UpperCompare(resourceTypeString, texture))
		{
			resourceType = ResourceType::Texture;
		}
		else if (Helpers::UpperCompare(resourceTypeString, mesh))
		{
			resourceType = ResourceType::Mesh;
		}
		else if (Helpers::UpperCompare(resourceTypeString, model))
		{
			resourceType = ResourceType::Model;
		}
		else if (Helpers::UpperCompare(resourceTypeString, font))
		{
			resourceType = ResourceType::Font;
		}
		else
		{
			ThrowInvalidArgumentExceptionWithLine("Resource type string [" + resourceTypeString + "] does not refer to a known resource type");
		}

		return(resourceType);
	}
}
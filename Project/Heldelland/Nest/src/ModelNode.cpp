
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/ModelNode.h"



namespace Nest
{
    ModelNode::ModelNode() :
        m_Position(Vector3f(0.0f, 0.0f, 0.0f)),
        m_Scale(Vector3f(1.0f, 1.0f, 1.0f))
    {
        // Nothing here
    }



    ModelNode::~ModelNode()
    {
        // Nothing here
    }



    void ModelNode::SetPosition(Vector3f position)
    {
        m_Position = position;
    }



    Vector3f ModelNode::GetPosition() const
    {
        return(m_Position);
    }



    void ModelNode::SetScale(Vector3f scale)
    {
        m_Scale = scale;
    }



    Vector3f ModelNode::GetScale() const
    {
        return(m_Scale);
    }



    int ModelNode::RegisterMeshData(const std::string meshResourceName, const std::string materialResourceName)
    {
        if (meshResourceName.size() == 0)
        {
            throw std::logic_error("Mesh resource name cannot have 0 characters.");
        }

        if (materialResourceName.size() == 0)
        {
            throw std::logic_error("Material resource name cannot have 0 characters.");
        }

        // TODO : filter for all white names, invalid characters (such as newline)?

        m_MeshData.emplace_back(meshResourceName, materialResourceName);
        return((int) m_MeshData.size() - 1);
    }



    std::string ModelNode::GetMeshName(const int meshIndex) const
    {
        if (meshIndex >= int(m_MeshData.size()) || meshIndex < 0)
        {
            throw std::out_of_range("Mesh index [" + std::to_string(meshIndex) + "] is out of range [" + std::to_string(m_MeshData.size()) + "]");
        }

        return(m_MeshData[meshIndex].meshResourceName);
    }



    std::string ModelNode::GetMaterialName(const int meshIndex) const
    {
        if (meshIndex >= int(m_MeshData.size()) || meshIndex < 0)
        {
            throw std::out_of_range("Mesh index [" + std::to_string(meshIndex) + "] is out of range [" + std::to_string(m_MeshData.size()) + "]");
        }

        return(m_MeshData[meshIndex].materialResourceName);
    }



    int ModelNode::GetMeshesDataCount() const
    {
        return((int)m_MeshData.size());
    }



    int ModelNode::AddChild(std::unique_ptr<ModelNode>& child)
    {
        if (child == nullptr)
        {
            throw std::invalid_argument("[Model Node] Cannot add null child.");
        }

        m_Children.emplace_back();
        auto index = m_Children.size() - 1;
        m_Children[index].swap(child);

        return((int) index);
    }



    ModelNode* ModelNode::GetChild(const int childIndex) const
    {
        if (childIndex >= int(m_Children.size()) || childIndex < 0)
        {
            throw std::out_of_range("Child index [" + std::to_string(childIndex) + "] is out of range [" + std::to_string(m_Children.size()) + "]");
        }

        return(m_Children[childIndex].get());
    }



    int ModelNode::GetChildCount() const
    {
        return((int) m_Children.size());
    }
}

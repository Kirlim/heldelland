
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Mesh.h"

#include"Nest/Vector2.h"
#include"Nest/Vector3.h"



namespace Nest
{
	void Mesh::Make(
		const std::vector<Vector3<float>>& vertices,
		const std::vector<Vector3<float>>& normals,
		const std::vector<Vector2<float>>& uvs,
		const std::vector<unsigned int>& triangles
		)
	{
		DoMake(vertices, normals, uvs, triangles);
	}



	void Mesh::Unload()
	{
		DoUnload();
	}



	void Mesh::Render() const
	{
		DoRender();
	}



	bool Mesh::IsLoaded() const
	{
		return DoIsLoaded();
	}
}

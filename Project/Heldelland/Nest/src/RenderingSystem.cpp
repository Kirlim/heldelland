
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/RenderingSystem.h"



namespace Nest
{
	RenderingSystem::RenderingSystem(SDL_Window& sdlWindow) : System(),
		m_SdlWindow(sdlWindow)
	{
		// Nothing here
	}



	RenderingSystem::~RenderingSystem()
	{
		// Nothing here
	}



	void RenderingSystem::Render(const float deltaTime)
	{
		DoRender(deltaTime);
	}



	void RenderingSystem::DoUpdate(const float deltaTime)
	{
		Render(deltaTime);
	}
}
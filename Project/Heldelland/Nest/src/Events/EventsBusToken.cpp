
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Events/EventsBusToken.h"

#include"Nest/Events/EventsBusBase.h"



namespace Nest
{
	EventsBusToken::EventsBusToken(
		std::weak_ptr<EventsBusControlledPtr> eventsBusControlledPtr,
		const unsigned int tokenId
	) :
		m_EventsBusControlledPtr(eventsBusControlledPtr),
		m_TokenId(tokenId)
	{
	}



	EventsBusToken::~EventsBusToken()
	{
		auto accessor = m_EventsBusControlledPtr.lock();
		if (accessor != nullptr && accessor->ptr != nullptr)
		{
			accessor->ptr->Unregister(m_TokenId);
		}
	}



	bool EventsBusToken::IsValid() const
	{
		return(!m_EventsBusControlledPtr.expired());
	}



	unsigned int EventsBusToken::GetId() const
	{
		return m_TokenId;
	}
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Events/EventsBusBase.h"



namespace Nest
{
	EventsBusBase::EventsBusBase()
	{
		m_ControlledPtr->ptr = this;
	}



	EventsBusBase::~EventsBusBase()
	{
		m_ControlledPtr->ptr = nullptr;
		m_ControlledPtr.reset();

		// TODO : hold destructor until all shared_ptr created by weak_ptr releases. Find a way to warn for too long waiting
		// Useful for parallel code.
		// This is important to avoid invalid memory access. This is the price of bleeding out ownership D=
		// Alternatively, there MUST be a better way than making the shared ptr point to this.
	}



	std::size_t EventsBusBase::GetRegisteredHandlersCount() const
	{
		return DoGetRegisteredHandlersCount();
	}



	bool EventsBusBase::IsHandlerRegistered(unsigned int tokenId) const
	{
		return DoIsHandlerRegistered(tokenId);
	}



	std::weak_ptr<EventsBusControlledPtr> EventsBusBase::GetControlledPointer() const
	{
		return std::weak_ptr<EventsBusControlledPtr>(m_ControlledPtr);
	}



	void EventsBusBase::Unregister(unsigned int tokenId)
	{
		DoUnregister(tokenId);
	}
}
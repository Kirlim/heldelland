//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/SystemsManager.h"

#include<algorithm>
#include"Nest/Exceptions/NullArgumentException.h"



namespace Nest
{
	SystemsManager::SystemsManager()
	{
		// Nothing here
	}



	SystemsManager::~SystemsManager()
	{
		// Nothing here
	}



	void SystemsManager::AddSystem(std::unique_ptr<System> systemPtr)
	{
		m_Systems.push_back(std::move(systemPtr));
		std::sort(
			m_Systems.begin(),
			m_Systems.end(),
			[](const std::unique_ptr<System>& lPtr, const std::unique_ptr<System>& rPtr) -> bool
			{
				return (lPtr->GetExecutionOrder() < rPtr->GetExecutionOrder());
			}
		);
	}



	System* SystemsManager::FindSystem(std::function<bool(System*)> comparison) const
	{
		// TODO : check only on debug builds?
		if (comparison == nullptr)
			ThrowNullArgumentExceptionWithLine("comparison is null.");


		for (auto& systemPtr : m_Systems)
		{
			if (comparison(systemPtr.get()))
			{
				return systemPtr.get();
			}
		}

		return nullptr;
	}



	void SystemsManager::FixedUpdate(float fixedDeltaTime) const
	{
		for (auto& systemPtr : m_Systems)
		{
			systemPtr->FixedUpdate(fixedDeltaTime);
		}
	}



	void SystemsManager::Update(float deltaTime) const
	{
		for (auto& systemPtr : m_Systems)
		{
			systemPtr->Update(deltaTime);
		}
	}
}

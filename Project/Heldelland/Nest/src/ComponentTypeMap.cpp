
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/ComponentTypeMap.h"



namespace Nest
{
	std::atomic_uint ComponentTypeMap::m_LastTypeId(0);
	ComponentTypeMap* ComponentTypeMap::m_Instance(nullptr);
	std::mutex ComponentTypeMap::m_InstanceMutex;



	ComponentTypeMap::ComponentTypeMap(bool isSingleton) :
		m_IsSingleton(isSingleton)
	{
		if (isSingleton)
		{
			std::unique_lock<std::mutex> instanceLock(m_InstanceMutex, std::defer_lock);
			instanceLock.lock();

			if (m_Instance != nullptr)
			{
				throw std::logic_error("Component Type map is already instantiated.");
			}

			m_Instance = this;
		}
	}



	ComponentTypeMap::~ComponentTypeMap()
	{
		if (m_IsSingleton)
		{
			std::unique_lock<std::mutex> instanceLock(m_InstanceMutex, std::defer_lock);
			instanceLock.lock();

			if (m_Instance == this)
			{
				m_Instance = nullptr;
			}
		}
	}



	ComponentTypeId ComponentTypeMap::GetTypeId(std::string typeName) const
	{
		int id = 0;
		for (const auto& type : m_TypesMap)
		{
			if (type.second == typeName)
			{
				id = type.first;
				break;
			}
		}

		return(id);
	}



	ComponentTypeMap* ComponentTypeMap::GetInstance()
	{
		return(m_Instance);
	}
}

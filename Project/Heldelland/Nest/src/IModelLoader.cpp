
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/IModelLoader.h"

#include"Nest/ModelNode.h"
#include"Nest/Model.h"



namespace Nest
{
    IModelLoader::IModelLoader()
    {
        // Nothing here
    }



    IModelLoader::~IModelLoader()
    {
        // Nothing here
    }



    std::unique_ptr<Model> IModelLoader::BuildModel(std::unique_ptr<ModelNode>& rootModelNode)
    {
        // TODO : check if the next line might cause exception and lose rootModelNode data
        return(Model::MakeUnique(rootModelNode));
    }
}
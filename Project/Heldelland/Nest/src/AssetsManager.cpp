
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/AssetsManager.h"

#include<algorithm>



namespace Nest
{
    AssetsManager::AssetsManager() :
        m_AssetsPaths()
    {
        // Nothing here
    }



    AssetsManager::~AssetsManager()
    {
        // Nothing here
    }



    void AssetsManager::LoadAssetsPath(std::string path)
    {
        auto it = std::find(m_AssetsPaths.begin(), m_AssetsPaths.end(), path);
        if (it != m_AssetsPaths.end())
        {
            // TODO : find all meta files in the path, recursively
        }
    }
}
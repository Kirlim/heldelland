//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Helpers/StringHelper.h"

#include<stdexcept>
#include<cctype>



namespace Nest
{
	namespace Helpers
	{
		std::vector<std::string> SplitString(const std::string& text, const char sep)
		{
			std::vector<std::string> tokens;
			std::size_t start = 0, end = 0;

			while ((end = text.find(sep, start)) != std::string::npos)
			{
				if (end != start)
				{
					tokens.push_back(text.substr(start, end - start));
				}
				start = end + 1;
			}

			if (end != start)
			{
				tokens.push_back(text.substr(start));
			}

			return tokens;
		}



		std::vector<char32_t> Utf8ToUtf32(const char* text)
		{
			if (text == nullptr)
			{
				throw std::runtime_error("Cannot convert null string.");
			}

			std::vector<char32_t> result;

			char32_t building = 0;
			int trailing = 0;

			// TODO : change exception to assert
			int index = 0;
			char c = text[index];
			while (c != '\0')
			{
				if ((c & 0x80) == 0)
				{
					if (trailing != 0)
					{
						throw std::runtime_error("Invalid utf sequence - found character while trailing character was expected.");
					}
					result.push_back(c);
				}
				else if ((c & 0xC0) == 0x80)
				{
					if (trailing == 0)
					{
						throw std::runtime_error("Invalid utf sequence - found trailing characeter while there was not a subsequence.");
					}
					--trailing;
					building |= (c & 0x3F) << (trailing * 6);

					if (trailing == 0)
					{
						result.push_back(building);
					}
				}
				else if ((c & 0xE0) == 0xC0)
				{
					if (trailing != 0)
					{
						throw std::runtime_error("Invalid utf sequence - found new subsequence while previous was still incomplete.");
					}
					trailing = 1;
					building = (c & 0x1F) << 6;
				}
				else if ((c & 0xF0) == 0xE0)
				{
					if (trailing != 0)
					{
						throw std::runtime_error("Invalid utf sequence - found new subsequence while previous was still incomplete.");
					}
					trailing = 2;
					building = (c & 0x0F) << 12;
				}
				else if ((c & 0xF8) == 0xF0)
				{
					if (trailing != 0)
					{
						throw std::runtime_error("Invalid utf sequence - found new subsequence while previous was still incomplete.");
					}
					trailing = 3;
					building = (c & 0x07) << 18;
				}
				else if ((c & 0xFC) == 0xF8)
				{
					if (trailing != 0)
					{
						throw std::runtime_error("Invalid utf sequence - found new subsequence while previous was still incomplete.");
					}
					trailing = 4;
					building = (c & 0x03) << 24;
				}
				else if ((c & 0xFE) == 0xFC)
				{
					if (trailing != 0)
					{
						throw std::runtime_error("Invalid utf sequence - found new subsequence while previous was still incomplete.");
					}
					trailing = 5;
					building = (c & 0x01) << 30;
				}

				c = text[++index];
			}

			return (result);
		}



		std::vector<char32_t> Utf8ToUtf32(const std::string& text)
		{
			return (Utf8ToUtf32(text.c_str()));
		}



		bool CharUpperCompare(int left, int right)
		{
			return (std::toupper(left) == std::toupper(right));
		}



		bool UpperCompare(const std::string left, const std::string right)
		{
			return (
				left.size() == right.size() &&
				std::equal(left.begin(), left.end(), right.begin(), CharUpperCompare)
			);
		}
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Texture.h"

#include"Nest/Exceptions/FailedToLoadException.h"
#include"Nest/Exceptions/InvalidArgumentException.h"
#include"Nest/RenderingModule.h"
#include"Nest/Engine.h"
#include"Nest/Global.h"



namespace Nest
{
	Texture::Texture() : Texture(Global->GetEngine()->GetRenderingModule()->GetGameRenderer())
	{
	}



	Texture::Texture(GameRenderer gameRenderer) :
		m_GameRenderer(gameRenderer)
	{
	}



	Texture::~Texture()
	{
		Unload();
	}



	void Texture::Load(std::string file, TextureFiltering filtering, TextureFiltering mipmapFiltering, TextureWrapMode wrapMode, TextureFormat format)
	{
		if (m_Loaded)
		{
			ThrowFailedToLoadExceptionWithLine("Failed to load texture: another texture is already loaded in this object");
		}



		int width, height;
		unsigned char* image = SOIL_load_image(file.c_str(), &width, &height, 0, TranslateTextureFormatSoil(format));

		if (image == nullptr)
		{
			ThrowFailedToLoadExceptionWithLine("Failed to load texture: failed to load image file.");
		}

		Load(image, width, height, filtering, mipmapFiltering, wrapMode, format);
		SOIL_free_image_data(image);

		m_Loaded = true;
	}



	void Texture::Load(const unsigned char* buffer, int width, int height, TextureFiltering filtering, TextureFiltering mipmapFiltering, TextureWrapMode wrapMode, TextureFormat format)
	{
		if (m_Loaded)
		{
			ThrowFailedToLoadExceptionWithLine("Failed to load texture: another texture is already loaded in this object");
		}



		// Setup the filterings
		m_InfoTextureFiltering = filtering;
		m_InfoMipmapFiltering = mipmapFiltering;
		m_InfoWrapMode = wrapMode;
		m_InfoTextureFormat = format;

		m_TextureFiltering = TranslateFiltering(m_InfoTextureFiltering);
		m_MipmapFiltering = TranslateFiltering(m_InfoMipmapFiltering);
		m_WrapMode = TranslateWrapMode(m_InfoWrapMode);
		m_TextureFormat = TranslateTextureFormatGL(m_InfoTextureFormat);


		// TODO : refactor this into a better alternative. m_Mock variable should not exist
		if (m_GameRenderer != GameRenderer::OpenGL) return;

		if (m_InfoTextureFormat != TextureFormat::RGB && m_InfoTextureFormat != TextureFormat::RGBA)
		{
			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		}

		// Load the opengl texture
		glGenTextures(1, &m_TextureId);
		glBindTexture(GL_TEXTURE_2D, m_TextureId);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m_WrapMode);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m_WrapMode);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_TextureFiltering);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_TextureFiltering);

		glTexImage2D(GL_TEXTURE_2D, 0, m_TextureFormat, width, height, 0, m_TextureFormat, GL_UNSIGNED_BYTE, buffer);
		glGenerateMipmap(GL_TEXTURE_2D);

		glBindTexture(GL_TEXTURE_2D, 0);


		if (m_InfoTextureFormat != TextureFormat::RGB && m_InfoTextureFormat != TextureFormat::RGBA)
		{
			glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		}
	}



	void Texture::Unload()
	{
		if (m_Loaded)
		{
			glDeleteTextures(1, &m_TextureId);
			m_Loaded = false;
		}
	}



	TextureFiltering Texture::GetTextureFiltering() const { return (m_InfoTextureFiltering); }
	TextureFiltering Texture::GetMipmapFiltering() const { return (m_InfoMipmapFiltering); }
	TextureWrapMode Texture::GetWrapMode() const { return (m_InfoWrapMode); }
	TextureFormat Texture::GetFormat() const { return (m_InfoTextureFormat); }

	GLuint Texture::GetTextureId() const { return (m_TextureId); }



	GLenum Texture::TranslateFiltering(const TextureFiltering filtering) const
	{
		switch (filtering)
		{
			case TextureFiltering::NearestNeighbor: return (GL_NEAREST);
			case TextureFiltering::Bilinear: return (GL_LINEAR);
			default:
				ThrowInvalidArgumentExceptionWithLine("Invalid texture filtering option.");
		}
	}



	GLenum Texture::TranslateWrapMode(const TextureWrapMode wrapMode) const
	{
		switch (wrapMode)
		{
			case TextureWrapMode::Clamp: return (GL_CLAMP_TO_EDGE);
			case TextureWrapMode::Repeat: return (GL_REPEAT);
			case TextureWrapMode::RepeatMirrored: return (GL_MIRRORED_REPEAT);
			default:
				ThrowInvalidArgumentExceptionWithLine("Invalid texture wrap mode option.");
		}
	}



	GLenum Texture::TranslateTextureFormatGL(const TextureFormat format) const
	{
		switch (format)
		{
			case TextureFormat::RGB: return (GL_RGB);
			case TextureFormat::RGBA: return (GL_RGBA);
			case TextureFormat::Red: return (GL_RED);
			case TextureFormat::Green: return (GL_GREEN);
			case TextureFormat::Blue: return (GL_BLUE);
			case TextureFormat::Alpha: return (GL_ALPHA);
			default:
				ThrowInvalidArgumentExceptionWithLine("Invalid loading texture format option.");
		}
	}



	int Texture::TranslateTextureFormatSoil(const TextureFormat format) const
	{
		switch (format)
		{
			case TextureFormat::RGB: return (SOIL_LOAD_RGB);
			case TextureFormat::RGBA: return (SOIL_LOAD_RGBA);
			default:
				ThrowInvalidArgumentExceptionWithLine("Invalid loading texture format option.");
		}
	}
}

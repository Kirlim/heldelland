//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/EntitiesAndComponentsRemap.h"

#include"Nest/ComponentId.h"



namespace Nest
{
	void EntitiesAndComponentsRemap::RegisterEntity(EntityId entityId)
	{
		if (IsEntityRegistered(entityId))
			ThrowLogicErrorExceptionWithLine("Entity of id [" + std::to_string(entityId.value) + "] is already registered.");

		std::weak_ptr<IEntity> nullEntityPtr;
		m_EntitiesMap.emplace(entityId, nullEntityPtr);
	}



	bool EntitiesAndComponentsRemap::IsEntityRegistered(EntityId entityId) const
	{
		return (m_EntitiesMap.count(entityId) != 0);
	}



	void EntitiesAndComponentsRemap::UnregisterEntity(EntityId entityId)
	{
		if (!IsEntityRegistered(entityId))
			ThrowLogicErrorExceptionWithLine("Entity of id [" + std::to_string(entityId.value) + "] is not registered.");

		m_EntitiesMap.erase(entityId);
	}



	void EntitiesAndComponentsRemap::RegisterComponent(ComponentId componentId)
	{
		if (IsComponentRegistered(componentId))
			ThrowLogicErrorExceptionWithLine("Component of id [" + std::to_string(componentId.value) + "] is already registered.");

		m_ComponentsMap.emplace(componentId, nullptr);
	}



	bool EntitiesAndComponentsRemap::IsComponentRegistered(ComponentId componentId) const
	{
		return (m_ComponentsMap.count(componentId) != 0);
	}



	void EntitiesAndComponentsRemap::UnregisterComponent(ComponentId componentId)
	{
		if (!IsComponentRegistered(componentId))
			ThrowLogicErrorExceptionWithLine("Component of id [" + std::to_string(componentId.value) + "] is not registered.");

		m_ComponentsMap.erase(componentId);
	}



	void EntitiesAndComponentsRemap::ClearEntitiesMapping()
	{
		for (auto& kvPair : m_EntitiesMap)
		{
			kvPair.second = std::weak_ptr<IEntity>();
		}
	}



	void EntitiesAndComponentsRemap::ClearComponentsMapping()
	{
		for (auto& kvPair : m_ComponentsMap)
		{
			kvPair.second = nullptr;
		}
	}



	void EntitiesAndComponentsRemap::ClearMappings()
	{
		ClearEntitiesMapping();
		ClearComponentsMapping();
	}



	void EntitiesAndComponentsRemap::MapEntity(EntityId sourceEntityId, std::weak_ptr<IEntity> targetEntity)
	{
		if (!IsEntityRegistered(sourceEntityId))
			ThrowLogicErrorExceptionWithLine("Cannot map to unregistered entity id [" + std::to_string(sourceEntityId.value) + "]");

		m_EntitiesMap.at(sourceEntityId) = targetEntity;
	}



	bool EntitiesAndComponentsRemap::IsEntityMapped(EntityId sourceEntityId) const
	{
		if (!IsEntityRegistered(sourceEntityId))
			ThrowLogicErrorExceptionWithLine("Cannot verify unregistered entity id [" + std::to_string(sourceEntityId.value) + "]");

		return (!m_EntitiesMap.at(sourceEntityId).expired());
	}



	std::weak_ptr<IEntity> EntitiesAndComponentsRemap::GetMappedEntity(EntityId sourceEntityId) const
	{
		if (!IsEntityRegistered(sourceEntityId))
			ThrowLogicErrorExceptionWithLine("Cannot obtain from unregistered entity id [" + std::to_string(sourceEntityId.value) + "]");

		return (m_EntitiesMap.at(sourceEntityId));
	}



	void EntitiesAndComponentsRemap::MapComponent(ComponentId sourceComponentId, ComponentReferenceBase& targetComponentReference)
	{
		if (!IsComponentRegistered(sourceComponentId))
			ThrowLogicErrorExceptionWithLine("Cannot map to unregistered component id [" + std::to_string(sourceComponentId.value) + "]");

		auto reference = targetComponentReference.Clone();
		m_ComponentsMap.at(sourceComponentId) = std::move(reference);
	}



	bool EntitiesAndComponentsRemap::IsComponentMapped(ComponentId sourceComponentId) const
	{
		if (!IsComponentRegistered(sourceComponentId))
			ThrowLogicErrorExceptionWithLine("Cannot verify unregistered component id [" + std::to_string(sourceComponentId.value) + "]");

		return (m_ComponentsMap.at(sourceComponentId) != nullptr);
	}



	ComponentReferenceBase* EntitiesAndComponentsRemap::GetMappedComponent(ComponentId sourceComponentId) const
	{
		if (!IsComponentRegistered(sourceComponentId))
			ThrowLogicErrorExceptionWithLine("Cannot obtain from unregistered component id [" + std::to_string(sourceComponentId.value) + "]");

		return (m_ComponentsMap.at(sourceComponentId).get());
	}
}

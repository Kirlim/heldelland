//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/RenderingSystemOpenGL.h"

#include<vector>
#include<numeric>
#include<algorithm>

#include<GL/glew.h>

#include"Nest/Exceptions/InvalidOperationException.h"
#include"Nest/Global.h"
#include"Nest/IComponentsContainers.h"
#include"Nest/ITemplatedSingleComponentsContainer.h"

#include"Nest/Engine.h"
#include"Nest/RenderingModule.h"
#include"Nest/RenderingManager.h"
#include"Nest/DirectionalLightComponent.h"
#include"Nest/PointLightComponent.h"
#include"Nest/Transform.h"
#include"Nest/MeshFilter.h"
#include"Nest/TextMesh.h"
#include"Nest/Material.h"
#include"Nest/MeshRenderer.h"
#include"Nest/Camera.h"
#include"Nest/Log.h"



namespace Nest
{
	RenderingSystemOpenGL::RenderingSystemOpenGL(
		SDL_Window& sdlWindow, const IComponentsContainers& componentsContainers, const RenderingManager& renderingManager
	) :
		RenderingSystem(sdlWindow),
		m_ComponentsContainers(componentsContainers),
		m_RenderingManager(renderingManager)
	{
		// Nothing here
	}



	void RenderingSystemOpenGL::Init()
	{
		InitQuad();
		InitGBuffer();
	}



	void RenderingSystemOpenGL::SetDirectionalLightPassShader(ShaderProgram* directionalLightPassShader)
	{
		m_DirectionalLightPassShader = directionalLightPassShader;
	}



	void RenderingSystemOpenGL::SetPointLightPassShader(ShaderProgram* pointLightPassShader)
	{
		m_PointLightPassShader = pointLightPassShader;
	}



	ShaderProgram* RenderingSystemOpenGL::GetDirectionalLightPassShader() const
	{
		return (m_DirectionalLightPassShader);
	}



	ShaderProgram* RenderingSystemOpenGL::GetPointLightPassShader() const
	{
		return (m_PointLightPassShader);
	}



	void RenderingSystemOpenGL::SetSphereLightVolume(Mesh* sphereVolumeMesh)
	{
		m_SphereLightVolume = sphereVolumeMesh;
	}



	Mesh* RenderingSystemOpenGL::GetSphereLightVolume() const
	{
		return (m_SphereLightVolume);
	}



	void RenderingSystemOpenGL::InitQuad()
	{
		GLfloat quadVertices[] = {
			// Vertex: (position, uv)
			-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
			-1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
			1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f, 1.0f, 1.0f,
		};

		glGenVertexArrays(1, &m_QuadVAO);
		glGenBuffers(1, &m_QuadVBO);
		glBindVertexArray(m_QuadVAO);
		glBindBuffer(GL_ARRAY_BUFFER, m_QuadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), static_cast<GLvoid*>(nullptr));
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(3 * sizeof(GLfloat)));

		glBindVertexArray(0);

		m_QuadInitialized = true;
	}



	void RenderingSystemOpenGL::InitGBuffer()
	{
		if (m_GeometryBufferInitialized) return;

		const auto renderingModule = Global->GetEngine()->GetRenderingModule();
		const GLsizei screenWidth = renderingModule->GetScreenWidth();
		const GLsizei screenHeight = renderingModule->GetScreenHeight();

		glGenFramebuffers(1, &m_GeometryBuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, m_GeometryBuffer);

		// Position color buffer
		glGenTextures(1, &m_GeometryPosition);
		glBindTexture(GL_TEXTURE_2D, m_GeometryPosition);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, screenWidth, screenHeight, 0, GL_RGB, GL_FLOAT, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_GeometryPosition, 0);

		// Normal color buffer
		glGenTextures(1, &m_GeometryNormal);
		glBindTexture(GL_TEXTURE_2D, m_GeometryNormal);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, screenWidth, screenHeight, 0, GL_RGB, GL_FLOAT, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_GeometryNormal, 0);

		// Color + Specular color buffer
		glGenTextures(1, &m_GeometryAlbedoSpec);
		glBindTexture(GL_TEXTURE_2D, m_GeometryAlbedoSpec);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, screenWidth, screenHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, m_GeometryAlbedoSpec, 0);


		// Tell OpenGL which color attachments we'll use (of this framebuffer) for rendering
		GLuint attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
		glDrawBuffers(3, attachments);


		// Create and attach depth buffer (renderbuffer)
		GLuint rboDepth;
		glGenRenderbuffers(1, &rboDepth);
		glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, screenWidth, screenHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);


		// Finally check if framebuffer is complete
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			Log::Error("Framebuffer is not complete.");
			m_GeometryBufferInitialized = false;
		}
		else
		{
			m_GeometryBufferInitialized = true;
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}



	void RenderingSystemOpenGL::DoRender(const float deltaTime)
	{
		RenderWorld();
		RenderGUI();


		SDL_GL_SwapWindow(&m_SdlWindow);
	}



	void RenderingSystemOpenGL::RenderQuad() const
	{
		if (!m_QuadInitialized)
			ThrowInvalidOperationExceptionWithLine("RenderQuad: quad is not initialized.");

		glCullFace(GL_FRONT);

		glBindVertexArray(m_QuadVAO);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		glBindVertexArray(0);
	}



	void RenderingSystemOpenGL::RenderWorld() const
	{
		auto camera = m_RenderingManager.GetActiveCamera();
		if (camera == nullptr) return;

		camera->RecalculateMatrices();



		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



		const auto meshRenderersContainer = m_ComponentsContainers.GetSingleContainer<MeshRenderer>();
		const auto directionalLightsContainer = m_ComponentsContainers.GetSingleContainer<DirectionalLightComponent>();
		const auto pointLightsContainer = m_ComponentsContainers.GetSingleContainer<PointLightComponent>();
		/**/

		// Geometry pass
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		BindGBufferForWriting();

		glDepthMask(GL_TRUE);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);

		for (auto& meshRenderer : *meshRenderersContainer)
		{
			if (meshRenderer.IsGui()) continue;

			// Get mesh filter
			auto entity = meshRenderer.GetOwner();

			const auto meshFilter = entity->GetComponent<MeshFilter>();
			if (meshFilter == nullptr || meshFilter->GetMesh() == nullptr) continue;

			// Get material
			auto material = meshRenderer.GetMaterial();
			if (material == nullptr) continue;

			// Get the transform
			auto transform = entity->GetComponent<Transform>();
			if (transform == nullptr) continue;

			// Get the model matrix
			const auto transformMatrix = transform->GetTransformMatrix();
			const Matrix4<GLfloat> modelMatrix
			(
				transformMatrix.data[0], transformMatrix.data[1], transformMatrix.data[2], transformMatrix.data[3],
				transformMatrix.data[4], transformMatrix.data[5], transformMatrix.data[6], transformMatrix.data[7],
				transformMatrix.data[8], transformMatrix.data[9], transformMatrix.data[10], transformMatrix.data[11],
				transformMatrix.data[12], transformMatrix.data[13], transformMatrix.data[14], transformMatrix.data[15]
			);


			// Load the material for rendering
			material->LoadForRendering();


			// Set the rendering matrices

			// Model
			const GLint model = glGetUniformLocation(material->GetShaderProgram()->GetProgramId(), "model");
			glUniformMatrix4fv(model, 1, GL_TRUE, &(modelMatrix.data[0]));

			// View
			const GLint view = glGetUniformLocation(material->GetShaderProgram()->GetProgramId(), "view");
			glUniformMatrix4fv(view, 1, GL_TRUE, &(camera->GetViewMatrix().data[0]));

			// Projection
			const GLint projection = glGetUniformLocation(material->GetShaderProgram()->GetProgramId(), "projection");
			glUniformMatrix4fv(projection, 1, GL_TRUE, &(camera->GetProjectionMatrix().data[0]));



			// Finally, render the mesh
			meshFilter->GetMesh()->Render();

			// Unload the material from rendering
			material->UnloadFromRendering();
		}

		glDepthMask(GL_FALSE);
		glDisable(GL_DEPTH_TEST);


		// Lighting pass
		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_ONE, GL_ONE);

		BindGBufferForReading();
		glClear(GL_COLOR_BUFFER_BIT);



		// Get the screen size
		GLfloat screenSize[2] = {
			float(Global->GetEngine()->GetRenderingModule()->GetScreenWidth()),
			float(Global->GetEngine()->GetRenderingModule()->GetScreenHeight())
		};




		// Fullscreen pass for directional lights.
		m_DirectionalLightPassShader->Use();

		glUniform1i(glGetUniformLocation(m_DirectionalLightPassShader->GetProgramId(), "gPosition"), 0);
		glUniform1i(glGetUniformLocation(m_DirectionalLightPassShader->GetProgramId(), "gNormal"), 1);
		glUniform1i(glGetUniformLocation(m_DirectionalLightPassShader->GetProgramId(), "gAlbedoSpec"), 2);


		int index = 0;
		for (auto& directionalLight : *directionalLightsContainer)
		{
			const auto dir = directionalLight.GetDirection();
			const auto color = directionalLight.GetColor();

			GLfloat dirOGL[3] = { GLfloat(dir.x), GLfloat(dir.y), GLfloat(dir.z) };
			GLfloat colorOGL[3] = { GLfloat(color.x), GLfloat(color.y), GLfloat(color.z) };

			glUniform3fv(glGetUniformLocation(m_DirectionalLightPassShader->GetProgramId(), ("directionalLights[" + std::to_string(index) + "].direction").c_str()), 1, dirOGL);
			glUniform3fv(glGetUniformLocation(m_DirectionalLightPassShader->GetProgramId(), ("directionalLights[" + std::to_string(index) + "].color").c_str()), 1, colorOGL);

			++index;
		}

		RenderQuad();


		// Light volumes pass of the point lights
		m_PointLightPassShader->Use();

		glUniform1i(glGetUniformLocation(m_PointLightPassShader->GetProgramId(), "gPosition"), 0);
		glUniform1i(glGetUniformLocation(m_PointLightPassShader->GetProgramId(), "gNormal"), 1);
		glUniform1i(glGetUniformLocation(m_PointLightPassShader->GetProgramId(), "gAlbedoSpec"), 2);

		const GLint ulLightColor = glGetUniformLocation(m_PointLightPassShader->GetProgramId(), "lightColor");
		const GLint ulLightPosition = glGetUniformLocation(m_PointLightPassShader->GetProgramId(), "lightPosition");
		const GLint ulLightLinear = glGetUniformLocation(m_PointLightPassShader->GetProgramId(), "lightLinear");
		const GLint ulLightQuadratic = glGetUniformLocation(m_PointLightPassShader->GetProgramId(), "lightQuadratic");

		for (auto& pointLight : *pointLightsContainer)
		{
			IEntity* pointLightEntity = pointLight.GetOwner();

			auto pointLightTransform = pointLightEntity->GetComponent<Transform>();
			if (pointLightTransform == nullptr) continue;

			const auto pos = pointLightTransform->GetPosition();
			const auto radius = pointLight.GetVolumeRadius();

			const Matrix4<GLfloat> modelMatrix
			(
				radius, 0.0f, 0.0f, pos.x,
				0.0f, radius, 0.0f, pos.y,
				0.0f, 0.0f, radius, pos.z,
				0.0f, 0.0f, 0.0f, 1.0f
			);



			// Set the rendering matrices

			// Model
			const GLint model = glGetUniformLocation(m_PointLightPassShader->GetProgramId(), "model");
			glUniformMatrix4fv(model, 1, GL_TRUE, &(modelMatrix.data[0]));

			// View
			const GLint view = glGetUniformLocation(m_PointLightPassShader->GetProgramId(), "view");
			glUniformMatrix4fv(view, 1, GL_TRUE, &(camera->GetViewMatrix().data[0]));

			// Projection
			const GLint projection = glGetUniformLocation(m_PointLightPassShader->GetProgramId(), "projection");
			glUniformMatrix4fv(projection, 1, GL_TRUE, &(camera->GetProjectionMatrix().data[0]));


			// Set the screen size
			const GLint screenSizeUniform = glGetUniformLocation(m_PointLightPassShader->GetProgramId(), "screenSize");
			glUniform2fv(screenSizeUniform, 1, screenSize);


			// Light properties
			GLfloat color[3]{ pointLight.GetColor().x, pointLight.GetColor().y, pointLight.GetColor().z };
			glUniform3fv(ulLightColor, 1, color);

			GLfloat position[3]{ pointLightTransform->GetPosition().x, pointLightTransform->GetPosition().y, pointLightTransform->GetPosition().z };
			glUniform3fv(ulLightPosition, 1, position);

			glUniform1f(ulLightLinear, pointLight.GetLinearAttenuation());
			glUniform1f(ulLightQuadratic, pointLight.GetQuadraticAttenuation());


			// Render the light volume
			m_SphereLightVolume->Render();
		}
	}



	void RenderingSystemOpenGL::RenderGUI()
	{
		auto guiCamera = m_RenderingManager.GetActiveGuiCamera();
		if (guiCamera == nullptr) return;

		const auto meshRenderersContainer = m_ComponentsContainers.GetSingleContainer<MeshRenderer>();

		guiCamera->RecalculateMatrices();
		const auto viewMatrix = guiCamera->GetViewMatrix();
		auto projectionMatrix = guiCamera->GetProjectionMatrix();
		const auto viewProjectionMatrix = projectionMatrix * viewMatrix;


		glDepthMask(GL_TRUE);
		glEnable(GL_DEPTH_TEST);
		glClear(GL_DEPTH_BUFFER_BIT);

		glDisable(GL_BLEND);
		glDisable(GL_CULL_FACE);


		// Build helper structures
		std::vector<MeshRenderer*> solidRenderers;
		std::vector<Material*> solidMaterials;
		std::vector<ShaderProgram*> solidShaders;
		std::vector<ComponentReference<Transform>> solidTransforms;
		std::vector<IEntity*> solidEntities;

		std::vector<MeshRenderer*> transparentRenderers;
		std::vector<Material*> transparentMaterials;
		std::vector<ShaderProgram*> transparentShaders;
		std::vector<ComponentReference<Transform>> transparentTransforms;
		std::vector<float> transparentRenderersDepth;
		std::vector<IEntity*> transparentEntities;


		solidRenderers.reserve(meshRenderersContainer->GetElementsCount());
		solidMaterials.reserve(meshRenderersContainer->GetElementsCount());
		solidShaders.reserve(meshRenderersContainer->GetElementsCount());
		solidTransforms.reserve(meshRenderersContainer->GetElementsCount());
		solidEntities.reserve(meshRenderersContainer->GetElementsCount());

		transparentRenderers.reserve(meshRenderersContainer->GetElementsCount());
		transparentMaterials.reserve(meshRenderersContainer->GetElementsCount());
		transparentShaders.reserve(meshRenderersContainer->GetElementsCount());
		transparentTransforms.reserve(meshRenderersContainer->GetElementsCount());
		transparentRenderersDepth.reserve(meshRenderersContainer->GetElementsCount());
		transparentEntities.reserve(meshRenderersContainer->GetElementsCount());


		for (auto& meshRenderer : *meshRenderersContainer)
		{
			if (!meshRenderer.IsGui()) continue;

			auto entity = meshRenderer.GetOwner();


			auto material = meshRenderer.GetMaterial();
			if (material == nullptr) continue;


			auto transform = entity->GetComponent<Transform>();
			if (transform == nullptr) continue;


			auto shader = material->GetShaderProgram();
			if (shader == nullptr)
			{
				// TODO : use "shaderless" shader.
				continue;
			}

			if (!shader->IsLoaded())
			{
				throw std::logic_error("[OpenGL Rendering System] Trying to use a shader that is not loaded.");
			}



			if (shader->IsTransparent())
			{
				const auto transformPos = transform->GetPosition();
				const Vector4<float> pos(transformPos.x, transformPos.y, transformPos.z, 1.0f);

				auto depthPos = viewProjectionMatrix * pos;
				float depth = depthPos.z();

				transparentRenderers.push_back(&meshRenderer);
				transparentMaterials.push_back(material);
				transparentShaders.push_back(shader);
				transparentTransforms.push_back(transform);
				transparentRenderersDepth.push_back(depth);
				transparentEntities.push_back(entity);
			}
			else
			{
				solidRenderers.push_back(&meshRenderer);
				solidMaterials.push_back(material);
				solidShaders.push_back(shader);
				solidTransforms.push_back(transform);
				solidEntities.push_back(entity);
			}
		}


		// Rendering
		for (decltype(solidEntities.size()) i = 0; i < solidEntities.size(); ++i)
		{
			auto textMesh = solidEntities[i]->GetComponent<TextMesh>();
			const auto meshFilter = solidEntities[i]->GetComponent<MeshFilter>();
			if (textMesh == nullptr && meshFilter == nullptr) continue;


			const auto transformMatrix = solidTransforms[i]->GetTransformMatrix();
			const Matrix4<GLfloat> modelMatrix
			(
				transformMatrix.data[0], transformMatrix.data[1], transformMatrix.data[2], transformMatrix.data[3],
				transformMatrix.data[4], transformMatrix.data[5], transformMatrix.data[6], transformMatrix.data[7],
				transformMatrix.data[8], transformMatrix.data[9], transformMatrix.data[10], transformMatrix.data[11],
				transformMatrix.data[12], transformMatrix.data[13], transformMatrix.data[14], transformMatrix.data[15]
			);

			solidMaterials[i]->LoadForRendering();

			const GLint model = glGetUniformLocation(solidShaders[i]->GetProgramId(), "model");
			glUniformMatrix4fv(model, 1, GL_TRUE, &(modelMatrix.data[0]));

			const GLint view = glGetUniformLocation(solidShaders[i]->GetProgramId(), "view");
			glUniformMatrix4fv(view, 1, GL_TRUE, &(viewMatrix.data[0]));

			const GLint projection = glGetUniformLocation(solidShaders[i]->GetProgramId(), "projection");
			glUniformMatrix4fv(projection, 1, GL_TRUE, &(projectionMatrix.data[0]));


			if (textMesh != nullptr)
			{
				textMesh->Render();
			}
			else
			{
				meshFilter->GetMesh()->Render();
			}

			solidMaterials[i]->UnloadFromRendering();
		}


		glDepthMask(GL_FALSE);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		std::vector<int> transparentOrder(transparentEntities.size());
		std::iota(transparentOrder.begin(), transparentOrder.end(), 0);
		std::sort(
			transparentOrder.begin(), transparentOrder.end(),
			[&](std::size_t i, std::size_t j) { return (transparentRenderersDepth[i] > transparentRenderersDepth[j]); } // Further first
		);

		for (int index : transparentOrder)
		{
			auto textMesh = transparentEntities[index]->GetComponent<TextMesh>();
			const auto meshFilter = transparentEntities[index]->GetComponent<MeshFilter>();
			if (textMesh == nullptr && meshFilter == nullptr) continue;


			const auto transformMatrix = transparentTransforms[index]->GetTransformMatrix();
			const Matrix4<GLfloat> modelMatrix
			(
				transformMatrix.data[0], transformMatrix.data[1], transformMatrix.data[2], transformMatrix.data[3],
				transformMatrix.data[4], transformMatrix.data[5], transformMatrix.data[6], transformMatrix.data[7],
				transformMatrix.data[8], transformMatrix.data[9], transformMatrix.data[10], transformMatrix.data[11],
				transformMatrix.data[12], transformMatrix.data[13], transformMatrix.data[14], transformMatrix.data[15]
			);

			transparentMaterials[index]->LoadForRendering();

			const GLint model = glGetUniformLocation(transparentShaders[index]->GetProgramId(), "model");
			glUniformMatrix4fv(model, 1, GL_TRUE, &(modelMatrix.data[0]));

			const GLint view = glGetUniformLocation(transparentShaders[index]->GetProgramId(), "view");
			glUniformMatrix4fv(view, 1, GL_TRUE, &viewMatrix.data[0]);

			const GLint projection = glGetUniformLocation(transparentShaders[index]->GetProgramId(), "projection");
			glUniformMatrix4fv(projection, 1, GL_TRUE, &projectionMatrix.data[0]);


			if (textMesh != nullptr)
			{
				textMesh->Render();
			}
			else
			{
				meshFilter->GetMesh()->Render();
			}

			transparentMaterials[index]->UnloadFromRendering();
		}

		glDisable(GL_BLEND);
	}



	void RenderingSystemOpenGL::BindGBufferForWriting() const
	{
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_GeometryBuffer);
	}



	void RenderingSystemOpenGL::BindGBufferForReading() const
	{
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_GeometryPosition);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, m_GeometryNormal);

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, m_GeometryAlbedoSpec);
	}
}

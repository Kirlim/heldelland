//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/WorldsManager.h"

#include<algorithm>

#include"Nest/Exceptions/NullPointerException.h"
#include"Nest/World.h"



namespace Nest
{
	void WorldsManager::AddWorld(WorldId worldId, std::unique_ptr<World>& world)
	{
		if (world == nullptr)
			ThrowNullPointerExceptionWithLine("World cannot be null.");

		if (GetWorld(worldId) != nullptr)
			ThrowLogicErrorExceptionWithLine("World witd id " + std::to_string(worldId.id) + " is already registered.");

		m_Worlds.emplace_back(WorldInformation{ worldId, std::move(world) });
	}



	void WorldsManager::RemoveWorld(WorldId worldId)
	{
		const auto findResult = std::find_if(
			std::begin(m_Worlds),
			std::end(m_Worlds),
			[&](const WorldInformation& worldInformation) { return worldInformation.worldId.id == worldId.id; }
		);

		if (findResult == std::end(m_Worlds))
			ThrowLogicErrorExceptionWithLine("World with id " + std::to_string(worldId.id) + " could not be found for removal.");

		m_Worlds.erase(findResult);
	}



	World* WorldsManager::GetWorld(WorldId worldId) const
	{
		World* result = nullptr;

		for (const auto& world : m_Worlds)
		{
			if (world.worldId.id == worldId.id)
			{
				result = world.world.get();
				break;
			}
		}

		return result;
	}



	WorldsManager::WorldIterator WorldsManager::begin() const
	{
		return m_Worlds.begin();
	}



	WorldsManager::WorldIterator WorldsManager::end() const
	{
		return m_Worlds.end();
	}
}

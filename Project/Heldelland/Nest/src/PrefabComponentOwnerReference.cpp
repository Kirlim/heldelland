
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/PrefabComponentOwnerReference.h"

#include"Nest/Component.h"



namespace Nest
{
	PrefabComponentOwnerReference::PrefabComponentOwnerReference(std::unique_ptr<Component> component, ComponentTypeId typeId)
		: ComponentOwnerReferenceBase(typeId),
		m_TypeId(typeId), m_Component(std::move(component))
	{
		// Nothing here
	}



	PrefabComponentOwnerReference::~PrefabComponentOwnerReference()
	{
		// Nothing here
	}



	Component* PrefabComponentOwnerReference::Get() const
	{
		return(m_Component.get());
	}



	void PrefabComponentOwnerReference::RegisterComponentReference(ComponentReferenceBase& reference)
	{
		// Nothing here
	}



	void PrefabComponentOwnerReference::UnregisterComponentReference(ComponentReferenceBase& reference)
	{
		// Nothing here
	}
}
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Metadata/TextureMetadata.h"

#include"Nest/Exceptions/FailedToLoadException.h"
#include"Nest/Helpers/StringHelper.h"
#include"Nest/IResourcesManager.h"



namespace Nest
{
	TextureMetadata::TextureMetadata(const std::string resourceName) : IMetadataInformation(resourceName)
	{
		// Nothing here
	}



	TextureMetadata::~TextureMetadata() = default;



	ResourceType TextureMetadata::GetResourceType() const
	{
		return (ResourceType::Texture);
	}



	void TextureMetadata::LoadMetadataFromYamlNode(const YAML::Node& node, const std::string metadataPath)
	{
		int separatorIndex = 0;
		for (int i = int(metadataPath.size()) - 1; i >= 0; --i)
		{
			if (metadataPath[i] == '/' || metadataPath[i] == '\\')
			{
				separatorIndex = i;
				break;
			}
		}

		m_TexturePath = metadataPath.substr(0, separatorIndex) + '/';

		if (node["Texture"])
		{
			m_TexturePath += node["Texture"].as<std::string>();
		}
		else
		{
			ThrowFailedToLoadExceptionWithLine("Failed to get the Texture node.");
		}


		TryLoadFilterings(node);
		TryLoadWrapMode(node);
		TryLoadFormat(node);


		m_IsLoaded = true;
	}



	void TextureMetadata::LoadResourceIntoManager(IResourcesManager& resourcesManager)
	{
		std::unique_ptr<Texture> texture = std::make_unique<Texture>();
		texture->Load(m_TexturePath, m_Filtering, m_MipmapFiltering, m_WrapMode, m_Format);

		resourcesManager.AddTexture(m_ResourceName, texture);
	}



	void TextureMetadata::TryLoadFilterings(const YAML::Node& mainNode)
	{
		const std::string bilinearStr{ "bilinear" };
		const std::string nearestStr{ "Nearest" };

		if (mainNode["Filtering"])
		{
			const std::string filteringType = mainNode["Filtering"].as<std::string>();

			if (Helpers::UpperCompare(filteringType, bilinearStr))
			{
				m_Filtering = TextureFiltering::Bilinear;
			}
			else if (Helpers::UpperCompare(filteringType, nearestStr))
			{
				m_Filtering = TextureFiltering::NearestNeighbor;
			}
			else
			{
				ThrowFailedToLoadExceptionWithLine("Unrecognized texture filtering type.");
			}
		}


		if (mainNode["MipmapFiltering"])
		{
			const std::string filteringType = mainNode["MipmapFiltering"].as<std::string>();
			if (Helpers::UpperCompare(filteringType, bilinearStr))
			{
				m_MipmapFiltering = TextureFiltering::Bilinear;
			}
			else if (Helpers::UpperCompare(filteringType, nearestStr))
			{
				m_MipmapFiltering = TextureFiltering::NearestNeighbor;
			}
			else
			{
				ThrowFailedToLoadExceptionWithLine("Unrecognized mipmap texture filtering type.");
			}
		}
	}



	void TextureMetadata::TryLoadWrapMode(const YAML::Node& mainNode)
	{
		const std::string repeatStr{ "repeat" };
		const std::string repeatMirroredStr{ "repeatmirrored" };
		const std::string clampStr{ "clamp" };

		if (mainNode["WrapMode"])
		{
			const std::string wrapModeType = mainNode["WrapMode"].as<std::string>();
			if (Helpers::UpperCompare(wrapModeType, repeatStr))
			{
				m_WrapMode = TextureWrapMode::Repeat;
			}
			else if (Helpers::UpperCompare(wrapModeType, repeatMirroredStr))
			{
				m_WrapMode = TextureWrapMode::RepeatMirrored;
			}
			else if (Helpers::UpperCompare(wrapModeType, clampStr))
			{
				m_WrapMode = TextureWrapMode::Clamp;
			}
			else
			{
				ThrowFailedToLoadExceptionWithLine("Unrecognized wrap mode.");
			}
		}
	}



	void TextureMetadata::TryLoadFormat(const YAML::Node& mainNode)
	{
		const std::string redStr{ "red" };
		const std::string greenStr{ "green" };
		const std::string blueStr{ "blue" };
		const std::string alphaStr{ "alpha" };
		const std::string rgbaStr{ "rgba" };
		const std::string rgbStr{ "rgb" };

		if (mainNode["Format"])
		{
			const std::string formatType = mainNode["Format"].as<std::string>();
			if (Helpers::UpperCompare(formatType, redStr))
			{
				m_Format = TextureFormat::Red;
			}
			else if (Helpers::UpperCompare(formatType, greenStr))
			{
				m_Format = TextureFormat::Green;
			}
			else if (Helpers::UpperCompare(formatType, blueStr))
			{
				m_Format = TextureFormat::Blue;
			}
			else if (Helpers::UpperCompare(formatType, alphaStr))
			{
				m_Format = TextureFormat::Alpha;
			}
			else if (Helpers::UpperCompare(formatType, rgbaStr))
			{
				m_Format = TextureFormat::RGBA;
			}
			else if (Helpers::UpperCompare(formatType, rgbStr))
			{
				m_Format = TextureFormat::RGB;
			}
			else
			{
				ThrowFailedToLoadExceptionWithLine("Unknow format type [" + formatType + "].");
			}
		}
	}
}

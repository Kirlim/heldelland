
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Metadata/FontMetadata.h"

#include"Nest/NestAssert.h"
#include"Nest/Exceptions/FailedToLoadException.h"
#include"Nest/Font.h"
#include"Nest/IResourcesManager.h"



namespace Nest
{
	FontMetadata::FontMetadata(const std::string resourceName) :
		IMetadataInformation(resourceName)
	{
		// Nothing here
	}



	FontMetadata::~FontMetadata()
	{
		// Nothing here
	}



	ResourceType FontMetadata::GetResourceType() const
	{
		return(ResourceType::Font);
	}



	void FontMetadata::LoadMetadataFromYamlNode(const YAML::Node& node, const std::string metadataPath)
	{
		// Get the font file
		int separatorIndex = 0;
		for (auto i = metadataPath.size() - 1; i > 0; --i)
		{
			if (metadataPath[i] == '/' || metadataPath[i] == '\\')
			{
				separatorIndex = int(i);
				break;
			}
		}

		std::string pathBase = metadataPath.substr(0, separatorIndex);
		pathBase += '/';


		if (node["File"])
		{
			m_FontFile = pathBase + node["File"].as<std::string>();
		}

		if (m_FontFile.size() == 0)
		{
			ThrowFailedToLoadExceptionWithLine("File name is empty.");
		}


		m_IsLoaded = true;
	}



	void FontMetadata::LoadResourceIntoManager(IResourcesManager& resourcesManager)
	{
		auto font = std::make_unique<Font>(resourcesManager);
		font->LoadFont(m_ResourceName, m_FontFile);

		NestAssert(font->IsLoaded());

		resourcesManager.AddFont(m_ResourceName, font);
	}
}

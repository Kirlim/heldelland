//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Metadata/ShaderProgramMetadata.h"

#include"Nest/Exceptions/FailedToLoadException.h"
#include"Nest/ShaderProgram.h"
#include"Nest/IResourcesManager.h"



namespace Nest
{
	ShaderProgramMetadata::ShaderProgramMetadata(const std::string resourceName) : IMetadataInformation(resourceName)
	{
	}



	ShaderProgramMetadata::~ShaderProgramMetadata() = default;



	ResourceType ShaderProgramMetadata::GetResourceType() const
	{
		return (ResourceType::Shader);
	}



	void ShaderProgramMetadata::LoadMetadataFromYamlNode(const YAML::Node& node, const std::string metadataPath)
	{
		LoadShaderFilesPaths(node, metadataPath);
		TryLoadMainTextureInformation(node);
		TryLoadTexturesInformation(node);
		TryLoadTransparencyFlag(node);

		m_IsLoaded = true;
	}



	void ShaderProgramMetadata::LoadResourceIntoManager(IResourcesManager& resourcesManager)
	{
		std::unique_ptr<ShaderProgram> shaderProgram = std::make_unique<ShaderProgram>();

		shaderProgram->LoadFromFile(m_VertexShaderFile, m_FragmentShaderFile);

		if (!m_MainTextureName.empty()) shaderProgram->SetMainTextureName(m_MainTextureName);

		for (auto& textureName : m_TexturesNames) shaderProgram->AddTextureName(textureName);

		shaderProgram->SetTransparentFlag(m_IsTransparent);

		resourcesManager.AddShaderProgram(m_ResourceName, shaderProgram);
	}



	void ShaderProgramMetadata::LoadShaderFilesPaths(const YAML::Node& mainNode, const std::string metadataPath)
	{
		int separatorIndex = 0;
		for (int i = int(metadataPath.size()) - 1; i >= 0; --i)
		{
			if (metadataPath[i] == '/' || metadataPath[i] == '\\')
			{
				separatorIndex = i;
				break;
			}
		}

		std::string basePath = metadataPath.substr(0, separatorIndex);
		basePath += '/';


		if (mainNode["Vertex"])
		{
			m_VertexShaderFile = basePath + mainNode["Vertex"].as<std::string>();
		}

		if (mainNode["Fragment"])
		{
			m_FragmentShaderFile = basePath + mainNode["Fragment"].as<std::string>();
		}

		if (m_VertexShaderFile.empty() || m_FragmentShaderFile.empty())
		{
			ThrowFailedToLoadExceptionWithLine("Vertex or Fragment shader file name are empty.");
		}
	}



	void ShaderProgramMetadata::TryLoadMainTextureInformation(const YAML::Node& mainNode)
	{
		if (mainNode["MainTexture"])
		{
			m_MainTextureName = mainNode["MainTexture"].as<std::string>();
			if (m_MainTextureName.empty())
			{
				ThrowFailedToLoadExceptionWithLine("MainTexture name is set up as empty.");
			}
		}
	}



	void ShaderProgramMetadata::TryLoadTexturesInformation(const YAML::Node& mainNode)
	{
		if (mainNode["Textures"])
		{
			for (const auto& texturesNodes : mainNode["Textures"])
			{
				const std::string textureName = texturesNodes.as<std::string>();
				m_TexturesNames.push_back(textureName);
			}
		}
	}



	void ShaderProgramMetadata::TryLoadTransparencyFlag(const YAML::Node& mainNode)
	{
		if (mainNode["IsTransparent"])
		{
			m_IsTransparent = mainNode["IsTransparent"].as<bool>();
		}
	}
}

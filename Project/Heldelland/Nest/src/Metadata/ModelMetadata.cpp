//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Metadata/ModelMetadata.h"

#include"Nest/Exceptions/FailedToLoadException.h"
#include"Nest/IModelLoader.h"



namespace Nest
{
	ModelMetadata::ModelMetadata(const std::string resourceName, IModelLoader& modelLoader, GameRenderer gameRenderer) :
		IMetadataInformation(resourceName),
		m_ModelLoader(modelLoader), m_GameRenderer(gameRenderer)
	{
		// Nothing here
	}



	ModelMetadata::~ModelMetadata()
	{
		// Nothing here
	}



	ResourceType ModelMetadata::GetResourceType() const
	{
		return (ResourceType::Model);
	}



	void ModelMetadata::LoadMetadataFromYamlNode(const YAML::Node& node, const std::string metadataPath)
	{
		// Get the model file
		int separatorIndex = 0;
		for (auto i = metadataPath.size() - 1; i > 0; --i)
		{
			if (metadataPath[i] == '/' || metadataPath[i] == '\\')
			{
				separatorIndex = (int)i;
				break;
			}
		}

		std::string pathBase = metadataPath.substr(0, separatorIndex);
		pathBase += '/';


		if (node["File"])
		{
			m_ModelFileName = pathBase + node["File"].as<std::string>();
		}

		if (m_ModelFileName.size() == 0)
			ThrowFailedToLoadExceptionWithLine("File name is empty.");
	}



	void ModelMetadata::LoadResourceIntoManager(IResourcesManager& resourcesManager)
	{
		m_ModelLoader.Load(resourcesManager, m_GameRenderer, m_ModelFileName, m_ResourceName, 0);
	}
}

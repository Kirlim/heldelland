//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Metadata/IMetadataInformation.h"

#include"Nest/Exceptions/InvalidOperationException.h"
#include"Nest/IResourcesManager.h"



namespace Nest
{
	IMetadataInformation::IMetadataInformation(const std::string resourceName) :
		m_ResourceName(resourceName)
	{
	}



	IMetadataInformation::~IMetadataInformation() = default;



	void IMetadataInformation::LoadMetadata(const YAML::Node& node, const std::string metadataPath)
	{
		if (m_IsLoaded)
			ThrowInvalidOperationExceptionWithLine("The metadata is already loaded");

		LoadMetadataFromYamlNode(node, metadataPath);

		m_IsLoaded = true;
	}



	void IMetadataInformation::LoadResource(IResourcesManager& resourcesManager)
	{
		if (!m_IsLoaded)
			ThrowInvalidOperationExceptionWithLine("The metadata is not loaded");

		LoadResourceIntoManager(resourcesManager);
	}



	bool IMetadataInformation::IsLoaded() const
	{
		return (m_IsLoaded);
	}
}

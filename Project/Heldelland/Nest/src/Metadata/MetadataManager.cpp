//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Metadata/MetadataManager.h"

#include<experimental/filesystem>
#include<fstream>

#include"Nest/Exceptions/FailedToLoadException.h"
#include"Nest/Exceptions/InvalidArgumentException.h"
#include"Nest/Exceptions/InvalidOperationException.h"
#include"Nest/Exceptions/LogicErrorException.h"
#include"Nest/Exceptions/NullArgumentException.h"

#include"Nest/ResourcesTypes.h"
#include"Nest/IResourcesManager.h"
#include"Nest/World.h"



namespace Nest
{
	MetadataManager::MetadataManager(
		std::function<std::unique_ptr<IMetadataInformation>(ResourceType, std::string)> metadataInformationFactoryFunction
	)
	{
		if (metadataInformationFactoryFunction == nullptr)
			ThrowNullArgumentExceptionWithLine("The Metadata Factory function cannot be null");

		m_MetadataInformationFactoryFunction = metadataInformationFactoryFunction;

		// The following lines were needed on g++, to make sure the maps exists
		m_MetadataMap[ResourceType::Font] = std::unordered_map<std::string, std::unique_ptr<IMetadataInformation>>{};
		m_MetadataMap[ResourceType::Material] = std::unordered_map<std::string, std::unique_ptr<IMetadataInformation>>{};
		m_MetadataMap[ResourceType::Model] = std::unordered_map<std::string, std::unique_ptr<IMetadataInformation>>{};
		m_MetadataMap[ResourceType::Shader] = std::unordered_map<std::string, std::unique_ptr<IMetadataInformation>>{};
		m_MetadataMap[ResourceType::Texture] = std::unordered_map<std::string, std::unique_ptr<IMetadataInformation>>{};
	}



	MetadataManager::~MetadataManager() = default;



	void MetadataManager::SetMetadataExtension(std::string extension)
	{
		if (extension.empty())
			ThrowInvalidArgumentExceptionWithLine("Extension cannot be empty");

		m_MetadataFileExtension = std::move(extension);
	}



	std::string MetadataManager::GetMetadataExtension() const
	{
		return (m_MetadataFileExtension);
	}



	void MetadataManager::LoadMetadataFromPath(std::string path)
	{
		for (auto& d : std::experimental::filesystem::recursive_directory_iterator(path))
		{
			const auto& file = d.path();
			if (file.extension() == m_MetadataFileExtension)
			{
				LoadMetadataFile(file.string());
			}
		}
	}



	void MetadataManager::LoadResource(ResourceType resourceType, std::string resourceName, IResourcesManager& resourcesManager)
	{
		if (resourceType == ResourceType::Undefined)
			ThrowInvalidOperationExceptionWithLine("Resource type is undefined.");

		auto& resourceMap = m_MetadataMap.at(resourceType);

		if (resourceMap.count(resourceName) > 0)
		{
			LoadResourceFromMetadata(*resourceMap.at(resourceName), resourcesManager);
		}
		else
		{
			ThrowInvalidOperationExceptionWithLine("Resource of name [" + resourceName + "] was not found.");
		}
	}



	void MetadataManager::LoadAllResources(IResourcesManager& resourcesManager)
	{
		// The order matters
		for (const auto& resource : m_MetadataMap.at(ResourceType::Texture))
		{
			LoadResourceFromMetadata(*resource.second, resourcesManager);
		}
		for (const auto& resource : m_MetadataMap.at(ResourceType::Shader))
		{
			LoadResourceFromMetadata(*resource.second, resourcesManager);
		}
		for (const auto& resource : m_MetadataMap.at(ResourceType::Material))
		{
			LoadResourceFromMetadata(*resource.second, resourcesManager);
		}
		for (const auto& resource : m_MetadataMap.at(ResourceType::Model))
		{
			LoadResourceFromMetadata(*resource.second, resourcesManager);
		}
		for (const auto& resource : m_MetadataMap.at(ResourceType::Font))
		{
			LoadResourceFromMetadata(*resource.second, resourcesManager);
		}
	}



	void MetadataManager::LoadMetadataFile(std::string file)
	{
		YAML::Node mainNode;

		try
		{
			mainNode = YAML::LoadFile(file);
		}
		catch (const std::ifstream::failure&)
		{
			ThrowFailedToLoadExceptionWithLine("Failed to load metadata file.")
		}

		if (mainNode["DataList"])
		{
			LoadMetadataInformationList(mainNode, file);
		}
		else
		{
			LoadSingleMetadataInformation(mainNode, file);
		}
	}



	void MetadataManager::LoadMetadataInformationList(YAML::Node node, std::string metaFileName)
	{
		for (const auto& element : node)
		{
			LoadSingleMetadataInformation(element, metaFileName);
		}
	}



	void MetadataManager::LoadSingleMetadataInformation(const YAML::Node& node, std::string metaFileName)
	{
		// Get the resource name
		std::string resourceName;
		if (node["Name"])
		{
			resourceName = node["Name"].as<std::string>();
		}

		// Get the resource type
		ResourceType resourceType = ResourceType::Undefined;
		if (node["Type"])
		{
			resourceType = GetResourceTypeFromString(node["Type"].as<std::string>());
		}

		// Get the resource path
		std::string resourcePath;
		if (node["Path"])
		{
			resourcePath = node["Path"].as<std::string>();
		}


		// Validate the resource name, type and path.
		if (resourceName.empty())
			ThrowInvalidOperationExceptionWithLine("Resource name is empty.");

		if (resourceType == ResourceType::Undefined)
			ThrowInvalidOperationExceptionWithLine("Resource type is 'undefined'.");

		if (resourcePath.empty())
			ThrowInvalidOperationExceptionWithLine("Resource path is empty.");


		// Load the metadata
		auto metadata = m_MetadataInformationFactoryFunction(resourceType, resourceName);
		if (metadata->GetResourceType() != resourceType)
			ThrowLogicErrorExceptionWithLine("Metadata and expected resource types mismatch");

		if (m_MetadataMap[resourceType].count(resourceName) == 0)
		{
			int separatorIndex = 0;
			for (int i = int(metaFileName.size()) - 1; i >= 0; --i)
			{
				if (metaFileName[i] == '/' || metaFileName[i] == '\\')
				{
					separatorIndex = i;
					break;
				}
			}

			std::string fullPath = metaFileName.substr(0, separatorIndex);
			fullPath += '/' + resourcePath;

			LoadIntoMetadataFromYamlFile(*metadata, fullPath);

			m_MetadataMap[resourceType][resourceName] = std::move(metadata);
		}
		else
		{
			ThrowInvalidOperationExceptionWithLine("There is already a resource with the same name and type registred [" + resourceName + "].");
		}
	}



	void MetadataManager::LoadIntoMetadataFromYamlFile(IMetadataInformation& metadata, std::string metadataFile)
	{
		YAML::Node mainNode;

		try
		{
			mainNode = YAML::LoadFile(metadataFile);
		}
		catch (const std::ifstream::failure&)
		{
			ThrowFailedToLoadExceptionWithLine("Failed to load metadata file.");
		}

		metadata.LoadMetadata(mainNode, metadataFile);
	}



	void MetadataManager::LoadResourceFromMetadata(IMetadataInformation& metadata, IResourcesManager& resourcesManager)
	{
		metadata.LoadResource(resourcesManager);
	}
}

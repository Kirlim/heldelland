
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Metadata/MaterialMetadata.h"

#include"Nest/NestAssert.h"
#include"Nest/Exceptions/FailedToLoadException.h"
#include"Nest/Exceptions/LogicErrorException.h"
#include"Nest/Material.h"
#include"Nest/IResourcesManager.h"



namespace Nest
{
	MaterialMetadata::MaterialMetadata(const std::string resourceName) :
		IMetadataInformation(resourceName)
	{
		// Nothing here
	}



	MaterialMetadata::~MaterialMetadata() = default;



	ResourceType MaterialMetadata::GetResourceType() const
	{
		return(ResourceType::Material);
	}



	void MaterialMetadata::LoadMetadataFromYamlNode(const YAML::Node& node, const std::string metadataPath)
	{
		LoadShaderName(node);
		TryLoadTextureData(node);

		m_IsLoaded = true;
	}



	void MaterialMetadata::LoadResourceIntoManager(IResourcesManager& resourcesManager)
	{
		std::unique_ptr<Material> material = std::make_unique<Material>();

		const auto shader = resourcesManager.FetchShaderProgram(m_ShaderName);
		if (shader == nullptr)
			ThrowLogicErrorExceptionWithLine("Shader Program of name " + m_ShaderName + " was not found in the resources manager.");

		material->SetShaderProgram(shader);


		for(unsigned int i = 0; i < m_TextureSlotNames.size(); ++i)
		{
			const auto texture = resourcesManager.FetchTexture(m_TextureSlotResources[i]);

			if (texture == nullptr)
				ThrowLogicErrorExceptionWithLine("Texture of name " + m_TextureSlotResources[i] + " was not found in the resources manager.");

			material->SetTexture(m_TextureSlotNames[i], texture);
		}


		resourcesManager.AddMaterial(m_ResourceName, material);
	}



	void MaterialMetadata::LoadShaderName(const YAML::Node& mainNode)
	{
		if (mainNode["Shader"])
		{
			m_ShaderName = mainNode["Shader"].as<std::string>();
		}

		if (m_ShaderName.empty())
		{
			ThrowFailedToLoadExceptionWithLine("Shader Name is set up as empty.");
		}
	}



	void MaterialMetadata::TryLoadTextureData(const YAML::Node& mainNode)
	{
		if (mainNode["Textures"])
		{
			for (const auto& textureNode : mainNode["Textures"])
			{
				std::string slotName;
				std::string textureResourceName;

				if (textureNode["Slot"])
				{
					slotName = textureNode["Slot"].as<std::string>();
				}

				if (textureNode["Texture"])
				{
					textureResourceName = textureNode["Texture"].as<std::string>();
				}

				if (slotName.empty())
				{
					ThrowFailedToLoadExceptionWithLine("Texture Slot name is set up as empty.");
				}

				if (textureResourceName.empty())
				{
					ThrowFailedToLoadExceptionWithLine("Texture Resource Name is set up as empty.");
				}

				m_TextureSlotNames.push_back(slotName);
				m_TextureSlotResources.push_back(textureResourceName);
				NestAssert(m_TextureSlotNames.size() == m_TextureSlotResources.size());
			}
		}
	}
}

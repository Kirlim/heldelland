
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Log.h"



namespace Nest
{
    namespace Log
    {
        extern std::function<void(std::string)> LogMessageFunction = nullptr;
        extern std::function<void(std::string)> LogWarningFunction = nullptr;
        extern std::function<void(std::string)> LogErrorFunction = nullptr;



        void Message(std::string message)
        {
            if (LogMessageFunction != nullptr) LogMessageFunction(std::move(message));
        }



        void Warning(std::string message)
        {
            if (LogWarningFunction != nullptr) LogWarningFunction(std::move(message));
        }



        void Error(std::string message)
        {
            if (LogErrorFunction != nullptr) LogErrorFunction(std::move(message));
        }



        void SetLogMessageFunction(std::function<void(std::string)> function)
        {
            LogMessageFunction = std::move(function);
        }



        void SetLogWarningFunction(std::function<void(std::string)> function)
        {
            LogWarningFunction = std::move(function);
        }



        void SetLogErrorFunction(std::function<void(std::string)> function)
        {
            LogErrorFunction = std::move(function);
        }
    }
}
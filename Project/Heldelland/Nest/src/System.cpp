
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/System.h"



namespace Nest
{
	System::System() :
		m_ExecutionOrder(0)
	{
	}



	System::~System() = default;



	void System::SetExecutionOrder(int newExecutionOrder)
	{
		this->m_ExecutionOrder = newExecutionOrder;
	}



	int System::GetExecutionOrder() const
	{
		return(m_ExecutionOrder);
	}



	void System::FixedUpdate(float fixedDeltaTime)
	{
		DoFixedUpdate(fixedDeltaTime);
	}



	void System::Update(float deltaTime)
	{
		DoUpdate(deltaTime);
	}
}
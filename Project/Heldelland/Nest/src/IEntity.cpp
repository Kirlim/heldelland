//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/IEntity.h"

#include"Nest/Exceptions/NullArgumentException.h"
#include"Nest/NestAssert.h"
#include"Nest/IComponentsFactory.h"
#include"Nest/Global.h"
#include"Nest/Component.h"



namespace Nest
{
	IEntity::IEntity()
	{
	}

	IEntity::~IEntity()
	{
	}



	ComponentOwnerReferenceBase* IEntity::AddComponent(std::unique_ptr<ComponentOwnerReferenceBase>& componentReference)
	{
		if (componentReference == nullptr)
			ThrowNullArgumentExceptionWithLine("Component reference must be a valid unique pointer.");

		// TODO : validate that no other component of same name is registered

		const auto componentAddress = componentReference.get();

		componentReference.get()->Get()->SetOwner(this);
		componentReference.get()->Get()->OnCreate();

		const int componentTypeId = componentReference->typeId;
		DoAddComponent(componentReference, componentTypeId);
		NestAssert(componentReference == nullptr);

		return componentAddress;
	}



	ComponentOwnerReferenceBase* IEntity::AddComponent(std::string typeName)
	{
		auto componentReference = DoInstantiateComponent(typeName);
		NestAssert(componentReference != nullptr);

		return (AddComponent(componentReference));
	}



	auto IEntity::GetComponent(std::string componentTypeName) const -> ComponentOwnerReferenceBase*

	
	{
		return (DoGetComponent(componentTypeName));
	}



	auto IEntity::GetComponent(ComponentTypeId componentTypeId) const -> ComponentOwnerReferenceBase*

	
	{
		return (DoGetComponent(componentTypeId));
	}



	auto IEntity::GetComponentByName(std::string componentName) const -> ComponentOwnerReferenceBase*

	
	{
		return (DoGetComponentByName(componentName));
	}



	void IEntity::RemoveComponent(std::string componentTypeName)
	{
		const auto componentTypeId = ComponentTypeMap::GetInstance()->GetTypeId(componentTypeName);
		RemoveComponent(componentTypeId);
	}



	void IEntity::RemoveComponent(ComponentTypeId componentTypeId)
	{
		const auto componentRef = GetComponent(componentTypeId);

		// TODO : Error if typeId is -1

		// TODO : condition for removal
		if (true) //auto component = componentRef.lock())
		{
			componentRef->Get()->OnDestroy();
		}
		else
		{
			// TODO : Error if component pointer is expired or null
		}

		DoRemoveComponent(componentTypeId);
	}



	void IEntity::RemoveComponentByName(std::string componentName)
	{
		const auto componentRef = GetComponentByName(componentName);

		// TODO : condition for removal
		if (true) //auto component = componentRef.lock())
		{
			componentRef->Get()->OnDestroy();
		}
		else
		{
			// TODO : Error if component pointer is expired or null
		}

		DoRemoveComponentByName(componentName);
	}



	EntityId IEntity::Id() const
	{
		return DoGetId();
	}



	void IEntity::SetId(EntityId id)
	{
		DoSetId(id);
	}



	void IEntity::Destroy()
	{
		DoDestroy();
	}
}

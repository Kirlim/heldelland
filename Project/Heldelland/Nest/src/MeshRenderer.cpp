
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/MeshRenderer.h"



namespace Nest
{
    void MeshRenderer::SetMaterial(Material* material)
    {
        m_Material = material;
    }



    Material* MeshRenderer::GetMaterial() const
    {
        return(m_Material);
    }



    void MeshRenderer::SetGuiFlag(const bool isGui)
    {
        m_IsGui = isGui;
    }



    bool MeshRenderer::IsGui() const
    {
        return(m_IsGui);
    }



    void MeshRenderer::OnDestroy()
    {
        // Nothing here
    }
}
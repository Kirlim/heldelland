//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Exceptions/ValueOutOfRangeException.h"



namespace Nest
{
	ValueOutOfRangeException::ValueOutOfRangeException(const std::string& message) : std::logic_error(message)
	{
	}



	ValueOutOfRangeException::ValueOutOfRangeException(const char* message) : std::logic_error(message)
	{
	}
}

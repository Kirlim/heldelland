//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Exceptions/NotRegisteredException.h"



namespace Nest
{
	NotRegisteredException::NotRegisteredException(const std::string& message) : std::logic_error(message)
	{
	}



	NotRegisteredException::NotRegisteredException(const char* message) : std::logic_error(message)
	{
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Exceptions/RuntimeException.h"



namespace Nest
{
	RuntimeException::RuntimeException(const std::string& message) : std::runtime_error(message)
	{
	}



	RuntimeException::RuntimeException(const char* message) : std::runtime_error(message)
	{
	}
}

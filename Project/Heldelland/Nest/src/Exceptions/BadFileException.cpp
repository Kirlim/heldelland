//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Exceptions/BadFileException.h"



namespace Nest
{
	BadFileException::BadFileException(const std::string& message) : std::runtime_error(message)
	{
	}



	BadFileException::BadFileException(const char* message) : std::runtime_error(message)
	{
	}
}

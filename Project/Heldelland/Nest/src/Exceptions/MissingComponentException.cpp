//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Exceptions/MissingComponentException.h"



namespace Nest
{
	MissingComponentException::MissingComponentException(const std::string& message) : std::logic_error(message)
	{
	}



	MissingComponentException::MissingComponentException(const char* message) : std::logic_error(message)
	{
	}
}

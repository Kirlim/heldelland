//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/ISingleComponentsContainer.h"

#include"Nest/Exceptions/NullArgumentException.h"

#include"Nest/ComponentContainerEvents.h"
#include"Nest/Events/EventsBusToken.h"



namespace Nest
{
	ISingleComponentsContainer::~ISingleComponentsContainer() = default;



	int ISingleComponentsContainer::GetElementsCount() const
	{
		return DoGetElementsCount();
	}



	std::unique_ptr<EventsBusToken> ISingleComponentsContainer::RegisterOnComponentAddedListener(
		const std::function<void(const ComponentAddedEvent&)> listener
	)
	{
		if (listener == nullptr)
			ThrowNullArgumentExceptionWithLine("Listener for component added event cannot be null.");

		return DoRegisterOnComponentAddedListener(listener);
	}



	std::unique_ptr<EventsBusToken> ISingleComponentsContainer::RegisterOnComponentDestroyedListener(
		const std::function<void(const ComponentDestroyedEvent&)> listener
	)
	{
		if (listener == nullptr)
			ThrowNullArgumentExceptionWithLine("Listener for component added event cannot be null.");

		return DoRegisterOnComponentDestroyedListener(listener);
	}
}

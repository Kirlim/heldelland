
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/MeshFilter.h"



namespace Nest
{
    MeshFilter::MeshFilter() : Component(),
        m_Mesh(nullptr)
    {
        // Nothing here
    }



    MeshFilter::~MeshFilter()
    {
        // Nothing here
    }



    Mesh* MeshFilter::GetMesh() const
    {
        return(m_Mesh);
    }



    void MeshFilter::SetMesh(Mesh* mesh)
    {
        m_Mesh = mesh;
    }



    void MeshFilter::OnDestroy()
    {
        // Nothing here
    }
}
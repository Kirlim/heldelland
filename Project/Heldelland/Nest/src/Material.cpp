//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"Nest/Material.h"

#include"Nest/Exceptions/InvalidOperationException.h"



namespace Nest
{
	Material::Material() :
		m_ShaderProgram(nullptr)
	{
	}



	Material::~Material()
	{
		Clear();
	}



	void Material::SetShaderProgram(ShaderProgram* shaderProgram)
	{
		Clear();
		m_ShaderProgram = shaderProgram;
	}



	void Material::SetMainTexture(const Texture* texture)
	{
		if (m_ShaderProgram == nullptr)
			ThrowInvalidOperationExceptionWithLine("No shader program has been set.");


		// TODO : better way to check if the shader has a main texture slot
		if (m_ShaderProgram->GetMainTextureName().length() == 0)
			ThrowInvalidOperationExceptionWithLine("Set shader program does not have a main texture slot.");


		m_TexturesSlots[m_ShaderProgram->GetTextureSlot(m_ShaderProgram->GetMainTextureName())] = texture;
	}



	void Material::SetTexture(std::string slotName, const Texture* texture)
	{
		if (m_ShaderProgram == nullptr)
			ThrowInvalidOperationExceptionWithLine("No shader program has been set.");


		const GLint slot = m_ShaderProgram->GetTextureSlot(slotName);
		m_TexturesSlots[slot] = texture;
	}



	ShaderProgram* Material::GetShaderProgram() const
	{
		return m_ShaderProgram;
	}



	void Material::LoadForRendering()
	{
		if (m_ShaderProgram != nullptr)
		{
			m_ShaderProgram->Use();
			const int currentTextureIndex = 0;
			for (const auto& textureSlot : *(m_ShaderProgram->GetTexturesSlots()))
			{
				if (m_TexturesSlots.count(textureSlot.second) > 0)
				{
					if (m_TexturesSlots[textureSlot.second] == nullptr) continue;

					glActiveTexture(GL_TEXTURE0 + currentTextureIndex);
					glBindTexture(GL_TEXTURE_2D, m_TexturesSlots[textureSlot.second]->GetTextureId());
					// TODO : confirm that the next line is or isnt needed.
					//glUniform1i(textureSlot.second, currentTextureIndex);
				}
			}
		}
		else
		{
			// TODO : decide what to do if shader program is null
		}
	}



	void Material::UnloadFromRendering()
	{
		if (m_ShaderProgram != nullptr)
		{
			m_ShaderProgram->Use();
			for (const auto& textureSlot : *(m_ShaderProgram->GetTexturesSlots()))
			{
				if (m_TexturesSlots.count(textureSlot.second) > 0)
				{
					if (m_TexturesSlots[textureSlot.second] == nullptr) continue;
					glBindTexture(GL_TEXTURE_2D, 0);
				}
			}
		}
		else
		{
			// TODO : decide what to do if shader program is null
		}
	}



	void Material::Clear()
	{
		m_ShaderProgram = nullptr;
		m_TexturesSlots.clear();
	}
}

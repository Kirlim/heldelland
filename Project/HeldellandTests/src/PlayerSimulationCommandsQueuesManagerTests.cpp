
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<memory>

#include<Game/Simulation/ClientSimulationCommandsQueue.h>
#include<Game/Simulation/PlayerSimulationCommandsQueuesManager.h>

#include"Mocks/ClientSimulationCommandMock.h"

using namespace Game;



namespace Game_PlayerSimulationCommandsQueuesManager_Tests
{
	TEST_CASE("Adding a command to a queue and then committing results in a queue to be fetched.", "[PlayerSimulationCommandsQueuesManager]")
	{
		PlayerSimulationCommandsQueuesManager queuesManager{};

		SECTION("Adding a command")
		{
			REQUIRE(queuesManager.GetCurrentQueueSize() == 0);
			queuesManager.AppendCommand(std::make_unique<ClientSimulationCommandMock>("message"));
			REQUIRE(queuesManager.GetCurrentQueueSize() == 1);
		}

		SECTION("Committing a queue")
		{
			queuesManager.AppendCommand(std::make_unique<ClientSimulationCommandMock>("message"));
			REQUIRE_FALSE(queuesManager.HasCommittedQueues());
			REQUIRE_NOTHROW(queuesManager.CommitQueue());
			REQUIRE(queuesManager.HasCommittedQueues());
		}

		SECTION("Committing two queues")
		{
			queuesManager.AppendCommand(std::make_unique<ClientSimulationCommandMock>("message from queue 1"));
			queuesManager.CommitQueue();

			queuesManager.AppendCommand(std::make_unique<ClientSimulationCommandMock>("message from queue 2"));
			queuesManager.CommitQueue();

			REQUIRE(queuesManager.CommittedQueuesCount() == 2);
		}

		SECTION("Commiting an empty queue")
		{
			REQUIRE_NOTHROW(queuesManager.CommitQueue());
		}
	}

	TEST_CASE("Adding a command but then clearing the queue results in empty queue", "[PlayerSimulationCommandsQueuesManager]")
	{
		PlayerSimulationCommandsQueuesManager queuesManager;
		queuesManager.AppendCommand(std::make_unique<ClientSimulationCommandMock>("message"));
		queuesManager.ClearQueue();
		REQUIRE(queuesManager.GetCurrentQueueSize() == 0);
	}

	TEST_CASE("Fetching a committed queue, when there is no queue committed, returns null.", "[PlayerSimulationCommandsQueuesManager]")
	{
		PlayerSimulationCommandsQueuesManager queuesManager;
		auto queue = queuesManager.FetchNextCommittedQueue();
		REQUIRE(queue == nullptr);
	}

	TEST_CASE("Committing a queue results in a queue with all the commands who have been added.", "[PlayerSimulationCommandsQueuesManager]")
	{
		PlayerSimulationCommandsQueuesManager queuesManager;
		queuesManager.AppendCommand(std::make_unique<ClientSimulationCommandMock>("message1"));
		queuesManager.AppendCommand(std::make_unique<ClientSimulationCommandMock>("message2"));

		REQUIRE_NOTHROW(queuesManager.CommitQueue());
		REQUIRE(queuesManager.CommittedQueuesCount() == 1);

		std::unique_ptr<ClientSimulationCommandsQueue> queue;
		REQUIRE_NOTHROW(queue = queuesManager.FetchNextCommittedQueue());
		REQUIRE(queue->Size() == 2);

		auto message1 = queue->Dequeue();
		auto message2 = queue->Dequeue();

		auto castedMessage1 = dynamic_cast<ClientSimulationCommandMock*>(message1.get());
		auto castedMessage2 = dynamic_cast<ClientSimulationCommandMock*>(message2.get());

		REQUIRE(castedMessage1 != nullptr);
		REQUIRE(castedMessage2 != nullptr);

		REQUIRE(castedMessage1->message == "message1");
		REQUIRE(castedMessage2->message == "message2");
	}
}
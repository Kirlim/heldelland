
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<stdexcept>

#include<Game/Simulation/Types/PlayerSimInfoId.h>
#include<Game/Simulation/PlayersSimulationsManager.h>

using namespace Game;



namespace PlayersSimulationsManagerTests
{
	TEST_CASE("Registering new players", "[PlayersSimulationsManager]")
	{
		PlayersSimulationsManager playersSimulationsManagers;

		SECTION("The Player ID is not yet registered.")
		{
			PlayerSimInfoId playerId{ 1 };
			playersSimulationsManagers.Register(playerId);
			REQUIRE(playersSimulationsManagers.IsRegistered(playerId));
		}

		SECTION("Player ID already registered, causing exception.")
		{
			const PlayerSimInfoId playerId{ 1 };
			playersSimulationsManagers.Register(playerId);
			REQUIRE_THROWS(playersSimulationsManagers.Register(playerId));
		}
	}

	TEST_CASE("Checking if player is already registered returns false when player is not registered", "[PlayersSimulationsManager]")
	{
		const PlayersSimulationsManager playersSimulationsManagers;
		PlayerSimInfoId playerId{ 1 };
		REQUIRE_FALSE(playersSimulationsManagers.IsRegistered(playerId));
	}

	TEST_CASE("Unregistering from Player ID", "[PlayersSimulationsManager]")
	{
		PlayersSimulationsManager playersSimulationsManagers;

		SECTION("Will unregister Player ID of already registered player.")
		{
			PlayerSimInfoId playerId{ 1 };
			playersSimulationsManagers.Register(playerId);
			REQUIRE_NOTHROW(playersSimulationsManagers.Unregister(playerId));
			REQUIRE_FALSE(playersSimulationsManagers.IsRegistered(playerId));
		}

		SECTION("Will throw when trying to unregister not registered Player ID.")
		{
			const PlayerSimInfoId playerId{ 1 };
			REQUIRE_THROWS(playersSimulationsManagers.Unregister(playerId));
		}
	}

	TEST_CASE("Fetching player simulation commands queues manager", "[PlayersSimulationsManager]")
	{
		PlayersSimulationsManager playersSimulationsManagers;

		SECTION("Will return simulation commands queues manager of registered id.")
		{
			const PlayerSimInfoId playerId{ 1 };
			playersSimulationsManagers.Register(playerId);
			auto commandsQueueManager = playersSimulationsManagers.GetPlayerCommandsQueueManager(playerId);
			REQUIRE(commandsQueueManager != nullptr);
		}

		SECTION("Will return null simulation commands queues manager, for not registered ids.")
		{
			const PlayerSimInfoId playerId{ 1 };
			auto commandsQueueManager = playersSimulationsManagers.GetPlayerCommandsQueueManager(playerId);
			REQUIRE(commandsQueueManager == nullptr);
		}
	}

	TEST_CASE("ForEach is called for all simulations", "[PlayersSimulationsManager]")
	{
		PlayersSimulationsManager playersSimulationsManagers;

		const PlayerSimInfoId playerId1{ 1 };
		const PlayerSimInfoId playerId2{ 2 };

		playersSimulationsManagers.Register(playerId1);
		playersSimulationsManagers.Register(playerId2);

		auto simulationCommandsQueuesManager1 = playersSimulationsManagers.GetPlayerCommandsQueueManager(playerId1);
		auto simulationCommandsQueuesManager2 = playersSimulationsManagers.GetPlayerCommandsQueueManager(playerId2);

		bool playerSimulation1Accessed = false;
		bool playerSimulation2Accessed = false;

		playersSimulationsManagers.ForEach(
			[&](PlayerSimulationCommandsQueuesManager* simulationCommandsQueuesManager)
		{
			if (simulationCommandsQueuesManager == simulationCommandsQueuesManager1) playerSimulation1Accessed = true;
			else if (simulationCommandsQueuesManager == simulationCommandsQueuesManager2) playerSimulation2Accessed = true;
			else throw new std::runtime_error("Unknown simulation detected.");
		}
		);

		REQUIRE(playerSimulation1Accessed);
		REQUIRE(playerSimulation2Accessed);
	}
}

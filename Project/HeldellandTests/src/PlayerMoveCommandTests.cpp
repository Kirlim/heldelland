
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<Nest/Vector2.h>

#include<Game/Simulation/ServerCommands/PlayerMoveCommand.h>
#include<Game/Network/SunaNetworkWriteSerializer.h>
#include<Game/Network/SunaNetworkReadSerializer.h>

using namespace Nest;
using namespace Game;



namespace PlayerMoveCommandTests
{
	TEST_CASE("PlayerMoveCommand serializes and deserializes properly.", "[PlayerMoveCommand]")
	{
		Vector2<simfix64> destination(simfix64(123.45), simfix64(-54.321));

		PlayerMoveCommand playerMoveCommand;
		playerMoveCommand.SetDestination(destination);

		unsigned char buffer[100];
		SunaNetworkWriteSerializer writer{ buffer,0,100 };
		REQUIRE(playerMoveCommand.Serialize(writer));

		SunaNetworkReadSerializer reader{ buffer, 0, 100 };
		PlayerMoveCommand recoveredPlayerMoveCommand;
		REQUIRE(recoveredPlayerMoveCommand.Serialize(reader));

		REQUIRE(recoveredPlayerMoveCommand.GetDestination() == destination);
	}
}

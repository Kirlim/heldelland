
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef HELPERS__GAME_SIMULATION_SETUP_HELPER_H
#define HELPERS__GAME_SIMULATION_SETUP_HELPER_H

#include<memory>



namespace Nest
{
	class IComponentsContainers;
	class World;
	template <typename T> class ITemplatedSingleComponentsContainer;
}

namespace Game
{
	// Forward declarations
	class GameSimulation;
	class ClientGameSimulation;
	class ServerGameSimulation;
	class SimActorMovementBehavior;
}



class GameSimulationSetupHelper
{
public:
	~GameSimulationSetupHelper();

	std::unique_ptr<Game::ClientGameSimulation> InstantiateGameClientSimulation();
	std::unique_ptr<Game::ServerGameSimulation> InstantiateGameServerSimulation(Nest::ITemplatedSingleComponentsContainer<Game::SimActorMovementBehavior>& simActorMovementBehaviorsContainer) const;

public:
	std::unique_ptr<Nest::World> world;
	Nest::IComponentsContainers* componentsContainers;
};

#endif
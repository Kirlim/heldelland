//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef HELPERS__NETWORK_SETUP_HELPER_H
#define HELPERS__NETWORK_SETUP_HELPER_H

#include<memory>



namespace Game
{
	// Forward declarations
	class NetworkMessagesHandlersManager;
}



class NetworkSetupHelper
{
public:
	NetworkSetupHelper();

	Game::NetworkMessagesHandlersManager* GetNetworkMessagesHandlersManager() const;



protected:
	void AddHandlers() const;



protected:
	std::unique_ptr<Game::NetworkMessagesHandlersManager> networkMessagesHandlersManager;
};



#endif

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"GameSimulationSetupHelper.h"

#include<Nest/IComponentsContainers.h>
#include<Nest/ITemplatedSingleComponentsContainer.h>
#include<Nest/Events/EventsBusToken.h>
#include<Nest/World.h>

#include<Game/SimulationCommons/SimulationActorMovementSystem.h>
#include<Game/SimulationCommons/SimulationTypes.h>
#include<Game/Simulation/ClientGameSimulation.h>
#include<Game/Simulation/ServerGameSimulation.h>

// TODO : figure out a way of not needing these includes here
#include<Game/SimulationCommons/ActorsManager.h>
#include"Game/Simulation/ISimActorsSpawner.h"
#include<Game/View/IViewActorsSpawner.h>
#include<Game/Map/Map.h>
#include<Game/Pathfinding/AStar.h>
#include<Game/WorldFactory.h>



GameSimulationSetupHelper::~GameSimulationSetupHelper() = default;



std::unique_ptr<Game::ClientGameSimulation> GameSimulationSetupHelper::InstantiateGameClientSimulation()
{
	if (world == nullptr)
	{
		Game::WorldFactory worldFactory{};
		world = worldFactory.InstantiateWorld();
		componentsContainers = world->GetComponentsContainers();
	}

	return std::make_unique<Game::ClientGameSimulation>(*world, nullptr, nullptr, nullptr, nullptr, nullptr, Game::simfix64{ 0.1f }, 0.2f, 1.1f);
}



std::unique_ptr<Game::ServerGameSimulation> GameSimulationSetupHelper::InstantiateGameServerSimulation(
	Nest::ITemplatedSingleComponentsContainer<Game::SimActorMovementBehavior>& simActorMovementBehaviorsContainer
) const
{
	auto serverSimulation = std::make_unique<Game::ServerGameSimulation>(nullptr, nullptr, nullptr, nullptr, simActorMovementBehaviorsContainer, nullptr);
	return serverSimulation;
}

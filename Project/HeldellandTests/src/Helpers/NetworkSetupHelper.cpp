//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"NetworkSetupHelper.h"

#include<Game/Network/NetworkMessagesHandlersManager.h>

#include<Game/Network/Messages/GenericMessagesHandler.h>

#include<Game/Network/Messages/GameSimulationSettingsMessage.h>
#include<Game/Network/Messages/GameSimulationLoadedMessage.h>

#include<Game/Network/Messages/JoinGameRequestMessage.h>
#include<Game/Network/Messages/JoinGameReplyMessage.h>

#include<Game/Simulation/ClientCommands/ForceNextTickValueCommand.h>
#include<Game/Simulation/ClientCommands/SimulationTickStartCommand.h>
#include<Game/Simulation/ClientCommands/SimulationTickEndCommand.h>

#include<Game/Simulation/ClientCommands/SetActorFullPathCommand.h>
#include<Game/Simulation/ClientCommands/SetActorMovementSpeedCommand.h>
#include<Game/Simulation/ClientCommands/SetActorPositionCommand.h>
#include<Game/Simulation/ClientCommands/SpawnActorCommand.h>
#include<Game/Simulation/ClientCommands/SpawnPlayerCharacterCommand.h>

#include<Game/Simulation/ServerCommands/PlayerMoveCommand.h>



NetworkSetupHelper::NetworkSetupHelper()
{
	networkMessagesHandlersManager = std::make_unique<Game::NetworkMessagesHandlersManager>();

	AddHandlers();
}



Game::NetworkMessagesHandlersManager* NetworkSetupHelper::GetNetworkMessagesHandlersManager() const
{
	return networkMessagesHandlersManager.get();
}



void NetworkSetupHelper::AddHandlers() const
{
	// Client commands
	auto joinGameRequestMessagesHandlerUPtr = std::make_unique<Game::GenericMessagesHandler<Game::JoinGameRequestMessage>>(Game::JoinGameRequestMessage::TypeId);
	networkMessagesHandlersManager->AddHandler(std::move(joinGameRequestMessagesHandlerUPtr));

	auto joinGameReplyMessagesHandlerUPtr = std::make_unique<Game::GenericMessagesHandler<Game::JoinGameReplyMessage>>(Game::JoinGameReplyMessage::TypeId);
	networkMessagesHandlersManager->AddHandler(std::move(joinGameReplyMessagesHandlerUPtr));

	auto gameSimulationSettingsMessagesHandlerUPtr = std::make_unique<Game::GenericMessagesHandler<Game::GameSimulationSettingsMessage>>(Game::GameSimulationSettingsMessage::TypeId);
	networkMessagesHandlersManager->AddHandler(std::move(gameSimulationSettingsMessagesHandlerUPtr));

	auto gameSimulationLoadedMessagesHandlerUPtr = std::make_unique<Game::GenericMessagesHandler<Game::GameSimulationLoadedMessage>>(Game::GameSimulationLoadedMessage::TypeId);
	networkMessagesHandlersManager->AddHandler(std::move(gameSimulationLoadedMessagesHandlerUPtr));

	auto forceNextTickValueCommandMessagesHandlerUPtr = std::make_unique<Game::GenericMessagesHandler<Game::ForceNextTickValueCommand>>(Game::ForceNextTickValueCommand::TypeId);
	networkMessagesHandlersManager->AddHandler(std::move(forceNextTickValueCommandMessagesHandlerUPtr));

	auto simulationTickStartCommandMessagesHandlerUPtr = std::make_unique<Game::GenericMessagesHandler<Game::SimulationTickStartCommand>>(Game::SimulationTickStartCommand::TypeId);
	networkMessagesHandlersManager->AddHandler(std::move(simulationTickStartCommandMessagesHandlerUPtr));

	auto simulationTickEndCommandMessagesHandlerUPtr = std::make_unique<Game::GenericMessagesHandler<Game::SimulationTickEndCommand>>(Game::SimulationTickEndCommand::TypeId);
	networkMessagesHandlersManager->AddHandler(std::move(simulationTickEndCommandMessagesHandlerUPtr));



	auto setActorFullPathCommandMessagesHandlerUPtr = std::make_unique<Game::GenericMessagesHandler<Game::ClientSimulationCommand>>(Game::SetActorFullPathCommand::TypeId);
	networkMessagesHandlersManager->AddHandler(std::move(setActorFullPathCommandMessagesHandlerUPtr));

	auto setActorMovementSpeedCommandMessagesHandrelUPtr = std::make_unique<Game::GenericMessagesHandler<Game::ClientSimulationCommand>>(Game::SetActorMovementSpeedCommand::TypeId);
	networkMessagesHandlersManager->AddHandler(std::move(setActorMovementSpeedCommandMessagesHandrelUPtr));

	auto setActorPositionCommandMessagesHandlerUPtr = std::make_unique<Game::GenericMessagesHandler<Game::ClientSimulationCommand>>(Game::SetActorPositionCommand::TypeId);
	networkMessagesHandlersManager->AddHandler(std::move(setActorPositionCommandMessagesHandlerUPtr));

	auto spawnActorCommandMessagesHandlerUPtr = std::make_unique<Game::GenericMessagesHandler<Game::ClientSimulationCommand>>(Game::SpawnActorCommand::TypeId);
	networkMessagesHandlersManager->AddHandler(std::move(spawnActorCommandMessagesHandlerUPtr));

	auto spawnPlayerCharacterCommandMessagesHandlerUPtr = std::make_unique<Game::GenericMessagesHandler<Game::SpawnPlayerCharacterCommand>>(Game::SpawnPlayerCharacterCommand::TypeId);
	networkMessagesHandlersManager->AddHandler(std::move(spawnPlayerCharacterCommandMessagesHandlerUPtr));



	// Server commands
	auto playerMoveCommandMessagesHandlerUPtr = std::make_unique<Game::GenericMessagesHandler<Game::ServerSimulationCommand>>(Game::PlayerMoveCommand::TypeId);
	networkMessagesHandlersManager->AddHandler(std::move(playerMoveCommandMessagesHandlerUPtr));
}

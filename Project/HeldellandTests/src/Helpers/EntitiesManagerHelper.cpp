
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"EntitiesManagerHelper.h"

#include<Nest/ComponentsFactory.h>

#include<Game/WorldFactory.h>



namespace Helpers
{
	std::unique_ptr<Nest::World> InstantiateWorld()
	{
		Game::WorldFactory worldFactory{};
		auto world = worldFactory.InstantiateWorld();
		return world;
	}
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef HELPERS__ENTITIES_MANAGER_HELPER_H
#define HELPERS__ENTITIES_MANAGER_HELPER_H

#include<memory>

#include<Nest/ComponentTypeMap.h>
#include<Nest/SimpleStoringFactoryWrap.h>
#include<Nest/ArrayStoringFactory.h>
#include<Nest/IComponentsFactory.h>
#include<Nest/World.h>

// TODO : rename file to WorldHelper



namespace Helpers
{
	std::unique_ptr<Nest::World> InstantiateWorld();

	template <typename ComponentType>
	void RegisterComponentFactory(Nest::ComponentTypeMap& componentTypeMap, const Nest::World& world)
	{
		auto componentsFactory = world.GetComponentsFactory();

		auto typeId = componentTypeMap.GetTypeId<ComponentType>();
		auto componentFactory = std::make_unique<Nest::ArrayStoringFactory<ComponentType>>();
		auto componentWrap = std::make_unique<Nest::SimpleStoringFactoryWrap<ComponentType>>(
			typeId, ComponentType::GetTypeName(), std::move(componentFactory)
			);

		componentsFactory->RegisterFactory(typeId, ComponentType::GetTypeName(), std::move(componentWrap));
	}
}



#endif

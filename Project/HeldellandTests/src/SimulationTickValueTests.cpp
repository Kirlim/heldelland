
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<Game/SimulationCommons/SimulationTickValue.h>
using namespace Game;



namespace SimulationTickValueTests
{
	TEST_CASE("SimulationTickValue::GetNext returns the next tick", "[SimulationTickValue]")
	{
		SimulationTickValue tick{};

		SECTION("Next from 0")
		{
			tick.SetCurrent(0);
			auto nextTick = tick.GetNext();
			REQUIRE(nextTick == 1);
		}

		SECTION("Next from some number in the middle")
		{
			tick.SetCurrent(100);
			auto nextTick = tick.GetNext();
			REQUIRE(nextTick == 101);
		}

		SECTION("Next from last number (wrap)")
		{
			tick.SetCurrent(SimulationTickValue::MaxTickValue);
			auto nextTick = tick.GetNext();
			REQUIRE(nextTick == 0);
		}
	}



	TEST_CASE("SimulationTickValue::GetPrevious returns the previous tick", "[SimulationTickValue]")
	{
		SimulationTickValue tick{};

		SECTION("Previous from max value")
		{
			tick.SetCurrent(SimulationTickValue::MaxTickValue);
			auto previousTick = tick.GetPrevious();
			REQUIRE(previousTick == (SimulationTickValue::MaxTickValue - 1));
		}

		SECTION("Previous from some number in the middle")
		{
			tick.SetCurrent(100);
			auto previousTick = tick.GetPrevious();
			REQUIRE(previousTick == 99);
		}

		SECTION("Previous from 0")
		{
			tick.SetCurrent(0);
			auto previousTick = tick.GetPrevious();
			REQUIRE(previousTick == SimulationTickValue::MaxTickValue);
		}
	}



	TEST_CASE("SimulationTickValue::Increment increments the tick value", "[SimulationTickValue]")
	{
		SimulationTickValue tick{};

		SECTION("Increment from 0")
		{
			tick.SetCurrent(0);
			tick.Increment();
			REQUIRE(tick.GetCurrent() == 1);
		}

		SECTION("Increment from some number in the middle")
		{
			tick.SetCurrent(100);
			tick.Increment();
			REQUIRE(tick.GetCurrent() == 101);
		}

		SECTION("Increment from max value")
		{
			tick.SetCurrent(SimulationTickValue::MaxTickValue);
			tick.Increment();
			REQUIRE(tick.GetCurrent() == 0);
		}
	}
}

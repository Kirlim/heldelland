
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<vector>
#include<string>

#include<Nest/NestAssert.h>
#include<Nest/Exceptions/InvalidArgumentException.h>
#include<Nest/Exceptions/InvalidOperationException.h>
#include<Nest/ComponentsContainers.h>
using namespace Nest;

#include<Game/DevelopersConsoleSystem.h>
using namespace Game;



void CommandSetInt(const std::vector<std::string> command, int& refInt)
{
	NestAssert(command.size() == 2);

	if (command[0] == "set_int")
	{
		refInt = std::atoi(command[1].c_str());
	}
}



void CommandClearInt(const std::vector<std::string> command, int& refInt)
{
	NestAssert(command.size() == 1);

	if (command[0] == "clear_int")
	{
		refInt = 0;
	}
}



namespace DevelopersConsoleSystemTests
{
	TEST_CASE("DevelopersConsoleSystem::RegisterCommand", "[DevelopersConsoleSystem]")
	{
		const ComponentsContainers componentsContainers{};
		DevelopersConsoleSystem developersConsoleSystem{ componentsContainers };

		SECTION("Registering a command allows for its execution")
		{
			int integer = 0;
			developersConsoleSystem.RegisterCommand("set_int", [&](const std::vector<std::string> command) { CommandSetInt(command, integer);});
			developersConsoleSystem.ExecuteLine("set_int 100");

			REQUIRE(integer == 100);
		}

		SECTION("Registering two commands allows for their execution")
		{
			int integer = 0;

			developersConsoleSystem.RegisterCommand("set_int", [&](const std::vector<std::string> command) { CommandSetInt(command, integer);});
			developersConsoleSystem.RegisterCommand("clear_int", [&](const std::vector<std::string> command) { CommandClearInt(command, integer);});

			developersConsoleSystem.ExecuteLine("set_int 100");
			REQUIRE(integer == 100);

			developersConsoleSystem.ExecuteLine("clear_int");
			REQUIRE(integer == 0);
		}

		SECTION("Registering the same command twice result in InvalidArgumentException")
		{
			int integer = 0;
			developersConsoleSystem.RegisterCommand("set_int", [&](const std::vector<std::string> command) { CommandSetInt(command, integer);});
			REQUIRE_THROWS_AS(
				developersConsoleSystem.RegisterCommand("set_int", [&](const std::vector<std::string> command) { CommandSetInt(command, integer);}),
				InvalidArgumentException
			);
		}

		SECTION("Registering an empty command results in InvalidArgumentException")
		{
			int integer = 0;
			REQUIRE_THROWS_AS(
				developersConsoleSystem.RegisterCommand("", [&](const std::vector<std::string> command) { CommandSetInt(command, integer);}),
				InvalidArgumentException
			);
		}

		SECTION("Registering a null action results in InvalidArgumentException")
		{
			REQUIRE_THROWS_AS(developersConsoleSystem.RegisterCommand("test_command", nullptr), InvalidArgumentException);
		}
	}



	TEST_CASE("DevelopersConsoleSystem::UnregisterCommand", "[DevelopersConsoleSystem]")
	{
		const ComponentsContainers componentsContainers{};
		DevelopersConsoleSystem developersConsoleSystem{ componentsContainers };

		SECTION("Unregistering registered command")
		{
			int integer = 0;
			REQUIRE_FALSE(developersConsoleSystem.IsCommandRegistered("set_int"));

			developersConsoleSystem.RegisterCommand("set_int", [&](const std::vector<std::string> command) { CommandSetInt(command, integer);});
			REQUIRE(developersConsoleSystem.IsCommandRegistered("set_int"));

			developersConsoleSystem.UnregisterCommand("set_int");
			REQUIRE_FALSE(developersConsoleSystem.IsCommandRegistered("set_int"));
		}

		SECTION("Unregistering a command that is not registered is an invalid operation.")
		{
			REQUIRE_THROWS_AS(developersConsoleSystem.UnregisterCommand("not_set"), InvalidOperationException);
		}

		SECTION("It is not possible to unregister the same command twice")
		{
			int integer = 0;

			developersConsoleSystem.RegisterCommand("set_int", [&](const std::vector<std::string> command) { CommandSetInt(command, integer);});
			developersConsoleSystem.UnregisterCommand("set_int");
			REQUIRE_THROWS_AS(developersConsoleSystem.UnregisterCommand("set_int"), InvalidOperationException);
		}
	}



	TEST_CASE("DevelopersConsoleSystem::ExecuteLine")
	{
		const ComponentsContainers componentsContainers{};
		DevelopersConsoleSystem developersConsoleSystem{ componentsContainers };

		SECTION("Executing a command line with an unregistered command results in a logic error exception.")
		{
			REQUIRE_THROWS_AS(developersConsoleSystem.ExecuteLine("Unregistered_command ha ha ha"), std::logic_error);
		}
	}
}

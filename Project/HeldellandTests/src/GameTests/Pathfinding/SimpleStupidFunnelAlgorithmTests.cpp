//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"

#include<memory>

#include<Game/Pathfinding/SimpleStupidFunnelAlgorithm.h>
using namespace Game;
using namespace Nest;



namespace SimpleStupidFunnelAlgorithmTests
{
	TEST_CASE("Simple Stupid Funnel Algorithm path processing", "[SimpleStupidFunnelAlgorithm]")
	{
		auto simpleStupidFunnelAlgorithm = std::make_unique<SimpleStupidFunnelAlgorithm>();
		std::vector<Nest::Vector2<simfix64>> unprocessedPath{};


		SECTION("Processing a path with only start and end positions, returns the start and end positions")
		{
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });

			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 0.0f });

			const auto path = simpleStupidFunnelAlgorithm->ProcessPath(unprocessedPath);

			REQUIRE(path.size() == 2);
			REQUIRE(path[0] == Nest::Vector2<simfix64>{ simfix64{ 0.0f }, simfix64{ 0.0f } });
			REQUIRE(path[1] == Nest::Vector2<simfix64>{ simfix64{ 1.0f }, simfix64{ 0.0f } });
		}


		SECTION("Processing a path with one portal, without turning")
		{
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });

			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 1.0f });
			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ -1.0f });

			unprocessedPath.emplace_back(simfix64{ 2.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 2.0f }, simfix64{ 0.0f });

			const auto path = simpleStupidFunnelAlgorithm->ProcessPath(unprocessedPath);
			REQUIRE(path.size() == 2);
			REQUIRE(path[0] == Nest::Vector2<simfix64>{ simfix64{ 0.0f }, simfix64{ 0.0f } });
			REQUIRE(path[1] == Nest::Vector2<simfix64>{ simfix64{ 2.0f }, simfix64{ 0.0f } });
		}


		SECTION("Processing a path with two portals, with one left and one right turns")
		{
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });

			unprocessedPath.emplace_back(simfix64{ -2.0f }, simfix64{ 1.0f });
			unprocessedPath.emplace_back(simfix64{ -1.0f }, simfix64{ 1.0f });

			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 2.0f });
			unprocessedPath.emplace_back(simfix64{ 2.0f }, simfix64{ 2.0f });

			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 3.0f });
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 3.0f });

			const auto path = simpleStupidFunnelAlgorithm->ProcessPath(unprocessedPath);
			REQUIRE(path.size() == 4);
			REQUIRE(path[0] == Nest::Vector2<simfix64>{ simfix64{ 0.0f }, simfix64{ 0.0f } });
			REQUIRE(path[1] == Nest::Vector2<simfix64>{ simfix64{ -1.0f }, simfix64{ 1.0f } });
			REQUIRE(path[2] == Nest::Vector2<simfix64>{ simfix64{ 1.0f }, simfix64{ 2.0f } });
			REQUIRE(path[3] == Nest::Vector2<simfix64>{ simfix64{ 0.0f }, simfix64{ 3.0f } });
		}


		SECTION("Processing a path with two portals, with one right and one left turns")
		{
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });

			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 1.0f });
			unprocessedPath.emplace_back(simfix64{ 2.0f }, simfix64{ 1.0f });

			unprocessedPath.emplace_back(simfix64{ -2.0f }, simfix64{ 2.0f });
			unprocessedPath.emplace_back(simfix64{ -1.0f }, simfix64{ 2.0f });

			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 3.0f });
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 3.0f });

			const auto path = simpleStupidFunnelAlgorithm->ProcessPath(unprocessedPath);
			REQUIRE(path.size() == 4);
			REQUIRE(path[0] == Nest::Vector2<simfix64>{ simfix64{ 0.0f }, simfix64{ 0.0f } });
			REQUIRE(path[1] == Nest::Vector2<simfix64>{ simfix64{ 1.0f }, simfix64{ 1.0f } });
			REQUIRE(path[2] == Nest::Vector2<simfix64>{ simfix64{ -1.0f }, simfix64{ 2.0f } });
			REQUIRE(path[3] == Nest::Vector2<simfix64>{ simfix64{ 0.0f }, simfix64{ 3.0f } });
		}


		SECTION("Processing a path with two portals, but each repeated once")
		{
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });

			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 1.0f });
			unprocessedPath.emplace_back(simfix64{ 2.0f }, simfix64{ 1.0f });

			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 1.0f });
			unprocessedPath.emplace_back(simfix64{ 2.0f }, simfix64{ 1.0f });

			unprocessedPath.emplace_back(simfix64{ -2.0f }, simfix64{ 2.0f });
			unprocessedPath.emplace_back(simfix64{ -1.0f }, simfix64{ 2.0f });

			unprocessedPath.emplace_back(simfix64{ -2.0f }, simfix64{ 2.0f });
			unprocessedPath.emplace_back(simfix64{ -1.0f }, simfix64{ 2.0f });

			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 3.0f });
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 3.0f });

			const auto path = simpleStupidFunnelAlgorithm->ProcessPath(unprocessedPath);
			REQUIRE(path.size() == 4);
			REQUIRE(path[0] == Nest::Vector2<simfix64>{ simfix64{ 0.0f }, simfix64{ 0.0f } });
			REQUIRE(path[1] == Nest::Vector2<simfix64>{ simfix64{ 1.0f }, simfix64{ 1.0f } });
			REQUIRE(path[2] == Nest::Vector2<simfix64>{ simfix64{ -1.0f }, simfix64{ 2.0f } });
			REQUIRE(path[3] == Nest::Vector2<simfix64>{ simfix64{ 0.0f }, simfix64{ 3.0f } });
		}


		SECTION("Processing a path with colinear portals")
		{
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });

			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 1.0f });
			unprocessedPath.emplace_back(simfix64{ 2.0f }, simfix64{ 1.0f });

			unprocessedPath.emplace_back(simfix64{ 4.0f }, simfix64{ 1.0f });
			unprocessedPath.emplace_back(simfix64{ 3.0f }, simfix64{ 1.0f });

			unprocessedPath.emplace_back(simfix64{ 4.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 4.0f }, simfix64{ 0.0f });

			const auto path = simpleStupidFunnelAlgorithm->ProcessPath(unprocessedPath);
			REQUIRE(path.size() == 4);
			REQUIRE(path[0] == Nest::Vector2<simfix64>{ simfix64{ 0.0f }, simfix64{ 0.0f } });
			REQUIRE(path[1] == Nest::Vector2<simfix64>{ simfix64{ 2.0f }, simfix64{ 1.0f } });
			REQUIRE(path[2] == Nest::Vector2<simfix64>{ simfix64{ 3.0f }, simfix64{ 1.0f } });
			REQUIRE(path[3] == Nest::Vector2<simfix64>{ simfix64{ 4.0f }, simfix64{ 0.0f } });
		}
	}
}

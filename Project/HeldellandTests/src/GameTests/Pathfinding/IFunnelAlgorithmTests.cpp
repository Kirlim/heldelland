//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"

#include<memory>

#include<Nest/Exceptions/InvalidDataException.h>
#include<Nest/Exceptions/LogicErrorException.h>
using namespace Nest;

#include<Game/Pathfinding/IFunnelAlgorithm.h>
using namespace Game;



namespace IFunnelAlgorithmTests
{
	class FunnelAlgorithmMock : public IFunnelAlgorithm
	{
	public:
		int doProcessPathCalledCount = 0;

	private:
		std::vector<Nest::Vector2<simfix64>> DoProcessPath(const std::vector<Nest::Vector2<simfix64>>&) override
		{
			doProcessPathCalledCount++;
			return std::vector<Nest::Vector2<simfix64>>{};
		}
	};



	TEST_CASE("Funnel Algorithm interface ProcessPath method", "[IFunnelAlgorithm]")
	{
		const auto funnelAlgorithm = std::make_unique<FunnelAlgorithmMock>();


		SECTION("ProcessPath throws LogicErrorException if portals does not have the minimum size of 4 nodes")
		{
			std::vector<Nest::Vector2<simfix64>> unprocessedPath{};
			REQUIRE_THROWS_AS(funnelAlgorithm->ProcessPath(unprocessedPath), LogicErrorException);

			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });
			REQUIRE_THROWS_AS(funnelAlgorithm->ProcessPath(unprocessedPath), LogicErrorException);
		}


		SECTION("ProcessPath throws InvalidDataException if portals does not have an even number of elements")
		{
			std::vector<Nest::Vector2<simfix64>> unprocessedPath{};
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 1.0f });
			unprocessedPath.emplace_back(simfix64{ 2.0f }, simfix64{ 2.0f });
			unprocessedPath.emplace_back(simfix64{ 2.0f }, simfix64{ 2.0f });

			REQUIRE_THROWS_AS(funnelAlgorithm->ProcessPath(unprocessedPath), InvalidDataException);
		}


		SECTION("ProcessPath throws LogicErrorException if the first two elements are not equal")
		{
			std::vector<Nest::Vector2<simfix64>> unprocessedPath{};
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.1f });
			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 1.0f });
			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 1.0f });

			REQUIRE_THROWS_AS(funnelAlgorithm->ProcessPath(unprocessedPath), LogicErrorException);
		}



		SECTION("ProcessPath throws LogicErrorException if the last two elements are not equal")
		{
			std::vector<Nest::Vector2<simfix64>> unprocessedPath{};
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 1.1f });
			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 1.0f });

			REQUIRE_THROWS_AS(funnelAlgorithm->ProcessPath(unprocessedPath), LogicErrorException);
		}


		SECTION("ProcessPath calls child implementation if portals have a valid number of elements")
		{
			std::vector<Nest::Vector2<simfix64>> unprocessedPath{};
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 0.0f }, simfix64{ 0.0f });
			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 1.0f });
			unprocessedPath.emplace_back(simfix64{ 1.0f }, simfix64{ 1.0f });

			REQUIRE_NOTHROW(funnelAlgorithm->ProcessPath(unprocessedPath));
			REQUIRE(funnelAlgorithm->doProcessPathCalledCount == 1);
		}
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"

#include<memory>

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NullArgumentException.h>
using namespace Nest;

#include<Game/Simulation/IGameServerSimulation.h>
#include<Game/Simulation/GameServerSimulationSystem.h>
using namespace Game;



namespace GameServerSimulationSystemTests
{
	class GameServerSimulationDummy : public IGameServerSimulation
	{
	public:
		~GameServerSimulationDummy() override
		{
		}

		PlayersSimulationsManager* GetPlayersSimulationsManager() const override { return nullptr; }

		void RegisterCommand(std::shared_ptr<ServerSimulationCommand>& command) override
		{
		}

		bool RegisterPlayer(std::shared_ptr<PlayerSimulationInformation> playerInformation) override { return false; }
		std::vector<PlayerSimInfoId> GetListRegisteredPlayersIds() const override { return std::vector<PlayerSimInfoId>{}; }

		void PlayerSimulationLoaded(PlayerSimInfoId playerId) override
		{
		}

		bool HasJoinGameRepliesToDispatch() const override { return false; }

		void DispatchJoinGameReplies(std::function<void(std::weak_ptr<PlayerSimulationInformation>)> dispatchFunction) override
		{
		}

		SimulationActorMovementSystem* GetSimulationActorMovementSystem() const override { return nullptr; }
	private:
		void DoTick(simfix64 tickDeltaTime) override
		{
		}
	};



	TEST_CASE("StartServerSimulation method", "[GameServerSimulationSystem]")
	{
		GameServerSimulationSystem gameServerSimulationSystem;

		SECTION("Passing a null pointer will throw NullArgumenException")
		{
			std::unique_ptr<IGameServerSimulation> gameServerSimulationDummy{};
			REQUIRE_THROWS_AS(gameServerSimulationSystem.StartServerSimulation(gameServerSimulationDummy), NullArgumentException);
		}

		SECTION("Passing a valid server simulation will move it")
		{
			std::unique_ptr<IGameServerSimulation> gameServerSimulationDummy = std::make_unique<GameServerSimulationDummy>();
			auto dummyAddress = gameServerSimulationDummy.get();

			REQUIRE_NOTHROW(gameServerSimulationSystem.StartServerSimulation(gameServerSimulationDummy));
			REQUIRE(gameServerSimulationSystem.GetServerSimulation() == dummyAddress);
			REQUIRE(gameServerSimulationDummy == nullptr);
		}

		SECTION("Passing servers twice will throw LogicErrorException")
		{
			std::unique_ptr<IGameServerSimulation> gameServerSimulationDummy1 = std::make_unique<GameServerSimulationDummy>();
			std::unique_ptr<IGameServerSimulation> gameServerSimulationDummy2 = std::make_unique<GameServerSimulationDummy>();
			auto dummyAddress1 = gameServerSimulationDummy1.get();

			REQUIRE_NOTHROW(gameServerSimulationSystem.StartServerSimulation(gameServerSimulationDummy1));
			REQUIRE_THROWS_AS(gameServerSimulationSystem.StartServerSimulation(gameServerSimulationDummy2), LogicErrorException);
			REQUIRE(gameServerSimulationSystem.GetServerSimulation() == dummyAddress1);
			REQUIRE(gameServerSimulationDummy2 != nullptr);
		}
	}



	class GameServerSimulationMock : public GameServerSimulationDummy
	{
	public:
		~GameServerSimulationMock() override
		{
		}

		PlayersSimulationsManager* GetPlayersSimulationsManager() const override { return nullptr; }

		void RegisterCommand(std::shared_ptr<ServerSimulationCommand>& command) override
		{
		}

		bool RegisterPlayer(std::shared_ptr<PlayerSimulationInformation> playerInformation) override { return false; }
		std::vector<PlayerSimInfoId> GetListRegisteredPlayersIds() const override { return std::vector<PlayerSimInfoId>{}; }

		void PlayerSimulationLoaded(PlayerSimInfoId playerId) override
		{
		}

		bool HasJoinGameRepliesToDispatch() const override { return false; }

		void DispatchJoinGameReplies(std::function<void(std::weak_ptr<PlayerSimulationInformation>)> dispatchFunction) override
		{
		}

		int doTickCallCount = 0;

	private:
		void DoTick(simfix64 tickDeltaTime) override
		{
			++doTickCallCount;
		}
	};



	TEST_CASE("FixedUpdate method", "[GameServerSimulationSystem]")
	{
		GameServerSimulationSystem gameServerSimulationSystem;

		SECTION("Calling many times with various values ticks correctly")
		{
			simfix64 deltaTime = gameServerSimulationSystem.TickDeltaTime * simfix64{ 0.70 };

			std::unique_ptr<IGameServerSimulation> gameServerSimulationMock = std::make_unique<GameServerSimulationMock>();
			gameServerSimulationSystem.StartServerSimulation(gameServerSimulationMock);
			auto gameServerSimulation = dynamic_cast<GameServerSimulationMock*>(gameServerSimulationSystem.GetServerSimulation());

			// 0.7
			gameServerSimulationSystem.FixedUpdate(deltaTime.ToFloat());
			REQUIRE(gameServerSimulation->doTickCallCount == 0);

			// 1.4
			gameServerSimulationSystem.FixedUpdate(deltaTime.ToFloat());
			REQUIRE(gameServerSimulation->doTickCallCount == 1);

			// 2.1
			gameServerSimulationSystem.FixedUpdate(deltaTime.ToFloat());
			REQUIRE(gameServerSimulation->doTickCallCount == 2);

			// 2.9
			gameServerSimulationSystem.FixedUpdate(deltaTime.ToFloat());
			REQUIRE(gameServerSimulation->doTickCallCount == 2);

			// Simulating that somehow fixed update accumulated stuff
			gameServerSimulationSystem.FixedUpdate(10.0f * gameServerSimulationSystem.TickDeltaTime.ToFloat());
			REQUIRE(gameServerSimulation->doTickCallCount == 12);
		}
	}
}

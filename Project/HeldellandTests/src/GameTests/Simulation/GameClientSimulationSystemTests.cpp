//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"

#include<memory>

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NullArgumentException.h>
#include<Nest/Events/EventsBusToken.h>
using namespace Nest;

#include<Game/SimulationCommons/ActorsManager.h>
#include<Game/SimulationCommons/SimulationActorMovementSystem.h>
#include<Game/Simulation/IGameClientSimulation.h>
#include<Game/Simulation/GameClientSimulationSystem.h>
using namespace Game;



namespace GameClientSimulationSystemTests
{
	class GameClientSimulationDummy : public IGameClientSimulation
	{
	public:
		~GameClientSimulationDummy() override = default;

		void Update(float) override
		{
		}

		Map* GetMap() const override { return nullptr; }
		SimulationActorMovementSystem* GetSimulationActorMovementSystem() const override { return nullptr; }
		PlayerSimulationCommandsQueuesManager* GetQueuesManager() const override { return nullptr; }
		ServerSimulationCommandsQueue* GetToServerSimulationCommandsQueue() const override { return nullptr; }
		ActorsManager* GetActorsManager() const override { return nullptr; }

		void SetPlayerActorNetworkId(ActorNetworkId) noexcept override
		{
		}

		void ForceNextTickValue(unsigned) override
		{
		}

		ActorId SpawnActor(ActorTypeId, Nest::Vector2<simfix64>, bool, ActorNetworkId) const override { return InvalidActorId; }
		float GetCurrentTickElapsedTimePercentage() const override { return 0.0f; }
		ActorNetworkId GetPlayerActorNetworkId() const noexcept override { return InvalidActorNetworkId; }
		std::unique_ptr<Nest::EventsBusToken> RegisterToActorSpawnedEvent(std::function<void(const ActorSpawnedEvt&)>) override { return nullptr; }
	};



	TEST_CASE("StartClientSimulation method", "[GameClientSimulationSystem]")
	{
		GameClientSimulationSystem gameClientSimulationSystem;

		SECTION("Passing a null pointer will throw NullArgumenException")
		{
			std::unique_ptr<IGameClientSimulation> gameClientSimulationDummy{};
			REQUIRE_THROWS_AS(gameClientSimulationSystem.StartClientSimulation(gameClientSimulationDummy), NullArgumentException);
		}

		SECTION("Passing a valid server simulation will move it")
		{
			std::unique_ptr<IGameClientSimulation> gameClientSimulationDummy = std::make_unique<GameClientSimulationDummy>();
			auto dummyAddress = gameClientSimulationDummy.get();

			REQUIRE_NOTHROW(gameClientSimulationSystem.StartClientSimulation(gameClientSimulationDummy));
			REQUIRE(gameClientSimulationSystem.GetClientSimulation() == dummyAddress);
			REQUIRE(gameClientSimulationDummy == nullptr);
		}

		SECTION("Passing servers twice will throw LogicErrorException")
		{
			std::unique_ptr<IGameClientSimulation> gameClientSimulationDummy1 = std::make_unique<GameClientSimulationDummy>();
			std::unique_ptr<IGameClientSimulation> gameClientSimulationDummy2 = std::make_unique<GameClientSimulationDummy>();
			auto dummyAddress1 = gameClientSimulationDummy1.get();

			REQUIRE_NOTHROW(gameClientSimulationSystem.StartClientSimulation(gameClientSimulationDummy1));
			REQUIRE_THROWS_AS(gameClientSimulationSystem.StartClientSimulation(gameClientSimulationDummy2), LogicErrorException);
			REQUIRE(gameClientSimulationSystem.GetClientSimulation() == dummyAddress1);
			REQUIRE(gameClientSimulationDummy2 != nullptr);
		}
	}



	class GameClientSimulationMock : public GameClientSimulationDummy
	{
	public:
		~GameClientSimulationMock() override = default;

		void Update(float) override { ++doUpdateCallCount; }

		int doUpdateCallCount = 0;
	};



	TEST_CASE("GameClientSimulationSystem::FixedUpdate method", "[GameClientSimulationSystem]")
	{
		GameClientSimulationSystem gameClientSimulationSystem;

		SECTION("Calling many times updates correctly")
		{
			std::unique_ptr<IGameClientSimulation> gameClientSimulationMock = std::make_unique<GameClientSimulationMock>();
			gameClientSimulationSystem.StartClientSimulation(gameClientSimulationMock);
			auto gameClientSimulation = dynamic_cast<GameClientSimulationMock*>(gameClientSimulationSystem.GetClientSimulation());

			REQUIRE(gameClientSimulation->doUpdateCallCount == 0);

			const float deltaTimeDoesNotAffectTest = 0.5f;
			gameClientSimulationSystem.Update(deltaTimeDoesNotAffectTest);
			REQUIRE(gameClientSimulation->doUpdateCallCount == 1);

			gameClientSimulationSystem.Update(deltaTimeDoesNotAffectTest);
			REQUIRE(gameClientSimulation->doUpdateCallCount == 2);
		}
	}
}

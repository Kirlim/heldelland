//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"

#include<Game/Pathfinding/Path.h>
#include<Game/Simulation/GameServerTickEventsContainer.h>
using namespace Game;
using namespace Nest;



namespace GameServerTickEventsContainerTests
{
	TEST_CASE("Working with New Actor Movement Speed Events", "[GameServerTickEventsContainer]")
	{
		GameServerTickEventsContainer gameServerTickEventsContainer{};


		SECTION("Registering two New Actor Movement Speed Events and iterating over them")
		{
			const ActorNetworkId actorNetworkId1{ 1 };
			const simfix64 actorSpeed1{ 1.0f };

			const ActorNetworkId actorNetworkId2{ 2 };
			const simfix64 actorSpeed2{ 1.0f };

			REQUIRE_NOTHROW(gameServerTickEventsContainer.RegisterNewActorMovementSpeedEvent(actorNetworkId1, actorSpeed1));
			REQUIRE_NOTHROW(gameServerTickEventsContainer.RegisterNewActorMovementSpeedEvent(actorNetworkId2, actorSpeed2));

			bool actor1_correct = false;
			bool actor2_correct = false;

			gameServerTickEventsContainer.RunForNewActorMovementSpeedEvents(
				[&](auto beginIt, auto endIt)
				{
					while (beginIt != endIt)
					{
						const auto& actorNetworkId = std::get<ActorNetworkId>(*beginIt);
						const auto& actorSpeed = std::get<simfix64>(*beginIt);

						if (actorNetworkId == actorNetworkId1 && actorSpeed == actorSpeed1)
							actor1_correct = true;

						if (actorNetworkId == actorNetworkId2 && actorSpeed == actorSpeed2)
							actor2_correct = true;

						++beginIt;
					}
				}
			);

			REQUIRE(actor1_correct);
			REQUIRE(actor2_correct);
		}
	}



	TEST_CASE("Working with New Actor Path Events", "[GameServerTickEventsContainer]")
	{
		GameServerTickEventsContainer gameServerTickEventsContainer{};


		SECTION("Registering two New Actor Path Events and iterating over them")
		{
			const ActorNetworkId actorNetworkId1{ 1 };
			auto actorPath1 = std::make_shared<Path>();

			const ActorNetworkId actorNetworkId2{ 2 };
			auto actorPath2 = std::make_shared<Path>();

			REQUIRE_NOTHROW(gameServerTickEventsContainer.RegisterNewActorPathEvent(actorNetworkId1, actorPath1));
			REQUIRE_NOTHROW(gameServerTickEventsContainer.RegisterNewActorPathEvent(actorNetworkId2, actorPath2));

			bool actor1_correct = false;
			bool actor2_correct = false;

			gameServerTickEventsContainer.RunForNewActorPathEvents(
				[&](auto beginIt, auto endIt)
				{
					while (beginIt != endIt)
					{
						const auto& actorNetworkId = std::get<ActorNetworkId>(*beginIt);
						const auto& actorPath = std::get<std::shared_ptr<Path>>(*beginIt);

						if (actorNetworkId == actorNetworkId1 && actorPath.get() == actorPath1.get())
							actor1_correct = true;

						if (actorNetworkId == actorNetworkId2 && actorPath.get() == actorPath2.get())
							actor2_correct = true;

						++beginIt;
					}
				}
			);

			REQUIRE(actor1_correct);
			REQUIRE(actor2_correct);
		}
	}



	TEST_CASE("Tick Events are cleared with the ClearAllRegisteredEvents method", "[GameServerTickEventsContainer]")
	{
		GameServerTickEventsContainer gameServerTickEventsContainer;

		REQUIRE(gameServerTickEventsContainer.GetNewActorMovementSpeedEventsCount() == 0);
		REQUIRE(gameServerTickEventsContainer.GetNewActorPathEventsCount() == 0);

		const ActorNetworkId actorNetworkId{ 1 };
		const simfix64 actorSpeed{ 1.0f };
		const auto actorPath = std::make_shared<Path>();

		REQUIRE_NOTHROW(gameServerTickEventsContainer.RegisterNewActorMovementSpeedEvent(actorNetworkId, actorSpeed));
		REQUIRE(gameServerTickEventsContainer.GetNewActorMovementSpeedEventsCount() == 1);
		REQUIRE(gameServerTickEventsContainer.GetNewActorPathEventsCount() == 0);

		REQUIRE_NOTHROW(gameServerTickEventsContainer.RegisterNewActorPathEvent(actorNetworkId, actorPath));
		REQUIRE(gameServerTickEventsContainer.GetNewActorMovementSpeedEventsCount() == 1);
		REQUIRE(gameServerTickEventsContainer.GetNewActorPathEventsCount() == 1);

		gameServerTickEventsContainer.ClearAllRegisteredEvents();
		REQUIRE(gameServerTickEventsContainer.GetNewActorMovementSpeedEventsCount() == 0);
		REQUIRE(gameServerTickEventsContainer.GetNewActorPathEventsCount() == 0);
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../../catch.hpp"

#include<Nest/Exceptions/ValueOutOfRangeException.h>
using namespace Nest;

#include<Game/Pathfinding/Path.h>

#include<Game/Simulation/CommandsTypes.h>
#include<Game/Simulation/ClientCommands/SetActorFullPathCommand.h>
#include<Game/Network/SunaNetworkWriteSerializer.h>
#include<Game/Network/SunaNetworkReadSerializer.h>
using namespace Game;



namespace SetActorFullPathCommandTests
{
	TEST_CASE("SetActorFullPathCommand::GetTypeId returns the correct type id", "[SetActorFullPathCommand]")
	{
		const SetActorFullPathCommand setActorFullPathCommand{};
		REQUIRE(setActorFullPathCommand.GetTypeId() == NetworkMessageTypeId { SET_ACTOR_FULL_PATH_COMMAND_TYPE });
	}



	TEST_CASE("SetActorFullPathCommand basic setting and getting of data", "[SetActorFullPathCommand]")
	{
		SetActorFullPathCommand setActorFullPathCommand;


		SECTION("Setting and Getting ActorNetworkId")
		{
			setActorFullPathCommand.SetActorNetworkId(InvalidActorNetworkId);
			REQUIRE(setActorFullPathCommand.GetActorNetworkId() == InvalidActorNetworkId);

			const ActorNetworkId actorNetworkId{ 123 };
			setActorFullPathCommand.SetActorNetworkId(actorNetworkId);
			REQUIRE(setActorFullPathCommand.GetActorNetworkId() == actorNetworkId);
		}


		SECTION("Setting and Getting Path information")
		{
			const Vector2<simfix64> waypoint0{ simfix64{ 0 }, simfix64{ 0 } };
			const Vector2<simfix64> waypoint1{ simfix64{ 1 }, simfix64{ 2 } };

			Path path{};
			path.PushBack(waypoint0);
			path.PushBack(waypoint1);

			setActorFullPathCommand.SetPath(path);
			REQUIRE(setActorFullPathCommand.GetPathSize() == 2);
			REQUIRE(setActorFullPathCommand.GetWaypointAtIndex(0) == waypoint0);
			REQUIRE(setActorFullPathCommand.GetWaypointAtIndex(1) == waypoint1);
		}


		SECTION("Getting path waypoint from invalid index throws ValueOutOfRangeException")
		{
			REQUIRE(setActorFullPathCommand.GetPathSize() == 0);
			REQUIRE_THROWS_AS(setActorFullPathCommand.GetWaypointAtIndex(-1), ValueOutOfRangeException);
			REQUIRE_THROWS_AS(setActorFullPathCommand.GetWaypointAtIndex(1), ValueOutOfRangeException);
		}
	}



	TEST_CASE("SetActorFullPathCommand::Serialize method for both writing and reading", "[SetActorFullPathCommand]")
	{
		const ActorNetworkId actorNetworkId{ 10 };
		const Vector2<simfix64> waypoint0{ simfix64{ 0 }, simfix64{ 0 } };
		const Vector2<simfix64> waypoint1{ simfix64{ 1 }, simfix64{ 2 } };
		const Vector2<simfix64> waypoint2{ simfix64{ 2 }, simfix64{ 8 } };

		Path path{};
		path.PushBack(waypoint0);
		path.PushBack(waypoint1);
		path.PushBack(waypoint2);

		SetActorFullPathCommand setActorFullPathCommandToWrite{};
		setActorFullPathCommandToWrite.SetActorNetworkId(actorNetworkId);
		setActorFullPathCommandToWrite.SetPath(path);

		unsigned char buffer[200];
		SunaNetworkWriteSerializer writer{ buffer, 0, static_cast<unsigned int>(std::size(buffer)) };
		REQUIRE(setActorFullPathCommandToWrite.Serialize(writer));

		SunaNetworkReadSerializer reader{ buffer, 0, static_cast<unsigned int>(std::size(buffer)) };
		SetActorFullPathCommand setActorFullPathCommandRecovered{};
		REQUIRE(setActorFullPathCommandRecovered.Serialize(reader));

		REQUIRE(setActorFullPathCommandRecovered.GetActorNetworkId() == actorNetworkId);
		REQUIRE(setActorFullPathCommandRecovered.GetPathSize() == 3);
		REQUIRE(setActorFullPathCommandRecovered.GetWaypointAtIndex(0) == waypoint0);
		REQUIRE(setActorFullPathCommandRecovered.GetWaypointAtIndex(1) == waypoint1);
		REQUIRE(setActorFullPathCommandRecovered.GetWaypointAtIndex(2) == waypoint2);
	}
}

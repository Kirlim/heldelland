//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../../catch.hpp"

#include<Game/Simulation/CommandsTypes.h>
#include<Game/Simulation/ClientCommands/SetActorMovementSpeedCommand.h>

#include<Game/Network/SunaNetworkWriteSerializer.h>
#include<Game/Network/SunaNetworkReadSerializer.h>

using namespace Game;



namespace SetActorMovementSpeedCommandTests
{
	TEST_CASE("SetActorMovementSpeedCommand::GetTypeId returns the correct type id", "[SetActorMovementSpeedCommand]")
	{
		const SetActorMovementSpeedCommand setActorMovementSpeedCommand{};
		REQUIRE(setActorMovementSpeedCommand.GetTypeId() == NetworkMessageTypeId { SET_ACTOR_MOVEMENT_SPEED_COMMAND_TYPE });
	}



	TEST_CASE("SetActorMovementSpeedCommand basic setting and getting data", "[SetActorMovementSpeedCommand]")
	{
		SetActorMovementSpeedCommand setActorMovementSpeedCommand{};


		SECTION("Setting and Getting ActorNetworkId")
		{
			setActorMovementSpeedCommand.SetActorNetworkId(InvalidActorNetworkId);
			REQUIRE(setActorMovementSpeedCommand.GetActorNetworkId() == InvalidActorNetworkId);

			const ActorNetworkId actorNetworkId{ 1 };
			setActorMovementSpeedCommand.SetActorNetworkId(actorNetworkId);
			REQUIRE(setActorMovementSpeedCommand.GetActorNetworkId() == actorNetworkId);
		}


		SECTION("Setting and Getting Movement Speed")
		{
			const simfix64 movementSpeed1{ 3.0f };
			const simfix64 movementSpeed2{ 1.5f };

			setActorMovementSpeedCommand.SetMovementSpeed(movementSpeed1);
			REQUIRE(setActorMovementSpeedCommand.GetMovementSpeed() == movementSpeed1);

			setActorMovementSpeedCommand.SetMovementSpeed(movementSpeed2);
			REQUIRE(setActorMovementSpeedCommand.GetMovementSpeed() == movementSpeed2);
		}
	}



	TEST_CASE("SetActorMovementSpeedCommand::Serialize method for both writing and reading", "[SetActorMovementSpeedCommand]")
	{
		const ActorNetworkId actorNetworkId{ 11 };
		const simfix64 movementSpeed{ 16.7f };

		SetActorMovementSpeedCommand setActorMovementSpeedCommandToWrite{};
		setActorMovementSpeedCommandToWrite.SetActorNetworkId(actorNetworkId);
		setActorMovementSpeedCommandToWrite.SetMovementSpeed(movementSpeed);

		unsigned char buffer[100];
		SunaNetworkWriteSerializer writer{ buffer, 0, static_cast<unsigned int>(std::size(buffer)) };
		REQUIRE(setActorMovementSpeedCommandToWrite.Serialize(writer));

		SunaNetworkReadSerializer reader{ buffer, 0, static_cast<unsigned int>(std::size(buffer)) };
		SetActorMovementSpeedCommand setActorMovementSpeedCommandRecovered{};
		REQUIRE(setActorMovementSpeedCommandRecovered.Serialize(reader));

		REQUIRE(setActorMovementSpeedCommandRecovered.GetActorNetworkId() == actorNetworkId);
		REQUIRE(setActorMovementSpeedCommandRecovered.GetMovementSpeed() == movementSpeed);
	}
}

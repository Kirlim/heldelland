//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<Nest/Events/EventsBus.h>
#include<Nest/InputEvent.h>
using namespace Nest;

#include<Game/CameraControlInputObserver.h>
using namespace Game;



namespace CameraControlInputObserverTests
{
	TEST_CASE("Camera Control Input Listener can handle camera YAW rotation related input", "[CameraControlInputObserver]")
	{
		EventsBus<InputEvent&> eventsBus{};
		CameraControlInputObserver cameraControlInputObserver{ eventsBus };

		int lastRotation = 0;
		const auto callbackAction = [&lastRotation](int yawRotation) { lastRotation = yawRotation; };
		cameraControlInputObserver.SetYawRotationCallback(callbackAction);

		InputEvent rightMouseButtonDownEvt;
		rightMouseButtonDownEvt.type = InputEventType::MouseButtonDown;
		rightMouseButtonDownEvt.mouseButton = MouseButton::Right;

		InputEvent rightMouseButtonUpEvt;
		rightMouseButtonUpEvt.type = InputEventType::MouseButtonUp;
		rightMouseButtonUpEvt.mouseButton = MouseButton::Right;

		InputEvent mouseMovedEvt;
		mouseMovedEvt.type = InputEventType::MouseMovement;


		SECTION("Moving the mouse while the right mouse button is held changes the rotation")
		{
			mouseMovedEvt.deltaX = 10;
			mouseMovedEvt.deltaY = 5;

			eventsBus.Signal(rightMouseButtonDownEvt);
			eventsBus.Signal(mouseMovedEvt);

			REQUIRE(lastRotation == 10);
		}


		SECTION("Moving the mouse while the right mouse button is not held does not change the rotation")
		{
			mouseMovedEvt.deltaX = 10;
			mouseMovedEvt.deltaY = 5;

			eventsBus.Signal(mouseMovedEvt);

			REQUIRE(lastRotation == 0);
		}


		SECTION("Moving the mouse after the right mouse button was released does not change the rotation")
		{
			mouseMovedEvt.deltaX = 10;
			mouseMovedEvt.deltaY = 5;

			eventsBus.Signal(rightMouseButtonDownEvt);
			eventsBus.Signal(mouseMovedEvt);

			REQUIRE(lastRotation == 10);
			lastRotation = 0;

			eventsBus.Signal(rightMouseButtonUpEvt);

			mouseMovedEvt.deltaX = 20;
			eventsBus.Signal(mouseMovedEvt);

			REQUIRE(lastRotation == 0);
		}
	}



	TEST_CASE("Camera Control Input Listener can handle Zoom related input", "[CameraControlInputObserver]")
	{
		EventsBus<InputEvent&> eventsBus{};
		CameraControlInputObserver cameraControlInputObserver{ eventsBus };

		int scrollDeltaY = 0;
		const auto callbackAction = [&scrollDeltaY](int mouseScrollDeltaY) { scrollDeltaY = mouseScrollDeltaY; };
		cameraControlInputObserver.SetZoomCallback(callbackAction);

		InputEvent mouseScrollEvt;
		mouseScrollEvt.type = InputEventType::MouseScroll;
		mouseScrollEvt.deltaY = -1;


		SECTION("Using the mouse scroll will activate the set callback")
		{
			REQUIRE(scrollDeltaY != mouseScrollEvt.deltaY);

			eventsBus.Signal(mouseScrollEvt);
			REQUIRE(scrollDeltaY == mouseScrollEvt.deltaY);
		}


		SECTION("Using the mouse scroll when there is no callback set won't cause and error")
		{
			cameraControlInputObserver.SetZoomCallback(nullptr);

			REQUIRE(scrollDeltaY != mouseScrollEvt.deltaY);
			eventsBus.Signal(mouseScrollEvt);
			REQUIRE(scrollDeltaY != mouseScrollEvt.deltaY);
		}
	}
}

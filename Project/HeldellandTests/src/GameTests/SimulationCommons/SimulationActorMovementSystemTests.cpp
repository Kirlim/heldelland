//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"



#include"Game/SimulationCommons/SimulationActorsMovementData.h"
#include"Game/SimulationCommons/SimulationActorsMovementDataManager.h"
#include"Game/SimulationCommons/SimulationActorsMovementLogic.h"
#include"Game/SimulationCommons/SimulationActorMovementSystem.h"
using namespace Game;
using namespace Nest;



namespace SimulationActorMovementSystemTests
{
	TEST_CASE("SimulationActorMovementSystem::GetMovementInFramePositionsForActors", "[SimulationActorMovementSystem]")
	{
		SimulationActorsMovementData simulationActorsMovementData{};
		SimulationActorsMovementLogic simulationActorsMovementLogic{ simulationActorsMovementData };

		SimulationActorsMovementDataManager simulationActorsMovementDataManager{ simulationActorsMovementData };


		SimulationActorMovementSystem simulationActorMovementSystem{
			simulationActorsMovementData,
			simulationActorsMovementDataManager,
			simulationActorsMovementLogic
		};


		SECTION("The method returns only the requested actors movement information")
		{
			const ActorNetworkId actor1NetworkId{ 1 };
			const ActorNetworkId actor2NetworkId{ 2 };
			const ActorNetworkId actor3NetworkId{ 3 };

			simulationActorsMovementData.RegisterNetworkActor(actor1NetworkId);
			simulationActorsMovementData.RegisterNetworkActor(actor2NetworkId);
			simulationActorsMovementData.RegisterNetworkActor(actor3NetworkId);

			const auto actor1Index = simulationActorsMovementData.GetNetworkActorIndex(actor1NetworkId);
			const auto actor2Index = simulationActorsMovementData.GetNetworkActorIndex(actor2NetworkId);
			const auto actor3Index = simulationActorsMovementData.GetNetworkActorIndex(actor3NetworkId);

			simulationActorsMovementData.actorsInFrameMovementPositions[actor1Index].frameEndX = simfix64{ 1 };
			simulationActorsMovementData.actorsInFrameMovementPositions[actor2Index].frameEndX = simfix64{ 2 };
			simulationActorsMovementData.actorsInFrameMovementPositions[actor3Index].frameEndX = simfix64{ 3 };

			std::vector<ActorNetworkId> actorsNetworkIds{};
			actorsNetworkIds.emplace_back(actor1NetworkId);
			actorsNetworkIds.emplace_back(actor3NetworkId);

			std::vector<ActorInFrameMovementPositions> actorInFrameMovementPositions{};

			REQUIRE_NOTHROW(
				simulationActorMovementSystem.GetMovementInFramePositionsForActors(actorsNetworkIds, /*out*/ actorInFrameMovementPositions)
			);
			REQUIRE(actorInFrameMovementPositions.size() == 2);
			REQUIRE(actorInFrameMovementPositions[0].frameEndX == simfix64{ 1 });
			REQUIRE(actorInFrameMovementPositions[1].frameEndX == simfix64{ 3 });
		}
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"

#include<Nest/Exceptions/LogicErrorException.h>
using namespace Nest;

#include<Game/SimulationCommons/SimulationActorsMovementData.h>
using namespace Game;



namespace SimulationActorsMovementDataTests
{
	TEST_CASE("Managing registered actors", "[SimulationActorsMovementData]")
	{
		SimulationActorsMovementData simulationActorsMovementData{};


		SECTION("Cannot fetch non registered actors network ids")
		{
			const ActorNetworkId actorNetworkId{ 1 };

			REQUIRE_FALSE(simulationActorsMovementData.HasNetworkActor(actorNetworkId));
			REQUIRE(simulationActorsMovementData.GetNetworkActorIndex(actorNetworkId) < 0);
		}


		SECTION("Registering one actor network id")
		{
			const ActorNetworkId actorNetworkId{ 1 };

			REQUIRE_NOTHROW(simulationActorsMovementData.RegisterNetworkActor(actorNetworkId));
			REQUIRE(simulationActorsMovementData.HasNetworkActor(actorNetworkId));
			REQUIRE(simulationActorsMovementData.GetNetworkActorIndex(actorNetworkId) == 0);
		}


		SECTION("Registering multiple actors network ids")
		{
			const ActorNetworkId actorNetworkId1{ 1 };
			const ActorNetworkId actorNetworkId2{ 2 };

			REQUIRE_NOTHROW(simulationActorsMovementData.RegisterNetworkActor(actorNetworkId1));
			REQUIRE_NOTHROW(simulationActorsMovementData.RegisterNetworkActor(actorNetworkId2));

			REQUIRE(simulationActorsMovementData.HasNetworkActor(actorNetworkId1));
			REQUIRE(simulationActorsMovementData.HasNetworkActor(actorNetworkId2));

			REQUIRE(simulationActorsMovementData.GetNetworkActorIndex(actorNetworkId1) >= 0);
			REQUIRE(simulationActorsMovementData.GetNetworkActorIndex(actorNetworkId2) >= 0);
			REQUIRE((
				simulationActorsMovementData.GetNetworkActorIndex(actorNetworkId1) != simulationActorsMovementData.GetNetworkActorIndex(actorNetworkId2)
			));
		}


		SECTION("Unregistering an registered actor network id")
		{
			const ActorNetworkId actorNetworkId{ 1 };
			REQUIRE_NOTHROW(simulationActorsMovementData.RegisterNetworkActor(actorNetworkId));

			REQUIRE_NOTHROW(simulationActorsMovementData.UnregisterNetworkActor(actorNetworkId));
			REQUIRE_FALSE(simulationActorsMovementData.HasNetworkActor(actorNetworkId));
			REQUIRE(simulationActorsMovementData.GetNetworkActorIndex(actorNetworkId) < 0);
		}


		SECTION("Unregistering an registered actor id will keep the other actors data unchanged")
		{
			const ActorNetworkId actorNetworkId1{ 1 };
			const ActorDistanceMovementInformation distanceMovementInfo1{ simfix64{ 1.0f }, simfix64{ 10.0f } };
			const ActorPathInformation pathInfo1{ nullptr };
			const ActorInFrameMovementPositions movementPositions1{ simfix64{ 1.0f }, simfix64{ 10.0f }, simfix64{ 100.0f }, simfix64{ 1000.0f } };

			simulationActorsMovementData.RegisterNetworkActor(actorNetworkId1);
			const auto actor1Index = simulationActorsMovementData.GetNetworkActorIndex(actorNetworkId1);
			simulationActorsMovementData.actorsDistanceMovementInformation[actor1Index] = distanceMovementInfo1;
			simulationActorsMovementData.actorsPathInformation[actor1Index] = pathInfo1;
			simulationActorsMovementData.actorsInFrameMovementPositions[actor1Index] = movementPositions1;

			const ActorNetworkId actorNetworkId2{ 2 };
			const ActorDistanceMovementInformation distanceMovementInfo2{ simfix64{ 2.0f }, simfix64{ 20.0f } };
			const ActorPathInformation pathInfo2{ nullptr };
			const ActorInFrameMovementPositions movementPositions2{ simfix64{ 2.0f }, simfix64{ 20.0f }, simfix64{ 200.0f }, simfix64{ 2000.0f } };

			simulationActorsMovementData.RegisterNetworkActor(actorNetworkId2);
			const auto actor2Index = simulationActorsMovementData.GetNetworkActorIndex(actorNetworkId2);
			simulationActorsMovementData.actorsDistanceMovementInformation[actor2Index] = distanceMovementInfo2;
			simulationActorsMovementData.actorsPathInformation[actor2Index] = pathInfo2;
			simulationActorsMovementData.actorsInFrameMovementPositions[actor2Index] = movementPositions2;

			simulationActorsMovementData.UnregisterNetworkActor(actorNetworkId1);
			const auto newActor2Index = simulationActorsMovementData.GetNetworkActorIndex(actorNetworkId2);
			REQUIRE((
				simulationActorsMovementData.actorsDistanceMovementInformation[newActor2Index].speed == distanceMovementInfo2.speed &&
				simulationActorsMovementData.actorsDistanceMovementInformation[newActor2Index].distance == distanceMovementInfo2.distance
			));
			REQUIRE(simulationActorsMovementData.actorsPathInformation[newActor2Index].path == pathInfo2.path);
			REQUIRE((
				simulationActorsMovementData.actorsInFrameMovementPositions[newActor2Index].frameStartX == movementPositions2.frameStartX &&
				simulationActorsMovementData.actorsInFrameMovementPositions[newActor2Index].frameStartY == movementPositions2.frameStartY &&
				simulationActorsMovementData.actorsInFrameMovementPositions[newActor2Index].frameEndX == movementPositions2.frameEndX &&
				simulationActorsMovementData.actorsInFrameMovementPositions[newActor2Index].frameEndY == movementPositions2.frameEndY
			));
		}

		SECTION("Unregistering a not registered actor id will throw LogicErrorException")
		{
			const ActorNetworkId actorNetworkId{ 1 };
			REQUIRE_THROWS_AS(simulationActorsMovementData.UnregisterNetworkActor(actorNetworkId), LogicErrorException);
		}
	}



	TEST_CASE("SimulationActorsMovementData::GetActorsIndexes", "[SimulationActorsMovementData]")
	{
		SimulationActorsMovementData simulationActorsMovementData{};


		SECTION("Indexes are returned, and in the same requested order")
		{
			const ActorNetworkId actor1NetworkId{ 1 };
			const ActorNetworkId actor2NetworkId{ 2 };
			const ActorNetworkId actor3NetworkId{ 3 };
			const ActorNetworkId actor4NetworkId{ 4 };

			simulationActorsMovementData.RegisterNetworkActor(actor1NetworkId);
			simulationActorsMovementData.RegisterNetworkActor(actor2NetworkId);
			simulationActorsMovementData.RegisterNetworkActor(actor3NetworkId);
			simulationActorsMovementData.RegisterNetworkActor(actor4NetworkId);

			std::vector<ActorNetworkId> searchActorsIds{};
			searchActorsIds.emplace_back(actor1NetworkId);
			searchActorsIds.emplace_back(actor4NetworkId);
			searchActorsIds.emplace_back(actor2NetworkId);

			std::vector<int> receivedActorsIndexes{};
			simulationActorsMovementData.GetNetworkActorsIndexes(searchActorsIds, /*out*/ receivedActorsIndexes);

			REQUIRE(receivedActorsIndexes.size() == 3);
			REQUIRE(receivedActorsIndexes[0] == simulationActorsMovementData.GetNetworkActorIndex(actor1NetworkId));
			REQUIRE(receivedActorsIndexes[1] == simulationActorsMovementData.GetNetworkActorIndex(actor4NetworkId));
			REQUIRE(receivedActorsIndexes[2] == simulationActorsMovementData.GetNetworkActorIndex(actor2NetworkId));
		}


		SECTION("Out vector contains only recovered data among multiple calls")
		{
			const ActorNetworkId actor1NetworkId{ 1 };
			const ActorNetworkId actor2NetworkId{ 2 };

			simulationActorsMovementData.RegisterNetworkActor(actor1NetworkId);
			simulationActorsMovementData.RegisterNetworkActor(actor2NetworkId);

			std::vector<ActorNetworkId> searchActorsIds{};
			std::vector<int> receivedActorsIndexes{};

			searchActorsIds.emplace_back(actor1NetworkId);
			simulationActorsMovementData.GetNetworkActorsIndexes(searchActorsIds, /*out*/ receivedActorsIndexes);
			REQUIRE(receivedActorsIndexes.size() == 1);
			REQUIRE(receivedActorsIndexes[0] == simulationActorsMovementData.GetNetworkActorIndex(actor1NetworkId));

			searchActorsIds.clear();
			searchActorsIds.emplace_back(actor2NetworkId);
			simulationActorsMovementData.GetNetworkActorsIndexes(searchActorsIds, /*out*/ receivedActorsIndexes);
			REQUIRE(receivedActorsIndexes.size() == 1);
			REQUIRE(receivedActorsIndexes[0] == simulationActorsMovementData.GetNetworkActorIndex(actor2NetworkId));
		}
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NullArgumentException.h>
using namespace Nest;

#include<Game/Pathfinding/Path.h>
#include<Game/SimulationCommons/SimulationActorsMovementData.h>
#include<Game/SimulationCommons/SimulationActorsMovementLogic.h>
using namespace Game;



namespace SimulationActorsMovementLogicTests
{
	TEST_CASE("SimulationActorsMovementLogic: setting actor path", "[SimulationActorsMovementLogic]")
	{
		SimulationActorsMovementData simulationActorsMovementData{};
		SimulationActorsMovementLogic simulationActorsMovementLogic{ simulationActorsMovementData };

		SECTION("Setting path of not registered actor network id throws LogicErrorException")
		{
			const ActorNetworkId actorNetworkId{ 1 };
			const auto path = std::make_shared<Path>();

			REQUIRE_THROWS_AS(simulationActorsMovementLogic.SetPath(actorNetworkId, path), LogicErrorException);
		}


		SECTION("Setting a null path to a registered actor network id throws NullArgumentException")
		{
			const ActorNetworkId actorNetworkId{ 1 };
			simulationActorsMovementData.RegisterNetworkActor(actorNetworkId);

			REQUIRE_THROWS_AS(simulationActorsMovementLogic.SetPath(actorNetworkId, nullptr), NullArgumentException);
		}


		SECTION("Setting a path to a registered actor network id sets up the path correctly")
		{
			const ActorNetworkId actorNetworkId{ 1 };
			simulationActorsMovementData.RegisterNetworkActor(actorNetworkId);

			const auto path = std::make_shared<Path>();
			REQUIRE_NOTHROW(simulationActorsMovementLogic.SetPath(actorNetworkId, path));

			REQUIRE(simulationActorsMovementData.actorsPathInformation[0].path.get() == path.get());
		}
	}



	TEST_CASE("SimulationActorsMovementLogic: updating actors movement distance", "[SimulationActorsMovementLogic]")
	{
		SimulationActorsMovementData simulationActorsMovementData{};
		SimulationActorsMovementLogic simulationActorsMovementLogic{ simulationActorsMovementData };

		SECTION("Updating without any actor is ok")
		{
			const simfix64 deltaTime{ 1.0f };
			REQUIRE_NOTHROW(simulationActorsMovementLogic.UpdateActorsMovementDistance(deltaTime));
		}


		SECTION("Updating with multiple actors will correctly update the distance")
		{
			const ActorNetworkId actorNetworkId1{ 1 };
			const ActorNetworkId actorNetworkId2{ 2 };

			simulationActorsMovementData.RegisterNetworkActor(actorNetworkId1);
			simulationActorsMovementData.RegisterNetworkActor(actorNetworkId2);

			const auto actorIndex1 = simulationActorsMovementData.GetNetworkActorIndex(actorNetworkId1);
			const simfix64 actor1Distance{ 0.0f };
			const simfix64 actor1Speed{ 1.0f };
			simulationActorsMovementData.actorsDistanceMovementInformation[actorIndex1].distance = actor1Distance;
			simulationActorsMovementData.actorsDistanceMovementInformation[actorIndex1].speed = actor1Speed;

			const auto actorIndex2 = simulationActorsMovementData.GetNetworkActorIndex(actorNetworkId2);
			const simfix64 actor2Distance{ 100.0f };
			const simfix64 actor2Speed{ 25.3f };
			simulationActorsMovementData.actorsDistanceMovementInformation[actorIndex2].distance = actor2Distance;
			simulationActorsMovementData.actorsDistanceMovementInformation[actorIndex2].speed = actor2Speed;

			const simfix64 deltaTime{ 0.166f };
			REQUIRE_NOTHROW(simulationActorsMovementLogic.UpdateActorsMovementDistance(deltaTime));

			REQUIRE((
				simulationActorsMovementData.actorsDistanceMovementInformation[actorIndex1].distance == (deltaTime * actor1Speed + actor1Distance)
			));
			REQUIRE((
				simulationActorsMovementData.actorsDistanceMovementInformation[actorIndex2].distance == (deltaTime * actor2Speed + actor2Distance)
			));
		}
	}



	TEST_CASE("SimulationActorsMovementLogic: updating information of the actors movement in frame", "[SimulationActorsMovementLogic]")
	{
		SimulationActorsMovementData simulationActorsMovementData{};
		SimulationActorsMovementLogic simulationActorsMovementLogic{ simulationActorsMovementData };

		SECTION("Updating without any actor is ok")
		{
			REQUIRE_NOTHROW(simulationActorsMovementLogic.UpdateActorsMovementInFrame());
		}


		SECTION("Updating with multiple actors will correctly update the new position information")
		{
			const ActorNetworkId actor1NetworkId{ 1 };
			const ActorNetworkId actor2NetworkId{ 2 };

			simulationActorsMovementData.RegisterNetworkActor(actor1NetworkId);
			simulationActorsMovementData.RegisterNetworkActor(actor2NetworkId);

			const auto actor1Index = simulationActorsMovementData.GetNetworkActorIndex(actor1NetworkId);
			const auto actor2Index = simulationActorsMovementData.GetNetworkActorIndex(actor2NetworkId);

			simulationActorsMovementData.actorsDistanceMovementInformation[actor1Index].distance = simfix64{ 0.5f };
			simulationActorsMovementData.actorsDistanceMovementInformation[actor2Index].distance = simfix64{ 1.0f };

			auto actor1Path = std::make_shared<Path>();
			actor1Path->PushBack(Vector2<simfix64>{ simfix64{ 0.0f }, simfix64{ 0.0f } });
			actor1Path->PushBack(Vector2<simfix64>{ simfix64{ 0.0f }, simfix64{ 1.0f } });
			actor1Path->RecalculateDistances();
			simulationActorsMovementData.actorsPathInformation[actor1Index].path = std::move(actor1Path);

			auto actor2Path = std::make_shared<Path>();
			actor2Path->PushBack(Vector2<simfix64>{ simfix64{ 1.0f }, simfix64{ 0.0f } });
			actor2Path->PushBack(Vector2<simfix64>{ simfix64{ 2.0f }, simfix64{ 0.0f } });
			actor2Path->RecalculateDistances();
			simulationActorsMovementData.actorsPathInformation[actor2Index].path = std::move(actor2Path);

			simulationActorsMovementData.actorsInFrameMovementPositions[actor1Index] = ActorInFrameMovementPositions{
				simfix64{ -1.0f }, simfix64{ -1.0f }, simfix64{ 0.0f }, simfix64{ 0.0f }
			};
			simulationActorsMovementData.actorsInFrameMovementPositions[actor2Index] = ActorInFrameMovementPositions{
				simfix64{ -1.0f }, simfix64{ -1.0f }, simfix64{ 1.0f }, simfix64{ 0.0f }
			};


			REQUIRE_NOTHROW(simulationActorsMovementLogic.UpdateActorsMovementInFrame());

			REQUIRE(simulationActorsMovementData.actorsInFrameMovementPositions[actor1Index].frameStartX == simfix64{0.0f});
			REQUIRE(simulationActorsMovementData.actorsInFrameMovementPositions[actor1Index].frameStartY == simfix64{0.0f});
			REQUIRE(simulationActorsMovementData.actorsInFrameMovementPositions[actor1Index].frameEndX == simfix64{0.0f});
			REQUIRE(simulationActorsMovementData.actorsInFrameMovementPositions[actor1Index].frameEndY == simfix64{0.5f} );

			REQUIRE(simulationActorsMovementData.actorsInFrameMovementPositions[actor2Index].frameStartX == simfix64{1.0f});
			REQUIRE(simulationActorsMovementData.actorsInFrameMovementPositions[actor2Index].frameStartY == simfix64{0.0f});
			REQUIRE(simulationActorsMovementData.actorsInFrameMovementPositions[actor2Index].frameEndX == simfix64{2.0f});
			REQUIRE(simulationActorsMovementData.actorsInFrameMovementPositions[actor2Index].frameEndY == simfix64{0.0f} );
		}
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"


#include"Game/SimulationCommons/SimulationActorsMovementData.h"
#include"Game/SimulationCommons/SimulationActorsMovementDataManager.h"
using namespace Game;
using namespace Nest;



namespace SimulationActorsMovementDataManagerTests
{
	TEST_CASE("SimulationActorsMovementDataManager: pending add and removes", "[SimulationActorsMovementDataManager]")
	{
		SimulationActorsMovementData simulationActorsMovementData{};
		SimulationActorsMovementDataManager simulationActorsMovementDataManager{ simulationActorsMovementData };



		SECTION("Applying with empty container does nothing")
		{
			simulationActorsMovementDataManager.ApplyPendingOperations();
			REQUIRE(simulationActorsMovementData.actorsNetworkIds.empty());
		}



		SECTION("Applying with two added components will result in two added components")
		{
			simulationActorsMovementDataManager.RegisterNewActorPendingToAdd(ActorNetworkId{ 1 });
			simulationActorsMovementDataManager.RegisterNewActorPendingToAdd(ActorNetworkId{ 2 });

			REQUIRE(simulationActorsMovementData.actorsNetworkIds.empty());

			simulationActorsMovementDataManager.ApplyPendingOperations();
			REQUIRE(simulationActorsMovementData.actorsNetworkIds.size() == 2);

			// Repeat to guarantee that its not adding repeatedly
			simulationActorsMovementDataManager.ApplyPendingOperations();
			REQUIRE(simulationActorsMovementData.actorsNetworkIds.size() == 2);
		}



		SECTION("Applying two added components, and then removing both")
		{
			const ActorNetworkId actorNetworkId1{ 1 };
			const ActorNetworkId actorNetworkId2{ 2 };

			simulationActorsMovementDataManager.RegisterNewActorPendingToAdd(actorNetworkId1);
			simulationActorsMovementDataManager.RegisterNewActorPendingToAdd(actorNetworkId2);

			simulationActorsMovementDataManager.ApplyPendingOperations();
			REQUIRE(simulationActorsMovementData.actorsNetworkIds.size() == 2);

			simulationActorsMovementDataManager.RegisterNewActorPendingToRemove(actorNetworkId2);
			simulationActorsMovementDataManager.RegisterNewActorPendingToRemove(actorNetworkId1);
			REQUIRE(simulationActorsMovementData.actorsNetworkIds.size() == 2);

			simulationActorsMovementDataManager.ApplyPendingOperations();
			REQUIRE(simulationActorsMovementData.actorsNetworkIds.empty());

			// Repeat to guarantee that won't try to remove twice
			simulationActorsMovementDataManager.ApplyPendingOperations();
			REQUIRE(simulationActorsMovementData.actorsNetworkIds.empty());
		}



		SECTION("Adding and removing the components before applying")
		{
			const ActorNetworkId actorNetworkId1{ 1 };
			const ActorNetworkId actorNetworkId2{ 2 };

			simulationActorsMovementDataManager.RegisterNewActorPendingToAdd(actorNetworkId1);
			simulationActorsMovementDataManager.RegisterNewActorPendingToAdd(actorNetworkId2);

			simulationActorsMovementDataManager.RegisterNewActorPendingToRemove(actorNetworkId2);
			simulationActorsMovementDataManager.RegisterNewActorPendingToRemove(actorNetworkId1);

			simulationActorsMovementDataManager.ApplyPendingOperations();
			REQUIRE(simulationActorsMovementData.actorsNetworkIds.empty());
		}
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"

#include<vector>

#include"../../Helpers/EntitiesManagerHelper.h"
using namespace Helpers;

#include<Nest/ComponentTypeMap.h>
using namespace Nest;

#include<Game/SimulationCommons/ActorBehavior.h>
#include<Game/SimulationCommons/ActorsManager.h>
using namespace Game;



namespace ActorsManagerTests
{
	TEST_CASE("ActorsManager::InstantiateNew", "[ActorsManager]")
	{
		ComponentTypeMap componentTypeMap{ false };
		const auto world = InstantiateWorld();
		ActorsManager actorsManager{ *world->GetEntitiesManager() };

		RegisterComponentFactory<ActorBehavior>(componentTypeMap, *world);


		SECTION("Intantiating a new actor returns a new ActorId every time")
		{
			std::vector<ActorId> actorsIds{};
			REQUIRE_NOTHROW(actorsIds.emplace_back(actorsManager.InstantiateNew(false)));
			REQUIRE_NOTHROW(actorsIds.emplace_back(actorsManager.InstantiateNew(false)));

			REQUIRE(actorsIds[0] != InvalidActorId);
			REQUIRE(actorsIds[1] != InvalidActorId);
			REQUIRE(actorsIds[0] != actorsIds[1]);
		}


		SECTION("Instantiating new actors that are also network actors, will return actors with unique network ids")
		{
			std::vector<ActorId> actorsIds{};
			REQUIRE_NOTHROW(actorsIds.emplace_back(actorsManager.InstantiateNew(true)));
			REQUIRE_NOTHROW(actorsIds.emplace_back(actorsManager.InstantiateNew(true)));

			std::vector<ActorNetworkId> actorsNetworkIds{};
			REQUIRE_NOTHROW(actorsManager.GetActorsNetworkIdsByAppending(actorsIds, actorsNetworkIds));

			REQUIRE(actorsNetworkIds[0] != InvalidActorNetworkId);
			REQUIRE(actorsNetworkIds[1] != InvalidActorNetworkId);
			REQUIRE(actorsNetworkIds[0] != actorsNetworkIds[1]);
		}


		SECTION("Intantiating non network actors result in actors with invalid network ids (to avoid mistakes)")
		{
			std::vector<ActorId> actorsIds{};
			REQUIRE_NOTHROW(actorsIds.emplace_back(actorsManager.InstantiateNew(true)));
			REQUIRE_NOTHROW(actorsIds.emplace_back(actorsManager.InstantiateNew(false)));

			ActorNetworkId actor1NetworkId, actor2NetworkId;
			REQUIRE_NOTHROW(actor1NetworkId = actorsManager.GetActor(actorsIds[0])->GetNetworkId());
			REQUIRE_NOTHROW(actor2NetworkId = actorsManager.GetActor(actorsIds[1])->GetNetworkId());

			REQUIRE(actor1NetworkId != InvalidActorNetworkId);
			REQUIRE(actor2NetworkId == InvalidActorNetworkId);
		}
	}
}

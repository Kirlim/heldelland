
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"



#include<vector>

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Vector2.h>
#include<Nest/Material.h>
using namespace Nest;

#include<Game/Map/Tileset.h>
using namespace Game;



namespace TilesetTests
{
	TEST_CASE("Tileset::Tileset", "[Tileset]")
	{
		int tilesCount;
		std::vector<unsigned int> ids;
		std::vector<Vector2f> uvsStarts;
		std::vector<Vector2f> uvsEnds;
		Material material{};

		SECTION("Tileset is created correctly with correct values")
		{
			tilesCount = 2;

			for (auto i = 0; i < tilesCount; ++i)
			{
				ids.push_back(i * 17);
				uvsStarts.emplace_back(float(i) / 2.0f, float(i) / 2.0f);
				uvsEnds.emplace_back(float(i + 1) / 2.0f, float(i + 1) / 2.0f);
			}

			Tileset tileset{ tilesCount, ids, uvsStarts, uvsEnds, &material };

			REQUIRE(tileset.GetMaterial() == &material);
			for (int i = 0; i < tilesCount; ++i)
			{
				Vector2f uvStart, uvEnd;
				tileset.GetTileInformation(i * 17, /*out*/ uvStart, /*out*/ uvEnd);

				REQUIRE((uvStart.x == ((float)i / 2.0f) && uvStart.y == ((float)i / 2.0f)));
				REQUIRE((uvEnd.x == ((float)(i + 1) / 2.0f) && uvEnd.y == ((float)(i + 1) / 2.0f)));
			}
		}

		SECTION("Creating a tileset with ids count different from tiles count throws LogicErrorException")
		{
			tilesCount = 1;
			ids.push_back(0);
			ids.push_back(2);
			uvsStarts.emplace_back(0.0f, 0.0f);
			uvsEnds.emplace_back(0.0f, 0.0f);

			REQUIRE_THROWS_AS(Tileset(tilesCount, ids, uvsStarts, uvsEnds, &material), Nest::LogicErrorException);
		}

		SECTION("Creating a tileset with uvs start count different from tiles count throws LogicErrorException")
		{
			tilesCount = 1;
			ids.push_back(0);
			uvsStarts.emplace_back(0.0f, 0.0f);
			uvsStarts.emplace_back(0.0f, 0.0f);
			uvsEnds.emplace_back(0.0f, 0.0f);

			REQUIRE_THROWS_AS(Tileset(tilesCount, ids, uvsStarts, uvsEnds, &material), Nest::LogicErrorException);
		}

		SECTION("Creating a tileset with uvs end count different from tiles count throws LogicErrorException")
		{
			tilesCount = 1;
			ids.push_back(0);
			uvsStarts.emplace_back(0.0f, 0.0f);
			uvsEnds.emplace_back(0.0f, 0.0f);
			uvsEnds.emplace_back(0.0f, 0.0f);

			REQUIRE_THROWS_AS(Tileset(tilesCount, ids, uvsStarts, uvsEnds, &material), Nest::LogicErrorException);
		}

		SECTION("Creating a tileset with 0 tiles ids throws LogicErrorException")
		{
			tilesCount = 0;
			REQUIRE_THROWS_AS(Tileset(tilesCount, ids, uvsStarts, uvsEnds, &material), Nest::LogicErrorException);
		}

		SECTION("Creating a tileset with repeated tiles ids throws LogicErrorException")
		{
			tilesCount = 2;
			ids.push_back(0);
			ids.push_back(0);
			uvsStarts.emplace_back(0.0f, 0.0f);
			uvsStarts.emplace_back(0.0f, 0.0f);
			uvsEnds.emplace_back(0.0f, 0.0f);
			uvsEnds.emplace_back(0.0f, 0.0f);

			REQUIRE_THROWS_AS(Tileset(tilesCount, ids, uvsStarts, uvsEnds, &material), Nest::LogicErrorException);
		}
	}



	TEST_CASE("Tileset::SetTileInformation", "[Tileset]")
	{
		const int tilesCount = 2;
		std::vector<unsigned int> ids;
		std::vector<Vector2f> uvsStarts;
		std::vector<Vector2f> uvsEnds;
		Material material{};

		for (auto i = 0; i < tilesCount; ++i)
		{
			ids.push_back(i);
			uvsStarts.emplace_back(float(i) / 2.0f, float(i) / 2.0f);
			uvsEnds.emplace_back(float(i + 1) / 2.0f, float(i + 1) / 2.0f);
		}

		Tileset tileset{ tilesCount, ids, uvsStarts, uvsEnds, &material };

		SECTION("Changing an existing tile uvs")
		{
			tileset.SetTileInformation(0, Nest::Vector2f(0.0f, 0.0f), Nest::Vector2f(1.0f, 1.0f));

			Vector2f uvStart, uvEnd;
			tileset.GetTileInformation(0, /*out*/ uvStart, /*out*/ uvEnd);

			REQUIRE((uvStart.x == 0.0f && uvStart.y == 0.0f));
			REQUIRE((uvEnd.x == 1.0f && uvEnd.y == 1.0f));
		}

		SECTION("Attempting to unregister a non-existing tile id throws LogicErrorException")
		{
			REQUIRE_THROWS_AS(tileset.SetTileInformation(10, Nest::Vector2f(0.0f, 0.0f), Nest::Vector2f(1.0f, 1.0f)), LogicErrorException);
		}
	}



	TEST_CASE("Tileset::GetTileInformation", "[Tileset]")
	{
		const int tilesCount = 1;
		std::vector<unsigned int> ids;
		std::vector<Vector2f> uvsStarts;
		std::vector<Vector2f> uvsEnds;
		Material material{};

		ids.push_back(0);
		uvsStarts.emplace_back(0.0f, 0.0f);
		uvsEnds.emplace_back(1.0f, 1.0f);

		Tileset tileset{ tilesCount, ids, uvsStarts, uvsEnds, &material };

		SECTION("Fetching from a non-existing tile id throw LogicErrorException")
		{
			Vector2f uvStart, uvEnd;
			REQUIRE_THROWS_AS(tileset.GetTileInformation(100, uvStart, uvEnd), LogicErrorException);
		}
	}
}

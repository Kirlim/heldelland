
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"

#include<Nest/MeshesFactory.h>
using namespace Nest;

#include<Game/Map/IMapData.h>
#include<Game/Map/Map.h>
#include<Game/Map/MapData.h>
#include<Game/Map/MapsFactory.h>
using namespace Game;



namespace MapsFactoryTests
{
	TEST_CASE("Maps Factory Instantiate method does not return nullptr", "[MapsFactoryTests]")
	{
		const MeshesFactory meshesFactory{ []() { return nullptr;} };
		MapsFactory mapsFactory{ meshesFactory };

		std::unique_ptr<IMapData> mapData = std::make_unique<MapData>();

		const auto newMap = mapsFactory.Instantiate(mapData);
		REQUIRE(newMap != nullptr);
	}
}

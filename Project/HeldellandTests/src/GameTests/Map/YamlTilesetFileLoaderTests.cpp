
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"



#include<memory>

#include<Nest/Exceptions/InvalidDataException.h>
#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/InvalidFileStructureException.h>

#include<Nest/Vector2.h>
#include<Nest/Material.h>
#include<Nest/ResourcesManager.h>
using namespace Nest;

#include<Game/Map/Tileset.h>
#include<Game/Map/YamlTilesetFileLoader.h>
using namespace Game;



bool AreEqual(const float x, const float y)
{
	auto diff = x - y;
	if (diff < 0) diff = 0 - diff;

	return(diff < 0.00001f);
}



namespace YamlTilesetFileLoaderTests
{
	TEST_CASE("YamlTilesetFileLoader::Load", "[YamlTilesetFileLoader]")
	{
		ResourcesManager resourcesManager;
		{
			auto material = std::make_unique<Material>();
			resourcesManager.AddMaterial("TestMaterial", material);
		}

		YamlTilesetFileLoader tilesetFileLoader{ resourcesManager };

		const float textureSizeX = 128;
		const float textureSizeY = 64;


		SECTION("Loading a valid tileset file")
		{
			auto tileset = tilesetFileLoader.Load("GameTests/Data/Tilesets/ValidTestTileset.tls");

			Vector2f uvStart, uvEnd;
			REQUIRE_NOTHROW(tileset->GetTileInformation(0, /*out*/ uvStart, /*out*/ uvEnd));
			REQUIRE((AreEqual(uvStart.x, (1.0f / textureSizeX)) && AreEqual(uvStart.y, (1.0f / textureSizeY))));
			REQUIRE((AreEqual(uvEnd.x, (19.0f / textureSizeX)) && AreEqual(uvEnd.y, (19.0f / textureSizeY))));

			REQUIRE_NOTHROW(tileset->GetTileInformation(10, /*out*/ uvStart, /*out*/ uvEnd));
			REQUIRE((AreEqual(uvStart.x, (21.0f / textureSizeX)) && AreEqual(uvStart.y, (21.0f / textureSizeY))));
			REQUIRE((AreEqual(uvEnd.x, (39.0f / textureSizeX)) && AreEqual(uvEnd.y, (39.0f / textureSizeY))));

			auto material = resourcesManager.FetchMaterial("TestMaterial");
			REQUIRE(tileset->GetMaterial() == material);
		}

		SECTION("Loading a valid tileset file without a material set")
		{
			auto tileset = tilesetFileLoader.Load("GameTests/Data/Tilesets/ValidTestTileset_NoMaterial.tls");

			Vector2f uvStart, uvEnd;
			REQUIRE_NOTHROW(tileset->GetTileInformation(0, /*out*/ uvStart, /*out*/ uvEnd));
			REQUIRE((AreEqual(uvStart.x, (1.0f / textureSizeX)) && AreEqual(uvStart.y, (1.0f / textureSizeY))));
			REQUIRE((AreEqual(uvEnd.x, (19.0f / textureSizeX)) && AreEqual(uvEnd.y, (19.0f / textureSizeY))));

			REQUIRE_NOTHROW(tileset->GetTileInformation(10, /*out*/ uvStart, /*out*/ uvEnd));
			REQUIRE((AreEqual(uvStart.x, (21.0f / textureSizeX)) && AreEqual(uvStart.y, (21.0f / textureSizeY))));
			REQUIRE((AreEqual(uvEnd.x, (39.0f / textureSizeX)) && AreEqual(uvEnd.y, (39.0f / textureSizeY))));

			REQUIRE(tileset->GetMaterial() == nullptr);
		}

		SECTION("Loading a valid tileset with a material that is not in the resources manager throws LogicErrorException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/ValidTestTileset_MaterialNotFound.tls"), LogicErrorException);
		}

		SECTION("Loading a tileset with tile size 0 throws InvalidDataException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_TileSize0.tls"), InvalidDataException);
		}

		SECTION("Loading a tileset with negative tile size throws InvalidDataException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_NegativeTileSize.tls"), InvalidDataException);
		}

		SECTION("Loading a tileset with negative tile pixel repeat size throws InvalidDataException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_NegativeTilePixelsRepeatSize.tls"), InvalidDataException);
		}

		SECTION("Loading a tileset with texture width 0 throws InvalidDataException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_TextureWidth0.tls"), InvalidDataException);
		}

		SECTION("Loading a tileset with texture height 0 throws InvalidDataException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_TextureHeight0.tls"), InvalidDataException);
		}

		SECTION("Loading a tileset with negative texture width throws InvalidDataException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_NegativeTextureWidth.tls"), InvalidDataException);
		}

		SECTION("Loading a tileset with negative texture height throws InvalidDataException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_NegativeTextureHeight.tls"), InvalidDataException);
		}

		SECTION("Loading a tileset without a Tile Size node throws InvalidFileStructureException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_NoTileSize.tls"), InvalidFileStructureException);
		}

		SECTION("Loading a tileset without a Tile Pixel Repeat Size node throws InvalidFileStructureException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_NoTilePixelRepeatSize.tls"), InvalidFileStructureException);
		}

		SECTION("Loading a tileset without a Tileset node throws InvalidFileStructureException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_NoTileset.tls"), InvalidFileStructureException);
		}

		SECTION("Loading a tileset without a TextureSize node throws InvalidFileStructureException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_NoTextureSize.tls"), InvalidFileStructureException);
		}

		SECTION("Loading a tileset without a TextureSizeX node throws InvalidFileStructureException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_NoTextureSizeX.tls"), InvalidFileStructureException);
		}

		SECTION("Loading a tileset without a TextureSizeY node throws InvalidFileStructureException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_NoTextureSizeY.tls"), InvalidFileStructureException);
		}

		SECTION("Loading a tileset with tile with negative Id throws InvalidDataException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_TileWithNegativeTileId.tls"), InvalidDataException);
		}

		SECTION("Loading a tileset with tile with negative xPos throws InvalidDataException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_TileWithNegativeTileXPos.tls"), InvalidDataException);
		}

		SECTION("Loading a tileset with tile with negative yPos throws InvalidDataException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_TileWithNegativeTileYPos.tls"), InvalidDataException);
		}

		SECTION("Loading a tileset with a tile without an id node throws InvalidFileStructureException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_TileWithoutId.tls"), InvalidFileStructureException);
		}

		SECTION("Loading a tileset with a tile without a xPos node throws InvalidFileStructureException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_TileWithoutXPos.tls"), InvalidFileStructureException);
		}

		SECTION("Loading a tileset with a tile without a yPos node throws InvalidFileStructureException")
		{
			REQUIRE_THROWS_AS(tilesetFileLoader.Load("GameTests/Data/Tilesets/InvalidTestTileset_TileWithoutYPos.tls"), InvalidFileStructureException);
		}
	}
}

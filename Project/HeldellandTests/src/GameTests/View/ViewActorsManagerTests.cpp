//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NullArgumentException.h>
using namespace Nest;

#include<Game/View/ViewActorsManager.h>
using namespace Game;



namespace ViewActorsManagerTests
{
	TEST_CASE("ViewActorsManager: Registering and unregistering Moving Actors", "[ViewActorsManagerTests]")
	{
		ViewActorsManager viewActorsManager{};


		SECTION("Registering two moving actors")
		{
			const ActorNetworkId actorNetworkId1{ 1 };
			const EntityId entityId1{ 11 };

			const ActorNetworkId actorNetworkId2{ 2 };
			const EntityId entityId2{ 22 };

			REQUIRE_FALSE(viewActorsManager.IsMovingActorRegistered(actorNetworkId1));
			REQUIRE_FALSE(viewActorsManager.IsMovingActorRegistered(actorNetworkId2));

			REQUIRE_NOTHROW(viewActorsManager.RegisterMovingActor(actorNetworkId1, entityId1));
			REQUIRE(viewActorsManager.IsMovingActorRegistered(actorNetworkId1));
			REQUIRE_FALSE(viewActorsManager.IsMovingActorRegistered(actorNetworkId2));

			REQUIRE_NOTHROW(viewActorsManager.RegisterMovingActor(actorNetworkId2, entityId2));
			REQUIRE(viewActorsManager.IsMovingActorRegistered(actorNetworkId1));
			REQUIRE(viewActorsManager.IsMovingActorRegistered(actorNetworkId2));
		}



		SECTION("Registering and unregistering two moving actors")
		{
			const ActorNetworkId actorNetworkId1{ 1 };
			const EntityId entityId1{ 11 };

			const ActorNetworkId actorNetworkId2{ 2 };
			const EntityId entityId2{ 22 };

			REQUIRE_NOTHROW(viewActorsManager.RegisterMovingActor(actorNetworkId1, entityId1));
			REQUIRE_NOTHROW(viewActorsManager.RegisterMovingActor(actorNetworkId2, entityId2));

			REQUIRE(viewActorsManager.IsMovingActorRegistered(actorNetworkId1));
			REQUIRE(viewActorsManager.IsMovingActorRegistered(actorNetworkId2));

			REQUIRE(viewActorsManager.UnregisterMovingActorIfRegistered(actorNetworkId1));
			REQUIRE_FALSE(viewActorsManager.IsMovingActorRegistered(actorNetworkId1));
			REQUIRE(viewActorsManager.IsMovingActorRegistered(actorNetworkId2));

			REQUIRE(viewActorsManager.UnregisterMovingActorIfRegistered(actorNetworkId2));
			REQUIRE_FALSE(viewActorsManager.IsMovingActorRegistered(actorNetworkId1));
			REQUIRE_FALSE(viewActorsManager.IsMovingActorRegistered(actorNetworkId2));

			REQUIRE_FALSE(viewActorsManager.UnregisterMovingActorIfRegistered(actorNetworkId1));
			REQUIRE_FALSE(viewActorsManager.UnregisterMovingActorIfRegistered(actorNetworkId2));
		}



		SECTION("Registering already registered throws LogicErrorException")
		{
			const ActorNetworkId actorNetworkId1{ 1 };
			const EntityId entityId1{ 11 };

			REQUIRE_NOTHROW(viewActorsManager.RegisterMovingActor(actorNetworkId1, entityId1));
			REQUIRE_THROWS_AS(viewActorsManager.RegisterMovingActor(actorNetworkId1, entityId1), LogicErrorException);
		}
	}



	TEST_CASE("ViewActorsManager: Executing an action for all registered moving actors", "[ViewActorsManager]")
	{
		ViewActorsManager viewActorsManager{};


		SECTION("Method iterates over all actors")
		{
			const ActorNetworkId actorNetworkId1{ 1 };
			const EntityId entityId1{ 11 };

			const ActorNetworkId actorNetworkId2{ 2 };
			const EntityId entityId2{ 22 };

			REQUIRE_NOTHROW(viewActorsManager.RegisterMovingActor(actorNetworkId1, entityId1));
			REQUIRE_NOTHROW(viewActorsManager.RegisterMovingActor(actorNetworkId2, entityId2));

			bool actor1Ok = false;
			bool actor2Ok = false;

			viewActorsManager.ExecuteForAllRegisteredMovingActors(
				[&](auto begin, auto end)
				{
					while (begin != end)
					{
						const auto& actorInfo = *begin;
						const auto& networkId = std::get<ActorNetworkId>(actorInfo);
						const auto& entityId = std::get<EntityId>(actorInfo);

						if (networkId == actorNetworkId1 && entityId == entityId1)
							actor1Ok = true;
						else if (networkId == actorNetworkId2 && entityId == entityId2)
							actor2Ok = true;

						++begin;
					}
				}
			);

			REQUIRE(actor1Ok);
			REQUIRE(actor2Ok);
		}



		SECTION("If action is null, NullArgumentException will be thrown")
		{
			REQUIRE_THROWS_AS(viewActorsManager.ExecuteForAllRegisteredMovingActors(nullptr), NullArgumentException);
		}
	}
}

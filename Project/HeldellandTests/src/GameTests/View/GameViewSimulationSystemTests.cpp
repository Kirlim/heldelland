#include"../../catch.hpp"

#include<memory>

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NullArgumentException.h>
using namespace Nest;

#include<Game/View/IGameViewSimulation.h>
#include<Game/View/GameViewSimulationSystem.h>
using namespace Game;



namespace GameViewSimulationSystemTests
{
	TEST_CASE("GameViewSimulationSystem::StartViewSimulation method", "[GameViewSimulationSystem]")
	{
		class GameViewSimulationDummy : public IGameViewSimulation
		{
		public:
			~GameViewSimulationDummy() override
			{
			}

		private:
			void DoUpdateViewSimulation(float) const override
			{
			}

			void DoBindToGameClientSimulation(IGameClientSimulation&) override
			{
			}

			void DoRegisterToClientSimulationEvents(IGameClientSimulation&) override
			{
			}
		};



		GameViewSimulationSystem gameViewSimulationSystem;

		SECTION("Passing a null pointer will throw NullArgumenException")
		{
			std::unique_ptr<IGameViewSimulation> gameViewSimulationDummy{};
			REQUIRE_THROWS_AS(gameViewSimulationSystem.StartViewSimulation(gameViewSimulationDummy), NullArgumentException);
		}

		SECTION("Passing a valid view simulation will move it")
		{
			std::unique_ptr<IGameViewSimulation> gameViewSimulationDummy = std::make_unique<GameViewSimulationDummy>();
			auto dummyAddress = gameViewSimulationDummy.get();

			REQUIRE_NOTHROW(gameViewSimulationSystem.StartViewSimulation(gameViewSimulationDummy));
			REQUIRE(gameViewSimulationSystem.GetViewSimulation() == dummyAddress);
			REQUIRE(gameViewSimulationDummy == nullptr);
		}

		SECTION("Passing views twice will throw LogicErrorException")
		{
			std::unique_ptr<IGameViewSimulation> gameViewSimulationDummy1 = std::make_unique<GameViewSimulationDummy>();
			std::unique_ptr<IGameViewSimulation> gameViewSimulationDummy2 = std::make_unique<GameViewSimulationDummy>();
			auto dummyAddress1 = gameViewSimulationDummy1.get();

			REQUIRE_NOTHROW(gameViewSimulationSystem.StartViewSimulation(gameViewSimulationDummy1));
			REQUIRE_THROWS_AS(gameViewSimulationSystem.StartViewSimulation(gameViewSimulationDummy2), LogicErrorException);
			REQUIRE(gameViewSimulationSystem.GetViewSimulation() == dummyAddress1);
			REQUIRE(gameViewSimulationDummy2 != nullptr);
		}
	}
}

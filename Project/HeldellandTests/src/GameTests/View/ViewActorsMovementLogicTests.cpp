//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"

#include<Game/Map/ITerrainView.h>
#include<Game/View/ViewActorsMovementLogic.h>
using namespace Game;
using namespace Nest;



namespace ViewActorsMovementLogicTests
{
	bool CompareVectors(Vector2f source, Vector2f other)
	{
		auto xDiff = source.x - other.x;
		auto yDiff = source.y - other.y;

		if (xDiff < 0) xDiff = 0.0f - xDiff;
		if (yDiff < 0) yDiff = 0.0f - yDiff;

		return (xDiff < 0.0001f && yDiff < 0.0001f);
	}



	TEST_CASE("Updating the view actors positions from tick data", "[ViewActorsMovementData]")
	{
		ViewActorsMovementLogic viewActorsMovementLogic;

		ActorNetworkId actorNetworkId1{ 1 };
		ActorNetworkId actorNetworkId2{ 2 };

		ActorInFrameMovementPositions actorInFrameMovementPositions1{
			simfix64{ 0.0f }, simfix64{ 0.0f },
			simfix64{ 1.0f }, simfix64{ 0.0f }
		};
		ActorInFrameMovementPositions actorInFrameMovementPositions2{
			simfix64{ 0.0f }, simfix64{ 0.0f },
			simfix64{ 0.0f }, simfix64{ 1.0f }
		};

		std::vector<ActorNetworkId> actorsNetworkIds{};
		actorsNetworkIds.emplace_back(actorNetworkId1);
		actorsNetworkIds.emplace_back(actorNetworkId2);

		std::vector<ActorInFrameMovementPositions> actorsInFrameMovementPositions{};
		actorsInFrameMovementPositions.emplace_back(actorInFrameMovementPositions1);
		actorsInFrameMovementPositions.emplace_back(actorInFrameMovementPositions2);


		SECTION("Running mid frame")
		{
			const float frameTimePct = 0.5f;
			viewActorsMovementLogic.CalculateActorsPositions(frameTimePct, actorsNetworkIds, actorsInFrameMovementPositions);

			REQUIRE(viewActorsMovementLogic.actorsNetworkIds.size() == 2);
			REQUIRE(viewActorsMovementLogic.actorsNetworkIds[0] == actorNetworkId1);
			REQUIRE(viewActorsMovementLogic.actorsNetworkIds[1] == actorNetworkId2);

			REQUIRE(viewActorsMovementLogic.actorsPositions.size() == 2);
			REQUIRE(CompareVectors(viewActorsMovementLogic.actorsPositions[0], Vector2f{0.5f, 0.0f}));
			REQUIRE(CompareVectors(viewActorsMovementLogic.actorsPositions[1], Vector2f{0.0f, 0.5f}));
		}


		SECTION("Frame elapsed time % has is corrected to minimum 0.0f")
		{
			const float frameTimePct = -1.5f;
			viewActorsMovementLogic.CalculateActorsPositions(frameTimePct, actorsNetworkIds, actorsInFrameMovementPositions);

			REQUIRE(viewActorsMovementLogic.actorsNetworkIds.size() == 2);
			REQUIRE(viewActorsMovementLogic.actorsNetworkIds[0] == actorNetworkId1);
			REQUIRE(viewActorsMovementLogic.actorsNetworkIds[1] == actorNetworkId2);

			REQUIRE(viewActorsMovementLogic.actorsPositions.size() == 2);
			REQUIRE(CompareVectors(viewActorsMovementLogic.actorsPositions[0], Vector2f{0.0f, 0.0f}));
			REQUIRE(CompareVectors(viewActorsMovementLogic.actorsPositions[1], Vector2f{0.0f, 0.0f}));
		}


		SECTION("Frame elapsed time % has is corrected to maximum 1.0f")
		{
			const float frameTimePct = 1.5f;
			viewActorsMovementLogic.CalculateActorsPositions(frameTimePct, actorsNetworkIds, actorsInFrameMovementPositions);

			REQUIRE(viewActorsMovementLogic.actorsNetworkIds.size() == 2);
			REQUIRE(viewActorsMovementLogic.actorsNetworkIds[0] == actorNetworkId1);
			REQUIRE(viewActorsMovementLogic.actorsNetworkIds[1] == actorNetworkId2);

			REQUIRE(viewActorsMovementLogic.actorsPositions.size() == 2);
			REQUIRE(CompareVectors(viewActorsMovementLogic.actorsPositions[0], Vector2f{1.0f, 0.0f}));
			REQUIRE(CompareVectors(viewActorsMovementLogic.actorsPositions[1], Vector2f{0.0f, 1.0f}));
		}


		SECTION("Executing twice with the same input yields the same output")
		{
			const float frameTimePct = 0.5f;
			viewActorsMovementLogic.CalculateActorsPositions(frameTimePct, actorsNetworkIds, actorsInFrameMovementPositions);
			viewActorsMovementLogic.CalculateActorsPositions(frameTimePct, actorsNetworkIds, actorsInFrameMovementPositions);

			REQUIRE(viewActorsMovementLogic.actorsNetworkIds.size() == 2);
			REQUIRE(viewActorsMovementLogic.actorsNetworkIds[0] == actorNetworkId1);
			REQUIRE(viewActorsMovementLogic.actorsNetworkIds[1] == actorNetworkId2);

			REQUIRE(viewActorsMovementLogic.actorsPositions.size() == 2);
			REQUIRE(CompareVectors(viewActorsMovementLogic.actorsPositions[0], Vector2f{0.5f, 0.0f}));
			REQUIRE(CompareVectors(viewActorsMovementLogic.actorsPositions[1], Vector2f{0.0f, 0.5f}));
		}
	}



	class TerrainViewMock : public ITerrainView
	{
	public:
		~TerrainViewMock() override = default;

		void GetPositionsHeights(const std::vector<Nest::Vector2f>& positions, std::vector<float>& out_Heights) const override
		{
			getPositionsHeightsCalled = true;
		}


	public:
		mutable bool getPositionsHeightsCalled = false;
	};



	TEST_CASE("Updating the view actors position heights correctly forwards the call to the terrain")
	{
		TerrainViewMock terrainViewMock{};
		ViewActorsMovementLogic viewActorsMovementLogic{};

		REQUIRE_FALSE(terrainViewMock.getPositionsHeightsCalled);
		REQUIRE_NOTHROW(viewActorsMovementLogic.CalculateActorsPositionsHeights(terrainViewMock));
		REQUIRE(terrainViewMock.getPositionsHeightsCalled);
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"

#include<memory>

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NullArgumentException.h>
#include<Nest/Exceptions/NullPointerException.h>
using namespace Nest;

#include<Game/Network/HostGameInformation.h>
#include<Game/Network/HostChannel.h>
#include<Game/Network/Host.h>
using namespace Game;



namespace HostsTests
{
	std::unique_ptr<IHostChannel> HostChannelFactoryFunction(IHost& host)
	{
		return std::make_unique<HostChannel>(host);
	}



	TEST_CASE("Instantiating new Host", "[Host]")
	{
		SECTION("Null channel factory function will throw NullArgumentException")
		{
			REQUIRE_THROWS_AS(Host( HostId{ 1 }, nullptr ), NullArgumentException);
		}

		SECTION("Channel factory function must return non-null channel, or else, throw NullPointerException")
		{
			REQUIRE_THROWS_AS(Host(HostId{1}, [](IHost&) {return nullptr;}), NullPointerException);
		}

		SECTION("On success, both channels are valid and distinct")
		{
			const Host host{ HostId{ 1 }, HostChannelFactoryFunction };

			REQUIRE(host.GetFromHostChannel() != nullptr);
			REQUIRE(host.GetToHostChannel() != nullptr);
			REQUIRE(host.GetFromHostChannel() != host.GetToHostChannel());

			REQUIRE(host.GetFromHostChannel()->GetHost()->GetLocadId() == host.GetLocadId());
			REQUIRE(host.GetToHostChannel()->GetHost()->GetLocadId() == host.GetLocadId());
		}
	}



	TEST_CASE("Setting Host Game Information", "[Host]")
	{
		Host host{ HostId{ 1 }, HostChannelFactoryFunction };

		SECTION("Setting a null Host Game Information throws NullArgumentException")
		{
			std::unique_ptr<HostGameInformation> hostGameInformation;
			REQUIRE_THROWS_AS(host.SetGameInformation(hostGameInformation), NullArgumentException);
		}

		SECTION("Setting a valid Host Game Information moves the data correctly")
		{
			auto hostGameInformation = std::make_unique<HostGameInformation>();
			auto hostGameInformationAddress = hostGameInformation.get();

			REQUIRE_NOTHROW(host.SetGameInformation(hostGameInformation));
			REQUIRE(hostGameInformationAddress == host.GetGameInformation());
			REQUIRE(hostGameInformation == nullptr);
		}

		SECTION("Setting valid host twice throws LogicErrorException")
		{
			auto hostGameInformation1 = std::make_unique<HostGameInformation>();
			auto hostGameInformation2 = std::make_unique<HostGameInformation>();

			REQUIRE_NOTHROW(host.SetGameInformation(hostGameInformation1));
			REQUIRE_THROWS_AS(host.SetGameInformation(hostGameInformation2), LogicErrorException);
			REQUIRE(hostGameInformation1 == nullptr);
			REQUIRE(hostGameInformation2 != nullptr);
		}
	}
}

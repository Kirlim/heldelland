//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../catch.hpp"

#include<Game/ConsoleCommands/GameNetworkConsoleCommandsHandler.h>
using namespace Game;

#include<Nest/Exceptions/InvalidArgumentException.h>
#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Helpers/StringHelper.h>
using namespace Nest;



namespace GameNetworkConsoleCommandsHandlerTests
{
	struct TestFunctionsData
	{
		bool isClient = false;
		bool isStarted = false;
		bool hasGameStarted = false;

		bool hasBeenConnectedAsynchronously = false;
		std::string connectedIp = "";
		unsigned int connectedPort = 0;
	};



	std::unique_ptr<GameNetworkConsoleCommandsHandler> InstantiateGameNetworkConsoleCommandsHandler(
		TestFunctionsData& testFunctionsData
	)
	{
		auto gameNetworkConsoleCommandsHandler = std::make_unique<GameNetworkConsoleCommandsHandler>(
			[&](const unsigned int port, const bool client)
			{
				testFunctionsData.isStarted = true;
				testFunctionsData.isClient = client;
			},
			[&]()
			{
				return testFunctionsData.isStarted;
			},
			[&]()
			{
				testFunctionsData.hasGameStarted = true;
			},
			[&]()
			{
				return testFunctionsData.hasGameStarted;
			},
			[&](const std::string ip, const unsigned int port)
			{
				testFunctionsData.hasBeenConnectedAsynchronously = true;
				testFunctionsData.connectedIp = ip;
				testFunctionsData.connectedPort = port;
			}
		);

		return gameNetworkConsoleCommandsHandler;
	}



	std::vector<std::string> SplitCommand(std::string command)
	{
		return Helpers::SplitString(command, ' ');
	}



	TEST_CASE("net_start command tests", "[GameNetworkConsoleCommandsHandler]")
	{
		TestFunctionsData testFunctionsData;
		const auto gameNetworkConsoleCommandsHandler = InstantiateGameNetworkConsoleCommandsHandler(testFunctionsData);

		SECTION("Wrong number of arguments throws InvalidArgumentException")
		{
			const auto command = "net_start";
			const auto splittedCommand = SplitCommand(command);

			REQUIRE_THROWS_AS(gameNetworkConsoleCommandsHandler->NetStartAction(splittedCommand), InvalidArgumentException);
		}

		SECTION("Invalid port number throws InvalidArgumentException")
		{
			const auto command = "net_start zombie client";
			const auto splittedCommand = SplitCommand(command);

			REQUIRE_THROWS_AS(gameNetworkConsoleCommandsHandler->NetStartAction(splittedCommand), InvalidArgumentException);
		}

		SECTION("Net type different from 'client' or 'server' throws InvalidArgumentException")
		{
			const auto command = "net_start 60000 invalidNetType";
			const auto splittedCommand = SplitCommand(command);

			REQUIRE_THROWS_AS(gameNetworkConsoleCommandsHandler->NetStartAction(splittedCommand), InvalidArgumentException);
		}

		SECTION("Starting the network as client")
		{
			const auto command = "net_start 60000 client";
			const auto splittedCommand = SplitCommand(command);

			gameNetworkConsoleCommandsHandler->NetStartAction(splittedCommand);

			REQUIRE(testFunctionsData.isStarted);
			REQUIRE(testFunctionsData.isClient);
		}

		SECTION("Starting the network as server")
		{
			const auto command = "net_start 60000 server";
			const auto splittedCommand = SplitCommand(command);

			gameNetworkConsoleCommandsHandler->NetStartAction(splittedCommand);

			REQUIRE(testFunctionsData.isStarted);
			REQUIRE_FALSE(testFunctionsData.isClient);
		}

		SECTION("Starting the network twice throws LogicErrorException")
		{
			const auto command = "net_start 60000 server";
			const auto splittedCommand = SplitCommand(command);

			gameNetworkConsoleCommandsHandler->NetStartAction(splittedCommand);
			REQUIRE_THROWS_AS(gameNetworkConsoleCommandsHandler->NetStartAction(splittedCommand), LogicErrorException);
		}
	}



	TEST_CASE("game_start command tests", "[GameNetworkConsoleCommandsHandler]")
	{
		TestFunctionsData testFunctionsData;
		const auto gameNetworkConsoleCommandsHandler = InstantiateGameNetworkConsoleCommandsHandler(testFunctionsData);

		SECTION("Wrong number of arguments throws InvalidArgumentException")
		{
			const auto command = "game_start foo";
			const auto splittedCommand = SplitCommand(command);

			REQUIRE_THROWS_AS(gameNetworkConsoleCommandsHandler->GameStartAction(splittedCommand), InvalidArgumentException);
		}

		SECTION("Executes the StartGameplay action")
		{
			const auto command = "game_start";
			const auto splittedCommand = SplitCommand(command);

			gameNetworkConsoleCommandsHandler->GameStartAction(splittedCommand);

			REQUIRE(testFunctionsData.hasGameStarted);
		}

		SECTION("Trying to start a game with another already started throws LogicErrorException")
		{
			const auto command = "game_start";
			const auto splittedCommand = SplitCommand(command);

			testFunctionsData.hasGameStarted = true;
			REQUIRE_THROWS_AS(gameNetworkConsoleCommandsHandler->GameStartAction(splittedCommand), LogicErrorException);
		}
	}



	TEST_CASE("s_start_client command tests", "[GameNetworkConsoleCommandsHandler]")
	{
		TestFunctionsData testFunctionsData;
		const auto gameNetworkConsoleCommandsHandler = InstantiateGameNetworkConsoleCommandsHandler(testFunctionsData);

		SECTION("Wrong number of arguments throws InvalidArgumentException")
		{
			const auto command = "s_start_client foo";
			const auto splittedCommand = SplitCommand(command);

			REQUIRE_THROWS_AS(gameNetworkConsoleCommandsHandler->StartClientAction(splittedCommand), InvalidArgumentException);
		}

		SECTION("Command starts the network")
		{
			const auto command = "s_start_client";
			const auto splittedCommand = SplitCommand(command);

			gameNetworkConsoleCommandsHandler->StartClientAction(splittedCommand);

			REQUIRE(testFunctionsData.isStarted);
			REQUIRE(testFunctionsData.isClient);
		}
	}



	TEST_CASE("s_start_server command tests", "[GameNetworkConsoleCommandsHandler]")
	{
		TestFunctionsData testFunctionsData;
		const auto gameNetworkConsoleCommandsHandler = InstantiateGameNetworkConsoleCommandsHandler(testFunctionsData);

		SECTION("Wrong number of arguments throws InvalidArgumentException")
		{
			const auto command = "s_start_server foo";
			const auto splittedCommand = SplitCommand(command);

			REQUIRE_THROWS_AS(gameNetworkConsoleCommandsHandler->StartServerAction(splittedCommand), InvalidArgumentException);
		}

		SECTION("Command will start the network, then the game, and then connect to it as a client in a local server")
		{
			const auto command = "s_start_server";
			const auto splittedCommand = SplitCommand(command);

			gameNetworkConsoleCommandsHandler->StartServerAction(splittedCommand);

			REQUIRE_FALSE(testFunctionsData.isClient);
			REQUIRE(testFunctionsData.isStarted);
			REQUIRE(testFunctionsData.hasGameStarted);
		}
	}



	TEST_CASE("connect command tests", "[GameNetworkConsoleCommandsHandler]")
	{
		TestFunctionsData testFunctionsData;
		const auto gameNetworkConsoleCommandsHandler = InstantiateGameNetworkConsoleCommandsHandler(testFunctionsData);

		SECTION("More arguments than expected throws InvalidArgumentException")
		{
			const auto command = "connect 127.0.0.1:60000 foo";
			const auto splittedCommand = SplitCommand(command);

			REQUIRE_THROWS_AS(gameNetworkConsoleCommandsHandler->ConnectAction(splittedCommand), InvalidArgumentException);
		}

		SECTION("Less arguments than expected throws InvalidArgumentException")
		{
			const auto command = "connect";
			const auto splittedCommand = SplitCommand(command);

			REQUIRE_THROWS_AS(gameNetworkConsoleCommandsHandler->ConnectAction(splittedCommand), InvalidArgumentException);
		}

		SECTION("Will call the asynchronous connection action with the given ip and port")
		{
			const auto command = "connect 127.0.0.1:60000";
			const auto splittedCommand = SplitCommand(command);

			gameNetworkConsoleCommandsHandler->ConnectAction(splittedCommand);

			REQUIRE(testFunctionsData.hasBeenConnectedAsynchronously);
			REQUIRE(testFunctionsData.connectedIp == "127.0.0.1");
			REQUIRE(testFunctionsData.connectedPort == 60000);
		}
	}

	TEST_CASE("s_connect command tests", "[GameNetworkConsoleCommandsHandler]")
	{
		TestFunctionsData testFunctionsData;
		const auto gameNetworkConsoleCommandsHandler = InstantiateGameNetworkConsoleCommandsHandler(testFunctionsData);

		SECTION("If command receives arguments, throw InvalidArgumentException")
		{
			const auto command = "s_connect foo";
			const auto splittedCommand = SplitCommand(command);

			REQUIRE_THROWS_AS(gameNetworkConsoleCommandsHandler->DefaultLoopbackConnectAction(splittedCommand), InvalidArgumentException);
		}

		SECTION("Will call the asynchronous connection action with the default ip and port")
		{
			const auto command = "s_connect";
			const auto splittedCommand = SplitCommand(command);

			gameNetworkConsoleCommandsHandler->DefaultLoopbackConnectAction(splittedCommand);

			REQUIRE(testFunctionsData.hasBeenConnectedAsynchronously);
			REQUIRE(testFunctionsData.connectedIp == "127.0.0.1");
			REQUIRE(testFunctionsData.connectedPort == 60000);
		}
	}
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<Nest/Vector2.h>
using namespace Nest;

#include<Game/Simulation/ClientCommands/SetActorPositionCommand.h>
#include<Game/Network/SunaNetworkReadSerializer.h>
#include<Game/Network/SunaNetworkWriteSerializer.h>
using namespace Game;



namespace SetActorPositionCommandTests
{
	TEST_CASE("SetActorPositionCommand::Serialize", "[SetActorPositionCommand]")
	{
		Vector2<simfix64> position{ simfix64{1.0f}, simfix64{2.0f} };
		ActorNetworkId actorNetworkId{ 200 };

		SetActorPositionCommand setActorPositionCommand;
		setActorPositionCommand.SetActorNetworkId(actorNetworkId);
		setActorPositionCommand.SetPosition(position);

		unsigned char buffer[100];
		SunaNetworkWriteSerializer writer{ buffer, 0, 100 };
		REQUIRE(setActorPositionCommand.Serialize(writer));

		SunaNetworkReadSerializer reader{ buffer,0,100 };
		SetActorPositionCommand recoveredSetActorPositionCommand;
		REQUIRE(recoveredSetActorPositionCommand.Serialize(reader));

		REQUIRE(recoveredSetActorPositionCommand.GetActorNetworkId() == actorNetworkId);
		REQUIRE(recoveredSetActorPositionCommand.GetPosition() == position);
	}
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<memory>
#include<vector>

#include<Nest/ArrayStoringFactory.h>
#include<Nest/StoringFactoryComponentsContainer.h>
using namespace Nest;

#include<Game/SimulationCommons/ActorsManager.h>
#include<Game/SimulationCommons/SimulationActorMovementSystem.h>
#include"Game/Simulation/ISimActorsSpawner.h"
#include<Game/Map/Map.h>
#include<Game/Pathfinding/AStar.h>

#include<Game/Simulation/ServerGameSimulation.h>
#include<Game/Simulation/Types/PlayerSimInfoId.h>
#include<Game/Simulation/PlayerSimulationInformation.h>

#include<Game/Simulation/SimActorMovementBehavior.h>
using namespace Game;

#include"Mocks/PlayerSimulationRangeManagerMock.h"



using namespace Game;

namespace ServerGameSimulationTests
{
	SCENARIO("Player information on server simulation Tests", "[Registering Management]")
	{
		GIVEN("I have a game simulation component set up")
		{
			ArrayStoringFactory<SimActorMovementBehavior> simActorMovementBehaviorsFactory;
			StoringFactoryComponentsContainer<SimActorMovementBehavior> simActorMovementBehaviorsContainer{ &simActorMovementBehaviorsFactory };
			ServerGameSimulation serverSimulation{ nullptr, nullptr, nullptr, nullptr, simActorMovementBehaviorsContainer, nullptr };


			WHEN("I try to register a new player information")
			{
				PlayerSimInfoId id{ 1 };
				auto playerSimulationRangeManager = std::make_unique<PlayerSimulationRangeManagerMock>();
				const auto playerInfo = std::make_shared<PlayerSimulationInformation>(id, std::move(playerSimulationRangeManager));

				THEN("The player information is registered in my simulation component")
				{
					serverSimulation.RegisterPlayer(playerInfo);
					auto retrievedPlayerInfo = serverSimulation.GetPlayerInformation(id).lock();

					REQUIRE(retrievedPlayerInfo != nullptr);
					REQUIRE(retrievedPlayerInfo->GetId() == id);
				}
			}



			WHEN("I try to register a new player information with an already registered id")
			{
				PlayerSimInfoId id{ 1 };
				auto playerSimulationRangeManager = std::make_unique<PlayerSimulationRangeManagerMock>();
				const auto playerInfo = std::make_shared<PlayerSimulationInformation>(id, std::move(playerSimulationRangeManager));

				serverSimulation.RegisterPlayer(playerInfo);
				auto insert2 = serverSimulation.RegisterPlayer(playerInfo);

				THEN("The player information won't be registered")
				{
					REQUIRE(insert2 == false);
				}
			}



			WHEN("I try to remove an existing player information from the simulation")
			{
				PlayerSimInfoId id{ 1 };
				auto playerSimulationRangeManager = std::make_unique<PlayerSimulationRangeManagerMock>();
				const auto playerInfo = std::make_shared<PlayerSimulationInformation>(id, std::move(playerSimulationRangeManager));

				const auto inserted = serverSimulation.RegisterPlayer(playerInfo);
				serverSimulation.UnregisterPlayer(id);

				THEN("The requested player information is removed.")
				{
					const auto retrieved = serverSimulation.GetPlayerInformation(id);
					REQUIRE(inserted);
					REQUIRE(retrieved.expired());
				}
			}



			WHEN("I request the list of the player ids registered in the simulation")
			{
				PlayerSimInfoId id{ 1 };
				auto playerSimulationRangeManager1 = std::make_unique<PlayerSimulationRangeManagerMock>();
				const auto playerInfo = std::make_shared<PlayerSimulationInformation>(id, std::move(playerSimulationRangeManager1));

				PlayerSimInfoId id2{ 10 };
				auto playerSimulationRangeManager2 = std::make_unique<PlayerSimulationRangeManagerMock>();
				const auto playerInfo2 = std::make_shared<PlayerSimulationInformation>(id2, std::move(playerSimulationRangeManager2));

				serverSimulation.RegisterPlayer(playerInfo);
				serverSimulation.RegisterPlayer(playerInfo2);
				auto registeredIds = serverSimulation.GetListRegisteredPlayersIds();

				THEN("The simulation returns to me a list of all currently registered player ids")
				{
					REQUIRE(registeredIds.size() == 2);

					const auto both =
						(registeredIds[0] == PlayerSimInfoId{ 1 } && registeredIds[1] == PlayerSimInfoId{ 10 }) ||
						(registeredIds[1] == PlayerSimInfoId{ 1 } && registeredIds[0] == PlayerSimInfoId{ 10 });

					REQUIRE(both);
				}
			}
		}
	}



	SCENARIO("Player information and join game replies tests", "[Check Join Game Reply Queuing]")
	{
		ArrayStoringFactory<SimActorMovementBehavior> simActorMovementBehaviorsFactory;
		StoringFactoryComponentsContainer<SimActorMovementBehavior> simActorMovementBehaviorsContainer{ &simActorMovementBehaviorsFactory };
		ServerGameSimulation serverSimulation{ nullptr, nullptr, nullptr, nullptr, simActorMovementBehaviorsContainer, nullptr };

		GIVEN("I have no join game replies to dispatch")
		{

			WHEN("I check if there are join game replies to dispatch")
			{
				bool messagesToDispatch = serverSimulation.HasJoinGameRepliesToDispatch();

				THEN("The simulation component will return true")
				{
					REQUIRE(!messagesToDispatch);
				}
			}
		}



		GIVEN("I have registered a new player information, but not dispatched an join game reply")
		{
			PlayerSimInfoId id{ 1 };
			auto playerSimulationRangeManager = std::make_unique<PlayerSimulationRangeManagerMock>();
			const auto playerInfo = std::make_shared<PlayerSimulationInformation>(id, std::move(playerSimulationRangeManager));
			serverSimulation.RegisterPlayer(playerInfo);

			WHEN("I check if there are join game replies to dispatch")
			{
				const auto messagesToDispatch = serverSimulation.HasJoinGameRepliesToDispatch();

				THEN("The simulation component will return true")
				{
					REQUIRE(messagesToDispatch);
				}
			}
		}



		GIVEN("I have registered a new player information")
		{
			PlayerSimInfoId id1{ 1 };
			PlayerSimInfoId id2{ 2 };

			auto playerSimulationRangeManager1 = std::make_unique<PlayerSimulationRangeManagerMock>();
			auto playerSimulationRangeManager2 = std::make_unique<PlayerSimulationRangeManagerMock>();

			const auto playerInfo1 = std::make_shared<PlayerSimulationInformation>(id1, std::move(playerSimulationRangeManager1));
			const auto playerInfo2 = std::make_shared<PlayerSimulationInformation>(id2, std::move(playerSimulationRangeManager2));

			serverSimulation.RegisterPlayer(playerInfo1);
			serverSimulation.RegisterPlayer(playerInfo2);


			WHEN("I try to dispatch the queued up join game replies with a custom function")
			{
				auto id1Called = false;
				auto id2Called = false;

				serverSimulation.DispatchJoinGameReplies([&id1Called, &id2Called, id1, id2](std::weak_ptr<PlayerSimulationInformation> infoWPtr) {
					auto info = infoWPtr.lock();
					if (info != nullptr)
					{
						if (info->GetId() == id1) id1Called = true;
						if (info->GetId() == id2) id2Called = true;
					}
				});

				THEN("My custom function will be executed once for each join game reply")
				{
					REQUIRE(id1Called);
					REQUIRE(id2Called);
				}
			}
		}
	}
}

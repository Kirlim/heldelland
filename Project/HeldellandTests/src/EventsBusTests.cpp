
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<Nest/Events/EventsBus.h>

using namespace Nest;



struct TestEvent
{
	int value;
};



unsigned int lastValue;
void EventFunction(TestEvent& ev)
{
	lastValue = ev.value;
}



namespace EventsBusTests
{
	SCENARIO("Using Events Bus classes", "[EventsBus]")
	{
		auto eventsBus = std::make_unique<EventsBus<TestEvent&>>();



		GIVEN("That I have a new EventsBus instance")
		{
			WHEN("I register a new event handler")
			{
				THEN("The EventsBus will register the handler, and return me the ownership of a token")
				{
					auto token = eventsBus->Register(EventFunction);
					REQUIRE(token != nullptr);
				}

				THEN("The returned token will return true when queried whether it is valid")
				{
					auto token = eventsBus->Register(EventFunction);
					REQUIRE(token->IsValid());
				}
			}

			WHEN("I register two new event handlers")
			{
				auto token1 = eventsBus->Register(EventFunction);
				auto token2 = eventsBus->Register(EventFunction);

				THEN("Both operations will return tokens with distinct id's")
				{
					CHECK(token1->IsValid());
					CHECK(token2->IsValid());
					REQUIRE(token1->GetId() != token2->GetId());
				}
			}
		}

		GIVEN("That a have an EventsBus instance with registered handlers")
		{
			auto token1 = eventsBus->Register(EventFunction);
			auto token2 = eventsBus->Register(EventFunction);

			auto token1Id = token1->GetId();
			auto token2Id = token2->GetId();

			WHEN("I destroy one event bus token")
			{
				token1.reset();

				THEN("The handler represented by that token will be unregistered from the bus")
				{
					REQUIRE(eventsBus->IsHandlerRegistered(token2Id));
					REQUIRE_FALSE(eventsBus->IsHandlerRegistered(token1Id));
				}
			}

			WHEN("I Signal an event to the bus")
			{
				TestEvent ev;
				ev.value = 124;

				eventsBus->Signal(ev);

				THEN("All registered handles will be called with that event as parameter")
				{
					REQUIRE(lastValue == 124);
				}
			}

			WHEN("I destroy the event bus")
			{
				eventsBus.reset();

				THEN("The tokens will return false when queried whether they are valid.")
				{
					REQUIRE(token1->IsValid() == false);
					CHECK(token2->IsValid() == false);
				}
			}
		}
	}
}

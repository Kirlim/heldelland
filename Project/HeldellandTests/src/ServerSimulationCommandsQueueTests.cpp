
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<memory>

#include"Mocks/ServerSimulationCommandMock.h"

#include<Game/Simulation/ServerSimulationCommand.h>
#include<Game/Simulation/ServerSimulationCommandsQueue.h>

using namespace Game;



namespace ServerSimulationCommandsQueueTests
{
	TEST_CASE("ServerSimulationCommandsQueue::Enqueue", "[ServerSimulationCommandsQueue]")
	{
		ServerSimulationCommandsQueue serverSimulationCommandsQueue;

		SECTION("Enqueueing valid command")
		{
			const auto command = std::make_shared<ServerSimulationCommandMock>("mock");

			REQUIRE(serverSimulationCommandsQueue.Size() == 0);
			serverSimulationCommandsQueue.Enqueue(command);
			REQUIRE(serverSimulationCommandsQueue.Size() == 1);
		}

		SECTION("Error when enqueueing null command")
		{
			const std::shared_ptr<ServerSimulationCommand> command = nullptr;

			REQUIRE_THROWS(serverSimulationCommandsQueue.Enqueue(command));
			REQUIRE(serverSimulationCommandsQueue.Size() == 0);
		}
	}



	TEST_CASE("ServerSimulationCommandsQueue::HasCommands", "[ServerSimulationCommandsQueue]")
	{
		ServerSimulationCommandsQueue serverSimulationCommandsQueue;
		const auto command = std::make_shared<ServerSimulationCommandMock>("mock");

		REQUIRE_FALSE(serverSimulationCommandsQueue.HasCommands());
		serverSimulationCommandsQueue.Enqueue(command);
		REQUIRE(serverSimulationCommandsQueue.HasCommands());
	}



	TEST_CASE("ServerSimulationCommandsQueue::PeekNext", "[ServerSimulationCommandsQueue]")
	{
		ServerSimulationCommandsQueue serverSimulationCommandsQueue;

		SECTION("Peeking an empty queue returns null")
		{
			REQUIRE(serverSimulationCommandsQueue.PeekNext() == nullptr);
		}

		SECTION("Peeking returns the front of the queue")
		{
			const auto command = std::make_shared<ServerSimulationCommandMock>("mock");
			serverSimulationCommandsQueue.Enqueue(command);

			auto peek1 = serverSimulationCommandsQueue.PeekNext();
			REQUIRE(peek1 != nullptr);
			REQUIRE(peek1.get() == command.get());

			auto peek2 = serverSimulationCommandsQueue.PeekNext();
			REQUIRE(peek2 != nullptr);
			REQUIRE(peek1.get() == peek2.get());
		}
	}



	TEST_CASE("ServerSimulationCommandsQueue::DequeueFront", "[ServerSimulationCommandsQueue]")
	{
		ServerSimulationCommandsQueue serverSimulationCommandsQueue;

		SECTION("Dequeueing two commands")
		{
			const auto command1 = std::make_shared<ServerSimulationCommandMock>("mock1");
			serverSimulationCommandsQueue.Enqueue(command1);

			const auto command2 = std::make_shared<ServerSimulationCommandMock>("mock2");
			serverSimulationCommandsQueue.Enqueue(command2);

			auto peek1 = serverSimulationCommandsQueue.PeekNext();
			REQUIRE((peek1 != nullptr && peek1.get() == command1.get()));

			serverSimulationCommandsQueue.DequeueFront();
			auto peek2 = serverSimulationCommandsQueue.PeekNext();
			REQUIRE((peek2 != nullptr && peek2.get() == command2.get()));
		}

		SECTION("Dequeuing empty queue results in exception")
		{
			const auto command1 = std::make_shared<ServerSimulationCommandMock>("mock1");
			serverSimulationCommandsQueue.Enqueue(command1);

			REQUIRE_NOTHROW(serverSimulationCommandsQueue.DequeueFront());
			REQUIRE_THROWS(serverSimulationCommandsQueue.DequeueFront());
		}
	}
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<Nest/Vector2.h>
#include<Nest/FixedPoint.h>

#include<Game/Simulation/ClientCommands/SpawnActorCommand.h>
#include<Game/Network/SunaNetworkReadSerializer.h>
#include<Game/Network/SunaNetworkWriteSerializer.h>

using namespace Nest;
using namespace Game;



namespace SpawnActorCommandTests
{
	TEST_CASE("SpawnActorCommand serializes and deserializes properly.", "[SpawnActorCommand]")
	{
		SpawnActorCommand command;
		command.SetActorTypeId(ActorTypeId{ 420 });
		command.SetSpawnPosition(Vector2<simfix64>(simfix64(200000), simfix64(400000)));
		command.SetNetworkId(ActorNetworkId{ 123 });

		unsigned char buffer[100];
		SunaNetworkWriteSerializer writer{ buffer, 0, 100 };
		REQUIRE(command.Serialize(writer));

		SunaNetworkReadSerializer reader(buffer, 0, 100);
		SpawnActorCommand recoveredCommand;
		REQUIRE(recoveredCommand.Serialize(reader));

		REQUIRE(command.GetActorTypeId() == recoveredCommand.GetActorTypeId());
		REQUIRE(command.GetSpawnPosition() == recoveredCommand.GetSpawnPosition());
		REQUIRE(command.GetNetworkId() == recoveredCommand.GetNetworkId());
	}
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<memory>

#include<Nest/World.h>

#include<Game/Simulation/ClientGameSimulation.h>
#include<Game/Simulation/PlayerSimulationCommandsQueuesManager.h>

#include<Game/Simulation/ClientCommands/ForceNextTickValueCommand.h>
#include<Game/Simulation/ClientCommands/SimulationTickStartCommand.h>
#include<Game/Simulation/ClientCommands/SimulationTickEndCommand.h>

#include"Helpers/GameSimulationSetupHelper.h"
#include"Mocks/ClientSimulationCommandMock.h"

using namespace Game;



namespace GameSimulationTests
{
	TEST_CASE("Player commands queue can start with a force next tick command.", "[ClientGameSimulation]")
	{
		GameSimulationSetupHelper gameSimulationSetupHelper;
		auto clientSimulation = gameSimulationSetupHelper.InstantiateGameClientSimulation();
		auto queuesManager = clientSimulation->GetQueuesManager();

		SECTION("Starts with a force tick value command")
		{
			auto forceNextTickValueCommand = std::make_shared<ForceNextTickValueCommand>();
			forceNextTickValueCommand->SetNextTickValue(100);
			queuesManager->AppendCommand(forceNextTickValueCommand);

			queuesManager->CommitQueue();

			clientSimulation->Update(0.11f);

			REQUIRE(clientSimulation->GetCurrentTickValue() == 99);
		}

		SECTION("Starts with a force tick command value, but its not the only command, causes an error.")
		{
			auto forceNextTickValueCommand = std::make_shared<ForceNextTickValueCommand>();
			forceNextTickValueCommand->SetNextTickValue(100);
			queuesManager->AppendCommand(forceNextTickValueCommand);

			const auto simulationCommandMock = std::make_shared<ClientSimulationCommandMock>("mock");
			queuesManager->AppendCommand(simulationCommandMock);

			queuesManager->CommitQueue();

			REQUIRE_THROWS(clientSimulation->Update(0.11f));
		}
	}



	TEST_CASE("Player commands queue can start with a tick start command, and then end with a tick end command.", "[ClientGameSimulation]")
	{
		GameSimulationSetupHelper gameSimulationSetupHelper;
		auto clientSimulation = gameSimulationSetupHelper.InstantiateGameClientSimulation();
		auto queuesManager = clientSimulation->GetQueuesManager();

		// Set up the initial tick
		auto forceNextTickValueCommand = std::make_shared<ForceNextTickValueCommand>();
		forceNextTickValueCommand->SetNextTickValue(100);
		queuesManager->AppendCommand(forceNextTickValueCommand);

		queuesManager->CommitQueue();
		clientSimulation->Update(0.11f);

		// Add simulation tick start command
		auto simulationTickStartCommand = std::make_shared<SimulationTickStartCommand>();
		simulationTickStartCommand->SetTickValue(100);
		queuesManager->AppendCommand(simulationTickStartCommand);

		// And then add random command
		const auto simulationCommandMock = std::make_shared<ClientSimulationCommandMock>("mock");
		queuesManager->AppendCommand(simulationCommandMock);

		SECTION("Starts with a tick start and ends with a tick end command is correct.")
		{
			auto simulationTickEndCommand = std::make_shared<SimulationTickEndCommand>();
			simulationTickEndCommand->SetTickValue(100);
			queuesManager->AppendCommand(simulationTickEndCommand);

			queuesManager->CommitQueue();

			REQUIRE_NOTHROW(clientSimulation->Update(0.11f));
		}

		SECTION("If the queue doesn't end with a simulation tick end command, an exception is thrown.")
		{
			queuesManager->CommitQueue();
			REQUIRE_THROWS(clientSimulation->Update(0.11f));
		}
	}



	TEST_CASE("If the queue does not start with either an force tick command, or tick start command, and exception is thrown.", "[ClientGameSimulation]")
	{
		GameSimulationSetupHelper gameSimulationSetupHelper;
		auto clientSimulation = gameSimulationSetupHelper.InstantiateGameClientSimulation();
		auto queuesManager = clientSimulation->GetQueuesManager();

		const auto simulationCommandMock = std::make_shared<ClientSimulationCommandMock>("mock");
		queuesManager->AppendCommand(simulationCommandMock);

		auto simulationTickEndCommand = std::make_shared<SimulationTickEndCommand>();
		simulationTickEndCommand->SetTickValue(100);
		queuesManager->AppendCommand(simulationTickEndCommand);

		queuesManager->CommitQueue();

		REQUIRE_THROWS(clientSimulation->Update(0.11f));
	}



	TEST_CASE("Simulation Tick Start Command and Simulation Tick End Command in the same queue must hold the same tick values.", "[ClientGameSimulation]")
	{
		GameSimulationSetupHelper gameSimulationSetupHelper;
		auto clientSimulation = gameSimulationSetupHelper.InstantiateGameClientSimulation();
		auto queuesManager = clientSimulation->GetQueuesManager();

		// Set up the initial tick
		auto forceNextTickValueCommand = std::make_shared<ForceNextTickValueCommand>();
		forceNextTickValueCommand->SetNextTickValue(100);
		queuesManager->AppendCommand(forceNextTickValueCommand);

		queuesManager->CommitQueue();
		clientSimulation->Update(0.11f);

		// Add simulation tick start command
		auto simulationTickStartCommand = std::make_shared<SimulationTickStartCommand>();
		simulationTickStartCommand->SetTickValue(100);
		queuesManager->AppendCommand(simulationTickStartCommand);

		// And then add random command
		const auto simulationCommandMock = std::make_shared<ClientSimulationCommandMock>("mock");
		queuesManager->AppendCommand(simulationCommandMock);

		SECTION("If both tick values are the same, there are no errors.")
		{
			auto simulationTickEndCommand = std::make_shared<SimulationTickEndCommand>();
			simulationTickEndCommand->SetTickValue(100);
			queuesManager->AppendCommand(simulationTickEndCommand);

			queuesManager->CommitQueue();

			REQUIRE_NOTHROW(clientSimulation->Update(0.11f));
		}

		SECTION("An exception will be thrown if the tick values are not the same.")
		{
			auto simulationTickEndCommand = std::make_shared<SimulationTickEndCommand>();
			simulationTickEndCommand->SetTickValue(101);
			queuesManager->AppendCommand(simulationTickEndCommand);

			queuesManager->CommitQueue();

			REQUIRE_THROWS(clientSimulation->Update(0.11f));
		}
	}



	TEST_CASE("If the tick start value command is different than the next tick being simulated, an exception is thrown.", "[ClientGameSimulation]")
	{
		GameSimulationSetupHelper gameSimulationSetupHelper;
		auto clientSimulation = gameSimulationSetupHelper.InstantiateGameClientSimulation();
		auto queuesManager = clientSimulation->GetQueuesManager();

		// Set up the initial tick
		auto forceNextTickValueCommand = std::make_shared<ForceNextTickValueCommand>();
		forceNextTickValueCommand->SetNextTickValue(100);
		queuesManager->AppendCommand(forceNextTickValueCommand);

		queuesManager->CommitQueue();
		clientSimulation->Update(0.11f);

		// Add simulation tick start command
		auto simulationTickStartCommand = std::make_shared<SimulationTickStartCommand>();
		simulationTickStartCommand->SetTickValue(200);
		queuesManager->AppendCommand(simulationTickStartCommand);

		// And then add random command
		const auto simulationCommandMock = std::make_shared<ClientSimulationCommandMock>("mock");
		queuesManager->AppendCommand(simulationCommandMock);

		// Add the tick end command
		auto simulationTickEndCommand = std::make_shared<SimulationTickEndCommand>();
		simulationTickEndCommand->SetTickValue(200);
		queuesManager->AppendCommand(simulationTickEndCommand);

		queuesManager->CommitQueue();

		REQUIRE_THROWS(clientSimulation->Update(0.11f));
	}



	TEST_CASE("When a tick ends, the next tick counter is incremented", "[ClientGameSimulation]")
	{
		GameSimulationSetupHelper gameSimulationSetupHelper;
		auto clientSimulation = gameSimulationSetupHelper.InstantiateGameClientSimulation();
		auto queuesManager = clientSimulation->GetQueuesManager();

		// Set up the initial tick
		auto forceNextTickValueCommand = std::make_shared<ForceNextTickValueCommand>();
		forceNextTickValueCommand->SetNextTickValue(100);
		queuesManager->AppendCommand(forceNextTickValueCommand);

		queuesManager->CommitQueue();
		clientSimulation->Update(0.11f);

		for (int i = 100; i <= 110; ++i)
		{
			REQUIRE(clientSimulation->GetCurrentTickValue() == i - 1);

			// Add simulation tick start command
			auto simulationTickStartCommand = std::make_shared<SimulationTickStartCommand>();
			simulationTickStartCommand->SetTickValue(i);
			queuesManager->AppendCommand(simulationTickStartCommand);

			// And then add random command
			const auto simulationCommandMock = std::make_shared<ClientSimulationCommandMock>("mock");
			queuesManager->AppendCommand(simulationCommandMock);

			// Add the tick end command
			auto simulationTickEndCommand = std::make_shared<SimulationTickEndCommand>();
			simulationTickEndCommand->SetTickValue(i);
			queuesManager->AppendCommand(simulationTickEndCommand);

			queuesManager->CommitQueue();

			REQUIRE_NOTHROW(clientSimulation->Update(0.11f));
		}
	}
}

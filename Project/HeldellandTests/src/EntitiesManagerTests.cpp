
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<Nest/IEntity.h>
#include<Nest/EntitiesManager.h>
#include<Nest/ComponentsFactory.h>
#include<Nest/World.h>
using namespace Nest;

#include<Game/WorldFactory.h>
using namespace Game;



namespace EntitiesManagerTests
{
	TEST_CASE("EntitiesManager::EntitiesManager", "[EntitiesManager]")
	{
		WorldFactory worldFactory;
		const auto world = worldFactory.InstantiateWorld();

		SECTION("Constructor sets up the variables correctly")
		{
			const EntitiesManager entitiesManager{ *world };

			REQUIRE(entitiesManager.GetComponentsFactory() == world->GetComponentsFactory());
			REQUIRE(entitiesManager.GetResourcesManager() == world->GetResourcesManager());
		}
	}



	TEST_CASE("EntitiesManager::Instantiate", "[EntitiesManager]")
	{
		WorldFactory worldFactory;
		const auto world = worldFactory.InstantiateWorld();

		EntitiesManager entitiesManager{ *world };

		SECTION("Instantiating a new entity")
		{
			const auto entityWPtr = entitiesManager.Instantiate();
			REQUIRE(!entityWPtr.expired());
		}

		SECTION("Instantiating new entities results in entities with different ids")
		{
			auto entity1 = entitiesManager.Instantiate().lock();
			auto entity2 = entitiesManager.Instantiate().lock();

			REQUIRE((entity1 != nullptr && entity2 != nullptr));
			REQUIRE(entity1->Id() != entity2->Id());
		}
	}



	TEST_CASE("EntitiesManager::GetById", "[EntitiesManager]")
	{
		WorldFactory worldFactory;
		const auto world = worldFactory.InstantiateWorld();

		EntitiesManager entitiesManager{ *world };

		auto entity1 = entitiesManager.Instantiate().lock();
		auto entity2 = entitiesManager.Instantiate().lock();

		REQUIRE(entity1.get() == entitiesManager.GetById(entity1->Id()).lock().get());
		REQUIRE(entity2.get() == entitiesManager.GetById(entity2->Id()).lock().get());
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<Nest/World.h>
#include<Game/WorldFactory.h>
using namespace Nest;

#include<Game/SimulationCommons/SimulationTypes.h>
#include<Game/Simulation/ClientGameSimulation.h>
#include<Game/SimulationCommons/SimulationActorMovementSystem.h>

// TODO : figure out why all those includes are needed
#include<Game/Simulation/ISimActorsSpawner.h>
#include<Game/SimulationCommons/ActorsManager.h>
#include<Game/View/IViewActorsSpawner.h>
#include<Game/Map/Map.h>
#include<Game/Pathfinding/AStar.h>
using namespace Game;



namespace ClientGameSimulationTests
{
	TEST_CASE("ClientGameSimulation::ForceNextTickValue", "[ClientGameSimulation]")
	{
		WorldFactory worldFactory{};
		const auto world = worldFactory.InstantiateWorld();

		ClientGameSimulation clientGameSimulation{ *world, nullptr, nullptr, nullptr, nullptr, nullptr, simfix64{ 0.1f }, 0.2f, 1.1f };

		clientGameSimulation.ForceNextTickValue(100);
		REQUIRE(clientGameSimulation.GetCurrentTickValue() == 99);
	}
}

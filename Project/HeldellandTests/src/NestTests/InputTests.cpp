//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<Nest/Exceptions/AssertException.h>
#include<Nest/Exceptions/RuntimeException.h>
#include<Nest/Input.h>
using namespace Nest;



namespace InputTests
{
	TEST_CASE("Handling keyboard presses input", "[Input]")
	{
		Input input{};
		const auto testKey = KeyCode::Space;


		SECTION("When a key is pressed, key can be read as 'on down' and 'pressed'")
		{
			REQUIRE_FALSE(input.IsKeyOnDown(testKey));
			REQUIRE_FALSE(input.IsKeyPressed(testKey));
			REQUIRE_FALSE(input.IsKeyOnUp(testKey));

			input.KeyPressed(testKey);

			REQUIRE(input.IsKeyOnDown(testKey));
			REQUIRE(input.IsKeyPressed(testKey));
			REQUIRE_FALSE(input.IsKeyOnUp(testKey));
		}


		SECTION("When a key is released, key can be read as 'on up', but not as 'pressed'")
		{
			input.KeyPressed(testKey);
			input.KeyReleased(testKey);

			REQUIRE_FALSE(input.IsKeyPressed(testKey));
			REQUIRE(input.IsKeyOnUp(testKey));
		}


		SECTION("When refresh is called, key 'on down' and 'on up' status are cleared")
		{
			input.KeyPressed(testKey);
			input.Refresh();
			REQUIRE_FALSE(input.IsKeyOnDown(testKey));

			input.KeyReleased(testKey);
			input.Refresh();
			REQUIRE_FALSE(input.IsKeyOnUp(testKey));
		}
	}



	TEST_CASE("Handling mouse input", "[Input]")
	{
		Input input{};
		const auto mouseButton = MouseButton::Left;


		SECTION("When a mouse button is pressed, it can be read as 'on down' and 'pressed'")
		{
			REQUIRE_FALSE(input.IsMouseOnDown(mouseButton));
			REQUIRE_FALSE(input.IsMousePressed(mouseButton));
			REQUIRE_FALSE(input.IsMouseOnUp(mouseButton));

			input.MouseButtonPressed(mouseButton);

			REQUIRE(input.IsMouseOnDown(mouseButton));
			REQUIRE(input.IsMousePressed(mouseButton));
			REQUIRE_FALSE(input.IsMouseOnUp(mouseButton));
		}


		SECTION("When a mouse button is released, it can be read as 'on up', but not as 'pressed'")
		{
			input.MouseButtonPressed(mouseButton);
			input.MouseButtonReleased(mouseButton);

			REQUIRE_FALSE(input.IsMousePressed(mouseButton));
			REQUIRE(input.IsMouseOnUp(mouseButton));
		}


		SECTION("When a refresh is called, mousu button 'on down' and 'on up' status are cleared")
		{
			input.MouseButtonPressed(mouseButton);
			input.Refresh();
			REQUIRE_FALSE(input.IsMouseOnDown(mouseButton));

			input.MouseButtonReleased(mouseButton);
			input.Refresh();
			REQUIRE_FALSE(input.IsMouseOnUp(mouseButton));
		}


		SECTION("When a mouse scroll is added, its value can be fetched")
		{
			input.AddMouseScroll(10, 20);

			const auto scroll = input.GetDeltaMouseScrollSinceLastRefresh();
			REQUIRE(scroll.x == 10);
			REQUIRE(scroll.y == 20);
		}


		SECTION("Mouse scroll additions are cumulative")
		{
			input.AddMouseScroll(10, 20);
			input.AddMouseScroll(5, 10);

			const auto scroll = input.GetDeltaMouseScrollSinceLastRefresh();
			REQUIRE(scroll.x == 15);
			REQUIRE(scroll.y == 30);
		}


		SECTION("Accumulated mouse scroll are cleared with the refresh method")
		{
			input.AddMouseScroll(10, 20);
			input.Refresh();

			const auto scroll = input.GetDeltaMouseScrollSinceLastRefresh();
			REQUIRE(scroll.x == 0);
			REQUIRE(scroll.y == 0);
		}
	}



	TEST_CASE("Mouse events dispatching, not assuming listener errors", "[Input]")
	{
		Input input{};

		InputEvent lastEvent;
		lastEvent.type = InputEventType::Undefined;

		auto eventToken = input.GetInputEventsBus().Register([&](InputEvent& evt) { lastEvent = evt; });


		SECTION("When a mouse button is pressed, the mouse button down event is dispatched")
		{
			input.MouseButtonPressed(MouseButton::Left);
			REQUIRE(lastEvent.type == InputEventType::MouseButtonDown);
			REQUIRE(lastEvent.mouseButton == MouseButton::Left);
		}


		SECTION("When a mouse button is released, the mouse button up event is dispatched")
		{
			input.MouseButtonReleased(MouseButton::Left);
			REQUIRE(lastEvent.type == InputEventType::MouseButtonUp);
			REQUIRE(lastEvent.mouseButton == MouseButton::Left);
		}


		SECTION("When the mouse moves, the mouse movement event is dispatched")
		{
			input.SetMousePosition(0, 0);
			input.SetMousePosition(20, 50);

			REQUIRE(lastEvent.type == InputEventType::MouseMovement);
			REQUIRE(lastEvent.deltaX == 20);
			REQUIRE(lastEvent.deltaY == 50);
		}


		SECTION("When a mouse scroll is moved, the Mouse Scroll Event is dispatched")
		{
			input.AddMouseScroll(10, 20);
			REQUIRE(lastEvent.type == InputEventType::MouseScroll);
			REQUIRE(lastEvent.deltaX == 10);
			REQUIRE(lastEvent.deltaY == 20);
		}
	}



	TEST_CASE("Mouse events dispatching nest listener exceptions into a RuntimeException", "[Input]")
	{
		Input input{};
		auto eventToken = input.GetInputEventsBus().Register([&](InputEvent& evt) { throw AssertException{ "Error" }; });

		REQUIRE_THROWS_AS(input.MouseButtonPressed(MouseButton::Left), RuntimeException);
		REQUIRE_THROWS_AS(input.MouseButtonReleased(MouseButton::Left), RuntimeException);
		REQUIRE_THROWS_AS(input.SetMousePosition(0, 0), RuntimeException);
		REQUIRE_THROWS_AS(input.AddMouseScroll(0, 0), RuntimeException);
	}
}

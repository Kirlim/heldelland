
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<Nest/Exceptions/InvalidArgumentException.h>
#include<Nest/ResourcesTypes.h>
using namespace Nest;



namespace ResourcesTypesTests
{
	TEST_CASE("GetResourceTypeFromString", "[ResourcesTypes]")
	{
		SECTION("Getting font works and is case insensitive")
		{
			REQUIRE(GetResourceTypeFromString("Font") == ResourceType::Font);
			REQUIRE(GetResourceTypeFromString("font") == ResourceType::Font);
		}

		SECTION("Getting material works and is case insensitive")
		{
			REQUIRE(GetResourceTypeFromString("Material") == ResourceType::Material);
			REQUIRE(GetResourceTypeFromString("material") == ResourceType::Material);
		}

		SECTION("Getting mesh works and is case insensitive")
		{
			REQUIRE(GetResourceTypeFromString("Mesh") == ResourceType::Mesh);
			REQUIRE(GetResourceTypeFromString("mesh") == ResourceType::Mesh);
		}

		SECTION("Getting shader works and is case insensitive")
		{
			REQUIRE(GetResourceTypeFromString("Shader") == ResourceType::Shader);
			REQUIRE(GetResourceTypeFromString("shader") == ResourceType::Shader);
		}

		SECTION("Getting texture works and is case insensitive")
		{
			REQUIRE(GetResourceTypeFromString("Texture") == ResourceType::Texture);
			REQUIRE(GetResourceTypeFromString("texture") == ResourceType::Texture);
		}

		SECTION("Trying to get from empty string throws InvalidArgumentException")
		{
			REQUIRE_THROWS_AS(GetResourceTypeFromString(""), InvalidArgumentException);
		}

		SECTION("Trying to get from wrong or invalid resource type string throws InvalidArgumentException")
		{
			REQUIRE_THROWS_AS(GetResourceTypeFromString("Coffee"), InvalidArgumentException);
		}
	}
}

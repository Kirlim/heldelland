
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<memory>

#include<Nest/ComponentTypeMap.h>
#include<Nest/Transform.h>
#include<Nest/MeshFilter.h>
#include<Nest/IComponentsFactory.h>
#include<Nest/ArrayStoringFactory.h>
#include<Nest/SimpleStoringFactoryWrap.h>
#include<Nest/IEntity.h>
#include<Nest/EntitiesManager.h>
#include<Nest/World.h>
using namespace Nest;

#include<Game/WorldFactory.h>
using namespace Game;



namespace EntityTests
{
	std::unique_ptr<ComponentTypeMap> componentTypeMap;
	ComponentTypeId transformTypeId = 0;
	ComponentTypeId meshFilterTypeId = 0;



	std::unique_ptr<World> InstantiateWorld()
	{
		WorldFactory worldFactory;
		auto world = worldFactory.InstantiateWorld();


		if (ComponentTypeMap::GetInstance() == nullptr)
		{
			componentTypeMap = std::make_unique<ComponentTypeMap>();
		}
		transformTypeId = componentTypeMap->GetTypeId<Transform>();
		meshFilterTypeId = componentTypeMap->GetTypeId<MeshFilter>();


		auto transformFactory = std::make_unique<ArrayStoringFactory<Transform>>();
		auto transformFactoryWrap = std::make_unique<SimpleStoringFactoryWrap<Transform>>(transformTypeId, Transform::GetTypeName(), std::move(transformFactory));

		auto meshFilterFactory = std::make_unique<ArrayStoringFactory<MeshFilter>>();
		auto meshFilterFactoryWrap = std::make_unique<SimpleStoringFactoryWrap<MeshFilter>>(meshFilterTypeId, MeshFilter::GetTypeName(), std::move(meshFilterFactory));

		auto componentsFactory = world->GetComponentsFactory();

		const auto transformComponentType = transformFactoryWrap->GetTypeId();
		const auto transformComponentName = transformFactoryWrap->GetTypeName();
		componentsFactory->RegisterFactory(transformComponentType, transformComponentName, std::move(transformFactoryWrap));

		const auto mesfFilterComponentType = meshFilterFactoryWrap->GetTypeId();
		const auto meshFilterComponentName = meshFilterFactoryWrap->GetTypeName();
		componentsFactory->RegisterFactory(mesfFilterComponentType, meshFilterComponentName, std::move(meshFilterFactoryWrap));

		return world;
	}



	TEST_CASE("Entity::SetName(const std::string)", "[Entity]")
	{
		const auto world = InstantiateWorld();
		auto entitiesManager = world->GetEntitiesManager();

		auto entity = entitiesManager->Instantiate().lock();

		entity->SetName("TestName");
		REQUIRE(entity->GetName() == "TestName");

		entity->SetName("AnotherTestName");
		REQUIRE(entity->GetName() == "AnotherTestName");
	}



	TEST_CASE("Entity::AddComponent(std::unique_ptr<ComponentOwnerReferenceBase>&)", "[Entity]")
	{
		const auto world = InstantiateWorld();
		auto entitiesManager = world->GetEntitiesManager();
		auto componentsFactory = world->GetComponentsFactory();

		auto entity = entitiesManager->Instantiate().lock();

		SECTION("Adding a valid component reference adds the component to the entity")
		{
			auto component = componentsFactory->Instantiate(transformTypeId);
			REQUIRE(component != nullptr);

			auto notFoundComponent = entity->GetComponent(transformTypeId);
			REQUIRE(notFoundComponent == nullptr);

			entity->AddComponent(component);
			REQUIRE(component == nullptr);

			auto recoveredComponent = entity->GetComponent(transformTypeId);
			REQUIRE(recoveredComponent != nullptr);
		}

		SECTION("Adding a null component reference throw null argument exception")
		{
			std::unique_ptr<ComponentOwnerReferenceBase> componentReference;
			REQUIRE_THROWS_AS(entity->AddComponent(componentReference), NullArgumentException);
		}
	}



	TEST_CASE("Entity::AddComponent(const std::string)", "[Entity]")
	{
		const auto world = InstantiateWorld();
		auto entitiesManager = world->GetEntitiesManager();

		auto entity = entitiesManager->Instantiate().lock();

		SECTION("Adding with a valid component type name adds the component to the entity")
		{
			auto notFoundComponent = entity->GetComponent(Transform::GetTypeName());
			REQUIRE(notFoundComponent == nullptr);

			entity->AddComponent(Transform::GetTypeName());

			auto recoveredComponent = entity->GetComponent(Transform::GetTypeName());
			REQUIRE(recoveredComponent != nullptr);
		}

		SECTION("Adding an unknown component type name throws a exception, and trying to fetch it as well")
		{
			REQUIRE_THROWS(entity->AddComponent("Paradimonsuta"));
			REQUIRE_THROWS(entity->GetComponent("Paradimonsuta"));
		}
	}



	TEST_CASE("Entity::AddComponent<ComponentType>", "[Entity]")
	{
		const auto world = InstantiateWorld();
		auto entitiesManager = world->GetEntitiesManager();

		auto entity = entitiesManager->Instantiate().lock();

		SECTION("Adding and retrieving a component")
		{
			REQUIRE(entity->GetComponent<Transform>() == nullptr);
			entity->AddComponent<Transform>();
			REQUIRE(entity->GetComponent<Transform>() != nullptr);
		}
	}



	TEST_CASE("Entity::GetComponentById<ComponentType>", "[Entity]")
	{
		const auto world = InstantiateWorld();
		auto entitiesManager = world->GetEntitiesManager();

		auto entity = entitiesManager->Instantiate().lock();

		SECTION("Fetching a valid component")
		{
			auto transformComponent = entity->AddComponent<Transform>();
			auto meshFilterComponent = entity->AddComponent<MeshFilter>();

			auto recoveredTransformComponent = entity->GetComponentById<Transform>(transformComponent->GetId());
			auto recoveredMeshFilterComponent = entity->GetComponentById<MeshFilter>(meshFilterComponent->GetId());

			REQUIRE(recoveredMeshFilterComponent == meshFilterComponent);
			REQUIRE(recoveredTransformComponent == transformComponent);
		}



		SECTION("Fetching a component that cannot be found throws LogicErrorException")
		{
			REQUIRE_THROWS_AS(entity->GetComponentById<Transform>(ComponentId{ 1 }), LogicErrorException);
		}



		SECTION("Fetching the wrong tye of compononent throws LogicErrorException")
		{
			const auto transformComponent = entity->AddComponent<Transform>();
			REQUIRE_THROWS_AS(entity->GetComponentById<MeshFilter>(transformComponent->GetId()), LogicErrorException);
		}
	}



	TEST_CASE("Entity::Clone(EntitiesManager&)", "[Entity]")
	{
		const auto world = InstantiateWorld();
		auto entitiesManager = world->GetEntitiesManager();

		auto entity = entitiesManager->Instantiate().lock();

		SECTION("Cloning an empty entity returns an empty entity")
		{
			const auto newEntity = entity->Clone(*entitiesManager);
			REQUIRE_FALSE(newEntity.expired());
			REQUIRE(newEntity.lock()->GetComponentsCount() == 0);
		}

		SECTION("Cloning an entity with a transform")
		{
			auto originalTransform = entity->AddComponent<Transform>();
			originalTransform->SetLocalPosition(Vector3f(1.0f, 2.0f, 3.0f));

			auto clonedEntity = entity->Clone(*entitiesManager).lock();
			REQUIRE(clonedEntity->GetComponentsCount() == 1);

			auto clonedTransform = clonedEntity->GetComponent<Transform>();
			REQUIRE(clonedTransform != nullptr);
			REQUIRE(clonedTransform->GetLocalPosition() == Vector3f(1.0f, 2.0f, 3.0f));
		}

		SECTION("Cloning a hierarchy of two entities with transforms")
		{
			auto originalParentTransform = entity->AddComponent<Transform>();
			originalParentTransform->SetLocalPosition(Vector3f(1.0f, 2.0f, 3.0f));

			auto originalChildEntity = entitiesManager->Instantiate().lock();
			auto originalChildTransform = originalChildEntity->AddComponent<Transform>();
			originalChildTransform->SetParent(originalParentTransform);
			originalChildTransform->SetLocalPosition(Vector3f{ 4.0f, 5.0f, 6.0f });


			auto clonedParentEntity = entity->Clone(*entitiesManager).lock();
			REQUIRE(clonedParentEntity->GetComponentsCount() == 1);

			auto clonedParentTransform = clonedParentEntity->GetComponent<Transform>();
			REQUIRE(clonedParentTransform != nullptr);
			REQUIRE(clonedParentTransform->GetLocalPosition() == Vector3f(1.0f, 2.0f, 3.0f));
			REQUIRE(clonedParentTransform->GetChildCount() == 1);
			REQUIRE(clonedParentTransform->GetOwner() == clonedParentEntity.get());

			auto clonedChildTransform = clonedParentTransform->GetChildAt(0);
			REQUIRE(clonedChildTransform != originalChildTransform);
			REQUIRE(clonedChildTransform->GetChildCount() == 0);
			REQUIRE(clonedChildTransform->GetLocalPosition() == Vector3f(4.0f, 5.0f, 6.0f));

			auto clonedChildParentTransform = clonedChildTransform->GetParent();
			REQUIRE(clonedChildParentTransform != nullptr);
			REQUIRE(clonedChildParentTransform == clonedParentTransform);
		}
	}
}

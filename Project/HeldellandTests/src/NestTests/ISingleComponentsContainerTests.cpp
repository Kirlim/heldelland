
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<Nest/Exceptions/NullArgumentException.h>
#include<Nest/ISingleComponentsContainer.h>
#include<Nest/Events/EventsBusToken.h>
using namespace Nest;



namespace ISingleComponentsContainerTests
{
	class Mock : public ISingleComponentsContainer
	{
	public:
		int DoGetElementsCount() const override
		{
			return 0;
		}

		std::unique_ptr<EventsBusToken> DoRegisterOnComponentAddedListener(std::function<void(const ComponentAddedEvent&)>) override
		{
			doRegisterOnComponentAddedCalled = true;
			return nullptr;
		}

		std::unique_ptr<EventsBusToken> DoRegisterOnComponentDestroyedListener(std::function<void(const ComponentDestroyedEvent&)>) override
		{
			doRegisterOnComponentDestroyedCalled = true;
			return nullptr;
		}



		bool doRegisterOnComponentAddedCalled = false;
		bool doRegisterOnComponentDestroyedCalled = false;
	};



	TEST_CASE("RegisterOnComponentAddedListener", "[ISingleComponentsContainer]")
	{
		Mock mock;

		SECTION("Registering a null listener throws NullArgumentException")
		{
			REQUIRE_THROWS_AS(mock.RegisterOnComponentAddedListener(nullptr), NullArgumentException);
		}

		SECTION("Registering a valid listener will call virtual implementation method")
		{
			REQUIRE_NOTHROW(mock.RegisterOnComponentAddedListener([](const ComponentAddedEvent&) {return;}));
			REQUIRE(mock.doRegisterOnComponentAddedCalled);
		}
	}

	TEST_CASE("RegisterOnComponentDestroyedListener", "[ISingleComponentsContainer]")
	{
		Mock mock;

		SECTION("Registering a null listener throws NullArgumentException")
		{
			REQUIRE_THROWS_AS(mock.RegisterOnComponentDestroyedListener(nullptr), NullArgumentException);
		}

		SECTION("Registering a valid listener will call virtual implementation method")
		{
			REQUIRE_NOTHROW(mock.RegisterOnComponentDestroyedListener([](const ComponentDestroyedEvent&) {return;}));
			REQUIRE(mock.doRegisterOnComponentDestroyedCalled);
		}
	}
}
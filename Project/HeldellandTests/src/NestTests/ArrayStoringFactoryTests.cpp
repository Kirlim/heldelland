//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<Nest/Exceptions/NullArgumentException.h>
#include<Nest/ArrayStoringFactory.h>
using namespace Nest;



namespace ArrayStoringFactoryTests
{
	struct TestDataType
	{
		int value;
	};



	//std::unique_ptr<EventsBusToken<DataType&>> RegisterOnDataAddedListener(std::function<void(DataType&)>) override;
	//std::unique_ptr<EventsBusToken<DataType&>> RegisterOnDataDeletedListener(std::function<void(DataType&)>) override;
	TEST_CASE("RegisterOnDataAddedListener", "[ArrayStoringFactory]")
	{
		ArrayStoringFactory<TestDataType> arrayStoringFactory{};

		SECTION("Registering null function will throw NullArgumentException")
		{
			REQUIRE_THROWS_AS(arrayStoringFactory.RegisterOnDataAddedListener(nullptr), NullArgumentException);
		}

		SECTION("Event is raised when an object is instantiated")
		{
			auto raised = false;
			auto listenerToken = arrayStoringFactory.RegisterOnDataAddedListener([&](TestDataType& data) { raised = true; });

			arrayStoringFactory.Instantiate();
			REQUIRE(raised);
		}
	}

	TEST_CASE("RegisterOnDataDeletedListener", "[ArrayStoringFactory]")
	{
		ArrayStoringFactory<TestDataType> arrayStoringFactory{};

		SECTION("Registering null function will throw NullArgumentException")
		{
			REQUIRE_THROWS_AS(arrayStoringFactory.RegisterOnDataDeletedListener(nullptr), NullArgumentException);
		}

		SECTION("Event is raised when an object is deleted")
		{
			auto raised = false;
			auto listenerToken = arrayStoringFactory.RegisterOnDataDeletedListener([&](TestDataType& data) { raised = true; });

			arrayStoringFactory.Instantiate();
			REQUIRE(raised);
		}
	}
}

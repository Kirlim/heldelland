
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../../../catch.hpp"

#include<yaml-cpp/yaml.h>

#include<Nest/Exceptions/InvalidDataException.h>
#include<Nest/Exceptions/LogicErrorException.h>

#include<Nest/MeshRenderer.h>
#include<Nest/EntitiesAndComponentsRemap.h>
#include<Nest/ResourcesManager.h>
using namespace Nest;

#include<Nest/Eggs/YamlEntitiesLoader/Loaders/YamlMeshRendererComponentLoader.h>
using namespace Nest::Eggs;



namespace Nest_Eggs_Loaders_YamlMeshRendererComponentLoaderTests
{
	YAML::Node LoadTestYamlFile(const std::string fileName)
	{
		return YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/MeshRenderer/" + fileName);
	}



	TEST_CASE("Loading a MeshRenderer from a Yaml File", "[YamlMeshRendererComponentLoader]")
	{
		const EntitiesAndComponentsRemap remapper;
		ResourcesManager resourcesManager{};

		YamlMeshRendererComponentLoader yamlMeshRendererComponentLoader{};
		MeshRenderer meshRenderer{};

		SECTION("Null Mesh Renderer node is valid")
		{
			const auto meshRendererYamlNode = LoadTestYamlFile("NullNode.txt");

			yamlMeshRendererComponentLoader.LoadAttributes(
				meshRenderer, meshRendererYamlNode["MeshRenderer"], remapper, resourcesManager
			);

			REQUIRE(meshRenderer.GetName().size() == 0);
			REQUIRE(meshRenderer.GetMaterial() == nullptr);
			REQUIRE(meshRenderer.IsGui() == false);
		}

		SECTION("Loading a Mesh Renderer with only its name loaded.")
		{
			const auto meshRendererYamlNode = LoadTestYamlFile("NameOnly.txt");

			yamlMeshRendererComponentLoader.LoadAttributes(
				meshRenderer, meshRendererYamlNode["MeshRenderer"], remapper, resourcesManager
			);

			REQUIRE(meshRenderer.GetName() == "Mesh Renderer");
			REQUIRE(meshRenderer.GetMaterial() == nullptr);
			REQUIRE(meshRenderer.IsGui() == false);
		}

		SECTION("Loading a Mesh Renderer with only a material loaded")
		{
			auto material = std::make_unique<Material>();
			auto materialAddress = material.get();
			resourcesManager.AddMaterial("MaterialName", material);

			const auto meshRendererYamlNode = LoadTestYamlFile("MaterialOnly.txt");

			yamlMeshRendererComponentLoader.LoadAttributes(
				meshRenderer, meshRendererYamlNode["MeshRenderer"], remapper, resourcesManager
			);

			REQUIRE(meshRenderer.GetName().size() == 0);
			REQUIRE(meshRenderer.GetMaterial() == materialAddress);
			REQUIRE(meshRenderer.IsGui() == false);
		}

		SECTION("Loading a Mesh Renderer with a material resource type that is not a material throw InvalidDataException")
		{
			const auto meshRendererYamlNode = LoadTestYamlFile("Error_InvalidMaterialResourceType.txt");

			REQUIRE_THROWS_AS(
				yamlMeshRendererComponentLoader.LoadAttributes(
					meshRenderer, meshRendererYamlNode["MeshRenderer"], remapper, resourcesManager
				),
				InvalidDataException
			);
		}

		SECTION("Loading a Mesh Renderer with a not found material will throw LogicErrorException")
		{
			const auto meshRendererYamlNode = LoadTestYamlFile("Error_MaterialNotLoaded.txt");

			REQUIRE_THROWS_AS(
				yamlMeshRendererComponentLoader.LoadAttributes(
					meshRenderer, meshRendererYamlNode["MeshRenderer"], remapper, resourcesManager
				),
				LogicErrorException
			);
		}

		SECTION("Loading a Mesh Renderer with IsGui flag set to true")
		{
			const auto meshRendererYamlNode = LoadTestYamlFile("IsGuiFlagIsTrue.txt");

			yamlMeshRendererComponentLoader.LoadAttributes(
				meshRenderer, meshRendererYamlNode["MeshRenderer"], remapper, resourcesManager
			);

			REQUIRE(meshRenderer.IsGui());
		}

		SECTION("Loading a Mesh Renderer with IsGui flag set to false")
		{
			const auto meshRendererYamlNode = LoadTestYamlFile("IsGuiFlagIsFalse.txt");

			yamlMeshRendererComponentLoader.LoadAttributes(
				meshRenderer, meshRendererYamlNode["MeshRenderer"], remapper, resourcesManager
			);

			REQUIRE(meshRenderer.IsGui() == false);
		}
	}
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../../../catch.hpp"

#include<cmath>
#include<memory>

#include<yaml-cpp/yaml.h>

#include<Nest/Transform.h>
#include<Nest/IEntity.h>
#include<Nest/EntitiesManager.h>
#include<Nest/EntitiesAndComponentsRemap.h>
#include<Nest/ResourcesManager.h>
#include<Nest/World.h>
#include<Nest/Eggs/YamlEntitiesLoader/Loaders/YamlTransformComponentLoader.h>
using namespace Nest;
using namespace Nest::Eggs;

#include"../../../../Helpers/EntitiesManagerHelper.h"
using namespace Helpers;



namespace Nest_Eggs_Loaders_YamlTransformComponentLoaderTests
{
	bool CompareFloat(float x, float y, float delta)
	{
		auto cmp = x - y;
		if (cmp < 0) cmp = 0 - cmp;
		return(cmp <= delta);
	}



	TEST_CASE("Loading simple Transform", "[YamlTransformComponentLoaderTests]")
	{
		ComponentTypeMap componentTypeMap{ false };
		const auto world = InstantiateWorld();
		RegisterComponentFactory<Transform>(componentTypeMap, *world);

		auto entitiesManager = world->GetEntitiesManager();
		const EntitiesAndComponentsRemap remapper{};
		YamlTransformComponentLoader transformComponentLoader{};
		const ResourcesManager resourcesManager{};

		SECTION("Without any extra tags, transform is still valid")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/Transform/AllDefaultTransform.txt");
			REQUIRE(mainYamlNode["Transform"]);

			auto entity = entitiesManager->Instantiate().lock();
			auto transform = entity->AddComponent<Transform>();

			REQUIRE_NOTHROW(transformComponentLoader.LoadAttributes(*(transform.Get()), mainYamlNode["Transform"], remapper, resourcesManager));
		}

		SECTION("Load all tags")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/Transform/AllTagsTransform.txt");
			REQUIRE(mainYamlNode["Transform"]);

			auto entity = entitiesManager->Instantiate().lock();
			auto transform = entity->AddComponent<Transform>();

			transformComponentLoader.LoadAttributes(*(transform.Get()), mainYamlNode["Transform"], remapper, resourcesManager);

			auto name = transform->GetName();
			REQUIRE(name == "Transform Test");

			auto localPosition = transform->GetLocalPosition();
			REQUIRE(localPosition.x == 1);
			REQUIRE(localPosition.y == 2);
			REQUIRE(localPosition.z == 5);

			auto localRotation = transform->GetLocalRotation();
			auto localRotationAsEuler = localRotation.GetEulerYawPitchRollDegrees();
			REQUIRE(CompareFloat(localRotationAsEuler.x, 10, 0.001f));
			REQUIRE(CompareFloat(localRotationAsEuler.y, 15, 0.001f));
			REQUIRE(CompareFloat(localRotationAsEuler.z, 20, 0.001f));

			auto localScale = transform->GetLocalScale();
			REQUIRE(CompareFloat(localScale.x, 1.0f, 0.001f));
			REQUIRE(CompareFloat(localScale.y, 3.0f, 0.001f));
			REQUIRE(CompareFloat(localScale.z, 9.0f, 0.001f));
		}
	}
}

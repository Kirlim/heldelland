
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../../../catch.hpp"

#include<yaml-cpp/yaml.h>

#include<Nest/MeshFilter.h>
#include<Nest/EntitiesAndComponentsRemap.h>
#include<Nest/ResourcesManager.h>
#include<Nest/Eggs/YamlEntitiesLoader/Loaders/YamlMeshFilterComponentLoader.h>
using namespace Nest;
using namespace Nest::Eggs;

#include"../../../../Helpers/EntitiesManagerHelper.h"
using namespace Helpers;

#include"../../../../Mocks/MeshMock.h"



namespace Nest_Eggs_Loaders_YamlMeshFilterComponentLoaderTests
{
	TEST_CASE("Loading MeshFilter from a Yaml File", "[YamlMeshFilterComponentLoader]")
	{
		const EntitiesAndComponentsRemap remapper{};
		ResourcesManager resourcesManager{};

		YamlMeshFilterComponentLoader yamlMeshFilterComponentLoader{};
		MeshFilter meshFilter{};

		SECTION("Null mesh filter node is valid")
		{
			const auto meshFilterYamlNode = 
				YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/MeshFilter/NullNode.txt");

			yamlMeshFilterComponentLoader.LoadAttributes(
				meshFilter, meshFilterYamlNode["MeshFilter"], remapper, resourcesManager
			);

			REQUIRE(meshFilter.GetName().size() == 0);
			REQUIRE(meshFilter.GetMesh() == nullptr);
		}

		SECTION("Load mesh filter with name only")
		{
			const auto meshFilterYamlNode =
				YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/MeshFilter/NameOnly.txt");

			yamlMeshFilterComponentLoader.LoadAttributes(
				meshFilter, meshFilterYamlNode["MeshFilter"], remapper, resourcesManager
			);

			REQUIRE(meshFilter.GetName() == "Test Name");
			REQUIRE(meshFilter.GetMesh() == nullptr);
		}

		SECTION("Load mesh filter links the mesh resource")
		{
			std::unique_ptr<Mesh> mesh = std::make_unique<MeshMock>();
			auto meshAddress = mesh.get();

			resourcesManager.AddMesh("TestMeshResourceName", mesh);

			const auto meshFilterYamlNode =
				YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/MeshFilter/WithMesh.txt");

			yamlMeshFilterComponentLoader.LoadAttributes(
				meshFilter, meshFilterYamlNode["MeshFilter"], remapper, resourcesManager
			);

			REQUIRE(meshFilter.GetName() == "Mesh Filter With a Mesh");
			REQUIRE(meshFilter.GetMesh() == meshAddress);
		}
	}
}

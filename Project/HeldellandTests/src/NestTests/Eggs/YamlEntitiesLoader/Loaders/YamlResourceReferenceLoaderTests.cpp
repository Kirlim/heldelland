
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../../../catch.hpp"

#include<yaml-cpp/yaml.h>

#include<Nest/Exceptions/InvalidDataException.h>
#include<Nest/Exceptions/InvalidFileStructureException.h>
using namespace Nest;

#include<Nest/Eggs/YamlEntitiesLoader/Loaders/YamlResourceReferenceLoader.h>
using namespace Nest::Eggs;



namespace Nest_Eggs_Loaders_YamlResourceReferenceLoaderTests
{
	TEST_CASE("Loading a Resource Reference struct from a Yaml node", "[YamlResourceReferenceLoader]")
	{
		SECTION("Null node throws InvalidDataException")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/ResourceReference/Error_NullNode.txt");
			const auto resourceReferenceNode = mainYamlNode["Resource"];
			REQUIRE_THROWS_AS(YamlEntitiesLoader::LoadResourceReference(resourceReferenceNode), InvalidDataException);
		}



		SECTION("Throw InvalidFileStructure if the Resource Reference node is not a map")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/ResourceReference/Error_NotMap.txt");
			const auto resourceReferenceNode = mainYamlNode["Resource"];
			REQUIRE_THROWS_AS(YamlEntitiesLoader::LoadResourceReference(resourceReferenceNode), InvalidFileStructureException);
		}



		SECTION("Load mesh resource information")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/ResourceReference/CorrectMesh.txt");
			const auto resourceReferenceNode = mainYamlNode["Resource"];

			const auto resourceInformation = YamlEntitiesLoader::LoadResourceReference(resourceReferenceNode);
			REQUIRE(resourceInformation.resourceType == ResourceType::Mesh);
			REQUIRE(resourceInformation.resourceName == "MeshResourceName");
		}



		SECTION("Throw InvalidFileStructureException if the resource node has more than two nodes")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/ResourceReference/Error_TooManyKeys.txt");
			const auto resourceReferenceNode = mainYamlNode["Resource"];

			REQUIRE_THROWS_AS(YamlEntitiesLoader::LoadResourceReference(resourceReferenceNode), InvalidFileStructureException);
		}



		SECTION("Throw InvalidFileStructureException if the resource node has less than two nodes")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/ResourceReference/Error_TooFewKeys.txt");
			const auto resourceReferenceNode = mainYamlNode["Resource"];

			REQUIRE_THROWS_AS(YamlEntitiesLoader::LoadResourceReference(resourceReferenceNode), InvalidFileStructureException);
		}



		SECTION("Throw InvalidFileStructureException if at least one key is not correct")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/ResourceReference/Error_IncorrectKey.txt");
			const auto resourceReferenceNode = mainYamlNode["Resource"];

			REQUIRE_THROWS_AS(YamlEntitiesLoader::LoadResourceReference(resourceReferenceNode), InvalidFileStructureException);
		}



		SECTION("Throw InvalidDataException if the resource type cannot be translated")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/ResourceReference/Error_InvalidResourceType.txt");
			const auto resourceReferenceNode = mainYamlNode["Resource"];

			REQUIRE_THROWS_AS(YamlEntitiesLoader::LoadResourceReference(resourceReferenceNode), InvalidDataException);
		}
	}
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../../../catch.hpp"

#include<yaml-cpp/yaml.h>

#include<Nest/Exceptions/InvalidDataException.h>
#include<Nest/Vector3.h>
using namespace Nest;

#include<Nest/Eggs/YamlEntitiesLoader/Loaders/YamlVector3fLoader.h>
using namespace Nest::Eggs;



namespace Nest_Eggs_Loaders_YamlVector3fLoaderTests
{
	TEST_CASE("Loading a Vector3f from a Yaml file", "[YamlVector3fLoader]")
	{
		SECTION("Null node throws InvalidDataException")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/Vector3f/NullNode.txt");
			const auto vectorNode = mainYamlNode["Vector"];
			REQUIRE_THROWS_AS(YamlEntitiesLoader::LoadVector3f(vectorNode), InvalidDataException);
		}

		SECTION("Missing X field throws InvalidDataException")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/Vector3f/MissingXField.txt");
			const auto vectorNode = mainYamlNode["Vector"];
			REQUIRE_THROWS_AS(YamlEntitiesLoader::LoadVector3f(vectorNode), InvalidDataException);
		}

		SECTION("Missing Y field throws InvalidDataException")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/Vector3f/MissingYField.txt");
			const auto vectorNode = mainYamlNode["Vector"];
			REQUIRE_THROWS_AS(YamlEntitiesLoader::LoadVector3f(vectorNode), InvalidDataException);
		}

		SECTION("Missing Z field throws InvalidDataException")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/Vector3f/MissingZField.txt");
			const auto vectorNode = mainYamlNode["Vector"];
			REQUIRE_THROWS_AS(YamlEntitiesLoader::LoadVector3f(vectorNode), InvalidDataException);
		}

		SECTION("Any field that is not X, Y or Z throws InvalidDataException")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/Vector3f/InvalidField.txt");
			const auto vectorNode = mainYamlNode["Vector"];
			REQUIRE_THROWS_AS(YamlEntitiesLoader::LoadVector3f(vectorNode), InvalidDataException);
		}

		SECTION("Valid Vector3 node")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/Vector3f/ValidNode.txt");
			const auto vectorNode = mainYamlNode["Vector"];
			auto vector = YamlEntitiesLoader::LoadVector3f(vectorNode);

			REQUIRE(vector.x == 1);
			REQUIRE(vector.y == 2);
			REQUIRE(vector.z == 3);
		}

		SECTION("Valid Vector3 node accepts field out of order")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/Vector3f/ValidOutOfOrderNode.txt");
			const auto vectorNode = mainYamlNode["Vector"];
			auto vector = YamlEntitiesLoader::LoadVector3f(vectorNode);

			REQUIRE(vector.x == 1);
			REQUIRE(vector.y == 2);
			REQUIRE(vector.z == 3);
		}

		SECTION("Using overloaded operator >> to load a vector from a valid node")
		{
			auto mainYamlNode = YAML::LoadFile("NestTests/Data/YamlEntitiesLoader/Loaders/Vector3f/ValidNode.txt");
			const auto vectorNode = mainYamlNode["Vector"];

			Vector3f vector{ 0.0f,0.0f, 0.0f };
			vectorNode >> vector;

			REQUIRE(vector.x == 1);
			REQUIRE(vector.y == 2);
			REQUIRE(vector.z == 3);
		}
	}
}

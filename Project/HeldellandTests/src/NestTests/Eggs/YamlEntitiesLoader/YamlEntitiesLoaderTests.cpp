
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../../catch.hpp"

#include<memory>


#include<Nest/Exceptions/BadFileException.h>
#include<Nest/Exceptions/InvalidDataException.h>
#include"Nest/Exceptions/InvalidFileStructureException.h"
#include"Nest/Exceptions/NullArgumentException.h"

#include"Nest/ComponentTypeMap.h"
#include<Nest/IEntity.h>
#include<Nest/EntitiesManager.h>
#include<Nest/Transform.h>
#include<Nest/ResourcesManager.h>
#include<Nest/World.h>
#include<Nest/Eggs/YamlEntitiesLoader/YamlComponentsLoadersCollection.h>
#include<Nest/Eggs/YamlEntitiesLoader/YamlEntitiesLoader.h>
#include<Nest/Eggs/YamlEntitiesLoader/Loaders/YamlTransformComponentLoader.h>
using namespace Nest;
using namespace Nest::Eggs;

#include"../../../Helpers/EntitiesManagerHelper.h"
using namespace Helpers;



namespace YamlEntitiesLoaderTests
{
	TEST_CASE("YamlEntitiesLoader::YamlEntitiesLoader(EntitiesManager&, std::unique_ptr<Eggs::YamlComponentsLoaderCollection>&)", "[YamlEntitiesLoader]")
	{
		ComponentTypeMap componentTypeMap{ false };
		const ResourcesManager resourcesManager{};

		SECTION("Loading with empty loaders collection throws NullArgumentException")
		{
			const auto world = InstantiateWorld();
			RegisterComponentFactory<Transform>(componentTypeMap, *world);

			const auto entitiesManager = world->GetEntitiesManager();

			std::unique_ptr<Eggs::YamlComponentsLoadersCollection> componentsLoadersCollection;
			REQUIRE_THROWS_AS(
				YamlEntitiesLoader(*entitiesManager, resourcesManager, componentsLoadersCollection),
				NullArgumentException
			);
		}
	}



	TEST_CASE("YamlEntitiesLoader::LoadEntity(const std::string)", "[YamlEntitiesLoader]")
	{
		ComponentTypeMap componentTypeMap{ false };
		const auto world = InstantiateWorld();
		RegisterComponentFactory<Transform>(componentTypeMap, *world);

		const auto entitiesManager = world->GetEntitiesManager();

		const ResourcesManager resourcesManager{};

		auto componentsLoadersCollection = std::make_unique<Eggs::YamlComponentsLoadersCollection>();
		std::unique_ptr<YamlComponentLoader> transformLoader = std::make_unique<YamlTransformComponentLoader>();
		componentsLoadersCollection->Register(ComponentTypeMap::GetInstance()->GetTypeId<Transform>(), transformLoader);

		YamlEntitiesLoader entitiesLoader{ *entitiesManager, resourcesManager, componentsLoadersCollection };



		SECTION("Loading non-existing file throws BadFileException")
		{
			REQUIRE_THROWS_AS(entitiesLoader.LoadEntity("FileShouldNotExist"), BadFileException);
		}



		SECTION("Loading an invalid YAML file throws InvalidFileStructureException")
		{
			REQUIRE_THROWS_AS(entitiesLoader.LoadEntity("NestTests/Data/YamlEntitiesLoader/InvalidYamlStructure.pfb"), InvalidFileStructureException);
		}



		SECTION("Loading a YAML file without a initial Entity tag throws InvalidDataException")
		{
			REQUIRE_THROWS_AS(entitiesLoader.LoadEntity("NestTests/Data/YamlEntitiesLoader/NoEntityTag.pfb"), InvalidDataException);
		}



		SECTION("Loading a simple entity without components.")
		{
			auto loadedEntity = entitiesLoader.LoadEntity("NestTests/Data/YamlEntitiesLoader/SimpleEntityNoComponents.pfb");
			auto entity = loadedEntity.lock();

			REQUIRE(entity != nullptr);
			REQUIRE(entity->GetName() == "TestEntity");
		}



		SECTION("Loading a simple entity with a transform")
		{
			auto loadedEntity = entitiesLoader.LoadEntity("NestTests/Data/YamlEntitiesLoader/SimpleEntityWithTransform.pfb");
			auto entity = loadedEntity.lock();

			REQUIRE(entity != nullptr);
			REQUIRE(entity->GetName() == "Simple Entity With Transform");
			REQUIRE(entity->GetComponentsCount() == 1);

			auto transform = entity->GetComponent<Transform>();
			REQUIRE(transform != nullptr);
			REQUIRE(transform->GetOwner() == entity.get());
			REQUIRE(transform->GetName() == "Transform Name");
			REQUIRE(transform->GetLocalPosition().x == 1);
			REQUIRE(transform->GetLocalPosition().y == 2);
			REQUIRE(transform->GetLocalPosition().z == 3);
			REQUIRE(transform->GetChildCount() == 0);
		}



		SECTION("Loading a simple hierarchical entity with two children")
		{
			auto loadedEntity = entitiesLoader.LoadEntity("NestTests/Data/YamlEntitiesLoader/SimpleEntityWithTransformHierarchy.pfb");
			auto entity = loadedEntity.lock();

			REQUIRE(entity != nullptr);
			REQUIRE(entity->GetName() == "Simple Entity With Transform Hierarchy");
			REQUIRE(entity->GetComponentsCount() == 1);

			auto transform = entity->GetComponent<Transform>();
			REQUIRE(transform != nullptr);
			REQUIRE(transform->GetOwner() == entity.get());
			REQUIRE(transform->GetName() == "Transform Name");
			REQUIRE(transform->GetLocalPosition().x == 1);
			REQUIRE(transform->GetLocalPosition().y == 2);
			REQUIRE(transform->GetLocalPosition().z == 3);
			REQUIRE(transform->GetChildCount() == 2);


			bool childOneOk = false;
			bool childTwoOk = false;

			for (auto i = 0; i < transform->GetChildCount(); ++i)
			{
				auto childTransform = transform->GetChildAt(i);
				auto childEntity = childTransform->GetOwner();
				REQUIRE(childEntity != nullptr);
				REQUIRE(childTransform != nullptr);

				if (childEntity->GetName() == "Child Entity One")
				{
					REQUIRE(childEntity->GetComponentsCount() == 1);
					REQUIRE(childTransform->GetName() == "Child Transform One");
					REQUIRE(childTransform->GetLocalPosition().x == 4);
					REQUIRE(childTransform->GetLocalPosition().y == 5);
					REQUIRE(childTransform->GetLocalPosition().z == 6);
					REQUIRE(childTransform->GetChildCount() == 0);

					childOneOk = true;
				}
				else if (childEntity->GetName() == "Child Entity Two")
				{
					REQUIRE(childEntity->GetComponentsCount() == 1);
					REQUIRE(childTransform->GetName() == "Child Transform Two");
					REQUIRE(childTransform->GetLocalPosition().x == 7);
					REQUIRE(childTransform->GetLocalPosition().y == 8);
					REQUIRE(childTransform->GetLocalPosition().z == 9);
					REQUIRE(childTransform->GetChildCount() == 0);

					childTwoOk = true;
				}
			}

			REQUIRE(childOneOk);
			REQUIRE(childTwoOk);
		}



		SECTION("Entity name is optional")
		{
			auto loadedEntity = entitiesLoader.LoadEntity("NestTests/Data/YamlEntitiesLoader/EntityNameIsOptional.pfb");
			auto entity = loadedEntity.lock();

			REQUIRE(entity != nullptr);
			REQUIRE(entity->GetName() == "");
			REQUIRE(entity->GetComponentsCount() == 1);

			auto transform = entity->GetComponent<Transform>();
			REQUIRE(transform != nullptr);
			REQUIRE(transform->GetOwner() == entity.get());
			REQUIRE(transform->GetName() == "Nameless Entity Transform");
			REQUIRE(transform->GetChildCount() == 0);
		}



		SECTION("If Components node is not a sequence, throw InvalidDataException")
		{
			REQUIRE_THROWS_AS(entitiesLoader.LoadEntity("NestTests/Data/YamlEntitiesLoader/InvalidEntity_ComponentsNotSequence.pfb"), InvalidDataException);
		}



		SECTION("If Children node is not a sequence, throw InvalidDataException")
		{
			REQUIRE_THROWS_AS(entitiesLoader.LoadEntity("NestTests/Data/YamlEntitiesLoader/InvalidEntity_ChildrenNotSequence.pfb"), InvalidDataException);
		}



		SECTION("If there is more than a component in a Components sequence node, throw InvalidDataException")
		{
			REQUIRE_THROWS_AS(entitiesLoader.LoadEntity("NestTests/Data/YamlEntitiesLoader/InvalidEntity_TooManyComponentsInPosition.pfb"), InvalidDataException);
		}
	}
}
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../../catch.hpp"

#include<memory>

#include<Nest/Exceptions/NullArgumentException.h>
#include<Nest/Exceptions/LogicErrorException.h>

#include<Nest/Transform.h>

#include<Nest/Eggs/YamlEntitiesLoader/YamlComponentLoader.h>
#include<Nest/Eggs/YamlEntitiesLoader/YamlComponentsLoadersCollection.h>
#include<Nest/Eggs/YamlEntitiesLoader/Loaders/YamlTransformComponentLoader.h>
using namespace Nest;
using namespace Nest::Eggs;



namespace YamlComponentsLoadersCollectionTests
{
	TEST_CASE("YamlComponentsLoadersCollection::Register(const ComponentTypeId, std::unique_ptr<YamlComponentLoader>)", "[YamlComponentsLoadersCollection]")
	{
		ComponentTypeId transformTypeId = 1;
		YamlComponentsLoadersCollection loadersCollection{};

		SECTION("Trying to register a null loader throws NullArgumentException")
		{
			std::unique_ptr<YamlComponentLoader> loader;
			REQUIRE(loader == nullptr);
			REQUIRE_THROWS_AS(loadersCollection.Register(transformTypeId, loader), NullArgumentException);
		}

		SECTION("Registering a valid loader")
		{
			std::unique_ptr<YamlComponentLoader> loader = std::make_unique<YamlTransformComponentLoader>();

			REQUIRE_FALSE(loadersCollection.IsRegistered(transformTypeId));
			REQUIRE_NOTHROW(loadersCollection.Register(transformTypeId, loader));
			REQUIRE(loadersCollection.IsRegistered(transformTypeId));
			REQUIRE(loader == nullptr);
		}

		SECTION("Registering a repeated loader throws LogicErrorException")
		{
			std::unique_ptr<YamlComponentLoader> loader1 = std::make_unique<YamlTransformComponentLoader>();
			std::unique_ptr<YamlComponentLoader> loader2 = std::make_unique<YamlTransformComponentLoader>();

			REQUIRE_NOTHROW(loadersCollection.Register(transformTypeId, loader1));
			REQUIRE_THROWS_AS(loadersCollection.Register(transformTypeId, loader2), LogicErrorException);
			REQUIRE(loadersCollection.IsRegistered(transformTypeId));
			REQUIRE(loader1 == nullptr);
			REQUIRE(loader2 != nullptr);
		}
	}



	TEST_CASE("YamlComponentsLoadersCollection::Unegister(const ComponentTypeId)", "[YamlComponentsLoadersCollection]")
	{
		ComponentTypeId transformTypeId = 1;
		YamlComponentsLoadersCollection loadersCollection{};

		SECTION("Unregistering a registered loader")
		{
			std::unique_ptr<YamlComponentLoader> loader = std::make_unique<YamlTransformComponentLoader>();

			REQUIRE_NOTHROW(loadersCollection.Register(transformTypeId, loader));
			REQUIRE(loadersCollection.IsRegistered(transformTypeId));

			REQUIRE_NOTHROW(loadersCollection.Unregister(transformTypeId));
			REQUIRE_FALSE(loadersCollection.IsRegistered(transformTypeId));
		}

		SECTION("Unregistering a not registered loader throws LogicErrorException")
		{
			std::unique_ptr<YamlComponentLoader> loader = std::make_unique<YamlTransformComponentLoader>();

			REQUIRE_NOTHROW(loadersCollection.Register(transformTypeId, loader));
			REQUIRE_NOTHROW(loadersCollection.Unregister(transformTypeId));
			REQUIRE_THROWS_AS(loadersCollection.Unregister(transformTypeId), LogicErrorException);
		}
	}



	TEST_CASE("YamlComponentsLoadersCollection::GetLoader(const ComponentTypeId)", "[YamlComponentsLoadersCollection]")
	{
		ComponentTypeId transformTypeId = 1;
		YamlComponentsLoadersCollection loadersCollection{};

		SECTION("Getting not registered id throws LogicErrorException")
		{
			REQUIRE_THROWS_AS(loadersCollection.GetLoader(transformTypeId), LogicErrorException);
		}

		SECTION("Getting registered id returs loader")
		{
			std::unique_ptr<YamlComponentLoader> loader = std::make_unique<YamlTransformComponentLoader>();
			auto rawLoaderAddress = loader.get();

			REQUIRE(rawLoaderAddress != nullptr);
			REQUIRE_NOTHROW(loadersCollection.Register(transformTypeId, loader));
			REQUIRE(loadersCollection.GetLoader(transformTypeId) == rawLoaderAddress);
		}
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../../catch.hpp"

#include<Nest/Exceptions/InvalidArgumentException.h>

#include<Nest/Eggs/CollisionSystem/CollisionSystem.h>
#include<Nest/Eggs/CollisionSystem/AABBCollider.h>
using namespace Nest;
using namespace Eggs::CollisionSystem;



namespace CollisionSystemTests
{
	class AABoxColliderSystemMock
	{
	public:
		void Register(ColliderId, AABBColliderInfo) { ++registerCallCount; }
		void Unregister(ColliderId) { ++unregisterCallCount; }

		bool IsRegistered(ColliderId)
		{
			++isRegisteredCallCount;
			return true;
		}

		void EnqueueApplyTransformPermissive(ColliderId, const Matrix4<float>&) { ++enqueueeApplyTransformPermissiveCallCount; }
		void ApplyEnqueuedTransforms() { ++applyEnqueuedTransformsCallCount; }

		void RaycastTime(Ray, std::vector<RaycastHitTime>&) const { ++raycastTimeCallCount; }

		int registerCallCount{ 0 };
		int unregisterCallCount{ 0 };
		int isRegisteredCallCount{ 0 };
		int enqueueeApplyTransformPermissiveCallCount{ 0 };
		int applyEnqueuedTransformsCallCount{ 0 };
		mutable int raycastTimeCallCount{ 0 };
	};



	TEST_CASE("Instantiation of Collision System", "[CollisionSystem]")
	{
		const CollisionSystem<AABoxColliderSystemMock> collisionSystem{};
		REQUIRE(collisionSystem.GetAxisAlignedBoxColliderSystem() != nullptr);
	}



	TEST_CASE("Enqueueing Transformations for Applying on Collision System", "[CollisionSystem]")
	{
		const CollisionSystem<AABoxColliderSystemMock> collisionSystem{};


		SECTION("Using invalid collider id throws InvalidArgumentException")
		{
			const auto colliderId = MakeColliderId(0, 0);
			const Nest::Matrix4<float> transformation{};

			REQUIRE_THROWS_AS(collisionSystem.EnqueueApplyTransformation(colliderId, transformation), InvalidArgumentException);
		}



		SECTION("Using valid AABB collider id")
		{
			const auto colliderId = MakeColliderId(ColliderIdTraits::AABBType, 1);
			const Nest::Matrix4<float> transformation{};

			REQUIRE_NOTHROW(collisionSystem.EnqueueApplyTransformation(colliderId, transformation));
			REQUIRE(collisionSystem.GetAxisAlignedBoxColliderSystem()->enqueueeApplyTransformPermissiveCallCount == 1);
		}
	}



	TEST_CASE("Applying all transformations for colliders is all colliders systems", "[CollisionSystem]")
	{
		const CollisionSystem<AABoxColliderSystemMock> collisionSystem{};
		REQUIRE_NOTHROW(collisionSystem.ApplyAllEnqueuedTransformations());
		REQUIRE(collisionSystem.GetAxisAlignedBoxColliderSystem()->applyEnqueuedTransformsCallCount == 1);
	}



	TEST_CASE("Collision System forwards the registering methods of AABB colliders", "[CollisionSystem]")
	{
		CollisionSystem<AABoxColliderSystemMock> collisionSystem{};
		const auto colliderId = MakeColliderId(ColliderIdTraits::AABBType, 1);


		SECTION("Registering AABB collider is forwarded")
		{
			const AABBColliderInfo colliderInfo{};
			REQUIRE_NOTHROW(collisionSystem.Register(colliderId, colliderInfo));
			REQUIRE(collisionSystem.GetAxisAlignedBoxColliderSystem()->registerCallCount == 1);
		}


		SECTION("Is AABB collider registered is forwarded")
		{
			REQUIRE_NOTHROW(collisionSystem.IsRegistered(colliderId));
			REQUIRE(collisionSystem.GetAxisAlignedBoxColliderSystem()->isRegisteredCallCount == 1);
		}


		SECTION("Unregister AABB collider is forwarded")
		{
			REQUIRE_NOTHROW(collisionSystem.Unregister(colliderId));
			REQUIRE(collisionSystem.GetAxisAlignedBoxColliderSystem()->unregisterCallCount == 1);
		}
	}



	TEST_CASE("Collision system colliders registering methods throw InvalidArgumentException if collider id is invalid")
	{
		CollisionSystem<AABoxColliderSystemMock> collisionSystem{};
		const auto invalidColliderId = MakeColliderId(0, 1);


		SECTION("Registering collider with invalid id throws InvalidArgumentException")
		{
			const AABBColliderInfo colliderInfo{};
			REQUIRE_THROWS_AS(collisionSystem.Register(invalidColliderId, colliderInfo), InvalidArgumentException);
		}


		SECTION("Unregisterer collider with invalid id throw InvalidArgumentException")
		{
			REQUIRE_THROWS_AS(collisionSystem.Unregister(invalidColliderId), InvalidArgumentException);
		}
	}



	TEST_CASE("Collision System delegates raycasts to the registered colliders systems", "[CollisionSystem]")
	{
		CollisionSystem<AABoxColliderSystemMock> collisionSystem{};
		const Ray ray;

		REQUIRE_NOTHROW(collisionSystem.RaycastTime(ray));
		REQUIRE(collisionSystem.GetAxisAlignedBoxColliderSystem()->raycastTimeCallCount == 1);
	}
}

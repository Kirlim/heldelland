//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../../catch.hpp"

#include<Nest/Exceptions/InvalidArgumentException.h>
#include<Nest/Exceptions/LogicErrorException.h>

#include<Nest/Vector3.h>
#include<Nest/Ray.h>
#include<Nest/Eggs/CollisionSystem/AABoxColliderSystem.h>
using namespace Nest;
using namespace Eggs::CollisionSystem;



namespace AABoxColliderSystemTests
{
	bool CompareFloat(float l, float r)
	{
		float result = l - r;
		if (result < 0.0f) result = 0.0f - result;

		return (result < 0.000001f);
	}



	TEST_CASE("Managing registered AABB colliders", "[AABoxColliderSytem]")
	{
		AABoxColliderSystem colliderSystem{};


		SECTION("Registering new AABB collider")
		{
			const auto id = MakeColliderId(ColliderIdTraits::AABBType, 1);
			const AABBColliderInfo info{ Vector3f{ 0.2f, 0.3f, 0.4f }, Vector3f{ 0.5f, 0.5f, 0.5f } };

			REQUIRE_FALSE(colliderSystem.IsRegistered(id));
			REQUIRE_NOTHROW(colliderSystem.Register(id, info));
			REQUIRE(colliderSystem.IsRegistered(id));

			REQUIRE(colliderSystem.collidersIds.size() == 1);
			REQUIRE(colliderSystem.collidersInfo.size() == 1);
			REQUIRE(colliderSystem.colliders.size() == 1);

			REQUIRE(CompareFloat(colliderSystem.colliders[0].min.x, -0.3f));
			REQUIRE(CompareFloat(colliderSystem.colliders[0].min.y, -0.2f));
			REQUIRE(CompareFloat(colliderSystem.colliders[0].min.z, -0.1f));

			REQUIRE(CompareFloat(colliderSystem.colliders[0].max.x, 0.7f));
			REQUIRE(CompareFloat(colliderSystem.colliders[0].max.y, 0.8f));
			REQUIRE(CompareFloat(colliderSystem.colliders[0].max.z, 0.9f));
		}



		SECTION("Registering repeated AABB collider id throws LogicErrorException")
		{
			const auto id = MakeColliderId(ColliderIdTraits::AABBType, 1);

			const AABBColliderInfo info1{ Vector3f{ 0.2f, 0.3f, 0.4f }, Vector3f{ 0.5f, 0.5f, 0.5f } };
			const AABBColliderInfo info2{ Vector3f{ 0.2f, 0.3f, 0.4f }, Vector3f{ 0.5f, 0.5f, 0.5f } };

			REQUIRE_NOTHROW(colliderSystem.Register(id, info1));
			REQUIRE_THROWS_AS(colliderSystem.Register(id, info2), LogicErrorException);

			REQUIRE(colliderSystem.collidersIds.size() == 1);
			REQUIRE(colliderSystem.collidersInfo.size() == 1);
			REQUIRE(colliderSystem.colliders.size() == 1);
		}



		SECTION("Registering AABB collider with a collider id that is not of AABB type throws InvalidArgumentException")
		{
			const auto id = MakeColliderId(0, 1);
			const AABBColliderInfo info{ Vector3f{ 0.2f, 0.3f, 0.4f }, Vector3f{ 0.5f, 0.5f, 0.5f } };

			REQUIRE_THROWS_AS(colliderSystem.Register(id, info), InvalidArgumentException);
		}



		SECTION("Unregistering AABB collider by id")
		{
			const auto id = MakeColliderId(ColliderIdTraits::AABBType, 1);
			const AABBColliderInfo info{ Vector3f{ 0.2f, 0.3f, 0.4f }, Vector3f{ 0.5f, 0.5f, 0.5f } };
			REQUIRE_NOTHROW(colliderSystem.Register(id, info));

			REQUIRE_NOTHROW(colliderSystem.Unregister(id));
			REQUIRE(colliderSystem.collidersIds.size() == 0);
			REQUIRE(colliderSystem.collidersInfo.size() == 0);
			REQUIRE(colliderSystem.colliders.size() == 0);
		}



		SECTION("Unregistering not registered AABB collider by id throws LogicErrorException")
		{
			const auto id = MakeColliderId(ColliderIdTraits::AABBType, 1);
			REQUIRE_THROWS_AS(colliderSystem.Unregister(id), LogicErrorException);
		}



		SECTION("Appending AABB colliders indexes to a vector, by their ids")
		{
			const auto id1 = MakeColliderId(ColliderIdTraits::AABBType, 1);
			const auto id2 = MakeColliderId(ColliderIdTraits::AABBType, 2);
			const auto id3 = MakeColliderId(ColliderIdTraits::AABBType, 3);

			const AABBColliderInfo info1{ Vector3f{ 0.2f, 0.3f, 0.4f }, Vector3f{ 0.5f, 0.5f, 0.5f } };
			const AABBColliderInfo info2{ Vector3f{ 0.2f, 0.3f, 0.4f }, Vector3f{ 0.5f, 0.5f, 0.5f } };

			REQUIRE_NOTHROW(colliderSystem.Register(id1, info1));
			REQUIRE_NOTHROW(colliderSystem.Register(id2, info2));

			const std::vector<ColliderId> ids{ { id2, id1, id2, id3 } };
			std::vector<int> indexes{};

			REQUIRE_NOTHROW(colliderSystem.AppendCollidersIndexesToVectorPermissive(ids, /*out*/ indexes));
			REQUIRE(indexes.size() == 4);
			REQUIRE(indexes[0] == 1);
			REQUIRE(indexes[1] == 0);
			REQUIRE(indexes[2] == 1);
			REQUIRE(indexes[3] == -1);
		}
	}



	TEST_CASE("Enqueueing transformations for AABB colliders", "[AABoxColliderSytem]")
	{
		AABoxColliderSystem colliderSystem{};

		const auto id = MakeColliderId(ColliderIdTraits::AABBType, 1);
		const AABBColliderInfo info{ Vector3f{ 0.2f, 0.3f, 0.4f }, Vector3f{ 0.5f, 0.5f, 0.5f } };
		const Matrix4<float> transformation{
			1.0f, 0.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f, 2.0f,
			0.0f, 0.0f, 1.0f, 3.0f,
			0.0f, 0.0f, 0.0f, 1.0f
		};


		SECTION("Enqueueing a transformation for an AABB collider")
		{
			REQUIRE_NOTHROW(colliderSystem.Register(id, info));

			REQUIRE_FALSE(colliderSystem.IsApplyTranformEnqueued(id));
			REQUIRE_NOTHROW(colliderSystem.EnqueueApplyTransformPermissive(id, transformation));
			REQUIRE(colliderSystem.IsApplyTranformEnqueued(id));
		}



		SECTION("Applying a transformation fon an AABB collider")
		{
			REQUIRE_NOTHROW(colliderSystem.Register(id, info));

			REQUIRE_NOTHROW(colliderSystem.EnqueueApplyTransformPermissive(id, transformation));
			REQUIRE(colliderSystem.IsApplyTranformEnqueued(id));


			REQUIRE_NOTHROW(colliderSystem.ApplyEnqueuedTransforms());
			REQUIRE_FALSE(colliderSystem.IsApplyTranformEnqueued(id));

			REQUIRE(CompareFloat(colliderSystem.colliders[0].min.x, 0.7f));
			REQUIRE(CompareFloat(colliderSystem.colliders[0].min.y, 1.8f));
			REQUIRE(CompareFloat(colliderSystem.colliders[0].min.z, 2.9f));

			REQUIRE(CompareFloat(colliderSystem.colliders[0].max.x, 1.7f));
			REQUIRE(CompareFloat(colliderSystem.colliders[0].max.y, 2.8f));
			REQUIRE(CompareFloat(colliderSystem.colliders[0].max.z, 3.9f));
		}
	}



	TEST_CASE("Raycasting to get collision 'time' against a single registered AABB", "[AABoxColliderSystem]")
	{
		AABoxColliderSystem colliderSystem{};
		const auto id = MakeColliderId(ColliderIdTraits::AABBType, 1);
		const AABBColliderInfo info{ Vector3f{ 0.0f, 0.0f, 0.0f }, Vector3f{ 0.5f, 0.5f, 0.5f } };

		colliderSystem.Register(id, info);

		Ray ray{};
		std::vector<RaycastHitTime> hits{};


		SECTION("Raycasting middle from left to right")
		{
			ray.direction.Set(1.0f, 0.0f, 0.0f);
			ray.origin.Set(-1.5f, 0.0f, 0.0f);

			REQUIRE_NOTHROW(colliderSystem.RaycastTime(ray, /*out*/ hits));
			REQUIRE(hits.size() == 1);
			REQUIRE(hits[0].colliderId == id);
			REQUIRE(CompareFloat(hits[0].time, 1.0f));
		}


		SECTION("Raycasting middle from right to left")
		{
			ray.direction.Set(-1.0f, 0.0f, 0.0f);
			ray.origin.Set(2.5f, 0.0f, 0.0f);

			REQUIRE_NOTHROW(colliderSystem.RaycastTime(ray, /*out*/ hits));
			REQUIRE(hits.size() == 1);
			REQUIRE(hits[0].colliderId == id);
			REQUIRE(CompareFloat(hits[0].time, 2.0f));
		}


		SECTION("Raycasting middle from top to bottom")
		{
			ray.direction.Set(0.0f, -1.0f, 0.0f);
			ray.origin.Set(0.0f, 3.5f, 0.0f);

			REQUIRE_NOTHROW(colliderSystem.RaycastTime(ray, /*out*/ hits));
			REQUIRE(hits.size() == 1);
			REQUIRE(hits[0].colliderId == id);
			REQUIRE(CompareFloat(hits[0].time, 3.0f));
		}


		SECTION("Raycasting middle from bottom to top")
		{
			ray.direction.Set(0.0f, 1.0f, 0.0f);
			ray.origin.Set(0.0f, -4.5f, 0.0f);

			REQUIRE_NOTHROW(colliderSystem.RaycastTime(ray, /*out*/ hits));
			REQUIRE(hits.size() == 1);
			REQUIRE(hits[0].colliderId == id);
			REQUIRE(CompareFloat(hits[0].time, 4.0f));
		}


		SECTION("Raycasting middle from back to front")
		{
			ray.direction.Set(0.0f, 0.0f, 1.0f);
			ray.origin.Set(0.0f, 0.0, -5.5f);

			REQUIRE_NOTHROW(colliderSystem.RaycastTime(ray, /*out*/ hits));
			REQUIRE(hits.size() == 1);
			REQUIRE(hits[0].colliderId == id);
			REQUIRE(CompareFloat(hits[0].time, 5.0f));
		}


		SECTION("Raycasting middle from front to back")
		{
			ray.direction.Set(0.0f, 0.0f, -1.0f);
			ray.origin.Set(0.0f, 0.0, 6.5f);

			REQUIRE_NOTHROW(colliderSystem.RaycastTime(ray, /*out*/ hits));
			REQUIRE(hits.size() == 1);
			REQUIRE(hits[0].colliderId == id);
			REQUIRE(CompareFloat(hits[0].time, 6.0f));
		}


		SECTION("Raycast that misses, from left to right")
		{
			ray.direction.Set(1.0f, 0.0f, 0.0f);
			ray.origin.Set(-1.0f, 0.0f, 0.50001f);

			REQUIRE_NOTHROW(colliderSystem.RaycastTime(ray, /*out*/ hits));
			REQUIRE(hits.empty());
		}


		SECTION("Raycast that misses, from right to left")
		{
			ray.direction.Set(-1.0f, 0.0f, 0.0f);
			ray.origin.Set(1.0f, 0.0f, -0.50001f);

			REQUIRE_NOTHROW(colliderSystem.RaycastTime(ray, /*out*/ hits));
			REQUIRE(hits.empty());
		}


		SECTION("Raycast that misses, from top to bottom")
		{
			ray.direction.Set(0.0f, -1.0f, 0.0f);
			ray.origin.Set(0.50001f, 1.0f, 0.0f);

			REQUIRE_NOTHROW(colliderSystem.RaycastTime(ray, /*out*/ hits));
			REQUIRE(hits.empty());
		}


		SECTION("Raycast that misses, from bottom to top")
		{
			ray.direction.Set(0.0f, 1.0f, 0.0f);
			ray.origin.Set(-0.50001f, -1.0f, 0.0f);

			REQUIRE_NOTHROW(colliderSystem.RaycastTime(ray, /*out*/ hits));
			REQUIRE(hits.empty());
		}


		SECTION("Raycast that misses, from back to front")
		{
			ray.direction.Set(0.0f, 0.0f, 1.0f);
			ray.origin.Set(0.0f, 0.50001f, -1.0f);

			REQUIRE_NOTHROW(colliderSystem.RaycastTime(ray, /*out*/ hits));
			REQUIRE(hits.empty());
		}


		SECTION("Raycast that misses, from front to back")
		{
			ray.direction.Set(0.0f, 0.0f, -1.0f);
			ray.origin.Set(0.0f, -0.50001f, 1.0f);

			REQUIRE_NOTHROW(colliderSystem.RaycastTime(ray, /*out*/ hits));
			REQUIRE(hits.empty());
		}
	}



	TEST_CASE("Raycasting to get collision 'time' against a multiple registered AABB", "[AABoxColliderSystem]")
	{
		AABoxColliderSystem colliderSystem{};

		const auto id1 = MakeColliderId(ColliderIdTraits::AABBType, 1);
		const auto id2 = MakeColliderId(ColliderIdTraits::AABBType, 2);

		const AABBColliderInfo info1{ Vector3f{ 0.0f, 0.0f, 0.0f }, Vector3f{ 0.5f, 0.5f, 0.5f } };
		const AABBColliderInfo info2{ Vector3f{ 2.0f, 0.0f, 0.0f }, Vector3f{ 0.5f, 0.5f, 0.5f } };

		colliderSystem.Register(id1, info1);
		colliderSystem.Register(id2, info2);

		Ray ray{};
		std::vector<RaycastHitTime> hits{};

		ray.direction.Set(1.0f, 0.0f, 0.0f);
		ray.origin.Set(-1.5f, 0.0f, 0.0f);


		REQUIRE_NOTHROW(colliderSystem.RaycastTime(ray, /*out*/ hits));
		REQUIRE(hits.size() == 2);

		REQUIRE(hits[0].colliderId == id1);
		REQUIRE(hits[0].time == 1.0f);

		REQUIRE(hits[1].colliderId == id2);
		REQUIRE(hits[1].time == 3.0f);
	}
}

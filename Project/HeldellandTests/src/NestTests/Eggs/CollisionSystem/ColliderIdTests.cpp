//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../../../catch.hpp"

#include<Nest/Eggs/CollisionSystem/ColliderId.h>
using namespace Nest::Eggs::CollisionSystem;



namespace ColliderIdTests
{
	TEST_CASE("Getting ColliderId type and id", "[ColliderId]")
	{
		const uint32_t type = 0x12000000;
		const uint32_t id = 0x00123456;

		auto colliderId = MakeColliderId(type, id);

		REQUIRE(colliderId.value == 0x12123456);
		REQUIRE(GetColliderType(colliderId) == type);
		REQUIRE(GetColliderId(colliderId) == id);
	}
}

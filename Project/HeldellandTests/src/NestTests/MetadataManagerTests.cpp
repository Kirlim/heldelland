
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<string>
#include<vector>

#include<Nest/Exceptions/InvalidArgumentException.h>
#include<Nest/Exceptions/NullArgumentException.h>
#include<Nest/Metadata/MetadataManager.h>
#include<Nest/ResourcesManager.h>
using namespace Nest;

#include"../Mocks/MetadataInformationMock.h"



namespace MetadataManagerTests
{
	TEST_CASE("MetadataManager::MetadataManager", "[MetadataManager]")
	{
		SECTION("If the factory function is null, NullArgumentException will be thrown")
		{
			REQUIRE_THROWS_AS(std::make_unique<MetadataManager>(nullptr), NullArgumentException);
		}
	}



	TEST_CASE("MetadataManager::SetMetadataExtension", "[MetadataManager]")
	{
		MetadataManager metadataManager{ [](const ResourceType, const std::string) { return(nullptr); } };

		SECTION("Set method works")
		{
			std::string extension = ".dfs";
			REQUIRE_NOTHROW(metadataManager.SetMetadataExtension(extension));
			REQUIRE(metadataManager.GetMetadataExtension() == extension);
		}

		SECTION("Set method throw InvalidArgumentException if extension is empty")
		{
			REQUIRE_THROWS_AS(metadataManager.SetMetadataExtension(""), InvalidArgumentException);
		}
	}



	TEST_CASE("MetadataManager::LoadMetadataFromPath loading data", "[MetadataManager]")
	{
		std::vector<MetadataInformationMock*> createdMetadata;
		MetadataManager metadataManager{
			[&createdMetadata](const ResourceType resourceType, const std::string resourceName)
			{
				auto metadata = std::make_unique<MetadataInformationMock>(resourceType, resourceName);
				createdMetadata.push_back(metadata.get());
				return metadata;
			}
		};

		const std::string extension = ".dfs";
		metadataManager.SetMetadataExtension(extension);

		metadataManager.LoadMetadataFromPath("NestTests/Data/MetadataManager/");
		REQUIRE(!createdMetadata.empty());
		for (auto& metadata : createdMetadata)
		{
			REQUIRE(metadata->GetLoadMetadataFromYamlNodeCallsCount() == 1);
			REQUIRE(metadata->GetLoadResourceIntoManagerCallsCount() == 0);
		}

		ResourcesManager resourcesManager;
		metadataManager.LoadAllResources(resourcesManager);
		for (auto& metadata : createdMetadata)
		{
			REQUIRE(metadata->GetLoadMetadataFromYamlNodeCallsCount() == 1);
			REQUIRE(metadata->GetLoadResourceIntoManagerCallsCount() == 1);
		}
	}
}

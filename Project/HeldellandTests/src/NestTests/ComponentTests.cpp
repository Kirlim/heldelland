
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<Nest/Component.h>
using namespace Nest;



namespace ComponentTests
{
	TEST_CASE("Component::SetName", "[Component]")
	{
		Component component;
		
		component.SetName("name1");
		REQUIRE(component.GetName() == "name1");

		component.SetName("name2");
		REQUIRE(component.GetName() == "name2");
	}



	TEST_CASE("Component::GetId", "[Component]")
	{
		const Component component1;
		const Component component2;

		REQUIRE(component1.GetId() != component2.GetId());
	}



	TEST_CASE("Component::CopyFrom throws LogicErrorException if base method is called.", "[Component]")
	{
		Component source, target;
		// TODO : verify why this REQUIRE was commented out
		//REQUIRE_THROWS_AS(source.CopyFrom(target), LogicErrorException);
	}
}
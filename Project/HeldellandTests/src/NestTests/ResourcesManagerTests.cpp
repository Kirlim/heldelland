
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<memory>
#include<string>

#include<Nest/Exceptions/AlreadyRegisteredException.h>
#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/Exceptions/NotRegisteredException.h>
#include<Nest/Exceptions/NullArgumentException.h>

#include<Nest/Font.h>
#include<Nest/Material.h>
#include<Nest/Mesh.h>
#include<Nest/ShaderProgram.h>
#include<Nest/RenderingModule.h>
#include<Nest/IResourcesManager.h>
#include<Nest/ResourcesManager.h>
using namespace Nest;

#include"../Mocks/MeshMock.h"




namespace ResourcesManagerTests
{
	TEST_CASE("IResourcesManager::AddFont", "[ResourcesManager]")
	{
		std::unique_ptr<IResourcesManager> resourcesManager = std::make_unique<ResourcesManager>();

		SECTION("Adding a font")
		{
			auto font = std::make_unique<Font>(*resourcesManager);
			auto fontAddress = font.get();

			REQUIRE(resourcesManager->FetchFont("fontName") == nullptr);
			REQUIRE_NOTHROW(resourcesManager->AddFont("fontName", font));
			REQUIRE(font == nullptr);
			REQUIRE(resourcesManager->FetchFont("fontName") == fontAddress);
		}

		SECTION("Adding two fonts with same resource name throws AlreadyRegisteredException")
		{
			auto font1 = std::make_unique<Font>(*resourcesManager);
			auto font2 = std::make_unique<Font>(*resourcesManager);

			const std::string fontName{ "fontName" };

			resourcesManager->AddFont(fontName, font1);
			REQUIRE_THROWS_AS(resourcesManager->AddFont(fontName, font2), AlreadyRegisteredException);
			REQUIRE(font2 != nullptr);
		}

		SECTION("Adding a font with empty resource name throws LogicErrorException")
		{
			auto font = std::make_unique<Font>(*resourcesManager);
			REQUIRE_THROWS_AS(resourcesManager->AddFont("", font), LogicErrorException);
			REQUIRE(font != nullptr);
		}

		SECTION("Adding a null font throws NullArgumentException")
		{
			std::unique_ptr<Font> font;
			REQUIRE_THROWS_AS(resourcesManager->AddFont("fontName", font), NullArgumentException);
		}
	}

	TEST_CASE("IResourcesManager::UnloadFont", "[ResourcesManager]")
	{
		std::unique_ptr<IResourcesManager> resourcesManager = std::make_unique<ResourcesManager>();

		SECTION("Removing an added font")
		{
			auto font = std::make_unique<Font>(*resourcesManager);

			const std::string fontName{ "fontName" };
			resourcesManager->AddFont(fontName, font);
			resourcesManager->UnloadFont(fontName);
			REQUIRE(resourcesManager->FetchFont(fontName) == nullptr);
		}

		SECTION("Removing a not added font throws NotRegisteredException")
		{
			REQUIRE_THROWS_AS(resourcesManager->UnloadFont("fontName"), NotRegisteredException);
		}
	}



	TEST_CASE("IResourcesManager::AddMaterial", "[ResourcesManager]")
	{
		std::unique_ptr<IResourcesManager> resourcesManager = std::make_unique<ResourcesManager>();

		SECTION("Adding a material")
		{
			auto material = std::make_unique<Material>();
			auto materialAddress = material.get();

			const std::string materialName{ "materialName" };

			REQUIRE(resourcesManager->FetchMaterial(materialName) == nullptr);
			REQUIRE_NOTHROW(resourcesManager->AddMaterial(materialName, material));
			REQUIRE(material == nullptr);
			REQUIRE(resourcesManager->FetchMaterial(materialName) == materialAddress);
		}

		SECTION("Adding two materials with same resource name throws AlreadyRegisteredException")
		{
			auto material1 = std::make_unique<Material>();
			auto material2 = std::make_unique<Material>();

			const std::string materialName{ "materialName" };

			resourcesManager->AddMaterial(materialName, material1);
			REQUIRE_THROWS_AS(resourcesManager->AddMaterial(materialName, material2), AlreadyRegisteredException);
			REQUIRE(material2 != nullptr);
		}

		SECTION("Adding a material with empty resource name throws LogicErrorException")
		{
			auto material = std::make_unique<Material>();
			REQUIRE_THROWS_AS(resourcesManager->AddMaterial("", material), LogicErrorException);
			REQUIRE(material != nullptr);
		}

		SECTION("Adding a null material throws NullArgumentException")
		{
			std::unique_ptr<Material> material;
			REQUIRE_THROWS_AS(resourcesManager->AddMaterial("materialName", material), NullArgumentException);
		}
	}

	TEST_CASE("IResourcesManager::UnloadMaterial", "[ResourcesManager]")
	{
		std::unique_ptr<IResourcesManager> resourcesManager = std::make_unique<ResourcesManager>();

		SECTION("Removing an added material")
		{
			auto material = std::make_unique<Material>();

			const std::string materialName{ "materialName" };
			resourcesManager->AddMaterial(materialName, material);
			resourcesManager->UnloadMaterial(materialName);
			REQUIRE(resourcesManager->FetchMaterial(materialName) == nullptr);
		}

		SECTION("Removing a not added material throws NotRegisteredException")
		{
			REQUIRE_THROWS_AS(resourcesManager->UnloadMaterial("materialName"), NotRegisteredException);
		}
	}



	TEST_CASE("IResourcesManager::AddMesh", "[ResourcesManager]")
	{
		std::unique_ptr<IResourcesManager> resourcesManager = std::make_unique<ResourcesManager>();

		SECTION("Adding a mesh")
		{
			std::unique_ptr<Mesh> mesh = std::make_unique<MeshMock>();
			auto meshAddress = mesh.get();

			const std::string meshName{ "meshName" };

			REQUIRE(resourcesManager->FetchMesh(meshName) == nullptr);
			REQUIRE_NOTHROW(resourcesManager->AddMesh(meshName, mesh));
			REQUIRE(mesh == nullptr);
			REQUIRE(resourcesManager->FetchMesh(meshName) == meshAddress);
		}

		SECTION("Adding two meshes with same resource name throws AlreadyRegisteredException")
		{
			std::unique_ptr<Mesh> mesh1 = std::make_unique<MeshMock>();
			std::unique_ptr<Mesh> mesh2 = std::make_unique<MeshMock>();

			const std::string meshName{ "meshName" };

			resourcesManager->AddMesh(meshName, mesh1);
			REQUIRE_THROWS_AS(resourcesManager->AddMesh(meshName, mesh2), AlreadyRegisteredException);
			REQUIRE(mesh2 != nullptr);
		}

		SECTION("Adding a mesh with empty resource name throws LogicErrorException")
		{
			std::unique_ptr<Mesh> mesh = std::make_unique<MeshMock>();
			REQUIRE_THROWS_AS(resourcesManager->AddMesh("", mesh), LogicErrorException);
			REQUIRE(mesh != nullptr);
		}

		SECTION("Adding a null mesh throws NullArgumentException")
		{
			std::unique_ptr<Mesh> mesh;
			REQUIRE_THROWS_AS(resourcesManager->AddMesh("meshName", mesh), NullArgumentException);
		}
	}

	TEST_CASE("IResourcesManager::UnloadMesh", "[ResourcesManager]")
	{
		std::unique_ptr<IResourcesManager> resourcesManager = std::make_unique<ResourcesManager>();

		SECTION("Removing an added mesh")
		{
			std::unique_ptr<Mesh> mesh = std::make_unique<MeshMock>();

			const std::string meshName{ "meshName" };
			resourcesManager->AddMesh(meshName, mesh);
			resourcesManager->UnloadMesh(meshName);
			REQUIRE(resourcesManager->FetchMesh(meshName) == nullptr);
		}

		SECTION("Removing a not added mesh throws NotRegisteredException")
		{
			REQUIRE_THROWS_AS(resourcesManager->UnloadMesh("meshName"), NotRegisteredException);
		}
	}



	TEST_CASE("IResourcesManager::AddModel", "[ResourcesManager]")
	{
		std::unique_ptr<IResourcesManager> resourcesManager = std::make_unique<ResourcesManager>();

		SECTION("Adding a model")
		{
			auto model = std::make_unique<Model>();
			auto modelAddress = model.get();

			const std::string modelName{ "modelName" };

			REQUIRE(resourcesManager->FetchModel(modelName) == nullptr);
			REQUIRE_NOTHROW(resourcesManager->AddModel(modelName, model));
			REQUIRE(model == nullptr);
			REQUIRE(resourcesManager->FetchModel(modelName) == modelAddress);
		}

		SECTION("Adding two models with same resource name throws AlreadyRegisteredException")
		{
			auto model1 = std::make_unique<Model>();
			auto model2 = std::make_unique<Model>();

			const std::string modelName{ "modelName" };

			resourcesManager->AddModel(modelName, model1);
			REQUIRE_THROWS_AS(resourcesManager->AddModel(modelName, model2), AlreadyRegisteredException);
			REQUIRE(model2 != nullptr);
		}

		SECTION("Adding a model with empty resource name throws LogicErrorException")
		{
			auto model = std::make_unique<Model>();
			REQUIRE_THROWS_AS(resourcesManager->AddModel("", model), LogicErrorException);
			REQUIRE(model != nullptr);
		}

		SECTION("Adding a null model throws NullArgumentException")
		{
			std::unique_ptr<Model> model;
			REQUIRE_THROWS_AS(resourcesManager->AddModel("modelName", model), NullArgumentException);
		}
	}

	TEST_CASE("IResourcesManager::UnloadModel", "[ResourcesManager]")
	{
		std::unique_ptr<IResourcesManager> resourcesManager = std::make_unique<ResourcesManager>();

		SECTION("Removing an added model")
		{
			auto model = std::make_unique<Model>();

			const std::string modelName{ "modelName" };
			resourcesManager->AddModel(modelName, model);
			resourcesManager->UnloadModel(modelName);
			REQUIRE(resourcesManager->FetchMaterial(modelName) == nullptr);
		}

		SECTION("Removing a not added model throws NotRegisteredException")
		{
			REQUIRE_THROWS_AS(resourcesManager->UnloadModel("modelName"), NotRegisteredException);
		}
	}



	TEST_CASE("IResourcesManager::AddShaderProgram", "[ResourcesManager]")
	{
		std::unique_ptr<IResourcesManager> resourcesManager = std::make_unique<ResourcesManager>();

		SECTION("Adding a shader program")
		{
			auto shaderProgram = std::make_unique<ShaderProgram>(GameRenderer::Test);
			auto shaderProgramAddress = shaderProgram.get();

			const std::string shaderProgramName{ "shaderProgramName" };

			REQUIRE(resourcesManager->FetchModel(shaderProgramName) == nullptr);
			REQUIRE_NOTHROW(resourcesManager->AddShaderProgram(shaderProgramName, shaderProgram));
			REQUIRE(shaderProgram == nullptr);
			REQUIRE(resourcesManager->FetchShaderProgram(shaderProgramName) == shaderProgramAddress);
		}

		SECTION("Adding two shader programs with same resource name throws AlreadyRegisteredException")
		{
			auto shaderProgram1 = std::make_unique<ShaderProgram>(GameRenderer::Test);
			auto shaderProgram2 = std::make_unique<ShaderProgram>(GameRenderer::Test);

			const std::string shaderProgramName{ "shaderProgramName" };

			resourcesManager->AddShaderProgram(shaderProgramName, shaderProgram1);
			REQUIRE_THROWS_AS(resourcesManager->AddShaderProgram(shaderProgramName, shaderProgram2), AlreadyRegisteredException);
			REQUIRE(shaderProgram2 != nullptr);
		}

		SECTION("Adding a shader program with empty resource name throws LogicErrorException")
		{
			auto shaderProgram = std::make_unique<ShaderProgram>(GameRenderer::Test);
			REQUIRE_THROWS_AS(resourcesManager->AddShaderProgram("", shaderProgram), LogicErrorException);
			REQUIRE(shaderProgram != nullptr);
		}

		SECTION("Adding a null shader program throws NullArgumentException")
		{
			std::unique_ptr<ShaderProgram> shaderProgram;
			REQUIRE_THROWS_AS(resourcesManager->AddShaderProgram("shaderProgramName", shaderProgram), NullArgumentException);
		}
	}

	TEST_CASE("IResourcesManager::UnloadShaderProgram", "[ResourcesManager]")
	{
		std::unique_ptr<IResourcesManager> resourcesManager = std::make_unique<ResourcesManager>();

		SECTION("Removing an added shader program")
		{
			auto shaderProgram = std::make_unique<ShaderProgram>(GameRenderer::Test);

			const std::string shaderProgramName{ "shaderProgramName" };
			resourcesManager->AddShaderProgram(shaderProgramName, shaderProgram);
			resourcesManager->UnloadShaderProgram(shaderProgramName);
			REQUIRE(resourcesManager->FetchShaderProgram(shaderProgramName) == nullptr);
		}

		SECTION("Removing a not added shader program throws NotRegisteredException")
		{
			REQUIRE_THROWS_AS(resourcesManager->UnloadShaderProgram("shaderProgramName"), NotRegisteredException);
		}
	}



	TEST_CASE("IResourcesManager::AddTexture", "[ResourcesManager]")
	{
		std::unique_ptr<IResourcesManager> resourcesManager = std::make_unique<ResourcesManager>();

		SECTION("Adding a texture")
		{
			auto texture = std::make_unique<Texture>(GameRenderer::Test);
			auto textureAddress = texture.get();

			const std::string textureName{ "textureName" };

			REQUIRE(resourcesManager->FetchTexture(textureName) == nullptr);
			REQUIRE_NOTHROW(resourcesManager->AddTexture(textureName, texture));
			REQUIRE(texture == nullptr);
			REQUIRE(resourcesManager->FetchTexture(textureName) == textureAddress);
		}

		SECTION("Adding two textures with same resource name throws AlreadyRegisteredException")
		{
			auto texture1 = std::make_unique<Texture>(GameRenderer::Test);
			auto texture2 = std::make_unique<Texture>(GameRenderer::Test);

			const std::string textureName{ "textureName" };

			resourcesManager->AddTexture(textureName, texture1);
			REQUIRE_THROWS_AS(resourcesManager->AddTexture(textureName, texture2), AlreadyRegisteredException);
			REQUIRE(texture2 != nullptr);
		}

		SECTION("Adding a texture with empty resource name throws LogicErrorException")
		{
			auto texture = std::make_unique<Texture>(GameRenderer::Test);
			REQUIRE_THROWS_AS(resourcesManager->AddTexture("", texture), LogicErrorException);
			REQUIRE(texture != nullptr);
		}

		SECTION("Adding a null texture throws NullArgumentException")
		{
			std::unique_ptr<Texture> texture;
			REQUIRE_THROWS_AS(resourcesManager->AddTexture("textureName", texture), NullArgumentException);
		}
	}

	TEST_CASE("IResourcesManager::UnloadTexture", "[ResourcesManager]")
	{
		std::unique_ptr<IResourcesManager> resourcesManager = std::make_unique<ResourcesManager>();

		SECTION("Removing an added texture")
		{
			auto texture = std::make_unique<Texture>(GameRenderer::Test);

			const std::string textureName{ "textureName" };
			resourcesManager->AddTexture(textureName, texture);
			resourcesManager->UnloadTexture(textureName);
			REQUIRE(resourcesManager->FetchTexture(textureName) == nullptr);
		}

		SECTION("Removing a not added texture throws NotRegisteredException")
		{
			REQUIRE_THROWS_AS(resourcesManager->UnloadTexture("textureName"), NotRegisteredException);
		}
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<Nest/FixedPoint.h>

using SigFix = FixedPoint<true, 48, 16>;
using UnsigFix = FixedPoint<false, 48, 16>;



namespace Nest_FixedPointTests
{
	TEST_CASE("FixedPoint::FixedPoint", "[FixedPoint]")
	{
		SECTION("Integer construction")
		{
			SigFix ten{ 10 };
			SigFix negativeTen{ -10 };
			UnsigFix twenty{ 20 };

			REQUIRE(ten == 10);
			REQUIRE(negativeTen == -10);
			REQUIRE(twenty == 20);
		}

		SECTION("Float construction")
		{
			SigFix signedPositive{ 10.5f };
			SigFix signedNegative{ -20.5f };
			UnsigFix unsignedFixed{ 12.44f };

			REQUIRE(signedPositive == 10.5f);
			REQUIRE(signedNegative == -20.5f);
			REQUIRE(unsignedFixed == 12.44f);
		}

		SECTION("Double construction")
		{
			SigFix signedPositive{ 10.5 };
			SigFix signedNegative{ -20.5 };
			UnsigFix unsignedFixed{ 12.44 };

			REQUIRE(signedPositive == 10.5);
			REQUIRE(signedNegative == -20.5);
			REQUIRE(unsignedFixed == 12.44);
		}

		SECTION("Unsigned int construction")
		{
			SigFix ten{ 10u };
			UnsigFix twenty{ 20u };

			REQUIRE(ten == 10u);
			REQUIRE(twenty == 20u);
		}

		SECTION("Copy constructor")
		{
			const SigFix ten{ 10u };
			const UnsigFix twenty{ 20u };

			const SigFix copyOfTen{ ten };
			const UnsigFix copyOfTwenty{ twenty };

			REQUIRE(copyOfTen == 10u);
			REQUIRE(copyOfTwenty == 20u);
		}
	}



	TEST_CASE("FixedPoint::operator+=", "[FixedPoint]")
	{
		SECTION("Signed Sum")
		{
			SigFix left1{ 10 };
			const SigFix right1{ 12.5 };
			left1 += right1;
			REQUIRE(left1 == 22.5);

			SigFix left2{ 10 };
			const SigFix right2{ -12.5 };
			left2 += right2;
			REQUIRE(left2 == -2.5);
		}

		SECTION("Unsigned Sum")
		{
			UnsigFix left{ 12.25 };
			const UnsigFix right{ 22.25 };
			left += right;
			REQUIRE(left == 34.5);
		}
	}



	TEST_CASE("FixedPoint::operator+", "[FixedPoint]")
	{
		SECTION("Signed Sum")
		{
			const SigFix left1{ 10 };
			const SigFix right1{ 12.5 };
			SigFix result1 = left1 + right1;
			REQUIRE(result1 == 22.5);

			const SigFix left2{ 10 };
			const SigFix right2{ -12.5 };
			SigFix result2 = left2 + right2;
			REQUIRE(result2 == -2.5);
		}

		SECTION("Unsigned Sum")
		{
			const UnsigFix left{ 12.25 };
			const UnsigFix right{ 22.25 };
			UnsigFix result = left + right;
			REQUIRE(result == 34.5);
		}
	}



	TEST_CASE("FixedPoint::operator-=", "[FixedPoint]")
	{
		SECTION("Signed Subtraction")
		{
			SigFix left1{ 12.75 };
			const SigFix right1{ 2.5 };
			left1 -= right1;
			REQUIRE(left1 == 10.25);

			SigFix left2{ 12.75 };
			const SigFix right2{ -2.5 };
			left2 -= right2;
			REQUIRE(left2 == 15.25);

			SigFix left3{ -12.75 };
			const SigFix right3{ 2.5 };
			left3 -= right3;
			REQUIRE(left3 == -15.25);

			SigFix left4{ -12.75 };
			const SigFix right4{ -2.5 };
			left4 -= right4;
			REQUIRE(left4 == -10.25);
		}

		SECTION("Unsigned Subtraction")
		{
			UnsigFix left1{ 2.5 };
			const UnsigFix right1{ 1 };
			left1 -= right1;
			REQUIRE(left1 == 1.5);

			UnsigFix left2{ 1 };
			const UnsigFix right2{ 2 };
			left2 -= right2;
			REQUIRE(left2 > 1);
		}
	}



	TEST_CASE("FixedPoint::operator-", "[FixedPoint]")
	{
		SECTION("Signed Subtraction")
		{
			const SigFix left1{ 12.75 };
			const SigFix right1{ 2.5 };
			SigFix result1 = left1 - right1;
			REQUIRE(result1 == 10.25);

			const SigFix left2{ 12.75 };
			const SigFix right2{ -2.5 };
			SigFix result2 = left2 - right2;
			REQUIRE(result2 == 15.25);

			const SigFix left3{ -12.75 };
			const SigFix right3{ 2.5 };
			SigFix result3 = left3 - right3;
			REQUIRE(result3 == -15.25);

			const SigFix left4{ -12.75 };
			const SigFix right4{ -2.5 };
			SigFix result4 = left4 - right4;
			REQUIRE(result4 == -10.25);
		}

		SECTION("Unsigned Subtraction")
		{
			const UnsigFix left1{ 2.5 };
			const UnsigFix right1{ 1 };
			UnsigFix result1 = left1 - right1;
			REQUIRE(result1 == 1.5);

			const UnsigFix left2{ 1 };
			const UnsigFix right2{ 2 };
			UnsigFix result2 = left2 - right2;
			REQUIRE(result2 > 1);
		}
	}



	TEST_CASE("FixedPoint::operator*=", "[FixedPoint]")
	{
		SECTION("Signed")
		{
			SigFix left1{ 4 };
			const SigFix right1{ 2.5 };
			left1 *= right1;
			REQUIRE(left1 == 10);

			SigFix left2{ 4 };
			const SigFix right2{ -2.5 };
			left2 *= right2;
			REQUIRE(left2 == -10);

			SigFix left3{ -4 };
			const SigFix right3{ 2.5 };
			left3 *= right3;
			REQUIRE(left3 == -10);

			SigFix left4{ -4 };
			const SigFix right4{ -2.5 };
			left4 *= right4;
			REQUIRE(left4 == 10);
		}

		SECTION("Unsigned")
		{
			UnsigFix left1{ 2.5 };
			const UnsigFix right1{ 4 };
			left1 *= right1;
			REQUIRE(left1 == 10);
		}
	}



	TEST_CASE("FixedPoint::operator*", "[FixedPoint]")
	{
		SECTION("Signed")
		{
			const SigFix left1{ 4 };
			const SigFix right1{ 2.5 };
			SigFix result1 = left1 * right1;
			REQUIRE(result1 == 10);

			const SigFix left2{ 4 };
			const SigFix right2{ -2.5 };
			SigFix result2 = left2 * right2;
			REQUIRE(result2 == -10);

			const SigFix left3{ -4 };
			const SigFix right3{ 2.5 };
			SigFix result3 = left3 * right3;
			REQUIRE(result3 == -10);

			const SigFix left4{ -4 };
			const SigFix right4{ -2.5 };
			SigFix result4 = left4 * right4;
			REQUIRE(result4 == 10);
		}

		SECTION("Unsigned")
		{
			const UnsigFix left1{ 2.5 };
			const UnsigFix right1{ 4 };
			UnsigFix result = left1 * right1;
			REQUIRE(result == 10);
		}
	}



	TEST_CASE("FixedPoint::operator/=", "[FixedPoint]")
	{
		SECTION("Signed")
		{
			SigFix left1{ 10 };
			const SigFix right1{ 2.5 };
			left1 /= right1;
			REQUIRE(left1 == 4);

			SigFix left2{ 10 };
			const SigFix right2{ -2.5 };
			left2 /= right2;
			REQUIRE(left2 == -4);

			SigFix left3{ -10 };
			const SigFix right3{ 2.5 };
			left3 /= right3;
			REQUIRE(left3 == -4);

			SigFix left4{ -10 };
			const SigFix right4{ -2.5 };
			left4 /= right4;
			REQUIRE(left4 == 4);
		}

		SECTION("Unsigned")
		{
			UnsigFix left1{ 10 };
			const UnsigFix right1{ 2.5 };
			left1 /= right1;
			REQUIRE(left1 == 4);
		}
	}



	TEST_CASE("FixedPoint::operator/", "[FixedPoint]")
	{
		SECTION("Signed")
		{
			const SigFix left1{ 10 };
			const SigFix right1{ 2.5 };
			SigFix result1 = left1 / right1;
			REQUIRE(result1 == 4);

			const SigFix left2{ 10 };
			const SigFix right2{ -2.5 };
			SigFix result2 = left2 / right2;
			REQUIRE(result2 == -4);

			const SigFix left3{ -10 };
			const SigFix right3{ 2.5 };
			SigFix result3 = left3 / right3;
			REQUIRE(result3 == -4);

			const SigFix left4{ -10 };
			const SigFix right4{ -2.5 };
			SigFix result4 = left4 / right4;
			REQUIRE(result4 == 4);
		}

		SECTION("Unsigned")
		{
			const UnsigFix left1{ 10 };
			const UnsigFix right1{ 2.5 };
			UnsigFix result = left1 / right1;
			REQUIRE(result == 4);
		}
	}



	TEST_CASE("FixedPoint::operator=", "[FixedPoint]")
	{
		SECTION("Attribution with other FixedPoint")
		{
			const SigFix positiveSignedOriginal{ 1.33 };
			const SigFix positiveSignedCopy = positiveSignedOriginal;  // NOLINT
			REQUIRE(positiveSignedCopy == 1.33);

			const SigFix negativeSignedOriginal{ -1.33 };
			SigFix negativeSignedCopy = negativeSignedOriginal;  // NOLINT
			REQUIRE(negativeSignedCopy == -1.33);

			const UnsigFix unsignedOriginal{ 1.33 };
			const UnsigFix unsignedCopy = unsignedOriginal;  // NOLINT
			REQUIRE(unsignedCopy == 1.33);
		}



		SECTION("Attribution with int")
		{
			SigFix sig {100};
			REQUIRE(sig == 100);

			sig = -100;
			REQUIRE(sig == -100);

			UnsigFix unsig {100};
			REQUIRE(unsig == 100);
		}



		SECTION("Attribution with double")
		{
			SigFix sig {100.11};
			REQUIRE(sig == 100.11);

			sig = -100.11;
			REQUIRE(sig == -100.11);

			UnsigFix unsig {100.11};
			REQUIRE(unsig == 100.11);
		}



		SECTION("Attribution with float")
		{
			SigFix sig{100.11f};
			REQUIRE(sig == 100.11f);

			sig = -100.11f;
			REQUIRE(sig == -100.11f);

			UnsigFix unsig{ 100.11f };
			REQUIRE(unsig == 100.11f);
		}
	}



	TEST_CASE("FixedPoint::operator==", "[FixedPoint]")
	{
		SECTION("Signed equals")
		{
			SigFix value{ 10 };

			REQUIRE(value == SigFix{ 10 });
			REQUIRE(value == 10);
			REQUIRE(value == 10u);
			REQUIRE(value == 10.0);
			REQUIRE(value == 10.0f);

			REQUIRE_FALSE(value == SigFix{ -10 });
			REQUIRE_FALSE(value == -10);
			REQUIRE_FALSE(value == 11u);
			REQUIRE_FALSE(value == -10.0);
			REQUIRE_FALSE(value == -10.0f);
		}



		SECTION("Unsigned equals")
		{
			UnsigFix value{ 10 };

			REQUIRE(value == UnsigFix{ 10 });
			REQUIRE(value == 10);
			REQUIRE(value == 10u);
			REQUIRE(value == 10.0);
			REQUIRE(value == 10.0f);

			REQUIRE_FALSE(value == UnsigFix{ 100 });
			REQUIRE_FALSE(value == 100);
			REQUIRE_FALSE(value == 100u);
			REQUIRE_FALSE(value == 100.0);
			REQUIRE_FALSE(value == 100.0f);
		}
	}



	TEST_CASE("FixedPoint::operator!=", "[FixedPoint]")
	{
		SECTION("Signed diff")
		{
			SigFix value{ 10 };

			REQUIRE(value != SigFix{ 11 });
			REQUIRE(value != 11);
			REQUIRE(value != 11u);
			REQUIRE(value != 11.0);
			REQUIRE(value != 11.0f);

			REQUIRE_FALSE(value != SigFix{ 10 });
			REQUIRE_FALSE(value != 10);
			REQUIRE_FALSE(value != 10u);
			REQUIRE_FALSE(value != 10.0);
			REQUIRE_FALSE(value != 10.0f);
		}



		SECTION("Unsigned diff")
		{
			UnsigFix value{ 10 };

			REQUIRE(value != UnsigFix{ 11 });
			REQUIRE(value != 11);
			REQUIRE(value != 11u);
			REQUIRE(value != 11.0);
			REQUIRE(value != 11.0f);

			REQUIRE_FALSE(value != UnsigFix{ 10 });
			REQUIRE_FALSE(value != 10);
			REQUIRE_FALSE(value != 10u);
			REQUIRE_FALSE(value != 10.0);
			REQUIRE_FALSE(value != 10.0f);
		}
	}



	TEST_CASE("FixedPoint::operator>=", "[FixedPoint]")
	{
		SECTION("Signed greater or equal")
		{
			SigFix value{ -10 };

			REQUIRE(value >= SigFix{ -10 });
			REQUIRE(value >= SigFix{ -11 });
			REQUIRE(value >= -10);
			REQUIRE(value >= -11);
			REQUIRE(SigFix{ 11 } >= 10u);
			REQUIRE(SigFix{ 11 } >= 11u);
			REQUIRE(value >= -10.0);
			REQUIRE(value >= -11.0);
			REQUIRE(value >= -10.0f);
			REQUIRE(value >= -11.0f);

			REQUIRE_FALSE(value >= SigFix{ -9.1 });
			REQUIRE_FALSE(value >= -9.1);
			REQUIRE_FALSE(value >= 12u);
			REQUIRE_FALSE(value >= 11.1);
			REQUIRE_FALSE(value >= 11.1f);
		}



		SECTION("Unsigned greater or equal")
		{
			UnsigFix value{ 11 };

			REQUIRE(value >= UnsigFix{ 10 });
			REQUIRE(value >= UnsigFix{ 11 });
			REQUIRE(value >= 10);
			REQUIRE(value >= 11);
			REQUIRE(value >= 10u);
			REQUIRE(value >= 11u);
			REQUIRE(value >= 10.0);
			REQUIRE(value >= 11.0);
			REQUIRE(value >= 10.0f);
			REQUIRE(value >= 11.0f);

			REQUIRE_FALSE(value >= UnsigFix{ 12 });
			REQUIRE_FALSE(value >= 12);
			REQUIRE_FALSE(value >= 12u);
			REQUIRE_FALSE(value >= 11.1);
			REQUIRE_FALSE(value >= 11.1f);
		}
	}



	TEST_CASE("FixedPoint::operator>", "[FixedPoint]")
	{
		SECTION("Signed greater than")
		{
			SigFix value{ -10 };

			REQUIRE(value > SigFix{ -11 });
			REQUIRE(value > -11);
			REQUIRE(SigFix{ 11 } > 10u);
			REQUIRE(value > -11.0);
			REQUIRE(value > -11.0f);

			REQUIRE_FALSE(value > SigFix{ -9.1 });
			REQUIRE_FALSE(value > SigFix{ -10.0 });
			REQUIRE_FALSE(value > -9);
			REQUIRE_FALSE(value > -10);
			REQUIRE_FALSE(value > 12u);
			REQUIRE_FALSE(SigFix{ 12 } > 12u);
			REQUIRE_FALSE(value > 11.1);
			REQUIRE_FALSE(value > -10.0);
			REQUIRE_FALSE(value > -10.0);
			REQUIRE_FALSE(value > 11.1f);
		}



		SECTION("Unsigned greater than")
		{
			UnsigFix value{ 11 };

			REQUIRE(value > UnsigFix{ 10 });
			REQUIRE(value > 10);
			REQUIRE(value > 10u);
			REQUIRE(value > 10.0);
			REQUIRE(value > 10.0f);

			REQUIRE_FALSE(value > UnsigFix{ 12 });
			REQUIRE_FALSE(value > UnsigFix{ 11 });
			REQUIRE_FALSE(value > 12);
			REQUIRE_FALSE(value > 11);
			REQUIRE_FALSE(value > 12u);
			REQUIRE_FALSE(value > 11u);
			REQUIRE_FALSE(value > 11.1);
			REQUIRE_FALSE(value > 11.0);
			REQUIRE_FALSE(value > 11.1f);
			REQUIRE_FALSE(value > 11.0f);
		}
	}



	TEST_CASE("FixedPoint::operator<=", "[FixedPoint]")
	{
		SECTION("Signed less or equal")
		{
			SigFix value{ -10 };

			REQUIRE(value <= SigFix{ -10 });
			REQUIRE(value <= SigFix{ -9 });
			REQUIRE(value <= -10);
			REQUIRE(value <= -9);
			REQUIRE(SigFix{ 10 } <= 10u);
			REQUIRE(value <= 10u);
			REQUIRE(value <= -10.0);
			REQUIRE(value <= -9.0);
			REQUIRE(value <= -10.0f);
			REQUIRE(value <= -9.0f);

			REQUIRE_FALSE(value <= SigFix{ -11 });
			REQUIRE_FALSE(value <= -11);
			REQUIRE_FALSE(SigFix{ 10 } <= SigFix{ 9 });
			REQUIRE_FALSE(value <= -11.0);
			REQUIRE_FALSE(value <= -11.0f);
		}



		SECTION("Unsigned less or equal")
		{
			UnsigFix value{ 9 };

			REQUIRE(value <= UnsigFix{ 10 });
			REQUIRE(value <= UnsigFix{ 9 });
			REQUIRE(value <= 10);
			REQUIRE(value <= 9);
			REQUIRE(value <= 10u);
			REQUIRE(value <= 9u);
			REQUIRE(value <= 10.0);
			REQUIRE(value <= 9.0);
			REQUIRE(value <= 10.0f);
			REQUIRE(value <= 9.0f);

			REQUIRE_FALSE(value <= UnsigFix{ 8 });
			REQUIRE_FALSE(value <= 8);
			REQUIRE_FALSE(value <= 8u);
			REQUIRE_FALSE(value <= 8.0);
			REQUIRE_FALSE(value <= 8.0f);
		}
	}



	TEST_CASE("FixedPoint::operator<", "[FixedPoint]")
	{
		SECTION("Signed less")
		{
			SigFix value{ -10 };

			REQUIRE(value < SigFix{ -9 });
			REQUIRE(value < -9);
			REQUIRE(SigFix{ 9 } < 10u);
			REQUIRE(value < 10u);
			REQUIRE(value < -9.0);
			REQUIRE(value < -9.0f);

			REQUIRE_FALSE(value < SigFix{ -11 });
			REQUIRE_FALSE(value < SigFix{ -10 });
			REQUIRE_FALSE(value < -11);
			REQUIRE_FALSE(value < -10);
			REQUIRE_FALSE(SigFix{ 10 } < SigFix{ 9 });
			REQUIRE_FALSE(SigFix{ 10 } < SigFix{ 10 });
			REQUIRE_FALSE(value < -11.0);
			REQUIRE_FALSE(value < -10.0);
			REQUIRE_FALSE(value < -11.0f);
			REQUIRE_FALSE(value < -10.0f);
		}



		SECTION("Unsigned less")
		{
			UnsigFix value{ 9 };

			REQUIRE(value < UnsigFix{ 10 });
			REQUIRE(value < 10);
			REQUIRE(value < 10u);
			REQUIRE(value < 10.0);
			REQUIRE(value < 10.0f);

			REQUIRE_FALSE(value < UnsigFix{ 8 });
			REQUIRE_FALSE(value < UnsigFix{ 9 });
			REQUIRE_FALSE(value < 8);
			REQUIRE_FALSE(value < 9);
			REQUIRE_FALSE(value < 8u);
			REQUIRE_FALSE(value < 9u);
			REQUIRE_FALSE(value < 8.0);
			REQUIRE_FALSE(value < 9.0);
			REQUIRE_FALSE(value < 8.0f);
			REQUIRE_FALSE(value < 9.0f);
		}
	}



	TEST_CASE("FixedPoint::ToInt", "[FixedPoint]")
	{
		const SigFix signedPositive{ 1.25 };
		REQUIRE(signedPositive.ToInt() == 1);

		const SigFix signedNegative{ -1.25 };
		REQUIRE(signedNegative.ToInt() == -2);

		const UnsigFix unsignedValue{ 1.25 };
		REQUIRE(unsignedValue.ToInt() == 1);
	}



	TEST_CASE("FixedPoint::ToFloat", "[FixedPoint]")
	{
		const SigFix signedPositive{ 1.25 };
		REQUIRE(signedPositive.ToFloat() == 1.25f);

		const SigFix signedNegative{ -1.25 };
		REQUIRE(signedNegative.ToFloat() == -1.25f);

		const UnsigFix unsignedValue{ 1.25 };
		REQUIRE(unsignedValue.ToFloat() == 1.25f);
	}



	TEST_CASE("FixedPoint::ToDouble", "[FixedPoint]")
	{
		const SigFix signedPositive{ 1.25 };
		REQUIRE(signedPositive.ToDouble() == 1.25);

		const SigFix signedNegative{ -1.25 };
		REQUIRE(signedNegative.ToDouble() == -1.25);

		const UnsigFix unsignedValue{ 1.25 };
		REQUIRE(unsignedValue.ToDouble() == 1.25);
	}



	TEST_CASE("int * FixedPoint", "[FixedPoint]")
	{
		SigFix value{ 2 };
		REQUIRE((3 * value) == 6);

		UnsigFix unsigValue{ 2 };
		REQUIRE((3 * unsigValue) == 6);
	}



	TEST_CASE("int + FixedPoint", "[FixedPoint]")
	{
		SigFix value{ 2 };
		REQUIRE((10 + value) == 12);

		UnsigFix unsigValue{ 2 };
		REQUIRE((10 + unsigValue) == 12);
	}



	TEST_CASE("int - FixedPoint", "[FixedPoint]")
	{
		SigFix value{ 2 };
		REQUIRE((10 - value) == 8);

		UnsigFix unsigValue{ 2 };
		REQUIRE((10 - unsigValue) == 8);
	}



	TEST_CASE("int / FixedPoint", "[FixedPoint]")
	{
		SigFix value{ 2 };
		REQUIRE((10 / value) == 5);

		UnsigFix unsigValue{ 2 };
		REQUIRE((10 / unsigValue) == 5);
	}



	TEST_CASE("FixedPoint / int", "[FixedPoint]")
	{
		SigFix value{ 3.5 };
		REQUIRE((value / 2) == 1.75);

		UnsigFix unsigValue{ 3.5 };
		REQUIRE((unsigValue / 2) == 1.75);
	}



	TEST_CASE("Sqrt on FixedPoint", "[FixedPoint]")
	{
		REQUIRE(sqrt(SigFix{ 9 }) == 3.0);
		REQUIRE(sqrt(UnsigFix{ 16 }) == 4.0f);
	}
}

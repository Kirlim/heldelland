
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"



#include<memory>

#include<Nest/Exceptions/LogicErrorException.h>

#include<Nest/IComponentsFactory.h>
#include<Nest/ArrayStoringFactory.h>
#include<Nest/SimpleStoringFactoryWrap.h>
#include<Nest/Transform.h>
#include<Nest/IEntity.h>
#include<Nest/EntitiesManager.h>
#include<Nest/EntitiesAndComponentsRemap.h>
#include<Nest/World.h>
using namespace Nest;

#include<Game/WorldFactory.h>
using namespace Game;



namespace EntitiesAndComponentsRemapTests
{
	std::unique_ptr<ComponentTypeMap> componentTypeMap;
	ComponentTypeId transformTypeId = 0;
	ComponentTypeId meshFilterTypeId = 0;



	std::unique_ptr<World> InstantiateWorld()
	{
		WorldFactory worldFactory;
		auto world = worldFactory.InstantiateWorld();


		if (ComponentTypeMap::GetInstance() == nullptr)
		{
			componentTypeMap = std::make_unique<ComponentTypeMap>();
		}
		transformTypeId = componentTypeMap->GetTypeId<Transform>(); // TODO : maybe ComponentTypeMap should not be a singleton?


		auto transformFactory = std::make_unique<ArrayStoringFactory<Transform>>();
		auto transformFactoryWrap = std::make_unique<SimpleStoringFactoryWrap<Transform>>(transformTypeId, Transform::GetTypeName(), std::move(transformFactory));


		auto componentsFactory = world->GetComponentsFactory();

		const auto transformComponentType = transformFactoryWrap->GetTypeId();
		const auto transformComponentName = transformFactoryWrap->GetTypeName();
		componentsFactory->RegisterFactory(transformComponentType, transformComponentName, std::move(transformFactoryWrap));

		return world;
	}



	TEST_CASE("EntitiesAndComponentsRemap::RegisterEntity", "[EntitiesAndComponentsRemap]")
	{
		EntitiesAndComponentsRemap remap{};



		SECTION("Registering an unregistered id")
		{
			REQUIRE_FALSE(remap.IsEntityRegistered(EntityId{ 1 }));
			REQUIRE_FALSE(remap.IsEntityRegistered(EntityId{ 2 }));

			remap.RegisterEntity(EntityId{ 1 });
			remap.RegisterEntity(EntityId{ 2 });

			REQUIRE(remap.IsEntityRegistered(EntityId{ 1 }));
			REQUIRE(remap.IsEntityRegistered(EntityId{ 2 }));
		}



		SECTION("Registering already registered id throws exception")
		{
			remap.RegisterEntity(EntityId{ 1 });
			REQUIRE_THROWS_AS(remap.RegisterEntity(EntityId{ 1 }), LogicErrorException);
		}
	}



	TEST_CASE("EntitiesAndComponentsRemap::UnregisterEntity", "[EntitiesAndComponentsRemap]")
	{
		EntitiesAndComponentsRemap remap{};



		SECTION("Unregistering a registered id")
		{
			remap.RegisterEntity(EntityId{ 1 });
			REQUIRE(remap.IsEntityRegistered(EntityId{ 1 }));

			remap.UnregisterEntity(EntityId{ 1 });
			REQUIRE_FALSE(remap.IsEntityRegistered(EntityId{ 1 }));
		}



		SECTION("Unregistering an not registered id throws exception")
		{
			REQUIRE_THROWS_AS(remap.UnregisterEntity(EntityId{ 1 }), LogicErrorException);

			remap.RegisterEntity(EntityId{ 1 });
			remap.UnregisterEntity(EntityId{ 1 });
			REQUIRE_THROWS_AS(remap.UnregisterEntity(EntityId{ 1 }), LogicErrorException);
		}
	}



	TEST_CASE("EntitiesAndComponentsRemap::RegisterComponent", "[EntitiesAndComponentsRemap]")
	{
		EntitiesAndComponentsRemap remap{};



		SECTION("Registering an unregistered id")
		{
			REQUIRE_FALSE(remap.IsComponentRegistered(ComponentId{ 1 }));
			REQUIRE_FALSE(remap.IsComponentRegistered(ComponentId{ 2 }));

			remap.RegisterComponent(ComponentId{ 1 });
			remap.RegisterComponent(ComponentId{ 2 });

			REQUIRE(remap.IsComponentRegistered(ComponentId{ 1 }));
			REQUIRE(remap.IsComponentRegistered(ComponentId{ 2 }));
		}



		SECTION("Registering already registered id throws exception")
		{
			remap.RegisterComponent(ComponentId{ 1 });
			REQUIRE_THROWS_AS(remap.RegisterComponent(ComponentId{ 1 }), LogicErrorException);
		}
	}



	TEST_CASE("EntitiesAndComponentsRemap::UnregisterComponent", "[EntitiesAndComponentsRemap]")
	{
		EntitiesAndComponentsRemap remap{};



		SECTION("Unregistering an registered id")
		{
			remap.RegisterComponent(ComponentId{ 1 });
			REQUIRE(remap.IsComponentRegistered(ComponentId{ 1 }));

			remap.UnregisterComponent(ComponentId{ 1 });
			REQUIRE_FALSE(remap.IsComponentRegistered(ComponentId{ 1 }));
		}



		SECTION("Unregistering a not registered id throws exceptions")
		{
			REQUIRE_THROWS_AS(remap.UnregisterComponent(ComponentId{ 1 }), LogicErrorException);

			remap.RegisterComponent(ComponentId{ 1 });
			remap.UnregisterComponent(ComponentId{ 1 });
			REQUIRE_THROWS_AS(remap.UnregisterComponent(ComponentId{ 1 }), LogicErrorException);
		}
	}



	TEST_CASE("EntitiesAndComponentsRemap::MapEntity", "[EntitiesAndComponentsRemap]")
	{
		const auto world = InstantiateWorld();
		auto entitiesManager = world->GetEntitiesManager();
		EntitiesAndComponentsRemap remap;



		SECTION("Remapping an entity")
		{
			auto entity = entitiesManager->Instantiate().lock();

			remap.RegisterEntity(entity->Id());
			REQUIRE_FALSE(remap.IsEntityMapped(entity->Id()));

			const auto newEntity = entitiesManager->Instantiate();
			remap.MapEntity(entity->Id(), newEntity);

			REQUIRE(remap.IsEntityMapped(entity->Id()));
			REQUIRE(remap.GetMappedEntity(entity->Id()).lock()->Id() == newEntity.lock()->Id());

			const std::weak_ptr<IEntity> nullEntity;
			remap.MapEntity(entity->Id(), nullEntity);
			REQUIRE(remap.GetMappedEntity(entity->Id()).expired());
		}



		SECTION("Remapping an unregistered entity id throws LogicErrorException")
		{
			const auto entity = entitiesManager->Instantiate().lock();
			const auto newEntity = entitiesManager->Instantiate();

			REQUIRE_THROWS_AS(remap.IsEntityMapped(entity->Id()), LogicErrorException);
			REQUIRE_THROWS_AS(remap.MapEntity(entity->Id(), newEntity), LogicErrorException);
			REQUIRE_THROWS_AS(remap.GetMappedEntity(entity->Id()), LogicErrorException);
		}
	}



	TEST_CASE("EntitiesAndComponentsRemap::MapComponent", "[EntitiesAndComponentsRemap]")
	{
		const auto world = InstantiateWorld();
		auto entitiesManager = world->GetEntitiesManager();
		EntitiesAndComponentsRemap remap;



		SECTION("Remapping a component")
		{
			auto entity = entitiesManager->Instantiate().lock();
			auto component = entity->AddComponent<Transform>();

			remap.RegisterComponent(component->GetId());
			REQUIRE_FALSE(remap.IsComponentMapped(component->GetId()));

			auto newEntity = entitiesManager->Instantiate().lock();
			auto newComponent = newEntity->AddComponent<Transform>();
			remap.MapComponent(component->GetId(), newComponent);

			REQUIRE(remap.IsComponentMapped(component->GetId()));
			auto recovered = dynamic_cast<ComponentReference<Transform>*>(remap.GetMappedComponent(component->GetId()));
			REQUIRE(*recovered == newComponent);
		}



		SECTION("Remapping an unregistered component id throws LogicErrorException")
		{
			auto entity = entitiesManager->Instantiate().lock();
			auto component = entity->AddComponent<Transform>();

			REQUIRE_THROWS_AS(remap.IsComponentMapped(component->GetId()), LogicErrorException);
			REQUIRE_THROWS_AS(remap.MapComponent(component->GetId(), component), LogicErrorException);
			REQUIRE_THROWS_AS(remap.GetMappedComponent(component->GetId()), LogicErrorException);
		}
	}



	TEST_CASE("EntitiesAndComponentsRemap::ClearMappings", "[EntitiesAndComponentsRemap]")
	{
		const auto world = InstantiateWorld();
		auto entitiesManager = world->GetEntitiesManager();
		EntitiesAndComponentsRemap remap;

		auto entity = entitiesManager->Instantiate().lock();
		auto component = entity->AddComponent<Transform>();

		auto targetEntity = entitiesManager->Instantiate().lock();
		auto targetComponent = targetEntity->AddComponent<Transform>();

		remap.RegisterEntity(entity->Id());
		remap.RegisterComponent(component->GetId());

		remap.MapEntity(entity->Id(), targetEntity);
		remap.MapComponent(component->GetId(), targetComponent);

		REQUIRE(remap.IsEntityMapped(entity->Id()));
		REQUIRE(remap.IsComponentMapped(component->GetId()));

		remap.ClearMappings();
		REQUIRE_FALSE(remap.IsEntityMapped(entity->Id()));
		REQUIRE_FALSE(remap.IsComponentMapped(component->GetId()));
	}
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<Nest/Exceptions/NullPointerException.h>
#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/World.h>
#include<Nest/WorldsManager.h>
using namespace Nest;

#include<Game/WorldFactory.h>
using namespace Game;



namespace WorldsManagerTests
{
	TEST_CASE("Adding Worlds", "[WorldsManager]")
	{
		WorldFactory worldFactory;
		WorldsManager worldsManager;

		SECTION("Adding a single world")
		{
			auto world = worldFactory.InstantiateWorld();
			const auto worldAddress = world.get();
			const WorldId id{ 0 };

			worldsManager.AddWorld(id, world);

			REQUIRE(world == nullptr);
			REQUIRE(worldsManager.GetWorld(id) == worldAddress);
		}

		SECTION("Adding two worlds")
		{
			auto world1 = worldFactory.InstantiateWorld();
			const auto worldAddress1 = world1.get();
			const WorldId id1{ 0 };

			auto world2 = worldFactory.InstantiateWorld();
			const auto worldAddress2 = world2.get();
			const WorldId id2{ 1 };

			worldsManager.AddWorld(id1, world1);
			worldsManager.AddWorld(id2, world2);

			REQUIRE(world1 == nullptr);
			REQUIRE(world2 == nullptr);
			REQUIRE(worldsManager.GetWorld(id1) == worldAddress1);
			REQUIRE(worldsManager.GetWorld(id2) == worldAddress2);
		}

		SECTION("Adding a null world throws NullPointerException")
		{
			std::unique_ptr<World> world;
			const WorldId id{ 0 };

			REQUIRE_THROWS_AS(worldsManager.AddWorld(id, world), NullPointerException);
			REQUIRE(worldsManager.GetWorld(id) == nullptr);
		}

		SECTION("Adding an already added id throws LogicErrorException and does not move the world being added")
		{
			auto world = worldFactory.InstantiateWorld();
			const WorldId id{ 0 };

			auto worldToFail = worldFactory.InstantiateWorld();
			const auto worldToFailId{ id };

			worldsManager.AddWorld(id, world);

			REQUIRE_THROWS_AS(worldsManager.AddWorld(worldToFailId, worldToFail), LogicErrorException);
			REQUIRE(world == nullptr);
			REQUIRE(worldToFail != nullptr);
		}
	}

	TEST_CASE("Removing worlds", "[WorldsManager]")
	{
		WorldFactory worldFactory;
		WorldsManager worldsManager;
		
		SECTION("Removing an added world")
		{
			auto world = worldFactory.InstantiateWorld();
			const WorldId id{ 0 };
			worldsManager.AddWorld(id, world);

			REQUIRE_NOTHROW(worldsManager.RemoveWorld(id));
			REQUIRE(worldsManager.GetWorld(id) == nullptr);
		}

		SECTION("Removing two added worlds")
		{
			auto world1 = worldFactory.InstantiateWorld();
			const WorldId id1{ 0 };

			auto world2 = worldFactory.InstantiateWorld();
			const WorldId id2{ 1 };

			worldsManager.AddWorld(id1, world1);
			worldsManager.AddWorld(id2, world2);

			REQUIRE_NOTHROW(worldsManager.RemoveWorld(id2));
			REQUIRE_NOTHROW(worldsManager.RemoveWorld(id1));
			REQUIRE(worldsManager.GetWorld(id2) == nullptr);
			REQUIRE(worldsManager.GetWorld(id1) == nullptr);
		}

		SECTION("Removing id that is not registered throws LogicErrorException")
		{
			auto world1 = worldFactory.InstantiateWorld();
			const WorldId id1{ 0 };
			worldsManager.AddWorld(id1, world1);

			REQUIRE_THROWS_AS(worldsManager.RemoveWorld(WorldId{100}), LogicErrorException);
		}
	}
}
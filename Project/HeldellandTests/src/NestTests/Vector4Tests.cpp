
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<Nest/Vector4.h>
using namespace Nest;



namespace Vector4Tests
{
	TEST_CASE("Vector4::Vector4()", "[Vector4]")
	{
		Vector4<float> v{};

		REQUIRE(v.x() == 0.0f);
		REQUIRE(v.y() == 0.0f);
		REQUIRE(v.z() == 0.0f);
		REQUIRE(v.w() == 0.0f);
	}



	TEST_CASE("Vector4::Vector4(const NumberType[])", "[Vector4]")
	{
		float values[4]{ 1.0f, 2.0f, 3.0f, 4.0f };
		Vector4<float> v{ values };

		REQUIRE(v.x() == 1.0f);
		REQUIRE(v.y() == 2.0f);
		REQUIRE(v.z() == 3.0f);
		REQUIRE(v.w() == 4.0f);
	}



	TEST_CASE("Vector4::Vector4(const NumberType, const NumberType, const NumberType, const NumberType)", "[Vector4]")
	{
		Vector4<float> v{ 1.0f, 2.0f, 3.0f, 4.0f };

		REQUIRE(v.x() == 1.0f);
		REQUIRE(v.y() == 2.0f);
		REQUIRE(v.z() == 3.0f);
		REQUIRE(v.w() == 4.0f);
	}



	TEST_CASE("Vector4::Vector4(const Vector4&)", "[Vector4]")
	{
		Vector4<float> originalVector{ 1.0f, 2.0f, 3.0f, 4.0f };
		Vector4<float> copy{ originalVector };

		REQUIRE(copy.x() == 1.0f);
		REQUIRE(copy.y() == 2.0f);
		REQUIRE(copy.z() == 3.0f);
		REQUIRE(copy.w() == 4.0f);
	}



	TEST_CASE("Vector4::Set(const NumberType[4])", "[Vector4]")
	{
		Vector4<float> v{};

		float values[4]{ 1.0f, 2.0f, 3.0f, 4.0f };
		v.Set(values);

		REQUIRE(v.x() == 1.0f);
		REQUIRE(v.y() == 2.0f);
		REQUIRE(v.z() == 3.0f);
		REQUIRE(v.w() == 4.0f);
	}



	TEST_CASE("Vector4::Set(const NumberType, const NumberType, const NumberType, const NumberType)", "[Vector4]")
	{
		Vector4<float> v{};
		v.Set(1.0f, 2.0f, 3.0f, 4.0f);

		REQUIRE(v.x() == 1.0f);
		REQUIRE(v.y() == 2.0f);
		REQUIRE(v.z() == 3.0f);
		REQUIRE(v.w() == 4.0f);
	}



	TEST_CASE("Vector4::operator +=(const Vector4&)", "[Vector4]")
	{
		Vector4<float> v{ 1.0f, 2.0f, 3.0f, 4.0f };
		Vector4<float> adding{ 5.0f, 6.0f, 7.0f, 8.0f };
		v += adding;

		REQUIRE(v.x() == 6.0f);
		REQUIRE(v.y() == 8.0f);
		REQUIRE(v.z() == 10.0f);
		REQUIRE(v.w() == 12.0f);
	}



	TEST_CASE("Vector4::operator -=(const Vector4)", "[Vector4]")
	{
		Vector4<float> v{ 8.0f, 7.0f, 6.0f, 5.0f };
		Vector4<float> subtracting{ 1.0f, 2.0f, 3.0f, 4.0f };
		v -= subtracting;

		REQUIRE(v.x() == 7.0f);
		REQUIRE(v.y() == 5.0f);
		REQUIRE(v.z() == 3.0f);
		REQUIRE(v.w() == 1.0f);
	}



	TEST_CASE("Vector4::operator +(const Vector4&)", "[Vector4]")
	{
		Vector4<float> left{ 1.0f, 2.0f, 3.0f, 4.0f };
		Vector4<float> right{ 5.0f, 6.0f, 7.0f, 8.0f };
		Vector4<float> result = left + right;

		REQUIRE(result.x() == 6.0f);
		REQUIRE(result.y() == 8.0f);
		REQUIRE(result.z() == 10.0f);
		REQUIRE(result.w() == 12.0f);
	}



	TEST_CASE("Vector4::operator -(const Vector4&)", "[Vector4]")
	{
		Vector4<float> left{ 8.0f, 7.0f, 6.0f, 5.0f };
		Vector4<float> right{ 1.0f, 2.0f, 3.0f, 4.0f };
		Vector4<float> result = left - right;

		REQUIRE(result.x() == 7.0f);
		REQUIRE(result.y() == 5.0f);
		REQUIRE(result.z() == 3.0f);
		REQUIRE(result.w() == 1.0f);
	}



	TEST_CASE("Vector4::operator *=(const NumberType&)", "[Vector4]")
	{
		Vector4<float> v{ 1.0f, 2.0f, 3.0f, 4.0f };
		float multiply = 2.0f;
		v *= multiply;

		REQUIRE(v.x() == 2.0f);
		REQUIRE(v.y() == 4.0f);
		REQUIRE(v.z() == 6.0f);
		REQUIRE(v.w() == 8.0f);
	}



	TEST_CASE("Vector4::operator /=(const NumberType&)", "[Vector4]")
	{
		Vector4<float> v{ 16.0f, 8.0f, 4.0f, 2.0f };
		float divide = 2.0f;
		v /= divide;

		REQUIRE(v.x() == 8.0f);
		REQUIRE(v.y() == 4.0f);
		REQUIRE(v.z() == 2.0f);
		REQUIRE(v.w() == 1.0f);
	}



	TEST_CASE("Vector4::operator *(const NumberType&)", "[Vector4]")
	{
		Vector4<float> v{ 1.0f, 2.0f, 3.0f, 4.0f };
		float multiply = 2.0f;
		auto result = v * multiply;

		REQUIRE(result.x() == 2.0f);
		REQUIRE(result.y() == 4.0f);
		REQUIRE(result.z() == 6.0f);
		REQUIRE(result.w() == 8.0f);
	}



	TEST_CASE("Vector4::operator /(const NumberType&)", "[Vector4]")
	{
		Vector4<float> v{ 16.0f, 8.0f, 4.0f, 2.0f };
		float divide = 2.0f;
		auto result = v / divide;

		REQUIRE(result.x() == 8.0f);
		REQUIRE(result.y() == 4.0f);
		REQUIRE(result.z() == 2.0f);
		REQUIRE(result.w() == 1.0f);
	}



	TEST_CASE("Vector4::Dot(const Vector4&)", "[Vector4]")
	{
		Vector4<float> vector1{ 1.0f, 2.0f, 3.0f, 4.0f };
		Vector4<float> vector2{ 5.0f, 6.0f, 7.0f, 8.0f };
		auto dot = vector1.Dot(vector2);

		REQUIRE(dot == 70.0f);
	}



	TEST_CASE("Dot(const Vector4&, const Vector4&)", "[Vector4]")
	{
		Vector4<float> vector1{ 1.0f, 2.0f, 3.0f, 4.0f };
		Vector4<float> vector2{ 5.0f, 6.0f, 7.0f, 8.0f };
		auto dot = Vector4<float>::Dot(vector1, vector2);

		REQUIRE(dot == 70.0f);
	}



	TEST_CASE("Vector4 inline elements setters", "[Vector4]")
	{
		Vector4<float> v{};
		v.x(1.0f);
		v.y(2.0f);
		v.z(3.0f);
		v.w(4.0f);

		REQUIRE(v.x() == 1.0f);
		REQUIRE(v.y() == 2.0f);
		REQUIRE(v.z() == 3.0f);
		REQUIRE(v.w() == 4.0f);
	}
}
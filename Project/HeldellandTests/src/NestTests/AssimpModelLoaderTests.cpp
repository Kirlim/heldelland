
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<Nest/Exceptions/FailedToLoadException.h>
#include<Nest/ShaderProgram.h>
#include<Nest/RenderingModule.h>
#include<Nest/ResourcesManager.h>
#include<Nest/MeshesFactory.h>
#include<Nest/AssimpModelLoader.h>
using namespace Nest;

#include "../Mocks/MeshMock.h"



namespace AssimpModelLoaderTests
{
	SCENARIO("Loading models with Assimp Model Loader", "[AssimpModelLoader]")
	{
		ResourcesManager resourcesManager;
		auto gameRenderer = GameRenderer::Test;
		const MeshesFactory meshesFactory{ []() { return std::make_unique<MeshMock>();} };
		AssimpModelLoader modelLoader{ meshesFactory };

		auto shader = std::make_unique<ShaderProgram>(gameRenderer);
		shader->AddTextureName("mainTextureName");
		shader->SetMainTextureName("mainTextureName");
		resourcesManager.AddShaderProgram("gSimpleDiffuse", shader);



		// TODO : need a better way to know if the meshes have been really loaded.

		GIVEN("I want to load an .fbx model file with Assimp")
		{
			WHEN("I try to load a valid .fbx file with all the resources available without name prepending")
			{
				auto pointerToModel = modelLoader.Load(resourcesManager, gameRenderer, "NestTests/Data/Fbx/ValidTestCube/ValidTestCube.fbx", "TestName", AssimpModelLoader::NO_PREPEND);

				THEN("All materials, textures and meshes will be loaded with their given names, and a Model will be returned.")
				{
					REQUIRE(pointerToModel != nullptr);
					REQUIRE(resourcesManager.FetchTexture("ValidTestCubeTexture") != nullptr);
					REQUIRE(resourcesManager.FetchMaterial("TestCubeMaterial") != nullptr);
					//REQUIRE(resourcesManager.FetchMesh("Cube") != nullptr);
					REQUIRE(resourcesManager.FetchModel("TestName") == pointerToModel);
				}
			}

			WHEN("I try to load a valid .fbx with all resources available and texture name prepending")
			{
				auto pointerToModel = modelLoader.Load(resourcesManager, gameRenderer, "NestTests/Data/Fbx/ValidTestCube/ValidTestCube.fbx", "TestName", AssimpModelLoader::TEXTURE_NAME);

				THEN("All resources will be loaded, but the texture will have a prepended name, and a Model will be returned.")
				{
					REQUIRE(pointerToModel != nullptr);
					REQUIRE(resourcesManager.FetchTexture("[TestName]ValidTestCubeTexture") != nullptr);
					REQUIRE(resourcesManager.FetchMaterial("TestCubeMaterial") != nullptr);
					//REQUIRE(resourcesManager.FetchMesh("Cube") != nullptr);
					REQUIRE(resourcesManager.FetchModel("TestName") == pointerToModel);
				}
			}

			WHEN("I try to load a valid .fbx with all resources available and material name prepending")
			{
				auto pointerToModel = modelLoader.Load(resourcesManager, gameRenderer, "NestTests/Data/Fbx/ValidTestCube/ValidTestCube.fbx", "TestName", AssimpModelLoader::MATERIAL_NAME);

				THEN("All resources will be loaded, but the material will have a prepended name, and a Model will be returned.")
				{
					REQUIRE(pointerToModel != nullptr);
					REQUIRE(resourcesManager.FetchTexture("ValidTestCubeTexture") != nullptr);
					REQUIRE(resourcesManager.FetchMaterial("[TestName]TestCubeMaterial") != nullptr);
					//REQUIRE(resourcesManager.FetchMesh("Cube") != nullptr);
					REQUIRE(resourcesManager.FetchModel("TestName") == pointerToModel);
				}
			}

			WHEN("I try to load a valid .fbx with all resources available and mesh name prepending")
			{
				auto pointerToModel = modelLoader.Load(resourcesManager, gameRenderer, "NestTests/Data/Fbx/ValidTestCube/ValidTestCube.fbx", "TestName", AssimpModelLoader::MESH_NAME);

				THEN("All resources will be loaded, but the mesh will have a prepended name, and a Model will be returned.")
				{
					REQUIRE(pointerToModel != nullptr);
					REQUIRE(resourcesManager.FetchTexture("ValidTestCubeTexture") != nullptr);
					REQUIRE(resourcesManager.FetchMaterial("TestCubeMaterial") != nullptr);
					//REQUIRE(resourcesManager.FetchMesh("[TestName]Cube") != nullptr);
					REQUIRE(resourcesManager.FetchModel("TestName") == pointerToModel);
				}
			}

			WHEN("I try to load a valid .fbx with all resources available and all names prepending")
			{
				auto pointerToModel = modelLoader.Load(resourcesManager, gameRenderer, "NestTests/Data/Fbx/ValidTestCube/ValidTestCube.fbx", "TestName", AssimpModelLoader::ALL_NAMES);

				THEN("All resources will be loaded, but the resources will have a prepended name, and a Model will be returned.")
				{
					REQUIRE(pointerToModel != nullptr);
					REQUIRE(resourcesManager.FetchTexture("[TestName]ValidTestCubeTexture") != nullptr);
					REQUIRE(resourcesManager.FetchMaterial("[TestName]TestCubeMaterial") != nullptr);
					//REQUIRE(resourcesManager.FetchMesh("[TestName]Cube") != nullptr);
					REQUIRE(resourcesManager.FetchModel("TestName") == pointerToModel);
				}
			}

			WHEN("I try to load a valid .fbx in which the texture cannot be found")
			{
				REQUIRE_THROWS(modelLoader.Load(resourcesManager, gameRenderer, "NestTests/Data/Fbx/ValidTestCube_MissingTexture/ValidTestCube.fbx", "TestName", AssimpModelLoader::NO_PREPEND));

				THEN("The Load method will throw an exception, and no resources will be loaded.")
				{
					REQUIRE(resourcesManager.FetchTexture("ValidTestCubeTexture") == nullptr);
					REQUIRE(resourcesManager.FetchMaterial("TestCubeMaterial") == nullptr);
					//REQUIRE(resourcesManager.FetchMesh("Cube") == nullptr);
					REQUIRE(resourcesManager.FetchModel("TestName") == nullptr);
				}
			}

			WHEN("I try to load a .fbx with two meshes")
			{
				auto pointerToModel = modelLoader.Load(resourcesManager, gameRenderer, "NestTests/Data/Fbx/TwoMeshesTest/TwoMeshes.fbx", "TestName", AssimpModelLoader::NO_PREPEND);

				THEN("The load method will load all resources.")
				{
					REQUIRE(pointerToModel != nullptr);
					REQUIRE(resourcesManager.FetchTexture("TwoMeshesTexture1") != nullptr);
					REQUIRE(resourcesManager.FetchTexture("TwoMeshesTexture2") != nullptr);
					REQUIRE(resourcesManager.FetchMaterial("CubeMat") != nullptr);
					REQUIRE(resourcesManager.FetchMaterial("IcosphereMat") != nullptr);
					//REQUIRE(resourcesManager.FetchMesh("Cube") != nullptr);
					//REQUIRE(resourcesManager.FetchMesh("Icosphere") != nullptr);
					REQUIRE(resourcesManager.FetchModel("TestName") == pointerToModel);
				}
			}

			WHEN("I try to load a .fbx with two meshes, but one of the textures are missing")
			{
				REQUIRE_THROWS_AS(modelLoader.Load(resourcesManager, gameRenderer, "NestTests/Data/Fbx/TwoMeshesTest_MissingTexture/TwoMeshes.fbx", "TestName", AssimpModelLoader::NO_PREPEND), FailedToLoadException);

				THEN("The load method will fail, and no resources will be loaded.")
				{
					REQUIRE(resourcesManager.FetchTexture("TwoMeshesTexture1") == nullptr);
					REQUIRE(resourcesManager.FetchTexture("TwoMeshesTexture2") == nullptr);
					REQUIRE(resourcesManager.FetchMaterial("CubeMat") == nullptr);
					REQUIRE(resourcesManager.FetchMaterial("IcosphereMat") == nullptr);
					//REQUIRE(resourcesManager.FetchMesh("Cube") == nullptr);
					//REQUIRE(resourcesManager.FetchMesh("Icosphere") == nullptr);
					REQUIRE(resourcesManager.FetchModel("TestName") == nullptr);
				}
			}

			WHEN("I try to load a non-existing .fbx file")
			{
				THEN("The Load method will throw a Failed to Load exception.")
				{
					REQUIRE_THROWS_AS(modelLoader.Load(resourcesManager, gameRenderer, "Huioiboesacikh", "TestName", AssimpModelLoader::NO_PREPEND), FailedToLoadException);
				}
			}

			WHEN("I try to load an empty file path")
			{
				THEN("The Load method will throw a Failed to Load exception.")
				{
					REQUIRE_THROWS_AS(modelLoader.Load(resourcesManager, gameRenderer, "", "TestName", AssimpModelLoader::NO_PREPEND), FailedToLoadException);
				}
			}

			WHEN("I try to load two fbxs with the same name")
			{
				auto pointerToModel = modelLoader.Load(resourcesManager, gameRenderer, "NestTests/Data/Fbx/ValidTestCube/ValidTestCube.fbx", "TestName", AssimpModelLoader::ALL_NAMES);
				REQUIRE_THROWS_AS(modelLoader.Load(resourcesManager, gameRenderer, "NestTests/Data/Fbx/ValidTestCube/ValidTestCube.fbx", "TestName", AssimpModelLoader::NO_PREPEND), FailedToLoadException);

				THEN("The Load method will throw an exception, and no resources will be loaded.")
				{
					REQUIRE(pointerToModel != nullptr);
					REQUIRE(resourcesManager.FetchTexture("[TestName]ValidTestCubeTexture") != nullptr);
					REQUIRE(resourcesManager.FetchMaterial("[TestName]TestCubeMaterial") != nullptr);
					//REQUIRE(resourcesManager.FetchMesh("[TestName]Cube") != nullptr);
					REQUIRE(resourcesManager.FetchModel("TestName") == pointerToModel);

					REQUIRE(resourcesManager.FetchTexture("ValidTestCubeTexture") == nullptr);
					REQUIRE(resourcesManager.FetchMaterial("TestCubeMaterial") == nullptr);
					//REQUIRE(resourcesManager.FetchMesh("Cube") == nullptr);
				}
			}
		}
	}
}

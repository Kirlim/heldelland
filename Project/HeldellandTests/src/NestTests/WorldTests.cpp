
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<Nest/Exceptions/NullPointerException.h>
#include<Nest/ComponentsContainers.h>
#include<Nest/ComponentsFactory.h>
#include<Nest/ResourcesManager.h>
#include<Nest/RenderingManager.h>
#include<Nest/SystemsManager.h>
#include<Nest/World.h>
using namespace Nest;



namespace WorldTests
{
	TEST_CASE("World Constructor", "[World]")
	{
		std::unique_ptr<IComponentsContainers> componentsContainers = std::make_unique<ComponentsContainers>();
		std::unique_ptr<IComponentsFactory> componentsFactory = std::make_unique<ComponentsFactory>();
		std::unique_ptr<IResourcesManager> resourcesManager = std::make_unique<ResourcesManager>();
		auto renderingManager = std::make_unique<RenderingManager>();
		auto systemsManager = std::make_unique<SystemsManager>();

		SECTION("Constructor fails if Components Containers is null, and the other owning pointers are not moved")
		{
			componentsContainers.reset();

			REQUIRE_THROWS_AS(World(componentsContainers, componentsFactory, resourcesManager, renderingManager, systemsManager), NullPointerException);
			REQUIRE(componentsFactory != nullptr);
			REQUIRE(resourcesManager != nullptr);
			REQUIRE(renderingManager != nullptr);
			REQUIRE(systemsManager != nullptr);
		}

		SECTION("Constructor fails if Components Factory is null, and the other owning pointers are not moved")
		{
			componentsFactory.reset();

			REQUIRE_THROWS_AS(World(componentsContainers, componentsFactory, resourcesManager, renderingManager, systemsManager), NullPointerException);
			REQUIRE(componentsContainers != nullptr);
			REQUIRE(resourcesManager != nullptr);
			REQUIRE(renderingManager != nullptr);
			REQUIRE(systemsManager != nullptr);
		}

		SECTION("Constructor fails if Resources Manager is null, and the other owning pointers are not moved")
		{
			resourcesManager.reset();

			REQUIRE_THROWS_AS(World(componentsContainers, componentsFactory, resourcesManager, renderingManager, systemsManager), NullPointerException);
			REQUIRE(componentsContainers != nullptr);
			REQUIRE(componentsFactory != nullptr);
			REQUIRE(renderingManager != nullptr);
			REQUIRE(systemsManager != nullptr);
		}

		SECTION("Constructor fails if Rendering Manager is null, and the other owning pointers are not moved")
		{
			renderingManager.reset();

			REQUIRE_THROWS_AS(World(componentsContainers, componentsFactory, resourcesManager, renderingManager, systemsManager), NullPointerException);
			REQUIRE(componentsContainers != nullptr);
			REQUIRE(componentsFactory != nullptr);
			REQUIRE(resourcesManager != nullptr);
			REQUIRE(systemsManager != nullptr);
		}


		SECTION("Constructor fails if Systems Manager is null, and the other owning pointers are not moved")
		{
			systemsManager.reset();

			REQUIRE_THROWS_AS(World(componentsContainers, componentsFactory, resourcesManager, renderingManager, systemsManager), NullPointerException);
			REQUIRE(componentsContainers != nullptr);
			REQUIRE(componentsFactory != nullptr);
			REQUIRE(resourcesManager != nullptr);
			REQUIRE(renderingManager != nullptr);
		}

		SECTION("If constructor succeeds, then all owning pointers will be moved")
		{
			REQUIRE_NOTHROW(World(componentsContainers, componentsFactory, resourcesManager, renderingManager, systemsManager));
			REQUIRE(componentsContainers == nullptr);
			REQUIRE(componentsFactory == nullptr);
			REQUIRE(resourcesManager == nullptr);
			REQUIRE(renderingManager == nullptr);
			REQUIRE(systemsManager == nullptr);
		}
	}
}


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<memory>

#include<Nest/Exceptions/LogicErrorException.h>
#include<Nest/ResourcesManager.h>
#include<Nest/RenderingModule.h>
#include<Nest/ModelNode.h>
#include<Nest/Model.h>
#include<Nest/IModelLoader.h>
#include<Nest/IEntityFromModelBuilder.h>
using namespace Nest;



namespace IEntityFromModelBuilderTests
{
	class EntityFromModelBuilderMock : public IEntityFromModelBuilder
	{
	public:
		~EntityFromModelBuilderMock() override = default;

	private:
		IEntity* DoBuildVisualEntity(const ModelNode& modelRootNode) override
		{
			++doBuildVisualEntityCallCount;
			return nullptr;
		}

	public:
		int doBuildVisualEntityCallCount = 0;
	};



	class ModelLoaderMock : public IModelLoader
	{
	public:
		~ModelLoaderMock() override = default;

		Model* Load(
			IResourcesManager& resourcesManager, GameRenderer gameRenderer, const std::string filename,
			const std::string modelName, const unsigned prependNamesFlags
		) override
		{
			auto node = std::make_unique<ModelNode>();
			model = BuildModel(node);
			return model.get();
		}



	public:
		std::unique_ptr<Model> model;
	};



	TEST_CASE("BuildVisualEntity method tests", "[IEntityFromModelBuilder]")
	{
		EntityFromModelBuilderMock entityFromModelBuilderMock;

		SECTION("If model is not loaded, throw LogicErrorException")
		{
			const Model model{};
			REQUIRE_THROWS_AS(entityFromModelBuilderMock.BuildVisualEntity(model), LogicErrorException);
		}

		SECTION("If model is loaded, DoBuildVisualEntity will be called once")
		{
			ResourcesManager resourcesManager{};
			ModelLoaderMock modelLoaderMock{};
			const auto model = modelLoaderMock.Load(resourcesManager, GameRenderer::NotSet, "", "", 0);
			entityFromModelBuilderMock.BuildVisualEntity(*model);

			REQUIRE(entityFromModelBuilderMock.doBuildVisualEntityCallCount == 1);
		}
	}
}
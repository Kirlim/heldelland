
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<cmath>

#include"../Helpers/EntitiesManagerHelper.h"
using namespace Helpers;

#include<Nest/Vector3.h>
#include<Nest/Quaternion.h>
#include<Nest/ComponentTypeMap.h>
#include<Nest/IEntity.h>
#include<Nest/EntitiesManager.h>
#include<Nest/Transform.h>
#include<Nest/EntitiesAndComponentsRemap.h>
#include<Nest/World.h>
using namespace Nest;



namespace TransformTests
{
	TEST_CASE("Transform::CopyFrom", "[Transform]")
	{
		ComponentTypeMap componentTypeMap{ false };
		const auto world = InstantiateWorld();
		RegisterComponentFactory<Transform>(componentTypeMap, *world);

		auto entitiesManager = world->GetEntitiesManager();

		SECTION("Copying from another transform, without parent and without mapping")
		{
			auto sourceEntity = entitiesManager->Instantiate().lock();
			auto sourceTransform = sourceEntity->AddComponent<Transform>();
			Vector3f position = Vector3f{ 1.0f, 2.0f, 3.0f };
			Vector3f scale = Vector3f{ 4.0f, 5.0f, 6.0f };
			Quaternion<float> rotation;
			rotation.SetRotation(1.0f, Vector3f{ 0.0f, 1.0f, 0.0f });

			sourceTransform->SetLocalPosition(position);
			sourceTransform->SetLocalRotation(rotation);
			sourceTransform->SetLocalScale(scale);

			auto otherEntity = entitiesManager->Instantiate().lock();
			auto otherTransform = otherEntity->AddComponent<Transform>();
			otherTransform->CopyFrom(*sourceTransform);

			REQUIRE(otherTransform->GetLocalPosition() == position);
			REQUIRE(otherTransform->GetLocalRotation() == rotation);
			REQUIRE(otherTransform->GetLocalScale() == scale);
			REQUIRE(otherTransform->GetParent() == nullptr);
		}



		SECTION("Copying from another transform, with parent, and without mapping")
		{
			auto parentEntity = entitiesManager->Instantiate().lock();
			auto parentTransform = parentEntity->AddComponent<Transform>();

			auto sourceEntity = entitiesManager->Instantiate().lock();
			auto sourceTransform = sourceEntity->AddComponent<Transform>();
			sourceTransform->SetParent(parentTransform);
			Vector3f position = Vector3f{ 1.0f, 2.0f, 3.0f };
			Vector3f scale = Vector3f{ 4.0f, 5.0f, 6.0f };
			Quaternion<float> rotation;
			rotation.SetRotation(1.0f, Vector3f{ 0.0f, 1.0f, 0.0f });

			sourceTransform->SetLocalPosition(position);
			sourceTransform->SetLocalRotation(rotation);
			sourceTransform->SetLocalScale(scale);

			auto otherEntity = entitiesManager->Instantiate().lock();
			auto otherTransform = otherEntity->AddComponent<Transform>();
			otherTransform->CopyFrom(*sourceTransform);

			REQUIRE(otherTransform->GetLocalPosition() == position);
			REQUIRE(otherTransform->GetLocalRotation() == rotation);
			REQUIRE(otherTransform->GetLocalScale() == scale);
			REQUIRE(otherTransform->GetParent() == parentTransform);
		}



		SECTION("Copying from another transform, with parent, and with mapping")
		{

			auto parentEntity = entitiesManager->Instantiate().lock();
			const auto parentTransform = parentEntity->AddComponent<Transform>();

			auto sourceEntity = entitiesManager->Instantiate().lock();
			auto sourceTransform = sourceEntity->AddComponent<Transform>();
			sourceTransform->SetParent(parentTransform);
			auto position = Vector3f{ 1.0f, 2.0f, 3.0f };
			auto scale = Vector3f{ 4.0f, 5.0f, 6.0f };
			Quaternion<float> rotation;
			rotation.SetRotation(1.0f, Vector3f{ 0.0f, 1.0f, 0.0f });

			sourceTransform->SetLocalPosition(position);
			sourceTransform->SetLocalRotation(rotation);
			sourceTransform->SetLocalScale(scale);

			auto otherEntity = entitiesManager->Instantiate().lock();
			auto otherTransform = otherEntity->AddComponent<Transform>();
			auto otherExpectedParentEntity = entitiesManager->Instantiate().lock();
			auto otherExpectedParentTransform = otherExpectedParentEntity->AddComponent<Transform>();

			EntitiesAndComponentsRemap remap;
			remap.RegisterComponent(parentTransform->GetId());
			remap.MapComponent(parentTransform->GetId(), otherExpectedParentTransform);

			otherTransform->CopyFrom(*sourceTransform, &remap);

			REQUIRE(otherTransform->GetLocalPosition() == position);
			REQUIRE(otherTransform->GetLocalRotation() == rotation);
			REQUIRE(otherTransform->GetLocalScale() == scale);
			REQUIRE(otherTransform->GetParent() == otherExpectedParentTransform);
		}
	}
}


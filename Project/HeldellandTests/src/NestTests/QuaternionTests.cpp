
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<cmath>

#include<Nest/Quaternion.h>
using namespace Nest;



namespace QuaternionTests
{
	bool FloatCompare(float left, float right)
	{
		float compare = left - right;
		if (compare < 0) compare = 0 - compare;
		return(compare < 0.00001f);
	}



	TEST_CASE("Quaternion::FromEuler", "[Quaternion]")
	{
		SECTION("Setting isolated yaw axis, and back")
		{
			auto rotation = Quaternion<float>::FromEuler(30.0f, 0.0f, 0.0f);
			REQUIRE(FloatCompare(rotation.w, 0.965926f));
			REQUIRE(FloatCompare(rotation.x, 0.0f));
			REQUIRE(FloatCompare(rotation.y, 0.258819f));
			REQUIRE(FloatCompare(rotation.z, 0.0f));

			auto angles = rotation.GetEulerYawPitchRollDegrees();
			REQUIRE(FloatCompare(angles.x, 0.0f));
			REQUIRE(FloatCompare(angles.y, 30.0f));
			REQUIRE(FloatCompare(angles.z, 0.0f));
		}

		SECTION("Setting isolated pitch axis, and back")
		{
			auto rotation = Quaternion<float>::FromEuler(0.0f, 30.0f, 0.0f);
			REQUIRE(FloatCompare(rotation.w, 0.965926f));
			REQUIRE(FloatCompare(rotation.x, 0.0f));
			REQUIRE(FloatCompare(rotation.y, 0.0f));
			REQUIRE(FloatCompare(rotation.z, 0.258819f));

			auto angles = rotation.GetEulerYawPitchRollDegrees();
			REQUIRE(FloatCompare(angles.x, 0.0f));
			REQUIRE(FloatCompare(angles.y, 0.0f));
			REQUIRE(FloatCompare(angles.z, 30.0f));
		}

		SECTION("Setting isolated roll axis, and back")
		{
			auto rotation = Quaternion<float>::FromEuler(0.0f, 0.0f, 30.0f);
			REQUIRE(FloatCompare(rotation.w, 0.965926f));
			REQUIRE(FloatCompare(rotation.x, 0.258819f));
			REQUIRE(FloatCompare(rotation.y, 0.0f));
			REQUIRE(FloatCompare(rotation.z, 0.0f));

			auto angles = rotation.GetEulerYawPitchRollDegrees();
			REQUIRE(FloatCompare(angles.x, 30.0f));
			REQUIRE(FloatCompare(angles.y, 00.0f));
			REQUIRE(FloatCompare(angles.z, 0.0f));
		}
	}
}

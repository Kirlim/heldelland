
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"../catch.hpp"

#include<string>

#include<Nest/ModelNode.h>
using namespace Nest;



namespace ModelNodeTests
{
	TEST_CASE("ModelNode::RegisterMeshData", "[ModelNode]")
	{
		ModelNode modelNode;

		SECTION("Registering a mesh name and material name")
		{
			std::string meshName = "meshName";
			std::string materialName = "materialName";

			REQUIRE(modelNode.GetMeshesDataCount() == 0);
			auto index = modelNode.RegisterMeshData(meshName, materialName);

			REQUIRE(modelNode.GetMeshesDataCount() == 1);
			REQUIRE(modelNode.GetMeshName(index) == meshName);
			REQUIRE(modelNode.GetMaterialName(index) == materialName);
		}

		SECTION("Registering multiple meshes and materials names")
		{
			std::string meshName1 = "meshName1", materialName1 = "materialName1";
			std::string meshName2 = "meshName2", materialName2 = "materialName2";
			std::string meshName3 = "meshName3", materialName3 = "materialName3";

			auto index1 = modelNode.RegisterMeshData(meshName1, materialName1);
			auto index2 = modelNode.RegisterMeshData(meshName2, materialName2);
			auto index3 = modelNode.RegisterMeshData(meshName3, materialName3);

			REQUIRE(modelNode.GetMeshesDataCount() == 3);
			REQUIRE((modelNode.GetMeshName(index1) == meshName1 && modelNode.GetMaterialName(index1) == materialName1));
			REQUIRE((modelNode.GetMeshName(index2) == meshName2 && modelNode.GetMaterialName(index2) == materialName2));
			REQUIRE((modelNode.GetMeshName(index3) == meshName3 && modelNode.GetMaterialName(index3) == materialName3));
		}

		SECTION("Registering empty mesh name throws exception")
		{
			REQUIRE_THROWS_AS(modelNode.RegisterMeshData("", "materialName"), std::logic_error);
			REQUIRE(modelNode.GetMeshesDataCount() == 0);
		}

		SECTION("Registering empty materail name throws exception")
		{
			REQUIRE_THROWS_AS(modelNode.RegisterMeshData("meshName", ""), std::logic_error);
			REQUIRE(modelNode.GetMeshesDataCount() == 0);
		}
	}

	TEST_CASE("ModelNode::GetMeshName throws out_of_range with invalid index", "[ModelNode]")
	{
		ModelNode modelNode;
		REQUIRE_THROWS_AS(modelNode.GetMeshName(0), std::out_of_range);
		REQUIRE_THROWS_AS(modelNode.GetMeshName(-1), std::out_of_range);
	}

	TEST_CASE("ModelNode::GetMaterialName throws out_of_range with invalid index", "[ModelNode]")
	{
		ModelNode modelNode;
		REQUIRE_THROWS_AS(modelNode.GetMaterialName(0), std::out_of_range);
		REQUIRE_THROWS_AS(modelNode.GetMaterialName(-1), std::out_of_range);
	}

	TEST_CASE("ModelNode::AddChild", "[ModelNode]")
	{
		ModelNode modelNode;

		SECTION("Adding a child")
		{
			auto childNode = std::make_unique<ModelNode>();
			auto childAddress = childNode.get();

			REQUIRE(modelNode.GetChildCount() == 0);
			auto childIndex = modelNode.AddChild(childNode);

			REQUIRE(modelNode.GetChildCount() == 1);
			REQUIRE(modelNode.GetChild(childIndex) == childAddress);
		}

		SECTION("Adding multiple children")
		{
			auto childNode1 = std::make_unique<ModelNode>();
			auto childNode2 = std::make_unique<ModelNode>();

			auto childAddress1 = childNode1.get();
			auto childAddress2 = childNode2.get();

			auto childIndex1 = modelNode.AddChild(childNode1);
			auto childIndex2 = modelNode.AddChild(childNode2);

			REQUIRE(modelNode.GetChildCount() == 2);
			REQUIRE(modelNode.GetChild(childIndex1) == childAddress1);
			REQUIRE(modelNode.GetChild(childIndex2) == childAddress2);
		}

		SECTION("Adding null child throws exception")
		{
			std::unique_ptr<ModelNode> nullChild;
			REQUIRE_THROWS_AS(modelNode.AddChild(nullChild), std::invalid_argument);
		}
	}

	TEST_CASE("ModelNode::GetChild", "[ModelNode]")
	{
		ModelNode modelNode;
		REQUIRE_THROWS_AS(modelNode.GetChild(0), std::out_of_range);
		REQUIRE_THROWS_AS(modelNode.GetChild(-1), std::out_of_range);
	}
}

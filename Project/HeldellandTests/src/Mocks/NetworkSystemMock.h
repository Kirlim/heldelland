
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef MOCKS__NETWORK_SYSTEM_MOCK_H
#define MOCKS__NETWORK_SYSTEM_MOCK_H

#include<Game/Network/INetworkSystem.h>



class NetworkSystemMock : public Game::INetworkSystem
{
public:
    NetworkSystemMock();
    ~NetworkSystemMock() override;
    void Tick() override;
    Game::NetworkSide GetNetworkSide() const override;
    bool Start(std::string listenIp, unsigned int listenPort, Game::NetworkSide networkSide) override;
    void Stop() override;
    void ConnectAsync(std::string destinationIp, unsigned int destinationPort) override;
    bool IsConnectedToServer() const override;
    void ExecuteWithServerHostsChannels(std::function<void(Game::IHostChannel&, Game::IHostChannel&)> function) override;
    void ExecuteWithClientHostsChannel(std::function<void(Game::IHost&, Game::IHostChannel&, Game::IHostChannel&)> function) override;
    Game::IHostsManager* GetHostsManager() const override;
    Game::INetworkMessagesFactory* GetMessagesFactory() const override;
    Game::INetworkMessagesHandlersManager* GetMessagesHandlersManager() const override;

    void SetNetworkMessagesHandlersManager(Game::INetworkMessagesHandlersManager* networkMessagesHandlersManager);



public:
    Game::INetworkMessagesHandlersManager* networkMessagesHandlersManager = nullptr;
};



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef MOCKS_NETWORK_MESSAGES_HANDLER_MOCK_H
#define MOCKS_NETWORK_MESSAGES_HANDLER_MOCK_H



#include<memory>

#include<Game/Network/INetworkMessagesHandler.h>



namespace Game
{
	// Forward declarations
	class NetworkMessage;
	class IHost;
}



class NetworkMessagesHandlerMock : public Game::INetworkMessagesHandler
{
public:
	~NetworkMessagesHandlerMock() override;

	void Execute(Game::IHost& remoteHost, std::shared_ptr<Game::NetworkMessage>& message) override;
	Game::NetworkMessageTypeId GetMessageTypeId() const override;



public:
	Game::NetworkMessageTypeId messageTypeId{ 1 };
};



#endif

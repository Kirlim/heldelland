
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"MetadataInformationMock.h"



MetadataInformationMock::MetadataInformationMock(Nest::ResourceType resourceType, std::string resourceName) :
	IMetadataInformation(std::move(resourceName)),
	m_ResourceType(resourceType)
{
	// Nothing here
}



MetadataInformationMock::~MetadataInformationMock() = default;



Nest::ResourceType MetadataInformationMock::GetResourceType() const
{
	return(m_ResourceType);
}



int MetadataInformationMock::GetLoadMetadataFromYamlNodeCallsCount() const
{
	return(m_LoadMetadataFromYamlNodeCallsCount);
}



int MetadataInformationMock::GetLoadResourceIntoManagerCallsCount() const
{
	return(m_LoadResourceIntoManagerCallsCount);
}



void MetadataInformationMock::LoadMetadataFromYamlNode(const YAML::Node& node, std::string metadataPath)
{
	++m_LoadMetadataFromYamlNodeCallsCount;
	m_IsLoaded = true;
}



void MetadataInformationMock::LoadResourceIntoManager(Nest::IResourcesManager& resourcesManager)
{
	++m_LoadResourceIntoManagerCallsCount;
}

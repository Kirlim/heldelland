
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"MeshMock.h"



void MeshMock::DoMake(
	const std::vector<Nest::Vector3<float>>& vertices, const std::vector<Nest::Vector3<float>>& normals,
	const std::vector<Nest::Vector2<float>>& uvs, const std::vector<unsigned>& triangles
)
{
	// Nothing here
}



void MeshMock::DoUnload()
{
	// Nothing here
}



void MeshMock::DoRender() const
{
	// Nothing here
}



bool MeshMock::DoIsLoaded() const
{
	// Nothing here
	return(true);
}
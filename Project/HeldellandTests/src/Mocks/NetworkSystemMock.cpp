//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"NetworkSystemMock.h"



NetworkSystemMock::NetworkSystemMock()
{
	// Nothing here
}



NetworkSystemMock::~NetworkSystemMock() = default;



void NetworkSystemMock::Tick()
{
	// Nothing here
}



Game::NetworkSide NetworkSystemMock::GetNetworkSide() const
{
	// Nothing here
	return (Game::NetworkSide::Server);
}



bool NetworkSystemMock::Start(std::string listenIp, const unsigned int listenPort, Game::NetworkSide networkSide)
{
	// Nothing here
	return (false);
}



void NetworkSystemMock::Stop()
{
	// Nothing here
}



void NetworkSystemMock::ConnectAsync(std::string destinationIp, const unsigned int destinationPort)
{
	// Nothing here
}



bool NetworkSystemMock::IsConnectedToServer() const
{
	// Nothing here
	return (false);
}



void NetworkSystemMock::ExecuteWithServerHostsChannels(std::function<void(Game::IHostChannel&, Game::IHostChannel&)> function)
{
	// Nothing here
}



void NetworkSystemMock::ExecuteWithClientHostsChannel(std::function<void(Game::IHost&, Game::IHostChannel&, Game::IHostChannel&)> function)
{
	// Nothing here
}



Game::IHostsManager* NetworkSystemMock::GetHostsManager() const
{
	// Nothing here
	return (nullptr);
}



Game::INetworkMessagesFactory* NetworkSystemMock::GetMessagesFactory() const
{
	// Nothing here
	return (nullptr);
}



Game::INetworkMessagesHandlersManager* NetworkSystemMock::GetMessagesHandlersManager() const
{
	return (networkMessagesHandlersManager);
}



void NetworkSystemMock::SetNetworkMessagesHandlersManager(Game::INetworkMessagesHandlersManager* networkMessagesHandlersManager)
{
	this->networkMessagesHandlersManager = networkMessagesHandlersManager;
}

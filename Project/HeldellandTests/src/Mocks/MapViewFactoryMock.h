
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef MOCKS__MAP_VIEW_FACTORY_MOCK_H
#define MOCKS__MAP_VIEW_FACTORY_MOCK_H

#include<memory>

#include<Game/Map/IMapViewFactory.h>
#include<Game/Map/Map.h>
#include<Game/Map/MapView.h>



class MapViewFactoryMock : public Game::IMapViewFactory
{
public:
	std::unique_ptr<Game::MapView> Instantiate(Game::Map&) const override {return nullptr;};
};



#endif


//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"ServerSimulationCommandMock.h"

#include<Game/Simulation/ServerGameSimulation.h>



ServerSimulationCommandMock::ServerSimulationCommandMock(std::string message)
    : message(message)
{
    // Nothing here
}



ServerSimulationCommandMock::~ServerSimulationCommandMock()
{
    // Nothing here
}



void ServerSimulationCommandMock::Execute(Game::ServerGameSimulation& simulation)
{
    // Nothing here
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef MOCKS__CLIENT_SIMULATION_COMMAND_MOCK_H
#define MOCKS__CLIENT_SIMULATION_COMMAND_MOCK_H

#include<string>

#include<Game/Network/NetworkMessage.h>
#include<Game/Simulation/ClientSimulationCommand.h>



// TODO : ClientSimulationCommand should itself inherit from NetworkMessage
class ClientSimulationCommandMock : public Game::ClientSimulationCommand, public Game::NetworkMessage
{
public:
	ClientSimulationCommandMock(std::string message);
	~ClientSimulationCommandMock() override;

	void Execute(Game::IGameClientSimulation& simulation) override;


	bool GetBitsSize(unsigned& outBitsCount) override;
	Game::NetworkMessageTypeId GetTypeId() const override;



protected:
	bool DoSerialize(Game::INetworkSerializer& serializer) override;



public:
	std::string message;
};



#endif

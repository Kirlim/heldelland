//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef MOCKS__PLAYER_SIMULATION_RANGE_MANAGER_H
#define MOCKS__PLAYER_SIMULATION_RANGE_MANAGER_H

#include<Game/Simulation/IPlayerSimulationRangeManager.h>
#include<Game/SimulationCommons/SimulationTypes.h>



namespace Game
{
	// Forward declarations
	class INetworkSerializer;
	class SimActorMovementBehavion;
	enum class NetworkMessageReliabilityType;
}



class PlayerSimulationRangeManagerMock : public Game::IPlayerSimulationRangeManager
{
public:
	~PlayerSimulationRangeManagerMock() override;

	void SetViewRadius(Game::simfix64 viewRadius) override;
	void SetPlayerPosition(Nest::Vector3<Game::simfix64> playerPosition) override;

	bool IsEntityRegistered(Game::ActorNetworkId actorNetworkId) const override;

	std::vector<Game::ActorNetworkId> RegisterAndReturnMovingEntitiesThatCameWithinRange() override;
	std::vector<Game::ActorNetworkId> UnregisterAndReturnMovingEntitiesThatWentOutOfRange() override;

	void GetRegisteredActorsNetworkIdsByAppending(std::vector<Game::ActorNetworkId>& out_RegisteredActors) const override;
};



#endif

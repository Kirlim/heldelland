//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"ClientSimulationCommandMock.h"

#include<Game/Simulation/ClientGameSimulation.h>



ClientSimulationCommandMock::ClientSimulationCommandMock(std::string message) :
	NetworkMessage(Game::NetworkMessageReliabilityType::Reliable),
	message(std::move(message))
{
	// Nothing here
}



ClientSimulationCommandMock::~ClientSimulationCommandMock() = default;



void ClientSimulationCommandMock::Execute(Game::IGameClientSimulation& simulation)
{
	// Nothing here
}



bool ClientSimulationCommandMock::GetBitsSize(unsigned& outBitsCount)
{
	// Nothing here
	return false;
}



Game::NetworkMessageTypeId ClientSimulationCommandMock::GetTypeId() const
{
	// Nothing here
	return Game::NetworkMessageTypeId{ 0 };
}



bool ClientSimulationCommandMock::DoSerialize(Game::INetworkSerializer& serializer)
{
	// Nothing here
	return false;
}

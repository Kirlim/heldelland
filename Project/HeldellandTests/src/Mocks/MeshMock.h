
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef MOCKS__MESH_MOCK_H
#define MOCKS__MESH_MOCK_H

#include<Nest/Mesh.h>



class MeshMock : public Nest::Mesh
{
private:
	void DoMake(
		const std::vector<Nest::Vector3<float>>& vertices, const std::vector<Nest::Vector3<float>>& normals,
		const std::vector<Nest::Vector2<float>>& uvs, const std::vector<unsigned>& triangles
	) override;
	void DoUnload() override;
	void DoRender() const override;
	bool DoIsLoaded() const override;
};


#endif

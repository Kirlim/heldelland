
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"NetworkMessageMock.h"

#include<Game/Network/INetworkSerializer.h>



Game::NetworkMessageTypeId NetworkMessageMock::TypeId{ 159 };



NetworkMessageMock::NetworkMessageMock(Game::NetworkMessageReliabilityType messageType, unsigned int value)
	: NetworkMessage(messageType), value(value)
{
	// Nothing here
}



NetworkMessageMock::~NetworkMessageMock()
{
	// Nothing here
}



Game::NetworkMessageTypeId NetworkMessageMock::GetTypeId() const
{
	return TypeId;
}



bool NetworkMessageMock::DoSerialize(Game::INetworkSerializer& serializer)
{
	auto typeId = GetTypeId();
	auto serializeValue = uint32_t(value);

	return(
		serializer.SerializeUChar(typeId.value) &&
		serializer.SerializeUInt32(serializeValue, 32)
		);
}

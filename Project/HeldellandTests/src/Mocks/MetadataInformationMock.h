
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef MOCKS__METADATA_INFORMATION_MOCK_H
#define MOCKS__METADATA_INFORMATION_MOCK_H

#include<string>

#include<Nest/ResourcesTypes.h>
#include<Nest/Metadata/IMetadataInformation.h>

namespace Nest
{
	// Forward declarations
	class IResourcesManager;
}



class MetadataInformationMock : public Nest::IMetadataInformation
{
public:
	MetadataInformationMock(Nest::ResourceType resourceType, std::string resourceName);
	~MetadataInformationMock() override;

	Nest::ResourceType GetResourceType() const override;

	int GetLoadMetadataFromYamlNodeCallsCount() const;
	int GetLoadResourceIntoManagerCallsCount() const;



protected:
	void LoadMetadataFromYamlNode(const YAML::Node& node, std::string metadataPath) override;
	void LoadResourceIntoManager(Nest::IResourcesManager& resourcesManager) override;



protected:
	Nest::ResourceType m_ResourceType;
	int m_LoadMetadataFromYamlNodeCallsCount = 0;
	int m_LoadResourceIntoManagerCallsCount = 0;
};



#endif

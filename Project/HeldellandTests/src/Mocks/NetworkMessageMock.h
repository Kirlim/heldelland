
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef MOCKS__NETWORK_MESSAGE_MOCK_H
#define MOCKS__NETWORK_MESSAGE_MOCK_H

#include<Game/Network/NetworkMessage.h>



namespace Game
{
    // Forward declarations
    class INetworkSerializer;
    enum class NetworkMessageReliabilityType;
}



class NetworkMessageMock : public Game::NetworkMessage
{
public:
    NetworkMessageMock(Game::NetworkMessageReliabilityType messageType, unsigned int value);
    ~NetworkMessageMock() override;
    Game::NetworkMessageTypeId GetTypeId() const override final;

public:
    unsigned int value;
    static Game::NetworkMessageTypeId TypeId;

protected:
    bool DoSerialize(Game::INetworkSerializer& serializer) override final;
};



#endif

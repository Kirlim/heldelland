
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"NetworkMessagesHandlerMock.h"

#include<Game/Network/NetworkMessage.h>
#include<Game/Network/IHost.h>



NetworkMessagesHandlerMock::~NetworkMessagesHandlerMock()
{
    // Nothing here
}



void NetworkMessagesHandlerMock::Execute(Game::IHost& remoteHost, std::shared_ptr<Game::NetworkMessage>& message)
{
    // Nothinge here
}



Game::NetworkMessageTypeId NetworkMessagesHandlerMock::GetMessageTypeId() const
{
    return messageTypeId;
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"PlayerSimulationRangeManagerMock.h"

#include<Nest/Vector3.h>



PlayerSimulationRangeManagerMock::~PlayerSimulationRangeManagerMock()
{
	// Nothing here
}



void PlayerSimulationRangeManagerMock::SetViewRadius(Game::simfix64 viewRadius)
{
	// Nothing here
}



void PlayerSimulationRangeManagerMock::SetPlayerPosition(Nest::Vector3<Game::simfix64> playerPosition)
{
	// Nothing here
}



bool PlayerSimulationRangeManagerMock::IsEntityRegistered(Game::ActorNetworkId actorId) const
{
	// Nothing here
	return false;
}



std::vector<Game::ActorNetworkId> PlayerSimulationRangeManagerMock::RegisterAndReturnMovingEntitiesThatCameWithinRange()
{
	// Nothing here
	return std::vector<Game::ActorNetworkId>{};
}



std::vector<Game::ActorNetworkId> PlayerSimulationRangeManagerMock::UnregisterAndReturnMovingEntitiesThatWentOutOfRange()
{
	// Nothing here
	return std::vector<Game::ActorNetworkId>{};
}



void PlayerSimulationRangeManagerMock::GetRegisteredActorsNetworkIdsByAppending(std::vector<Game::ActorNetworkId>& out_RegisteredActors) const
{
	// Nothinge here
}

//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#ifndef MOCKS__SERVER_SIMULATION_COMMAND_MOCK_H
#define MOCKS__SERVER_SIMULATION_COMMAND_MOCK_H

#include<string>

#include<Game/Simulation/ServerSimulationCommand.h>



namespace Game
{
    // Forward declarations
    class ServerGameSimulation;
}



class ServerSimulationCommandMock : public Game::ServerSimulationCommand
{
public:
    ServerSimulationCommandMock(std::string message);
    virtual ~ServerSimulationCommandMock();

    virtual void Execute(Game::ServerGameSimulation& simulation) override;



public:
    std::string message;
};

#endif

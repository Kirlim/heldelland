
//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<Game/Simulation/ClientSimulationCommandsQueue.h>

#include"Mocks/ClientSimulationCommandMock.h"

using namespace Game;



namespace SimulationCommandQueueTests
{
	TEST_CASE("Enqueueing a new command to the commands queue", "[ClientSimulationCommandsQueue]")
	{
		ClientSimulationCommandsQueue commandsQueue;

		SECTION("Enqueueing a new valid command")
		{
			REQUIRE(commandsQueue.Size() == 0);
			commandsQueue.Enqueue(std::make_shared<ClientSimulationCommandMock>("command"));
			REQUIRE(commandsQueue.Size() == 1);
		}

		SECTION("Enqueueing a null command throws an exception")
		{
			REQUIRE_THROWS(commandsQueue.Enqueue(nullptr));
		}
	}

	TEST_CASE("Dequeueing added commands from the commands queue", "[ClientSimulationCommandsQueue]")
	{
		ClientSimulationCommandsQueue commandsQueue;

		SECTION("Dequeueing enqueued commands, in order")
		{
			REQUIRE_FALSE(commandsQueue.HasCommands());

			commandsQueue.Enqueue(std::make_shared<ClientSimulationCommandMock>("command1"));
			commandsQueue.Enqueue(std::make_shared<ClientSimulationCommandMock>("command2"));
			commandsQueue.Enqueue(std::make_shared<ClientSimulationCommandMock>("command3"));

			REQUIRE(commandsQueue.HasCommands());

			auto command1 = std::dynamic_pointer_cast<ClientSimulationCommandMock>(commandsQueue.Dequeue());
			auto command2 = std::dynamic_pointer_cast<ClientSimulationCommandMock>(commandsQueue.Dequeue());
			auto command3 = std::dynamic_pointer_cast<ClientSimulationCommandMock>(commandsQueue.Dequeue());

			REQUIRE(command1->message == "command1");
			REQUIRE(command2->message == "command2");
			REQUIRE(command3->message == "command3");

			REQUIRE_FALSE(commandsQueue.HasCommands());
		}

		SECTION("Dequeueing an empty queue returns null.")
		{
			auto command = commandsQueue.Dequeue();
			REQUIRE(command == nullptr);
		}
	}

	TEST_CASE("Calling clear removes all the commands from the queue", "[ClientSimulationCommandsQueue]")
	{
		ClientSimulationCommandsQueue commandsQueue;

		commandsQueue.Enqueue(std::make_shared<ClientSimulationCommandMock>("command1"));
		commandsQueue.Enqueue(std::make_shared<ClientSimulationCommandMock>("command2"));
		commandsQueue.Enqueue(std::make_shared<ClientSimulationCommandMock>("command3"));

		REQUIRE(commandsQueue.Size() == 3);
		commandsQueue.Clear();
		REQUIRE(commandsQueue.Size() == 0);
	}
}

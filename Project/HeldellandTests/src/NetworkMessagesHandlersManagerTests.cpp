//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<memory>

#include<Game/Network/INetworkMessagesHandler.h>
#include<Game/Network/NetworkMessagesHandlersManager.h>

#include"Mocks/NetworkMessagesHandlerMock.h"
#include"Mocks/NetworkMessageMock.h"

using namespace Game;



namespace NetworkMessagesHandlersManagerTests
{
	TEST_CASE("NetworkMessagesHandlersManager::AddHandler", "[NetworkMessagesHandlersManager]")
	{
		NetworkMessagesHandlersManager manager{};

		SECTION("Adding null handler throws exception")
		{
			REQUIRE_THROWS(manager.AddHandler(nullptr));

			std::unique_ptr<INetworkMessagesHandler> nullHandler;
			REQUIRE_THROWS(manager.AddHandler(std::move(nullHandler)));
		}



		SECTION("Adding repeated handler id throws exception")
		{
			auto handler1 = std::make_unique<NetworkMessagesHandlerMock>();
			handler1->messageTypeId = NetworkMessageTypeId{ 1 };

			auto handler2 = std::make_unique<NetworkMessagesHandlerMock>();
			handler2->messageTypeId = NetworkMessageTypeId{ 1 };

			REQUIRE_NOTHROW(manager.AddHandler(std::move(handler1)));
			REQUIRE_THROWS(manager.AddHandler(std::move(handler2)));
		}
	}



	TEST_CASE("NetworkMessagesHandlersManager::GetHandler", "[NetworkMessagesHandlersManager]")
	{
		NetworkMessagesHandlersManager manager{};

		SECTION("Getting with unregistered type id returns null")
		{
			auto returnedHandler = manager.GetHandler(NetworkMessageTypeId{ 100 });
			REQUIRE(returnedHandler == nullptr);
		}

		SECTION("Getting with message of unregistered type id returns null")
		{
			auto handler = std::make_unique<NetworkMessagesHandlerMock>();
			handler->messageTypeId = NetworkMessageTypeId{ static_cast<unsigned char>(NetworkMessageMock::TypeId.value + 1) };
			manager.AddHandler(std::move(handler));

			NetworkMessageMock messageMock{ NetworkMessageReliabilityType::Unreliable, 1 };

			REQUIRE(manager.GetHandler(messageMock) == nullptr);
		}

		SECTION("Getting with registered type id")
		{
			auto handler = std::make_unique<NetworkMessagesHandlerMock>();
			const auto handlerAddress = handler.get();
			handler->messageTypeId = NetworkMessageMock::TypeId;
			manager.AddHandler(std::move(handler));

			REQUIRE_NOTHROW((manager.GetHandler(NetworkMessageTypeId{ NetworkMessageMock::TypeId }) == handlerAddress));
		}

		SECTION("Getting with message of registered type id")
		{
			auto handler = std::make_unique<NetworkMessagesHandlerMock>();
			const auto handlerAddress = handler.get();
			handler->messageTypeId = NetworkMessageMock::TypeId;
			manager.AddHandler(std::move(handler));

			NetworkMessageMock messageMock{ NetworkMessageReliabilityType::Unreliable, 1 };

			REQUIRE_NOTHROW((manager.GetHandler(messageMock) == handlerAddress));
		}
	}



	TEST_CASE("NetworkMessagesHandlersManager::RemoveHandler", "[NetworkMessagesHandlersManager]")
	{
		NetworkMessagesHandlersManager manager{};

		SECTION("Removing registered type id")
		{
			auto handler = std::make_unique<NetworkMessagesHandlerMock>();
			handler->messageTypeId = NetworkMessageTypeId{ 1 };
			manager.AddHandler(std::move(handler));

			REQUIRE_NOTHROW(manager.RemoveHandler(NetworkMessageTypeId{ 1 }));
		}
	}



	TEST_CASE("NetworkMessagesHandlersManager::GetAllRegisteredNetworkMessageTypeIds", "[NetworkMessagesHandlersManager]")
	{
		NetworkMessagesHandlersManager manager{};


		SECTION("Recovering all registered type ids")
		{
			const auto recovered1 = manager.GetAllRegisteredNetworkMessageTypeIds();
			REQUIRE(recovered1.empty());

			auto handler1 = std::make_unique<NetworkMessagesHandlerMock>();
			handler1->messageTypeId = NetworkMessageTypeId{ 1 };
			manager.AddHandler(std::move(handler1));

			const auto recovered2 = manager.GetAllRegisteredNetworkMessageTypeIds();
			REQUIRE(recovered2.size() == 1);
			REQUIRE(recovered2[0] == NetworkMessageTypeId{ 1 });

			auto handler2 = std::make_unique<NetworkMessagesHandlerMock>();
			handler2->messageTypeId = NetworkMessageTypeId{ 2 };
			manager.AddHandler(std::move(handler2));

			const auto recovered3 = manager.GetAllRegisteredNetworkMessageTypeIds();
			REQUIRE(recovered3.size() == 2);
			REQUIRE(recovered3[0] == NetworkMessageTypeId{ 1 });
			REQUIRE(recovered3[1] == NetworkMessageTypeId{ 2 });
		}
	}
}

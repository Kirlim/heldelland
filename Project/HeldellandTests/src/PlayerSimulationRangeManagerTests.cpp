//
// Copyright (c) Eric Naves dos Reis. All rights reserved.  
// Licensed under the BSD 3-Clause License. See LICENSE file in the project root for full license information.  
//

#include"catch.hpp"

#include<Nest/NestAssert.h>
#include<Nest/IEntity.h>
#include<Nest/EntitiesManager.h>
using namespace Nest;

#include<Game/SimulationCommons/SimulationActorsMovementData.h>
#include<Game/Simulation/PlayerSimulationRangeManager.h>


using namespace Game;

#include"Helpers/EntitiesManagerHelper.h"



namespace
{
	void SetActorPosition(SimulationActorsMovementData& simulationActorsMovementData, ActorNetworkId actorNetworkId, simfix64 xPos, simfix64 yPos)
	{
		const auto actorIndex = simulationActorsMovementData.GetNetworkActorIndex(actorNetworkId);
		NestAssert(actorIndex >= 0);

		simulationActorsMovementData.actorsInFrameMovementPositions[actorIndex].frameStartX = xPos;
		simulationActorsMovementData.actorsInFrameMovementPositions[actorIndex].frameStartY = yPos;
		simulationActorsMovementData.actorsInFrameMovementPositions[actorIndex].frameEndX = xPos;
		simulationActorsMovementData.actorsInFrameMovementPositions[actorIndex].frameEndY = yPos;
	}



	void InstantiateActorAt(SimulationActorsMovementData& simulationActorsMovementData, ActorNetworkId actorNetworkId, simfix64 xPos, simfix64 yPos)
	{
		simulationActorsMovementData.RegisterNetworkActor(actorNetworkId);
		SetActorPosition(simulationActorsMovementData, actorNetworkId, xPos, yPos);
	}
}



namespace PlayerSimulationRangeManagerTests
{
	struct MainClasses
	{
		std::unique_ptr<SimulationActorsMovementData> simulationActorsMovementData;
		std::unique_ptr<PlayerSimulationRangeManager> playerSimulationRangeManager;

		MainClasses() :
			simulationActorsMovementData(std::make_unique<SimulationActorsMovementData>()),
			playerSimulationRangeManager(std::make_unique<PlayerSimulationRangeManager>(*simulationActorsMovementData))
		{
		}
	};



	TEST_CASE("PlayerSimulationRangeManager::RegisterMovingEntitiesThatCameWithinRange", "[PlayerSimulationRangeManager]")
	{
		MainClasses mainClasses;
		const auto simulationActorsMovementData = mainClasses.simulationActorsMovementData.get();
		auto playerSimulationRangeManager = mainClasses.playerSimulationRangeManager.get();


		SECTION("Actor within range is registered")
		{
			const ActorNetworkId actorNetworkId{ 1 };
			InstantiateActorAt(*simulationActorsMovementData, actorNetworkId, simfix64(100), simfix64(0));

			playerSimulationRangeManager->SetPlayerPosition(Nest::Vector3<simfix64>(simfix64(199), simfix64(0), simfix64(0)));
			playerSimulationRangeManager->SetViewRadius(simfix64(100));
			playerSimulationRangeManager->RegisterAndReturnMovingEntitiesThatCameWithinRange();

			REQUIRE(playerSimulationRangeManager->IsEntityRegistered(actorNetworkId));
		}


		SECTION("Two actors within range are registered")
		{
			const ActorNetworkId actor1NetworkId{ 1 };
			InstantiateActorAt(*simulationActorsMovementData, actor1NetworkId, simfix64(100), simfix64(0));

			const ActorNetworkId actor2NetworkId{ 2 };
			InstantiateActorAt(*simulationActorsMovementData, actor2NetworkId, simfix64(0), simfix64(100));

			playerSimulationRangeManager->SetPlayerPosition(Nest::Vector3<simfix64>(simfix64(50), simfix64(0), simfix64(50)));
			playerSimulationRangeManager->SetViewRadius(simfix64(100));
			playerSimulationRangeManager->RegisterAndReturnMovingEntitiesThatCameWithinRange();

			REQUIRE(playerSimulationRangeManager->IsEntityRegistered(actor1NetworkId));
			REQUIRE(playerSimulationRangeManager->IsEntityRegistered(actor2NetworkId));
		}


		SECTION("Actor outside range is not registered")
		{
			const ActorNetworkId actorNetworkId{ 1 };
			InstantiateActorAt(*simulationActorsMovementData, actorNetworkId, simfix64(0), simfix64(1000));

			playerSimulationRangeManager->SetPlayerPosition(Nest::Vector3<simfix64>(simfix64(101), simfix64(0), simfix64(0)));
			playerSimulationRangeManager->SetViewRadius(simfix64(50));
			playerSimulationRangeManager->RegisterAndReturnMovingEntitiesThatCameWithinRange();

			REQUIRE_FALSE(playerSimulationRangeManager->IsEntityRegistered(actorNetworkId));
		}


		SECTION("Actor is not registered twice")
		{
			// SimActorsTracker will throw exception if same id is registered twice
			const ActorNetworkId actorNetworkId{ 1 };
			InstantiateActorAt(*simulationActorsMovementData, actorNetworkId, simfix64(100), simfix64(0));

			playerSimulationRangeManager->SetPlayerPosition(Nest::Vector3<simfix64>(simfix64(199), simfix64(0), simfix64(0)));
			playerSimulationRangeManager->SetViewRadius(simfix64(100));
			playerSimulationRangeManager->RegisterAndReturnMovingEntitiesThatCameWithinRange();
			REQUIRE_NOTHROW(playerSimulationRangeManager->RegisterAndReturnMovingEntitiesThatCameWithinRange());
		}
	}


	TEST_CASE("PlayerSimulationRangeManager::UnregisterMovingEntitiesThatWentOutOfRange", "[PlayerSimulationRangeManager]")
	{
		MainClasses mainClasses;
		const auto simulationActorsMovementData = mainClasses.simulationActorsMovementData.get();
		auto playerSimulationRangeManager = mainClasses.playerSimulationRangeManager.get();


		SECTION("Registered actor is unregistered if out of range")
		{
			const ActorNetworkId actorNetworkId{ 1 };
			InstantiateActorAt(*simulationActorsMovementData, actorNetworkId, simfix64(100), simfix64(0));

			playerSimulationRangeManager->SetPlayerPosition(Nest::Vector3<simfix64>(simfix64(199), simfix64(0), simfix64(0)));
			playerSimulationRangeManager->SetViewRadius(simfix64(100));
			playerSimulationRangeManager->RegisterAndReturnMovingEntitiesThatCameWithinRange();
			playerSimulationRangeManager->UnregisterAndReturnMovingEntitiesThatWentOutOfRange();

			REQUIRE(playerSimulationRangeManager->IsEntityRegistered(actorNetworkId));

			playerSimulationRangeManager->SetViewRadius(simfix64(1));
			playerSimulationRangeManager->UnregisterAndReturnMovingEntitiesThatWentOutOfRange();
			REQUIRE_FALSE(playerSimulationRangeManager->IsEntityRegistered(actorNetworkId));

			REQUIRE_NOTHROW(playerSimulationRangeManager->UnregisterAndReturnMovingEntitiesThatWentOutOfRange());
		}


		SECTION("Registered actor is not unregistered twice")
		{
			// SimActorsTracker will throw exception if same id is unregistered twice
			const ActorNetworkId actorNetworkId{ 1 };
			InstantiateActorAt(*simulationActorsMovementData, actorNetworkId, simfix64(100), simfix64(0));

			playerSimulationRangeManager->SetPlayerPosition(Nest::Vector3<simfix64>(simfix64(199), simfix64(0), simfix64(0)));
			playerSimulationRangeManager->SetViewRadius(simfix64(100));
			playerSimulationRangeManager->RegisterAndReturnMovingEntitiesThatCameWithinRange();
			playerSimulationRangeManager->UnregisterAndReturnMovingEntitiesThatWentOutOfRange();
			playerSimulationRangeManager->SetViewRadius(simfix64(1));
			playerSimulationRangeManager->UnregisterAndReturnMovingEntitiesThatWentOutOfRange();
			REQUIRE_NOTHROW(playerSimulationRangeManager->UnregisterAndReturnMovingEntitiesThatWentOutOfRange());
		}
	}


	TEST_CASE("PlayerSimulationRangeManager register and unregister callbacks are called.", "[PlayerSimulationRangeManager]")
	{
		MainClasses mainClasses;
		const auto simulationActorsMovementData = mainClasses.simulationActorsMovementData.get();
		auto playerSimulationRangeManager = mainClasses.playerSimulationRangeManager.get();


		SECTION("Registered actor is unregistered if out of range")
		{
			const ActorNetworkId actorNetworkId{ 1 };
			InstantiateActorAt(*simulationActorsMovementData, actorNetworkId, simfix64(100), simfix64(0));

			playerSimulationRangeManager->SetPlayerPosition(Nest::Vector3<simfix64>(simfix64(199), simfix64(0), simfix64(0)));
			playerSimulationRangeManager->SetViewRadius(simfix64(100));

			const auto& registeredIds = playerSimulationRangeManager->RegisterAndReturnMovingEntitiesThatCameWithinRange();
			playerSimulationRangeManager->UnregisterAndReturnMovingEntitiesThatWentOutOfRange();
			REQUIRE(registeredIds.size() == 1);
			REQUIRE(registeredIds[0] == actorNetworkId);

			playerSimulationRangeManager->SetViewRadius(simfix64(1));

			const auto& unregisteredIds = playerSimulationRangeManager->UnregisterAndReturnMovingEntitiesThatWentOutOfRange();
			REQUIRE(unregisteredIds.size() == 1);
			REQUIRE(unregisteredIds[0] == actorNetworkId);
		}
	}


	TEST_CASE("PlayerSimulationRangeManager::GetRegisteredActorsIdsByAppending", "[PlayerSimulationRangeManager]")
	{
		MainClasses mainClasses;
		const auto simulationActorsMovementData = mainClasses.simulationActorsMovementData.get();
		auto playerSimulationRangeManager = mainClasses.playerSimulationRangeManager.get();


		SECTION("No actor nework ids are appended if they're not registered")
		{
			std::vector<ActorNetworkId> outIds{};

			REQUIRE_NOTHROW(playerSimulationRangeManager->GetRegisteredActorsNetworkIdsByAppending(outIds));
			REQUIRE(outIds.size() == 0);
		}


		SECTION("All registered actor nework ids are returned")
		{
			const ActorNetworkId actor1NetworkId{ 1 };
			InstantiateActorAt(*simulationActorsMovementData, actor1NetworkId, simfix64(100), simfix64(0));

			const ActorNetworkId actor2NetworkId{ 2 };
			InstantiateActorAt(*simulationActorsMovementData, actor2NetworkId, simfix64(0), simfix64(100));

			playerSimulationRangeManager->SetPlayerPosition(Nest::Vector3<simfix64>(simfix64(50), simfix64(0), simfix64(50)));
			playerSimulationRangeManager->SetViewRadius(simfix64(100));
			playerSimulationRangeManager->RegisterAndReturnMovingEntitiesThatCameWithinRange();

			std::vector<ActorNetworkId> outIds{};
			REQUIRE_NOTHROW(playerSimulationRangeManager->GetRegisteredActorsNetworkIdsByAppending(outIds));
			REQUIRE(outIds.size() == 2);

			REQUIRE(std::any_of(std::begin(outIds), std::end(outIds), [&](const ActorNetworkId& id) { return (actor1NetworkId == id);}));
			REQUIRE(std::any_of(std::begin(outIds), std::end(outIds), [&](const ActorNetworkId& id) { return (actor2NetworkId == id);}));
		}


		SECTION("Actors are appended - list is not cleared")
		{
			const ActorNetworkId actor2NetworkId{ 2 };
			InstantiateActorAt(*simulationActorsMovementData, actor2NetworkId, simfix64(0), simfix64(100));

			playerSimulationRangeManager->SetPlayerPosition(Nest::Vector3<simfix64>(simfix64(50), simfix64(0), simfix64(50)));
			playerSimulationRangeManager->SetViewRadius(simfix64(100));
			playerSimulationRangeManager->RegisterAndReturnMovingEntitiesThatCameWithinRange();

			std::vector<ActorNetworkId> outIds{};

			ActorNetworkId actor1NetworkId{ 1 };
			outIds.emplace_back(actor1NetworkId);

			REQUIRE_NOTHROW(playerSimulationRangeManager->GetRegisteredActorsNetworkIdsByAppending(outIds));
			REQUIRE(outIds.size() == 2);

			REQUIRE(std::any_of(std::begin(outIds), std::end(outIds), [&](const ActorNetworkId& id) { return (actor1NetworkId == id);}));
			REQUIRE(std::any_of(std::begin(outIds), std::end(outIds), [&](const ActorNetworkId& id) { return (actor2NetworkId == id);}));
		}
	}
}

# README #

Heldelland is a game in development aiming to be an over-simplified
RPG Tower Defense game. The game is being developed on my free time,
and the main objective was for me to get more used to C++ coding as
well as trying out better coding practices.

Tests coverage are low, as I got into Automatic Testing way after
starting this project, but I'm gradually improving testability and
making new tests.

### Should I build it? ###

As the game is of now, no. There's no gameplay, so there's no meaning
in building.

### What is this repository for, right now? ###

Since the game was meant to be under the BSD-3 Clause License, I've
decided to make the source available now, instead of later. Of course,
if you have insights or code reviews to share about this source, I'd
be glad to hear them :)

### Building the project ###

The project is being developed with Visual Studio Community 2017 and
CMake. Every merge to the master, I try to make it buildable on Ubuntu
as well. The dependencies are:

* Assimp
* Asio (Standalone)
* Freetype (2.6.5)
* Glew
* OpenGL
* SDL2
* Soil2
* Suna
* YAML-Cpp

Suna is another project being developed on my free time, for learning
purposes. As soon as I come back to make some changes and continue
some needed features, I'll include the License headers and make the
repository public as well.


### Who do I talk to? ###

* Repo owner or admin